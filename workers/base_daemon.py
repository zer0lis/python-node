# -*- coding: utf-8 -*-
"""
..  py:module:: base_daemon

"""
import os
import sys
import atexit
import time
import logging
import resource
import errno
import signal
import pwd
import grp
import fcntl

#used for abstract classes
from abc import ABCMeta, abstractmethod

# Logger
from common.socrative_api import initDefaultLogger, exceptionStack
from common.socrative_logging import SocrativeSyslogHandler

initDefaultLogger(SocrativeSyslogHandler)
logger = logging.getLogger(__name__)


class SocrativeDaemon(object):
    """
    An abstract class for creating daemons.
    Usage: subclass the SocrativeDaemon class and override the run() and the reload method.
    The reload method can be left unimplemented if it's not needed
    """
    __metaclass__ = ABCMeta

    def __init__(self, inPidfile, changeWorkingDir=False, preventCoreDump=False, inStdin='/dev/null',
                 inStdout='/dev/null', inStderr='/dev/null', onStartExitZero=True, maxOpenFile=1048576):
        """
        Constructor
        :param inPidfile :type String - contains the full path of the file that will be used to store the PID of the
         daemon
        :param changeWorkingDir :type Boolean - True if you want to change the working dir to / .
        use this option only when running the daemon with root rights
        :param preventCoreDump  :type Boolean - True if you want to disable core dumps when the daemon crashes
        :param inStdin :type String - contains the path to the file that will be used as input standard file
        :param inStdout :type String - contains the path to the file that will be used as output standard file
        :param inStderr :type String - contains the path the file that will be used as error standard file
        :param onStartExitZero: If True, on start procedure will exit(0) IF customStart unlock (default).
        Set to False by SpringDaemon.
        :param maxOpenFile: Max open file to set before the fork
        :type maxOpenFile: int
        """

        self.stdin = inStdin
        self.stdout = inStdout
        self.stderr = inStderr
        self.pidfile = inPidfile
        self.prevent_core_dump = preventCoreDump
        self.change_working_dir = changeWorkingDir
        self._onStartExitZero = onStartExitZero
        self._lock = None
        self._maxOpenFile = maxOpenFile

        logger.info("__init__ : stdin=%s", self.stdin)
        logger.info("__init__ : stdout=%s", self.stdout)
        logger.info("__init__ : stderr=%s", self.stderr)
        logger.info("__init__ : pidfile=%s", self.pidfile)
        logger.info("__init__ : prevent_core_dump=%s", self.prevent_core_dump)
        logger.info("__init__ : change_working_dir=%s", self.change_working_dir)
        logger.info("__init__ : _onStartExitZero=%s", self._onStartExitZero)
        logger.info("__init__ : _maxOpenFile=%s", self._maxOpenFile)

    def __redirectStd(self):
        """
        Redirect stdxxx
        """

        # Flush
        logger.info("__redirectStd : flush sys.std")
        sys.stdout.flush()
        sys.stderr.flush()

        # Open new std
        try:
            logger.info("__redirectStd : opening new std")
            si = open(self.stdin, 'r')
            so = open(self.stdout, 'a+')
            se = open(self.stderr, 'a+', 0)

            # Dup std
            logger.info("__redirectStd : duplicate std (you may lost logs now)")
            os.dup2(si.fileno(), sys.stdin.fileno())
            os.dup2(so.fileno(), sys.stdout.fileno())
            os.dup2(se.fileno(), sys.stderr.fileno())
        except Exception as e:
            logger.warn("__redirectStd : std redirect failed, trying alternate now, Exception, ex=%s",
                        exceptionStack(e))
            self.__redirectStdAlternate()

    def __redirectStdAlternate(self):
        """
        Redirect stdxxx
        """

        try:
            # Flush
            logger.info("flush sys.std")
            sys.stdout.flush()
            sys.stderr.flush()

            # Open new std
            logger.info("__redirectStd : opening new std")
            si = open(self.stdin, 'r')
            so = open(self.stdout, 'a+')
            se = open(self.stderr, 'a+', 0)

            # Dup std
            logger.info("__redirectStd : re-assigning std (you may lost logs now) + logger re-init")
            sys.stdin = si
            sys.stdout = so
            sys.stderr = se
        except Exception as e:
            logger.error("Exception, fatal, exit(-2) now, ex=%s", exceptionStack(e))
            sys.exit(-2)

    def __daemonize(self):
        """
        do the UNIX double-fork magic, see Stevens' "Advanced
        Programming in the UNIX Environment" for details (ISBN 0201563177)
        http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16
        protected method used to create the daemon and run the main loop of the daemonized routine
        on errors it exists with error code
        """

        logger.info("__daemonize : Entering")

        logger.info("__daemonize : Setting max open file=%s", self._maxOpenFile)
        try:
            # Get
            self.softLimit, self.hardLimit = resource.getrlimit(resource.RLIMIT_NOFILE)
            logger.info("RLIMIT_NOFILE BEFORE, soft=%s, hard=%s", self.softLimit, self.hardLimit)

            # Try to set
            resource.setrlimit(resource.RLIMIT_NOFILE, (self._maxOpenFile, self._maxOpenFile))

            # Re-get
            self.softLimit, self.hardLimit = resource.getrlimit(resource.RLIMIT_NOFILE)
            logger.info("RLIMIT_NOFILE AFTER SET, soft=%s, hard=%s", self.softLimit, self.hardLimit)

        except Exception as e:
            self.softLimit, self.hardLimit = resource.getrlimit(resource.RLIMIT_NOFILE)
            logger.warning("Setting max open files failed, soft=%s, hard=%s, required=%s, ex=%s", self.softLimit,
                           self.hardLimit, self._maxOpenFile, exceptionStack(e))
            logger.info("RLIMIT_NOFILE AFTER EX, soft=%s, hard=%s", self.softLimit, self.hardLimit)

        logger.info("__daemonize : fork 1 : go")
        try:
            pid = os.fork()
            if pid > 0:
                # exit first parent
                sys.exit(0)
        except OSError as e:
            logger.error('__daemonize : fork 1 failed, going to exit(1) : errno=%s err=%s', e.errno, e.strerror)
            sys.exit(1)

        # decouple from parent environment
        if self.change_working_dir:
            logger.info("__daemonize : change_working_dir : go")
            os.chdir("/")

        logger.info("__daemonize : setsid/umask")
        os.setsid()
        os.umask(0)

        # do second fork
        logger.info("__daemonize : fork 2 : go")
        try:
            pid = os.fork()
            if pid > 0:
                # exit from second parent
                sys.exit(0)
            else:
                pass
        except OSError as e:
            logger.error('__daemonize : fork 2 failed, going to exit(1) : errno=%s err=%s', e.errno, e.strerror)
            sys.exit(1)

        # Log
        logger.info("__daemonize : process pid=%s", os.getpid())

        # Redirect std
        self.__redirectStd()

        # Go
        logger.info("__daemonize : initializing pidFile=%s", self.pidfile)

        # Register the method called at exit
        atexit.register(self.__delPID)

        # Write pidfile
        pid = str(os.getpid())
        try:
            f = open(self.pidfile, 'w')
            f.write("%s" % pid)
            f.close()
            self._lock = open(self.pidfile, "a")
            fcntl.flock(self._lock, fcntl.LOCK_EX | fcntl.LOCK_NB)
            logger.info("wrote PID %s in file %s" % (pid, self.pidfile))
        except IOError as e:
            logger.info("__daemonize : pidFile failed, going to exit(3), ex=%s", exceptionStack(e))
            sys.exit(3)

        # Finish
        logger.info("__daemonize : registering signal handler : SIGUSR1")
        signal.signal(signal.SIGUSR1, self._customDaemonReload)
        logger.info("__daemonize : registering signal handler : SIGUSR2")
        signal.signal(signal.SIGUSR2, self.__daemonStatusHandler)
        logger.info("__daemonize : registering signal handler : SIGTERM")
        signal.signal(signal.SIGTERM, self.__daemonTerminateHandler)

        logger.info("__daemonize : registering  signal handler : Done")

        if self.prevent_core_dump:
            logger.info("__daemonize : preventing core dump")
            self.__daemonPreventCoreDump()

        logger.info("__daemonize : process started, pid=%s", pid)
        logger.info("__daemonize : done")

    def __delPID(self):
        """
        deletes the pidlock file
        method used when stopping the daemon to make sure the pidfile is deleted
        """
        if os.path.exists(self.pidfile):
            os.remove(self.pidfile)

    def __changeUIDAndGUID(self, inUID, inGID):
        """
        method to change the ownership of the current process, to the specified uid and/or gid
        :param inUID :type string - the user that will own the process
        :param inGID :type string - the group that will own the process
        :return: None
        """

        try:
            if inGID is not None:
                gid = grp.getgrnam(inGID).gr_gid
                os.setgid(gid)
            if inUID is not None:
                uid = pwd.getpwnam(inUID).pw_uid
                os.setuid(uid)
        except Exception as e:
            logger.error("__changeUIDAndGUID : Exception ex=%s", exceptionStack(e))

    def _daemonStart(self, uid, gid):
        """
        Start the daemon
        method called to start the daemon after creation...it is called after parsing the arguments from command line
        :param uid :type string - the user to which to change the daemon process to
        :param gid :type string - the group id to which to change the daemon process to
        """
        # Check for a pidfile to see if the daemon already runs
        logger.info("_daemonStart : entering")
        try:
            pf = open(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None

        running = True
        if pid is not None:
            #try to send a SIGUSR2 signal to test if the process exist
            try:
                #test the process existence
                os.kill(int(pid), signal.SIGUSR2)
            except OSError as err:
                if err.errno == errno.ESRCH:  # process not found
                    logger.debug("lockpid file exists but there is no process running with the %s", pid)
                    if os.path.exists(self.pidfile):
                        self.__delPID()
                    running = False
            except Exception as e:
                logger.warn("_daemonStart : Exception on check, ex=%s", exceptionStack(e))
        else:
            running = False

        if not running:
            logger.info("_daemonStart : Not running, calling __daemonize")
            self.__daemonize()

            logger.info("_daemonStart : calling __changeUIDAndGUID")
            try:
                self.__changeUIDAndGUID(uid, gid)
            except Exception as e:
                logger.warn("_daemonStart : Exception on __changeUIDAndGUID, ex=%s", exceptionStack(e))

            # Custom start
            logger.info("_daemonStart : calling  _customDaemonStart")
            try:
                self._customDaemonStart()
            except Exception as e:
                logger.warn("_daemonStart : Exception on _customDaemonStart, ex=%s", exceptionStack(e))

            #=====================
            # CAUTION : With SpringDaemon, this should not happen (customStart will exit the main
            # due to unlock by customStop)
            # So, the exit(0) is USELESS and DISABLED by SpringDaemon
            #=====================

            # Exit
            if self._onStartExitZero is True:
                logger.info("_daemonStart : exiting WITH exit(0) due to _onStartExitZero==True")
                sys.exit(0)
            else:
                logger.info("_daemonStart : exiting WITHOUT exit(0)")
        else:
            # Already running
            logger.info("_daemonStart : Already running, pid=%s", pid)
            logger.info("_daemonStart : exit(1) now")
            sys.exit(1)

    def _daemonStop(self):
        """
        Stop the daemon
        method called after parsing the command line arguments. It will stop the current running daemon
        """

        logger.info("_daemonStop : entering")

        # Get the pid from the pidfile
        try:
            pf = open(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None

        if not pid:
            # Not running
            logger.info("_daemonStop : Daemon is not running, pidFile=%s", self.pidfile)
            # Not an error in a restart
            return

        # Try killing the daemon process
        logger.info("_daemonStop : killing now (SIGTERM), pid=%s, pidFile=%s", pid, self.pidfile)
        try:
            os.kill(int(pid), signal.SIGTERM)
        except OSError as e:
            if e.errno == errno.ESRCH and os.path.exists(self.pidfile):
                #process not found
                self.__delPID()
                logger.info("_daemonStop : Unable to kill - no process, pid=%s, pidFile=%s, ex=%s", pid,
                            self.pidfile, exceptionStack(e))
            else:
                logger.warn("_daemonStop : Strange OSError=%s", exceptionStack(e))
                if os.path.exists(self.pidfile):
                    self.__delPID()

        except Exception as e:
            logger.error("_daemonStop : Unable to kill - exception, pid=%s, pidFile=%s, ex=%s", pid, self.pidfile,
                         exceptionStack(e))
            if os.path.exists(self.pidfile):
                self.__delPID()
            sys.exit(1)

        logger.info("_daemonStop : SIGTERM sent to pid=%s", pid)

        dtStart = time.time()
        stopOk = False

        #wait 15 seconds for the process to stop
        #if after 1 minute the process is still running it will print that the process couldn't be stopped
        #this is just a measure for processes that can block , and won't exit, to know that they couldn't be terminated
        while int(time.time()-dtStart) * 1000 < 30000:
            #check if the pidlock file exists...
            #each process has a descriptor in /proc/pid
            if os.path.exists("/proc/%d" % int(pid)):
                # Sleep
                time.sleep(0.1)
                logger.debug("_daemonStop : waiting for /proc/%s", pid)
            else:
                stopOk = True
                break

        if stopOk is True:
            logger.info("_daemonStop : SIGTERM OK to pid=%s", pid)
            self.__delPID()
        else:
            logger.warn("_daemonStop : SIGTERM TimeOut to pid=%s", pid)

    def _daemonAskReload(self):
        """
        Asks the program that the Daemon loaded to reload the configuration
        method called after parsing the command line arguments.
        Sends a SIGUSR1 to the daemon to make it call reload method
        """
        # Get the pid from the pidfile
        try:
            pf = open(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None

        if not pid:
            logger.warn("_daemonAskReload : Daemon not running, pidfile does not exist, pidFile=%s", self.pidfile)
            return  # not an error in a restart

        # send the SIGUSR1 signal to the process
        try:
            os.kill(int(pid), signal.SIGUSR1)
        except OSError as err:
            if err.errno == errno.ESRCH:
                #process not found
                logger.error("_daemonAskReload : SIGUSR1 failed, going to exit(2), ex=%s", exceptionStack(err))
                self.__delPID()
                sys.exit(2)

        logger.info("_daemonAskReload : SIGUSR1 sent")

    def _daemonRestart(self, uid, gid):
        """
        Restart the daemon. will do stop and start
        method called after parsing the command line arguments .
        Will try to stop the current daemon then it will start it again with the same options as it was created
        """
        logger.info("_daemonRestart : calling _daemonStop")
        self._daemonStop()

        logger.info("_daemonRestart : calling _daemonStart")
        self._daemonStart(uid, gid)

    @abstractmethod
    def _customDaemonStart(self):
        """
        You should override this method when you subclass Daemon. It will be called after the process has been
        daemonized by start() or restart().
        """
        pass

    @abstractmethod
    def _customDaemonReload(self, *args, **kwargs):
        """
        You should override this method when you subclass Daemon.
        It will be called when it will receive a SIGUSR1 signal
        meaning a forced reload from the user
        """
        pass

    @abstractmethod
    def _customDaemonStop(self):
        """
        you should override this method when you subclass Daemon. It will be called before exiting the daemon.
        """
        pass

    def __daemonPreventCoreDump(self):
        """ Prevent this process from generating a core dump.

        Sets the soft and hard limits for core dump size to zero. On
        Unix, this prevents the process from creating core dump
        altogether.

        """
        core_resource = resource.RLIMIT_CORE
        is_possible_to_set_core_limits = True

        try:
            # Ensure the resource limit exists on this platform, by requesting
            # its current value
            resource.getrlimit(core_resource)
        except ValueError as exc:
            logger.error("__daemonPreventCoreDump : System does not support RLIMIT_CORE resource limit=%s", exc)
            is_possible_to_set_core_limits = False

        if is_possible_to_set_core_limits:
            # Set hard and soft limits to zero, i.e. no core dump at all
            core_limit = (0, 0)
            resource.setrlimit(core_resource, core_limit)

    def _daemonStatus(self):
        """
        Checks if the daemon is running or not and displays the message in the stdout/console
        """
        # Get the pid from the pidfile
        try:
            logger.info("_daemonStatus : pidfile=%s", self.pidfile)
            pf = open(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None

        if not pid:
            logger.info("_daemonStatus : Daemon is not running (pidfile KO) : pidfile=%s", self.pidfile)
            return

        #we have the pid file ...we're checking the the process id is valid by sending SIGUSR2 signal
        # send the SIGUSR2 signal to the process -- it's just to check if the process exists or not
        try:
            os.kill(int(pid), signal.SIGUSR2)
        except OSError as err:
            if err.errno == errno.ESRCH:
                #process not found
                logger.info("_daemonStatus : Daemon is not running (pid KO, pidfile ok) : pid=%s, pidfile=%s", pid,
                            self.pidfile)
                self.__delPID()
                sys.exit(0)

        # Ok
        logger.info("_daemonStatus : Daemon is running (pid ok, pidfile ok) : pid=%s, pidfile=%s", pid, self.pidfile)

    #noinspection PyUnusedLocal
    def __daemonStatusHandler(self, *args, **kwargs):
        """
        handler for SIGUSR2. It doesn't have to do anything. It's just sent to check if the process is running
        """
        return

    # noinspection PyUnusedLocal
    def __daemonTerminateHandler(self, *args, **kwargs):
        """
        SIGTERM handler
        """
        logger.info("__daemonTerminateHandler : SIGTERM caught, calling _customDaemonStop")
        self._customDaemonStop()

        fcntl.flock(self._lock, fcntl.LOCK_UN)

        #=====================
        # CAUTION : With SpringDaemon, this should not happen
        # (customStart will exit the main due to unlock by customStop)
        #=====================
        logger.info("__daemonTerminateHandler : Exiting WITH exit(0)")
        sys.exit(0)

    def daemonMain(self, argv):
        """
        ..  py:function:: daemonMain(argv)
        the entry point from the console.
        It accepts the following parameters : start | stop | restart | reload | status
        :param argv: :type list - contains 2 Strings - 1 the daemon path , 2 the option which can be one of the above
        """
        action = None
        user = None
        group = None
        #the number of arguments can be between 2 and 6
        if 6 >= len(argv) >= 2:
            skip = False
            for i in range(1, len(argv)):
                if skip is True:
                    skip = False
                    continue
                arg = argv[i]
                if 'start' == arg:
                    action = 0
                elif 'stop' == arg:
                    action = 1
                elif 'restart' == arg:
                    action = 2
                elif 'status' == arg:
                    action = 3
                elif 'reload' == argv[1]:
                    action = 4
                elif '-uid' == arg:
                    user = argv[i + 1]
                    skip = True
                elif '-gid' == arg:
                    group = argv[i + 1]
                    skip = True
                else:
                    print("Unknown command: %s\n" % arg)
                    print("usage: %s [-uid user] [-gid group] start|stop|restart|status|reload" % argv[0])
                    sys.exit(2)
        else:
            print ("usage: %s [-uid user] [-gid group] start|stop|restart|status|reload" % argv[0])
            sys.exit(2)

        if (user is not None and type(user) is not str) or (group is not None and type(group) is not str):
            raise Exception("The username or group provided are not strings")

        if 0 == action:
            self._daemonStart(user, group)
        elif action == 1:
            self._daemonStop()
        elif action == 2:
            self._daemonRestart(user, group)
        elif action == 3:
            self._daemonStatus()
        elif action == 4:
            self._daemonAskReload()
        else:
            print("The action is not recognised %d" % action)