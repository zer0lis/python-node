# coding=utf-8
import StringIO
import logging
import os
import sys
import time
import ujson
from email import message_from_file
from multiprocessing import Process
from multiprocessing.queues import Empty

import requests
import wand.image
from boto.s3.connection import S3Connection
from datadog import statsd, initialize

from common.socrative_api import exceptionStack, safeToInt, isStringNotEmpty
from common.socrative_config import ConfigRegistry
from workers.counters import WorkerCounters
from workers.task_types import TaskType

logger = logging.getLogger(__name__)


class ImageWorker(Process):
    """
    worker for resizing images
    """

    FORMATS = ["XS", "S", "M", "L", "XL", "R"]

    def __init__(self, redisClient, processQueue, inQueue, daoRegistry):
        """

        :param redisClient:
        :param processQueue:
        :return:
        """

        super(ImageWorker, self).__init__()

        self.redisClient = redisClient
        self.queue = processQueue
        self.inQueue = inQueue
        self.imgSizes = dict()

        self.imgSizes["XS"] = safeToInt(ConfigRegistry().getItem("IMAGE_SIZE_XS"))
        self.imgSizes["S"] = safeToInt(ConfigRegistry().getItem("IMAGE_SIZE_S"))
        self.imgSizes["M"] = safeToInt(ConfigRegistry().getItem("IMAGE_SIZE_M"))
        self.imgSizes["L"] = safeToInt(ConfigRegistry().getItem("IMAGE_SIZE_L"))
        self.imgSizes["XL"] = safeToInt(ConfigRegistry().getItem("IMAGE_SIZE_XL"))
        self.imgSizes["XXL"] = safeToInt(ConfigRegistry().getItem("IMAGE_SIZE_XXL"))
        self.resizeQuality = safeToInt(ConfigRegistry().getItem("IMAGE_RESIZE_QUALITY"))
        self.daoRegistry = daoRegistry

        for k in self.imgSizes:
            if self.imgSizes[k] is None:
                raise ValueError("Missing maximum dimension for size %s" % k)

        self.botoConnection = None
        self.s3Bucket = None

    def run(self):
        """
        overwrote run method
        :return:
        """

        try:
            self.botoConnection = S3Connection(ConfigRegistry().getItem("AWS_ACCESS_KEY_ID"),
                                               ConfigRegistry().getItem("AWS_SECRET_ACCESS_KEY"))
            self.s3Bucket = self.botoConnection.get_bucket(ConfigRegistry().getItem("AWS_STORAGE_BUCKET_NAME"))
        except Exception as e:
            logger.error(exceptionStack(e))

        initialize(api_key=os.environ["DATADOG_API_KEY"], app_key=os.environ["DATADOG_APP_KEY"])

        while True:
            try:
                item = None
                try:
                    item = self.queue.get_nowait()
                except Empty:
                    logger.debug("empty queue, worker didn't received the poison pill")

                if item == "stop":
                    logger.info("ImageWorker:received the poison pill.Exiting")
                    break

                item = self.redisClient.get_task(TaskType.IMAGE_RESIZE)

                if item is None:
                    logger.debug("the processing queue for images is empty")
                    time.sleep(1)
                    continue

                dt = time.time()
                procDict = ujson.loads(item)
                mediaResource = self.daoRegistry.mediaResourceDao.getMediaById(procDict["id"])

                resizeFormats = self.checkImageFormats(mediaResource, procDict["formats"])

                if not resizeFormats:
                    logger.debug("All formats are accounted for.No need of processing")
                    statsd.increment(WorkerCounters.PROCESS_IMAGE_SKIP_COUNTER)
                    continue

                self.resizeImage(mediaResource, resizeFormats)
                duration = time.time() - dt
                statsd.histogram(WorkerCounters.PROCESS_IMAGE_COUNTER, duration)
            except OSError as e:
                statsd.increment(WorkerCounters.PROCESS_IMAGE_FAIL)
                try:
                    logger.warn("preparing for restarting worker", exceptionStack(e))
                    self.inQueue.put("STOP")
                    sys.exit(1)
                except Exception as e:
                    logger.error(exceptionStack(e))
            except Exception as e:
                statsd.increment(WorkerCounters.PROCESS_IMAGE_FAIL)
                logger.error(exceptionStack(e))
                time.sleep(1)
            finally:
                # avoid throttling the cpu
                time.sleep(0.1)

    def checkImageFormats(self, mediaModel, formatList):
        """

        :param mediaModel:
        :param formatList:
        :return:
        """

        if formatList == "*":
            formats = self.FORMATS[:-1]
        elif type(formatList) is list:
            formats = formatList
        else:
            logger.debug("received a wrong format list %s" % repr(formatList))
            return list()

        alreadyDict = dict()
        if mediaModel:
            alreadyDict = ujson.loads(mediaModel.data).get("formats", {}) if isStringNotEmpty(mediaModel.data) else {}

        resp = list()
        for fmt in formats:
            if fmt in self.FORMATS[:-1]:
                if fmt not in alreadyDict:
                    resp.append(fmt)

        return resp

    @statsd.timed(WorkerCounters.PROCESS_IMAGE_RESIZE)
    def resizeImage(self, mediaModel, formatList):
        """

        :param mediaModel:
        :param formatList:
        :return:
        """

        logger.debug("resizeImage: get the original image")
        origImg = StringIO.StringIO()
        try:
            req = requests.get(mediaModel.url)
            contentType = req.headers.get("Content-Type")
            if "image" not in contentType and "multipart" not in contentType:
                origImg.close()
                raise Exception("Invalid image: Content type is :%s . URL is %s" % (contentType, mediaModel.url))

            if "multipart" in contentType:
                tmpIo = StringIO.StringIO(req.content)
                obj = message_from_file(tmpIo)
                found = False
                for part in obj.walk():
                    if part.get_content_maintype() == "image":
                        origImg.write(part.get_payload(decode=True))
                        found = True
                        break

                tmpIo.close()

                if found is False:
                    raise Exception("there is no image in the multipart buffer")
            else:
                origImg.write(req.content)

            metadataDict = ujson.loads(mediaModel.data) if mediaModel.data is not None else dict()

            for fmt in formatList:
                origImg.seek(0, 0)
                maxSize = self.imgSizes[fmt]
                metaDict = self.transcodeAndResize(origImg, mediaModel.url, maxSize, fmt)
                metadataDict.update(metaDict)

            mediaModel.data = ujson.dumps(metadataDict).replace("http:\\/", "https:\\/")
            self.daoRegistry.mediaResourceDao.updateData(mediaModel.id, mediaModel.data)

            logger.info("finished resizing the image resource")
        except Exception as e:
            logger.error(exceptionStack(e) + " URL : %s" % (mediaModel.url if mediaModel else "None"))
            raise
        finally:
            origImg.close()

    def transcodeAndResize(self, origImg, origUrl, maxSize, imgFormat, leaveGif=True):
        """
        resize to maximum dimension origImg
        :param origImg: buffer
        :param origUrl: the original image url - the one uploaded by the user
        :param maxSize:
        :param imgFormat
        :param leaveGif: if it is a GIF image leave it as a GIF after resizing
        :return:
        """


        newOutput = StringIO.StringIO()
        img = None
        try:
            mimetype = None
            img = wand.image.Image(file=origImg)
            if img.height is None or img.width is None:
                raise Exception("one of the dimensions is None: width %s, height %s" % (str(img.width),
                                                                                        str(img.height)))
            #  compute which dimension is greater and keep the aspect ratio
            if img.height >= img.width and img.height > maxSize:
                newHeight = maxSize
                newWidth = maxSize * img.width / img.height
            elif img.width > img.height and img.width > maxSize:
                newWidth = maxSize
                newHeight = maxSize * img.height/img.width
            else:
                return {imgFormat: {"w": img.width, "h": img.height, "url": origUrl}}

            img.resize(width=newWidth, height=newHeight, filter="lanczos")

            if (img.mimetype == "image/gif" and leaveGif is False) or img.mimetype.startswith("image"):
                img.format = "jpg"
                img.compression_quality = self.resizeQuality
                img.save(file=newOutput)
                newOutput.seek(0, 0)
                mimetype = "image/jpeg"

            elif img.mimetype == "image/gif":
                img.compression_quality = self.resizeQuality
                img.save(file=newOutput)
                newOutput.seek(0, 0)
                mimetype = "image/gif"

            name = imgFormat+"_"+origUrl.split('/')[-1]
            name = "socrative/" + name[:name.rindex('.') if '.' in name else None] + ".jpg" if mimetype != "image/gif" else ".gif"

            logger.info("Generating s3 key for upload of file %s" % name)
            if self.s3Bucket is None:
                if self.botoConnection is None:
                    self.botoConnection = S3Connection(ConfigRegistry().getItem("AWS_ACCESS_KEY_ID"),
                                                       ConfigRegistry().getItem("AWS_SECRET_ACCESS_KEY"))
                self.s3Bucket = self.botoConnection.get_bucket(ConfigRegistry().getItem("AWS_STORAGE_BUCKET_NAME"))

            k = self.s3Bucket.new_key(name)
            logger.info("received new key from amazon s3 %s", k)
            k.set_contents_from_string(newOutput.read(), reduced_redundancy=True, headers={"Content-Type": mimetype})
            k.set_canned_acl('public-read')
            logger.info("finished uploading image file to amazon s3")
            url = k.generate_url(0, query_auth=False)

            return {imgFormat: {"w": newWidth, "h": newHeight, "url": url}}
        except Exception as e:
            logger.error(exceptionStack(e))
            raise
        finally:
            newOutput.close()
            if img:
                img.close()