# coding=utf-8
from socrative.settings import PLATFORM


class TaskType(object):
    """
    enum class
    """

    IMAGE_RESIZE = "image" + PLATFORM
    GENERATE_REPORTS = "reports" + PLATFORM
    GENERATE_BIG_REPORTS = "bigReports" + PLATFORM
    GENERATE_PRO_REPORTS = "proReports" + PLATFORM
    REPORT_ACTIONS = "actions" + PLATFORM
    SEND_EMAIL = "email" + PLATFORM


class EmailTypes(object):
    """

    """

    REGISTRATION = "registration"
    REPORTS = "reports"
    QUIZ_SHARE = "share_quiz"
    FORGOT_PASSWORD = "forgot_password"
    UPGRADE_PRO = "upgrade_pro"
    RECEIPTS = "receipt"
    MULTI_SEATS_RECEIPT = 'multi_seats_receipt'
    ACCOUNTING_REPORT = "accounting"
    PRO_EXPIRING = "pro_expiring"
    LOGINS = "logins"


class ActionTypes(object):
    """
    enum class
    """
    UPLOAD_TO_GOOGLE_DRIVE = "gd"
    SEND_VIA_EMAIL = "em"
    DOWNLOAD = "dl"