# coding=utf-8

import errno
import io
import logging
import os
import re
import sys
import time
import ujson
from cStringIO import StringIO
from datetime import timedelta
from multiprocessing import Process
from multiprocessing.queues import Empty
from zipfile import ZipFile

import xlsxwriter
from boto.s3.connection import S3Connection
from datadog import statsd, initialize
from django.utils.translation import ugettext, trans_real
from lxml import html as lxmlParser
from lxml.etree import ParserError
from lxml.html.clean import Cleaner

from common.base_dao import BaseDao
from common.dao import ActivityInstanceModel
from common.socrative_api import exceptionStack, remove_control_chars, htmlEscape, convert, safeToBool
from common.socrative_config import ConfigRegistry
from quizzes.pdf_generator import StudentPdfReport, WholeClassPdfReport
from workers.counters import WorkerCounters
from workers.task_types import TaskType
from common.soc_constants import MAX_REPORT_PROCESSING_LIMIT

logger = logging.getLogger(__name__)

MIN_MEMORY_FOR_BIG_REPORTS = 10 * (1024 **3)

class SkipReport(Exception):
    """
    Exception to be handled to skip trying creating the report
    """
    def __init__(self, *args, **kwargs):
        super(SkipReport, self).__init__(*args, **kwargs)


class LocalContext(object):
    """
    class to replace the request object for workers. It behaves like a container for different objects
    """

    def __init__(self):
        """
        Constructor
        """

        self.activity = None
        self.responses = None
        self.names = None
        self.nameDict = None
        self.questions = None
        self.room_name = None
        self.textAnswerDict = None
        self.textResponseDict = None
        self.answerSelectionDict = None
        self.qCADict = None
        self.user = None


class ReportWorker(Process):
    """
    worker for resizing images
    """
    # x- xlsx, q-question pdf, s - students zip
    FORMATS = ["X", "Q", "S"]

    WORD_PATTERN = re.compile(r'[\W_]+', re.UNICODE)
    IS_ALL_DIGITS = re.compile(r'^.[0-9]*$')
    ESCAPED_HTML = re.compile(r'.*(&amp;|&gt;|&lt;|&emsp;|&quot;|&#x27;).*')

    SPACE_RACE_TEAMS = {
        0: convert(ugettext("Blue")),
        1: convert(ugettext("Magenta")),
        2: convert(ugettext("Lime")),
        3: convert(ugettext("Peach")),
        4: convert(ugettext("Violet")),
        5: convert(ugettext("Teal")),
        6: convert(ugettext("Indigo")),
        7: convert(ugettext("Orange")),
        8: convert(ugettext("Red")),
        9: convert(ugettext("Silver")),
        10: convert(ugettext("Green")),
        11: convert(ugettext("Rose")),
        12: convert(ugettext("Gold")),
        13: convert(ugettext("Maroon")),
        14: convert(ugettext("Yellow")),
        15: convert(ugettext("Tan")),
        16: convert(ugettext("Hot Pink")),
        17: convert(ugettext("Sienna")),
        18: convert(ugettext("Dimgray")),
        19: convert(ugettext("Crimson"))
    }

    def __init__(self, redisClient, processQueue, inQueue, daoRegistry):
        """
        :param redisClient:
        :param processQueue:
        :param inQueue:
        :param daoRegistry:
        :return:
        """

        super(ReportWorker, self).__init__()

        self.redisClient = redisClient
        self.queue = processQueue
        self.inQueue = inQueue

        self.botoConnection = None
        self.s3Bucket = None
        self.pdfkit = None
        self.pdfkitConfig = None
        self.daoRegistry = daoRegistry
        self.reportQueue = None

    @staticmethod
    def removeBrTag(text):
        """
        removes <br> tags and replaces them with \n
        :param text:
        :returns str
        """

        if text.strip() != "":
            s = text.replace("<br data-mce-bogus=\"1\">", "<br>").replace("<br></p>", "<br>")
            s = s.replace("&nbsp;", " ").replace("<strong>", "<b>").replace("</strong>", "</b>").replace("<em>", "<i>")
            s = s.replace("</em>", "</i>").replace("<p>", "").replace("</p>", "<br>").replace("\n", "<br>")
        else:
            s = text

        if s.strip() != "":
            cleaner = Cleaner(safe_attrs_only=True, safe_attrs=('src', 'href'), remove_tags=('p', 'span', 'img',
                                                                                             'html'))
            try:
                s = cleaner.clean_html(s)
                s = s.replace("<br>", "<br></br>").replace("</br></br>", "</br>")
            except Exception as e:
                logger.debug(exceptionStack(e))

        return s

    def run(self):
        """
        overwrote run method
        :return:
        """
        total_mem = os.sysconf('SC_PAGE_SIZE') * os.sysconf('SC_PHYS_PAGES')
        self.reportQueue = TaskType.GENERATE_BIG_REPORTS if total_mem > MIN_MEMORY_FOR_BIG_REPORTS else TaskType.GENERATE_REPORTS

        self.botoConnection = S3Connection(ConfigRegistry().getItem("AWS_ACCESS_KEY_ID"),
                                           ConfigRegistry().getItem("AWS_SECRET_ACCESS_KEY"))
        self.s3Bucket = self.botoConnection.get_bucket(ConfigRegistry().getItem("AWS_STORAGE_BUCKET_NAME"))

        initialize(api_key=os.environ["DATADOG_API_KEY"], app_key=os.environ["DATADOG_APP_KEY"])

        while True:
            item = None
            try:
                try:
                    item = self.queue.get_nowait()
                except Empty:
                    logger.debug("empty queue, worker didn't received the poison pill")

                if item == "stop":
                    logger.info("ReportWorker:received the poison pill.Exiting")
                    break

                # treat the pro reports first
                item = self.redisClient.get_task_from_set(TaskType.GENERATE_PRO_REPORTS)

                if item is None:
                    item = self.redisClient.get_task_from_set(self.reportQueue)

                if item is None:
                    time.sleep(1)
                    continue

                resDict = ujson.loads(item)
                self.createReports(resDict)
            except MemoryError as e:
                if item:
                    # add it back to the big reports queue because it needs more RAM
                    self.redisClient.add_tasks_to_set(TaskType.GENERATE_BIG_REPORTS, [item])
                logger.warn("worker out of memory, %s", repr(e))
            except IOError as e:
                logger.error(exceptionStack(e))
                if e.errno == errno.ENOMEM:
                    if item:
                        # add it back to the big reports queue because it needs more RAM
                        self.redisClient.add_tasks_to_set(TaskType.GENERATE_BIG_REPORTS, [item])
                    logger.warn("Item %s caused an out of memory exception" % str(item))
                else:
                    time.sleep(1)
            except OSError as e:
                logger.error(exceptionStack(e))
                try:
                    logger.warn("preparing for restarting worker")
                    self.inQueue.put("STOP")
                    sys.exit(1)
                except Exception as e:
                    logger.error(exceptionStack(e))
            except Exception as e:
                logger.error(exceptionStack(e))
                time.sleep(1)

            finally:
                # avoid throttling the cpu
                time.sleep(0.1)

    def createReports(self, dataDict):
        """
        generate all type of reports
        :param dataDict:
        :return:
        """
        activity = None
        try:
            dt = time.time()
            activity = self.daoRegistry.activityInstanceDao.getActivityById(dataDict["id"])

            if activity is None:
                statsd.increment(WorkerCounters.CREATE_REPORTS_FAIL_COUNTER)
                logger.error("Activity for id: %s is None", str(dataDict["id"]))
                raise Exception("activity is None")
            if activity.report_status != ActivityInstanceModel.REPORT_NOT_DONE:
                logger.info("report for activity id %d in progress or done. status : %d", activity.id,
                            activity.report_status)
                statsd.increment(WorkerCounters.CREATE_REPORTS_IN_PROGRESS)
                return

            quiz = self.daoRegistry.quizDao.loadQuizById(activity.activity_id, _type=BaseDao.TO_MODEL)
            responses = self.daoRegistry.studentResponseDao.getAllResponses(activity.id,
                                                                            order_by=BaseDao.USER_UUID,
                                                                            sortOrder=BaseDao.ASCENDING,
                                                                            hidden=False)
            questions = self.daoRegistry.questionDao.getQuestions(quiz.id, sortOrder=BaseDao.ASCENDING,
                                                                  orderBy=BaseDao.ORDER)
            names = self.daoRegistry.studentNameDao.getStudentNamesForActivityId(None, activity.id,
                                                                                 _type=BaseDao.TO_MODEL)

            imgCount = self.daoRegistry.questionDao.countResources([question.question_id for question in questions])

            nameDict = dict((s.user_uuid, s.name) for s in names)

            # if there are no images , the report size will be manageable
            if imgCount > 0 and len(names) * imgCount > MAX_REPORT_PROCESSING_LIMIT:
                self.redisClient.add_tasks_to_set(TaskType.GENERATE_BIG_REPORTS, [dataDict])
                return

            self.daoRegistry.activityInstanceDao.updateReportStatus(activity.id,
                                                                    ActivityInstanceModel.REPORT_IN_PROGRESS)

            user = self.daoRegistry.usersDao.loadUserById(dataDict["user_id"])

            if user.tz_offset is None:
                local_time = activity.start_time
            else:
                local_time = activity.start_time - timedelta(minutes=user.tz_offset)

            context = LocalContext()
            context.activity = activity
            context.responses = responses
            context.names = names
            context.nameDict = nameDict
            context.questions = questions
            context.room_name = None
            context.textAnswerDict = None
            context.textResponseDict = None
            context.answerSelectionDict = None
            context.qCADict = None
            context.user = user

            trans_real.activate(user.language)

            xlsxOutput = self.whole_class_excel_report(context, quiz, responses, questions, local_time, activity)

            qClass = WholeClassPdfReport(self.daoRegistry, responses, questions, quiz, names, local_time, activity,
                                         context)
            questionPdfOutput = qClass.getQuestionPdf()

            qStudents = StudentPdfReport(self.daoRegistry, responses, questions, quiz, names, local_time, activity,
                                         context)

            studentReports = []
            if responses:
                studentReports = qStudents.getStudentsPdfs()

            quiz_name = self.WORD_PATTERN.sub('', quiz.name).lower()
            xlsxFile = FilenameHelper("xlsx")
            xlsxFile.from_report_settings("xlsx", 1, fileParams=[quiz_name, activity.activity_type, local_time])

            questionPdfFile = FilenameHelper("pdf")
            questionPdfFile.from_report_settings("pdf", 1, fileParams=[quiz_name, activity.activity_type, local_time])

            studentPdfFileDict = dict()

            for student in studentReports:
                phile = FilenameHelper("pdf", extra=student[1])
                phile.changePDFFilename(extra=student[1], fileParams=[quiz_name, activity.activity_type, local_time])
                studentPdfFileDict[phile] = studentReports[student]

            self.uploadToS3((xlsxFile, xlsxOutput), (questionPdfFile, questionPdfOutput), studentPdfFileDict,
                            fileParams=[quiz_name, activity.activity_type, local_time], activity_id=activity.id)

            xlsxOutput.close()
            questionPdfOutput.close()

            for s in studentPdfFileDict.values():
                s.close()

            self.daoRegistry.activityInstanceDao.updateReportStatus(activity.id,
                                                                    ActivityInstanceModel.REPORT_DONE)

            duration = time.time() - dt
            statsd.histogram(WorkerCounters.CREATE_REPORTS_COUNTER, duration)
        except SkipReport:
            # mark this as done, but this type of report shouldn't be made is 1Q MC or TF
            # type which doesn't have a report
            if activity:
                self.daoRegistry.activityInstanceDao.updateReportStatus(activity.id,
                                                                        ActivityInstanceModel.REPORT_IGNORE)
                statsd.increment(WorkerCounters.CREATE_REPORTS_SKIP_COUNTER)
        except Exception as e:
            statsd.increment(WorkerCounters.CREATE_REPORTS_FAIL_COUNTER)
            logger.error(exceptionStack(e))
            if activity:
                self.daoRegistry.activityInstanceDao.updateReportStatus(activity.id,
                                                                        ActivityInstanceModel.REPORT_NOT_DONE)
                logger.error("Exception occured for activity instance :%d", activity.id)
            raise
        finally:
            trans_real.deactivate()

    @classmethod
    def writeToArchive(cls, archive, fileHelper):
        """
        :param archive:
        :param fileHelper:
        :return:
        """
        fileHelper[1].seek(0, 0)
        archive.writestr(fileHelper[0].prettier, fileHelper[1].read())

    def uploadSimpleFileToS3(self, fileHelper, actitvity_id):
        """

        :param fileHelper:
        :param actitvity_id:
        :return:
        """

        try:
            if self.s3Bucket is None:
                if self.botoConnection is None:
                    self.botoConnection = S3Connection(ConfigRegistry().getItem("AWS_ACCESS_KEY_ID"),
                                                       ConfigRegistry().getItem("AWS_SECRET_ACCESS_KEY"))
                self.s3Bucket = self.botoConnection.get_bucket(ConfigRegistry().getItem("AWS_STORAGE_BUCKET_NAME"))
            # upload the xlsx
            logger.info("uploading the file %s", fileHelper[0].prettier)
            k = self.s3Bucket.new_key("reports/%d/" % actitvity_id + fileHelper[0].prettier)
            logger.info("received new key from amazon s3 %s", k)
            fileHelper[1].seek(0, 0)
            logger.info("reports/"+fileHelper[0].prettier)

            k.set_contents_from_string(fileHelper[1].read(), reduced_redundancy=True,
                                       headers={"Content-Type": fileHelper[0].mimetype()})
            logger.info("finished uploading the file %s", fileHelper[0].prettier)

        except Exception as e:
            logger.error(exceptionStack(e))
            raise

    def uploadArchiveToS3(self, name, data, activity_id):
        """

        :param name:
        :param data:
        :param activity_id:
        :return:
        """

        try:
            if self.s3Bucket is None:
                if self.botoConnection is None:
                    self.botoConnection = S3Connection(ConfigRegistry().getItem("AWS_ACCESS_KEY_ID"),
                                                       ConfigRegistry().getItem("AWS_SECRET_ACCESS_KEY"))
                self.s3Bucket = self.botoConnection.get_bucket(ConfigRegistry().getItem("AWS_STORAGE_BUCKET_NAME"))

            # upload the xlsx
            logger.info("uploading the file %s", name)
            k = self.s3Bucket.new_key("reports/%d/" % activity_id + name)
            logger.info("received new key from amazon s3 %s", k)

            k.set_contents_from_string(data, reduced_redundancy=True,
                                       headers={"Content-Type": "application/zip"})
            logger.info("finished uploading the file %s", name)

        except Exception as e:
            logger.error(exceptionStack(e))
            raise

    def createArchive(self, fileHelperList):
        """

        :param fileHelperList:
        :return:
        """
        mem = io.BytesIO()
        archive = ZipFile(mem, "w")
        for v in fileHelperList:
            self.writeToArchive(archive, v)
        archive.close()

        mem.seek(0, 0)
        return mem

    @statsd.timed(WorkerCounters.UPLOAD_REPORT_TO_S3)
    def uploadToS3(self, xlsxFile, questionPdfFile, studentDictFiles, fileParams, activity_id):
        """

        :param xlsxFile:
        :param questionPdfFile:
        :param studentDictFiles:
        :param fileParams:
        :param activity_id:
        :return:
        """
        try:
            self.uploadSimpleFileToS3(xlsxFile, activity_id)
            self.uploadSimpleFileToS3(questionPdfFile, activity_id)

            if studentDictFiles:
                # upload students
                f = FilenameHelper("zip")
                f.from_report_settings("zip", 2, fileParams=fileParams, atend="A")
                mem = self.createArchive([(k, studentDictFiles[k]) for k in studentDictFiles])
                self.uploadArchiveToS3(f.prettier, mem.read(), activity_id)
                mem.close()

            # upload students + xlsx
            f = FilenameHelper("zip")
            f.from_report_settings("zip", 2, fileParams=fileParams, atend="C")
            l = []
            if studentDictFiles:
                l = [(k, studentDictFiles[k]) for k in studentDictFiles]
            l.append(xlsxFile)
            mem = self.createArchive(l)
            self.uploadArchiveToS3(f.prettier, mem.read(), activity_id)
            mem.close()

            # upload students + question pdf
            f = FilenameHelper("zip")
            f.from_report_settings("zip", 2, fileParams=fileParams, atend="D")
            l = []
            if studentDictFiles:
                l = [(k, studentDictFiles[k]) for k in studentDictFiles]
            l.append(questionPdfFile)
            mem = self.createArchive(l)
            self.uploadArchiveToS3(f.prettier, mem.read(), activity_id)
            mem.close()

            # upload xls + question pdf
            f = FilenameHelper("zip")
            f.from_report_settings("zip", 2, fileParams=fileParams, atend="B")
            l = list()
            l.append(questionPdfFile)
            l.append(xlsxFile)
            mem = self.createArchive(l)
            self.uploadArchiveToS3(f.prettier, mem.read(), activity_id)
            mem.close()

            # upload xls + question pdf + students
            f = FilenameHelper("zip")
            f.from_report_settings("zip", 2, fileParams=fileParams, atend="E")
            l = []
            if studentDictFiles:
                l = [(k, studentDictFiles[k]) for k in studentDictFiles]
            l.append(questionPdfFile)
            l.append(xlsxFile)

            mem = self.createArchive(l)
            self.uploadArchiveToS3(f.prettier, mem.read(), activity_id)
            mem.close()
        except Exception as e:
            logger.error(exceptionStack(e))
            raise

    def free_response_student_details(self, **kwargs):
        """

        :return: 
        """
        presence_column = kwargs.get("presence_column")
        presence = kwargs.get("presence")
        current_row = kwargs.get("current_row")
        student_name = kwargs.get("student_name")
        worksheet = kwargs.get("worksheet")
        student_name_column = kwargs.get("student_name_column")
        students_dict = kwargs.get("students_dict")
        student_id = kwargs.get("student_id")
        student_id_column = kwargs.get("student_id_column")
        style = kwargs.get("style")
        name_id_dict = kwargs.get("name_id_dict")

        if presence_column is not None and presence is not None:
            worksheet.write(current_row, presence_column, (ugettext("Yes") if presence else ugettext("No")))

        if student_name is not None:
            sName = student_name.strip()
            worksheet.write(current_row, student_name_column,
                            (lxmlParser.fromstring(sName).text_content() if sName != "" else ""))

        if students_dict:
            if student_id == '-':
                student_ids = name_id_dict.get(student_name, [])
                if len(student_ids) != 1:
                    student_id = '-'
                else:
                    student_id = student_ids[0]
            worksheet.write(current_row, student_id_column,
                            (lxmlParser.fromstring(student_id).text_content() if student_id else ""),
                            style)

    @statsd.timed(WorkerCounters.FREE_RESPONSE_REPORT)
    def free_response_report_file(self, output, quiz, activity_instance, request):
        """
        report for free response activities 1Q FR
        :param output:
        :param quiz:
        :param activity_instance:
        :param request:
        :return:
        """

        if request.questions is None:
            questions = self.daoRegistry.questionDao.getQuestions(quiz.id)
        else:
            questions = request.questions

        if activity_instance.activity_type not in (ActivityInstanceModel.SINGLE_QUESTION,
                                                   ActivityInstanceModel.SINGLE_QUESTION_VOTING):
            logger.info("this is not a 1Q activity for a FR response question")
            raise Exception("Bad type of activity for this kind of excel")

        if activity_instance.activity_type == ActivityInstanceModel.SINGLE_QUESTION:
            # check to see if there is a voting activity after this (activity_type = 1V)
            sec_activity = self.daoRegistry.activityInstanceDao.checkVoteActivity(activity_instance)
            fr_activity = activity_instance
            vote_activity = self.daoRegistry.activityInstanceDao.getActivityById(sec_activity) if sec_activity is not None else None
        elif activity_instance.activity_type == ActivityInstanceModel.SINGLE_QUESTION_VOTING:
            # find the activity with the fr responses
            sec_activity = self.daoRegistry.activityInstanceDao.checkVoteActivity(activity_instance)
            vote_activity = activity_instance
            fr_activity = self.daoRegistry.activityInstanceDao.getActivityById(sec_activity) if sec_activity is not None else None
        else:
            raise SkipReport

        if fr_activity is None:
            raise Exception("There is no 1q fr activity for this 1q voting activity")

        if request.responses is None or questions[0].type == "MC":
            responses = self.daoRegistry.studentResponseDao.getAllResponses(fr_activity.id,
                                                                            order_by=BaseDao.USER_UUID,
                                                                            sortOrder=BaseDao.ASCENDING,
                                                                            hidden=False)
        else:
            responses = request.responses

        if request.names is None or questions[0].type == "MC":
            names = self.daoRegistry.studentNameDao.getAllNamesByActivity(fr_activity.id)
        else:
            names = dict((sn.user_uuid, sn.name) for sn in request.names)

        if request.questions is None or questions[0].type == "MC":
            questions = self.daoRegistry.questionDao.getQuestions(fr_activity.activity_id)
        else:
            questions = request.questions

        require_names = False
        name_setting = safeToBool(self.daoRegistry.activitySettingsDao.getSetting(fr_activity.id, "require_names"))

        if name_setting is not None:
            require_names = name_setting

        votes = None
        if vote_activity is not None:
            # votes is a dictionary with answer_text, number of votes
            votes = self.daoRegistry.studentResponseDao.getVotes(vote_activity.id, vote_activity.activity_id)

        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet()

        basic_formatting = workbook.add_format({'font_name': 'Arial', 'font_size': 16})
        bold_left = workbook.add_format({'bold': True, 'font_name': 'Arial', 'font_size': 16})
        plain_centered = workbook.add_format({'align': 'center', 'font_name': 'Arial', 'font_size': 16})
        blue_header = workbook.add_format({'bg_color': '#89BFFB', 'text_wrap': True, 'align': 'center',
                                           'font_name': 'Arial', 'font_size': 16})
        null_answer = workbook.add_format({'font_name': 'Arial', 'font_size': 16, 'text_wrap': True, 'align': 'left'})

        worksheet.set_column(0, 0, 30, basic_formatting)

        if request.user.tz_offset is None:
            local_time = fr_activity.start_time
        else:
            local_time = fr_activity.start_time - timedelta(minutes=request.user.tz_offset)

        qDict = dict((q.question_id, q) for q in questions)
        qText = questions[0].question_text.strip()
        activity_name = (lxmlParser.fromstring(qText).text_content() if qText != "" else "") if len(questions) > 0 and\
                                                                            len(questions[0].question_text) > 0 else ""

        if self.IS_ALL_DIGITS.match(activity_name):
            activity_name = """=concatenate("%s","")""" % activity_name
        if activity_name == "":
            activity_name = "Short Answer Activity"

        if request.room_name is None:
            room_name = fr_activity.room_name
            request.room_name = room_name
        else:
            room_name = request.room_name

        responseIds = [r.id for r in responses]
        if request.textAnswerDict is None:
            textAnswerDict = self.daoRegistry.studentResponseTextAnswerDao.getTexts(responseIds)
            request.textAnswerDict = textAnswerDict
        else:
            textAnswerDict = request.textAnswerDict

        if activity_name == "":
            activity_name = convert(ugettext("Short Answer Activity"))

        worksheet.write(0, 0, activity_name, bold_left)
        worksheet.write(1, 0, local_time.strftime("%A, %B %d %Y %I:%M %p"))
        room_name = convert(ugettext("Room:")).encode('utf-8') + " " + (room_name.encode('utf-8') if type(room_name) is unicode else room_name)
        room_name = room_name + " (" + (request.user.email.encode('utf-8') if type(request.user.email) is unicode else request.user.email) + ")"
        room_name = room_name.decode('utf-8')
        worksheet.write(2, 0, room_name)

        current_row = 4
        response_col = 0

        studentIDDict = dict()
        studentsDict = dict()
        studentList = None

        student_name_column = None
        student_id_column = None
        presence_column = None
        votes_column = None

        if require_names:

            room = self.daoRegistry.roomDao.getRoomByName(activity_instance.room_name, activity_instance.started_by_id)
            if not room:
                room_id, room_status = self.daoRegistry.roomHistoryDao.getRoomId(activity_instance.started_by_id,
                                                                                 room_name)
            else:
                room_id = room.id

            if room_id:
                studentsList = self.daoRegistry.studentActivityDao.getPresence(activity_instance.id)
                studentsDict = dict()
                user_uuid = 0
                for student in studentsList:
                    if student.get("user_uuid"):
                        studentsDict[student.get("user_uuid")] = [student.get("name"),
                                                                  student.get("student_id"),
                                                                  student.get("present") or False
                                                                  ]
                    else:
                        studentsDict[user_uuid] = [student.get("name"),
                                                   student.get("student_id"),
                                                   student.get("present") or False
                                                   ]
                        user_uuid += 1
            else:
                studentsDict = dict()

            if studentsDict:
                worksheet.write(current_row, response_col, convert(ugettext('Presence')), blue_header)
                presence_column = response_col
                response_col += 1

            worksheet.write(current_row, response_col, convert(ugettext('Student Name')), blue_header)
            student_name_column = response_col
            response_col += 1

            worksheet.write(current_row, response_col, convert(ugettext('Student ID')), blue_header)
            studentIDDict = self.daoRegistry.studentDao.getStudentIDs(activity_instance.id)
            student_id_column = response_col
            response_col += 1

        if votes is not None:
            worksheet.write(current_row, response_col, convert(ugettext('Votes')), blue_header)
            votes_column = response_col
            response_col += 1

        worksheet.set_column(response_col, response_col, 60, basic_formatting)

        worksheet.write(current_row, response_col, convert(ugettext('Student Answer')), blue_header)

        nameIdDict = dict()

        responseDict = dict()
        for response in responses:
            if response.user_uuid in responseDict:
                responseDict[response.user_uuid].append(response)
            else:
                responseDict[response.user_uuid] = [response]

        if not studentsDict:
            for user_uuid in names:
                studentsDict[user_uuid] = [names[user_uuid], studentIDDict.get(user_uuid), True]
        else:
            for user_uuid in names:
                if user_uuid in studentsDict:
                    studentsDict[user_uuid][2] = True
                else:
                    studentsDict[user_uuid] = [names[user_uuid], '-', True]

                if user_uuid in studentIDDict:
                    studentsDict[user_uuid][1] = studentIDDict[user_uuid]

            remove_keys = list()
            for user_uuid in studentsDict:
                if user_uuid not in studentIDDict:
                    for uid, student_id in studentIDDict.items():
                        if studentsDict[user_uuid][1] == student_id:
                            remove_keys.append((user_uuid, uid))
                            break

            # remove newer user_uuid that were not in the activity
            for k, new_k in remove_keys:
                if k in responseDict:
                    studentsDict[new_k] = studentsDict[k]
                else:
                    studentsDict[new_k] = studentsDict.pop(k)

        orderedNaming = sorted(studentsDict.items(), key=lambda x: x[1][0])

        current_row += 1
        for user_uuid, student_details in orderedNaming:
            student_name = student_details[0]
            student_id = student_details[1]
            presence = student_details[2]

            row_used = False
            start_row = current_row
            if require_names:
                self.free_response_student_details(style=plain_centered,
                                                   student_name=student_name,
                                                   student_id=student_id,
                                                   presence=presence,
                                                   student_name_column=student_name_column,
                                                   student_id_column=student_id_column,
                                                   presence_column=presence_column,
                                                   current_row=current_row,
                                                   students_dict=studentsDict,
                                                   name_id_dict=nameIdDict,
                                                   worksheet=worksheet)
                row_used = True

            for response in responseDict.get(user_uuid, []):
                question = qDict.get(response.question_id)
                if question and question.type == "FR":
                    if require_names and start_row != current_row:
                        self.free_response_student_details(style=plain_centered,
                                                           student_name=student_name,
                                                           student_id=student_id,
                                                           presence=presence,
                                                           student_name_column=student_name_column,
                                                           student_id_column=student_id_column,
                                                           presence_column=presence_column,
                                                           current_row=current_row,
                                                           students_dict=studentsDict,
                                                           name_id_dict=nameIdDict,
                                                           worksheet=worksheet)
                    text_answers = []
                    for text_answer in textAnswerDict.get(response.id, []):
                        text_answers.append(text_answer)
                    display_text = ", ".join(text_answers)
                    if self.ESCAPED_HTML.match(display_text):
                        display_text = lxmlParser.fromstring(
                            display_text).text_content() if display_text.strip() != "" else ""
                    worksheet.write(current_row, response_col, display_text, null_answer)
                    if votes:
                        value = str(votes.get(htmlEscape(text_answers[0]), 0) or votes.get(text_answers[0], 0))
                        worksheet.write(current_row, votes_column, value)
                    current_row += 1

            if row_used is True and start_row == current_row:
                current_row += 1

        workbook.close()

    @statsd.timed(WorkerCounters.WHOLE_CLASS_REPORT)
    def whole_class_excel_report(self, request, quiz, responses, questions, local_time, activity):
        """

        :param request:
        :param quiz:
        :param responses:
        :param questions:
        :param activity:
        :param local_time:
        :return:
        """

        activity_instance = activity

        if activity and activity.activity_type in (ActivityInstanceModel.SINGLE_QUESTION,
                                                   ActivityInstanceModel.SINGLE_QUESTION_VOTING):
            output = StringIO()
            self.free_response_report_file(output, quiz, activity_instance, request)
            output.seek(0)
            return output
        elif activity and activity.activity_type == ActivityInstanceModel.SINGLE_QUESTION_NO_REPORT:
            raise SkipReport

        # Variables used in tallying responses
        order_mapping = {}
        correct_list = []
        current_row = 6
        total_correct = 0
        total_gradable = 0
        student_count = 0
        question_count = len(questions)

        output = StringIO()
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet()

        # Formatting options
        basic_formatting = workbook.add_format({'font_name': 'Arial', 'font_size': 16})
        bold_left = workbook.add_format({'bold': True, 'font_name': 'Arial', 'font_size': 16})
        plain_centered = workbook.add_format({'align': 'center', 'font_name': 'Arial', 'font_size': 16})
        blue_header = workbook.add_format({'bg_color': '#89BFFB','text_wrap': True, 'align': 'center', 'font_name': 'Arial', 'font_size': 16})
        grey_left = workbook.add_format({'bg_color': '#B1B1B1','border': 2, 'font_name': 'Arial', 'font_size': 16})
        grey_percentage = workbook.add_format({'bg_color': '#B1B1B1','border': 2, 'align': 'center', 'font_name': 'Arial', 'font_size': 16, 'num_format': '0.0%'})
        grey_decimal = workbook.add_format({'bg_color': '#B1B1B1','border': 2, 'align': 'center', 'font_name': 'Arial', 'font_size': 16, 'num_format': '0.00'})
        right_answer = workbook.add_format({'bg_color': '#C0FAC1', 'font_name': 'Arial', 'font_size': 16, 'text_wrap': True})
        wrong_answer = workbook.add_format({'bg_color': '#F3BFC0', 'font_name': 'Arial', 'font_size': 16, 'text_wrap': True})
        null_answer = workbook.add_format({'font_name': 'Arial', 'font_size': 16, 'text_wrap': True})

        # COLORS FOR SPACE RACE
        teamCellFormat = {
            0: workbook.add_format({'bg_color': '#276baa', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            1: workbook.add_format({'bg_color': '#ca2692', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            2: workbook.add_format({'bg_color': '#a2d35d', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            3: workbook.add_format({'bg_color': '#fcbc89', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            4: workbook.add_format({'bg_color': '#7d3990', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            5: workbook.add_format({'bg_color': '#258a8c', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            6: workbook.add_format({'bg_color': '#5f82c2', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            7: workbook.add_format({'bg_color': '#d76d2c', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            8: workbook.add_format({'bg_color': '#b83b24', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            9: workbook.add_format({'bg_color': '#5c646e', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            10: workbook.add_format({'bg_color': '#4fa45e', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            11: workbook.add_format({'bg_color': '#ec7c90', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            12: workbook.add_format({'bg_color': '#ba8324', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            13: workbook.add_format({'bg_color': '#9d3444', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            14: workbook.add_format({'bg_color': '#fbdb45', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            15: workbook.add_format({'bg_color': '#D2B48C', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            16: workbook.add_format({'bg_color': '#e602e7', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            17: workbook.add_format({'bg_color': '#e18942', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            18: workbook.add_format({'bg_color': '#696969', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            19: workbook.add_format({'bg_color': '#990000', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16})
        }
        # END COLORS FOR SPACE RACE

        # Set column widths and default formatting
        worksheet.set_column(0, 2 + question_count, 30, basic_formatting)
        worksheet.set_column(1, 2, 15)
        # Write main header
        quiz.name = lxmlParser.fromstring(quiz.name).text_content() if quiz.name.strip() != "" else ""
        if self.IS_ALL_DIGITS.match(quiz.name[:30]):
            worksheet.write(0, 0, """=concatenate("%s","")""" % quiz.name[:30], bold_left)
        else:
            worksheet.write(0, 0, quiz.name[:30], bold_left)
        worksheet.write(1, 0, local_time.strftime("%A, %B %d %Y %I:%M %p"))

        if request.room_name is None:
            roomName = activity_instance.room_name
            request.room_name = roomName
        else:
            roomName = request.room_name

        roomName = roomName.decode('utf-8') if type(roomName) is not unicode else roomName

        worksheet.write(2, 0, convert(ugettext("Room:"))+ u" " + roomName)

        standardName = self.daoRegistry.standardsDao.getStandardName(quiz.id)
        if standardName:
            # Write common core tags
            worksheet.write(4, 0, convert(ugettext("Standard:")), bold_left)
            worksheet.write(4, 1, standardName, basic_formatting)

        # Write column headings
        require_names = safeToBool(self.daoRegistry.activitySettingsDao.getSetting(activity_instance.id,
                                                                                   "require_names")
                                   )

        col = 0

        studentIDDict = dict()
        studentsDict = dict()
        presenceCol = None

        if require_names:

            studentsList = self.daoRegistry.studentActivityDao.getPresence(activity_instance.id)
            studentsDict = dict()
            user_uuid = 0
            for student in studentsList:
                if student.get("user_uuid"):
                    studentsDict[student.get("user_uuid")] = [student.get("name"),
                                                              student.get("student_id"),
                                                              student.get("present") or False
                                                              ]
                else:
                    studentsDict[user_uuid] = [student.get("name"),
                                               student.get("student_id"),
                                               student.get("present") or False
                                               ]
                    user_uuid += 1

            if studentsDict:
                worksheet.write(current_row, col, convert(ugettext('Presence')), blue_header)
                col += 1
                presenceCol = 0
            else:
                presenceCol = None

        # Write column headings

        isSR = False
        teamCol = 0
        totalScoreCol = 0
        nbCorrectCol = 0

        if presenceCol is not None:
            worksheet.write(current_row - 1, 0, convert(ugettext("Column calculations only reflect present students")))
        if activity_instance.activity_type == activity_instance.SPACE_RACE:
            worksheet.write(current_row, col, convert(ugettext("Team")), blue_header)
            teamCol = col
            col += 1
            isSR = True

        worksheet.write(current_row, col, convert(ugettext("Student Names")), blue_header)
        nameCol = col
        col += 1

        worksheet.write(current_row, col, convert(ugettext('Student ID')), blue_header)
        studentIDDict = self.daoRegistry.studentDao.getStudentIDs(activity_instance.id)
        studentIdColumn = col
        col += 1

        worksheet.write(current_row, col, convert(ugettext("Total Score (0 - 100)")), blue_header)
        totalScoreCol = col
        col += 1
        worksheet.write(current_row, col, convert(ugettext("Number of correct answers")), blue_header)
        nbCorrectCol = col
        q_n = col + 1
        questionCol = q_n

        # Write question headers and record number of gradable responses
        # qCADict - question:number of correct answers dictionary
        qids = [q.question_id for q in questions]
        if request.qCADict is None:
            qCADict = self.daoRegistry.answerDao.getNbCorrectAnswersForQuestions(qids)
            request.qCADict = qCADict
        else:
            qCADict = request.qCADict

        answerCountDict = self.daoRegistry.answerDao.getAnswerCount(qids)
        for question in questions:
            # qText = self.removeBrTag(question.question_text)
            try:
                qText = question.question_text
                text = lxmlParser.fromstring(qText).text_content() if qText.strip() != "" else ""
            except ParserError:
                text = question.question_text
            except ValueError:
                text = question.question_text

            worksheet.write(current_row, q_n, text, blue_header)
            order_mapping[question.question_id] = q_n
            correct_list.append(0)
            q_n += 1

            # Count number of gradable answers in
            if qCADict.get(question.question_id, 0) > 0:
                total_gradable += 1
            elif question.type == "FR" and answerCountDict.get(question.question_id, 0) > 0:
                total_gradable += 1

        # get text responses for text answers
        responseIds = [r.id for r in responses]
        if request.textResponseDict is None:
            textResponseDict = self.daoRegistry.studentResponseTextAnswerDao.getTexts(responseIds)
            request.textResponseDict = textResponseDict
        else:
            textResponseDict = request.textResponseDict

        # get text from answer selections
        if request.answerSelectionDict is None:
            answerSelectionDict = self.daoRegistry.studentResponseAnswerSelectionDao.getTexts(responseIds)
            request.answerSelectionDict = answerSelectionDict
        else:
            answerSelectionDict = request.answerSelectionDict

        textResponseDict.update(answerSelectionDict)

        responseDict = {}
        for response in responses:
            if response.user_uuid in responseDict:
                if response.question_id in responseDict[response.user_uuid]:
                    responseDict[response.user_uuid][response.question_id].append(response)
                else:
                    responseDict[response.user_uuid][response.question_id] = [response]
            else:
                responseDict[response.user_uuid] = {response.question_id: [response]}

        if not studentsDict:
            for user_uuid, student_name in request.nameDict.items():
                studentsDict[user_uuid] = [student_name, studentIDDict.get(user_uuid), True]
        else:
            for user_uuid, student_name in request.nameDict.items():
                if user_uuid in studentsDict:
                    studentsDict[user_uuid][2] = True
                else:
                    studentsDict[user_uuid] = [student_name, '-', True]

                if user_uuid in studentIDDict:
                    studentsDict[user_uuid][1] = studentIDDict[user_uuid]

            remove_keys = list()
            for user_uuid in studentsDict:
                if user_uuid not in studentIDDict:
                    for uid, student_id in studentIDDict.items():
                        if studentsDict[user_uuid][1] == student_id:
                            remove_keys.append((user_uuid, uid))
                            break

            # remove newer user_uuid that were not in the activity
            for k, new_k in remove_keys:
                if k in responseDict:
                    studentsDict[new_k] = studentsDict[k]
                else:
                    studentsDict[new_k] = studentsDict.pop(k)

        nameIdDict = dict()

        for user_uuid, student_name in request.nameDict.items():
            if student_name not in nameIdDict:
                nameIdDict[student_name] = list()

            if user_uuid in studentIDDict:
                nameIdDict[student_name].append(studentIDDict[user_uuid])

        for user_uuid in studentsDict:
            if user_uuid not in responseDict:
                responseDict[user_uuid] = {}
                for question in questions:
                    responseDict[user_uuid][question.question_id] = []
            else:
                for question in questions:
                    if question.question_id not in responseDict[user_uuid]:
                        responseDict[user_uuid][question.question_id] = []

        orderedNaming = sorted(studentsDict.items(), key=lambda x: x[1][0])

        # Record student responses
        for user_uuid, student_details in orderedNaming:

            student_name = student_details[0]
            student_id = student_details[1] or '-'
            presence = student_details[2]

            questionDict = responseDict.get(user_uuid)
            # Reset counters
            num_correct = 0

            # Increment for new student
            current_row += 1
            if presenceCol is not None and presence:
                student_count += 1
            elif presenceCol is None:
                student_count += 1

                # Record new student name
            if require_names:
                sName = remove_control_chars(student_name).strip()
                worksheet.write(current_row, nameCol,
                                lxmlParser.fromstring(sName).text_content() if sName != "" else "")

                if student_id == '-':
                    student_ids = nameIdDict.get(student_name, [])
                    if len(student_ids) != 1:
                        student_id = '-'
                    else:
                        student_id = student_ids[0]

                worksheet.write(current_row, studentIdColumn,
                                (lxmlParser.fromstring(student_id).text_content() if student_id != "" else ""),
                                plain_centered)
                if presenceCol is not None:
                    worksheet.write(current_row, presenceCol, (ugettext("Yes") if presence else ugettext("No")))
            else:
                worksheet.write(current_row, nameCol, convert(ugettext("Student names disabled")))
                worksheet.write(current_row, studentIdColumn, "-", plain_centered)

            firstPass = True
            for question_id in questionDict:

                if question_id not in order_mapping:
                    logger.error("Question ID %d not in the order mapping for activity id: %d" % (question_id,
                                                                                                  activity.id))
                    continue

                response = max(questionDict[question_id], key=lambda x: x.id) if questionDict[question_id] else None

                if isSR and response and firstPass:
                    worksheet.write(current_row, teamCol, self.SPACE_RACE_TEAMS.get(response.team) or "",
                                    teamCellFormat.get(response.team))
                    firstPass = False

                # Determine correctness and formatting for cell
                cell_formatting = None
                if response and response.is_correct is True:
                    cell_formatting = right_answer
                    num_correct += 1
                    correct_list[order_mapping[response.question_id] - questionCol] += 1
                elif response and response.is_correct is False:
                    cell_formatting = wrong_answer
                elif response and response.is_correct is None:
                    cell_formatting = null_answer
                elif response is None and qCADict.get(question_id, 0) > 0:
                    cell_formatting = wrong_answer
                elif response is None and qCADict.get(question_id, 0) == 0:
                    cell_formatting = null_answer
                else:
                    logger.error("this shouldn't happen. Activity id : %d" % activity_instance.id)

                # Get text to display student response
                if response:
                    display_text = ", ".join(textResponseDict.get(response.id, []))
                    try:
                        # if ESCAPED_HTML.match(display_text):
                        display_text = lxmlParser.fromstring(
                            display_text).text_content() if display_text.strip() != "" else ""
                    except ParserError:
                        logger.info("This text is making issues with lxml: %s", display_text)
                    except ValueError as e:
                        logger.warn(exceptionStack(e) + "||DISPLAY_TEXT||%s||" % display_text)
                else:
                    display_text = ""
                # Write appropriate response with correct formatting
                worksheet.write(current_row, order_mapping[question_id], display_text,
                                null_answer if presenceCol is not None and presence is False else cell_formatting)

            if total_gradable != 0:
                worksheet.write(current_row, totalScoreCol,
                                int(round((100.0 * num_correct) / total_gradable)), plain_centered)
            else:
                worksheet.write(current_row, totalScoreCol, 0, plain_centered)
            worksheet.write(current_row, nbCorrectCol, num_correct, plain_centered)

            # Record final student grade
            if student_count != 0:
                if total_gradable != 0:
                    worksheet.write(current_row, totalScoreCol, int(round((100.0 * num_correct) / total_gradable)),
                                    plain_centered)
                else:
                    worksheet.write(current_row, totalScoreCol, 0, plain_centered)
                worksheet.write(current_row, nbCorrectCol, num_correct, plain_centered)

            # Last increment of total correct
            total_correct += num_correct

            # Final row stats
        current_row += 1
        worksheet.write(current_row, 0, convert(ugettext("Class Scoring")), grey_left)

        if presenceCol is not None:
            worksheet.write(current_row, nameCol, "", grey_left)

        if teamCol:
            worksheet.write(current_row, teamCol, "", grey_left)

        if isSR:
            worksheet.write(current_row, nameCol, "", grey_left)

        worksheet.write(current_row, studentIdColumn, "", grey_left)

        # Average class score
        if total_gradable != 0 and student_count != 0:
            worksheet.write(current_row, totalScoreCol, (float(total_correct) / (total_gradable * student_count)),
                            grey_percentage)
        else:
            worksheet.write(current_row, totalScoreCol, 0, grey_percentage)

        # Percent correct by question
        q_n = questionCol
        if student_count != 0:
            worksheet.write(current_row, nbCorrectCol, float(total_correct) / student_count, grey_decimal)
            for n in range(question_count):
                worksheet.write(current_row, n + q_n, float(correct_list[n]) / student_count, grey_percentage)
        else:
            worksheet.write(current_row, nbCorrectCol, 0, grey_decimal)
            for n in range(question_count):
                worksheet.write(current_row, n + q_n, 0, grey_percentage)
                n += 1

        workbook.close()

        output.seek(0, 0)

        return output


class FilenameHelper(object):
    """
    """

    FILENAME_FORBIDDEN_CHARS = dict((ord(char), None) for char in '<>:\"/\\|?*')
    _basename = "socrative-report"

    def __init__(self, file_format, extra=None, basename=None, prepend=None):
        """

        :param file_format:
        :param extra:
        :param basename:
        :param prepend:
        :return:
        """
        self.file_format = file_format
        self.extra = extra
        self.prepend = prepend
        self.prettier = None

    def changePDFFilename(self, **kwargs):
        """
        will alter the folder name for students and student file name
        :param kwargs:
        :return:
        """

        fileParams = kwargs.get("fileParams")

        self.prepend = "Students_" + fileParams[2].strftime("%m_%d_%Y__%H_%M_") + fileParams[1] + "_"
        if type(fileParams[0]) is unicode or type(self.prepend) is unicode:
            self.prepend += fileParams[0][:70]
            self.prepend = self.prepend.translate(self.FILENAME_FORBIDDEN_CHARS)
        else:
            self.prepend += fileParams[0][:70]
            self.prepend = self.prepend.translate(None, '<>:\"/\\|?*')

        baseName = "Student_" + self.extra + "_" + fileParams[2].strftime("%m_%d_%Y__%H_%M") + "_"

        if type(baseName) is unicode or type(fileParams[0]) is unicode:
            baseName += fileParams[0][:70]
            baseName.translate(self.FILENAME_FORBIDDEN_CHARS)
        else:
            baseName += fileParams[0][:70]
            baseName = baseName.translate(None, '<>:\"/\\|?*')

        self._basename = baseName
        self.prettier = self.prepend + "/" + baseName + ".pdf"

    def from_report_settings(self, orig_format, num_reports, *args, **kwargs):
        """
        :param orig_format:
        :param num_reports:
        :param args:
        :param kwargs:
        :return:
        """

        if num_reports > 1:
                file_format = "zip"
                fileParams = kwargs.get("fileParams")

                baseName = "Socrative_" + fileParams[2].strftime("%m_%d_%Y__%H_%M_") + fileParams[1]+"_"

                if type(baseName) is unicode or type(fileParams[0]) is unicode:
                    baseName += fileParams[0][:70]
                    baseName = baseName.translate(self.FILENAME_FORBIDDEN_CHARS)
                else:
                    baseName += fileParams[0][:70]
                    baseName = baseName.translate(None, '<>:\"/\\|?*')

                self._basename = baseName
                self.file_format = file_format
        elif num_reports == 1:
            self.file_format = orig_format
            fileParams = kwargs.get("fileParams")

            if orig_format == "xlsx":
                baseName = "Class_" + fileParams[2].strftime("%m_%d_%Y__%H_%M_") + fileParams[1] + "_"

                if type(baseName) is unicode or type(fileParams[0]) is unicode:
                    baseName += fileParams[0][:70]
                    baseName = baseName.translate(self.FILENAME_FORBIDDEN_CHARS)
                else:
                    baseName += fileParams[0][:70]
                    baseName = baseName.translate(None, '<>:\"/\\|?*')

                self._basename = baseName
            elif orig_format == "pdf":
                baseName = "Question_" + fileParams[2].strftime("%m_%d_%Y__%H_%M_") + fileParams[1]+"_"

                if type(baseName) is unicode or type(fileParams[0]) is unicode:
                    baseName += fileParams[0][:70]
                    baseName = baseName.translate(self.FILENAME_FORBIDDEN_CHARS)
                else:
                    baseName += fileParams[0][:70]
                    baseName = baseName.translate(None, '<>:\"/\\|?*')
                self._basename = baseName
        else:
            raise Exception("invalid number of reports")

        self.prettier = self._basename + kwargs.get("atend", '') + ".%s" % self.file_format

    def mimetype(self):
        """
        :return:
        """
        if self.file_format == "pdf":
            return "application/pdf"
        if self.file_format == "xlsx":
            return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        if self.file_format == "zip":
            return "application/zip"
        else:
            return "application/octet-stream"
