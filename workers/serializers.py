# coding=utf-8

from common.base_dto import DTOConverter, ItemConverter

class EmailReportDTO(DTOConverter):
    """
    Serializer DTO for Answer DTO
    """
    stringifyDict = {
        "user_id": ItemConverter(),
        "email_type": ItemConverter(),
        "filename": ItemConverter(),
        "email_address": ItemConverter(),
        "id": ItemConverter(),
        "url": ItemConverter(),
        "mimetype": ItemConverter(optional=True),
        "action": ItemConverter(optional=True),
        "timestamp": ItemConverter(optional=True),
        "hide_password": ItemConverter(optional=True)
    }