#!/usr/bin/env python
# coding=utf-8

import os
import sys
pth = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + "/../")
sys.path.append(pth)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "socrative.settings")

from common.socrative_api import exceptionStack
from common.spring_container import SpringContainer
from workers.base_daemon import SocrativeDaemon
from multiprocessing.queues import Queue, Empty

import logging
import time
import django

logger = logging.getLogger(__name__)


class SocrativeSpringDaemon(SocrativeDaemon):
    """
    class to load the celery configuration for workers
    """

    def __init__(self, inPidfile, changeWorkingDir=False, preventCoreDump=False, inStdin='/dev/null',
                 inStdout='/dev/null', inStderr='/dev/null', onStartExitZero=True, maxOpenFile=1048576):
        """
        Constructor
        :param inPidfile:
        :param changeWorkingDir:
        :param preventCoreDump:
        :param inStdin:
        :param inStdout:
        :param inStderr:
        :param onStartExitZero:
        :param maxOpenFile:
        :return:
        """
        super(SocrativeSpringDaemon, self).__init__(inPidfile, changeWorkingDir, preventCoreDump, inStdin, inStdout,
                                                    inStderr, onStartExitZero, maxOpenFile)

        self.queue = Queue()

        self.__container = None

    def _customDaemonStart(self):
        """

        :return:
        """
        # Current path
        currentDir = os.path.dirname(os.path.abspath(__file__)) + os.sep + "../conf/async_workers/"

        try:

            django.setup()

            logger.info("__instantiate spring objects : currentDir=%s", currentDir)

            # Get container
            logger.info("Getting container")
            self.__container = SpringContainer(os.path.join(currentDir, "workers.conf"),
                                               os.path.join(currentDir,
                                                            "workers.%s.property" % os.environ.get("SOCRATIVE_PLATFORM")
                                                            )
                                               )
        except Exception as e:
            logging.error("__SpringInitializer.init :  Exception, ex=%s", exceptionStack(e))
            raise

        logging.error("All good instantiating workers")
        while True:
            try:
                res = self.queue.get(block=True, timeout=0.5)
                if res == "stop":
                    break
            except Empty:
                time.sleep(1)
            except Exception as e:
                logger.error(exceptionStack(e))
                time.sleep(1)

    def _customDaemonReload(self, *args, **kwargs):
        """
        reload
        :param args:
        :param kwargs:
        :return:
        """
        logger.info("Method is not implemented")
        pass

    def _customDaemonStop(self):
        """
        method to stop the current Daemon
        :return:
        """

        try:
            self.__container.shutdown_hook()
            self.__container = None

        except Exception as e:
            logger.error(exceptionStack(e))

        self.queue.put("stop")

        logger.info("stopping the daemon")

if __name__ == "__main__":
    s = SocrativeSpringDaemon("/tmp/socrative_workers.pid")
    s.daemonMain(sys.argv)