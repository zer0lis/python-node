# coding = utf-8

class WorkerCounters(object):
    """
    class to hold counters for datadog specific to worker processes
    """

    REPORTS_TO_GOOGLE_DRIVE = 'socrative_workers.upload.google_drive'
    REPORTS_TO_GOOGLE_DRIVE_FAIL = 'socrative_workers.upload.google_drive.fail'

    PROCESS_IMAGE_SKIP_COUNTER = 'socrative_workers.images.process.skip'
    PROCESS_IMAGE_COUNTER = 'socrative_workers.images.process'
    PROCESS_IMAGE_FAIL = 'socrative_workers.images.process.fail'
    PROCESS_IMAGE_RESIZE = 'socrative_workers.images.resize'

    REPORTS_MISSING = 'socrative_workers.reports.missing'
    REPORT_TOO_BIG_FOR_EMAIL = 'socrative_workers.reports.email.attachment_size_too_large'
    REPORTS_VIA_EMAIL_FAIL = 'socrative_workers.reports.sent.email.fail'
    REPORTS_VIA_EMAIL = 'socrative_workers.reports.sent.email'
    REGISTRATION_EMAIL_COUNTER = 'socrative_workers.emails.registration.sent'
    REGISTRATION_EMAIL_FAIL = 'socrative_workers.emails.registration.sent.fail'
    SHARE_QUIZ_EMAIL_SENT = 'socrative_workers.emails.share_quiz.sent'
    SHARE_QUIZ_EMAIL_FAIL = 'socrative_workers.emails.share_quiz.fail'
    FORGOT_PWD_EMAIL_SENT = 'socrative_workers.emails.forgot_password.sent'
    FORGOT_PWD_EMAIL_FAIL = 'socrative_workers.emails.forgot_password.fail'
    UPGRADE_TO_PRO_EMAIL_SENT = 'socrative_workers.emails.upgrade_pro.sent'
    UPGRADE_TO_PRO_EMAIL_FAIL = 'socrative_workers.emails.upgrade_pro.fail'
    PRO_RECEIPT_EMAIL_SENT = 'socrative_workers.emails.pro_receipt.sent'
    PRO_RECEIPT_EMAIL_FAIL = 'socrative_workers.emails.pro_receipt.fail'
    ACCOUNTING_EMAIL = 'socrative_workers.emails.accounting.time'
    ACCOUNTING_EMAIL_FAIL = 'socrative_workers.emails.accounting.fail'
    EXPIRING_ACCOUNTS = 'socrative_workers.emails.expiring_accounts.time'
    EXPIRING_ACCOUNTS_FAIL = 'socrative_workers.emails.expiring_accounts.fail'
    LAST_LOGINS = 'socrative_workers.emails.last_logins.time'
    LAST_LOGINS_FAIL = 'socrative_workers.emails.last_logins.fail'
    ONBOARD_PACKAGE = 'socrative_workers.emails.onboard_package.time'
    ONBOARD_PACKAGE_FAIL = 'socrative_workers.emails.onboard_package.fail'

    CREATE_REPORTS_COUNTER = 'socrative_workers.reports.create'
    CREATE_REPORTS_FAIL_COUNTER = 'socrative_workers.reports.create.fail'
    CREATE_REPORTS_IN_PROGRESS = 'socrative_workers.reports.create.in_progress'
    CREATE_REPORTS_SKIP_COUNTER = 'socrative_workers.reports.create.skip'
    UPLOAD_REPORT_TO_S3 = 'socrative_workers.reports.upload'
    FREE_RESPONSE_REPORT = 'socrative_workers.reports.create.free_response_excel'
    WHOLE_CLASS_REPORT = 'socrative_workers.reports.create.whole_class_excel'

