# coding=utf-8

from common.spring_container import SpringItem
from common.socrative_api import safeToInt
from workers.redis_tasks import WorkerTasks
from common.email_client import EmailClient
from multiprocessing.queues import Queue, Empty
from workers.imagesWorker import ImageWorker
from workers.reportsWorker import ReportWorker
from workers.actionWorker import ActionWorker
from workers.email_worker import EmailingWorker
from threading import Timer
import time

import logging

logger = logging.getLogger(__name__)


class WorkerManager(SpringItem):
    """
    master process for the rest
    """

    def __init__(self, imageWorkersNb, reportWorkersNb, actionWorkersNb, emailClient, redisClient, daoRegistry):
        """

        :param imageWorkersNb: number of workers to process images
        :param reportWorkersNb: number of workers to process reports
        :param emailClient: email client
        :param redisClient: redis client
        :return:
        """

        super(WorkerManager, self).__init__()

        self.__imgWorkers = safeToInt(imageWorkersNb)

        if self.__imgWorkers is None:
            raise ValueError("imageWorkersNb param is not a valid number, type: %s" % str(type(imageWorkersNb)))

        self.__reportWorkers = safeToInt(reportWorkersNb)
        if self.__reportWorkers is None:
            raise ValueError("reportWorkersNb param is not a valid number, type: %s" % str(type(reportWorkersNb)))

        self.__actionWorkers = safeToInt(actionWorkersNb)
        if self.__actionWorkers is None:
            raise ValueError("actionWorkersNb param is not a valid number, type: %s" % str(type(reportWorkersNb)))

        if type(emailClient) is not EmailClient:
            raise ValueError("emailThread is not of type EmailClient, but %s" % str(type(emailClient)))

        if type(redisClient) is not WorkerTasks:
            raise ValueError("redisClient is not of type WorkerTasks, but %s" % str(type(redisClient)))

        logger.info("settings are ok, worker manager created")
        self.queue = Queue()

        self.inQueue = Queue()

        logger.info("creating the workers")
        self.procList = []

        self.emailClient = emailClient
        self.redisClient = redisClient
        self.daoRegistry = daoRegistry

        self.stop = False

        self.timer = Timer(5, self.checkWorkers)

    def checkWorkers(self):
        """
        checking and reviving workers
        :return:
        """
        while True:
            try:
                if self.stop is True:
                    break

                x = self.inQueue.get(True, 1)
                if x == "STOP":
                    logger.debug("before adding the poison pill to stop the workers")
                    for _ in self.procList:
                        self.queue.put("stop")

                    for p in self.procList:
                        p.join()

                    logger.debug("finished sending the poison pill")
                    self.procList = []

                    for _ in range(self.__imgWorkers):
                        self.procList.append(ImageWorker(self.redisClient, self.queue, self.inQueue, self.daoRegistry))

                    for _ in range(self.__reportWorkers):
                        self.procList.append(ReportWorker(self.redisClient, self.queue, self.inQueue, self.daoRegistry))

                    for _ in range(self.__actionWorkers):
                        self.procList.append(ActionWorker(self.redisClient, self.queue, self.inQueue, self.daoRegistry))

                    self.procList.append(EmailingWorker(self.redisClient, self.emailClient, self.queue, self.inQueue,
                                                            self.daoRegistry))

                    for proc in self.procList:
                        proc.start()
            except Empty:
                time.sleep(1)

    def destroy(self):
        """

        :return:
        """

        self.stop = True

        logger.debug("before adding the poison pill to stop the workers")
        for _ in self.procList:
            self.queue.put("stop")

        for p in self.procList:
            p.join()

        logger.debug("finished sending the poison pill")

    def after_properties_set(self):
        """

        :return:
        """

        for _ in range(self.__imgWorkers):
            self.procList.append(ImageWorker(self.redisClient, self.queue, self.inQueue, self.daoRegistry))

        for _ in range(self.__reportWorkers):
            self.procList.append(ReportWorker(self.redisClient, self.queue, self.inQueue, self.daoRegistry))

        for _ in range(self.__actionWorkers):
            self.procList.append(ActionWorker(self.redisClient, self.queue, self.inQueue, self.daoRegistry))

        self.procList.append(EmailingWorker(self.redisClient, self.emailClient, self.queue, self.inQueue,
                                            self.daoRegistry))

        for proc in self.procList:
            proc.start()

        self.timer.start()