# coding=utf-8

import cStringIO
import datetime
import logging
import os
import sys
import time
import ujson
import urllib
import uuid
from multiprocessing import Process
from multiprocessing.queues import Empty

import pytz
import xlsxwriter
from boto.s3.connection import S3Connection
from datadog import statsd, initialize
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template import Template, Context
from django.utils.translation import ugettext, trans_real
from psycopg2 import IntegrityError

from common.cryptography import Cryptography
from common.dao import ActivityInstanceModel
from common.dao import BaseDao
from common.socrative_api import safeToInt, exceptionStack
from common.socrative_config import ConfigRegistry
from socrative_users.dao import TempTokenModel
from workers.counters import WorkerCounters
from workers.task_types import TaskType, EmailTypes

logger = logging.getLogger(__name__)


class EmailingWorker(Process):
    """
    worker for sending registration emails
    """

    def __init__(self, redisClient, emailClient, processQueue, inQueue, daoRegistry):
        """
        :param redisClient:
        :param processQueue:
        :return:
        """

        super(EmailingWorker, self).__init__()

        self.redisClient = redisClient
        self.emailClient = emailClient
        self.queue = processQueue
        self.inQueue = inQueue
        self.daoRegistry = daoRegistry
        self.botoConnection = None
        self.s3Bucket = None

        self.welcomeEmailTemplate = None
        with open(os.path.dirname(os.path.abspath(__file__))+"/../socrative_users/templates/emails/welcome.html",
                  "r") as f:
            self.welcomeEmailTemplate = Template(f.read())

        self.upgradedProEmailTemplate = None
        with open(os.path.dirname(os.path.abspath(__file__)) + "/../socrative_users/templates/emails/upgrade_pro.html",
                  "r") as f:
            self.upgradedProEmailTemplate = Template(f.read())

        self.emailReportTemplate = None
        self.emailReportLinkTemplate = None
        with open(os.path.dirname(os.path.abspath(__file__)) + "/../quizzes/templates/emails/email_report.html",
                  "r") as f:
            self.emailReportTemplate = Template(f.read())

        with open(os.path.dirname(
                os.path.abspath(__file__)) + "/../quizzes/templates/emails/email_report_with_file_link.html", "r") as f:
            self.emailReportLinkTemplate = Template(f.read())

        self.receiptEmailTemplate = None
        with open(os.path.dirname(os.path.abspath(__file__)) + "/../lecturers/templates/pro_receipt.html", "r") as f:
            self.receiptEmailTemplate = Template(f.read())

        with open(os.path.dirname(
                os.path.abspath(__file__)) + "/../quizzes/templates/emails/share_quiz.txt") as f:
            self.shareQuizTextTemplate = f.read()

        with open(os.path.dirname(
                os.path.abspath(__file__)) + "/../quizzes/templates/emails/share_quiz.html") as f:
            self.shareQuizHtmlTemplate = f.read()

        with open(os.path.dirname(os.path.abspath(__file__)) + "/../socrative_users/templates/emails/multi_pro_buyer.html",
                  "r") as f:
            self.multi_pro_buyer = Template(f.read())

        self.ADMIN_MARKETING_URL = ConfigRegistry.getItem("ADMIN_MARKETING_URL")

        self.DOWNLOAD_URL = ConfigRegistry.getItem("REPORT_DOWNLOAD_URL")
        self.MAX_EMAIL_FILE_SIZE = safeToInt(ConfigRegistry.getItem("MAX_EMAIL_FILE_SIZE")) or 10 * 1024 ** 2
        self.CREATE_PASSWORD_URL = ConfigRegistry.getItem("CREATE_PASSWORD_URL")
        self.CREATE_PASSWORD_TOKEN_DURATION = ConfigRegistry.getItem("CREATE_PASSWORD_TOKEN_DURATION") or 30*24

    def run(self):
        """
        overwrote run method
        :return:
        """

        initialize(api_key=os.environ["DATADOG_API_KEY"], app_key=os.environ["DATADOG_APP_KEY"])

        while True:
            try:
                item = None
                try:
                    item = self.queue.get_nowait()
                except Empty:
                    logger.debug("empty queue, worker didn't received the poison pill")

                if item == "stop":
                    logger.info("RegistrationWorker:received the poison pill.Exiting")
                    break

                item = self.redisClient.get_task(TaskType.SEND_EMAIL)

                if item is None:
                    time.sleep(1)
                    continue

                item = ujson.loads(item)

                email_type = item.get("email_type")

                if email_type == EmailTypes.REGISTRATION:
                    self.send_registration_email(item)
                elif email_type == EmailTypes.REPORTS:
                    self.send_report_email(item)
                elif email_type == EmailTypes.QUIZ_SHARE:
                    self.send_quiz_share_email(item)
                elif email_type == EmailTypes.UPGRADE_PRO:
                    self.send_upgrade_pro_email(item)
                elif email_type == EmailTypes.FORGOT_PASSWORD:
                    self.send_forgot_password_email(item)
                elif email_type == EmailTypes.RECEIPTS:
                    self.send_pro_receipt_email(item)
                elif email_type == EmailTypes.ACCOUNTING_REPORT:
                    self.send_accounting_report(item)
                elif email_type == EmailTypes.PRO_EXPIRING:
                    self.send_pro_expiring_report(item)
                elif email_type == EmailTypes.LOGINS:
                    self.send_last_logins_report(item)
                elif email_type == EmailTypes.MULTI_SEATS_RECEIPT:
                    self.send_multi_seats_receipt(item)
                else:
                    logger.error("Don't know what to do for this email type: %s" % str(item))

            except OSError as e:
                logger.error(exceptionStack(e))
                try:
                    logger.warn("preparing for restarting worker")
                    self.inQueue.put("STOP")
                    sys.exit(1)
                except Exception as e:
                    logger.error(exceptionStack(e))

            except Exception as e:
                logger.error(exceptionStack(e))
                time.sleep(1)
            finally:
                # avoid throttling the cpu
                trans_real.deactivate()
                time.sleep(0.1)

    def __getFileFromS3(self, activityId, filename):
        """

        :param activityId:
        :param filename:
        :return:
        """
        k = self.s3Bucket.get_key("reports/%d/" % activityId + filename)
        if k:
            return k

        for k in self.s3Bucket.list(prefix="reports/%d/" % activityId):
            if k.name[-6:] == filename[-6:]:
                return k

        return None

    def send_report_email(self, itemDict):
        """
        :param itemDict:
        :param: user_id
        :return:
        """

        dt = time.time()
        user_id = itemDict.get("user_id")
        lang = 'en'
        if user_id:
            try:
                user = self.daoRegistry.usersDao.loadUserById(user_id)
                lang = user.language or 'en'
            except BaseDao.NotFound:
                pass

        trans_real.activate(lang)

        try:
            filename = itemDict["filename"]
            email_address = itemDict["email_address"]
            mimetype = itemDict["mimetype"]

            subject = ugettext('Your report from Socrative')

            # get the size of the file
            if self.s3Bucket is None:
                if self.botoConnection is None:
                    self.botoConnection = S3Connection(ConfigRegistry().getItem("AWS_ACCESS_KEY_ID"),
                                                       ConfigRegistry().getItem("AWS_SECRET_ACCESS_KEY"))
                self.s3Bucket = self.botoConnection.get_bucket(ConfigRegistry().getItem("AWS_STORAGE_BUCKET_NAME"))

            k = self.__getFileFromS3(itemDict["id"], filename)

            logger.info("File to be sent via email reports/%s", filename)

            if k is None:
                self.daoRegistry.activityInstanceDao.updateReportStatus(itemDict["id"],
                                                                        ActivityInstanceModel.REPORT_NOT_DONE)
                logger.warn("reports/%d_" % itemDict["id"] + filename + " is not on s3, changing report status to not"
                                                                        " done for user %s." % email_address)
                self.redisClient.add_tasks(TaskType.REPORT_ACTIONS, [itemDict])
                statsd.increment(WorkerCounters.REPORTS_MISSING)
                return

            payload = None

            if k.size >= self.MAX_EMAIL_FILE_SIZE:
                user_id = itemDict.get("user_id")
                msg = "%s|%d|%s" % (user_id, itemDict["id"], filename.encode('utf-8'))
                url = itemDict.get("url") + self.DOWNLOAD_URL + "/" + Cryptography.encryptDES_CBC(Cryptography.DOWNLOAD,
                                                                                                  msg)
                url = url.encode("utf-8")
                body = self.emailReportLinkTemplate.render(Context({"DOWNLOAD_URL": url, "url": self.ADMIN_MARKETING_URL},
                                                                   use_l10n=True))
                logger.warn("file too big, url for download is %s for email %s", url, email_address)
                statsd.increment(WorkerCounters.REPORT_TOO_BIG_FOR_EMAIL)
            else:

                body = self.emailReportTemplate.render(Context({"url": self.ADMIN_MARKETING_URL}, use_l10n=True))
                payload = k.get_contents_as_string()
                logger.warn("attachment size %d for user %s", len(payload), email_address)

            logger.warn("mail to be sent to %s", email_address)
            email = EmailMessage(subject, body, 'reports@socrative.com', [email_address])
            email.encoding = "utf-8"
            email.content_subtype = "html"
            if k.size < self.MAX_EMAIL_FILE_SIZE:
                email.attach(urllib.quote(filename.encode('utf-8')), payload, mimetype=mimetype)

            self.emailClient.sendEmail(email)

            logger.info("mail to %s added to the queue", email_address)
            duration = time.time() - dt
            statsd.histogram(WorkerCounters.REPORTS_VIA_EMAIL, duration)
        except Exception as e:
            logger.error("email %s -> exception %s", str(itemDict.get("email_address")), exceptionStack(e))
            statsd.increment(WorkerCounters.REPORTS_VIA_EMAIL_FAIL)
        finally:
            trans_real.deactivate()

    def send_registration_email(self, itemDict):
        """

        :param itemDict:
        :return:
        """
        dt = time.time()
        email_addr = itemDict.get("email_address")
        if not email_addr:
            logger.error("Email address is missing for item dict %s" % str(itemDict))
            return

        try:
            user = self.daoRegistry.usersDao.loadUserByEmail(email_addr.lower())
        except BaseDao.NotFound:
            logger.error("User with email %s is not found" % email_addr)
            return

        try:
            trans_real.activate(user.language or "en")

            subject = ugettext('Welcome to Socrative!')
            body = self.welcomeEmailTemplate.render(Context({"url": self.ADMIN_MARKETING_URL}))

            email = EmailMessage(subject, body, 'noreply@socrative.com', [user.email],
                                 headers={'From': 'Socrative <noreply@socrative.com>'})
            email.content_subtype = "html"

            logger.info("registration email to be sent to %s" % user.email)
            self.emailClient.sendEmail(email)

            logger.error("registration email sent to %s" % user.email)

            trans_real.deactivate()
            duration = time.time() - dt
            statsd.histogram(WorkerCounters.REGISTRATION_EMAIL_COUNTER, duration)
        except Exception as e:
            logger.error(exceptionStack(e))
            statsd.increment(WorkerCounters.REGISTRATION_EMAIL_FAIL)

    def send_quiz_share_email(self, itemDict):
        """

        :param itemDict:
        :return:
        """

        dt = time.time()
        try:
            user = self.daoRegistry.usersDao.loadUserByEmail(itemDict["email_address"])
        except BaseDao.NotFound:
            logger.error("User with email %s is not found" % itemDict["email_address"])
            return

        try:
            trans_real.activate(user.language or "en")
            txt_template = Template(self.shareQuizTextTemplate)
            html_template = Template(self.shareQuizHtmlTemplate)

            textBody = txt_template.render(Context(itemDict))

            htmlBody = html_template.render(Context(itemDict))

            email = EmailMultiAlternatives(ugettext('Socrative shared quiz'), textBody,
                                           from_email='noreply@socrative.com', to=[itemDict["to"]])
            email.attach_alternative(htmlBody, "text/html")

            logger.info("share quiz email to be sent to %s" % itemDict["to"])
            self.emailClient.sendEmail(email)
            logger.error("share quiz email sent to %s" % itemDict["to"])

            trans_real.deactivate()

            duration = time.time() - dt
            statsd.histogram(WorkerCounters.SHARE_QUIZ_EMAIL_SENT, duration)
        except Exception as e:
            logger.error(exceptionStack(e))
            statsd.increment(WorkerCounters.SHARE_QUIZ_EMAIL_FAIL)

    def send_forgot_password_email(self, itemDict):
        """

        :param itemDict:
        :return:
        """
        dt = time.time()
        try:
            user = self.daoRegistry.usersDao.loadUserByEmail(itemDict["to"])
        except BaseDao.NotFound:
            logger.error("User with email %s is not found" % itemDict["to"])
            return

        try:
            trans_real.activate(user.language or "en")
            commonBody = ugettext("""Hi %s,""" % itemDict["first_name"]) + "\n\n" + \
                         ugettext("""We received a request to reset the password for your Socrative account. """
                                  """If you didn't request a password reset, please ignore this email. """
                                  """If you did; to reset your password, click the link below or """
                                  """copy and paste the entire URL into your browser. """)

            helpCenterUrl = "http://help.socrative.com/article/resetting-a-forgotten-password/"
            emailBody = commonBody + "\n\n" + itemDict["url"] + "\n" + \
                        ugettext("""If you have more questions about password reset, please visit our %s Help Center"""
                                 """ %s""") % ('', '') + "<%s>." % helpCenterUrl

            htmlBody = "<html><body>" + commonBody.replace("\n", "<br>") + "<br><br>" + \
                       "<a href='%s'> %s </a>" % (itemDict["url"], itemDict["url"]) + "<br>" + \
                       ugettext("""If you have more questions about password reset, please visit our %s Help Center """
                                """%s""") % ("<a href='%s'>" % helpCenterUrl, "</a>")

            email = EmailMultiAlternatives(ugettext('Socrative Reset Password'), emailBody,
                                           from_email='noreply@socrative.com', to=[itemDict["to"]])
            email.attach_alternative(htmlBody, "text/html")

            logger.info("reset password mail to be sent to %s" % itemDict["to"])

            self.emailClient.sendEmail(email)

            logger.error("reset password mail sent to %s" % itemDict["to"])

            trans_real.deactivate()

            duration = time.time() - dt
            statsd.histogram(WorkerCounters.FORGOT_PWD_EMAIL_SENT, duration)
        except Exception as e:
            logger.error(exceptionStack(e))
            statsd.increment(WorkerCounters.FORGOT_PWD_EMAIL_FAIL)

    def send_upgrade_pro_email(self, itemDict):
        """

        :param itemDict:
        :return:
        """

        dt = time.time()
        try:
            user = self.daoRegistry.usersDao.loadUserByEmail(itemDict["email_address"].lower())
            license_key = self.daoRegistry.licenseDao.getLastLicenseKey(user.id) or ''
        except BaseDao.NotFound:
            logger.error("User with email %s is not found" % itemDict["email_address"])
            return
        except Exception as e:
            logger.error(exceptionStack(e))
            return

        try:
            token = uuid.uuid4().hex
            url = self.CREATE_PASSWORD_URL + token
            hide = itemDict.get("hide_password", False)
            logger.error(itemDict)
            if not hide:
                self.redisClient.set_password_token(token, user.id)

            trans_real.activate(user.language or "en")

            subject = ugettext('Welcome to Socrative PRO!')
            body = self.upgradedProEmailTemplate.render(Context({
                "url": url,
                "license_key": license_key.upper(),
                "hide": hide
            }))

            email = EmailMessage(subject, body, 'noreply@socrative.com', [user.email],
                                 headers={'From': 'Socrative <noreply@socrative.com>'})
            email.content_subtype = "html"

            self.emailClient.sendEmail(email)

            trans_real.deactivate()

            duration = time.time() - dt
            statsd.histogram(WorkerCounters.UPGRADE_TO_PRO_EMAIL_SENT, duration)
        except Exception as e:
            logger.error(exceptionStack(e))
            statsd.increment(WorkerCounters.UPGRADE_TO_PRO_EMAIL_FAIL)

    def send_pro_receipt_email(self, itemDict):
        """

        :param itemDict:
        :return:
        """
        dt = time.time()
        try:
            user = self.daoRegistry.usersDao.loadUserByEmail(itemDict["email_address"])
        except BaseDao.NotFound:
            logger.error("User with email %s is not found" % itemDict["email_address"])
            return

        try:
            trans_real.activate(user.language or "en")

            tmp, license_id, years, license_key, expiration_date = self.daoRegistry.licenseDao.getLicenseData(user.id)
            stripeData = ujson.loads(tmp) if tmp else {}

            if not stripeData:
                logger.error("No pro payment info for user %s" % user.email)
                return

            data = {}
            if expiration_date.date() < datetime.date(2017, 8, 1):
                expiration_date = datetime.datetime(2017, 8, 1)
            data["email"] = user.email
            data["receipt_id"] = stripeData.get("receipt_id", license_id)
            data["amount"] = "$" + str(stripeData["amount"]*1.0/100)
            data["card_type"] = stripeData["source"]["brand"]
            data["last4"] = stripeData["source"]["last4"]
            data["expiration_date"] = expiration_date
            data["name"] = "%s, %s" % ('' if user.last_name is None else user.last_name,
                                       '' if user.first_name is None else user.first_name)
            if user.organization_type in ("OTHR", "CORP"):
                data["organization"] = user.description
            elif user.organization_type == "USHE":
                data["organization"] = user.university
            elif user.organization_type == "K12":
                data["organization"] = user.school_name
            else:
                data["organization"] = ''
            data["license_key"] = license_key.upper()
            data["years"] = years

            subject = ugettext('Socrative Pro Order Confirmation')
            body = self.receiptEmailTemplate.render(Context(data))

            email = EmailMessage(subject, body, 'noreply@socrative.com', [user.email],
                                 headers={'From': 'Socrative <noreply@socrative.com>'})
            email.content_subtype = "html"

            self.emailClient.sendEmail(email)

            trans_real.deactivate()

            duration = time.time() - dt
            statsd.histogram(WorkerCounters.PRO_RECEIPT_EMAIL_SENT, duration)
        except Exception as e:
            logger.error(exceptionStack(e))
            statsd.increment(WorkerCounters.PRO_RECEIPT_EMAIL_FAIL)

    def parse_accounting_date(self, date_obj):
        """
        transform date in MST timezone
        :return:
        """
        if type(date_obj) not in (str, unicode):
            raise Exception("Date is not string formatted")

        dt = datetime.datetime.strptime(date_obj, "%Y-%m-%d")

        mst = pytz.timezone('US/Mountain')
        dt = mst.localize(dt).astimezone(pytz.utc)

        return dt

    def send_accounting_report(self, itemObj):
        """

        :param itemObj: dictionary with parameters for generating the reports
        :return:
        """

        if not itemObj.get('emails'):
            logger.error("Empty or invalid list of recipients")
            return

        start = time.time()
        try:
            start_date = self.parse_accounting_date(itemObj.get('start_date')) if "start_date" in itemObj else None
            end_date = self.parse_accounting_date(itemObj.get('end_date')) if 'end_date' in itemObj else None

            # we leave only masteryconnect addresses as a security measure
            receiver_list = [emailAddr for emailAddr in itemObj.get("emails", []) if emailAddr.endswith("@masteryconnect.com")]

            transaction_dict = self.daoRegistry.licenseTransactionDao.getTransactions(start_date, end_date)

            license_ids = transaction_dict.keys()
            license_dict = self.daoRegistry.licenseDao.getLicensesByIds(license_ids)
            activation_dict = self.daoRegistry.licenseActivationDao.getLicenseActivations(license_ids)

            buyer_dict = dict((license_dict[k][0], k) for k in license_dict)
            buyer_ids = list(set([value[0] for value in license_dict.values()]))
            coupon_ids = list(set([value[1] for value in license_dict.values() if value[1] is not None]))

            coupon_dict = self.daoRegistry.couponDao.getCouponNames(coupon_ids)

            user_ids = list(set(activation_dict.keys() + buyer_ids))

            user_dict = self.daoRegistry.usersDao.getUserAccountingData(user_ids)

            excel_data = list()
            for user_id in buyer_ids:
                license_id = buyer_dict[user_id]
                renewal = str(activation_dict.get(user_id, [0, 1])[1] > 1)
                purchase_date = transaction_dict[license_id][2]
                paymentId = transaction_dict[license_id][3]
                amount = (transaction_dict[license_id][1] / 100.0)
                seats = transaction_dict[license_id][0]
                autoRenew = str(license_dict[license_id][2])
                years = license_dict[license_id][3]
                price_per_seat = license_dict[license_id][4]
                license_key = license_dict[license_id][5]
                buyer_id = license_dict[license_id][0]
                buyer_email = user_dict[buyer_id][2]
                buyer_email = (buyer_email or '').decode('utf-8') if type(buyer_email) is str else (buyer_email or '')
                coupon_name = coupon_dict.get(license_dict[license_id][1], '')
                applyNow = str(buyer_id in activation_dict)
                first_name = user_dict[user_id][0] or ''
                last_name = user_dict[user_id][1] or ''
                registration_date = user_dict[user_id][3]
                expiration_date = license_dict[license_id][6]
                purchaser_email = ''
                country = user_dict[user_id][4] or ''
                org_type = user_dict[user_id][5] or ''
                org_name = user_dict[user_id][6] or user_dict[user_id][7] or user_dict[user_id][8] or ''
                state = user_dict[user_id][9] or ''
                zip_code = user_dict[user_id][10] or ''
                role = ''
                try:
                    role = ujson.loads(user_dict[user_id][11])[0]
                except Exception:
                    pass

                try:
                    manualPurchase = False
                    purchaser_email = ujson.loads(license_dict[license_id][7])["description"]
                    purchaser_email = purchaser_email[purchaser_email.rfind(' ') + 1:]
                except:
                    manualPurchase = True
                    pass
                mst = pytz.timezone('US/Mountain')
                excel_data.append((str(manualPurchase), purchase_date.strftime("%Y-%m-%d %H:%M:%S"),
                                 purchase_date.astimezone(mst).strftime("%Y-%m-%d %H:%M:%S"),
                                 paymentId, registration_date.strftime("%Y-%m-%d"), user_id, buyer_email, purchaser_email,
                                 first_name, last_name,
                                 seats, years, coupon_name, price_per_seat / 100.0, amount, applyNow, autoRenew, renewal,
                                 license_key, expiration_date.astimezone(mst).strftime("%Y-%m-%d %H:%M:%S"), country,
                                 org_type,
                                 org_name, state, zip_code, role))

            sorted_data = sorted(excel_data, key=lambda item: item[1], reverse=True)

            memFile = cStringIO.StringIO()
            workbook = xlsxwriter.Workbook(memFile)
            worksheet = workbook.add_worksheet('Socrative Purchases')

            worksheet.write(0, 0, 'Manual purchase')
            worksheet.write(0, 1, 'Purchase Date (UTC)')
            worksheet.write(0, 2, 'Purchase Date (MST)')
            worksheet.write(0, 3, 'Payment ID')
            worksheet.write(0, 4, 'Registration Date')
            worksheet.write(0, 5, 'Buyer ID')
            worksheet.write(0, 6, 'Buyer Email')
            worksheet.write(0, 7, 'Stripe Charge Email')
            worksheet.write(0, 8, 'First Name')
            worksheet.write(0, 9, 'Last Name')
            worksheet.write(0, 10, 'Seats')
            worksheet.write(0, 11, 'Years')
            worksheet.write(0, 12, 'Coupon')
            worksheet.write(0, 13, 'Price Per Seat ($)')
            worksheet.write(0, 14, 'Amount ($)')
            worksheet.write(0, 15, 'Apply Now')
            worksheet.write(0, 16, 'Auto Renewal')
            worksheet.write(0, 17, 'Renewal (T/F)')
            worksheet.write(0, 18, 'License Key')
            worksheet.write(0, 19, 'Expiration Date (MST)')
            worksheet.write(0, 20, 'Country')
            worksheet.write(0, 21, 'Org Type')
            worksheet.write(0, 22, 'Org Name/School Name')
            worksheet.write(0, 23, 'State')
            worksheet.write(0, 24, 'Zip Code')
            worksheet.write(0, 25, 'Role')

            i = 1
            for data in sorted_data:
                j = 0
                for val in data:
                    worksheet.write(i, j, val)
                    j += 1
                i += 1

            workbook.close()
            memFile.seek(0, 0)

            start_date = start_date.strftime("%Y-%m-%d") if start_date else None
            end_date = end_date.strftime("%Y-%m-%d") if end_date else None
            email = EmailMessage("Monthly report with Socrative PRO license upgrades",
                                 "Hi,<br>I've attached the xlsx file with the report for the period %s - %s .\
                                 <br>Have a nice day,<br>Yours truly,<br>The cron job" % (start_date or excel_data[-1][1][:10],
                                                                                          end_date or excel_data[0][1][:10]),
                                 'noreply@socrative.com',
                                 receiver_list)
            email.encoding = "utf-8"
            email.content_subtype = "html"
            email.attach("Monthly_Socrative_Pro_Report.xlsx", memFile.read(),
                         mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")

            memFile.close()
            self.emailClient.sendEmail(email)
            statsd.histogram(WorkerCounters.ACCOUNTING_EMAIL, time.time()-start)
        except Exception as e:
            logger.error(exceptionStack(e))
            statsd.increment(WorkerCounters.ACCOUNTING_EMAIL_FAIL)

    def send_pro_expiring_report(self, itemObj):
        """
        send the pro accounts expiring by a specific date
        :param itemObj:
        :return:
        """

        if not itemObj.get('emails'):
            logger.error("Empty or invalid list of recipients")
            return

        start = time.time()
        try:
            start_date = self.parse_accounting_date(itemObj.get('start_date')) if "start_date" in itemObj else None
            end_date = self.parse_accounting_date(itemObj.get('end_date')) if 'end_date' in itemObj else None

            # we leave only masteryconnect addresses as a security measure
            receiver_list = [emailAddr for emailAddr in itemObj.get("emails", []) if emailAddr.endswith("@masteryconnect.com")]

            org_type = itemObj.get('org_type')

            licenseIds = self.daoRegistry.licenseTransactionDao.getExpiringLicenses(start_date, end_date)

            # get all license data

            licenseDict = self.daoRegistry.licenseDao.getLicensesForIds(licenseIds)

            userIds = [licenseItem[0] for licenseItem in licenseDict.values()] + [licenseItem[3] for licenseItem in licenseDict.values()]
            userIds = list(set(userIds))

            userDict = self.daoRegistry.usersDao.getEmailForUserIds(userIds, org_type)

            data = []
            for licenseId in licenseDict:
                if licenseDict[licenseId][0] not in userDict:  # the user doesn't correspond to the org type filter
                    continue

                userId = licenseDict[licenseId][0]
                buyerId = licenseDict[licenseId][3]
                email = userDict[userId][0]
                orgType = userDict[userId][1]
                buyerEmail = userDict[buyerId][0]
                expirationDate = licenseDict[licenseId][2]
                licenseKey = licenseDict[licenseId][1]
                data.append((email, orgType, buyerEmail, expirationDate.strftime('%Y-%m-%d'), licenseKey))

            excelData = sorted(data, key=lambda item: item[3])

            memFile = cStringIO.StringIO()
            workbook = xlsxwriter.Workbook(memFile)
            worksheet = workbook.add_worksheet('Expiring Socrative Pro')

            worksheet.write(0, 0, 'Account Email')
            worksheet.write(0, 1, 'Organization Type')
            worksheet.write(0, 2, 'Buyer Email')
            worksheet.write(0, 3, 'Expiration Date')
            worksheet.write(0, 4, 'License Key')

            i = 1
            for user in excelData:
                j = 0
                for val in user:
                    worksheet.write(i, j, val)
                    j += 1
                i += 1

            workbook.close()
            memFile.seek(0, 0)

            start_date = start_date.strftime("%Y-%m-%d") if start_date else None
            end_date = end_date.strftime("%Y-%m-%d") if end_date else None
            email = EmailMessage("Expiring PRO accounts",
                                 "Hi,<br>I've attached the xlsx file with the report for the period %s - %s .\
                                 <br>Have a nice day,<br>Yours truly,<br>The cron job" % (
                                 start_date or excelData[0][3],
                                 end_date or excelData[-1][3]),
                                 'noreply@socrative.com',
                                 receiver_list)
            email.encoding = "utf-8"
            email.content_subtype = "html"
            email.attach("PRO_Expiring.xlsx", memFile.read(),
                         mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")

            memFile.close()
            self.emailClient.sendEmail(email)
            statsd.histogram(WorkerCounters.EXPIRING_ACCOUNTS, time.time()-start)
        except Exception as e:
            logger.error(exceptionStack(e))
            statsd.increment(WorkerCounters.EXPIRING_ACCOUNTS_FAIL)

    def __filterLastLogins(self, userDict, tmpDict):
        """

        :param userDict:
        :param tmpDict:
        :return:
        """
        # filter users
        for user_id in tmpDict:
            if user_id in userDict:
                userDict[user_id] = max(userDict[user_id], tmpDict[user_id])
            else:
                userDict[user_id] = tmpDict[user_id]

    def send_last_logins_report(self, itemObj):
        """
        send the pro accounts expiring by a specific date
        :param itemObj:
        :return:
        """

        if not itemObj.get('emails'):
            logger.error("Empty or invalid list of recipients")
            return

        if not itemObj.get('start_date'):
            logger.error("Received bad start date")
            return

        start = time.time()
        try:

            start_date = self.parse_accounting_date(itemObj.get('start_date'))
            end_date = self.parse_accounting_date(itemObj.get('end_date')) if 'end_date' in itemObj else None

            # we leave only masteryconnect addresses as a security measure
            receiver_list = [emailAddr for emailAddr in itemObj.get("emails", []) if emailAddr.endswith("@masteryconnect.com")]
            if not receiver_list:
                logger.error("No valid emails left in the receiver's list")
                return

            org_type = itemObj.get('org_type')
            query_type = itemObj.get('output')

            if query_type.lower() not in ('count', 'email-list'):
                logger.error("No valid query type to perform: %s" % str(query_type))
                return

            # get all users that started activities between two dates
            userDict = self.daoRegistry.activityInstanceDao.getActivityOwnersDuringPeriod(start_date, end_date)

            # get all users that created quizzes between two dates
            tmpUserDict = self.daoRegistry.quizDao.getQuizOwnersForDateInterval(start_date, end_date)

            self.__filterLastLogins(userDict, tmpUserDict)

            # get all the users that created/updated folders between dates.
            tmpUserDict = self.daoRegistry.folderDao.getFolderOwnersDuringPeriod(start_date, end_date)
            self.__filterLastLogins(userDict, tmpUserDict)

            tmpUserDict = self.daoRegistry.usersDao.getLastLogins(start_date, end_date)
            self.__filterLastLogins(userDict, tmpUserDict)

            resultDict = self.daoRegistry.usersDao.getLastLoginDetails(userDict.keys(), org_type, query_type)

            if query_type == 'email-list':

                data = []
                for user_id in resultDict:
                    if user_id not in userDict:  # the user doesn't correspond to the org type filter
                        continue

                    first_name = resultDict[user_id][0]
                    last_name = resultDict[user_id][1]
                    email = resultDict[user_id][2]
                    org_type = resultDict[user_id][3]
                    last_login = userDict[user_id]
                    data.append((first_name, last_name, email, org_type, last_login.strftime('%Y-%m-%d %H:%M:%S')))

                excelData = sorted(data, key=lambda item: item[4])

                memFile = cStringIO.StringIO()
                workbook = xlsxwriter.Workbook(memFile)
                worksheet = workbook.add_worksheet('Last logins')

                worksheet.write(0, 0, 'First name')
                worksheet.write(0, 1, 'Last name')
                worksheet.write(0, 2, 'Email')
                worksheet.write(0, 3, 'Organization Type')
                worksheet.write(0, 4, 'Last login date')

                i = 1
                for user in excelData:
                    j = 0
                    for val in user:
                        worksheet.write(i, j, val)
                        j += 1
                    i += 1

                workbook.close()
                memFile.seek(0, 0)

                start_date = start_date.strftime("%Y-%m-%d") if start_date else None
                end_date = end_date.strftime("%Y-%m-%d") if end_date else None
                email = EmailMessage("Last logins",
                                     "Hi,<br>I've attached the xlsx file with the account list for the period %s - %s .\
                                     <br>Have a nice day,<br>Yours truly,<br>The cron job" % (
                                     start_date or excelData[0][4],
                                     end_date or excelData[-1][4]),
                                     'noreply@socrative.com',
                                     receiver_list)
                email.encoding = "utf-8"
                email.content_subtype = "html"
                email.attach("Last_Logins.xlsx", memFile.read(),
                             mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")

                memFile.close()
            else:
                email = EmailMessage("Last logins count",
                                     "Hi, <br> The number of accounts that logged in from %s till %s : %d" %
                                     (start_date.strftime('%Y-%m-%d'),
                                      end_date.strftime('%Y-%m-%d') if end_date else datetime.datetime.today().strftime('%Y-%m-%d'),
                                      resultDict['count']),
                                     'noreply@socrative.com',
                                     receiver_list)
                email.encoding = "utf-8"
                email.content_subtype = "html"
            self.emailClient.sendEmail(email)
            statsd.histogram(WorkerCounters.LAST_LOGINS, time.time()-start)
        except Exception as e:
            logger.error(exceptionStack(e))
            statsd.increment(WorkerCounters.LAST_LOGINS_FAIL)

    def send_multi_seats_receipt(self, itemDict):
        """

        :param itemDict:
        :return:
        """
        start = time.time()

        try:
            user = self.daoRegistry.usersDao.loadUserByEmail(itemDict["email_address"])
        except BaseDao.NotFound:
            logger.error("User with email %s is not found" % itemDict["email_address"])
            return

        try:
            trans_real.activate(user.language or "en")

            tmp, license_id, years, license_key, expiration_date = self.daoRegistry.licenseDao.getLicenseData(user.id)
            stripeData = ujson.loads(tmp) if tmp else {}

            seats = self.daoRegistry.licenseDao.getSeats(license_id)

            if seats <= 1:
                logger.error("This email is for buyers that acquired licenses with multiple seats")
                return

            if not stripeData:
                logger.error("No pro payment info for user %s" % user.email)
                return

            data = {}
            data["expiration_date"] = expiration_date.strftime('%B %d, %Y')
            data["name"] = "%s, %s" % ('' if user.last_name is None else user.last_name,
                                       '' if user.first_name is None else user.first_name)
            data["seats"] = seats
            data["license_key"] = license_key.upper()

            subject = ugettext('Getting Started with Socrative PRO')
            body = self.multi_pro_buyer.render(Context(data))

            email = EmailMessage(subject, body, 'noreply@socrative.com', [user.email],
                                 headers={'From': 'Socrative <noreply@socrative.com>'})
            email.content_subtype = "html"

            self.emailClient.sendEmail(email)

            trans_real.deactivate()

            statsd.histogram(WorkerCounters.ONBOARD_PACKAGE, time.time()-start)
        except Exception as e:
            logger.error(exceptionStack(e))
            statsd.increment(WorkerCounters.ONBOARD_PACKAGE_FAIL)