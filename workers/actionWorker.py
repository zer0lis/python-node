# coding=utf-8
import datetime
import logging
import random
import string
import sys
import time
import os
import ujson
import urllib
from multiprocessing import Process
from multiprocessing.queues import Empty
from datadog import statsd, initialize

import requests
from boto.s3.connection import S3Connection

from common.dao import ActivityInstanceModel, PartnerModel
from quizzes.dao import QuestionModel
from common.dao import BaseDao
from common.socrative_api import exceptionStack, safeToInt
from common.socrative_config import ConfigRegistry
from workers.serializers import EmailReportDTO
from workers.task_types import ActionTypes
from workers.task_types import EmailTypes
from workers.task_types import TaskType
from workers.counters import WorkerCounters

logger = logging.getLogger(__name__)


class ActionWorker(Process):
    """
    worker for resizing images
    """
    # x- xlsx, q-question pdf, s - students zip
    FORMATS = ["X", "Q", "S"]

    _BOUNDARY_CHARS = string.digits + string.ascii_letters

    def __init__(self, redisClient, processQueue, inQueue, daoRegistry):
        """
        :param redisClient:
        :param processQueue:
        :return:
        """

        super(ActionWorker, self).__init__()

        self.redisClient = redisClient
        self.queue = processQueue
        self.inQueue = inQueue
        self.daoRegistry = daoRegistry

        self.botoConnection = None
        self.s3Bucket = None

        self.maxProcessingSeconds = safeToInt(ConfigRegistry.getItem("MAX_ALLOWED_SECONDS_FOR_REPORTS_IN_PENDING")) or 1000

        self.ADMIN_MARKETING_URL = ConfigRegistry.getItem("ADMIN_MARKETING_URL")

        logger.info("started with max allowed seconds %d" % self.maxProcessingSeconds)

        self.googleClient = None

    def run(self):
        """
        overwrote run method
        :return:
        """

        try:
            self.botoConnection = S3Connection(ConfigRegistry().getItem("AWS_ACCESS_KEY_ID"),
                                               ConfigRegistry().getItem("AWS_SECRET_ACCESS_KEY"))
            self.s3Bucket = self.botoConnection.get_bucket(ConfigRegistry().getItem("AWS_STORAGE_BUCKET_NAME"))

        except Exception as e:
            logger.error(exceptionStack(e))

        initialize(api_key=os.environ["DATADOG_API_KEY"], app_key=os.environ["DATADOG_APP_KEY"])
        self.googleClient = requests.session()

        while True:
            try:
                item = None
                try:
                    item = self.queue.get_nowait()
                except Empty:
                    logger.debug("empty queue, worker didn't received the poison pill")

                if item == "stop":
                    logger.info("ActionWorker:received the poison pill.Exiting")
                    break

                item = self.redisClient.get_task(TaskType.REPORT_ACTIONS)

                if item is None:
                    time.sleep(1)
                    continue

                itemDict = ujson.loads(item)

                activity = self.daoRegistry.activityInstanceDao.getActivityById(itemDict["id"])
                if activity.activity_type == ActivityInstanceModel.SINGLE_QUESTION:
                    question_type, q_text = self.daoRegistry.questionDao.getQuestionTypeForQuiz(activity.activity_id)
                    if question_type == QuestionModel.FREE_RESPONSE:
                        continue
                    if q_text.startswith("(Vote) ") is False and question_type == QuestionModel.MULTIPLE_CHOICE:
                        continue
                logger.info("activity id -> %d and status %d", activity.id, activity.report_status)
                if activity.report_status == ActivityInstanceModel.REPORT_DONE:
                    action = itemDict.get("action")
                    if action == ActionTypes.UPLOAD_TO_GOOGLE_DRIVE:
                        self.upload_to_drive(itemDict, activity)
                    elif action == ActionTypes.SEND_VIA_EMAIL:
                        self.send_email(itemDict, activity.started_by_id)
                        logger.info("Added send email action")
                    else:
                        logger.error("unknown action type : %s" % str(action))
                elif activity.report_status == ActivityInstanceModel.REPORT_IN_PROGRESS:
                    if "timestamp" in itemDict and (time.time() - itemDict["timestamp"]) > self.maxProcessingSeconds:
                        logger.error("the report for the activity %d can't be generated.It spent too much time in the"""
                                     " queue, reverting status to NOT DONE" % activity.id)
                        self.daoRegistry.activityInstanceDao.updateReportStatus(activity.id,
                                                                                ActivityInstanceModel.REPORT_NOT_DONE)
                        continue
                    elif "timestamp" not in itemDict:
                        itemDict["timestamp"] = time.time()

                    self.redisClient.add_tasks(TaskType.REPORT_ACTIONS, [itemDict])
                    logger.info("task %s added back to processing queue in redis" % str(itemDict))

                elif activity.report_status == ActivityInstanceModel.REPORT_NOT_DONE:
                    logger.info("The report for activity %d is not done. Adding task in redis to generate the reports",
                                activity.id)
                    reportQueue = TaskType.GENERATE_REPORTS if itemDict.get('is_pro') is False else TaskType.GENERATE_PRO_REPORTS
                    self.redisClient.add_tasks_to_set(reportQueue, [{"id": activity.id,
                                                              "user_id": activity.started_by_id,
                                                              "formats": "*"}])
                    logger.info("Readding the task in redis to deliver the report after it's done")
                    self.redisClient.add_tasks(TaskType.REPORT_ACTIONS, [itemDict])
                elif activity.report_status == ActivityInstanceModel.REPORT_IGNORE:
                    logger.info("ignoring report for activity %d because it's not possible to create it" % activity.id)
                    continue
                else:
                    logger.error("Unknown report status. Check the database field for activity %d" % activity.id)

            except OSError as e:
                logger.error(exceptionStack(e))
                try:
                    logger.warn("preparing for restarting worker")
                    self.inQueue.put("STOP")
                    sys.exit(1)
                except Exception as e:
                    logger.error(exceptionStack(e))

            except Exception as e:
                logger.error(exceptionStack(e))
                time.sleep(1)
            finally:
                # avoid throttling the cpu
                time.sleep(0.1)

    def encode_multipart(self, fields, fileContent, boundary=None, mimetype=None):
        """
        encode multipart a json body plus file inside an http response
        :param fields
        :param fileContent
        :param boundary
        :param mimetype
        :return tuple
        """

        if boundary is None:
            boundary = ''.join(random.choice(self._BOUNDARY_CHARS) for _ in range(30))
        lines = []

        lines.extend((
            '--{0}'.format(boundary),
            'Content-Type: application/json; charset=UTF-8',
            '',
            ujson.dumps(fields),
        ))

        mimetype = mimetype
        lines.extend((
            '--{0}'.format(boundary),
            'Content-Type: {0}'.format(mimetype),
            '',
            fileContent,
        ))

        lines.extend((
            '--{0}--'.format(boundary),
            '',
        ))
        body = '\r\n'.join(lines)

        headers = {
            'Content-Type': 'multipart/related; boundary={0}'.format(boundary),
            'Content-Length': str(len(body)),
        }

        return body, headers

    def readdActionTask(self, itemDict):
        """

        :param itemDict:
        :return:
        """
        if "timestamp" not in itemDict:
            itemDict["timestamp"] = time.time()

        if time.time() - itemDict["timestamp"] < self.maxProcessingSeconds:
            self.redisClient.add_tasks(TaskType.REPORT_ACTIONS, [itemDict])

    def __getFileFromS3(self, activityId, filename):
        """

        :param activityId:
        :param filename:
        :return:
        """
        k = self.s3Bucket.get_key("reports/%d/" % activityId + filename)
        if k:
            return k

        for k in self.s3Bucket.list(prefix="reports/%d/" % activityId):
            if k.name[-6:] == filename[-6:]:
                return k

        return None

    def upload_to_drive(self, itemDict, activity):
        """

        :param itemDict:
        :param activity:
        :return:
        """
        user = None
        dt = time.time()
        try:
            userId = activity.started_by_id
            filename = itemDict.get("filename").encode('utf-8')
            mimetype = itemDict.get("mimetype").encode('utf-8')

            logger.error("Upload to drive started : File to be uploaded to GD : %s for user_id %s" % (filename, userId))

            # get token
            partnerObj = self.daoRegistry.partnerDao.getPartner(userId, PartnerModel.GOOGLE)
            oAuthDict = ujson.loads(partnerObj.data)

            # if it expired try to refresh it?
            tokenExpiration = oAuthDict.get("expires")
            if tokenExpiration is None or type(tokenExpiration) not in (float, long, int):
                expired = True
            else:
                expired = datetime.datetime.fromtimestamp(tokenExpiration) < datetime.datetime.utcnow()

            if oAuthDict and expired:
                # get the refresh token and get a new access token
                if oAuthDict.get("refresh_token", '') == '':
                    raise Exception("the refresh token is invalid. Can't upload to drive")
                else:
                    # refresh the token
                    postData = "client_id=%s&client_secret=%s&refresh_token=%s&grant_type=refresh_token" %\
                               (ConfigRegistry.getItem("GOOGLE_OAUTH_CLIENT_ID"),
                                ConfigRegistry.getItem("GOOGLE_OAUTH_CLIENT_SECRET"),
                                oAuthDict.get("refresh_token"))
                    resp = self.googleClient.post(ConfigRegistry.getItem("GOOGLE_OAUTH_REFRESH_TOKEN_URI"),
                                                  data=postData,
                                                  headers={'Content-Type': 'application/x-www-form-urlencoded'},
                                                  verify=False)
                    if resp.status_code != 200:
                        if resp.status_code >= 500:
                            # internal error on google side. add the task back into the action queue in redis
                            self.readdActionTask(itemDict)
                        elif resp.status_code >= 400:
                            self.daoRegistry.partnerDao.updateData(partnerObj.id, '{}')
                        raise Exception(resp.content)

                    newData = ujson.loads(resp.content)
                    oAuthDict["access_token"] = newData["access_token"]
                    oAuthDict["expires"] = datetime.datetime.utcnow() + datetime.timedelta(seconds=newData["expires_in"])
                    partnerObj.data = ujson.dumps(oAuthDict)
                    self.daoRegistry.partnerDao.updateData(partnerObj.id, partnerObj.data)

            accessToken = oAuthDict.get("access_token")

            # check if the file and folder exists
            folderId = oAuthDict.get('folder_id')
            if folderId is not None:
                resp = self.googleClient.get(ConfigRegistry.getItem("GOOGLE_DRIVE_URI_UPLOAD_FOLDER")+"/%s" % folderId,
                                             headers={'Authorization': "Bearer " + accessToken})
                if resp.status_code != 200 and resp.status_code != 404:
                    if resp.status_code >= 500:
                        self.readdActionTask(itemDict)
                    raise Exception(resp.content)

                if resp.status_code == 200:
                    respData = ujson.loads(resp.content)
                    folder_name = respData.get("title")

                    if folder_name != "Socrative Reports" or respData.get("explicitlyTrashed") is True:
                        oAuthDict["folder_id"] = None
                        folderId = None
                elif resp.status_code == 404:
                    oAuthDict["folder_id"] = None
                    folderId = None

            if folderId is None:
                # create the socrative_reports folder
                jsonData = {"description": "socrative activity reports folder",
                            "mimeType": "application/vnd.google-apps.folder",
                            "title": "Socrative Reports"}

                resp = self.googleClient.post(ConfigRegistry.getItem("GOOGLE_DRIVE_URI_UPLOAD_FOLDER"),
                                              data=ujson.dumps(jsonData),
                                              headers={
                                                  'Authorization': "Bearer " + accessToken,
                                                  'Content-Type': 'application/json; charset=UTF-8',
                                              })
                if resp.status_code != 200:
                    if resp.status_code >= 500:
                        self.readdActionTask(itemDict)
                    raise Exception(resp.content)

                resp = ujson.loads(resp.content)
                if resp.get("id") is None:
                    raise Exception("couldn't create the folder on google drive for user %s" % user.email)
                else:
                    oAuthDict["folder_id"] = resp.get("id")
                    partnerObj.data = ujson.dumps(oAuthDict)
                    self.daoRegistry.partnerDao.updateData(partnerObj.id, partnerObj.data)

                    # save the socrative folder id

            title = urllib.quote("title=\'" + filename + "\' and trashed=false")
            query = "q="+title+"&fields=items"

            resp = self.googleClient.get(ConfigRegistry.getItem("GOOGLE_DRIVE_URL_LIST_FILES")+"?" + query,
                                         headers={'Authorization': "Bearer " + accessToken})

            if resp.status_code != 200:
                if resp.status_code >= 500:
                    self.readdActionTask(itemDict)
                raise Exception(resp.content)

            resp = ujson.loads(resp.content)

            if resp.get("items") is None or len(resp.get("items")) == 0:
                # the file was not found we can add it to google drive

                if self.s3Bucket is None:
                    if self.botoConnection is None:
                        self.botoConnection = S3Connection(ConfigRegistry().getItem("AWS_ACCESS_KEY_ID"),
                                                           ConfigRegistry().getItem("AWS_SECRET_ACCESS_KEY"))
                        self.s3Bucket = self.botoConnection.get_bucket(ConfigRegistry().getItem("AWS_STORAGE_BUCKET_NAME"))

                k = self.__getFileFromS3(activity.id, filename)
                if k is None:
                    logger.info("reports/%d/" % activity.id + filename + " not found on s3,adding it back to the "\
                                                                          "reports queue")
                    self.daoRegistry.activityInstanceDao.updateReportStatus(activity.id,
                                                                            ActivityInstanceModel.REPORT_NOT_DONE)
                    self.redisClient.add_tasks(TaskType.REPORT_ACTIONS, [itemDict])
                    statsd.increment(WorkerCounters.REPORTS_MISSING)
                    return

                payload = k.get_contents_as_string()

                fields = {"description": "socrative activity report",
                          "mimeType": mimetype,
                          "title": filename.encode('utf-8') if type(filename) is unicode else filename,
                          "parents": [{'id': oAuthDict["folder_id"].encode('utf-8')}]}

                data, headers = self.encode_multipart(fields, payload, mimetype=mimetype)

                headers['Authorization'] = "Bearer " + accessToken

                xlsx_mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                resp = self.googleClient.post(ConfigRegistry.getItem("GOOGLE_DRIVE_URI_UPLOAD_FILE") +
                                              "&fields=selfLink&convert=%s" % ("true" if mimetype == xlsx_mimetype else "false"),
                                              data=data, headers=headers)

                if resp.status_code != 200:
                    if resp.status_code >= 500:
                        self.readdActionTask(itemDict)
                    raise Exception(resp.content)
                logger.info("upload to google drive for %s is done" % filename)
                duration = time.time() - dt
                statsd.histogram(WorkerCounters.REPORTS_TO_GOOGLE_DRIVE, duration)
        except BaseDao.NotFound:
            logger.warn("user %s is not logged via google oauth, but it accessed upload to drive", user.email)
            statsd.increment(WorkerCounters.REPORTS_TO_GOOGLE_DRIVE_FAIL)
            return
        except Exception as e:
            logger.error(exceptionStack(e))
            statsd.increment(WorkerCounters.REPORTS_TO_GOOGLE_DRIVE_FAIL)

    def send_email(self, itemDict, userId):
        """

        :param itemDict:
        :param userId:
        :return:
        """
        itemDict["email_type"] = EmailTypes.REPORTS
        itemDict["user_id"] = userId

        itemDct = EmailReportDTO.fromSocrativeDict(itemDict)
        self.redisClient.add_tasks(TaskType.SEND_EMAIL, [itemDct])

