# coding=utf-8

from common.base_cache import BaseCache
from common.socrative_api import exceptionStack
import ujson
import logging


logger = logging.getLogger(__name__)


class WorkerTasks(BaseCache):
    """

    """

    def __init__(self, redisPool):
        """

        :param redisPool:
        :return:
        """

        super(WorkerTasks, self).__init__(redisPool)

    def add_tasks(self, key, data):
        """
        :param data:
        :param key
        :return:
        """

        if type(data) is not list:
            raise TypeError("data must be a list of dictionaries")

        if not data:
            return

        try:
            values = [ujson.dumps(item) for item in data]
            self.getConnection(False).rpush(key, *values)
        except Exception as e:
            logger.error(exceptionStack(e))

    def add_tasks_to_set(self, key, data):
        """

        :param key:
        :param data:
        :return:
        """
        if type(data) is not list:
            raise TypeError("data must be a list of dictionaries")

        if not data:
            return

        try:
            values = []
            for item in data:
                values += [ujson.dumps(item), 1]  # append the item and the score
            self.getConnection(False).zadd(key, *values)
        except Exception as e:
            logger.error(exceptionStack(e))

    def get_task(self, key):
        """

        :param key:
        :return:
        """
        try:
            # we need a writable redis instance because lpop modifies the list
            return self.getConnection(False).lpop(key)
        except Exception as e:
            logger.error(exceptionStack(e))

    def get_task_from_set(self, key):
        """

        :param key:
        :return:
        """

        try:
            # we use a pipeline to make the operations atomic , sorted sets don't have a pop equivalent
            with self.getConnection(False).pipeline() as pipe:
                resp = pipe.zrange(key, 0, 0).zremrangebyrank(key, 0, 0).execute()[0]

            if resp:
                return resp[0]  # zrange returns an array, we just need the first and only element
        except Exception as e:
            logger.error(exceptionStack(e))

    def set_password_token(self, token, teacherId):
        """

        :param token: The password token
        :param teacherId: The id of the teacher
        :return:
        """

        try:
            self.getConnection(False).setex("password_token:" + token, teacherId, 60 * 60 * 2)  # Expire in 2 hours.
        except Exception as e:
            logger.error(exceptionStack(e))