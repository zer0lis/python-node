# coding=utf-8
from django.db import models
from quizzes.models import Question, Answer

# The name the student submitted at the beginning of a Quiz or Activity
class StudentName(models.Model):
    """
    StudentName model structure to be saved into the database
    """
    name = models.CharField(max_length=128)
    user_uuid = models.CharField(max_length=40)
    activity_instance = models.ForeignKey('common.ActivityInstance', related_name='student_names')


class StudentResponse(models.Model):
    """
    StudentResponse model structure to be saved into the database
    """
    class Meta:
        verbose_name_plural = "Student Responses"

    legacy_id = models.IntegerField(blank=True, null=True)

    # which user made the response
    user_uuid = models.CharField(max_length=40)

    # copied here from the question -> answer for easy lookup
    is_correct = models.NullBooleanField()

    activity_instance = models.ForeignKey('common.ActivityInstance', related_name="responses")

    # response for this question
    question = models.ForeignKey(Question, related_name="responses")

    team = models.IntegerField(default=-1)

    hidden = models.BooleanField(default=False)

    # need to add a response_time field


class StudentResponses(models.Model):
    """
    StudentResponse model structure to be saved into the database
    """
    class Meta:
        db_table = "student_responses"

    id = models.BigAutoField(db_index=True, serialize=False, auto_created=True, primary_key=True)

    # which user made the response
    user_uuid = models.CharField(max_length=40)

    # copied here from the question -> answer for easy lookup
    is_correct = models.NullBooleanField()

    activity_instance = models.ForeignKey('common.ActivityInstance')

    # response for this question
    question = models.ForeignKey(Question)

    team = models.IntegerField(default=-1)

    hidden = models.BooleanField(default=False)

    legacy_id = models.IntegerField(blank=True, null=True)


class StudentResponseTextAnswer(models.Model):
    """
    Student response text answer structure to be saved into the database
    """
    answer_text = models.TextField(max_length=65536)
    student_response = models.ForeignKey(StudentResponse, related_name="text_answers")

class StudentResponsesTextAnswer(models.Model):
    """
    Student response text answer structure to be saved into the database
    """
    class Meta:
        db_table = "student_responses_text_answer"

    id = models.BigAutoField(db_index=True, serialize=False, auto_created=True, primary_key=True)
    answer_text = models.TextField(max_length=65536)
    student_response = models.ForeignKey(StudentResponse)


class StudentResponseAnswerSelection(models.Model):
    """
    Student response for MC & TF questions
    """
    answer_id = models.IntegerField()
    student_response = models.ForeignKey(StudentResponse, related_name="selection_answers")


class StudentResponsesAnswerSelection(models.Model):
    """
    Student response for MC & TF questions
    """
    class Meta:
        db_table = "student_responses_answer_selection"

    id = models.BigAutoField(db_index=True, serialize=False, auto_created=True, primary_key=True)
    answer_id = models.IntegerField()
    student_response = models.ForeignKey(StudentResponse)


class Student(models.Model):
    """
    Student model structure to be saved into the database
    """

    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=64, null=False)
    student_id = models.CharField(max_length=32, null=False)
    user_uuid = models.CharField(max_length=40, null=False)
    sync_id = models.IntegerField(null=True)
    status = models.IntegerField(null=False, default=0)
    rosters = models.ManyToManyField("rooms.Roster")


class ActivityStudent(models.Model):
    """
    Student activity data
    """
    activity_instance = models.ForeignKey("common.ActivityInstance", db_constraint=True, db_index=True)
    student = models.ForeignKey(Student, db_constraint=True, db_index=True)
    status = models.IntegerField(null=False, default=0)
    user_uuid = models.TextField(max_length=128, null=True)
    student_username = models.CharField(max_length=32, null=True)
    first_name = models.CharField(max_length=64, null=True)
    last_name = models.CharField(max_length=64, null=True)
    old_type = models.NullBooleanField(null=True)
    team = models.IntegerField(null=True)