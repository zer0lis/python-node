# -*- coding: utf-8 -*-
from common import http_status as status
from common import soc_constants
from common.socrative_errors import BaseError
from common.socrative_api import exceptionStack, isStringNotEmpty, safeToInt, safeToBool, htmlEscape, htmlUnescape
from common.base_services import BaseService
from common.base_dao import BaseDao
from common.socrative_config import ConfigRegistry
from common.soc_constants import SHOW_RESULTS
from socrative_tornado.publish import send_model
from rooms.dao import RoomModel
from students.dao import StudentModel
from common.cryptography import Cryptography
from socrative import settings as projSettings

from students.serializers import StudentViewableResponse, SubmitStudentResponseRequestDTO
from students.dao import StudentResponseAnswerSelectionModel, StudentResponseTextAnswerModel, StudentResponseModel
from common.dao import ActivityInstanceModel

import ujson
import pytz
import datetime
import logging

logger = logging.getLogger(__name__)


class StudentServices(BaseService):
    """
    class that holds methods for the student services
    """

    def __init__(self, daoRegistry, cacheRegistry):
        """
        Constructor
        :return:
        """
        super(StudentServices, self).__init__(daoRegistry, cacheRegistry)

        self.NEW_SCHOOL_YEAR = 2016
        self.NEW_SCHOOL_MONTH = 8
        self.NEW_SCHOOL_DAY = 1
        self.MIN_STUDENT_RESPONSE_ID = 0
        self.MIN_STUDENT_NAME_ID = 0

    def after_properties_set(self):
        """

        :return:
        """

        self.NEW_SCHOOL_YEAR = ConfigRegistry.getItem("NEW_SCHOOL_YEAR")
        self.NEW_SCHOOL_MONTH = ConfigRegistry.getItem("NEW_SCHOOL_MONTH")
        self.NEW_SCHOOL_DAY = ConfigRegistry.getItem("NEW_SCHOOL_DAY")
        self.MIN_STUDENT_RESPONSE_ID = ConfigRegistry.getItem("MIN_STUDENT_RESPONSE_ID")
        self.MIN_STUDENT_NAME_ID = ConfigRegistry.getItem("MIN_STUDENT_NAME_ID")

    def is_answer_correct(self, **kwargs):
        """
        checks if the answer is correct
        :params kwargs: dictionary of arguments sent by the caller
        """
        isCorrect = None
        question = kwargs.get('question')

        correctAnswers = self.daoRegistry.answerDao.getAnswerModels(question.question_id, True)
        if correctAnswers:

            uniqueAnswerIds = kwargs.get("uniqueAnswerIds", [])
            answerText = kwargs.get("answerText", None)

            if answerText is not None:
                isCorrect = False

                trimedAnswerText = answerText.lower().strip()

                for correctAnswer in correctAnswers:
                    correctAnswer.text = htmlUnescape(correctAnswer.text)
                    if correctAnswer.text.lower().strip() == trimedAnswerText:
                        isCorrect = True
                        break

            else:  # check if they missed any correct answers

                # get the ids for each of the correct answers of this question
                correctAnswerIds = set([a.id for a in correctAnswers])

                if len(uniqueAnswerIds) > 0:
                    # diff of student answers and correct answers will be len(0) if correct
                    diff1 = len(correctAnswerIds - uniqueAnswerIds)
                    diff2 = len(uniqueAnswerIds - correctAnswerIds)
                    isCorrect = diff1 == 0 and diff2 == 0

                # if there are correct answers and none were provided, question is wrong
                elif len(correctAnswerIds) > 0:
                    isCorrect = False

        return isCorrect, correctAnswers

    def hideResponse(self, request, dto):
        """
        """

        try:

            self.daoRegistry.studentResponseDao.hideResponse(dto.response_id)

            return {}, status.HTTP_200_OK

        except BaseDao.NotUpdated as e:
            logger.debug(exceptionStack(e))
            return BaseError.INVALID_RESPONSE_ID, status.HTTP_400_BAD_REQUEST

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def findStudentResponses(self, request, dto):
        """
        """

        try:
            responses = self.daoRegistry.studentResponseDao.getResponses(dto.user_uuid, dto.activity_instance_id)

            responseIds = dict()
            for i in range(len(responses)):
                responseIds[responses[i].get("id")] = i

            if responseIds.keys():
                responseSelections = self.daoRegistry.studentResponseAnswerSelectionDao.getSelectionByResponseIds(responseIds.keys())
                for responseSelection in responseSelections:
                    index = responseIds.get(responseSelection[StudentResponseAnswerSelectionModel.STUDENT_RESPONSE_ID])
                    if "selection_answers" not in responses[index]:
                        responses[index]["selection_answers"] = [responseSelection]
                    else:
                        responses[index]["selection_answers"].append(responseSelection)

                responseTexts = self.daoRegistry.studentResponseTextAnswerDao.getTextAnswersByResponseIds(responseIds.keys())
                for responseText in responseTexts:
                    index = responseIds.get(responseText[StudentResponseTextAnswerModel.STUDENT_RESPONSE_ID])
                    if "text_answers" not in responses[index]:
                        responses[index]["text_answers"] = [responseText]
                    else:
                        responses[index]["text_answers"].append(responseText)

            responsesData = list()

            for response in responses:
                responsesData.append(StudentViewableResponse.fromSocrativeDict(response))

            self.cacheRegistry.studentFindResponseCache.cacheData(dto.activity_instance_id, dto.user_uuid,
                                                                  ujson.dumps(responsesData))

            return responsesData, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def __getActivitySettings(self, activity_instance_id):
        """

        :param activity_instance_id:
        :return:
        """
        try:
            settings_dict = self.cacheRegistry.activitySettingsCache.get(activity_instance_id)

            if not settings_dict:
                settings_dict = self.daoRegistry.activitySettingsDao.getAllSettings(activity_instance_id)

            return settings_dict
        except Exception as e:
            logger.error(exceptionStack(e))
            raise

    def submitStudentResponse(self, request, dto):
        """
        :param request
        :param dto
        :type dto: SubmitStudentResponseRequestDTO
        """
        try:
            dto.team = dto.team if dto.team is not None else -1

            if dto.answer_ids != "":
                uniqueAnswerIds = set([safeToInt(_id) for _id in dto.answer_ids.split(",") if safeToInt(_id) is not None])
            else:
                uniqueAnswerIds = []

            question = self.daoRegistry.questionDao.getQuestionModelById(dto.question_id)

            if not question:
                logger.warn("Error question_id (%s) not found", dto.question_id)
                return BaseError.INVALID_QUESTION_ID, status.HTTP_400_BAD_REQUEST

            activityInstance = self.daoRegistry.activityInstanceDao.getActivityById(dto.activity_instance_id,
                                                                                    _type=BaseDao.TO_MODEL)

            if not activityInstance:
                logger.debug("error activity_instance not (%s) not found", dto.activity_instance_id)
                return BaseError.INVALID_ACTIVITY_ID, status.HTTP_400_BAD_REQUEST

            if activityInstance.end_time is not None:
                return BaseError.ACTIVITY_HAS_ENDED, status.HTTP_400_BAD_REQUEST

            activityDate = activityInstance.start_time.date()
            augFirst = datetime.date(self.NEW_SCHOOL_YEAR, self.NEW_SCHOOL_MONTH, self.NEW_SCHOOL_DAY)
            if augFirst <= activityDate:
                response_min_id = self.MIN_STUDENT_RESPONSE_ID
                student_name_min_id = self.MIN_STUDENT_NAME_ID
            else:
                response_min_id = None
                student_name_min_id = None

            settingsDict = self.__getActivitySettings(activityInstance.id)

            name_required = safeToBool(settingsDict.get("require_names"))
            duration = safeToInt(settingsDict.get("seconds"))
            show_feedback = safeToBool(settingsDict.get("show_feedback"))

            if duration is not None and duration > 0:
                now = pytz.utc.localize(datetime.datetime.utcnow())
                if (now-activityInstance.start_time).total_seconds() > duration:
                    return BaseError.ACTIVITY_EXPIRED, status.HTTP_400_BAD_REQUEST

            room = self.serviceRegistry.roomService.validateRoom(activityInstance.room_name)
            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if name_required:
                checkStudentName = self.daoRegistry.studentNameDao.checkNameExist(dto.user_uuid,
                                                                                  dto.activity_instance_id,
                                                                                  minId=student_name_min_id)
                if checkStudentName is False:
                    # try to get it from the roster
                    if room.status & RoomModel.ROSTERED == RoomModel.ROSTERED:
                        first_name, last_name = self.daoRegistry.studentDao.getStudentName(dto.activity_instance_id,
                                                                                           dto.user_uuid)
                        if not first_name or not last_name:
                            # if it wasn't set before and the room is rostered , set the activity, update student status
                            # and try again

                            self.daoRegistry.studentActivityDao.insert(dto.user_uuid,
                                                                       room.name_lower,
                                                                       dto.activity_instance_id)
                            self.daoRegistry.studentDao.updateStatus(dto.user_uuid, room.name_lower,
                                                                     StudentModel.LOGGED_IN)
                            first_name, last_name = self.daoRegistry.studentDao.getStudentName(dto.activity_instance_id,
                                                                                                   dto.user_uuid)
                        if first_name and last_name:
                            self.daoRegistry.studentNameDao.upsert("%s,%s" % (last_name, first_name),
                                                                   dto.user_uuid,
                                                                   dto.activity_instance_id)
                        else:
                            return BaseError.STUDENT_NAME_MISSING, status.HTTP_400_BAD_REQUEST
                    else:
                        return BaseError.STUDENT_NAME_MISSING, status.HTTP_400_BAD_REQUEST

                if activityInstance.activity_type == ActivityInstanceModel.SPACE_RACE and \
                    room.status & RoomModel.ROSTERED == RoomModel.ROSTERED:
                    team = self.daoRegistry.studentActivityDao.updateTeam(dto.activity_instance_id,
                                                                          dto.user_uuid,
                                                                          dto.team)
                    dto.team = team if team is not None else -1
            # Figure out if this answer is correct
            isCorrect, correctAnswers = self.is_answer_correct(
                question=question,
                uniqueAnswerIds=uniqueAnswerIds,
                answerText=dto.answer_text,
            )
            correctChanged = False

            # Create or update the student response
            responseStatus = status.HTTP_200_OK
            studentResponse = None

            if dto.multi_response:
                # short answer responses never update, they only create new responses
                studentResponse = StudentResponseModel()
                responseStatus = status.HTTP_201_CREATED
            else:
                studentResponses = self.daoRegistry.studentResponseDao.getResponseModels(activityInstance.id,
                                                                                         dto.user_uuid,
                                                                                         dto.question_id,
                                                                                         minId=response_min_id)
                if studentResponses:
                    if show_feedback is True:
                        return BaseError.QUESTION_ALREADY_RESPONDED, status.HTTP_400_BAD_REQUEST
                    responseCount = len(studentResponses)
                    if responseCount > 1:
                        idsToDelete = list()
                        for oldResponse in studentResponses[1:]:
                            idsToDelete.append(oldResponse.id)
                        self.daoRegistry.studentResponseDao.deleteResponsesByIdList(idsToDelete)

                    elif responseCount == 1:
                        studentResponse = studentResponses[0]
                    else:
                        studentResponse = StudentResponseModel()
                        responseStatus = status.HTTP_201_CREATED

                    # clear answers
                    self.daoRegistry.studentResponseAnswerSelectionDao.deleteByResponseId(studentResponse.id)
                    self.daoRegistry.studentResponseTextAnswerDao.deleteByResponseId(studentResponse.id)
                    correctChanged = isCorrect != studentResponse.is_correct
                else:
                    studentResponse = StudentResponseModel()
                    responseStatus = status.HTTP_201_CREATED

            studentResponse.user_uuid = dto.user_uuid
            studentResponse.is_correct = isCorrect
            studentResponse.activity_instance_id = activityInstance.id
            studentResponse.question_id = question.question_id
            studentResponse.team = dto.team

            if studentResponse.id is None:
                self.daoRegistry.studentResponseDao.create(studentResponse)
            elif correctChanged:
                self.daoRegistry.studentResponseDao.update(studentResponse)

            if dto.answer_text is not None:
                self.daoRegistry.studentResponseTextAnswerDao.create(dto.answer_text, studentResponse.id)

            if uniqueAnswerIds:
                self.daoRegistry.studentResponseAnswerSelectionDao.createByIdList(uniqueAnswerIds, studentResponse.id)

            answerIdCount = len(uniqueAnswerIds)

            # Hand serialized here as an optimization over the StudentResponseSerializer
            selectionAnswers = []
            textAnswers = []
            if dto.answer_text is not None:
                textAnswers = [{'answer_text': htmlEscape(dto.answer_text)}]

            if answerIdCount > 0:
                selectionAnswers = []

                for answerId in uniqueAnswerIds:
                    selectionAnswers.append({'answer_id': answerId})

            student = self.daoRegistry.studentNameDao.getFirstStudent(dto.user_uuid, activityInstance.id,
                                                                      minId=student_name_min_id)

            studentName = student.name if student else ""

            responseData = ujson.dumps({
                "id": studentResponse.id,
                "user_uuid": dto.user_uuid,
                "question": question.question_id,
                "is_correct": isCorrect,
                "activity_instance": activityInstance.id,
                "text_answers": textAnswers,
                "selection_answers": selectionAnswers,
                "team": dto.team,
                "hidden": False,
                "student_name": studentName
            })


            # send the response to just the teacher through tornado
            send_model(room.name_lower + "-teacher", key="studentResponse", data=responseData,
                       message_uuid=dto.user_uuid)

            if dto.check_activity:
                uuids = ujson.loads(activityInstance.students_finished)
                uuids = list(set(uuids + [dto.user_uuid]))
                self.daoRegistry.activityInstanceDao.updateFinishedStudents(activityInstance.id, ujson.dumps(uuids))

                # send the response to the teacher through pubnub
                send_model(room.name_lower + "-teacher", key="studentFinished", data={'user_uuid': dto.user_uuid,
                                                                                      'student_finished': True})

            self.cacheRegistry.studentFindResponseCache.invalidateData(dto.user_uuid, dto.activity_instance_id)

            if show_feedback is True:
                correctAnswerTexts = []
                for answer in correctAnswers:
                    correctAnswerTexts.append(answer.text)

                return {"is_correct": isCorrect,
                        "explanation": question.explanation,
                        "correct_answers": correctAnswerTexts
                       }, responseStatus

            return {}, responseStatus
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def setStudentName(self, request, dto):
        """
        :param request:
        :param dto:
        :return
        """
        try:
            activityInstance = self.daoRegistry.activityInstanceDao.getActivityById(dto.activity_instance_id,
                                                                                    _type=BaseDao.TO_MODEL)

            if not activityInstance:
                logger.debug("no activity found for activity_id %s", dto.activity_instance_id)
                return BaseError.INVALID_ACTIVITY_ID, status.HTTP_400_BAD_REQUEST

            room = self.serviceRegistry.roomService.validateRoom(activityInstance.room_name)
            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            send_model(room.name_lower + "-teacher", key="studentNameSet",
                       data={'user_uuid': dto.user_uuid,
                             'student_name': dto.student_name})

            self.daoRegistry.studentNameDao.upsert(dto.student_name, dto.user_uuid, activityInstance.id)

            return {}, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def autoAssignTeam(self, request, dataDict):
        """

        :param request:
        :param dataDict:
        :return:
        """

        activity_instance_id = safeToInt(dataDict.get("activity_instance_id"))

        if activity_instance_id is None:
            return BaseError.ACTIVITY_ID_MISSING, status.HTTP_400_BAD_REQUEST

        try:
            teamCount = safeToInt(self.__getActivitySettings(activity_instance_id).get("team_count"))
            nextAssignment = self.daoRegistry.activitySettingsDao.updateTeamAssignment(activity_instance_id)
            thisTeam = int(nextAssignment) - 1
            team = self.daoRegistry.studentActivityDao.updateTeam(activity_instance_id,
                                                                  dataDict["user_uuid"],
                                                                  thisTeam % teamCount)
            if team is None:
                return {"team_num": thisTeam % teamCount}, status.HTTP_200_OK

            return {"team_num": team}, status.HTTP_200_OK
        except BaseDao.NotFound:
            return BaseError.ACTIVITY_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR
        
    def studentActivityStatus(self, request, dataDict):
        """

        :param request:
        :param dataDict:
        :return:
        """

        activity_instance_id = dataDict.get("activity_instance_id")
        activity_instance_id = safeToInt(activity_instance_id) if activity_instance_id else None
        userUuid = dataDict.get("user_uuid")

        if type(activity_instance_id) not in (int, long) or activity_instance_id < 0:
            return BaseError.ACTIVITY_ID_MISSING, status.HTTP_400_BAD_REQUEST

        if not isStringNotEmpty(userUuid):
            return BaseError.USER_ID_MISSING, status.HTTP_400_BAD_REQUEST

        try:
            activity = self.daoRegistry.activityInstanceDao.getActivityById(safeToInt(activity_instance_id))
            if activity is None:
                return BaseError.ACTIVITY_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            questionIds = self.daoRegistry.studentResponseDao.getActivityStatus(userUuid, activity_instance_id,
                                                                                activity.activity_id)

            return questionIds, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def finishActivity(self, request, dataDict):
        """

        :param request:
        :param dataDict:
        :return:
        """

        studentUuid = dataDict.get("user_uuid")
        activityId = dataDict.get("activity_instance_id")
        activityId = safeToInt(activityId) if activityId else None

        if not isStringNotEmpty(studentUuid):
            return BaseError.USER_ID_MISSING, status.HTTP_400_BAD_REQUEST

        if type(activityId) not in (int, long) or activityId <= 0:
            return BaseError.INVALID_ACTIVITY_ID, status.HTTP_400_BAD_REQUEST

        try:
            activity = self.daoRegistry.activityInstanceDao.getActivityById(activityId)

            if activity is None:
                return BaseError.ACTIVITY_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if activity.end_time is not None:
                return BaseError.ACTIVITY_HAS_ENDED, status.HTTP_400_BAD_REQUEST

            room = self.serviceRegistry.roomService.validateRoom(activity.room_name)
            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            studentIds = ujson.loads(activity.students_finished) if activity.students_finished else list()
            studentIds = ujson.dumps(list(set(studentIds + [studentUuid])))

            self.daoRegistry.activityInstanceDao.updateFinishedStudents(activityId, studentIds)

            send_model(room.name_lower + "-teacher", key="studentFinished", data={'user_uuid': studentUuid,
                                                                                  'student_finished': True})

            return {}, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def getStudentScore(self, request, dtoObj):
        """

        :param request:
        :param dtoObj:
        :return:
        """

        try:

            showScore = safeToBool(self.__getActivitySettings(dtoObj.activity_instance_id).get(SHOW_RESULTS))

            if safeToBool(showScore) is False:
                return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

            activity = self.daoRegistry.activityInstanceDao.getActivityById(dtoObj.activity_instance_id)

            if activity is None:
                return BaseError.ACTIVITY_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            # returns back the number of
            total, neutral = self.daoRegistry.questionDao.getQuestionStats(activity.activity_id)
            correctNb = self.daoRegistry.studentResponseDao.getStudentCorrectResponses(dtoObj.activity_instance_id,
                                                                                       dtoObj.user_uuid)
            resp = dict()
            resp["total"] = total
            resp["neutral"] = neutral
            resp["correct"] = correctNb

            return resp, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def loginRoster(self, request, dto):
        """
        authenticates a student from a roster
        :param request:
        :param dto:
        :return:
        """

        try:
            room = self.serviceRegistry.roomService.validateRoom(dto.room_name.lower())

            if room is None:
                logger.warn("STUDENT_LOGIN_ROOM_404: %s" % dto.room_name)
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST, None

            if room.status & RoomModel.ROSTERED == RoomModel.ROSTERED:
                # the room is ok to login
                # check the student id
                studentModel = self.daoRegistry.studentDao.checkStudent(dto.student_id, room.id)

                one_attempt = False

                if studentModel and ((studentModel.status & StudentModel.LOGGED_IN != StudentModel.LOGGED_IN) or
                   (dto.force is True and studentModel.status & StudentModel.LOGGED_IN == StudentModel.LOGGED_IN)):
                    # check to see if there is any activity started
                    activity_id = self.daoRegistry.studentActivityDao.getActivityId(studentModel.id, studentModel.user_uuid)
                    data = {"kick": studentModel.user_uuid}
                    if activity_id:
                        settingsDict = self.__getActivitySettings(activity_id)
                        if safeToBool(settingsDict.get(soc_constants.ONE_ATTEMPT, False)) is True:
                            data["except"] = dto.user_uuid
                            one_attempt = True

                    ss_status = StudentModel.LOGGED_IN | (studentModel.status & StudentModel.HANDRAISE)
                    if one_attempt:
                        self.daoRegistry.studentDao.updateStatus(studentModel.user_uuid, room.name_lower, ss_status)
                        user_uuid = studentModel.user_uuid
                        resetTeam = False
                    else:
                        self.daoRegistry.studentDao.updateUuid(dto.user_uuid, studentModel.id, ss_status)
                        resetTeam = True
                        user_uuid = dto.user_uuid

                    student_username, team = self.daoRegistry.studentActivityDao.updateStatus(user_uuid,
                                                                                              activity_id,
                                                                                              ss_status,
                                                                                              room.name_lower,
                                                                                              resetTeam)

                    if dto.force is True and studentModel.status & StudentModel.LOGGED_IN == StudentModel.LOGGED_IN:
                            if studentModel.user_uuid:
                                send_model(room.name_lower + '-student',
                                           key="student",
                                           data=ujson.dumps(data))

                    cookie = None
                    if one_attempt:
                        cookie = (projSettings.STUDENT_AUTH_TOKEN,
                                  Cryptography.encryptStudentAuth(user_uuid))
                    return {
                               "first_name": studentModel.first_name,
                               "last_name": studentModel.last_name,
                               "student_id": studentModel.student_id,
                               "id": studentModel.id,
                               "handraise": studentModel.status & StudentModel.HANDRAISE == StudentModel.HANDRAISE,
                               "old_uuid": user_uuid,
                               "team_num": team
                           }, status.HTTP_200_OK, cookie
                elif studentModel:
                    jsonErr = BaseError.STUDENT_ALREADY_LOGGED_IN
                    jsonErr["error"]["first_name"] = studentModel.first_name
                    jsonErr["error"]["last_name"] = studentModel.last_name
                    logger.error("STUDENT_ALREADY_LOGGED_IN %s %s" %(room.name_lower, dto.student_id))
                    return jsonErr, status.HTTP_400_BAD_REQUEST, None
                else:
                    logger.error("STUDENT_NOT_FOUND: %s %s" % (room.name_lower, dto.student_id))
                    return BaseError.STUDENT_NOT_FOUND, status.HTTP_400_BAD_REQUEST, None
            else:
                return BaseError.ROOM_DOESNT_HAVE_ROSTER, status.HTTP_400_BAD_REQUEST, None

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR, None

    def logout(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            if dto.user_uuid and not dto.room_name:
                return BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST

            room = self.serviceRegistry.roomService.validateRoom(dto.room_name)

            if room and room.status < RoomModel.ARCHIVED and room.status & RoomModel.ROSTERED == RoomModel.ROSTERED:
                self.daoRegistry.studentDao.updateStatus(dto.user_uuid, room.name_lower, StudentModel.LOGGED_OUT)
                self.daoRegistry.studentActivityDao.logoutStudent(dto.user_uuid, room.name_lower)

            return {}, status.HTTP_200_OK
        except BaseDao.NotFound:
            return BaseError.STUDENT_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def studentHandraise(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:

            room = self.serviceRegistry.roomService.validateRoom(dto.room_name)

            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.status & RoomModel.ROSTERED != RoomModel.ROSTERED:
                return BaseError.ROOM_NOT_ROSTERED, status.HTTP_400_BAD_REQUEST

            checkStudent = self.daoRegistry.studentDao.checkStudentId(dto.id, room.id)
            if checkStudent is False:
                return BaseError.STUDENT_NOT_IN_ROSTER, status.HTTP_400_BAD_REQUEST

            if dto.active is True:
                self.daoRegistry.studentDao.updateHandraiseStatus(StudentModel.HANDRAISE, dto.id)
                # send the response to just the teacher through tornado
                send_model(room.name_lower + "-teacher", key="handraise", data=ujson.dumps({"state": "on",
                                                                                            "id": dto.id})
                           )
            else:
                self.daoRegistry.studentDao.updateHandraiseStatus(soc_constants.STUDENT_HANDRAISE_OFF,
                                                                  dto.id,
                                                                  off=True)
                send_model(room.name_lower + "-teacher",
                           key="handraise",
                           data=ujson.dumps({"state": "off",
                                             "id": dto.id})
                           )

            return {}, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR