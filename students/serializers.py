# -*- coding: utf-8 -*-

from common.base_dto import DTOConverter, ItemConverter
from common.base_dto import RequestDTOValidator, ItemValidator
from common.socrative_api import isStringNotEmpty, safeToInt, isNotNone, htmlEscape, htmlUnescape, serializerToBool
from common.socrative_api import isString, room_name_validator, isBool
from string import strip, lower, upper
from common.socrative_errors import BaseError
from common import http_status as status
from common import base_limits


class StudentResponseTextAnswer(DTOConverter):
    """
    DTO class to serialize the studentresponsetextanswer
    """
    stringifyDict = {"answer_text": ItemConverter(modifiers=[htmlEscape])}


class StudentResponseAnswerSelection(DTOConverter):
    """
    class to serialize the studentresponseanswerselection
    """
    stringifyDict = {"answer_id": ItemConverter()}


class StudentResponse(DTOConverter):
    """
    class to serialize the studentreponse object
    """
    stringifyDict = {
        "activity_instance_id": ItemConverter(dtoName="activity_instance"),
        "question_id": ItemConverter(dtoName="question"),
        "id": ItemConverter(),
        "user_uuid": ItemConverter(),
        "is_correct": ItemConverter(),
        "text_answers": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=StudentResponseTextAnswer,
                                      optional=True),
        "selection_answers": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=StudentResponseAnswerSelection,
                                           optional=True),
        "team": ItemConverter(optional=True),
        "hidden": ItemConverter()
    }


class StudentName(DTOConverter):
    """
    class to serialize the studentname object
    """

    stringifyDict = {
        "name": ItemConverter(),
        "user_uuid": ItemConverter(),
        "student_id": ItemConverter(optional=True),
        "present": ItemConverter(optional=True),
    }


class StudentViewableResponse(DTOConverter):
    """
    class to serialize the studentresponse
    """

    stringifyDict = {
        "activity_instance_id": ItemConverter(dtoName="activity_instance"),
        "question_id": ItemConverter(dtoName="question"),
        "id": ItemConverter(),
        "user_uuid": ItemConverter(),
        "text_answers": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=StudentResponseTextAnswer,
                                      optional=True),
        "selection_answers": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=StudentResponseAnswerSelection,
                                           optional=True),
    }


class StudentDTO(DTOConverter):
    """
    class to serialize the student object
    """

    stringifyDict = {
        "first_name": ItemConverter(),
        "last_name" : ItemConverter(),
        "user_uuid": ItemConverter(),
        "student_id": ItemConverter(),
        "id": ItemConverter(),
    }


class HideResponseRequestDTO(RequestDTOValidator):
    """
    validates the hide response request data
    """

    rulesDict = {
        "response_id": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.RESPONSE_ID_MISSING,
                                                                                 status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(funcList=[strip, lower, isStringNotEmpty, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.response_id = None
        self.room_name = None


class FindStudentResponseRequestDTO(RequestDTOValidator):
    """
    validates the data for find student response request
    """

    rulesDict = {
        "user_uuid": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.STUDENT_AUTH_FAILURE,
                                                                           status.HTTP_400_BAD_REQUEST)),
        "activity_instance_id": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.ACTIVITY_ID_MISSING,
                                                                                          status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.user_uuid = None
        self.activity_instance_id = None


class SubmitStudentResponseRequestDTO(RequestDTOValidator):
    """
    validates the data for find student response request
    """

    rulesDict = {
        "user_uuid": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.STUDENT_AUTH_FAILURE,
                                                                           status.HTTP_400_BAD_REQUEST),
                                   funcLimits=[(len, base_limits.AUTH_TOKEN_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                    status.HTTP_400_BAD_REQUEST))]),
        "activity_instance_id": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.ACTIVITY_ID_MISSING,
                                                                                          status.HTTP_400_BAD_REQUEST)),
        "question_id": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.QUESTION_ID_MISSING,
                                                                                 status.HTTP_400_BAD_REQUEST)),
        "answer_ids": ItemValidator(mandatory=False, defaultValue="", funcList=[isStringNotEmpty], errorCode=()),
        "answer_text": ItemValidator(mandatory=False, defaultValue=None, funcList=[isStringNotEmpty, htmlUnescape],
                                     errorCode=(),
                                     funcLimits=[(len, base_limits.STUDENT_RESPONSE_TEXT_LIMIT,
                                                  (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST))]),
        "multi_response": ItemValidator(mandatory=False, defaultValue=False, funcList=[serializerToBool, isBool],
                                        errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST)),
        "team": ItemValidator(mandatory=False, defaultValue=-1, funcList=[safeToInt], errorCode=()),
        "check_activity": ItemValidator(mandatory=False, defaultValue=False, funcList=[serializerToBool, isBool],
                                        errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.user_uuid = None
        self.activity_instance_id = None
        self.question_id = None
        self.answer_ids = ""
        self.answer_text = None
        self.multi_response = False
        self.team = -1
        self.check_activity = False


class GetStudentScoreRequestDTO(RequestDTOValidator):
    """
    validates the data for find student response request
    """

    rulesDict = {
        "user_uuid": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.STUDENT_AUTH_FAILURE,
                                                                           status.HTTP_400_BAD_REQUEST),
                                   funcLimits=[(len, base_limits.AUTH_TOKEN_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                    status.HTTP_400_BAD_REQUEST))]),
        "activity_instance_id": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.ACTIVITY_ID_MISSING,
                                                                                          status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.user_uuid = None
        self.activity_instance_id = None


class SetStudentNameRequestDTO(RequestDTOValidator):
    """
    validates the data for find student response request
    """

    rulesDict = {
        "user_uuid": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.STUDENT_AUTH_FAILURE,
                                                                           status.HTTP_400_BAD_REQUEST),
                                   funcLimits=[(len, base_limits.AUTH_TOKEN_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                    status.HTTP_400_BAD_REQUEST))]),
        "activity_instance_id": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.ACTIVITY_ID_MISSING,
                                                                                          status.HTTP_400_BAD_REQUEST)),
        "student_name": ItemValidator(funcList=[isStringNotEmpty],
                                      errorCode=(BaseError.STUDENT_NAME_MISSING, status.HTTP_400_BAD_REQUEST),
                                      funcLimits=[(len, base_limits.STUDENT_NAME_LIMIT,
                                                   (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST))])
    }

    def __init__(self):
        """

        :return:
        """
        self.user_uuid = None
        self.activity_instance_id = None
        self.student_name = None


class StudentLoginRosterDTO(RequestDTOValidator):
    """
    validates the data for find student response request
    """

    rulesDict = {
        "student_id": ItemValidator(funcList=[isString, strip, isStringNotEmpty, upper],
                                    errorCode=(BaseError.STUDENT_ID_MISSING, status.HTTP_400_BAD_REQUEST),
                                    funcLimits=[(len, base_limits.STUDENT_ROSTER_ID_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                            status.HTTP_400_BAD_REQUEST)
                                                 )]),
        "room_name": ItemValidator(funcList=[isString, strip, isStringNotEmpty, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST)),
        "user_uuid": ItemValidator(funcList=[isString, strip, isStringNotEmpty], errorCode=(BaseError.STUDENT_AUTH_FAILURE,
                                                                                            status.HTTP_400_BAD_REQUEST),
                                   funcLimits=[(len, base_limits.AUTH_TOKEN_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                     status.HTTP_400_BAD_REQUEST))]),
        "force": ItemValidator(mandatory=False, defaultValue=False, funcList=[serializerToBool, isBool],
                               errorCode=(BaseError.INVALID_FORCE_FIELD, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.student_id = None
        self.room_name = None
        self.user_uuid = None
        self.force = None


class StudentLogoutRequestDTO(RequestDTOValidator):
    """
    validates the data for find student response request
    """

    rulesDict = {
        "room_name": ItemValidator(funcList=[isString, strip, lower, isStringNotEmpty, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST)),
        "user_uuid": ItemValidator(funcList=[isString, strip, isStringNotEmpty], errorCode=(BaseError.STUDENT_AUTH_FAILURE,
                                                                                            status.HTTP_400_BAD_REQUEST),
                                   funcLimits=[(len, base_limits.AUTH_TOKEN_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                    status.HTTP_400_BAD_REQUEST))]),
    }

    def __init__(self):
        """

        :return:
        """
        self.room_name = None
        self.user_uuid = None


class StudentHandraiseRequestDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "room_name": ItemValidator(funcList=[isString, strip, lower, isStringNotEmpty, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST)),
        "user_uuid": ItemValidator(funcList=[isString, strip, isStringNotEmpty],
                                   errorCode=(BaseError.STUDENT_AUTH_FAILURE,
                                              status.HTTP_400_BAD_REQUEST),
                                   funcLimits=[(len, base_limits.AUTH_TOKEN_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                    status.HTTP_400_BAD_REQUEST))]),
        "active": ItemValidator(mandatory=True, funcList=[serializerToBool, isNotNone],
                                errorCode=(BaseError.INVALID_ACTIVE_FIELD, status.HTTP_400_BAD_REQUEST)),
        "id": ItemValidator(mandatory=True, funcList=[safeToInt, isNotNone],
                            errorCode=(BaseError.INVALID_STUDENT_ID, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.room_name = None
        self.user_uuid = None
        self.active = None
        self.id = None