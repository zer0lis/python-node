# -*- coding: utf-8 -*-
import time

from psycopg2 import OperationalError

from common.base_dao import BaseDao, retry
from common.base_dao import BaseModel
from common.counters import DatadogThreadStats
from common.socrative_api import exceptionStack, safeToInt
from rooms.dao import RoomModel, RosterModel

statsd = DatadogThreadStats.STATS

import logging
logger = logging.getLogger(__name__)


class StudentNameModel(BaseModel):
    """
    model class to
    """
    NAME = "name"
    USER_UUID = "user_uuid"
    ID = "id"
    ACTIVITY_INSTANCE_ID = "activity_instance_id"

    FIELD_COUNT = 4

    def __init__(self):
        """
        Constructor
        """
        self.name = None
        self.user_uuid = None
        self.id = None
        self.activity_instance_id = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.NAME] = self.name
        obj[self.USER_UUID] = self.user_uuid
        obj[self.ID] = self.id
        obj[self.ACTIVITY_INSTANCE_ID] = self.activity_instance_id
        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = StudentNameModel()
        model.name = dictObj.get(cls.NAME)
        model.user_uuid = dictObj.get(cls.USER_UUID)
        model.id = dictObj.get(cls.ID)
        model.activity_instance_id = dictObj.get(cls.ACTIVITY_INSTANCE_ID)

        return model


class StudentNameDao(BaseDao):
    """
    class to hold the student name dao
    """
    FETCH_MANY_SIZE = 100
    MODEL = StudentNameModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(StudentNameDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.student_name.get_first_student.time')
    def getFirstStudent(self, userUuid, activityInstanceId, minId=None):
        """
        returns the first student it finds with the specified param
        :param userUuid:
        :param activityInstanceId:
        :param minId:
        :return:
        """

        if userUuid is None:
            raise ValueError("userUuid must be a valid string not None")

        if activityInstanceId is None:
            raise ValueError("activityInstanceId must be a valid int not None")

        error = None
        try:
            with self.connection.cursor() as cursor:
                if minId is None:
                    cursor.execute("""SELECT * FROM students_studentname WHERE  activity_instance_id=%s AND """
                                   """user_uuid=%s LIMIT 1""", (activityInstanceId, userUuid))
                else:
                    cursor.execute("""SELECT * FROM students_studentname WHERE id>%s AND activity_instance_id=%s AND """
                                   """user_uuid=%s LIMIT 1""", (minId, activityInstanceId, userUuid))

                return self.fetchOneAsModel(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.student_name.upsert.time')
    def upsert(self, name, userUuid, activityInstanceId):
        """
        inserts or update a student record
        :param name
        :param userUuid
        :param activityInstanceId
        """

        if name is None:
            raise ValueError("name must be a valid string not None")

        if userUuid is None:
            raise ValueError("userUuid must be a valid string not None")

        if activityInstanceId is None:
            raise ValueError("activityInstanceId must be a valid int not None")

        try:
            start = time.time()
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE students_studentname SET "name"=%s WHERE user_uuid=%s AND """
                               """activity_instance_id=%s""", (name, userUuid, activityInstanceId))

                if cursor.rowcount <= 0:
                    cursor.execute("""INSERT INTO students_studentname (name, user_uuid, activity_instance_id) VALUES"""
                                   """(%s, %s, %s)""", (name, userUuid, activityInstanceId))

                if cursor.rowcount <= 0:
                    self.connection.rollback()
                    raise BaseDao.NotUpserted("no student was updated or inserted for activity_id %d and user_uuid %s" %
                                              (activityInstanceId, userUuid))
                else:
                    self.connection.commit()
        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.student_name.get_student_names.time')
    def getStudentNames(self, responseIds):
        """
        """

        error = None

        try:
            start = time.time()
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT a.user_uuid, a.activity_instance_id, b.name FROM student_responses AS"""
                               """ a INNER JOIN students_studentname AS b ON a.user_uuid=b.user_uuid AND """
                               """a.activity_instance_id=b.activity_instance_id WHERE a.id IN %s""",
                               (tuple(responseIds),))

                resp = cursor.fetchall()
                respDict = dict()
                for user_uuid, aId, name in resp:
                    respDict[(user_uuid, aId)] = name

                return respDict
        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student_name.get_all_names_by_activity.time')
    def getAllNamesByActivity(self, activity_instance_id):
        """
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT user_uuid,"name" FROM students_studentname WHERE activity_instance_id=%s""",
                               (activity_instance_id,))
                resp = cursor.fetchall()
                respDict = dict((uuid, name) for uuid, name in resp)
                return respDict
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.student_name.get_student_names_for_activity_id.time')
    def getStudentNamesForActivityId(self, auth_token, activity_instance_id, _type=BaseDao.TO_DICT):
        """

        :param auth_token:
        :param activity_instance_id:
        :param _type:
        :return:
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                if auth_token:
                    cursor.execute("""SELECT a.* FROM students_studentname as a INNER JOIN common_activityinstance as"""
                                   """ b ON a.activity_instance_id=b.id INNER JOIN socrative_users_socrativeuser as c"""
                                   """ ON b.started_by_id = c.id WHERE b.id=%s and c.auth_token=%s""",
                                   (activity_instance_id, auth_token))
                else:
                    cursor.execute("""SELECT a.* FROM students_studentname as a INNER JOIN common_activityinstance as"""
                                   """ b ON a.activity_instance_id=b.id WHERE b.id=%s""", (activity_instance_id,))
                if _type == self.TO_DICT:
                    return self.fetchManyAsDict(cursor)
                else:
                    return self.fetchManyAsModel(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.student_name.check_name_exist.time')
    def checkNameExist(self, user_uuid, activityId, minId=None):
        """
        :param user_uuid
        :param activityId
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                if minId is None:
                    cursor.execute("""select count(id) from students_studentname where activity_instance_id=%s and """
                                   """user_uuid=%s""", (activityId, user_uuid))
                else:
                    cursor.execute("""select count(id) from students_studentname where id>%s AND """
                                   """activity_instance_id=%s AND user_uuid=%s""", (minId, activityId, user_uuid))
                val = cursor.fetchone()[0]
                if val == 1:
                    return True
                else:
                    return False

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()


class StudentResponseModel(BaseModel):

    LEGACY_ID = "legacy_id"
    USER_UUID = "user_uuid"
    IS_CORRECT = "is_correct"
    ACTIVITY_INSTANCE_ID = "activity_instance_id"
    QUESTION_ID = "question_id"
    TEAM = "team"
    HIDDEN = "hidden"
    ID = "id"

    FIELD_COUNT = 8

    def __init__(self):
        """
        Constructor
        """
        self.legacy_id = None
        self.user_uuid = None
        self.is_correct = None
        self.activity_instance_id = None
        self.question_id = None
        self.team = -1  # default value
        self.hidden = False
        self.id = None

    def toDict(self):
        """
        """
        obj = dict()
        obj[self.LEGACY_ID] = self.legacy_id
        obj[self.USER_UUID] = self.user_uuid
        obj[self.IS_CORRECT] = self.is_correct
        obj[self.ACTIVITY_INSTANCE_ID] = self.activity_instance_id
        obj[self.QUESTION_ID] = self.question_id
        obj[self.TEAM] = self.team
        obj[self.HIDDEN] = self.hidden
        obj[self.ID] = self.id

        return obj


    @classmethod
    def fromDict(cls, dictObj):
        """
        """
        model = StudentResponseModel()
        model.legacy_id = dictObj.get(cls.LEGACY_ID)
        model.user_uuid = dictObj.get(cls.USER_UUID)
        model.is_correct = dictObj.get(cls.IS_CORRECT)
        model.activity_instance_id = dictObj.get(cls.ACTIVITY_INSTANCE_ID)
        model.question_id = dictObj.get(cls.QUESTION_ID)
        model.team = dictObj.get(cls.TEAM)
        model.hidden = dictObj.get(cls.HIDDEN)
        model.id = dictObj.get(cls.ID)

        return model


class StudentResponseDao(BaseDao):
    """
    dao for student response
    """

    FETCH_MANY_SIZE = 100
    MODEL = StudentResponseModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        """

        super(StudentResponseDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.student_response.hide_response.time')
    def hideResponse(self, responseId):
        """
        method that will set hidden to True for a response with pk=responseId
        """

        try:
            if responseId is None:
                raise TypeError("invalid response id")

            start = time.time()
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE student_responses set hidden=%s where id=%s""", (True, responseId))

                if cursor.rowcount <= 0:
                    raise BaseDao.NotUpdated("update query with param %s didn't work" % responseId)

            self.connection.commit()
        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.student_response.get_responses.time')
    def getResponses(self, user_uuid, activity_instance):
        """
        method to retrieve all answers for a student or an activity id
        """

        if activity_instance is None:
            raise ValueError("activity instance is None, but it must be a valid int")

        error = None
        try:
            start = time.time()
            with self.connection.cursor() as cursor:
                if user_uuid is None:
                    cursor.execute("""SELECT * FROM student_responses WHERE activity_instance_id=%s""",
                                   (activity_instance,))
                else:
                    cursor.execute("""SELECT * FROM student_responses WHERE activity_instance_id=%s AND """
                                   """user_uuid=%s""", (activity_instance, user_uuid))

                return self.fetchManyAsDict(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.student_response.get_response_models.time')
    def getResponseModels(self, activityId, userUuid, questionId, minId=None):
        """
        get the student responses that correspond to the activity id for useruuid and questionId
        :param activityId:
        :param userUuid:
        :param questionId:
        """
        
        if activityId is None:
            raise ValueError("activity id is None, but it must be int")
        
        if userUuid is None:
            raise ValueError("userUuid is None, but it must be string")
        
        if questionId is None:
            raise ValueError("questionId is None, but it must be int")

        error = None
        try:
            with self.connection.cursor() as cursor:
                if minId is None:
                    cursor.execute("""SELECT * from student_responses WHERE activity_instance_id=%s AND """
                                   """user_uuid=%s AND question_id=%s""", (activityId, userUuid, questionId))
                else:
                    cursor.execute("""SELECT * from student_responses where id>%s AND activity_instance_id=%s"""
                                   """ AND question_id=%s AND user_uuid=%s""", (minId, activityId, questionId, userUuid)
                                   )
                
                return self.fetchManyAsModel(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.student_response.delete_responses_by_id_list.time')
    def deleteResponsesByIdList(self, idList):
        """
        delete the records that have the ids in the list
        """

        if not idList:
            # the list is None or empty
            logger.debug("students.dao.deleteResponseByIdList received an empty list of ids")
            return

        try:
            start = time.time()
            with self.connection.cursor() as cursor:
                cursor.execute("""DELETE FROM student_responses_text_answer where student_response_id in %s""",
                               (tuple(idList),))
                cursor.execute("""DELETE FROM student_responses_answer_selection where student_response_id in %s""",
                               (tuple(idList),))
                cursor.execute("""DELETE FROM student_responses where id in %s""", (tuple(idList),))
                self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.student_response.create.time')
    def create(self, model):
        """
        inserts a new student response into the db
        :param model:
        :type model: StudentResponseModel
        """
        if type(model) is not StudentResponseModel:
            raise TypeError("the model is not of type StudentResponseModel, now it's %s" % str(type(model)))

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""INSERT INTO student_responses (legacy_id, user_uuid, is_correct,"""
                               """activity_instance_id, question_id, team, hidden) VALUES (%s, %s, %s, %s, %s, %s, %s)"""
                               """RETURNING id""", (model.legacy_id, model.user_uuid, model.is_correct,
                                                    model.activity_instance_id, model.question_id, model.team,
                                                    model.hidden))
                if cursor.rowcount != 1:
                    raise BaseDao.NotInserted
                model.id = safeToInt(cursor.fetchone()[0])
                self.connection.commit()
        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.student_response.update.time')
    def update(self, model):
        """
        updates a student response into the db
        :param model:
        :type model: StudentResponseModel
        """
        if type(model) is not StudentResponseModel:
            raise TypeError("the model is not of type StudentResponseModel, now it's %s" % str(type(model)))

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE student_responses SET legacy_id=%s, user_uuid=%s, is_correct=%s,"""
                               """activity_instance_id=%s, question_id=%s, team=%s, hidden=%s WHERE id=%s""",
                               (model.legacy_id, model.user_uuid, model.is_correct, model.activity_instance_id,
                                model.question_id, model.team, model.hidden, model.id))
                if cursor.rowcount != 1:
                    raise BaseDao.NotUpdated
                self.connection.commit()
        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.student_response.get_all_responses.time')
    def getAllResponses(self, activity_instance_id, order_by=None, sortOrder=None, _type=BaseDao.TO_MODEL, hidden=None):
        """
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                query = cursor.mogrify("""SELECT * FROM student_responses WHERE activity_instance_id=%s""",
                                       (activity_instance_id,))

                if hidden is not None:
                    query += cursor.mogrify(""" AND hidden=%s""", (hidden,))

                if order_by is not None:
                    if order_by == self.USER_UUID and sortOrder == self.ASCENDING:
                        query += cursor.mogrify(""" ORDER BY user_uuid ASC""")
                    elif order_by == self.USER_UUID and sortOrder == self.DESCENDING:
                        query += cursor.mogrify(""" ORDER BY user_uuid DESC""")
                    elif order_by == self.USER_UUID(""" ORDER BY user_uuid"""):
                        query += cursor.mogrify(""" ORDER BY user_uuid""")

                cursor.execute(query)

                if _type == self.TO_MODEL:
                    return self.fetchManyAsModel(cursor)
                else:
                    return self.fetchManyAsDict(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.student_response.get_activity_status.time')
    def getActivityStatus(self, studentId, activityInstanceId, quizId):
        """
        return all questions ids that were unanswered
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT question_id FROM quizzes_question WHERE quiz_id = %s AND question_id NOT IN"""
                               """ (SELECT question_id FROM student_responses WHERE activity_instance_id=%s """
                               """AND user_uuid=%s)""", (quizId, activityInstanceId, studentId))
                if cursor.rowcount == 0:
                    return list()
                else:
                    questionIds = [a[0] for a in cursor.fetchall()]
                return questionIds
        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student_response.get_votes.time')
    def getVotes(self, activity_id, quiz_id):
        """
        get the number of votes per
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT a.id, a.text FROM quizzes_answer as a INNER JOIN quizzes_question AS """
                               """q ON a.question_id=q.question_id WHERE q.quiz_id=%s""", (quiz_id,))
                if cursor.rowcount == 0:
                    return dict()

                answerDict = dict((a[0], a[1]) for a in cursor.fetchall())

                cursor.execute("""SELECT a.answer_id FROM student_responses_answer_selection"""
                               """ as a INNER JOIN student_responses as s ON a.student_response_id=s.id WHERE """
                               """s.activity_instance_id=%s""", (activity_id,))

                responseDict = dict((v, 0) for v in answerDict.values())

                for resp in cursor.fetchall():
                    responseDict[answerDict[resp[0]]] += 1

                return responseDict

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.student_response.get_student_correct_responses.time')
    def getStudentCorrectResponses(self, activity_id, student_uuid):
        """
        get the question_id and if the response was correct or not
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT COUNT(id) FROM student_responses WHERE """
                               """activity_instance_id=%s AND user_uuid=%s AND is_correct IS TRUE""",
                               (activity_id, student_uuid))
                if cursor.rowcount == 0:
                    return 0

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.debug(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()


class StudentResponseTextAnswerModel(BaseModel):
    """

    """
    ANSWER_TEXT = "answer_text"
    STUDENT_RESPONSE_ID = "student_response_id"
    ID = "id"

    def __init__(self):
        """
        Constructor
        """
        self.id = None
        self.answer_text = None
        self.student_response_id = None

    def toDict(self):
        """
        toDict
        """
        obj = dict()
        obj[self.ANSWER_TEXT] = self.answer_text
        obj[self.STUDENT_RESPONSE_ID] = self.student_response_id
        obj[self.ID] = self.id
        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        from dict
        """
        model = StudentResponseTextAnswerModel()
        model.answer_text = dictObj.get(cls.ANSWER_TEXT)
        model.student_response_id = dictObj.get(cls.STUDENT_RESPONSE_ID)
        model.id = dictObj.get(cls.ID)
        return model


class StudentResponseTextAnswerDao(BaseDao):
    """

    """

    FETCH_MANY_SIZE = 100
    MODEL = StudentResponseTextAnswerModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        Constructor
        :param dbConfigCtx:
        :param connMgr:
        :return:
        """

        super(StudentResponseTextAnswerDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.student_response_text_answer.get_text_answers_by_response_ids.time')
    def getTextAnswersByResponseIds(self, studentResponseIds):
        """
        get text answers of student response ids
        :param studentResponseIds:
        :return:
        """

        if type(studentResponseIds) is not list:
            raise TypeError("studentResponseIds is not a list, now it's %s" % str(type(studentResponseIds)))

        if not studentResponseIds:
            return list()

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM student_responses_text_answer where student_response_id in %s""",
                               (tuple(studentResponseIds),))
                if cursor.rowcount <= 0:
                    return list()
                else:
                    resp = self.fetchManyAsDict(cursor)
                    return resp

        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.student_response_text_answer.delete_by_response_id.time')
    def deleteByResponseId(self, studentResponseId):
        """
        """

        if studentResponseId is None:
            raise ValueError("student response id is none, but it must be an int")

        try:
            start = time.time()
            with self.connection.cursor() as cursor:
                cursor.execute("""DELETE FROM student_responses_text_answer WHERE student_response_id=%s""",
                               (studentResponseId,))
                if cursor.rowcount < 0:
                    raise BaseDao.NotDeleted("no row was deleted from studentresponsetextanswer")
                self.connection.commit()
        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.student_response_text_answer.create.time')
    def create(self, answerText, studentResponseId):
        """
        inserts a new record in student_responses_text_answer
        """

        if studentResponseId is None:
            raise ValueError("studentResponseId is None, and it must be an integer")

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""INSERT INTO student_responses_text_answer (answer_text, student_response_id)"""
                               """ VALUES (%s, %s)""", (answerText, studentResponseId))
                if cursor.rowcount != 1:
                    raise BaseDao.NotInserted("failed to insert answer text for student resp id %s" % studentResponseId)
                self.connection.commit()
        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.student_response_text_answer.get_texts.time')
    def getTexts(self, responseIdList):
        """
        get all texts that correspond to response ids from the list
        :param responseIdList:
        :return: a dict -> responseId: list of texts
        """

        if not responseIdList:
            return dict()
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT student_response_id, answer_text FROM student_responses_text_answer WHERE"""
                               """ student_response_id in %s""", (tuple(responseIdList),))
                resp = cursor.fetchall()

                respDict = dict()
                for a in resp:
                    if a[0] in respDict:
                        respDict[a[0]].append(a[1])
                    else:
                        respDict[a[0]] = [a[1]]
                return respDict

        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.student_response_text_answer.get_all_text_responses.time')
    def getAllTextResponses(self, activityId):
        """
        """
        error = None
        try:
            start = time.time()
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT b.answer_text FROM student_responses as a INNER JOIN """
                               """student_responses_text_answer AS b ON a.id=b.student_response_id WHERE """
                               """a.activity_instance_id=%s AND a.hidden is FALSE""", (activityId,))
                respList = [a[0] for a in cursor.fetchall()]

                return respList

        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()


class StudentResponseAnswerSelectionModel(BaseModel):
    """

    """

    ANSWER_ID = "answer_id"
    STUDENT_RESPONSE_ID = "student_response_id"
    ID = "id"

    def __init__(self):
        """
        Constructor
        :return:
        """
        self.answer_id = None
        self.student_response_id = None
        self.id = None

    def toDict(self):
        """

        :return:
        """

        obj = dict()
        obj[self.ANSWER_ID] = self.answer_id
        obj[self.STUDENT_RESPONSE_ID] = self.student_response_id
        obj[self.ID] = self.id

        return obj

    def fromDict(cls, dictObj):
        """

        :param cls:
        :param dictObj:
        :return:
        """

        model = StudentResponseAnswerSelectionModel()
        model.answer_id = dictObj.get(cls.ANSWER_ID)
        model.student_response_id = dictObj.get(cls.STUDENT_RESPONSE_ID)
        model.id = dictObj.get(cls.ID)

        return model


class StudentResponseAnswerSelectionDao(BaseDao):
    """

    """

    FETCH_MANY_SIZE = 100
    MODEL = StudentResponseAnswerSelectionModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        Constructor
        :param dbConfigCtx:
        :param connMgr:
        :return:
        """

        super(StudentResponseAnswerSelectionDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.student_response_answer_selection.get_selection_by_response_ids.time')
    def getSelectionByResponseIds(self, studentResponseIds):
        """
        get selection answers based on responseIds
        :param studentResponseIds:
        :return:
        """

        if type(studentResponseIds) is not list:
            raise TypeError("studentResponseIds is not a list, now it's %s" % str(type(studentResponseIds)))

        if not studentResponseIds:
            return list()

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM student_responses_answer_selection WHERE student_response_id IN %s""",
                               (tuple(studentResponseIds),))
                if cursor.rowcount <= 0:
                    return list()
                else:

                    resp = self.fetchManyAsDict(cursor)
                    return resp

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.student_response_text_answer.delete_by_response_id.time')
    def deleteByResponseId(self, studentResponseId):
        """
        """

        if studentResponseId is None:
            raise ValueError("student response id is none, but it must be an int")

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""DELETE FROM student_responses_answer_selection WHERE student_response_id=%s""",
                               (studentResponseId,))
                logger.debug("deleted %d records from student_responses_answer_selection" % cursor.rowcount)
                if cursor.rowcount < 0:
                    raise BaseDao.NotDeleted("error at delete")
                self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.student_response_text_answer.create_by_id_list.time')
    def createByIdList(self, uniqueAnswerIds, studentResponseId):
        """

        :param uniqueAnswerIds:
        :param studentResponseId:
        :return:
        """

        if not uniqueAnswerIds:
            raise ValueError("the uniqueAnswerIds was None or an empty list")

        if studentResponseId is None:
            raise ValueError("the studentResponseId must be a valid int, not None")

        try:
            with self.connection.cursor() as cursor:
                args_str = ','.join(cursor.mogrify("(%s,%s)", (answerId, studentResponseId)) for answerId in uniqueAnswerIds)
                cursor.execute("""INSERT INTO student_responses_answer_selection (answer_id, student_response_id)"""
                               """ VALUES """ + args_str)

                logger.debug("studentResponseAnswerSelectionDao.createByIdList , inserted %d out of %d values" %
                             (cursor.rowcount, len(uniqueAnswerIds)))
                self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise

        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.student_response_text_answer.get_texts.time')
    def getTexts(self, responseIdList):
        """
        get all texts that correspond to response ids from the list
        :param responseIdList:
        :return: a dict -> responseId: list of texts
        """
        if not responseIdList:
            return dict()
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT a.student_response_id, b.text FROM student_responses_answer_selection AS a"""
                               """ INNER JOIN quizzes_answer AS b ON a.answer_id=b.id WHERE a.student_response_id in %s""",
                               (tuple(responseIdList),))
                resp = cursor.fetchall()

                respDict = dict()
                for a in resp:
                    if a[0] in respDict:
                        respDict[a[0]].append(a[1])
                    else:
                        respDict[a[0]] = [a[1]]

                return respDict
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.student_response_text_answer.get_choice_list.time')
    def getChoiceList(self, responseIds):
        """

        :param responseIds:
        :return:
        """

        error = None
        if not responseIds:
            return dict()
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT student_response_id, answer_id FROM student_responses_answer_selection"""
                               """ WHERE student_response_id in %s""", (tuple(responseIds),))

                resp = cursor.fetchall()
                respDict = dict()
                for k, v in resp:
                    if k in respDict:
                        respDict[k].append(v)
                    else:
                        respDict[k] = [v]

                return respDict

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()


class StudentModel(BaseModel):
    """
    model class to
    """
    ID = "id"
    FIRST_NAME = "first_name"
    LAST_NAME = "last_name"
    STUDENT_ID = "student_id"
    USER_UUID = "user_uuid"
    SYNC_ID = "sync_id"
    STATUS = "status"

    FIELD_COUNT = 7

    MANUAL = "MA"
    CSV = "CS"
    XLS = "XL"

    DEFAULT = 0
    LOGGED_IN = 1
    LOGGED_OUT = 2
    HANDRAISE = 4

    ARCHIVED = 128

    def __init__(self):
        """
        Constructor
        """
        self.id = None
        self.first_name = None
        self.last_name = None
        self.student_id = None
        self.user_uuid = None
        self.sync_id = None
        self.status = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.FIRST_NAME] = self.first_name
        obj[self.LAST_NAME] = self.last_name
        obj[self.STUDENT_ID] = self.student_id
        obj[self.USER_UUID] = self.user_uuid
        obj[self.SYNC_ID] = self.sync_id
        obj[self.STATUS] = self.status
        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = StudentModel()
        model.id = dictObj.get(cls.ID)
        model.first_name = dictObj.get(cls.FIRST_NAME)
        model.last_name = dictObj.get(cls.LAST_NAME)
        model.student_id = dictObj.get(cls.STUDENT_ID)
        model.user_uuid = dictObj.get(cls.USER_UUID)
        model.sync_id = dictObj.get(cls.SYNC_ID)
        model.status = dictObj.get(cls.STATUS)

        return model


class StudentDao(BaseDao):
    """

    """

    FETCH_MANY_SIZE = 100
    MODEL = StudentModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        Constructor
        :param dbConfigCtx:
        :param connMgr:
        :return:
        """

        super(StudentDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.student.update_uuid.time')
    def updateUuid(self, user_uuid, student_id, status=None):
        """
        update the user uuid of the student
        :param user_uuid:
        :param student_id:
        :param status:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                if status is None:
                    cursor.execute("""update students_student set user_uuid=%s where id=%s""", (user_uuid, student_id))
                else:
                    cursor.execute("""update students_student set user_uuid=%s, status=%s where id=%s""", (user_uuid,
                                                                                                           status,
                                                                                                           student_id))
            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.student.insert_students.time')
    def insertStudents(self, studentObjects, roster_id):
        """

        :param first_name:
        :param last_name:
        :param student_id:
        :param roster_id:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""insert into students_student (first_name, last_name, student_id,user_uuid, sync_id,"""
                               """ "status") values """ + ','.join([cursor.mogrify("(%s,%s,%s, '', NULL, 0)",
                                                                    (s.first_name,
                                                                     s.last_name,
                                                                     s.student_id)) for s in studentObjects]) +
                               """ RETURNING id,student_id, last_name, first_name"""
                           )

                if cursor.rowcount != len(studentObjects):
                    raise BaseDao.NotInserted()

                studentDict = dict((result[0], (result[1], result[2], result[3])) for result in cursor.fetchall())

                cursor.execute("""insert into students_student_rosters (student_id, roster_id) values """ +
                               ','.join([cursor.mogrify("""(%s, %s)""", (student_id, roster_id)) for student_id in studentDict]))

                if cursor.rowcount != len(studentDict):
                    raise BaseDao.NotInserted()

                return studentDict

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            logger.error(exceptionStack(e))
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student.check_student.time')
    def checkStudent(self, student_id, room_id):
        """
        get the model using the pk
        :param student_id:
        :param room_id:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT ss.* FROM students_student AS ss INNER JOIN students_student_rosters AS ssr """
                               """ON ssr.student_id=ss.id INNER JOIN rooms_roster AS rr ON rr.id = ssr.roster_id """
                               """INNER JOIN rooms_room AS r ON r.id=rr.room_id WHERE ss.student_id=%s AND r.id=%s """
                               """AND r.status < %s AND r.status & %s=%s AND ss.status < %s""",
                               (student_id, room_id, RoomModel.ARCHIVED, RoomModel.ROSTERED, RoomModel.ROSTERED,
                                StudentModel.ARCHIVED))
                return self.fetchOneAsModel(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student.check_student_id.time')
    def checkStudentId(self, student_db_id, room_id):
        """
        get the model using the pk
        :param student_id:
        :param room_id:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT ss.id FROM students_student AS ss INNER JOIN students_student_rosters AS ssr """
                               """ON ssr.student_id=%s INNER JOIN rooms_roster AS rr ON rr.id = ssr.roster_id """
                               """ and rr.room_id=%s WHERE ss.id=%s AND ss.status < %s AND rr.status < %s""",
                               (student_db_id, room_id, student_db_id, StudentModel.ARCHIVED, RosterModel.ARCHIVED))
                if cursor.rowcount == 1:
                    return True
                else:
                    return False
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student.verify_student_id.time')
    def verifyStudentId(self, student_id, _id, roster_id):
        """

        :param student_id:
        :param _id:
        :param roster_id:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT ss.student_id from students_student as ss inner join """
                               """students_student_rosters as ssr on ssr.student_id=ss.id where ss.student_id=%s and """
                               """ss.id != %s and ssr.roster_id=%s and ss.status < %s""", (student_id, _id, roster_id,
                                                                                     StudentModel.ARCHIVED))
                if cursor.rowcount == 0:
                    return None

                return cursor.fetchone()[0]
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student.verify_student_ids.time')
    def verifyStudentIds(self, student_ids, roster_id):
        """

        :param student_ids:
        :param roster_id:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT ss.student_id from students_student as ss inner"""
                               """ join students_student_rosters as ssr on ssr.student_id=ss.id where """
                               """ ss.student_id in %s and ssr.roster_id=%s and ss.status < %s""", (tuple(student_ids),
                                                                                                  roster_id,
                                                                                                  StudentModel.ARCHIVED))
                if cursor.rowcount == 0:
                    return []

                return [result[0] for result in cursor.fetchall()]
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student.delete_student.time')
    def deleteStudent(self, _id):
        """
        update the user uuid of the student
        :param _id:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""update students_student set status=%s where id=%s AND status < %s RETURNING """
                               """user_uuid""", (StudentModel.ARCHIVED, _id, StudentModel.ARCHIVED))
                if cursor.rowcount == 0:
                    raise BaseDao.NotDeleted
                return cursor.fetchone()[0]
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student.update_student.time')
    def updateStudent(self, first_name, last_name, student_id, _id):
        """

        :param first_name:
        :param last_name:
        :param student_id:
        :param _id:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""update students_student set first_name=%s,last_name=%s,student_id=%s where id=%s """
                               """returning *""", (first_name, last_name, student_id, _id))
                if cursor.rowcount != 1:
                    raise BaseDao.NotUpdated()

                return self.fetchOneAsModel(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            logger.error(exceptionStack(e))
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student.get_student_name.time')
    def getStudentName(self, activity_instance_id, user_uuid):
        """

        :param activity_instance_id:
        :param user_uuid
        :return:
        """

        if user_uuid is None:
            return None, None

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""select ss.first_name, ss.last_name from students_student as ss inner join """
                               """students_activitystudent as sas on sas.student_id=ss.id where """
                               """sas.activity_instance_id=%s and sas.user_uuid=%s""", (activity_instance_id,
                                                                                        user_uuid))
                if cursor.rowcount == 0:
                    return None, None

                return cursor.fetchone()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student.get_students.time')
    def getStudents(self, roomId):
        """

        :param roomId:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""select ss.* from students_student as ss inner join students_student_rosters as ssr """
                               """on ssr.student_id=ss.id inner join rooms_roster as rr on ssr.roster_id=rr.id and """
                               """rr.status < %s inner join rooms_room as r on r.id=rr.room_id and r.status < %s and """
                               """r.status & %s=%s where r.id = %s and ss.status < %s""",
                               (RosterModel.ARCHIVED, RoomModel.ARCHIVED, RoomModel.ROSTERED, RoomModel.ROSTERED, roomId,
                                StudentModel.ARCHIVED))

                return self.fetchManyAsDict(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student.get_student_ids.time')
    def getStudentIDs(self, activity_instance_id):
        """

        :param activity_instance_id:
        :return:
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""select sas.user_uuid, ss.student_id from students_student as ss inner"""
                               """ join students_activitystudent as sas on sas.student_id=ss.id where """
                               """ sas.activity_instance_id=%s""", (activity_instance_id, ))

                if cursor.rowcount <= 0:
                    return dict()

                return dict((result[0], result[1]) for result in cursor.fetchall())
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.student.get_logged_in_student_count.time')
    def getLoggedInStudentCount(self, roomIds):
        """
        returns the number of logged in students in each room
        :param roomIds:
        :return dict
        """
        error = None
        if not roomIds:
            return dict()

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""select count(ss.id), r.id from students_student as ss inner join students_student_rosters as ssr """
                               """on ssr.student_id=ss.id inner join rooms_roster as rr on ssr.roster_id=rr.id and """
                               """rr.status < %s inner join rooms_room as r on r.id=rr.room_id and r.status < %s and """
                               """r.status & %s=%s where r.id in %s and ss.status=%s group by r.id""",
                               (RoomModel.ARCHIVED, RoomModel.ARCHIVED, RoomModel.ROSTERED, RoomModel.ROSTERED,
                                tuple(roomIds), StudentModel.LOGGED_IN))

                return dict((result[1], result[0]) for result in cursor.fetchall())
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student.update_status.time')
    def updateStatus(self, user_uuid, room_name, status):
        """
        :param user_uuid
        :param room_name
        :param status
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""select ss.id, ss.status from students_student as ss inner join students_student_rosters as ssr on """
                               """ssr.student_id=ss.id inner join rooms_roster as rr on ssr.roster_id=rr.id and """
                               """rr.status < %s INNER JOIN rooms_room as r on r.id = rr.room_id and r.status < %s """
                               """ where r.name_lower=%s and ss.user_uuid=%s and ss.status < %s""",
                               (RosterModel.ARCHIVED, RoomModel.ARCHIVED, room_name.lower(), user_uuid,
                                StudentModel.ARCHIVED)
                )

                if cursor.rowcount == 0:
                    raise BaseDao.NotFound()

                sid, old_status = cursor.fetchone()

                cursor.execute("""update students_student set status=%s where id=%s""", (status, sid))

            self.connection.commit()

        except BaseDao.NotFound:
            raise
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.student.update_handraise_status.time')
    def updateHandraiseStatus(self, status, student_id, off=False):
        """
        :param status
        :param student_id
        :param off
        """

        try:
            with self.connection.cursor() as cursor:
                if off:
                    cursor.execute("""UPDATE students_student set status=status & %s where id=%s """, (status,
                                                                                                       student_id))
                else:
                    cursor.execute("""UPDATE students_student set status=status | %s where id=%s """, (status,
                                                                                                       student_id))

            self.connection.commit()

        except BaseDao.NotFound:
            raise
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.student.update_handraise.time')
    def updateHandraise(self, status, room_id , off=False):
        """
        :param status
        :param room_id
        """

        try:
            with self.connection.cursor() as cursor:
                if off:
                    cursor.execute("""UPDATE students_student set status=status & %s where id in (select ss.id from """
                                   """students_student as ss inner join students_student_rosters as ssr on """
                                   """ssr.student_id=ss.id inner join rooms_roster as rr on ssr.roster_id=rr.id and """
                                   """rr.status < %s inner join rooms_room as r on r.id=rr.room_id and r.status < %s"""
                                   """ where r.id=%s and ss.status < %s)""", (status,
                                                                              RosterModel.ARCHIVED,
                                                                              RoomModel.ARCHIVED,
                                                                              room_id,
                                                                              StudentModel.ARCHIVED))
                else:
                    cursor.execute("""UPDATE students_student set status=status | %s where id in (select ss.id from """
                                   """students_student as ss inner join students_student_rosters as ssr on """
                                   """ssr.student_id=ss.id inner join rooms_roster as rr on ssr.roster_id=rr.id and """
                                   """rr.status < %s inner join rooms_room as r on r.id=rr.room_id and r.status < %s"""
                                   """ where r.id=%s and ss.status < %s)""", (status,
                                                                              RosterModel.ARCHIVED,
                                                                              RoomModel.ARCHIVED,
                                                                              room_id,
                                                                              StudentModel.ARCHIVED))

            self.connection.commit()

        except BaseDao.NotFound:
            raise
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.student.logout_students.time')
    def logoutStudents(self, room_id):
        """
        :param room_id
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""select ss.id from students_student as ss inner join students_student_rosters as ssr on """
                               """ssr.student_id=ss.id inner join rooms_roster as rr on ssr.roster_id=rr.id and """
                               """rr.status < %s INNER JOIN rooms_room as r on r.id = rr.room_id and r.status < %s """
                               """ where r.id=%s and ss.status < %s""", (RosterModel.ARCHIVED,
                                                                         RoomModel.ARCHIVED,
                                                                         room_id,
                                                                         StudentModel.ARCHIVED))

                if cursor.rowcount == 0:
                    return

                sids = [result[0] for result in cursor.fetchall()]

                if sids:
                    cursor.execute("""update students_student set status=%s where id in %s""", (StudentModel.LOGGED_OUT,
                                                                                                tuple(sids)))

            self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.student.delete_students.time')
    def deleteStudents(self, roster_ids):
        """

        :param roster_ids:
        :return:
        """

        if type(roster_ids) is not list:
            raise TypeError("roster_ids must be a list")

        try:
            with self.connection.cursor() as cursor:
                cursor.execute(
                    """update students_student set status=%s where id in (select student_id from """
                    """students_student_rosters where roster_id in %s)""", (StudentModel.ARCHIVED, tuple(roster_ids)))

                cursor.execute("""delete from students_student_rosters where roster_id in %s""", (tuple(roster_ids),))

            self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

class StudentActivityModel(BaseModel):
    """
    model for students_activitystudent
    """

    ID = "id"
    STUDENT_ID = "student_id"
    STATUS = "status"
    ACTIVITY_INSTANCE_ID = "activity_instance_id"
    USER_UUID = "user_uuid"
    STUDENT_USERNAME = "student_username"
    FIRST_NAME = "first_name"
    LAST_NAME = "last_name"
    OLD_TYPE = "old_type"
    TEAM = "team"

    FIELD_COUNT = 10

    LOGGED_IN = 1
    DEFAULT = 0
    LOGGED_OFF = 2
    BLOCKED = 128

    def __init__(self):
        """
        Constructor
        """
        self.id = None
        self.student_id = None
        self.status = None
        self.activity_instance_id = None
        self.user_uuid = None
        self.student_username = None
        self.first_name = None
        self.last_name = None
        self.old_type = None
        self.team = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.STUDENT_ID] = self.student_id
        obj[self.STATUS] = self.status
        obj[self.ACTIVITY_INSTANCE_ID] = self.activity_instance_id
        obj[self.USER_UUID] = self.user_uuid
        obj[self.STUDENT_USERNAME] = self.student_username
        obj[self.FIRST_NAME] = self.first_name
        obj[self.LAST_NAME] = self.last_name
        obj[self.OLD_TYPE] = self.old_type
        obj[self.TEAM] = self.team
        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = StudentActivityModel()
        model.id = dictObj.get(cls.ID)
        model.student_id = dictObj.get(cls.STUDENT_ID)
        model.status = dictObj.get(cls.STATUS)
        model.activity_instance_id = dictObj.get(cls.ACTIVITY_INSTANCE_ID)
        model.user_uuid = dictObj.get(cls.USER_UUID)
        model.student_username = dictObj.get(cls.STUDENT_USERNAME)
        model.first_name = dictObj.get(cls.FIRST_NAME)
        model.last_name = dictObj.get(cls.LAST_NAME)
        model.old_type = dictObj.get(cls.OLD_TYPE)
        model.team = dictObj.get(cls.TEAM)

        return model


class StudentActivityDao(BaseDao):
    """

    """

    FETCH_MANY_SIZE = 100
    MODEL = StudentActivityModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        Constructor
        :param dbConfigCtx:
        :param connMgr:
        :return:
        """

        super(StudentActivityDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.student_activity.logout_student.time')
    def logoutStudent(self, user_uuid, room_name):
        """
        insert if the student is not attached to an activity
        :param user_uuid:
        :param room_id:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""update students_activitystudent set status=%s where user_uuid=%s and """
                               """activity_instance_id=(select id from common_activityinstance where room_name=%s """
                               """order by id DESC limit 1)""", (StudentActivityModel.LOGGED_OFF, user_uuid, room_name))

                self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.debug(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.student_activity.update_status.time')
    def updateStatus(self, user_uuid, activity_instance_id, status, room_name, resetTeam=False):
        """

        :param activity_instance_id:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute(
                    """select ss.id, ss.student_id from students_student as ss inner join students_student_rosters """
                    """as ssr on ssr.student_id=ss.id inner join rooms_roster as rr on ssr.roster_id=rr.id and """
                    """rr.status < %s INNER JOIN rooms_room as r on r.id = rr.room_id and r.status < %s """
                    """ where r.name_lower=%s and ss.user_uuid=%s and ss.status < %s""",
                    (RosterModel.ARCHIVED, RoomModel.ARCHIVED, room_name, user_uuid,
                     StudentModel.ARCHIVED)
                    )
                if cursor.rowcount <= 0:
                    raise BaseDao.NotFound("STUDENT NOT FOUND")

                student_id, student_username = cursor.fetchone()

                if resetTeam:
                    cursor.execute("""update students_activitystudent set "status"=%s, user_uuid=%s, team=NULL where """
                                   """activity_instance_id=%s and student_id=%s""",
                                   (status, user_uuid, activity_instance_id, student_id))
                    return student_username, None

                cursor.execute("""update students_activitystudent set "status"=%s, user_uuid=%s where """
                                   """activity_instance_id=%s and student_id=%s returning team""",
                                   (status, user_uuid, activity_instance_id, student_id))
                team = cursor.fetchone()[0]
                return student_username, team
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            logger.error(exceptionStack(e))
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student_activity.getPresence.time')
    def getPresence(self, activity_instance_id):
        """

        :param activity_instance_id:
        :return:
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT student_username,first_name,last_name, user_uuid,"status" from """
                               """ students_activitystudent where activity_instance_id=%s and status < %s""",
                               (activity_instance_id, StudentActivityModel.BLOCKED))

                return [{"student_id": result[0],
                         "name": "%s, %s" % (result[2], result[1]),
                         "user_uuid": result[3],
                         "present": result[4] != StudentActivityModel.DEFAULT} for result in cursor.fetchall()]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.student_activity.add_roster.time')
    def addRoster(self, roster_id, activity_instance_id):
        """
        add the whole
        :param roster_id:
        :param activity_instance_id:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT ss.first_name, ss.last_name,ss.student_id,ss.id from students_student as ss """
                               """inner join students_student_rosters as ssr on ss.id=ssr.student_id and """
                               """ssr.roster_id=%s where ss.status < %s """, (roster_id, StudentModel.ARCHIVED))

                if cursor.rowcount <= 0:
                    logger.error("Roster %d is empty")
                    raise Exception("This shouldn't happen")

                rosterData = [cursor.mogrify("""(%s, %s, %s, %s, %s, %s)""",
                                             (
                                                 result[3],
                                                 activity_instance_id,
                                                 StudentActivityModel.DEFAULT,
                                                 result[0],
                                                 result[1],
                                                 result[2]
                                              )
                                             ) for result in cursor.fetchall()]

                query = cursor.mogrify("""insert into students_activitystudent (student_id, activity_instance_id, """
                               """ "status", first_name, last_name, student_username) VALUES """)
                query += ','.join(rosterData)

                cursor.execute(query)

                if cursor.rowcount <= 0:
                    raise BaseDao.NotInserted()

                self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.debug(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.student_activity.check_rostered.time')
    def checkRostered(self, activity_instance_id):
        """
        check if the activity was run inside a rostered room
        :param activity_instance_id: 
        :return: 
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT id from students_activitystudent where activity_instance_id=%s and old_type is not TRUE limit 1""",
                               (activity_instance_id,))

                return cursor.rowcount == 1

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            logger.error(exceptionStack(e))
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student_activity.get_activity_id.time')
    def getActivityId(self, student_id, user_uuid):
        """
        check if the activity was run inside a rostered room
        :param student_id
        :param user_uuid
        :return: 
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT ca.id FROM common_activityinstance as ca INNER JOIN students_activitystudent"""
                               """ as sas on sas.activity_instance_id=ca.id and sas.student_id = %s and sas.user_uuid=%s"""
                               """ WHERE ca.end_time is NULL ORDER BY sas.id DESC LIMIT 1""",
                               (student_id, user_uuid))

                if cursor.rowcount != 1:
                    return None

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            logger.error(exceptionStack(e))
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.student_activity.updateTeam.time')
    def updateTeam(self, activity_instance_id, user_uuid, teamNumber):
        """
        check if the activity was run inside a rostered room
        :param student_id
        :param user_uuid
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT id, team from students_activitystudent where activity_instance_id=%s and user_uuid=%s""",
                               (activity_instance_id, user_uuid))

                if cursor.rowcount <= 0:
                    return None

                id, team = cursor.fetchone()

                if team is None:
                    cursor.execute("""update students_activitystudent set team=%s where id=%s""", (teamNumber, id))
                    return teamNumber

                return team

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            logger.error(exceptionStack(e))
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()
