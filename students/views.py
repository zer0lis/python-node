# coding=utf-8
import logging
import ujson

from common.http_status import HTTP_400_BAD_REQUEST
from common.socrative_api import exceptionStack
from common.socrative_errors import BaseError
from common.base_view import BaseView
from common import http_status as status
from students.serializers import HideResponseRequestDTO, FindStudentResponseRequestDTO, SubmitStudentResponseRequestDTO
from students.serializers import GetStudentScoreRequestDTO, SetStudentNameRequestDTO, StudentLoginRosterDTO
from students.serializers import StudentHandraiseRequestDTO
from common.counters import DatadogThreadStats
from students.counters import StudentCounters

logger = logging.getLogger(__name__)
statsd = DatadogThreadStats.STATS


class HideResponseView(BaseView):
    """
    class that handles the post request for hide response
    """
    http_method_names = ["post"]

    @statsd.timed(StudentCounters.HIDE_RESPONSE_VIEW)
    def post(self, request, *args, **kwargs):
        """
        handler for HTTP POST request for hide response api
        """

        dataDict = dict()
        for k in request.POST:
            dataDict[k] = request.POST.get(k)

        dto = HideResponseRequestDTO.fromDict(dataDict)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=StudentCounters.HIDE_RESPONSE_FAIL)

        # call the hide response service method
        response = request.studentService.hideResponse(request, dto)

        return self.render_json_response(response[0], statusCode=response[1],
                                         failCounter=StudentCounters.HIDE_RESPONSE_FAIL)


class StudentResponsesView(BaseView):
    """
    view that handles the HTTP GET on find student responses api
    """

    http_method_names = ["get", "post"]

    @statsd.timed(StudentCounters.FIND_STUDENT_RESPONSES)
    def get(self, request, *args, **kwargs):
        """
        find student responses
        :param request:
        :param args:
        :param kwargs:
        """

        data = dict()
        data.update(kwargs)

        self.getStudentUUID(data)

        dto = FindStudentResponseRequestDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1],
                                             failCounter=StudentCounters.FIND_STUDENT_RESPONSES_FAIL)

        try:
            obj = request.studentService.cacheRegistry.studentFindResponseCache.get(dto.activity_instance_id,
                                                                                    dto.user_uuid)
            if obj:
                return self.render_response(obj, statusCode=200)

        except Exception as e:
            logger.error(exceptionStack(e))
            pass

        response = request.studentService.findStudentResponses(request, dto)

        return self.render_json_response(response[0], statusCode=response[1],
                                         failCounter=StudentCounters.FIND_STUDENT_RESPONSES_FAIL)

    @statsd.timed(StudentCounters.SUBMIT_STUDENT_RESPONSE)
    def post(self, request, *args, **kwargs):
        """
        submit student response
        handler for HTTP post for submit student response api
        :param request
        :param args
        :param kwargs
        """

        data = dict()

        for k in request.POST:
            data[k] = request.POST.get(k)

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            pass

        self.getStudentUUID(data)

        dto = SubmitStudentResponseRequestDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1],
                                             failCounter=StudentCounters.SUBMIT_STUDENT_RESPONSE_FAIL)

        resp = request.studentService.submitStudentResponse(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1],
                                         failCounter=StudentCounters.SUBMIT_STUDENT_RESPONSE_FAIL)


class SetStudentNameView(BaseView):
    """
    class that handles the HTTP POST for set student name api
    """

    http_method_names = ["post"]

    @statsd.timed(StudentCounters.SET_STUDENT_NAME)
    def post(self, request, *args, **kwargs):
        """
        handler for the HTTP post request for set student name api
        """

        failCounter = StudentCounters.SET_STUDENT_NAME_FAIL

        data = dict()
        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        self.getStudentUUID(data)

        dto = SetStudentNameRequestDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.studentService.setStudentName(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class AutoAssignTeamView(BaseView):
    """
    class that handles the auto assign team api
    """
    http_method_names = ["get"]

    @statsd.timed(StudentCounters.AUTO_ASSIGN_TEAM)
    def get(self, request, *args, **kwargs):
        """
        handler for HTTP GET for auto assign team api
        """
        failCounter = StudentCounters.AUTO_ASSIGN_TEAM_FAIL

        dataDict = dict()
        for k in request.GET:
            dataDict[k] = request.GET.get(k)

        dataDict.update(kwargs)
        self.getStudentUUID(dataDict)

        resp = request.studentService.autoAssignTeam(request, dataDict)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class StudentFinishActivityView(BaseView):
    """
    class that handles the student finished activity view
    """

    http_method_names = ["post"]

    @statsd.timed(StudentCounters.STUDENT_FINISH_ACTIVITY)
    def post(self, request, *args, **kwargs):
        """
        HTTP POST handler for finish activity
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = StudentCounters.STUDENT_FINISH_ACTIVITY_FAIL

        try:
            data = ujson.loads(request.body)
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST, statusCode=HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        self.getStudentUUID(data)

        # validate received data - DTO for that?
        resp = request.studentService.finishActivity(request, data)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class StudentActivityStatusView(BaseView):
    """
    class that handles the student activity status. How many questions were unanswered
    """

    http_method_names = ["get"]

    @statsd.timed(StudentCounters.GET_STUDENT_ACTIVITY_STATUS)
    def get(self, request, *args, **kwargs):
        """
        handler of student activity status GET request
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        failCounter = StudentCounters.GET_STUDENT_ACTIVITY_STATUS_FAIL

        data = dict()
        data.update(kwargs)
        self.getStudentUUID(data)

        # validate received data - DTO for that?
        resp = request.studentService.studentActivityStatus(request, data)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class GetStudentScoreView(BaseView):
    """
    view that handles the HTTP GET on find student responses api
    """

    http_method_names = ["get"]

    @statsd.timed(StudentCounters.GET_STUDENT_SCORE)
    def get(self, request, *args, **kwargs):
        """
        handler for HTTP GET for find student responses api
        :param request:
        :param args:
        :param kwargs:
        """

        failCounter = StudentCounters.GET_STUDENT_SCORE_FAIL

        data = dict()
        data.update(kwargs)

        self.getStudentUUID(data)

        dto = GetStudentScoreRequestDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.studentService.getStudentScore(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class StudentLoginView(BaseView):
    """
    view that handles the HTTP GET on find student responses api
    """

    http_method_names = ["post"]

    @statsd.timed(StudentCounters.STUDENT_LOGIN)
    def post(self, request, *args, **kwargs):
        """
        handler for HTTP GET for find student responses api
        :param request:
        :param args:
        :param kwargs:
        """

        failCounter = StudentCounters.STUDENT_LOGIN_FAIL

        data = dict()
        self.getStudentUUID(data)
        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.render_json_response(BaseError.INTERNAL_SERVER_ERROR, statusCode=500, failCounter=failCounter)

        dto = StudentLoginRosterDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.studentService.loginRoster(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter,
                                         cookie=response[2])


class StudentHandraiseView(BaseView):
    """

    """

    http_method_names = ["post"]

    @statsd.timed(StudentCounters.STUDENT_HANDRAISE)
    def post(self, request, *args, **kwargs):
        """
        handler for HTTP GET for find student responses api
        :param request:
        :param args:
        :param kwargs:
        """

        failCounter = StudentCounters.STUDENT_HANDRAISE_FAIL

        data = dict()
        self.getStudentUUID(data)
        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST, statusCode=HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        dto = StudentHandraiseRequestDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.studentService.studentHandraise(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)