# -*- coding: utf-8 -*-
from django.conf.urls import url

from students.views import StudentResponsesView, HideResponseView, SetStudentNameView
from students.views import AutoAssignTeamView, StudentFinishActivityView, StudentActivityStatusView
from students.views import GetStudentScoreView, StudentLoginView, StudentHandraiseView

urlpatterns = [
    url(r'^api/responses/(?P<activity_instance_id>[0-9]+)/?', StudentResponsesView.as_view()),
    url(r'^api/responses/?$', StudentResponsesView.as_view()),
    url(r'^api/hide-response/?$', HideResponseView.as_view()),
    url(r'^api/set-name/?$', SetStudentNameView.as_view()),
    url(r'^api/auto-assign-team/(?P<activity_instance_id>[0-9]+)/?$', AutoAssignTeamView.as_view()),
    url(r'^api/finish-activity/?$', StudentFinishActivityView.as_view()),
    url(r'^api/student-activity-status/(?P<activity_instance_id>[0-9]+)/?$', StudentActivityStatusView.as_view()),
    url(r'api/student-score/(?P<activity_instance_id>[0-9]+)/?$', GetStudentScoreView.as_view()),
    url(r'api/login/?$', StudentLoginView.as_view()),
    url(r'api/handraise/?$', StudentHandraiseView.as_view())
]