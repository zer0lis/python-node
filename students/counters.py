# coding=utf-8


class StudentCounters(object):
    """
    class to hold student counter names
    """

    HIDE_RESPONSE_VIEW = 'socrative.views.students.hide_response.time'
    HIDE_RESPONSE_FAIL = 'socrative.views.students.hide_response.fail'
    FIND_STUDENT_RESPONSES_FAIL = 'socrative.views.students.find_student_responses.fail'
    FIND_STUDENT_RESPONSES = 'socrative.views.students.find_student_responses.time'
    SUBMIT_STUDENT_RESPONSE_FAIL = 'socrative.views.students.submit_student_response.fail'
    SUBMIT_STUDENT_RESPONSE = 'socrative.views.students.submit_student_response.time'
    SET_STUDENT_NAME_FAIL = 'socrative.views.students.set_student_name.fail'
    SET_STUDENT_NAME = 'socrative.views.students.set_student_name.time'
    AUTO_ASSIGN_TEAM_FAIL = 'socrative.views.students.auto_assign_team.fail'
    AUTO_ASSIGN_TEAM = 'socrative.views.students.auto_assign_team.time'
    STUDENT_FINISH_ACTIVITY = 'socrative.views.students.student_finish_activity.time'
    STUDENT_FINISH_ACTIVITY_FAIL = 'socrative.views.students.student_finish_activity.fail'
    GET_STUDENT_ACTIVITY_STATUS = 'socrative.views.students.get_student_activity_status.time'
    GET_STUDENT_ACTIVITY_STATUS_FAIL = 'socrative.views.students.get_student_activity_status.fail'
    GET_STUDENT_SCORE = 'socrative.views.students.get_student_score.time'
    GET_STUDENT_SCORE_FAIL = 'socrative.views.students.get_student_score.fail'
    STUDENT_LOGIN = 'socrative.views.students.student_login.time'
    STUDENT_LOGIN_FAIL = 'socrative.views.students.student_login.fail'
    STUDENT_HANDRAISE = 'socrative.views.students.handraise.time'
    STUDENT_HANDRAISE_FAIL = 'socrative.views.students.handraise.fail'