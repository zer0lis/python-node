# coding=utf-8
from common.base_cache import BaseCache
from common.socrative_api import exceptionStack, safeToInt
import logging
import ujson

logger = logging.getLogger(__name__)


class FindStudentResponseCache(BaseCache):
    """
    base class to handle caching of dto json responses
    """

    def __init__(self, redisPool, expireTime):
        """

        :param redisPool:
        :param expireTime:
        :return:
        """

        super(FindStudentResponseCache, self).__init__(redisPool)
        self.expireTime = safeToInt(expireTime) or 84100

    def cacheData(self, activity_instance_id, user_uuid, data):
        """
        :param activity_instance_id:
        :param data:
        :param user_uuid
        :return:
        """

        try:
            key = "find_response_%s_%d" % (user_uuid, activity_instance_id)
            self.getConnection(False).set(key, data, expireTime=self.expireTime)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateData(self, user_uuid, activity_instance_id):
        """

        :param user_uuid:
        :param activity_instance_id:
        :return:
        """
        try:
            key = "find_response_%s_%d" % (user_uuid, activity_instance_id)
            self.getConnection(False).delete(key)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateMulti(self, keyTuples):
        """

        :param keyTuples:
        :return:
        """
        try:
            keys = list()
            for user_uuid, aid in keyTuples:
                key = "find_response_%s_%d" % (user_uuid, aid)
                keys.append(key)
            self.getConnection(False).delete_multi(keys)
        except Exception as e:
            logger.error(exceptionStack(e))

    def get(self, activity_instance_id, user_uuid):
        """
        get the current activity
        :param activity_instance_id:
        :param user_uuid:
        :return:
        """

        try:
            key = "find_response_%s_%d" % (user_uuid, activity_instance_id)
            obj = self.getConnection(True).get(key)
            return obj
        except Exception as e:
            logger.error(exceptionStack(e))


class ActivitySettingsCache(BaseCache):
    """
    base class to handle caching of dto json responses
    """

    def __init__(self, redisPool, expireTime):
        """

        :param redisPool:
        :param expireTime:
        :return:
        """

        super(ActivitySettingsCache, self).__init__(redisPool)
        self.expireTime = safeToInt(expireTime) or 172800  # 2 days

    def cacheData(self, activity_instance_id, settings):
        """
        :param activity_instance_id:
        :param data:
        :param user_uuid
        :return:
        """

        try:
            key = "as_%d" % activity_instance_id
            self.getConnection(False).set(key, ujson.dumps(settings), expireTime=self.expireTime)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateData(self, activity_instance_id):
        """

        :param user_uuid:
        :param activity_instance_id:
        :return:
        """
        try:
            key = "as_%d" % activity_instance_id
            self.getConnection(False).delete(key)
        except Exception as e:
            logger.error(exceptionStack(e))

    def get(self, activity_instance_id):
        """
        get the current activity
        :param activity_instance_id:
        :return:
        """

        try:
            key = "as_%d" % activity_instance_id
            obj = self.getConnection(True).get(key)
            if obj:
                return ujson.loads(obj)

            return None
        except Exception as e:
            logger.error(exceptionStack(e))

