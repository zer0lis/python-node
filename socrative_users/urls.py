# coding=utf-8
from django.conf.urls import url

from socrative_users.views import RegisterUserView, CheckEmailView, LoginUserView, ResetPasswordByEmailView
from socrative_users.views import ChangePasswordView, UserAuthView, GoogleLoginView, MCLoginView
from socrative_users.views import UserSystemMessageView
from socrative_users.views import GetPremiumStatusView, LogoutView, ZendeskSSOView

urlpatterns = [
    url(r'^register/v1/?$', RegisterUserView.as_view()),
    url(r'^check-email/(?P<email>.{3,128})/?$', CheckEmailView.as_view()),
    url(r'^login/?$', LoginUserView.as_view()),
    url(r'^reset-password/?$', ResetPasswordByEmailView.as_view()),
    url(r'^change-password/?$', ChangePasswordView.as_view()),
    url(r'^validate-auth/(?P<auth_token>[0-9a-fA-F]{20,40})/?$', UserAuthView.as_view()),
    url(r'^authorization-code/?$', GoogleLoginView.as_view()),
    url(r'^api/mc-login/?$', MCLoginView.as_view()),
    url(r'^premium-status/(?P<api_key>[a-zA-Z0-9]{1,48})/(?P<email>.{1,128})/$', GetPremiumStatusView.as_view()),
    url(r'^api/logout/?$', LogoutView.as_view()),
    url(r'^api/zendesk/sso/', ZendeskSSOView.as_view()),
    url(r'^api/user-sys-msg/?$', UserSystemMessageView.as_view()),
    url(r'^api/user-sys-msg/(?P<usr_msg_id>[0-9]+)/?$', UserSystemMessageView.as_view())
]
