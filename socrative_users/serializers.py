# -*- coding: utf-8 -*-
from common.base_dto import DTOConverter, ItemConverter, RequestDTOValidator, ItemValidator
from common.socrative_api import isStringNotEmpty, isString, safeToInt, isNotNone, safeToDate, getNextYear, isEmailValid
from common import base_limits
from string import strip, lower, upper
from common.socrative_errors import BaseError
from common import http_status as status


class User(DTOConverter):
    stringifyDict = {
        "id": ItemConverter(),
        "email": ItemConverter(),
        "register_date": ItemConverter(),
        "display_name": ItemConverter(),
        "language": ItemConverter(),
        "tz_offset": ItemConverter()
    }


class UserProfileDTO(DTOConverter):
    """
    serializer for profile data
    """
    stringifyDict = {}


class UserSystemMessage(DTOConverter):

    stringifyDict = {
        "id": ItemConverter(),
        "title": ItemConverter(),
        "content": ItemConverter(),
        "url": ItemConverter(optional=True),
        "action_type": ItemConverter()
    }


class UserAuthRequestDTO(RequestDTOValidator):
    """
    validates the data for find student response request
    """

    rulesDict = {
        "auth_token": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.AUTH_TOKEN_MISSING,
                                                                            status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.auth_token = None


class CheckEmailRequestDTO(RequestDTOValidator):
    """
    validates the data for find student response request
    """

    rulesDict = {
        "email": ItemValidator(funcList=[isString, strip, lower, isStringNotEmpty, isEmailValid],
                               errorCode=(BaseError.EMAIL_ADDRESS_MISSING, status.HTTP_400_BAD_REQUEST),
                               funcLimits=[(len,
                                            base_limits.MAX_EMAIL_LENGTH,
                                            (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST)
                                            )
                                           ]
                               )
    }

    def __init__(self):
        """

        :return:
        """
        self.email = None


class GetPremiumStatusRequestDTO(RequestDTOValidator):
    """
    validates the request to get the account status
    """

    rulesDict = {
        "api_key": ItemValidator(funcList=[isString, strip, isStringNotEmpty],
                                 errorCode=(BaseError.API_KEY_MISSING, status.HTTP_400_BAD_REQUEST),
                                 funcLimits=[(len,
                                              base_limits.MAX_API_KEY_LENGTH,
                                              (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST)
                                              )
                                             ]
                                 ),
        "email": ItemValidator(funcList=[isString, strip, lower, isStringNotEmpty, isEmailValid],
                               errorCode=(BaseError.EMAIL_ADDRESS_MISSING, status.HTTP_400_BAD_REQUEST),
                               funcLimits=[(len,
                                            base_limits.MAX_EMAIL_LENGTH,
                                            (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST)
                                            )
                                           ]
                               )
    }





class UserLoginRequestDTO(RequestDTOValidator):
    """
    validates the request to get the account status
    """

    rulesDict = {
        "email": ItemValidator(funcList=[isString, strip, lower, isStringNotEmpty, isEmailValid],
                               errorCode=(BaseError.EMAIL_ADDRESS_MISSING, status.HTTP_400_BAD_REQUEST),
                               funcLimits=[(len,
                                            base_limits.MAX_EMAIL_LENGTH,
                                            (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST)
                                            )
                                           ]
                               ),
        "password": ItemValidator(mandatory=True,
                                  funcList=[isStringNotEmpty],
                                  errorCode=(BaseError.PASSWORD_MISSING, status.HTTP_400_BAD_REQUEST),
                                  funcLimits=[(len,
                                               base_limits.MAX_PASSWORD_LENGTH,
                                               (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST)
                                               )
                                              ]
                                  ),
    }


class ZendeskSSORequestDTO(RequestDTOValidator):
    """
    validates the data for find student response request
    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=False,
                                    funcList=[isStringNotEmpty], errorCode=(BaseError.AUTH_TOKEN_MISSING,
                                                                            status.HTTP_400_BAD_REQUEST)),
        "email": ItemValidator(mandatory=False,
                               funcList=[isString, strip, lower, isStringNotEmpty, isEmailValid],
                               errorCode=(BaseError.EMAIL_ADDRESS_MISSING, status.HTTP_400_BAD_REQUEST),
                               funcLimits=[(len,
                                            base_limits.MAX_EMAIL_LENGTH,
                                            (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST)
                                            )
                                           ]
                               ),
        "password": ItemValidator(mandatory=False,
                                  funcList=[isStringNotEmpty],
                                  errorCode=(BaseError.PASSWORD_MISSING, status.HTTP_400_BAD_REQUEST),
                                  funcLimits=[(len,
                                               base_limits.MAX_PASSWORD_LENGTH,
                                               (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST)
                                               )
                                              ]
                                  ),
        "redirect_to": ItemValidator(mandatory=False, funcList=[], errorCode=())
    }

    def __init__(self):
        """

        :return:
        """
        self.auth_token = None
        self.email = None
        self.password = None
        self.redirect_to = None


class UpdateUserMessageRequestDTO(RequestDTOValidator):
    """
    validates the data for find student response request
    """

    rulesDict = {
        "auth_token": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.AUTH_TOKEN_MISSING,
                                                                            status.HTTP_400_BAD_REQUEST)),
        "status": ItemValidator(funcList=[isString, strip, isStringNotEmpty],
                                funcLimits=[(len,
                                             base_limits.MAX_USR_MSG_STATUS_LENGTH,
                                             (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST))],
                                errorCode=(BaseError.USR_MSG_STATUS_MISSING, status.HTTP_400_BAD_REQUEST)),
        "usr_msg_id": ItemValidator(funcList=[safeToInt, isNotNone],
                                    errorCode=(BaseError.USR_MSG_ID_IS_MISSING, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.auth_token = None
        self.status = None
        self.usr_msg_id = None