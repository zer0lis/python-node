# -*- coding: utf-8 -*-
from django.http import HttpResponse
from common.base_view import BaseView

from common.socrative_api import exceptionStack, byteLen, safeToInt
from common.socrative_errors import BaseError
from common.socrative_config import ConfigRegistry
from socrative_users.counters import UserCounters
from socrative_users.serializers import UserAuthRequestDTO, CheckEmailRequestDTO, GetPremiumStatusRequestDTO
from socrative_users.serializers import UserLoginRequestDTO, ZendeskSSORequestDTO
from socrative_users.serializers import UpdateUserMessageRequestDTO
from students.serializers import StudentLogoutRequestDTO
from socrative import settings as projSettings
import ujson
import logging
from common.counters import DatadogThreadStats


logger = logging.getLogger(__name__)
statsd = DatadogThreadStats.STATS

class RegisterUserView(BaseView):
    """
    class that handles user registration api
    """
    http_method_names = ["post"]

    @statsd.timed(UserCounters.REGISTER_USER)
    def post(self, request, *args, **kwargs):
        """
        handle for HTTP POST request for register user
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        data = dict()

        failCounter = UserCounters.REGISTER_USER_FAIL

        try:
            if "application/json" not in request.META["CONTENT_TYPE"]:
                for k in request.POST:
                    data[k] = request.POST.get(k)
            else:
                data = ujson.loads(request.body)
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST, statusCode=400, failCounter=failCounter)

        response = request.usersService.registerUser(request, data)

        return self.render_json_response(response[0], statusCode=response[1], language=response[2], cookie=response[3],
                                         failCounter=failCounter)


class LoginUserView(BaseView):
    """
    class that handles user login
    """

    http_method_names = ["post"]

    @statsd.timed(UserCounters.LOGIN_USER)
    def post(self, request, *args, **kwargs):
        """
        handler for login user (HTTP POST request)
        """

        data = dict()

        for k in request.POST:
            data[k] = request.POST.get(k)

        try:
            data.update(ujson.loads(request.body))
        except Exception:
            pass

        failCounter = UserCounters.LOGIN_USER_FAIL

        dto = UserLoginRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        # check if the ip, or the email account is blocked temporarily
        # check ip first
        ip = request.META.get("HTTP_X_FORWARDED_FOR") or request.META["REMOTE_ADDR"]
        val = request.usersService.ipLimiter.get(ip)
        if val > 10000:
            statsd.increment(UserCounters.IPS_BLOCKED)
            return self.render_json_response(BaseError.IP_BLOCKED_FOR_ONE_DAY, statusCode=400,
                                             failCounter=failCounter)

        # check account and ip
        strVal = request.usersService.emailLimiter.get(ip, dto.email)
        val = safeToInt(strVal)
        if val > 10:
            request.usersService.ipLimiter.cacheData(ip, dto.email)

        if 10 < val <= 20:
            request.usersService.emailLimiter.cacheData(ip, dto.email, level=1)
            statsd.increment(UserCounters.ACCOUNTS_LOCKED_5_MINUTES)
            return self.render_json_response(BaseError.ACCOUNT_BLOCKED_FOR_FIVE_MINUTES, statusCode=400,
                                             failCounter=failCounter)
        elif 20 < val <= 30:
            request.usersService.emailLimiter.cacheData(ip, dto.email, level=2)
            statsd.increment(UserCounters.ACCOUNTS_LOCKED_1_HOUR)
            return self.render_json_response(BaseError.ACCOUNT_BLOCKED_FOR_ONE_HOUR, statusCode=400,
                                             failCounter=failCounter)
        elif val > 30:
            request.usersService.emailLimiter.cacheData(ip, dto.email, level=3)
            statsd.increment(UserCounters.ACCOUNTS_LOCKED_1_DAY)
            return self.render_json_response(BaseError.ACCOUNT_BLOCKED_FOR_ONE_DAY, statusCode=400,
                                             failCounter=failCounter)

        response = request.usersService.loginUser(request, dto)

        if response[1] == 400:
            request.usersService.emailLimiter.cacheData(ip, dto.email, level=1)

        return self.render_json_response(response[0], statusCode=response[1], language=response[2], cookie=response[3],
                                         failCounter=failCounter)


class UserAuthView(BaseView):
    """
    class that handles getting authenticated users
    """

    http_method_names = ["get"]

    @statsd.timed(UserCounters.AUTHENTICATE_USER)
    def get(self, request, *args, **kwargs):
        """
        handler to get authenticated users
        """

        data = dict()

        failCounter = UserCounters.AUTHENTICATE_USER_FAIL

        data.update(request.GET)
        data.update(kwargs)

        dto = UserAuthRequestDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.usersService.authenticateUser(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class CheckEmailView(BaseView):
    """
    class that handles the update of the email address api
    """
    http_method_names = ["get"]

    @statsd.timed(UserCounters.CHECK_EMAIL)
    def get(self, request, *args, **kwargs):
        """
        handler for checking the email
        """

        data = dict()

        data.update(kwargs)

        failCounter = UserCounters.CHECK_EMAIL_FAIL

        # validate the request

        if "email" in data and type(data["email"]) in (str, unicode) and data["email"].endswith('/'):
            data["email"] = data["email"][:-1]
        dto = CheckEmailRequestDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.usersService.checkEmail(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class GoogleLoginView(BaseView):
    """
    class that handles the redirect of google oauth api and provides the authorization code
    """

    http_method_names = ["get"]

    @statsd.timed(UserCounters.GOOGLE_LOGIN)
    def get(self, request, *args, **kwargs):
        """
        method to handle the redirect from google
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        data = dict()

        for k in request.GET:
            data[k] = request.GET.get(k)
        data.update(kwargs)

        url, cookie = request.usersService.googleLogin(request, data)

        if request.is_secure() is True:
            url = url.replace("http://", "https://")

        return self.render_redirect_response(url, cookie)


class ChangePasswordView(BaseView):
    """
    class that handles the change password view
    """

    http_method_names = ["post"]

    @statsd.timed(UserCounters.CHANGE_PASSWORD)
    def post(self, request, *args, **kwargs):
        """
        handler for changing the password
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        data = dict()

        failCounter = UserCounters.CHANGE_PASSWORD_FAIL

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST, statusCode=400, failCounter=failCounter)

        response = request.usersService.changePassword(request, data)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class ResetPasswordByEmailView(BaseView):
    """
    class that handles the reset password by email
    """

    http_method_names = ["post"]

    @statsd.timed(UserCounters.RESET_PASSWORD_BY_MAIL)
    def post(self, request, *args, **kwargs):
        """
        handler for reset password by email
        """

        data = dict()

        for k in request.POST:
            data[k] = request.POST.get(k)

        try:
            data.update(ujson.loads(request.body))
        except Exception:
            pass

        failCounter = UserCounters.RESET_PASSWORD_BY_MAIL_FAIL

        response = request.usersService.resetPasswordByMail(request, data)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class MCLoginView(BaseView):
    """
    class to implement the MC login api
    """

    http_method_names = ["post"]

    @statsd.timed(UserCounters.MC_LOGIN)
    def post(self, request, *args, **kwargs):
        """
        POST http call implementing the MC Login REST API
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        data = dict()

        failCounter = UserCounters.MC_LOGIN_FAIL

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST, statusCode=400, failCounter=failCounter)

        response = request.usersService.mcLogin(request, data)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class ImportQuizForUserView(BaseView):
    """

    """

    http_method_names = ["get"]

    @statsd.timed(UserCounters.IMPORT_QUIZ_FOR_USER)
    def get(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """


        data = dict()

        for k in request.POST:
            data[k] = request.POST.get(k)

        failCounter = UserCounters.IMPORT_QUIZ_FOR_USER_FAIL

        response = request.usersService.importQuizForUser(request, data)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class UserSystemMessageView(BaseView):
    """
    class that implements methods to handle the user system message
    """

    http_method_names = ["put", "get"]

    def get(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        data = dict()

        self.getAuthToken(data)

        dto = UserAuthRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1])

        response = request.usersService.getUserSystemMessage(request, dto)

        return self.render_json_response(response[0], statusCode=response[1])

    @statsd.timed(UserCounters.UPDATE_INFO_BANNER)
    def put(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        failCounter = UserCounters.UPDATE_INFO_BANNER_FAIL
        data = dict()

        self.getAuthToken(data)

        data.update(kwargs)

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST, statusCode=400, failCounter=failCounter)

        dto = UpdateUserMessageRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.usersService.updateUserSystemMessage(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class GetPremiumStatusView(BaseView):
    """
    """

    http_method_names = ["get"]

    @statsd.timed(UserCounters.GET_ACCOUNT_STATUS)
    def get(self, request, *args, **kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        """

        data = dict()

        failCounter = UserCounters.GET_ACCOUNT_STATUS_FAIL

        data.update(kwargs)

        dto = GetPremiumStatusRequestDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.usersService.getAccountStatus(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class LogoutView(BaseView):
    """
    """

    http_method_names = ["post"]

    @statsd.timed(UserCounters.LOGOUT)
    def post(self, request, *args, **kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        """

        data = dict()

        self.getAuthToken(data)
        self.getStudentUUID(data)

        httpResp = HttpResponse()
        httpResp.status_code = 200
        httpResp.content = ujson.dumps({})
        httpResp["Content-Length"] = len(httpResp.content)
        httpResp["Content-Type"] = "application/json"

        if "auth_token" in data:
            httpResp.set_cookie(projSettings.USER_AUTH_TOKEN, '', httponly=True, secure=self.request.is_secure(), path="/",
                                max_age=-1, domain=ConfigRegistry.getItem("COOKIE_DOMAIN"))

        if "user_uuid" in data:
            try:
                data.update(ujson.loads(request.body))
            except Exception as e:
                logger.debug(exceptionStack(e))
                pass

            httpResp.set_cookie(projSettings.STUDENT_AUTH_TOKEN, '', httponly=True, secure=self.request.is_secure(),
                                path="/",
                                max_age=-1, domain=ConfigRegistry.getItem("COOKIE_DOMAIN"))

            dto = StudentLogoutRequestDTO.fromDict(data)
            if type(dto) is tuple:
                httpResp.content = ujson.dumps(dto[0])
                httpResp.status_code = dto[1]
                httpResp["Content-Length"] = byteLen(httpResp.content)
                return httpResp

            resp = request.studentService.logout(request, dto)

            httpResp.content = ujson.dumps(resp[0])
            httpResp["Content-Length"] = byteLen(httpResp.content)

            httpResp.status_code = 200

        return httpResp


class ZendeskSSOView(BaseView):
    """

    """

    http_method_names = ["post"]

    @statsd.timed(UserCounters.ZENDESK_LOGIN)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        data = dict()
        failCounter = UserCounters.ZENDESK_LOGIN_FAIL

        self.getAuthToken(data)
        if "redirect_to" in request.GET:
            data["redirect_to"] = request.GET.get("redirect_to")

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            pass

        dto = ZendeskSSORequestDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.usersService.zendeskSSO(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], cookie=resp[2], failCounter=failCounter)
