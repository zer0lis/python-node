# -*- coding: utf-8 -*-

import datetime
from django.db import models


class SocrativeUser(models.Model):
    """
    Socrative user model structure to be saved in the database
    """
    class Meta:
        verbose_name_plural = "Users"

    legacy_id = models.IntegerField(blank=True, null=True)
    register_date = models.DateField(default=datetime.date.today)
    # TODO: make display_name unique if this can be done safely
    display_name = models.CharField(db_index=True, max_length=32, default="", blank=True)
    auth_token = models.CharField(db_index=True, max_length=20, default="", blank=True)
    first_name = models.CharField(max_length=255, default="", blank=True)
    last_name = models.CharField(max_length=255, default="", blank=True)
    tz_offset = models.IntegerField(null=True)
    language = models.CharField(max_length=32, default="en")
    QUIZ = "QZ"
    SINGLE_QUESTION = "1Q"
    SPACE_RACE = "SR"
    type_choices = (
        (QUIZ, u"Quiz"),
        (SINGLE_QUESTION, u"Single Question Activity"),
        (SPACE_RACE, u"Space Race")
    )
    organization_choices = (
        ("K12", u"K-12"),
        ("USHE", u"US Higher Ed"),
        ("INT", u"International"),
        ("OTHR", u"Other"),
        ("CORP", u"Corporation")
    )
    organization_type = models.CharField(max_length=4, choices=organization_choices, default=None, null=True)

    zip_code = models.CharField(max_length=10, default=None, null=True)

    university = models.CharField(max_length=255, default=None, null=True)

    country = models.CharField(max_length=128, default=None, null=True)

    description = models.CharField(max_length=255, default=None, null=True)

    school_id = models.IntegerField(db_index=True, null=True, default=None)

    school_name = models.CharField(max_length=255, default=None, null=True)

    # will keep all roles in json format
    user_role = models.CharField(max_length=255, default=None, null=True)

    state = models.CharField(max_length=32, default=None, null=True, db_index=True)
    district_name = models.CharField(max_length=128, null=True, default=None, db_index=True)
    school_data = models.TextField(max_length=65536, null=True, default=None)

    email = models.EmailField(max_length=255, unique=True, db_index=True)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=None)
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(default=None)


class TempToken(models.Model):
    """
    class to handle temporary token storage
    """

    CHANGE_PASSWORD = "CP"

    temp_token = models.CharField(max_length=64, unique=True, db_index=True)
    purpose = models.CharField(max_length=2)
    created_by_id = models.ForeignKey(SocrativeUser)


class SystemMessage(models.Model):
    """
    System message structure to be stored in the database
    """

    content = models.TextField(max_length=256000, null=True)
    title = models.TextField(max_length=1024, null=True)
    url = models.TextField(max_length=4096, null=True)
    action_type = models.TextField(max_length=64, null=True)
    start_date = models.DateTimeField(null=True, default=None)
    end_date = models.DateTimeField(null=True, default=None)
    priority = models.IntegerField(null=True)
    dismissible = models.BooleanField(default=True)
    status = models.IntegerField(null=True)
    audience = models.IntegerField(default=0)


class UserSysMsg(models.Model):
    """
    model
    """
    user = models.ForeignKey(SocrativeUser, db_index=True)
    status = models.IntegerField(null=True)
    show_date = models.DateTimeField(null=True)
    sys_message = models.ForeignKey(SystemMessage, db_index=True)


class ScoreExportSettings(models.Model):
    """

    """

    teacher = models.ForeignKey(SocrativeUser, db_index=True)
    key = models.IntegerField()
    next_line = models.IntegerField()
    speed = models.IntegerField()
    prefix = models.CharField(max_length=1)
    method = models.IntegerField()


# to help unit tests by creating easily pro users
class Licenses(models.Model):
    """

    """
    class Meta:
        db_table = "licenses"

    buyer = models.ForeignKey(SocrativeUser, db_index=True)
    key = models.TextField()
    expiration_date = models.DateField()
    coupon_id = models.IntegerField()
    auto_renew = models.NullBooleanField(default=False)
    years = models.IntegerField()
    price_per_seat = models.IntegerField()
    customer_id = models.TextField()

class License_Activations(models.Model):
    """
    
    """
    class Meta:
        db_table = "license_activations"

    license = models.ForeignKey(Licenses)
    user = models.ForeignKey(SocrativeUser)
    activation_date = models.DateField()
