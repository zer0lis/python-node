# -*- coding: utf-8 -*-
import datetime
import logging
import time

from psycopg2 import OperationalError

from common import soc_constants
from common.base_dao import BaseDao, retry
from common.base_dao import BaseModel
from common.socrative_api import exceptionStack
from common.socrative_api import safeToInt
from common.counters import DatadogThreadStats

statsd = DatadogThreadStats.STATS
logger = logging.getLogger(__name__)


class SocrativeUserModel(BaseModel):

    ID = "id"
    PASSWORD = "password"
    LAST_LOGIN = "last_login"
    EMAIL = "email"
    IS_ACTIVE = "is_active"
    DATE_JOINED = "date_joined"
    REGISTER_DATE = "register_date"
    DISPLAY_NAME = "display_name"
    AUTH_TOKEN = "auth_token"
    FIRST_NAME = "first_name"
    LAST_NAME = "last_name"
    LEGACY_ID = "legacy_id"
    LANGUAGE = "language"
    TZ_OFFSET = "tz_offset"
    ORGANIZATION_TYPE = "organization_type"
    ZIP_CODE = "zip_code"
    UNIVERSITY = "university"
    COUNTRY = "country"
    DESCRIPTION = "description"
    SCHOOL_ID = "school_id"
    SCHOOL_NAME = "school_name"
    USER_ROLE = "user_role"
    STATE = "state"
    DISTRICT_NAME = "district_name"
    SCHOOL_DATA = "school_data"

    organization_choices = (
        ("K12", "K-12"),
        ("USHE", "US Higher Ed"),
        ("INT", "International"),
        ("OTHR", "Other"),
        ("CORP", "Corporation")
    )

    FIELD_COUNT = 25

    def __init__(self):
        """
        Constructor
        :return:
        """
        self.id = None
        self.password = None
        self.last_login = datetime.datetime.utcnow()
        self.email = None
        self.is_active = True
        self.date_joined = datetime.datetime.utcnow()
        self.register_date = datetime.date.today()
        self.display_name = ""
        self.auth_token = None
        self.first_name = ""
        self.last_name = ""
        self.legacy_id = None
        self.language = "en"
        self.tz_offset = None
        self.organization_type = None
        self.zip_code = None
        self.university = None
        self.country = None
        self.description = None
        self.school_id = None
        self.school_name = None
        self.user_role = None
        self.state = None
        self.district_name = None
        self.school_data = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.PASSWORD] = self.password
        obj[self.LAST_LOGIN] = self.last_login
        obj[self.EMAIL] = self.email
        obj[self.IS_ACTIVE] = self.is_active
        obj[self.DATE_JOINED] = self.date_joined
        obj[self.REGISTER_DATE] = self.register_date
        obj[self.DISPLAY_NAME] = self.display_name
        obj[self.AUTH_TOKEN] = self.auth_token
        obj[self.FIRST_NAME] = self.first_name
        obj[self.LAST_NAME] = self.last_name
        obj[self.LEGACY_ID] = self.legacy_id
        obj[self.LANGUAGE] = self.language
        obj[self.TZ_OFFSET] = self.tz_offset
        obj[self.ORGANIZATION_TYPE] = self.organization_type
        obj[self.ZIP_CODE] = self.zip_code
        obj[self.UNIVERSITY] = self.university
        obj[self.COUNTRY] = self.country
        obj[self.DESCRIPTION] = self.description
        obj[self.SCHOOL_ID] = self.school_id
        obj[self.SCHOOL_NAME] = self.school_name
        obj[self.USER_ROLE] = self.user_role
        obj[self.STATE] = self.state
        obj[self.DISTRICT_NAME] = self.district_name
        obj[self.SCHOOL_DATA] = self.school_data
        obj["level"] = getattr(self, "level", soc_constants.FREE)
        obj["proExpiration"] = getattr(self, "proExpiration", None)
        obj["licenseKey"] = getattr(self, "licenseKey", None)

        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        :param dictObj
        """

        model = SocrativeUserModel()
        model.id = dictObj.get(cls.ID)
        model.password = dictObj.get(cls.PASSWORD)
        model.last_login = dictObj.get(cls.LAST_LOGIN)
        model.email = dictObj.get(cls.EMAIL)
        model.is_active = dictObj.get(cls.IS_ACTIVE, True)
        model.date_joined = dictObj.get(cls.DATE_JOINED)
        model.register_date = dictObj.get(cls.REGISTER_DATE)
        model.display_name = dictObj.get(cls.DISPLAY_NAME, "")
        model.auth_token = dictObj.get(cls.AUTH_TOKEN)
        model.first_name = dictObj.get(cls.FIRST_NAME, "")
        model.last_name = dictObj.get(cls.LAST_NAME, "")
        model.legacy_id = dictObj.get(cls.LEGACY_ID)
        model.language = dictObj.get(cls.LANGUAGE, "en")
        model.tz_offset = dictObj.get(cls.TZ_OFFSET)
        model.organization_type = dictObj.get(cls.ORGANIZATION_TYPE)
        model.zip_code = dictObj.get(cls.ZIP_CODE)
        model.university = dictObj.get(cls.UNIVERSITY)
        model.country = dictObj.get(cls.COUNTRY)
        model.description = dictObj.get(cls.DESCRIPTION)
        model.school_id = dictObj.get(cls.SCHOOL_ID)
        model.school_name = dictObj.get(cls.SCHOOL_NAME)
        model.user_role = dictObj.get(cls.USER_ROLE)
        model.state = dictObj.get(cls.STATE)
        model.district_name = dictObj.get(cls.DISTRICT_NAME)
        model.school_data = dictObj.get(cls.SCHOOL_DATA)
        setattr(model, "level", dictObj.get("level", soc_constants.FREE))
        setattr(model, "proExpiration", dictObj.get("proExpiration", None))
        setattr(model, "licenseKey", dictObj.get("licenseKey", None))

        return model


class SocrativeUserDao(BaseDao):
    """

    """

    FETCH_MANY_SIZE = 100
    MODEL = SocrativeUserModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(SocrativeUserDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.user.load_user.time')
    def loadUser(self, auth_token):
        """
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT * FROM socrative_users_socrativeuser WHERE auth_token=%s""", (auth_token,))

                if cursor.rowcount <= 0:
                    raise self.NotFound

                userModel = self.fetchOneAsModel(cursor)

                # check for licenses
                cursor.execute("""SELECT l.expiration_date, l.key FROM licenses AS l INNER JOIN license_activations AS la """
                               """ ON la.license_id=l.id AND la.user_id=%s WHERE l.expiration_date + INTERVAL """
                               """'30 days' >= now() ORDER BY l.expiration_date DESC LIMIT 1""", (userModel.id,))

                if cursor.rowcount == 0:
                    setattr(userModel, "level", soc_constants.FREE)
                    setattr(userModel, "proExpiration", None)
                    setattr(userModel, "licenseKey", None)
                else:
                    result = cursor.fetchone()
                    setattr(userModel, "level", soc_constants.PREMIUM)
                    setattr(userModel, "proExpiration", result[0])
                    setattr(userModel, "licenseKey", result[1])

                return userModel

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except BaseDao.NotFound:
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.user.get_pro_expiration_date.time')
    def getProExpirationDate(self, userId):
        """

        :param userId:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                # check for licenses
                cursor.execute("""SELECT l.expiration_date FROM licenses AS l INNER JOIN license_activations AS la """
                               """ ON la.license_id=l.id AND la.user_id=%s WHERE l.expiration_date + INTERVAL """
                               """'30 days' >= now() ORDER BY l.expiration_date DESC LIMIT 1""", (userId,))
                if cursor.rowcount == 0:
                    return None

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.user.loadUserById.time')
    def loadUserById(self, userId, _type=BaseDao.TO_MODEL):
        """
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT * FROM socrative_users_socrativeuser WHERE id=%s""", (userId,))

                if cursor.rowcount <= 0:
                    raise self.NotFound

                if _type == BaseDao.TO_DICT:
                    return self.fetchOneAsDict(cursor)
                else:
                    return self.fetchOneAsModel(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.user.loadUserByEmail.time')
    def loadUserByEmail(self, email, _type=BaseDao.TO_MODEL):
        """
        load user by his email address
        :param email:
        :param _type:
        :return:
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT * FROM socrative_users_socrativeuser WHERE email=%s""", (email,))

                if cursor.rowcount <= 0:
                    raise self.NotFound

                if _type == BaseDao.TO_DICT:
                    userModel = self.fetchOneAsDict(cursor)
                else:
                    userModel = self.fetchOneAsModel(cursor)

                # check for licenses
                cursor.execute(
                    """SELECT l.expiration_date, l.key FROM licenses AS l INNER JOIN license_activations AS la """
                    """ ON la.license_id=l.id AND la.user_id=%s WHERE l.expiration_date + INTERVAL """
                    """'30 days' >= now() ORDER BY l.expiration_date DESC LIMIT 1""", (userModel.id,))

                if _type == BaseDao.TO_DICT:
                    if cursor.rowcount == 0:
                        userModel["level"] = 0
                        userModel["proExpiration"] = None
                        userModel["licenseKey"] = None
                    else:
                        result = cursor.fetchone()
                        userModel["level"] = soc_constants.PREMIUM
                        userModel["proExpiration"] = result[0]
                        userModel["licenseKey"] = result[1]
                else:
                    if cursor.rowcount == 0:
                        setattr(userModel, "level", soc_constants.FREE)
                        setattr(userModel, "proExpiration", None)
                        setattr(userModel, "licenseKey", None)
                    else:
                        result = cursor.fetchone()
                        setattr(userModel, "level", soc_constants.PREMIUM)
                        setattr(userModel, "proExpiration", result[0])
                        setattr(userModel, "licenseKey", result[1])

                return userModel
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.user.get_user_language.time')
    def getUserLanguage(self, userId):
        """
        load user by his email address
        :param userId:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT "language" FROM socrative_users_socrativeuser WHERE id=%s""", (userId,))

                if cursor.rowcount <= 0:
                    raise self.NotFound

                return cursor.fetchone()[0]
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.user.update_timezone.time')
    def updateTimezone(self, userId, timezone):
        """
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE socrative_users_socrativeuser SET tz_offset=%s WHERE id=%s""",
                               (timezone, userId))

                if cursor.rowcount <= 0:
                    raise self.NotUpdated

            self.connection.commit()

        except OperationalError as e:
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.user.update_last_login.time')
    def updateLastLogin(self, userId, date, auth_token=None):
        """

        :param userId:
        :param date:
        :param auth_token: new token
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                if auth_token is None:
                    cursor.execute("""UPDATE socrative_users_socrativeuser SET last_login=%s WHERE id=%s""",
                                   (date, userId))
                else:
                    cursor.execute("""UPDATE socrative_users_socrativeuser SET last_login=%s, auth_token=%s WHERE """
                                   """id=%s""", (date, auth_token, userId))

                if cursor.rowcount <= 0:
                    raise self.NotUpdated

            self.connection.commit()

            return auth_token

            # counter.populate()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.user.update_user.time')
    def updateUser(self, modelDict):
        """

        :param modelDict:
        :return:
        """
        try:
            if not modelDict.get(SocrativeUserModel.ID):
                raise Exception("SystemMessage missing id")

            mId = safeToInt(modelDict[SocrativeUserModel.ID])
            if mId is None or mId < 0:
                raise Exception("SystemMessage Id is not an integer value")

            with self.connection.cursor() as cursor:
                query = ",".join(["\""+key+"\""+"=%("+key+")s" for key in modelDict if key != SocrativeUserModel.ID and key != "level" and key != "licenseKey" and key != "proExpiration"])
                query = cursor.mogrify(query, modelDict)
                cursor.execute("""UPDATE socrative_users_socrativeuser SET """ + query + """ WHERE id=%d""" % mId)

                if cursor.rowcount != 1:
                    raise self.NotUpdated

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.user.update_password.time')
    def updatePassword(self, userId, password):
        """
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE socrative_users_socrativeuser SET password=%s WHERE id=%s""", (password,
                                                                                                        userId))

                if cursor.rowcount <= 0:
                    raise self.NotUpdated

            self.connection.commit()

            # counter.populate()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            raise


    @retry
    @statsd.timed('socrative.dao.user.update_email.time')
    def updateEmail(self, userId, newEmail):
        """
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE socrative_users_socrativeuser SET email=%s WHERE id=%s""", (newEmail,
                                                                                                     userId))

                if cursor.rowcount <= 0:
                    raise self.NotUpdated

            self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            raise

    @retry
    @statsd.timed('socrative.dao.user.create_user.time')
    def createUser(self, modelDict):
        """
        creates an user in the database
        :param modelDict
        """

        if modelDict[SocrativeUserModel.AUTH_TOKEN] is None:
            raise BaseDao.NotInserted("Auth token is missing")

        if modelDict[SocrativeUserModel.PASSWORD] is None:
            raise BaseDao.NotInserted("Password is missing")

        error = None
        try:
            with self.connection.cursor() as cursor:
                query = ",".join(["\"%s\"" % key for key in modelDict if key != SocrativeUserModel.ID and key != "level" and key != "licenseKey" and key != "proExpiration"])
                values = ",".join(["%("+key+")s" for key in modelDict if key != SocrativeUserModel.ID and key != "level" and key != "licenseKey" and key != "proExpiration"])
                values = cursor.mogrify(values, modelDict)

                cursor.execute("""INSERT INTO socrative_users_socrativeuser (""" + query + """) VALUES (""" + values +
                               """) RETURNING id""")

                if cursor.rowcount != 1:
                    raise self.NotInserted

                modelDict[SocrativeUserModel.ID] = cursor.fetchone()[0]

                return SocrativeUserModel.fromDict(modelDict)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.user.check_email.time')
    def checkEmail(self, email):
        """
        creates an user in the database
        :param email:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT id FROM socrative_users_socrativeuser WHERE email=%s""", (email.lower(),))

                if cursor.rowcount >= 1:
                    return False

                return True

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.user.update_password_and_language.time')
    def updatePasswordAndLanguage(self, userId, password, language):
        """
        """
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE socrative_users_socrativeuser SET "password"=%s,"language"=%s WHERE id=%s""",
                               (password, language, userId))

                if cursor.rowcount <= 0:
                    raise self.NotUpdated

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.user.get_user_accounting_data.time')
    def getUserAccountingData(self, user_ids):
        """

        :param user_ids:
        :return:
        """
        if not user_ids:
            return dict()

        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute(
                    """SELECT id, first_name, last_name, email, register_date, country, organization_type, school_name"""
                    """, university, description, state, zip_code, user_role from socrative_users_socrativeuser where """
                    """id IN %s""", (tuple(user_ids),))

                return dict(
                    (row[0], (
                        row[1],
                        row[2],
                        row[3],
                        row[4],
                        row[5],
                        row[6],
                        row[7],
                        row[8],
                        row[9],
                        row[10],
                        row[11],
                        row[12]
                    )) for row in cursor.fetchall())

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.user.get_email_for_user_ids.time')
    def getEmailForUserIds(self, userIds, orgType=None):
        """

        :param userIds:
        :return:
        """
        if not userIds:
            return dict()

        try:
            with self.readOnlyConnection.cursor() as cursor:
                query = cursor.mogrify("""SELECT id, email, organization_type from socrative_users_socrativeuser where """
                                       """id IN %s""", (tuple(userIds),))
                if orgType and orgType == 'K12':
                    query += cursor.mogrify(""" AND organization_type='K12""")
                elif orgType and orgType != 'K12':
                    query += cursor.mogrify(""" AND organization_type != 'K12'""")

                cursor.execute(query)

                return dict((row[0], (row[1], row[2])) for row in cursor.fetchall())

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.user.get_last_logins.time')
    def getLastLogins(self, start_date, end_date):
        """

        :param start_date:
        :param end_date:
        :return:
        """
        if not start_date:
            return dict()

        error = None
        try:
            with self.readOnlyConnection.cursor() as cursor:
                query = cursor.mogrify("""select id, last_login from socrative_users_socrativeuser where"""
                                       """ last_login >= %s""", (start_date,))
                if end_date:
                    query += cursor.mogrify(""" AND last_login < %s""", (end_date,))

                cursor.execute(query)

                return dict((row[0], row[1]) for row in cursor.fetchall())

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.user.get_last_login_details.time')
    def getLastLoginDetails(self, user_ids, org_type, query_type):
        """

        :param userIds:
        :return:
        """
        if not user_ids:
            return dict()

        try:
            with self.readOnlyConnection.cursor() as cursor:
                if query_type == 'email_list':
                    query = cursor.mogrify("""SELECT id, first_name, last_name, email, organization_type FROM  """
                                           """socrative_users_socrativeuser WHERE id IN %s""", (tuple(user_ids),))
                elif query_type == 'count':
                    query = cursor.mogrify("""SELECT COUNT(id) FROM socrative_users_socrativeuser WHERE id IN %s""",
                                           (tuple(user_ids),))
                else:
                    return dict()

                if org_type and org_type == 'k12':
                    query += cursor.mogrify(""" AND organization_type='K12'""")
                elif org_type and org_type == 'non-k12':
                    query += cursor.mogrify(""" and organization_type != 'K12'""")

                cursor.execute(query)
                if query_type == 'email_list':
                    return dict((row[0], row[1]) for row in cursor.fetchall())
                else:
                    return {"count": cursor.fetchone()[0]}

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise


class TempTokenModel(BaseModel):
    """
    class to handle temporary token storage
    """

    CHANGE_PASSWORD = "CP"
    FIRST_PASSWORD = "FP"

    ID = "id"
    TEMP_TOKEN = "temp_token"
    PURPOSE = "purpose"
    CREATED_BY_ID = "created_by_id"

    def __init__(self):
        """
        Constructor
        :return:
        """
        self.id = None
        self.temp_token = None
        self.purpose = False
        self.created_by_id = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.TEMP_TOKEN] = self.temp_token
        obj[self.CREATED_BY_ID] = self.created_by_id
        obj[self.PURPOSE] = self.purpose
        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = TempTokenModel()
        model.id = dictObj.get(cls.ID)
        model.temp_token = dictObj.get(cls.TEMP_TOKEN)
        model.created_by_id = dictObj.get(cls.CREATED_BY_ID)
        model.purpose = dictObj.get(cls.PURPOSE)

        return model


class TempTokenDao(BaseDao):
    """

    """

    FETCH_MANY_SIZE = 100
    MODEL = TempTokenModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(TempTokenDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.temp_token.delete_all_by_user.time')
    def deleteAllByUser(self, userId, purpose):
        """

        :param userId
        :param purpose
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""DELETE FROM socrative_users_temptoken WHERE created_by_id=%s AND purpose=%s""",
                    (userId, purpose))

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.temp_token.create_token.time')
    def createToken(self, modelDict):
        """
        creates a record in the database for the temp token
        :param modelDict: dictionary corresponding to a TempTokenModel
        :return None
        """

        try:
            with self.connection.cursor() as cursor:
                query = ",".join(["\"%s\"" % key for key in modelDict if key != TempTokenModel.ID])
                values = ",".join(["%("+key+")s" for key in modelDict if key != TempTokenModel.ID])
                values = cursor.mogrify(values, modelDict)

                cursor.execute("""INSERT INTO socrative_users_temptoken (""" + query + """) VALUES (""" + values +
                               """)""")

                if cursor.rowcount != 1:
                    raise self.NotInserted("temp token not inserted in the database")

            self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.temp_token.get_token.time')
    def getToken(self, tokenStr):
        """
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * from socrative_users_temptoken WHERE temp_token=%s""", (tokenStr,))
                return self.fetchOneAsModel(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.temp_token.delete_token.time')
    def deleteToken(self, tokenId):
        """
        delete a temp token using the id
        :param tokenId: 
        :return: 
        """
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""DELETE FROM socrative_users_temptoken WHERE id=%s""", (tokenId,))

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            raise


class SystemMessageModel(BaseModel):
    """

    """

    ID = "id"
    CONTENT = "content"
    TITLE = "title"
    URL = "url"
    ACTION_TYPE = "action_type"
    START_DATE = "start_date"
    END_DATE = "end_date"
    PRIORITY = "priority"
    DISMISSIBLE = "dismissible"
    STATUS = "status"
    AUDIENCE = "audience"

    ACTIVE = 0
    HIDDEN = 64

    ALL = 0
    FREE = 1
    PRO = 2

    def __init__(self):
        """
        Constructor
        :return:
        """
        self.id = None
        self.content = None
        self.title = None
        self.url = None
        self.start_date = None
        self.end_date = None
        self.priority = None
        self.dismissible = None
        self.status = None
        self.action_type = None
        self.audience = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.CONTENT] = self.content
        obj[self.TITLE] = self.title
        obj[self.URL] = self.url
        obj[self.START_DATE] = self.start_date
        obj[self.END_DATE] = self.end_date
        obj[self.PRIORITY] = self.priority
        obj[self.DISMISSIBLE] = self.dismissible
        obj[self.STATUS] = self.status
        obj[self.ACTION_TYPE] = self.action_type
        obj[self.AUDIENCE] = self.audience

        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = SystemMessageModel()
        model.id = dictObj.get(cls.ID)
        model.content = dictObj.get(cls.CONTENT)
        model.title = dictObj.get(cls.TITLE)
        model.url = dictObj.get(cls.URL)
        model.start_date = dictObj.get(cls.START_DATE)
        model.end_date = dictObj.get(cls.END_DATE)
        model.priority = dictObj.get(cls.PRIORITY)
        model.dismissible = dictObj.get(cls.DISMISSIBLE)
        model.status = dictObj.get(cls.STATUS)
        model.action_type = dictObj.get(cls.ACTION_TYPE)
        model.audience = dictObj.get(cls.AUDIENCE)

        return model


class SystemMessageDao(BaseDao):
    """

    """
    FETCH_MANY_SIZE = 100
    MODEL = SystemMessageModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(SystemMessageDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.system_message.create.time')
    def create(self, model):
        """
        """

        if model is None or type(model) is not dict:
            raise ValueError("model must be a system message model dictionary")

        error = None
        try:
            with self.connection.cursor() as cursor:
                query = ",".join(["\"%s\"" % key for key in model if key != SystemMessageModel.ID])
                values = ",".join(["%("+key+")s" for key in model if key != SystemMessageModel.ID])
                values = cursor.mogrify(values, model)

                cursor.execute("""INSERT INTO socrative_users_systemmessage (""" + query + """) VALUES (""" + values + """) """
                               """RETURNING id""")

                return cursor.fetchone()[0]
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.system_message.hide_last_message.time')
    def hideLastMessage(self, audience):
        """
        hides the last system message
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                if audience == 0:
                    cursor.execute("""UPDATE socrative_users_systemmessage set status=%s where status != %s""",
                                   (SystemMessageModel.HIDDEN, SystemMessageModel.HIDDEN))
                else:
                    cursor.execute("""UPDATE socrative_users_systemmessage set status=%s where audience=%s and """
                                   """status != %s""", (SystemMessageModel.HIDDEN, audience, SystemMessageModel.HIDDEN))

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.system_message.load_last_message.time')
    def loadLastMessage(self, level=0):
        """
        load the last system message
        :param level:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM socrative_users_systemmessage WHERE status=%s AND (audience=%s OR """
                               """ audience=0) ORDER BY id DESC LIMIT 1""", (SystemMessageModel.ACTIVE, level))

                if cursor.rowcount <= 0:
                    return None

                return self.fetchOneAsModel(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()


class UserSystemMessageModel(BaseModel):
    """

    """
    ID = "id"
    USER_ID = "user_id"
    STATUS = "status"
    SYS_MESSAGE_ID = "sys_message_id"
    SHOW_DATE = "show_date"

    UNREAD = 0
    SHOW = 1
    READ = 16
    REMIND_LATER = 64
    DISMISSED = 256

    def __init__(self):
        """
        Constructor
        :return:
        """
        self.id = None
        self.user_id = None
        self.status = False
        self.sys_message_id = None
        self.show_date = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.USER_ID] = self.user_id
        obj[self.STATUS] = self.status
        obj[self.SYS_MESSAGE_ID] = self.sys_message_id
        obj[self.SHOW_DATE] = self.show_date
        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = UserSystemMessageModel()
        model.id = dictObj.get(cls.ID)
        model.user_id = dictObj.get(cls.USER_ID)
        model.status = dictObj.get(cls.STATUS)
        model.sys_message_id = dictObj.get(cls.SYS_MESSAGE_ID)
        model.show_date = dictObj.get(cls.SHOW_DATE)

        return model


class UserSystemMessageDao(BaseDao):
    """

    """
    FETCH_MANY_SIZE = 100
    MODEL = UserSystemMessageModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(UserSystemMessageDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)


    @retry
    @statsd.timed('socrative.dao.system_message.load_message.time')
    def loadMessage(self, sys_msg_id, user_id):
        """

        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * from socrative_users_usersysmsg WHERE sys_message_id=%s and user_id=%s """
                               """ ORDER BY id DESC LIMIT 1""", (sys_msg_id, user_id))

                if cursor.rowcount <= 0:
                    return None

                return self.fetchOneAsModel(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.system_message.create_user_message.time')
    def createUserMessage(self, model):
        """
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""INSERT INTO socrative_users_usersysmsg ("user_id","status","""
                               """ "sys_message_id", "show_date") VALUES (%s,%s,%s,%s) RETURNING id""",
                               (model.user_id, model.status, model.sys_message_id, model.show_date))

                if cursor.rowcount <= 0:
                    return None

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.system_message.update_status.time')
    def updateStatus(self, old_status, new_status, user_id):
        """
        updates the status of the user system message
        :param old_status:
        :param new_status:
        :param user_id:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                if new_status != UserSystemMessageModel.DISMISSED:
                    cursor.execute("""UPDATE socrative_users_usersysmsg set status=%s where user_id=%s and id=(select"""
                               """ max(id) from socrative_users_usersysmsg where user_id=%s) and status=%s""",
                                   (new_status, user_id, user_id, old_status))
                else:
                    # for dismissed we need to check that the system message is dismissible,
                    # otherwise transform it in a remind me later status, so it is shown at the next login
                    cursor.execute("""SELECT suus.id, susm.dismissible FROM socrative_users_usersysmsg as suus """
                                   """ INNER JOIN socrative_users_systemmessage as susm on susm.id=suus.sys_message_id"""
                                   """ WHERE suus.user_id=%s and suus.status=%s ORDER BY suus.id DESC LIMIT 1""",
                                   (user_id,
                                    old_status))

                    if cursor.rowcount == 1:
                        user_msg_id, dismissible = cursor.fetchone()
                        if dismissible is False:
                            new_status = UserSystemMessageModel.REMIND_LATER

                        cursor.execute("""update socrative_users_usersysmsg set status=%s where id=%s""",
                                           (new_status, user_msg_id))

                self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.system_message.update.time')
    def update(self, usr_msg_id, status, user_id):
        """

        :param usr_msg_id:
        :param status:
        :param user_id:
        :return:
        """
        try:
            with self.connection.cursor() as cursor:
                if status != UserSystemMessageModel.DISMISSED:
                    cursor.execute("""UPDATE socrative_users_usersysmsg set status=%s where id=%s and user_id=%s""",
                                   (status, usr_msg_id, user_id))
                else:
                    # for dismissed we need to check that the system message is dismissible,
                    # otherwise transform it in a remind me later status, so it is shown at the next login
                    cursor.execute("""SELECT susm.dismissible FROM socrative_users_usersysmsg as suus INNER JOIN """
                                   """socrative_users_systemmessage as susm on susm.id=suus.sys_message_id """
                                   """WHERE suus.id=%s""",
                                   (usr_msg_id,))

                    if cursor.rowcount == 1:
                        dismissible = cursor.fetchone()[0]
                        if dismissible is False:
                            status = UserSystemMessageModel.REMIND_LATER

                        cursor.execute("""update socrative_users_usersysmsg set status=%s where id=%s""",
                                       (status, usr_msg_id))

                self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise


class ScoreSettingsModel(BaseModel):
    """

    """

    ID = "id"
    TEACHER_ID = "teacher_id"
    KEY = "key"
    NEXT_LINE = "next_line"
    SPEED = "speed"
    PREFIX = "prefix"
    METHOD = "method"

    # next line
    ENTER = 1
    ENTER_TWICE = 2
    TAB = 3
    TAB_TWICE = 4
    DOWN_ARROW = 5

    # speed
    FAST = 50
    MEDIUM = 150
    SLOW = 300
    VERY_SLOW = 500

    # method
    NUMERIC = 1
    PERCENTAGE = 2

    def __init__(self):
        """
        Constructor
        :return:
        """
        self.id = None
        self.teacher_id = None
        self.key = None
        self.next_line = None
        self.speed = None
        self.prefix = None
        self.method = None

    def toDict(self):
        """
        model object to dict method
        :return:
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.TEACHER_ID] = self.teacher_id
        obj[self.KEY] = self.key
        obj[self.NEXT_LINE] = self.next_line
        obj[self.SPEED] = self.speed
        obj[self.PREFIX] = self.prefix
        obj[self.METHOD] = self.method

        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        from dict to model object method
        :param dictObj:
        :return:
        """

        obj = ScoreSettingsModel()
        obj.id = dictObj.get(cls.ID)
        obj.teacher_id = dictObj.get(cls.TEACHER_ID)
        obj.key = dictObj.get(cls.KEY)
        obj.next_line = dictObj.get(cls.NEXT_LINE)
        obj.speed = dictObj.get(cls.SPEED)
        obj.prefix = dictObj.get(cls.PREFIX)
        obj.method = dictObj.get(cls.METHOD)

        return obj

    @classmethod
    def fromRequestDto(cls, dtoModel):
        """
        from dict to model object method
        :param dtoModel:
        :return:
        """

        obj = ScoreSettingsModel()
        obj.id = dtoModel.pk
        obj.key = dtoModel.key
        obj.next_line = dtoModel.next_line
        obj.speed = dtoModel.speed
        obj.prefix = dtoModel.prefix
        obj.method = dtoModel.method

        return obj


class ScoreSettingsDao(BaseDao):
    """

    """

    FETCH_MANY_SIZE = 100
    MODEL = ScoreSettingsModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(ScoreSettingsDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.score_settings.get_score_settings.time')
    def getScoreSettings(self, teacher_id):
        """

        :param teacher_id:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * from socrative_users_scoreexportsettings WHERE teacher_id=%s""",
                               (teacher_id,))

                if cursor.rowcount == 0:
                    return None

                return self.fetchOneAsDict(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.score_settings.exists.time')
    def exists(self, teacher_id, export_settings_id=None):
        """

        :param teacher_id:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                if export_settings_id is None:
                    cursor.execute("""SELECT id from socrative_users_scoreexportsettings WHERE teacher_id=%s""",
                                   (teacher_id,))
                else:
                    cursor.execute("""SELECT id from socrative_users_scoreexportsettings WHERE teacher_id=%s and id=%s""",
                                   (teacher_id, export_settings_id))

                if cursor.rowcount == 1:
                    return True

                return False

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.score_settings.create.time')
    def create(self, dictModel):
        """

        :param dictModel:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                query = ",".join(["\"%s\"" % key for key in dictModel if key != ScoreSettingsModel.ID])
                values = ",".join(["%(" + key + ")s" for key in dictModel if key != ScoreSettingsModel.ID])
                values = cursor.mogrify(values, dictModel)

                cursor.execute("""INSERT INTO socrative_users_scoreexportsettings (""" + query + """) VALUES (""" + \
                               values + """) RETURNING id""")

                if cursor.rowcount != 1:
                    raise self.NotInserted

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.score_settings.update.time')
    def update(self, exportSettingsDict):
        """

        :param exportSettingsDict:
        :return:
        """
        conn = None
        try:
            if not exportSettingsDict.get(ScoreSettingsModel.ID):
                raise Exception("Export score data missing id")

            sId = safeToInt(exportSettingsDict[ScoreSettingsModel.ID])
            if sId is None or sId < 0:
                raise Exception("Export score model Id is not an integer value")

            with self.connection.cursor() as cursor:
                query = ",".join(["\"" + key + "\"" + "=%(" + key + ")s" for key in exportSettingsDict if
                                  key != ScoreSettingsModel.ID])
                query = cursor.mogrify(query, exportSettingsDict)
                conn = 1
                cursor.execute("""UPDATE socrative_users_scoreexportsettings SET """ + query + """ WHERE id=%d""" % sId)

                if cursor.rowcount != 1:
                    raise self.NotUpdated

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            self.connection = None
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            if conn:
                self.connection.rollback()
            raise