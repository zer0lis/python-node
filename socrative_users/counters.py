# coding=utf-8


class UserCounters(object):
    """
    class to hold the user counter names
    """

    SALESFORCE_POST_OK = 'socrative.views.registration.salesforce.time'
    SALESFORCE_POST_FAIL = 'socrative.views.registration.salesforce.fail'
    SALESFORCE_INTERNAL_SERVER_ERROR = 'socrative.views.registration.salesforce.http500'

    GOOGLE_LOGIN_FAIL = 'socrative.views.registration.google_login.fail'
    GOOGLE_LOGIN_OK = 'socrative.views.registration.google_login.ok'

    REGISTER_USER = 'socrative.views.users.register_user.time'
    REGISTER_USER_FAIL = 'socrative.views.users.register_user.fail'
    LOGIN_USER = 'socrative.views.users.login.time'
    LOGIN_USER_FAIL = 'socrative.views.users.login.fail'
    AUTHENTICATE_USER = 'socrative.views.users.authenticate_user.time'
    AUTHENTICATE_USER_FAIL = 'socrative.views.users.authenticate_user.fail'
    CHECK_EMAIL = 'socrative.views.users.check_email.time'
    CHECK_EMAIL_FAIL = 'socrative.views.users.check_email.fail'
    GOOGLE_LOGIN = 'socrative.views.users.google_login.time'
    CHANGE_PASSWORD_FAIL = 'socrative.views.users.change_password.fail'
    CHANGE_PASSWORD = 'socrative.views.users.change_password.time'
    RESET_PASSWORD_BY_MAIL_FAIL = 'socrative.views.users.reset_password_mail.fail'
    RESET_PASSWORD_BY_MAIL = 'socrative.views.users.reset_password_mail.time'

    ACCOUNTS_LOCKED_1_DAY = 'socrative.views.login.accounts_locked_1_day'
    ACCOUNTS_LOCKED_1_HOUR = 'socrative.views.login.accounts_locked_1_hour'
    ACCOUNTS_LOCKED_5_MINUTES = 'socrative.views.login.accounts_locked_5_minutes'
    IPS_BLOCKED = 'socrative.views.login.ips_blocked'

    MC_LOGIN_FAIL = 'socrative.views.users.mc_login.fail'
    MC_LOGIN = 'socrative.views.users.mc_login.time'
    IMPORT_QUIZ_FOR_USER = 'socrative.views.users.import_quiz.time'
    IMPORT_QUIZ_FOR_USER_FAIL = 'socrative.views.users.import_quiz.fail'
    UPDATE_INFO_BANNER_FAIL = 'socrative.views.users.update_info_banner.fail'
    UPDATE_INFO_BANNER = 'socrative.views.users.update_info_banner.time'
    GET_ACCOUNT_STATUS_FAIL = 'socrative.views.users.get_account_status.fail'
    GET_ACCOUNT_STATUS = 'socrative.views.users.get_account_status.time'
    LOGOUT = 'socrative.views.users.logout.time'
    ZENDESK_LOGIN = 'socrative.views.users.zendesk_sso.time'
    ZENDESK_LOGIN_FAIL = 'socrative.views.users.zendesk_sso.fail'


