# -*- coding: utf-8 -*-
import base64
import datetime
import hmac
import logging
import random
import string
import time
import ujson
import urllib
import urllib2
from hashlib import sha256

import pytz
import requests
from django.contrib.auth.hashers import PBKDF2PasswordHasher
from django.utils.translation import ugettext
from psycopg2 import IntegrityError
from datadog import statsd

from common import http_status as status
from common import soc_constants
from common.base_dao import BaseDao
from common.base_services import BaseService
from common.cryptography import Cryptography
from common.dao import PartnerModel
from common.socrative_api import exceptionStack, isStringNotEmpty, safeToInt, isInteger, isString, isEmailValid
from common.socrative_config import ConfigRegistry
from common.socrative_errors import BaseError
from lecturers.dao import NotificationModel
from socrative import settings as projSettings
from socrative_users.serializers import User, UserSystemMessage
from socrative_users.counters import UserCounters
from socrative_users.dao import SocrativeUserModel, UserSystemMessageModel, TempTokenModel, SystemMessageModel
from workers.task_types import TaskType, EmailTypes

logger = logging.getLogger(__name__)


class SocrativeUsersServices(BaseService):
    """
    class that for now just initializes the dao counters
    """

    MIN_PASSWORD_LENGTH = 8
    DISPLAY_NAME_LENGTH = 9
    PBK_ITERATIONS = 12000

    languages = [a[0] for a in projSettings.LANGUAGES]

    def __init__(self, daoRegistry, cacheRegistry, ipLimiter, emailLimiter):
        """
        Constructor
        :return:
        """
        super(SocrativeUsersServices, self).__init__(daoRegistry, cacheRegistry)

        self.ADMIN_MARKETING_URL = None

        self.cookie_teacher_name = "com.socrative.app.user_data_teacher"
        self.passwordHasher = PBKDF2PasswordHasher()
        self.temp_token_duration = None
        self.reset_pwd_url = None
        self.admin_email_list = None
        self.SALESFORCE_URL = None
        self.SALESFORCE_OID = None
        self.SALESFORCE_ORG_TYPE = None
        self.SALESFORCE_SCHOOL_NAME = None
        self.SALESFORCE_ROLE = None
        self.SALESFORCE_WEB_LEAD = None
        self.SALESFORCE_WEB_SOURCE = None
        self.httpClient = requests.session()
        self.ipLimiter = ipLimiter
        self.emailLimiter = emailLimiter
        self.__zendesk_sso_key = ConfigRegistry.getItem("ZENDESK_SSO_KEY")
        self.__zendesk_redirect_url = ConfigRegistry.getItem("ZENDESK_SSO_REDIRECT_URL")
        self.BACKEND_ADMIN_KEY = None

        self.alphabet = string.digits + string.letters

    def destroy(self):
        """

        :return:
        """
        self.httpClient.close()

    def after_properties_set(self):
        """

        :return:
        """

        self.temp_token_duration = safeToInt(ConfigRegistry.getItem("TEMPORARY_TOKEN_DURATION")) or 8  # or 8 hours
        self.reset_pwd_url = ConfigRegistry.getItem("RESET_PASSWORD_URL")
        self.admin_email_list = ConfigRegistry.getItem("ADMIN_EMAIL_LIST") or []
        self.SALESFORCE_URL = ConfigRegistry.getItem("SALESFORCE_URL")
        self.SALESFORCE_OID = ConfigRegistry.getItem("SALESFORCE_OID")
        self.SALESFORCE_ORG_TYPE = ConfigRegistry.getItem("SALESFORCE_ORG_TYPE")
        self.SALESFORCE_SCHOOL_NAME = ConfigRegistry.getItem("SALESFORCE_SCHOOL_NAME")
        self.SALESFORCE_ROLE = ConfigRegistry.getItem("SALESFORCE_ROLE")
        self.SALESFORCE_WEB_LEAD = ConfigRegistry.getItem("SALESFORCE_WEB_LEAD")
        self.SALESFORCE_WEB_SOURCE = ConfigRegistry.getItem("SALESFORCE_WEB_SOURCE")
        self.BACKEND_ADMIN_KEY = ConfigRegistry.getItem("BACKEND_ADMIN_API_KEY")

        self.ADMIN_MARKETING_URL = ConfigRegistry.getItem("ADMIN_MARKETING_URL")

    def salesforceApi(self, model, roles, school_data, mc_demo_requested, phone_number):
        """
        :param roles:
        :param model: SocrativeUser model
        :type model: SocrativeUserModel
        :param roles:
        :param school_data:
        :param mc_demo_requested:
        :param phone_number:
        :return:
        """

        dt = time.time()
        lead_source_string = 'Socrative Request Demo'

        if mc_demo_requested is not True:
            lead_source_string = 'Socrative admin signups'

        role_list = roles
        if "administrator" in roles:
            role_list = ["Administrator"]
        elif "it-technology" in roles:
            role_list = ["IT-Technology"]

        data = {"first_name": model.first_name,
                "last_name": model.last_name,
                "email": model.email,
                "country": model.country,
                "zip": model.zip_code,
                "state": model.state,
                "oid": self.SALESFORCE_OID,
                "lead_source": lead_source_string,
                self.SALESFORCE_WEB_SOURCE: 'Socrative',
                self.SALESFORCE_ORG_TYPE: model.organization_type,
                self.SALESFORCE_SCHOOL_NAME: model.school_name,
                self.SALESFORCE_ROLE: role_list,
                self.SALESFORCE_WEB_LEAD: 1}

        if phone_number:
            data["phone"] = phone_number

        try:
            if school_data and "lea_id" in school_data and "nces_school_id" in school_data:
                data["00NU0000003oYDd"] = int(str(school_data["lea_id"]) + str(school_data["nces_school_id"]).zfill(5))

            resp = self.httpClient.post(
                self.SALESFORCE_URL,
                data=data,
                headers={"Content-Type": "application/x-www-form-urlencoded"}
            )

            duration = time.time() - dt

            if resp.status_code == 200:
                statsd.histogram(UserCounters.SALESFORCE_POST_OK, duration)
            else:
                statsd.increment(UserCounters.SALESFORCE_POST_FAIL)
        except Exception as e:
            statsd.increment(UserCounters.SALESFORCE_INTERNAL_SERVER_ERROR)
            logger.error(exceptionStack(e))
            pass

    @classmethod
    def __check_registration_data(cls, data):
        """

        :param data:
        :return:
        """

        firstName = data.get("first_name")
        lastName = data.get("last_name")
        orgType = data.get("org_type")
        roles = data.get("role", [])

        fieldList = list()
        errorList = [BaseError.FIRST_NAME_IS_INVALID, BaseError.LAST_NAME_IS_INVALID,
                     BaseError.ORGANIZATION_TYPE_INVALID, BaseError.INVALID_ROLES_SENT]
        if firstName is None:
            fieldList.append(-1)
        else:
            fieldList.append(0)

        if lastName is None:
            fieldList.append(-1)
        else:
            fieldList.append(0)

        if orgType is None:
            fieldList.append(-1)
        else:
            fieldList.append(0)

        if not roles:
            fieldList.append(-1)
        else:
            fieldList.append(0)

        if sum(fieldList) != -4 and sum(fieldList) != 0:
            index = -1
            for a in fieldList:
                index += 1
                if a == -1:
                    break
            return errorList[index]

        fieldList = list()

        if not isStringNotEmpty(firstName):
            fieldList.append(-1)
        else:
            fieldList.append(0)

        if not isStringNotEmpty(lastName):
            fieldList.append(-1)
        else:
            fieldList.append(0)

        if not isStringNotEmpty(orgType):
            fieldList.append(-1)
        else:
            fieldList.append(0)

        if not roles:
            fieldList.append(-1)
        else:
            fieldList.append(0)

        if sum(fieldList) != -4 and sum(fieldList) != 0:
            index = -1
            for a in fieldList:
                index += 1
                if a == -1:
                    break
            return errorList[index]

    @staticmethod
    def is_user_premium(userModel):
        """

        :param userModel:
        :return:
        """
        now = pytz.utc.localize(datetime.datetime.utcnow())

        if userModel.level != soc_constants.PREMIUM or\
                (userModel.level == soc_constants.PREMIUM and
                             userModel.proExpiration + datetime.timedelta(days=30) < now):
            return False

        return True

    def registerUser(self, request, dataDict):
        """
        handle for HTTP POST request for register user
        :param request:
        :param dataDict:
        :return:
        """

        try:
            email = dataDict.get("email", "").strip()
            password = dataDict.get("password")
            firstName = dataDict.get("first_name")
            lastName = dataDict.get("last_name")
            phone_number = dataDict.get("phone_number")

            if phone_number is not None and type(phone_number) in (str, unicode) \
                    and (len(phone_number) < 13 or len(phone_number) > 18):
                return BaseError.INVALID_PHONE_NUMBER, status.HTTP_400_BAD_REQUEST, None

            orgType = dataDict.get("org_type")
            country = dataDict.get("country")
            mc_demo = dataDict.get("mc_demo")

            # keep compatibility between client app ( orgName was sent before
            school_name = dataDict.get("school_name")
            if school_name is None:
                school_data = dataDict.get("school")
                school_name = school_data.get("school_name", "") if school_data else ""
            organization_zip = dataDict.get("org_zip") or dataDict.get("zip", "")
            organization_id = dataDict.get("org_id")
            district_name = dataDict.get("district_name")

            if district_name is None:
                school_data = dataDict.get("school")
                district_name = school_data.get("district_name", "") if school_data else ""

            organization_state = dataDict.get("org_state")
            if organization_state is None:
                school_data = dataDict.get("school")
                organization_state = school_data.get("state_code", "") if school_data else ""

            school_data = dataDict.get("school_data", "")
            org_name = dataDict.get("org_name")

            role = dataDict.get("role", [])

            language = request.COOKIES.get(projSettings.LANGUAGE_COOKIE_NAME)

            if not isStringNotEmpty(email) or not isEmailValid(email):
                return BaseError.INVALID_EMAIL_ADDRESS, status.HTTP_400_BAD_REQUEST, None, None

            if not isStringNotEmpty(password) or len(password) < self.MIN_PASSWORD_LENGTH:
                return BaseError.INVALID_PASSWORD, status.HTTP_400_BAD_REQUEST, None, None

            if isStringNotEmpty(firstName) is False:
                return BaseError.FIRST_NAME_IS_INVALID, status.HTTP_400_BAD_REQUEST, None, None

            if isStringNotEmpty(lastName) is False:
                return BaseError.LAST_NAME_IS_INVALID, status.HTTP_400_BAD_REQUEST, None, None

            if orgType:
                orgType = [a[0] for a in SocrativeUserModel.organization_choices if orgType in a]
                if orgType:
                    orgType = orgType[0]
                else:
                    orgType = None

            if orgType is None:
                return BaseError.ORGANIZATION_TYPE_INVALID, status.HTTP_400_BAD_REQUEST, None, None

            error = self.__check_registration_data(dataDict)

            if error is not None:
                return error, status.HTTP_400_BAD_REQUEST, None, None

            # TODO move this into the generic validator when it will be done
            good_roles = []
            if role:
                sRoles = ["teacher", "administrator", "it-technology", "other"]
                good_roles = list(set(sRoles) & set(role))

            if role and not good_roles:
                return BaseError.INVALID_ROLES_SENT, status.HTTP_400_BAD_REQUEST, None, None

            if orgType == "K12":

                if not isStringNotEmpty(school_name):
                    return BaseError.INVALID_SCHOOL_NAME, status.HTTP_400_BAD_REQUEST, None, None

                # check country
                if country in "USA":
                    if not isStringNotEmpty(organization_zip):
                        return BaseError.INVALID_ZIP_CODE, status.HTTP_400_BAD_REQUEST, None, None

                    if type(organization_id) not in (int, long):
                        return BaseError.INVALID_ORGRANIZATION_ID, status.HTTP_400_BAD_REQUEST, None, None

                    # if the org id is not -1, meaning the teacher chose a school from the dropdown
                    if organization_id != -1 and not isString(district_name):
                        return BaseError.INVALID_DISTRICT_NAME, status.HTTP_400_BAD_REQUEST, None, None

            elif orgType == "USHE":
                # check if we received the university name
                # independent of country
                if not isStringNotEmpty(org_name):
                    return BaseError.INVALID_SCHOOL_NAME, status.HTTP_400_BAD_REQUEST, None, None

                if country in "USA":
                    if not isStringNotEmpty(organization_state):
                        return BaseError.INVALID_STATE, status.HTTP_400_BAD_REQUEST, None, None

            elif orgType == "OTHR" or orgType == "CORP":
                # if org is corp the name comes in description
                if not isStringNotEmpty(org_name):
                    return BaseError.INVALID_SCHOOL_NAME, status.HTTP_400_BAD_REQUEST, None, None
            else:
                return BaseError.ORGANIZATION_TYPE_INVALID, status.HTTP_400_BAD_REQUEST, None, None

            if password and password != "":
                email = email.lower() if type(email) in [unicode, str] else None
                ok = False
                try:
                    user = self.daoRegistry.usersDao.loadUserByEmail(email)
                    if user.password == "":
                        # if user logged in with mastery connect, create an account
                        isPartner = self.daoRegistry.partnerDao.checkPartner(user.id, PartnerModel.MASTERY_CONNECT)
                        # if it's MC
                        if isPartner:
                            # update password
                            user.password = self.passwordHasher.encode(password, self.passwordHasher.salt(),
                                                                       self.PBK_ITERATIONS)
                            languages = [l[0] for l in projSettings.LANGUAGES]
                            # update language
                            user.language = language if language in languages else "en"
                            self.daoRegistry.usersDao.updatePasswordAndLanguage(user.id, password, language)
                            ok = True
                            self.cacheRegistry.userModelCache.invalidateData(user.auth_token)

                except BaseDao.NotFound:
                    user = SocrativeUserModel()
                    user.password = self.passwordHasher.encode(password, self.passwordHasher.salt(),
                                                               self.PBK_ITERATIONS)
                    user.auth_token = Cryptography.generate_auth_token()
                    user.display_name = Cryptography.generate_random_password(self.MIN_PASSWORD_LENGTH)

                    user.email = email if email != "" else user.email
                    user.first_name = firstName or ""
                    user.last_name = lastName or ""
                    user.organization_type = orgType
                    user.zip_code = organization_zip if organization_zip != "" else None
                    user.university = org_name if orgType == "USHE" else None
                    user.country = country
                    user.language = language or "en"
                    user.description = org_name if orgType == "OTHR" or orgType == "CORP" else None
                    user.school_id = organization_id if isInteger(organization_id) else None
                    user.school_name = school_name if orgType == "K12" else None
                    role = list(set(role))
                    good_roles = list()
                    if role:
                        sRoles = ["teacher", "administrator", "it-technology", "other"]
                        for i in range(len(role)):
                            if role[i]:
                                role[i] = role[i].lower()
                        good_roles = list(set(sRoles) & set(role))
                    user.user_role = ujson.dumps(good_roles)
                    user.state = organization_state if organization_state != "" else None
                    user.district_name = district_name if district_name != "" else None
                    user.school_data = ujson.dumps(school_data) if school_data is not None else None
                    user.tz_offset = 0
                    user = self.daoRegistry.usersDao.createUser(user.toDict())

                    ok = True

                    soc_number = ConfigRegistry.getItem("INITIAL_QUIZ_SOC_NUMBER")
                    quizList = self.daoRegistry.quizDao.getQuizzesFromSoc(soc_number=soc_number,
                                                                          sharable=True,
                                                                          orderBy=BaseDao.UPDATED_DATE,
                                                                          sortOrder=BaseDao.DESCENDING,
                                                                          limit=1,
                                                                          _type=BaseDao.TO_MODEL)

                    quiz = quizList[0] if quizList else None

                    if quiz is not None:
                        self.daoRegistry.quizDao.cloneQuiz(quiz, user.id, hideCloning=True, hideOldQuiz=False)

                    if 'administrator' in good_roles or 'it-technology' in good_roles:
                        self.salesforceApi(user, good_roles, school_data, mc_demo,
                                           (phone_number if mc_demo is True else None))

                if ok:
                    responseSerializer = User.fromSocrativeDict(user.toDict())

                    request.tasksClient.add_tasks(TaskType.SEND_EMAIL, [{
                        "email_address": user.email,
                        "email_type": EmailTypes.REGISTRATION,
                        "user_id": user.id
                    }])

                    cookie = (projSettings.USER_AUTH_TOKEN, Cryptography.encryptUserAuth(user.auth_token))

                    return responseSerializer, status.HTTP_201_CREATED, user.language, cookie
                else:
                    return BaseError.EMAIL_ALREADY_REGISTERED, status.HTTP_400_BAD_REQUEST, None, None

            return BaseError.PASSWORD_MISSING, status.HTTP_400_BAD_REQUEST, None, None

        except IntegrityError as e:
            logger.debug(exceptionStack(e))
            return BaseError.EMAIL_ALREADY_REGISTERED, status.HTTP_400_BAD_REQUEST, None, None

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR, None, None

    def loginUser(self, request, dto):
        """
        handler for login user (HTTP POST request)
        :param request:
        :param dto:
        """
        try:

            user = self.daoRegistry.usersDao.loadUserByEmail(dto.email)
            if user.password == '' or self.passwordHasher.verify(dto.password, user.password) is False:
                return BaseError.LOGIN_FAILED, status.HTTP_400_BAD_REQUEST, None, None

            if not user.is_active:
                return BaseError.ACCOUNT_DISABLED, status.HTTP_400_BAD_REQUEST, None, None

            lastLogin = pytz.utc.localize(datetime.datetime.utcnow())

            self.cacheRegistry.userModelCache.invalidateData(user.auth_token)

            user.auth_token = self.daoRegistry.usersDao.updateLastLogin(user.id,
                                                                        lastLogin,
                                                                        Cryptography.generate_auth_token())
            self.cacheRegistry.userModelCache.cacheModelData(user.auth_token, ujson.dumps(user.toDict()))

            dataDict = User.fromSocrativeModel(user)
            cookie = (projSettings.USER_AUTH_TOKEN, Cryptography.encryptUserAuth(user.auth_token))

            # update dismiss later message to show status
            self.daoRegistry.userMessageDao.updateStatus(UserSystemMessageModel.REMIND_LATER,
                                                         UserSystemMessageModel.SHOW,
                                                         user.id)

            if self.is_user_premium(user):
                self.daoRegistry.notificationDao.resetNotifications(user.id,
                                                                    [NotificationModel.PRO_ENDING,
                                                                     NotificationModel.PRO_ENDED],
                                                                    )

            return dataDict, status.HTTP_200_OK, user.language, cookie
        except BaseDao.NotFound:
            return BaseError.USER_NOT_FOUND, status.HTTP_400_BAD_REQUEST, None, None
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR, None, None

    def authenticateUser(self, request, dto):
        """
        handler to get authenticated users
        :param request:
        :param dto:
        """
        try:

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            data = User.fromSocrativeDict(user.toDict())

            return data, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def checkEmail(self, request, dto):
        """
        handler for checking the email
        :param request:
        :param dto:
        """

        try:
            if self.daoRegistry.usersDao.checkEmail(dto.email) is True:
                return {}, status.HTTP_200_OK
            else:
                return BaseError.INVALID_EMAIL_ADDRESS, status.HTTP_400_BAD_REQUEST

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def googleLogin(self, request, dataDict):
        """
        method to handle the redirect from google
        :param request:
        :param dataDict:
        :return:
        """

        code = dataDict.get("code")
        error = dataDict.get("error")
        state = dataDict.get("state")

        redirected_host = ConfigRegistry.getItem("FRONTEND_HOST")

        if error or code is None:
            statsd.increment(UserCounters.GOOGLE_LOGIN_FAIL)
            return "http://%s/#error=%s&state=%s" % (redirected_host, error, state), None

        postData = {
            "code": str(code),
            "client_id": ConfigRegistry.getItem("GOOGLE_OAUTH_CLIENT_ID"),
            "client_secret": ConfigRegistry.getItem("GOOGLE_OAUTH_CLIENT_SECRET"),
            "redirect_uri": ConfigRegistry.getItem("GOOGLE_OAUTH_REDIRECT_URI"),
            "grant_type": str("authorization_code")
        }

        req = urllib2.Request(ConfigRegistry.getItem("GOOGLE_OAUTH_REFRESH_TOKEN_URI"),
                              data=urllib.urlencode(postData),
                              headers={'Content-Type': "application/x-www-form-urlencoded"})

        try:
            resp = urllib2.urlopen(req)

            if resp.code == 200:
                # get the email
                dataDict = ujson.loads(resp.read())

                access_token = dataDict.get("access_token")
                # get email
                req = urllib2.Request(ConfigRegistry.getItem("GOOGLE_EMAIL_API_URL"),
                                      headers={'Authorization': 'Bearer ' + access_token})

                resp = urllib2.urlopen(req)

                if resp.code == 200:
                    jsonData = ujson.loads(resp.read())
                    email = jsonData["emails"][0]["value"].strip()

                    # check if the user exists
                    try:
                        # check if the user exists
                        user = self.daoRegistry.usersDao.loadUserByEmail(email.lower())
                        now = pytz.utc.localize(datetime.datetime.utcnow())

                        self.cacheRegistry.userModelCache.invalidateData(user.auth_token)
                        user.auth_token = self.daoRegistry.usersDao.updateLastLogin(user.id,
                                                                                    now,
                                                                                    Cryptography.generate_auth_token())
                        self.cacheRegistry.userModelCache.cacheModelData(user.auth_token, ujson.dumps(user.toDict()))
                    except BaseDao.NotFound:
                        user = SocrativeUserModel()
                        user.email = email.lower()
                        passString = Cryptography.generate_random_password(self.MIN_PASSWORD_LENGTH)
                        user.password = self.passwordHasher.encode(passString, self.passwordHasher.salt(),
                                                                   self.PBK_ITERATIONS)
                        user.display_name = Cryptography.generate_display_name(self.DISPLAY_NAME_LENGTH)
                        user.auth_token = Cryptography.generate_auth_token()
                        language = request.COOKIES.get(projSettings.LANGUAGE_COOKIE_NAME)
                        languages = [l[0] for l in projSettings.LANGUAGES]
                        user.language = language if language in languages else "en"
                        user.tz_offset = 0
                        user = self.daoRegistry.usersDao.createUser(user.toDict())
                        user.level = 0
                        user.proExpiration = None
                        user.licenseKey = None

                        request.tasksClient.add_tasks(TaskType.SEND_EMAIL, [{
                            "email_address": user.email,
                            "email_type": EmailTypes.REGISTRATION,
                            "user_id": user.id
                        }])

                        soc_number = ConfigRegistry.getItem("INITIAL_QUIZ_SOC_NUMBER")
                        quizList = self.daoRegistry.quizDao.getQuizzesFromSoc(soc_number=soc_number,
                                                                              sharable=True,
                                                                              orderBy=BaseDao.UPDATED_DATE,
                                                                              sortOrder=BaseDao.DESCENDING,
                                                                              limit=1,
                                                                              _type=BaseDao.TO_MODEL)

                        quiz = quizList[0] if quizList else None

                        if quiz is not None:
                            self.daoRegistry.quizDao.cloneQuiz(quiz, user.id, hideCloning=True, hideOldQuiz=False)

                    # good user exists , load partner or create if it doesn't exist
                    expires = datetime.datetime.utcnow() + datetime.timedelta(seconds=dataDict["expires_in"])

                    newData = {
                        'access_token': access_token,
                        'expires': expires
                    }

                    if dataDict.get('refresh_token'):
                        newData['refresh_token'] = dataDict['refresh_token']

                    try:
                        partner = self.daoRegistry.partnerDao.getPartner(user.id, pType=PartnerModel.GOOGLE)
                        partnerData = ujson.loads(partner.data)
                        if not partnerData:
                            partnerData = newData
                        else:
                            partnerData.update(newData)
                        data = ujson.dumps(partnerData)
                        self.daoRegistry.partnerDao.updateData(partner.id, data)
                    except BaseDao.NotFound:
                        partner = PartnerModel()
                        partner.user_id = user.id
                        partner.type = PartnerModel.GOOGLE
                        partner.data = ujson.dumps(newData)
                        self.daoRegistry.partnerDao.createPartner(partner.toDict())

                    statsd.increment(UserCounters.GOOGLE_LOGIN_OK)

                    cookie = (projSettings.USER_AUTH_TOKEN, Cryptography.encryptUserAuth(user.auth_token))

                    if self.is_user_premium(user):
                        self.daoRegistry.notificationDao.resetNotifications(user.id,
                                                                            [NotificationModel.PRO_ENDING,
                                                                             NotificationModel.PRO_ENDED],
                                                                            )

                    if state != "zendesk-sso":

                        # update dismiss later message to show status
                        self.daoRegistry.userMessageDao.updateStatus(UserSystemMessageModel.REMIND_LATER,
                                                                     UserSystemMessageModel.SHOW,
                                                                     user.id)

                        return "http://%s/#google-login-success/%s" % (redirected_host,
                                                                       urllib.quote(user.email)), cookie
                    else:
                        return "http://%s/#zendesk-sso" % redirected_host, cookie

            statsd.increment(UserCounters.GOOGLE_LOGIN_FAIL)
            return "http://%s/#%s&state=%s" % (redirected_host,
                                               urllib.urlencode({"error": ugettext('bad request')}), state), None
        except BaseDao.NotFound:
            statsd.increment(UserCounters.GOOGLE_LOGIN_FAIL)
            return "http://%s/#%s&state=%s" % (redirected_host,
                                               urllib.urlencode(BaseError.REGISTER_WITH_GOOGLE_ACCOUNT), state), None
        except Exception as e:
            statsd.increment(UserCounters.GOOGLE_LOGIN_FAIL)
            logger.error(exceptionStack(e))
            return "http://%s/#%s&state=%s" % (redirected_host,
                                               urllib.urlencode(BaseError.INTERNAL_SERVER_ERROR), state), None

    def changePassword(self, request, dataDict):
        """
        handler for changing the password
        :param request:
        :param dataDict:
        :return:
        """

        try:
            if "application/json" not in request.META["CONTENT_TYPE"]:
                return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

            token = dataDict.get("token")
            newPassword = dataDict.get("newPassword")

            if not isStringNotEmpty(token):
                return BaseError.TEMPORARY_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if not isStringNotEmpty(newPassword):
                return BaseError.NEW_PASSWORD_MISSING, status.HTTP_400_BAD_REQUEST

            if len(newPassword) < self.MIN_PASSWORD_LENGTH:
                return BaseError.INVALID_PASSWORD, status.HTTP_400_BAD_REQUEST

            tokenModel = self.daoRegistry.tempTokenDao.getToken(token)

            if tokenModel is None:
                return BaseError.PERMISION_DENIED, status.HTTP_403_FORBIDDEN

            clearToken = ujson.loads(Cryptography.decrypt(token))

            if clearToken[0] not in (TempTokenModel.CHANGE_PASSWORD, TempTokenModel.FIRST_PASSWORD):
                return BaseError.INVALID_CHANGE_PASSWORD_TOKEN, status.HTTP_400_BAD_REQUEST

            if datetime.datetime.utcnow() > datetime.datetime.utcfromtimestamp(clearToken[1]):
                self.daoRegistry.tempTokenDao.deleteToken(tokenModel.id)
                return BaseError.TEMPORARY_TOKEN_EXPIRED, status.HTTP_400_BAD_REQUEST

            # everything ok with the token , let's get the user and update the password
            userId = tokenModel.created_by_id

            encPassword = self.passwordHasher.encode(newPassword, self.passwordHasher.salt(), self.PBK_ITERATIONS)

            self.daoRegistry.usersDao.updatePassword(userId, encPassword)

            return {}, status.HTTP_200_OK

        except BaseDao.NotFound:
            return BaseError.PERMISION_DENIED, status.HTTP_400_BAD_REQUEST
        except ValueError:
            return BaseError.PERMISION_DENIED, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def resetPasswordByMail(self, request, dataDict):
        """
        handler for reset password by email
        :param request:
        :param dataDict:
        """

        try:

            email = dataDict.get('email', "").strip()

            if isStringNotEmpty(email) is False:
                return BaseError.EMAIL_ADDRESS_MISSING, status.HTTP_400_BAD_REQUEST

            base_url = "https://" + ConfigRegistry.getItem("FRONTEND_HOST") + self.reset_pwd_url

            user = self.daoRegistry.usersDao.loadUserByEmail(email.lower())

            i = 0
            url = None
            while i < 20:
                try:
                    currentDate = datetime.datetime.utcnow() + datetime.timedelta(hours=self.temp_token_duration)

                    # 0 - reason - CP --> change password
                    # 1 - expiration date -->
                    # 2 - user_id -->
                    tokenClear = [TempTokenModel.CHANGE_PASSWORD, currentDate]

                    token = Cryptography.encryptDES_CBC(Cryptography.TOKEN, ujson.dumps(tokenClear))
                    url = base_url + token

                    self.daoRegistry.tempTokenDao.deleteAllByUser(user.id, TempTokenModel.CHANGE_PASSWORD)

                    tt = TempTokenModel()
                    tt.temp_token = token
                    tt.created_by_id = user.id
                    tt.purpose = TempTokenModel.CHANGE_PASSWORD

                    self.daoRegistry.tempTokenDao.createToken(tt.toDict())
                    i = 0
                    break
                except IntegrityError as e:
                    logger.debug(exceptionStack(e))

                i += 1

            if i > 0:
                return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_400_BAD_REQUEST

            itemDict = {"first_name": user.first_name, "url": url, "to": user.email, "user_id": user.id,
                        "email_type": EmailTypes.FORGOT_PASSWORD}

            request.tasksClient.add_tasks(TaskType.SEND_EMAIL, [itemDict])

            return {}, status.HTTP_200_OK
        except BaseDao.NotFound:
            return BaseError.USER_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INVALID_EMAIL_ADDRESS, status.HTTP_400_BAD_REQUEST

    @classmethod
    def __check_api_key_for_mc(cls, encrypted, orig):
        """

        :param encrypted:
        :param orig:
        :return:
        """
        x = ConfigRegistry().getItem("MASTERY_CONNECT_KEY")
        return x == encrypted

    def mcLogin(self, request, dataDict):
        """
        POST http call implementing the MC Login REST API
        :param request:
        :param dataDict:
        :return:
        """

        try:

            language = request.COOKIES.get(projSettings.LANGUAGE_COOKIE_NAME)

            email = dataDict.get("email", "").strip()
            mc_id = dataDict.get("mc_id")
            api_key = dataDict.get("api_key")

            if not isStringNotEmpty(email) or not isEmailValid(email):
                return {"status": "email is missing", "url": None}, status.HTTP_400_BAD_REQUEST

            if not isStringNotEmpty(mc_id):
                return {"status": "mc_id is missing", "url": None}, status.HTTP_400_BAD_REQUEST

            if not isStringNotEmpty(api_key):
                return {"status": "api_key is missing", "url": None}, status.HTTP_400_BAD_REQUEST

            if self.__check_api_key_for_mc(api_key, mc_id) is False:
                return {"status": "invalid api_key", "url": None}, status.HTTP_400_BAD_REQUEST

            user = None
            try:
                user = self.daoRegistry.usersDao.loadUserByEmail(email.lower())
                self.cacheRegistry.userModelCache.invalidateData(user.auth_token)
                now = pytz.utc.localize(datetime.datetime.utcnow())
                user.auth_token = self.daoRegistry.usersDao.updateLastLogin(user.id,
                                                                            now,
                                                                            Cryptography.generate_auth_token())
                self.cacheRegistry.userModelCache.cacheModelData(user.auth_token, ujson.dumps(user.toDict()))
            except BaseDao.NotFound:
                pass

            if user is None:
                user = SocrativeUserModel()
                user.email = email.lower()
                user.password = Cryptography.generate_random_password(self.MIN_PASSWORD_LENGTH)
                user.display_name = Cryptography.generate_display_name(self.DISPLAY_NAME_LENGTH)
                user.auth_token = Cryptography.generate_auth_token()
                languages = [a[0] for a in projSettings.LANGUAGES]
                user.language = language if language in languages else "en"
                user.tz_offset = 0
                user = self.daoRegistry.usersDao.createUser(user.toDict())
                user.level = 0
                user.proExpiration = None
                user.licenseKey = None

                soc_number = ConfigRegistry.getItem("INITIAL_QUIZ_SOC_NUMBER")
                quizList = self.daoRegistry.quizDao.getQuizzesFromSoc(soc_number=soc_number,
                                                                      sharable=True,
                                                                      orderBy=BaseDao.UPDATED_DATE,
                                                                      sortOrder=BaseDao.DESCENDING,
                                                                      limit=1,
                                                                      _type=BaseDao.TO_MODEL)

                quiz = quizList[0] if quizList else None

                if quiz is not None:
                    self.daoRegistry.quizDao.cloneQuiz(quiz, user.id, hideCloning=True, hideOldQuiz=False)

                request.tasksClient.add_tasks(TaskType.SEND_EMAIL, [{
                        "email_address": user.email,
                        "email_type": EmailTypes.REGISTRATION,
                        "user_id": user.id
                    }])

            partner = None
            if user is not None:
                try:
                    self.daoRegistry.partnerDao.getPartner(user.id, pType=PartnerModel.MASTERY_CONNECT)
                except BaseDao.NotFound:
                    pass

            if partner is None:
                partner = PartnerModel()
                partner.user_id = user.id
                partner.type = PartnerModel.MASTERY_CONNECT
                partner.data = ujson.dumps({"created_date": datetime.datetime.utcnow(), "mc_id": mc_id})
                self.daoRegistry.partnerDao.createPartner(partner.toDict())

            mcUrl = ConfigRegistry().getItem("MASTERY_CONNECT_URL")
            front_host = ConfigRegistry().getItem("FRONTEND_HOST")
            url = "%s://%s/teacher%s%s" % ("https" if request.is_secure() else "http", front_host, mcUrl,
                                           user.auth_token)

            # update dismiss later message to show status
            self.daoRegistry.userMessageDao.updateStatus(UserSystemMessageModel.REMIND_LATER,
                                                         UserSystemMessageModel.SHOW,
                                                         user.id,
                                                         )
            if self.is_user_premium(user):
                self.daoRegistry.notificationDao.resetNotifications(user.id,
                                                                    [NotificationModel.PRO_ENDING,
                                                                     NotificationModel.PRO_ENDED],
                                                                    )

            return {"status": "success", "url": url}, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return {"status": ugettext("internal server error"), "url": None}, status.HTTP_500_INTERNAL_SERVER_ERROR

    def importQuizForUser(self, request, dataDict):
        """
        :param request:
        :param dataDict:
        :return:
        """

        try:

            soc_number = dataDict.get("soc_number")

            if not isStringNotEmpty(soc_number):
                url = request.build_absolute_uri("/")
                return url, None, None

            cookie = request.COOKIES.get(self.cookie_teacher_name)

            if cookie:

                cookie = urllib.unquote(cookie)
                cookie = ujson.loads(cookie)

                auth_token = cookie.get("auth_token")

                if auth_token is None:
                    url = request.build_absolute_uri("/")
                    return url, None, None

                if self.validateUser(auth_token) is None:
                    return BaseError.USER_NOT_FOUND, status.HTTP_400_BAD_REQUEST

                resp = self.serviceRegistry.quizService.importQuizBySoc(request, {"auth_token": auth_token,
                                                                                  "soc_number": soc_number})

                url = request.build_absolute_uri("/teacher/#launch")
                cookie["importQuizStatus"] = resp[1]
                cookie_string = ujson.dumps(cookie)

            else:
                url = request.build_absolute_uri("/")

                cookie = dict()

                cookie["importQuizStatus"] = -1
                cookie["quiz_sn"] = soc_number
                cookie_string = ujson.dumps(cookie)

            return url, self.cookie_teacher_name, urllib.quote(cookie_string)
        except BaseDao.NotFound:
            return BaseError.USER_NOT_FOUND, status.HTTP_400_BAD_REQUEST, None
        except Exception as e:
            logger.exception(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR, None

    def dismissSystemMessage(self, request, dataDict):
        """
        :param request:
        :param dataDict:
        :return:
        """

        try:
            sysMsgId = dataDict.get("sm_id")
            auth_token = dataDict.get("auth_token")

            if not isStringNotEmpty(auth_token):
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if type(sysMsgId) not in (int, long) or sysMsgId < 0:
                return BaseError.INVALID_SYS_MSG_ID, status.HTTP_400_BAD_REQUEST

            sm = self.daoRegistry.systemMessageDao.loadById(sysMsgId)

            if sm.dismissible is False:
                return BaseError.PERMISION_DENIED, status.HTTP_403_FORBIDDEN

            self.daoRegistry.userMessageDao.updateUserMessage(sm.id, auth_token, True)

            return {}, status.HTTP_200_OK

        except BaseDao.NotFound:
            return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def validateUser(self, auth_token):
        """
        retrieve from cache or database the socrative_user model
        :param auth_token:
        :return: socrative user model without the password field
        """

        try:

            if not isStringNotEmpty(auth_token):
                return None

            userModel = self.cacheRegistry.userModelCache.get(auth_token)
            if userModel:
                if userModel.proExpiration:
                    userModel.proExpiration = pytz.utc.localize(datetime.datetime.fromtimestamp(userModel.proExpiration))
                return userModel

            model = self.daoRegistry.usersDao.loadUser(auth_token)
            model.password = None

            self.cacheRegistry.userModelCache.cacheData(auth_token, ujson.dumps(model.toDict()))

            return model

        except BaseDao.NotFound:
            return None
        except Exception as e:
            logger.error(exceptionStack(e))
            raise

    def __check_admin_api_key(self, key):
        """
        checks that the request is valid using the key provided
        :param key: admin backend key
        :type key: basestring
        """

        return key == self.BACKEND_ADMIN_KEY

    def getAccountStatus(self, request, dto):
        """
        :param request:
        :param dto:
        :return
        """

        try:
            resp = dict()
            if self.__check_admin_api_key(dto.api_key) is False:
                return BaseError.INVALID_API_KEY, status.HTTP_400_BAD_REQUEST

            user = self.daoRegistry.usersDao.loadUserByEmail(dto.email)

            is_user_pro, stripe_customer_id = self.daoRegistry.licenseDao.get_pro_data(user.id)

            # compute if it is premium and still valid
            resp["premium"] = False
            resp["stripe_customer_id"] = None

            if is_user_pro:
                resp["premium"] = True
            if stripe_customer_id:
                resp["stripe_customer_id"] = stripe_customer_id

            resp["valid_account"] = user.is_active
            resp["name"] = "%s, %s" % ('' if user.last_name is None else user.last_name,
                                       '' if user.first_name is None else user.first_name)

            if user.organization_type in ('OTHR', 'CORP'):
                resp["organization"] = user.description
            elif user.organization_type == 'USHE':
                resp["organization"] = user.university
            elif user.organization_type == 'K12':
                resp["organization"] = user.school_name
            else:
                resp["organization"] = ''

            return resp, status.HTTP_200_OK

        except BaseDao.NotFound:
            logger.debug("No user was found with email %s" % dto.email)
            return BaseError.USER_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def upgradeAccount(self, request, dto):
        """
        :param request:
        :param dto:
        :return
        """

        try:
            if self.__check_admin_api_key(dto.api_key) is False:
                return BaseError.INVALID_API_KEY, status.HTTP_400_BAD_REQUEST

            user = self.daoRegistry.usersDao.loadUserByEmail(dto.email)

            level = self.daoRegistry.teacherRoleDao.getRoleForTeacherId(user.id)

            # compute if it is premium and still valid
            if level:
                return BaseError.USER_IS_PREMIUM_ALREADY, status.HTTP_400_BAD_REQUEST

            # insert the new role for the user id
            self.daoRegistry.teacherRoleDao.createRole(user.id, dto.stripe_customer_id, dto.amount, dto.currency,
                                                       dto.expiry)

            defaultRoom = self.daoRegistry.roomDao.getDefaultRoom(user.id)
            if defaultRoom:
                self.serviceRegistry.roomService.roomFullLimiter.invalidateData(defaultRoom)

            self.cacheRegistry.userModelCache.invalidateData(user.auth_token)

            request.tasksClient.add_tasks(TaskType.SEND_EMAIL, [{
                "email_address": user.email,
                "email_type": EmailTypes.UPGRADE_PRO
            }])

            return {}, status.HTTP_200_OK

        except BaseDao.NotFound:
            logger.debug("No user was found with email %s" % dto.email)
            return BaseError.USER_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_400_BAD_REQUEST

    def __base64url_encode(self, input):
        """

        :return:
        """
        return base64.urlsafe_b64encode(input).replace(b'=', b'')

    def zendeskSSO(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:

            if dto.auth_token:
                user = self.validateUser(dto.auth_token)
            else:
                if dto.email is None:
                    return BaseError.EMAIL_ADDRESS_MISSING, status.HTTP_400_BAD_REQUEST, None

                if dto.password is None:
                    return BaseError.PASSWORD_MISSING, status.HTTP_400_BAD_REQUEST, None

                try:
                    user = self.daoRegistry.usersDao.loadUserByEmail(dto.email)
                except BaseDao.NotFound:
                    return BaseError.USER_NOT_FOUND, status.HTTP_400_BAD_REQUEST, None

                if self.passwordHasher.verify(dto.password, user.password) is False:
                    return BaseError.INVALID_PASSWORD_COMBO, status.HTTP_400_BAD_REQUEST, None

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST, None

            data = dict()
            user_fields = dict()

            user_role = ujson.loads(user.user_role) if user.user_role else None

            user_fields["socrative_role"] = ("soc_" + user_role[0].lower()) if user_role else ""

            lang = "socrative_english"

            if user.language == 'en_uk':
                lang = "soc_english_uk"
            elif user.language == 'fr_ca':
                lang = "soc_french_canadian"
            elif user.language == "nl":
                lang = "soc_dutch"
            elif user.language == "fr":
                lang = "soc_french"
            elif user.language == "ko":
                lang = "soc_korean"
            elif user.language == "es_mx":
                lang = "soc_spanish_mexico"
            elif user.language == "zh":
                lang = "soc_chinese_simplified"
            elif user.language == "pt_br":
                lang = "soc_portuguese_brazil"
            elif user.language == "es":
                lang = "soc_spanish"
            elif user.language  in ("en", "en_us", None):
                lang = "soc_english"

            user_fields["socrative_language"] = lang

            user_fields["socrative_country"] = user.country

            rooms = self.daoRegistry.roomDao.getAllActiveRooms(user.id)

            user_fields["socrative_room_name"] = ",".join(rooms)

            org_type = "socrative_k_12"

            if user.organization_type == "USHE":
                org_type = "socrative_higher_ed"
            elif user.organization_type == "CORP":
                org_type = "socrative_corporate"
            elif user.organization_type == "OTHR":
                org_type = "socrative_other"
            elif user.organization_type == "INTR":
                org_type = "socrative_international"

            user_fields["socrative_organization_type"] = org_type

            if self.is_user_premium(user):
                user_fields["socrative_access_type"] = "socrative_access_pro"
            else:
                user_fields["socrative_access_type"] = "socrative_access_free"
            user_fields["socrative_account_id"] = str(user.id)
            data["user_fields"] = user_fields
            data["email"] = user.email
            data["name"] = (user.first_name.encode('utf-8') if type(user.first_name) is not str else user.first_name) +\
                           " " + (user.last_name.encode('utf-8') if type(user.last_name) is not str else user.last_name)
            if not data["name"]:
                data["name"] = user.email[:user.email.indexof('@')]

            data["jti"] = ''.join([self.alphabet[random.randint(0, len(self.alphabet) - 1)] for _ in range(64)])
            data["iat"] = int(time.time())

            segments = []

            # Header
            header = {'typ': "JWT", 'alg': "HS256"}

            json_header = ujson.dumps(header)

            segments.append(self.__base64url_encode(json_header))
            segments.append(self.__base64url_encode(ujson.dumps(data)))

            # Segments
            signing_input = b'.'.join(segments)
            signature = hmac.new(self.__zendesk_sso_key, signing_input, sha256).digest()
            segments.append(self.__base64url_encode(signature))

            query = '.'.join(segments)

            if dto.redirect_to:
                query += "&redirect_to=%s" % urllib.quote(dto.redirect_to)

            cookie = (projSettings.USER_AUTH_TOKEN, Cryptography.encryptUserAuth(user.auth_token))

            return {"redirect_url": self.__zendesk_redirect_url + query}, status.HTTP_200_OK, cookie

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR, None

    def createSystemMessage(self, request, dto):
        """
        create system message - message for banners
        :param request:
        :param dto:
        :return:
        """

        try:

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if user.email.endswith("@masteryconnect.com") is False:
                return BaseError.PERMISION_DENIED, status.HTTP_400_BAD_REQUEST

            msg = SystemMessageModel()
            msg.content = dto.content
            msg.title = dto.title
            msg.url = dto.url
            msg.action_type = dto.action_type
            msg.status = SystemMessageModel.ACTIVE
            msg.end_date = None
            msg.start_date = None
            msg.priority = None
            msg.dismissible = False

            if dto.audience == "all":
                msg.audience = SystemMessageModel.ALL
            elif dto.audience == "free":
                msg.audience = SystemMessageModel.FREE
            elif dto.audience == "pro":
                msg.audience = SystemMessageModel.PRO
            else:
                return BaseError.INVALID_SYS_MSG_AUDIENCE, status.HTTP_400_BAD_REQUEST

            self.daoRegistry.systemMessageDao.hideLastMessage(msg.audience)

            msgId = self.daoRegistry.systemMessageDao.create(msg.toDict())

            return {"id": msgId}, status.HTTP_201_CREATED

        except BaseDao.NotFound:
            return BaseError.USER_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def updateSystemMessage(self, request, dto):
        """
        update a specific system message
        """

        try:

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if user.email.endswith("@masteryconnect.com") is False:
                return BaseError.PERMISION_DENIED, status.HTTP_403_FORBIDDEN

            if dto.hide is True:
                msg_status = SystemMessageModel.HIDDEN
            else:
                msg_status = SystemMessageModel.ACTIVE

            self.daoRegistry.systemMessageDao.hideMessage(dto.id, msg_status)

            return {}, status.HTTP_200_OK

        except BaseDao.NotUpdated:
            return BaseError.SYS_MSG_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def getSystemMessage(self, request, dto):
        """
        service for get system message
        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if user.email.endswith("@masteryconnect.com") is False:
                return BaseError.PERMISION_DENIED, status.HTTP_403_FORBIDDEN

            msg = self.daoRegistry.systemMessageDao.loadById(dto.msg_id)
            if not msg:
                return BaseError.SYS_MSG_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            return msg, status.HTTP_200_OK
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def getUserSystemMessage(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if self.serviceRegistry.userService.is_user_premium(user):
                level = SystemMessageModel.PRO
            else:
                level = SystemMessageModel.FREE

            sysMsg = self.daoRegistry.systemMessageDao.loadLastMessage(level)

            msg = {}

            if sysMsg and (sysMsg.audience == SystemMessageModel.ALL or sysMsg.audience == level):

                usrMsg = self.daoRegistry.userMessageDao.loadMessage(sysMsg.id, user.id)

                if usrMsg is None:
                    usrMsg = UserSystemMessageModel()
                    usrMsg.user_id = user.id
                    usrMsg.sys_message_id = sysMsg.id
                    usrMsg.status = UserSystemMessageModel.UNREAD
                    usrMsg.show_date = None

                    usrMsg.id = self.daoRegistry.userMessageDao.createUserMessage(usrMsg)
                elif usrMsg.status >= UserSystemMessageModel.READ:
                    usrMsg = {}

                if usrMsg:
                    data = sysMsg.toDict()
                    data.update(usrMsg.toDict())
                    msg = UserSystemMessage.fromSocrativeDict(data)

            return msg, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def updateUserSystemMessage(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if dto.status == "remind_me":
                msg_status = UserSystemMessageModel.REMIND_LATER
            elif dto.status == "read":
                msg_status = UserSystemMessageModel.READ
            elif dto.status == "dismissed":
                msg_status = UserSystemMessageModel.DISMISSED
            else:
                return BaseError.INVALID_USER_MESSAGE_STATUS, status.HTTP_400_BAD_REQUEST

            self.daoRegistry.userMessageDao.update(dto.usr_msg_id, msg_status, user.id)

            return {}, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR
