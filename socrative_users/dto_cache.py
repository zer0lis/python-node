# coding=utf-8
from common.base_cache import BaseCache
from common.socrative_api import exceptionStack, safeToInt
from socrative_users.dao import SocrativeUserModel
import logging
import ujson

logger = logging.getLogger(__name__)


class UserModelCache(BaseCache):
    """

    """

    def __init__(self, redisPool, expireTime):
        """

        :param redisPool:
        :param expireTime:
        :return:
        """

        super(UserModelCache, self).__init__(redisPool)
        self.expireTime = safeToInt(expireTime) or 84100

    def cacheData(self, user_uuid, data):
        """
        :param data:
        :param user_uuid
        :return:
        """

        try:
            key = "user:%s" % user_uuid
            self.getConnection(False).set(key, data, expireTime=self.expireTime)
        except Exception as e:
            logger.error(exceptionStack(e))

    def cacheModelData(self, user_uuid, model):
        """
        :param model
        :param user_uuid
        :return:
        """

        try:
            key = "user:%s" % user_uuid
            model.password = None
            data = ujson.dumps(model.toDict())
            self.getConnection(False).set(key, data, expireTime=self.expireTime)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateData(self, user_uuid):
        """

        :param user_uuid:
        :return:
        """

        try:
            key = "user:%s" % user_uuid
            self.getConnection(False).delete(key)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateMulti(self, auth_tokens):
        """

        :param auth_tokens:
        :return:
        """
        if type(auth_tokens) is not list:
            raise TypeError("auth tokens must be a list")

        try:
            keys = ["user:%s" % user_uuid for user_uuid in auth_tokens]
            self.getConnection(False).delete_multi(keys)
        except Exception as e:
            logger.error(exceptionStack(e))

    def get(self, user_uuid):
        """
        get the current activity
        :param user_uuid:
        :return:
        """
        try:
            key = "user:%s" % user_uuid
            obj = self.getConnection(True).get(key)
            if obj is None:
                return None

            model = SocrativeUserModel.fromDict(ujson.loads(obj))
            if hasattr(model, "level") is False:
                return None

            return model
        except Exception as e:
            logger.error(exceptionStack(e))


class IPRateLimiter(BaseCache):
    """
    rate limiting class for ips
    """

    def __init__(self, redisPool, expireTime):
        """

        :param redisPool:
        :param expireTime:
        :return:
        """

        super(IPRateLimiter, self).__init__(redisPool)
        self.expireTime = safeToInt(expireTime) or 86400  # 1 day

    def get(self, ip):
        """

        :param ip:
        :return:
        """
        try:
            key = "ip_%s" % ip
            obj = self.getConnection(True).scard(key)
            if obj is None:
                return 0

            return obj
        except Exception as e:
            logger.error(exceptionStack(e))

    def cacheData(self, ip, email):
        """
        :param ip:
        :param email:
        :return:
        """

        try:
            key = "ip_%s" % ip
            self.getConnection(False).addToSet(key, email, expireTime=self.expireTime)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateData(self, ip):
        """

        :param ip:
        :return:
        """

        try:
            key = "ip_%s" % ip
            self.getConnection(False).delete(key)
        except Exception as e:
            logger.error(exceptionStack(e))


class EmailRateLimiter(BaseCache):
    """
    rate limiting requests from a certain user
    """

    def __init__(self, redisPool, expireTimeLevel1, expireTimeLevel2, expireTimeLevel3):
        """

        :param redisPool:
        :param expireTimeLevel1:
        :param expireTimeLevel2:
        :param expireTimeLevel3:
        :return:
        """

        super(EmailRateLimiter, self).__init__(redisPool)
        self.expireLevel1 = safeToInt(expireTimeLevel1) or 300  # 5 minutes
        self.expireLevel2 = safeToInt(expireTimeLevel2) or 3600  # 1 hour
        self.expireLevel3 = safeToInt(expireTimeLevel3) or 86400  # 1 day

    def get(self, ip, email):
        """

        :param ip:
        :param email:
        :return:
        """
        try:
            key = "bem_%s_%s" % (ip, email)
            obj = self.getConnection(True).get(key)
            if obj is None:
                return 0

            return obj
        except Exception as e:
            logger.error(exceptionStack(e))
            return 0

    def cacheData(self, ip, email, level=1):
        """
        :param ip:
        :param email
        :param level
        :return:
        """

        try:
            key = "bem_%s_%s" % (ip, email)
            expireTime = self.expireLevel1
            if level == 2:
                expireTime = self.expireLevel2
            elif level >= 3:
                expireTime = self.expireLevel3

            self.getConnection(False).increment(key, expireTime=expireTime)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateData(self, ip, email):
        """

        :param ip:
        :param email:
        :return:
        """

        try:
            key = "bem_%s_%s" % (ip, email)
            self.getConnection(False).delete(key)
        except Exception as e:
            logger.error(exceptionStack(e))
