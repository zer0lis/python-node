# -*- coding: utf-8 -*-
from common.base_dto import DTOConverter, ItemConverter, RequestDTOValidator, ItemValidator
from common.serializers import MediaResourceDTO
from common.socrative_api import isNotNone, isStringNotEmpty, safeToInt, serializerToBool, isBool, isBoolOrNone
from common.socrative_api import isString, room_name_validator
from common.socrative_errors import BaseError
from common import base_limits
from common import http_status as status
from string import strip, lower


class AnswerDTO(DTOConverter):
    """
    Serializer DTO for Answer DTO
    """
    stringifyDict = {
        "id": ItemConverter(),
        "text": ItemConverter(optional=True),
        "order": ItemConverter()
    }


class StandardDTO(DTOConverter):
    """
    Serializer DTO for Answer DTO
    """
    stringifyDict = {
        "subject_id": ItemConverter(),
        "core_id": ItemConverter(),
        "grade_id": ItemConverter(),
        "standard_id": ItemConverter(),
        "name": ItemConverter(),
        "description": ItemConverter(optional=True, default="")
    }


class SimpleStandardDTO(DTOConverter):
    """
    Serializer DTO for Answer DTO
    """
    stringifyDict = {
        "name": ItemConverter()
    }


class TeacherAnswer(DTOConverter):
    """
    Teacher answer DTO
    """

    stringifyDict = {
        "id": ItemConverter(),
        "text": ItemConverter(optional=True),
        "order": ItemConverter(),
        "is_correct": ItemConverter(optional=True)
    }


class QuestionDTO(DTOConverter):
    """
    Question DTO serializer
    """

    stringifyDict = {
        "question_id": ItemConverter(),
        "question_text": ItemConverter(optional=True),
        "grading_weight": ItemConverter(optional=True),
        "type": ItemConverter(),
        "order": ItemConverter(),
        "answers": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=AnswerDTO, optional=True, default=[]),
        "resources": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=MediaResourceDTO, optional=True, default=[])
    }


class TeacherQuestion(DTOConverter):
    """
    teacher question serializer
    """

    stringifyDict = {
        "question_id": ItemConverter(),
        "question_text": ItemConverter(optional=True),
        "explanation": ItemConverter(optional=True),
        "grading_weight": ItemConverter(optional=True),
        "type": ItemConverter(),
        "order": ItemConverter(),
        "answers": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=TeacherAnswer, optional=True, default=[]),
        "resources": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=MediaResourceDTO, optional=True, default=[]),
        "has_correct_answer": ItemConverter()
    }


class ReportQuestion(DTOConverter):
    """
    report question serializer
    """

    stringifyDict = {
        "question_text": ItemConverter(optional=True),
        "question_type": ItemConverter(dtoName="type", optional=True)
    }


class QuizMetadata(DTOConverter):
    """
    quiz metadata serializer
    """

    stringifyDict = {
        "id": ItemConverter(),
        "name": ItemConverter(),
        "soc_number": ItemConverter(),
        "is_hidden": ItemConverter(),
        "created_by": ItemConverter(),
        "created_date": ItemConverter(),
        "last_updated": ItemConverter(),
        "sharable": ItemConverter()
    }


class QuizDTO(DTOConverter):
    """
    Quiz serializer
    """

    stringifyDict = {
        "id": ItemConverter(),
        "name": ItemConverter(),
        "created_by_id": ItemConverter(dtoName="created_by"),
        "questions": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=QuestionDTO, optional=True)
    }


class TeacherQuiz(DTOConverter):
    """
    teacher quiz serializer
    """

    stringifyDict = {
        "id": ItemConverter(),
        "name": ItemConverter(),
        "soc_number": ItemConverter(),
        "is_hidden": ItemConverter(),
        "created_by_id": ItemConverter(dtoName="created_by"),
        "created_date": ItemConverter(),
        "last_updated": ItemConverter(),
        "sharable": ItemConverter(),
        "questions": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=TeacherQuestion, optional=True, default=[]),
        "standard": ItemConverter(objType=ItemConverter.SINGLE, itemClass=StandardDTO, optional=True,
                                  dtoName="standard")
    }


class QuizListDTO(DTOConverter):
    """
    report quiz serializer
    """

    stringifyDict = {
        "id": ItemConverter(),
        "name": ItemConverter(),
        "soc_number": ItemConverter(dtoName="soc"),
        "last_updated": ItemConverter(dtoName="date"),
        "questions": ItemConverter(),
        "standard": ItemConverter(optional=True),
        "sharable": ItemConverter()
    }


class GetQuizRequestDTO(RequestDTOValidator):
    """
    validates the get quiz request data
    """

    rulesDict = {
        "pk": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.QUIZ_ID_MISSING,
                                                                        status.HTTP_400_BAD_REQUEST)),
        "auth_token": ItemValidator(mandatory=False, funcList=[isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "user_uuid": ItemValidator(mandatory=False, funcList=[isStringNotEmpty],
                                   errorCode=(BaseError.INVALID_STUDENT_UUID, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=False, funcList=[strip, strip, lower, room_name_validator],
                                   errorCode=(BaseError.INVALID_ROOM_NAME, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """

        self.pk = None
        self.auth_token = None
        self.user_uuid = None
        self.room_name = None


class GetQuizBySocRequestDTO(RequestDTOValidator):
    """
    validates the get quiz by soc number request data
    """
    rulesDict = {
        "soc_number": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.SOC_NUMBER_IS_MISSING,
                                                                            status.HTTP_400_BAD_REQUEST)),
        "auth_token": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.AUTH_TOKEN_MISSING,
                                                                            status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """

        self.soc_number = None
        self.auth_token = None


class AnswerRequestDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "order": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.ORDER_IS_MISSING,
                                                                           status.HTTP_400_BAD_REQUEST)),
        "is_correct": ItemValidator(mandatory=False, defaultValue=None, funcList=[serializerToBool, isBoolOrNone],
                                    errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST)),
        "text": ItemValidator(mandatory=False, defaultValue="",
                              funcLimits=[(len, base_limits.ANSWER_TEXT_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                 status.HTTP_400_BAD_REQUEST))]),
        "id": ItemValidator(mandatory=False, defaultValue=None)
    }

    def __init__(self):
        """
        """
        self.order = None
        self.is_correct = None
        self.text = ""
        self.id = None


class ResourceRequestDTO(RequestDTOValidator):
    """

    """
    rulesDict = {
        "name": ItemValidator(mandatory=False, defaultValue="", funcLimits=[(len, base_limits.RESOURCE_NAME_LIMIT,
                                                                             (BaseError.STRING_TOO_LONG,
                                                                              status.HTTP_400_BAD_REQUEST))]),
        "url": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.RESOURCE_URL_MISSING,
                                                                     status.HTTP_400_BAD_REQUEST),
                             funcLimits=[(len, base_limits.RESOURCE_URL_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                status.HTTP_400_BAD_REQUEST))])
    }

    def __init__(self):
        """
        """
        self.name = ""
        self.url = None


class CreateQuestionRequestDTO(RequestDTOValidator):
    """
    validates the request for create question
    """

    rulesDict = {
        "auth_token": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.AUTH_TOKEN_MISSING,
                                                                            status.HTTP_400_BAD_REQUEST),
                                    funcLimits=[(len, base_limits.AUTH_TOKEN_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                     status.HTTP_400_BAD_REQUEST))]),
        "quiz_id": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.QUIZ_ID_MISSING,
                                                                             status.HTTP_400_BAD_REQUEST)),
        "type": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.TYPE_IS_MISSING,
                                                                      status.HTTP_400_BAD_REQUEST),
                              funcLimits=[(len, base_limits.QUESTION_TYPE_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                status.HTTP_400_BAD_REQUEST))]),
        "order": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.ORDER_IS_MISSING,
                                                                           status.HTTP_400_BAD_REQUEST)),
        "question_text": ItemValidator(mandatory=False, defaultValue="", errorCode=(),
                                       funcLimits=[(len, base_limits.QUESTION_TEXT_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                           status.HTTP_400_BAD_REQUEST))]),
        "explanation": ItemValidator(mandatory=False, defaultValue="", errorCode=(),
                                     funcLimits=[(len, base_limits.QUESTION_EXPLANATION_LIMIT,
                                                  (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST))]),
        "grading_weight": ItemValidator(mandatory=False, defaultValue=1.0, errorCode=()),
        "answers": ItemValidator(iterator=ItemValidator.LIST, itClass=AnswerRequestDTO, mandatory=False, defaultValue=list()),
        "resources": ItemValidator(iterator=ItemValidator.LIST, itClass=ResourceRequestDTO, mandatory=False, defaultValue=list()),
        "duplicate": ItemValidator(mandatory=False, defaultValue=False, funcList=[serializerToBool, isBool],
                                   errorCode=())
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.quiz_id = None
        self.type = None
        self.order = None
        self.question_text = ""
        self.explanation = ""
        self.grading_weight = 1.0
        self.answers = list()
        self.resources = list()
        self.question_id = None


class UpdateQuestionRequestDTO(RequestDTOValidator):
    """
    validates the request for create question
    """

    rulesDict = {
        "question_id": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.QUESTION_ID_MISSING,
                                                                       status.HTTP_400_BAD_REQUEST)),
        "auth_token": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.AUTH_TOKEN_MISSING,
                                                                            status.HTTP_400_BAD_REQUEST),
                                    funcLimits=[(len, base_limits.AUTH_TOKEN_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                     status.HTTP_400_BAD_REQUEST))]),
        "quiz_id": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.QUIZ_ID_MISSING,
                                                                             status.HTTP_400_BAD_REQUEST)),
        "type": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.TYPE_IS_MISSING,
                                                                      status.HTTP_400_BAD_REQUEST),
                              funcLimits=[(len, base_limits.QUESTION_TYPE_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                  status.HTTP_400_BAD_REQUEST))]),
        "order": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.ORDER_IS_MISSING,
                                                                           status.HTTP_400_BAD_REQUEST)),
        "question_text": ItemValidator(mandatory=False, defaultValue="", errorCode=(),
                                       funcLimits=[(len, base_limits.QUESTION_TEXT_LIMIT,
                                                    (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST))]),
        "explanation": ItemValidator(mandatory=False, defaultValue="", errorCode=(),
                                     funcLimits=[(len, base_limits.QUESTION_EXPLANATION_LIMIT,
                                                  (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST))]),
        "grading_weight": ItemValidator(mandatory=False, defaultValue=1.0, errorCode=()),
        "answers": ItemValidator(iterator=ItemValidator.LIST, itClass=AnswerRequestDTO, mandatory=False, defaultValue=list()),
        "resources": ItemValidator(iterator=ItemValidator.LIST, itClass=ResourceRequestDTO, mandatory=False, defaultValue=list())
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.quiz_id = None
        self.type = None
        self.order = None
        self.question_text = ""
        self.explanation = ""
        self.grading_weight = 1.0
        self.answers = list()
        self.resources = list()
        self.question_id = None


class QuestionOrderDTO(RequestDTOValidator):
    """
    """
    rulesDict = {
        "question_id": ItemValidator(mandatory=True, funcList=[safeToInt, isNotNone],
                                     errorCode=(BaseError.INVALID_QUESTION_ID, status.HTTP_400_BAD_REQUEST)),
        "order": ItemValidator(mandatory=True, funcList=[safeToInt, isNotNone], errorCode=(BaseError.INVALID_REQUEST,
                                                                                           status.HTTP_400_BAD_REQUEST))
    }


class QuestionReorderRequestDTO(RequestDTOValidator):
    """

    """
    rulesDict = {
        "orders": ItemValidator(mandatory=True, defaultValue=[], iterator=ItemValidator.LIST, itClass=QuestionOrderDTO,
                                errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST)),

        "auth_token": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.AUTH_TOKEN_MISSING,
                                                                            status.HTTP_400_BAD_REQUEST)),

        "quiz_id": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.QUIZ_ID_MISSING,
                                                                             status.HTTP_400_BAD_REQUEST))
    }

class StandardDTOValidator(RequestDTOValidator):
    """
    standard dto serializer
    """

    rulesDict = {
        "id": ItemValidator(mandatory=False, funcList=[safeToInt, isNotNone],
                            errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST)),
        "subject_id": ItemValidator(mandatory=True, funcList=[safeToInt, isNotNone],
                                    errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST)),
        "core_id": ItemValidator(mandatory=True, funcList=[safeToInt, isNotNone],
                                 errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST)),
        "grade_id": ItemValidator(mandatory=True, funcList=[safeToInt, isNotNone],
                                  errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST)),
        "standard_id": ItemValidator(mandatory=True, funcList=[safeToInt, isNotNone],
                                     errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST)),
        "name": ItemValidator(mandatory=True, funcList=[isStringNotEmpty],
                              funcLimits=[(len, base_limits.AUTH_TOKEN_LIMIT,
                                           (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST))]),
        "description": ItemValidator(mandatory=False, funcList=[isString],
                                     funcLimits=[(len, base_limits.STANDARD_DESCRIPTION_LIMIT,
                                                  (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST))])

    }

    def __init__(self):
        """

        :return:
        """

        self.id = None
        self.subject_id = None
        self.core_id = None
        self.grade_id = None
        self.standard_id = None
        self.name = None
        self.description = None


class UpdateQuizRequestDTO(RequestDTOValidator):
    """
    request serializer
    """

    rulesDict = {
        "pk": ItemValidator(mandatory=True, funcList=[safeToInt, isNotNone],
                            errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST)),

        "auth_token": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.AUTH_TOKEN_MISSING,
                                                                            status.HTTP_400_BAD_REQUEST)),

        "create": ItemValidator(mandatory=False, funcList=[serializerToBool, isBool], defaultValue=False,
                                errorCode=(BaseError.QUIZ_ID_MISSING, status.HTTP_400_BAD_REQUEST)),
        "name": ItemValidator(mandatory=False, funcList=[isStringNotEmpty], defaultValue="",
                              errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST)),
        "sharable": ItemValidator(mandatory=False, funcList=[serializerToBool, isBool], defaultValue=None,
                                  errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST)),
        "standard": ItemValidator(itClass=StandardDTOValidator, iterator=ItemValidator.OBJECT, defaultValue=None,
                                  mandatory=False)
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.pk = None
        self.create = False
        self.created_date = None
        self.last_updated = None
        self.standard = None
        self.name = ""


class ImportQuizBySOCRequestDTO(RequestDTOValidator):
    """
    request serializer
    """

    rulesDict = {
        "auth_token": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.AUTH_TOKEN_MISSING,
                                                                            status.HTTP_400_BAD_REQUEST),
                                    funcLimits=[(len, base_limits.AUTH_TOKEN_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                     status.HTTP_400_BAD_REQUEST))]),
        "soc_number": ItemValidator(funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.SOC_NUMBER_IS_MISSING, status.HTTP_400_BAD_REQUEST),
                                    funcLimits=[(len, base_limits.QUIZ_SOC_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                   status.HTTP_400_BAD_REQUEST))])
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.soc_number = None


class ListQuizzesRequestDTO(RequestDTOValidator):
    """
    request serializer
    """

    rulesDict = {
        "auth_token": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.AUTH_TOKEN_MISSING,
                                                                            status.HTTP_400_BAD_REQUEST),
                                    funcLimits=[(len, base_limits.AUTH_TOKEN_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                     status.HTTP_400_BAD_REQUEST))])
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None


class PurgeQuizzesRequestDTO(RequestDTOValidator):
    """
    request serializer
    """

    rulesDict = {
        "auth_token": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.AUTH_TOKEN_MISSING,
                                                                            status.HTTP_400_BAD_REQUEST),
                                    funcLimits=[(len, base_limits.AUTH_TOKEN_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                     status.HTTP_400_BAD_REQUEST))]),
        "ids": ItemValidator(iterator=ItemValidator.LIST, funcList=[safeToInt, isNotNone],
                             errorCode=(BaseError.INVALID_QUIZ_IDS, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.ids = list()


class QuizMergeRequestDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "auth_token": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.AUTH_TOKEN_MISSING,
                                                                            status.HTTP_400_BAD_REQUEST),
                                    funcLimits=[(len, base_limits.AUTH_TOKEN_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                     status.HTTP_400_BAD_REQUEST))]),
        "name": ItemValidator(mandatory=False, funcList=[isString, strip],
                              errorCode=(BaseError.INVALID_QUIZ_NAME, status.HTTP_400_BAD_REQUEST)),
        "quiz1": ItemValidator(funcList=[safeToInt, isNotNone, str, strip, isStringNotEmpty],
                               errorCode=(BaseError.INVALID_QUIZ1_SOC_NUMBER, status.HTTP_400_BAD_REQUEST)),
        "quiz2": ItemValidator(funcList=[safeToInt, isNotNone, str, strip, isStringNotEmpty],
                               errorCode=(BaseError.INVALID_QUIZ2_SOC_NUMBER, status.HTTP_400_BAD_REQUEST)),
    }