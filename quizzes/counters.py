# coding=utf-8


class QuizCounters(object):
    """
    class that holds the name of the quiz counters
    """

    CREATE_QUIZ = 'socrative.views.quizzes.create_quiz.time'
    CREATE_QUIZ_FAIL = 'socrative.views.quizzes.create_quiz.fail'
    GET_QUIZ_FAIL = 'socrative.views.quizzes.get_quiz.fail'
    GET_QUIZ = 'socrative.views.quizzes.get_quiz.time'
    UPDATE_QUIZ_FAIL = 'socrative.views.quizzes.update_quiz.fail'
    UPDATE_QUIZ = 'socrative.views.quizzes.update_quiz.time'
    CREATE_QUESTION = 'socrative.views.quizzes.create_question.time'
    CREATE_QUESTION_FAIL = 'socrative.views.quizzes.create_question.fail'
    DELETE_QUIZ = 'socrative.views.quizzes.delete_quiz.time'
    DELETE_QUIZ_FAIL = 'socrative.views.quizzes.delete_quiz.fail'
    DELETE_QUESTION_FAIL = 'socrative.views.quizzes.delete_question.fail'
    DELETE_QUESTION = 'socrative.views.quizzes.delete_question.time'
    UPDATE_QUESTION = 'socrative.views.quizzes.update_question.time'
    UPDATE_QUESTION_FAIL = 'socrative.views.quizzes.update_question.fail'
    LIST_QUIZZES_FAIL = 'socrative.views.quizzes.list_quizzes.fail'
    LIST_QUIZZES = 'socrative.views.quizzes.list_quizzes.time'
    EDIT_QUIZ = 'socrative.views.quizzes.edit_quiz.time'
    EDIT_QUIZ_FAIL = 'socrative.views.quizzes.edit_quiz.fail'
    GET_ACTIVITY_REPORT_FAIL = 'socrative.views.quizzes.activity_report.fail'
    GET_ACTIVITY_REPORT = 'socrative.views.quizzes.activity_report.time'
    IMPORT_QUIZ_BY_SOC_FAIL = 'socrative.views.quizzes.import_quiz_by_soc.fail'
    IMPORT_QUIZ_BY_SOC = 'socrative.views.quizzes.import_quiz_by_soc.time'
    DOWNLOAD_REPORT_FAIL = 'socrative.views.quizzes.download_report.fail'
    DOWNLOAD_REPORT = 'socrative.views.quizzes.download_report.time'
    IMPORT_QUIZ_BY_EXCEL_FAIL = 'socrative.views.quizzes.import_quiz_by_excel.fail'
    IMPORT_QUIZ_BY_EXCEL = 'socrative.views.quizzes.import_quiz_by_excel.time'
    GET_QUIZ_SHARE_URL_FAIL = 'socrative.views.quizzes.get_share_quiz_url.fail'
    GET_QUIZ_SHARE_URL = 'socrative.views.quizzes.get_share_quiz_url.time'
    SHARE_QUIZ_BY_MAIL_FAIL = 'socrative.views.quizzes.share_quiz_by_mail.fail'
    SHARE_QUIZ_BY_MAIL = 'socrative.views.quizzes.share_quiz_by_mail.time'
    REORDER_QUESTIONS_FAIL = 'socrative.views.quizzes.reorder_questions.fail'
    REORDER_QUESTIONS = 'socrative.views.quizzes.reorder_questions.time'

