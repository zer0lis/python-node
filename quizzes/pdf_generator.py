# coding=utf-8
import copy
import datetime
import logging
import os
import re
import time
import ujson
import unicodedata
import hashlib
from cStringIO import StringIO
from multiprocessing.queues import Queue, Empty
from threading import Thread

import magic
import requests
from datadog import statsd
from django.utils.translation import ugettext
from lxml.html.clean import Cleaner
from reportlab.lib.colors import HexColor
from reportlab.lib.enums import TA_LEFT, TA_CENTER, TA_JUSTIFY, TA_RIGHT
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from reportlab.platypus import SimpleDocTemplate, Paragraph, Image, Table
from reportlab.platypus.doctemplate import LayoutError
from wand.image import Image as wandImage

from common.dao import MediaResourceModel
from common.socrative_api import exceptionStack, htmlEscape

logger = logging.getLogger(__name__)

def getAlphaOrder(order):
    """
    return alphabetical order
    :param order:
    :return:
    """
    resp = ''
    order -= 1
    if order > 0:
        first = True
        while order > 0:
            if first:
                resp = chr(ord('A') + (order % 26)) + resp
            else:
                resp = chr(ord('A') + (order % 26)-1) + resp
            order /= 26
            first = False
    else:
        resp = 'A'

    return resp


def getRows(flowable):
    """

    :param flowable:
    :return:
    """

    if type(flowable) is str:
        return 1

    if hasattr(flowable, "blPara"):
        return len(flowable.blPara.lines)
    elif hasattr(flowable, "frags"):
        return len(flowable.frags)
    else:
        return 1


def computeHeight(flowableList):
    """

    :param flowableList:
    :return:
    """
    total_height = 0
    for i in range(len(flowableList)):
        heights = []
        for j in range(len(flowableList[i])):
            if hasattr(flowableList[i][j], "height"):
                heights.append(flowableList[i][j].height)
            elif hasattr(flowableList[i][j], "_height"):
                heights.append(flowableList[i][j]._height)
            else:
                heights.append(24) # default row height
        total_height += max(heights)

    return total_height

def handleBmpAndPng(imageBuffer):
    """
    transcode bmp or png into png images -> hopefully that can be decoded by Pillow
    :param imageBuffer:
    :return: image buffer encoded as png
    """
    output = StringIO()
    _input = StringIO(imageBuffer)
    try:
        img = wandImage(file=_input)
        img.format = "png"
        img.save(file=output)
        img.close()
        output.seek(0, 0)
        data = output.read()
        return data
    except Exception as e:
        logger.error(exceptionStack(e))
        return imageBuffer
    finally:
        output.close()
        _input.close()


class Circle(Paragraph):
    """
    Class that draws a circle having the center in the center of the paragraph box
    """

    def __init__(self, text, style, bulletText=None, frags=None, caseSensitive=1, encoding='utf8', fill=0, color=None):
        """
        """
        Paragraph.__init__(self, text, style, bulletText, frags, caseSensitive, encoding)
        self.fill = fill
        self.fillcolor = color

    def draw(self):
        """
        overwritten the draw method
        """
        tmpCanvas = self.canv

        tmpCanvas.saveState()
        if self.fillcolor:
            tmpCanvas.setFillColor(self.fillcolor)
            tmpCanvas.setStrokeColor((self.fillcolor if self.fill else "#555555"))
        tmpCanvas.circle(self.width/2, self.height/2, max(self.width, self.height)/2, 1, self.fill)
        tmpCanvas.restoreState()

        Paragraph.draw(self)


class NumberedCanvas(canvas.Canvas):
    """
    Class that adds page numbers after each page
    """

    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self._saved_page_states = []

    def showPage(self):
        """
        adds a page
        :return:
        """
        self._saved_page_states.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        """
        add page info to each page (page x of y)
        """
        num_pages = len(self._saved_page_states)
        for state in self._saved_page_states:
            self.__dict__.update(state)
            self.draw_page_number(num_pages)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)

    def draw_page_number(self, page_count):
        """
        draws the page number
        :param page_count: number of pages
        """
        self.setFont("OpenSans", 10)
        color = self._fillColorObj
        self.setFillColor("#555555")
        self.drawRightString(555, 18, ugettext("Page %d of %d" % (self._pageNumber, page_count)))

        if self._doc.info.keywords:
            self.drawString(55, 18, self._doc.info.keywords)

        self.setFillColor(color)


class BasePdfClass(object):
    """
    Base class to hold styles
    """

    BASE_PATH = os.path.abspath(os.path.dirname(__file__))
    FONT_PATH = os.path.abspath(os.path.dirname(__file__) + "/../socrative/")
    LOGO_IMAGE_PATH = FONT_PATH + "/static/img/logo_new.png"
    CORRECT_IMAGE_PATH = BASE_PATH + "/static/img/correct.png"
    INCORRECT_IMAGE_PATH = BASE_PATH + "/static/img/wrong.png"
    NO_GRADE_IMAGE_PATH = BASE_PATH + "/static/img/no_grade.png"

    SOCRATIVE_PRO_IMAGE_PATH = FONT_PATH + "/static/img/soc-pro.png"
    RECEIPT_HEADER_PATH = FONT_PATH + "/static/img/header.png"

    # the max height of a question on the page with answers and all
    MAX_HEIGHT = 680

    # OpenSans family
    pdfmetrics.registerFont(TTFont('OpenSans', FONT_PATH + '/static/fonts/OpenSans-Regular.ttf'))
    pdfmetrics.registerFont(TTFont('OpenSans-Bold', FONT_PATH + '/static/fonts/OpenSans-Bold.ttf'))
    pdfmetrics.registerFont(TTFont('OpenSans-Italic', FONT_PATH + '/static/fonts/OpenSans-Italic.ttf'))
    pdfmetrics.registerFont(TTFont('OpenSans-BoldItalic', FONT_PATH + '/static/fonts/OpenSans-BoldItalic.ttf'))
    pdfmetrics.registerFontFamily('OpenSans', bold='OpenSans-Bold', italic='OpenSans-Italic',
                                  boldItalic='OpenSans-BoldItalic', normal='OpenSans')

    # OpenSans SemiBold family
    pdfmetrics.registerFont(TTFont('OpenSansSB', FONT_PATH + '/static/fonts/OpenSansSB.ttf'))
    pdfmetrics.registerFont(TTFont('OpenSansSB-Bold', FONT_PATH + '/static/fonts/OpenSans-Bold.ttf'))
    pdfmetrics.registerFont(TTFont('OpenSansSB-Italic', FONT_PATH + '/static/fonts/OpenSansSB-Italic.ttf'))
    pdfmetrics.registerFont(TTFont('OpenSansSB-BoldItalic', FONT_PATH + '/static/fonts/OpenSans-BoldItalic.ttf'))
    pdfmetrics.registerFontFamily('OpenSansSB', bold='OpenSansSB-Bold', italic='OpenSansSB-Italic',
                                  boldItalic='OpenSansSB-BoldItalic', normal='OpenSansSB')

    # OpenSans family
    pdfmetrics.registerFont(TTFont('russian', FONT_PATH + '/static/fonts/OpenSans-Regular.ttf'))
    pdfmetrics.registerFont(TTFont('russian-bold', FONT_PATH + '/static/fonts/OpenSans-Bold.ttf'))
    pdfmetrics.registerFont(TTFont('russian-italic', FONT_PATH + '/static/fonts/OpenSans-Italic.ttf'))
    pdfmetrics.registerFont(TTFont('russian-bolditalic', FONT_PATH + '/static/fonts/OpenSans-BoldItalic.ttf'))
    pdfmetrics.registerFontFamily('russian', bold='russian-bold', italic='russian-italic',
                                  boldItalic='russian-bolditalic', normal='russian')

    pdfmetrics.registerFont(TTFont('greek', FONT_PATH + '/static/fonts/greek.ttf'))
    pdfmetrics.registerFont(TTFont('greek-bold', FONT_PATH + '/static/fonts/greek-bold.ttf'))
    pdfmetrics.registerFont(TTFont('greek-italic', FONT_PATH + '/static/fonts/greek-italic.ttf'))
    pdfmetrics.registerFont(TTFont('greek-bolditalic', FONT_PATH + '/static/fonts/greek-bolditalic.ttf'))
    pdfmetrics.registerFontFamily('greek', bold='greek-bold', italic='greek-italic',
                                  boldItalic='greek-bolditalic', normal='greek')

    # OpenSans family
    pdfmetrics.registerFont(TTFont('vietnamese', FONT_PATH + '/static/fonts/OpenSans-Regular.ttf'))
    pdfmetrics.registerFont(TTFont('vietnamese-bold', FONT_PATH + '/static/fonts/OpenSans-Bold.ttf'))
    pdfmetrics.registerFont(TTFont('vietnamese-italic', FONT_PATH + '/static/fonts/OpenSans-Italic.ttf'))
    pdfmetrics.registerFont(TTFont('vietnamese-bolditalic', FONT_PATH + '/static/fonts/OpenSans-BoldItalic.ttf'))
    pdfmetrics.registerFontFamily('vietnamese', bold='vietnamese-bold', italic='vietnamese-italic',
                                  boldItalic='vietnamese-bolditalic', normal='vietnamese')

    # Oswald family
    pdfmetrics.registerFont(TTFont('Oswald', FONT_PATH + '/static/fonts/Oswald-Regular.ttf'))
    pdfmetrics.registerFont(TTFont('Oswald-Italic', FONT_PATH + '/static/fonts/Oswald-Italic.ttf'))
    pdfmetrics.registerFont(TTFont('Oswald-Bold', FONT_PATH + '/static/fonts/Oswald-Bold.ttf'))
    pdfmetrics.registerFont(TTFont('Oswald-BoldItalic', FONT_PATH + '/static/fonts/Oswald-BoldItalic.ttf'))
    pdfmetrics.registerFontFamily('Oswald', bold='Oswald-Bold', italic='Oswald-Italic', boldItalic='Oswald-BoldItalic',
                                  normal='Oswald')

    # hebrew
    pdfmetrics.registerFont(TTFont('hebrew', FONT_PATH + '/static/fonts/hebrew.ttf'))
    pdfmetrics.registerFont(TTFont('hebrew-bold', FONT_PATH + '/static/fonts/hebrew-bold.ttf'))
    pdfmetrics.registerFont(TTFont('hebrew-italic', FONT_PATH + '/static/fonts/hebrew-italic.ttf'))
    pdfmetrics.registerFont(TTFont('hebrew-bolditalic', FONT_PATH + '/static/fonts/hebrew-bold.ttf'))
    pdfmetrics.registerFontFamily('hebrew', normal='hebrew', bold='hebrew-bold', italic='hebrew-italic',
                                  boldItalic='hebrew-bolditalic')

    # thai
    pdfmetrics.registerFont(TTFont('thai', FONT_PATH + '/static/fonts/thai.ttf'))
    pdfmetrics.registerFont(TTFont('thai-bold', FONT_PATH + '/static/fonts/thai-bold.ttf'))
    pdfmetrics.registerFontFamily('thai', normal='thai', bold='thai-bold')

    # indian
    pdfmetrics.registerFont(TTFont('indian', FONT_PATH + '/static/fonts/indian.ttf'))
    pdfmetrics.registerFont(TTFont('indian-bold', FONT_PATH + '/static/fonts/indian-bold.ttf'))
    pdfmetrics.registerFontFamily('indian', normal='indian', bold='indian-bold')

    # bengali
    pdfmetrics.registerFont(TTFont('bengali', FONT_PATH + '/static/fonts/bengali.ttf'))
    pdfmetrics.registerFont(TTFont('bengali-bold', FONT_PATH + '/static/fonts/bengali-bold.ttf'))
    pdfmetrics.registerFontFamily('bengali', normal='bengali', bold='bengali-bold')

    # hindu
    pdfmetrics.registerFont(TTFont('hindu', FONT_PATH + '/static/fonts/hindu.ttf'))
    pdfmetrics.registerFont(TTFont('hindu-bold', FONT_PATH + '/static/fonts/hindu-bold.ttf'))
    pdfmetrics.registerFontFamily('hindu', normal='hindu', bold='hindu-bold')

    # korean
    pdfmetrics.registerFont(TTFont('korean', FONT_PATH + '/static/fonts/korean.ttf'))
    pdfmetrics.registerFont(TTFont('japanese', FONT_PATH + '/static/fonts/japanese.ttf'))
    pdfmetrics.registerFont(TTFont('chinese', FONT_PATH + '/static/fonts/wqy-zenhei.ttc'))

    # arabic
    pdfmetrics.registerFont(TTFont('arabic', FONT_PATH + '/static/fonts/arabic.ttf'))
    pdfmetrics.registerFont(TTFont('arabic-bold', FONT_PATH + '/static/fonts/arabic-bold.ttf'))
    pdfmetrics.registerFont(TTFont('arabic-bolditalic', FONT_PATH + '/static/fonts/arabic-bolditalic.ttf'))
    pdfmetrics.registerFont(TTFont('arabic-italic', FONT_PATH + '/static/fonts/arabic-italic.ttf'))
    pdfmetrics.registerFontFamily('arabic', normal='arabic', bold='arabic-bold', italic='arabic-italic',
                                  boldItalic='arabic-bolditalic')

    pdfmetrics.registerFont(TTFont('armenian', FONT_PATH + '/static/fonts/armenian.ttf'))
    
    # OpenSans family
    pdfmetrics.registerFont(TTFont('symbols', FONT_PATH + '/static/fonts/OpenSans-Regular.ttf'))
    pdfmetrics.registerFont(TTFont('symbols-bold', FONT_PATH + '/static/fonts/OpenSans-Bold.ttf'))
    pdfmetrics.registerFont(TTFont('symbols-italic', FONT_PATH + '/static/fonts/OpenSans-Italic.ttf'))
    pdfmetrics.registerFont(TTFont('symbols-bolditalic', FONT_PATH + '/static/fonts/OpenSans-BoldItalic.ttf'))
    pdfmetrics.registerFontFamily('symbols', bold='symbols-bold', italic='symbols-italic',
                                  boldItalic='symbols-bolditalic', normal='symbols')

    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER))
    styles.add(ParagraphStyle(name='Left', alignment=TA_LEFT))
    styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
    styles.add(ParagraphStyle(name='Right', alignment=TA_RIGHT))

    # colors
    socrativeGray = HexColor('#cccccc')
    studentQuestionSelected = HexColor(0x2a3e15)

    fontDict = {'hebrew': 'hebrew', 'cjk': 'chinese', 'thai': 'thai', 'hangul': 'korean', 'bengali': 'bengali',
                'arabic': 'arabic', 'hiragana': 'japanese', 'armenian': 'armenian', 'cyrillic': 'russian',
                'devanagari': 'hindu', 'gujarati': 'indian', "with hook above": 'vietnamese',
                'with dot below': 'vietnamese', 'with circumflex and hook above': 'vietnamese', 'greek': 'greek',
                'gamma': 'symbols', 'modifier letter': 'symbols', 'combining': 'symbols'}

    fontMapping = {'hebrew': 'hebrew', 'chinese': 'chinese', 'thai': 'thai', 'korean': 'korean', 'bengali': 'bengali',
                   'arabic': 'arabic', 'japanese': 'japanese', 'armenian': 'armenian', 'russian': 'OpenSans',
                   'hindu': 'hindu', 'indian': 'indian', 'vietnamese': 'OpenSans', 'greek': 'greek',
                   'symbols': 'OpenSans', 'Oswald': 'Oswald', 'OpenSans': 'OpenSans'}

    ESCAPED_HTML = re.compile(r'.*(&amp;|&gt;|&lt;|&emsp;|&quot;|&#x27;).*')
    START_TAG = re.compile(r"^<(/)?(br|p|i|u|b|font|sup|sub|div|em|strong|span)(\ .+)*?>")

    startFontTemplate = "<font %s face=%s>"
    endFontTemplate = "</font>"

    @classmethod
    def brrepl(cls, matchobj):
        """

        :param matchobj:
        :return:
        """
        return '<br>'

    def getFontMapping(self, fontName):
        """

        :param fontName:
        :return:
        """

        return self.fontMapping.get(fontName) or "OpenSans"

    def __matchTags(self, htmlCloseTag, htmlStartTag):
        """
        """
        temp = htmlCloseTag.replace('/', '')[:-1]
        return htmlStartTag.startswith(temp)

    def __getEndTag(self, tag):
        """
        """
        temp = tag.replace('>', ' ')

        spaceIndex = temp.find(' ')

        if spaceIndex == -1:
            raise Exception("Malformed html tag: %s" % tag)

        return '</' + temp[1:spaceIndex] + '>'

    def __closeHtmlTags(self, htmlCloseTag, htmlStartTagsStack):
        """
        """
        htmlTagList = []
        temp = htmlCloseTag.replace('/', '')[:-1]

        htmlTagList.append(htmlCloseTag)

        insert = False
        for tag in htmlStartTagsStack:
            if insert is True:
                htmlTagList.insert(0, self.__getEndTag(tag))
                htmlTagList.append(tag)
                continue
            if tag.startswith(temp):
                insert = True

        return htmlTagList

    def __fixHtmlTags(self, textList):
        """
        it makes sure that each html start tag has it's own end tag where it supposed to
        """

        # oook so we need to match starting html tags with ending html tags
        # wysiwyg all over again :)

        updatedList = list()
        started_tags = list()

        modified = False
        index = 0
        for text in textList:
            if text.startswith('</'):
                if text.startswith('</') and self.__matchTags(text, started_tags[-1]) is True:
                    # if the end tag has the start tag in the stack, then remove it from start tag from the stack
                    # update the text list
                    updatedList.append(text)
                    started_tags = started_tags[:-1]
                else:
                    # if they don't match , add the closing tags and reopen the ones that were not closed,so the
                    # formatting is not lost
                    updatedList += self.__closeHtmlTags(text, started_tags)
                    modified = True

                    # once we make a modification, let's go again an rearrange the formatting tags inside <font> tags
                    break
            elif text.startswith('<'):
                # as long as we read html start tags, add them on the stack and in the text list
                started_tags.append(text)
                updatedList.append(text)
            else:
                # if it's just text, add it in the list
                updatedList.append(text)
            index += 1

        if index < len(textList):
            # if we didn't reached the end, append the remaining list of items
            updatedList = updatedList + textList[index+1:]

        return updatedList, modified

    def __simplifyFontTags(self, texts):
        """

        :param texts:
        :return:
        """

        # remove extra font tags
        newTexts = []
        last_font = None
        last_font_closed_index = -1
        remove_close_tag = False

        for text in texts:
            if text.startswith("<font"):
                if text == last_font:
                    # we have the same font as the last we used , no need of duplicating the tag
                    # check if we closed the last font tag
                    if last_font_closed_index != -1:
                        newTexts.pop(last_font_closed_index)
                        remove_close_tag = False
                    else:
                        remove_close_tag = True
                else:
                    last_font = text
                    last_font_closed_index = -1
                    remove_close_tag = False
                    newTexts.append(text)
            elif text.startswith("</font"):
                if remove_close_tag:
                    continue
                newTexts.append(text)
                last_font_closed_index = len(newTexts) - 1
            else:
                newTexts.append(text)

        return newTexts

    def appendHtmlSafe(self, texts, text):
        """

        :param texts:
        :param text:
        :return:
        """

        if '<' in text or '>' in text:
            texts.append(htmlEscape(text))
        else:
            texts.append(text)

    def formatText(self, textToBeFormatted, default="OpenSans", formatString=""):
        """
        :param textToBeFormatted:
        :param default:
        :param formatString:
        """
        # if it's unicode but can be transformed to utf-8 by calling str
        if type(textToBeFormatted) is str:
            htmlText = textToBeFormatted.decode('utf-8')
        else:
            htmlText = textToBeFormatted

        text = self.removeBrTag(htmlText)

        startIndex = 0
        endIndex = 0
        font = default
        texts = []

        # make sure the fonts and tags are well placed and enclosed.
        htmlTagStarted = False
        for s in text:
            if htmlTagStarted:
                endIndex += 1
                if s == '>':
                    htmlTagStarted = False
                    texts.append(text[startIndex:endIndex])
                    startIndex = endIndex
                    font = default
                continue

            if htmlTagStarted is False and s == '<':
                if self.START_TAG.match(text[endIndex:]) is not None:
                    htmlTagStarted = True
                    if startIndex < endIndex:
                        texts.append(self.startFontTemplate % (formatString, self.getFontMapping(font)))
                        self.appendHtmlSafe(texts, text[startIndex:endIndex])
                        texts.append(self.endFontTemplate)

                    font = default
                    startIndex = endIndex
            name = ""
            try:
                name = unicodedata.name(s).lower()
            except Exception as e:
                logger.debug(exceptionStack(e))

            newFonts = [self.fontDict[a] for a in self.fontDict if a in name]
            newFont = newFonts[0] if newFonts else default

            # if it starts with latin or continues with latin, leave change to the new font and apply it to the string
            # up until now
            if newFont != font and font == default:
                font = newFont

            if newFont != font and newFont != default and endIndex > 0:
                texts.append(self.startFontTemplate % (formatString, self.getFontMapping(font)))
                self.appendHtmlSafe(texts, text[startIndex:endIndex])
                texts.append(self.endFontTemplate)
                font = newFont
                startIndex = endIndex
            elif newFont != font and newFont != default:
                font = newFont

            endIndex += 1

        if startIndex < endIndex:
            texts.append(self.startFontTemplate % (formatString, self.getFontMapping(font)))
            self.appendHtmlSafe(texts, text[startIndex:endIndex])
            texts.append(self.endFontTemplate)

        # Enclose the formatting fonts inside the <font> </font> tags
        # Parse the list until we don't have to interchange tags
        found = True
        consecutiveFalse = 0
        start = time.time()
        while found:
            found = False
            for i in range(0, len(texts)-1):
                # move the font tag to enclose all text possible inside the div element
                if texts[i] not in ("</font>","<div>") and texts[i+1].startswith("<font"):
                    aux = texts[i]
                    texts[i] = texts[i+1]
                    texts[i+1] = aux
                    found = True
                elif texts[i] == "</font>" and texts[i+1] in ("</b>", "</i>", "</u>", "</sup>", "</sub>", "<br>",
                                                              "</br>"):
                    aux = texts[i]
                    texts[i] = texts[i+1]
                    texts[i+1] = aux
                    found = True

                if found:
                    break

            texts = self.__simplifyFontTags(texts)

            if found is False:
                # fix html tags inside font tags
                # the flag is modified because the list will be updated and the font tags must move again to enclose
                # formatting tags. If no change is made , found is returned as False
                texts, found = self.__fixHtmlTags(texts)
                consecutiveFalse += 1
            else:
                consecutiveFalse = 0

            if consecutiveFalse > 100:
                # avoid infinite loop if the fixHtmlTags doesn't do anything that needs to be updated
                logger.error(textToBeFormatted)
                break

            # avoid infinite loops. We need to investigate these strings that produce this behavior and fine tune the
            # algorithm to avoid this kind of situations
            end = time.time()
            if end - start > 1:
                logger.error(textToBeFormatted)
                break

        return ''.join(texts)

    def add_underline(self, text):
        """
        changes the <span class='underline'> text</span> to <u>text</u>
        :param text:
        :return:
        """
        s = text.replace("<span class=\"underline\">", "<u>")

        index = s.find("<u>")
        totalIndex = index
        while index >= 0:
            openedSpanIndex = s[totalIndex+5:].find("<span")
            spanIndex = s[totalIndex:].find("</span>")
            if openedSpanIndex > spanIndex or (openedSpanIndex == -1 and spanIndex > 0):
                s = s[:spanIndex+totalIndex] + "</u>" + s[spanIndex+totalIndex+7:]
                index = s.rfind("</u>")
                totalIndex = index
            elif 0 < openedSpanIndex < spanIndex:
                index = openedSpanIndex+1
                totalIndex += index
            elif spanIndex > 0:
                index = spanIndex + 7
                totalIndex += index
            else:
                break
        return s

    def removeBrTag(self, text):
        """
        :param text:
        """

        if text.strip() != "":
            s = text.replace("<br data-mce-bogus=\"1\">","<br>").replace("<br></p>", "<br>")
            s = s.replace("<span data-mce-bogus=\"true\" id=\"_mce_caret\">", "")
            s = s.replace("&nbsp;", " ").replace("<strong>", "<b>").replace("</strong>", "</b>").replace("<em>", "<i>")
            s = s.replace("</em>", "</i>").replace("<p>", "").replace("</p>", "<br>").replace("\n", "<br>")
            s = s.replace("\t", "    ")
            s = self.add_underline(s)
        else:
            s = text

        if s.strip() != "":
            cleaner = Cleaner(safe_attrs_only=True, safe_attrs=('src', 'href'), remove_tags=('p', 'span', 'img',
                                                                                             'html', 'font'))
            try:
                s = cleaner.clean_html(s)
                s = s.replace("<br>", "<br></br>").replace("</br></br>", "</br>")
            except Exception as e:
                logger.debug(exceptionStack(e))

        return s

    def splitParagraph(self, paragraph, width, height=None, orphan=None):
        """
        :param paragraph:
        :param width:
        """

        retList = list()
        pToSplit = paragraph
        while True:
            if orphan:
                pToSplit.allowOrphans = 1
            l = pToSplit.split(width, height or 600)
            if len(l) == 1:
                retList.append(l[0])
                break
            elif len(l) == 0:
                retList.append(pToSplit)
                break
            else:
                retList.append(l[0])
                pToSplit = l[1]

        if len(retList) > 1 and orphan is True:
            if len(retList[0].frags) >=1 and hasattr(retList[0].frags[0], "words") is True:
                retList[0].frags[0].words.append("...")
        return retList

    def buildPdf(self, title, flowables, canvasmaker, student_name=None):
        """
        method to fix build issues with flowables that can't fit the page
        :param flowables:
        :param canvasmaker:
        :return:
        """
        error = 0 # initialize it with 0 so we can go into the first while loop

        ioString = None
        index = -1
        while error is not None:
            try:
                # not memory friendly -- but for now I have no other idea on how to fix this
                # if I don't make a copy , the pdf generator removes each flowable (table/paragraph) after it process it
                flowables_copy = copy.deepcopy(flowables)

                ioString = StringIO()

                doc = SimpleDocTemplate(ioString, rightMargin=0, leftMargin=0, topMargin=20,
                                        bottomMargin=20, pagesize=letter)

                doc.author = "socrative.com"
                doc.title = title
                doc.creator = "socrative.com"

                if student_name:
                    doc.keywords = student_name

                doc.build(flowables_copy, canvasmaker=canvasmaker)
                error = None

                return ioString
            except LayoutError as e:
                logger.info(e.message)
                # the problematic flowable will be removed from the flowable list. we just need to check with the copy,
                # to see which one is it, and remove the nosplit commands
                bad_flowable_index = len(flowables)-len(flowables_copy)
                if bad_flowable_index == index:
                    raise

                if bad_flowable_index > 0:
                    flowables[bad_flowable_index-1]._nosplitCmds = list()
                    flowables[bad_flowable_index - 1]._nosplitRanges = dict()

                    index = bad_flowable_index
            finally:
                if error is not None:
                    ioString.close()


class ImageRetrieverThread(Thread):
    """
        thread class to retrieve image files
    """

    IMAGE_SUPPORTED_FORMATS = ("gif", "jpeg", "jpg", "jpe", "png", "tiff", "bmp", "wbmp", "x-ms-bmp")

    def __init__(self, urlQueue, resultQueue):
        super(ImageRetrieverThread, self).__init__()

        self.urlQueue = urlQueue
        self.resultQueue = resultQueue
        self.imgNotSupportedPath = os.path.abspath(os.path.dirname(__file__) + "/../socrative/static/img/img_not_supported.png")

    def run(self):
        """
        main thread method
        """

        while True:
            try:
                url = self.urlQueue.get(block=True, timeout=1)
                if url[1]:
                    response = requests.get(url[1])
                    if response.status_code == 200:
                        content = response.content
                        mimeType = magic.from_buffer(content, mime=True)
                        if 'image' in mimeType:
                            if mimeType.split('/')[1] in self.IMAGE_SUPPORTED_FORMATS:
                                if mimeType in ("image/png", "image/bmp", "image/x-ms-bmp"):
                                    handleBmpAndPng(content)
                                    imagePath = self.writeContent(content)
                                else:
                                    imagePath = self.writeContent(content)
                                self.resultQueue.put((url[0], imagePath))
                                continue
                if url[0]:
                    response = requests.get(url[0])
                    if response.status_code == 200:
                        content = response.content
                        mimeType = magic.from_buffer(content, mime=True)
                        if 'image' in mimeType:
                            if mimeType.split('/')[1] in self.IMAGE_SUPPORTED_FORMATS:
                                if mimeType in ("image/png", "image/bmp", "image/x-ms-bmp"):
                                    content = handleBmpAndPng(content)
                                    imagePath = self.writeContent(content)
                                else:
                                    imagePath = self.writeContent(content)
                                self.resultQueue.put((url[0], imagePath))
                            else:
                                self.resultQueue.put((url[0], self.imgNotSupportedPath))
                        else:
                            logger.error("The content for url: %s is not an image - mimetype : %s", url[0], mimeType)
                            self.resultQueue.put((url[0], self.imgNotSupportedPath))
                    else:
                        self.resultQueue.put((url[0], None))
                else:
                    self.resultQueue.put((url[0], None))
            except Empty:
                break
            except EOFError as e:
                logger.error(exceptionStack(e))
                break
            except Exception as e:
                logger.error(exceptionStack(e))

    def writeContent(self, content):
        """

        :param content:
        :return:
        """
        try:
            shaSum = hashlib.sha256()
            shaSum.update(content)
            filename = "/tmp/%s" % shaSum.hexdigest()

            # the file already exist, so don't bother to write the content again, it's the same file -> same sha256 hash
            if os.path.exists(filename):
                return filename

            with open(filename, "wb") as f:
                f.write(content)

            return filename
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.imgNotSupportedPath

class StudentPdfReport(BasePdfClass):
    """
    class to handle student pdf reports
    """
    LEFT_MARGIN = 50
    RIGHT_MARGIN = 55

    def __init__(self, daoRegistry, responses, questions, quiz, students, localTime, activity, context):

        super(StudentPdfReport, self).__init__()

        self.questions = questions
        self.responses = responses
        self.quiz = quiz
        self.context = context
        self.students = students
        self.activity = activity
        self.localTime = localTime
        self.studentScoreDict = dict((a.user_uuid, 0) for a in self.students)
        self.qResponses = dict()

        for r in self.responses:
            if r.question_id in self.qResponses:
                self.qResponses[r.question_id].append(r)
            else:
                self.qResponses[r.question_id] = [r]

        responseIds = [a.id for a in responses]

        questionIds = [q.question_id for q in questions]

        if context.qCADict is None:
            self.qCADict = daoRegistry.answerDao.getNbCorrectAnswersForQuestions(questionIds)
            context.qCADict = self.qCADict
        else:
            self.qCADict = context.qCADict

        if context.names is None:
            self.studentNameDict = daoRegistry.studentNameDao.getStudentNames(responseIds)
        else:
            self.studentNameDict = dict(((sn.user_uuid, sn.activity_instance_id), sn.name) for sn in context.names)

        self.imagesDict = daoRegistry.mediaResourceDao.getImages(questionIds, _type=MediaResourceModel.IMAGE)
        self.answersDict = daoRegistry.answerDao.loadAnswersForQuestions(questionIds)

        if context.textResponseDict is None:
            self.textResponseDict = daoRegistry.studentResponseTextAnswerDao.getTexts(responseIds)
            context.textResponseDict = self.textResponseDict
        else:
            self.textResponseDict = context.textResponseDict

        # get text from answer selections
        answerSelectionDict = daoRegistry.studentResponseAnswerSelectionDao.getTexts(responseIds)

        self.selectionChoiceDict = daoRegistry.studentResponseAnswerSelectionDao.getChoiceList(responseIds)

        self.textResponseDict.update(answerSelectionDict)

        self.standardName = daoRegistry.standardsDao.getStandardName(quiz.id)

    def questionTemplate(self, order, text, is_correct):
        """

        :param order:
        :param text:
        :param is_correct:
        :return:
        """

        data = list()

        mark = ""
        if is_correct is True:
            mark = Image(self.CORRECT_IMAGE_PATH, height=12, width=17, hAlign='RIGHT')
        elif is_correct is False:
            mark = Image(self.INCORRECT_IMAGE_PATH, height=12, width=11, hAlign='RIGHT')
        elif is_correct is None:
            mark = Image(self.NO_GRADE_IMAGE_PATH, height=17, width=17, hAlign='RIGHT')
        p1 = mark if mark != "" else Paragraph("", style=self.styles["Right"])

        question = self.formatText(text, "OpenSans", formatString="size=12 color=#555555")
        question = question.replace(" face=OpenSans", " face=OpenSansSB")
        p2 = Paragraph("<font face=OpenSansSB size=12 color=#555555>%d." % order + "&nbsp;"*4 + "</font>%s" % question,
                       style=self.styles["Left"])

        paras = self.splitParagraph(p2, 500)
        data.append([p1, paras[0], ""])

        for p in paras[1:]:
            data.append(["", p, ""])

        return data

    def answerTemplate(self, flowableList, order, correct, text, img, last=False, has_image=False, chosen=None,
                       offset=None, first=None):
        """
        :param flowableList
        :param order
        :param correct
        :param text
        :param img
        :param last
        :param has_image
        :param chosen:
        """

        if last is True:
            if img:
                imgFlow = Image(img, height=140, width=140, kind="proportional")

                for row in flowableList[:offset]:
                    row += ["", ""]

                if len(flowableList) == offset:
                    flowableList.append(["", "", "", ""])
                flowableList[offset].insert(3, imgFlow)

                for i in range(offset + 1, len(flowableList)):
                    flowableList[i].append("")

                # compute how many rows do we span for the image
                span = -1
                pixels = 0
                maxHeight = imgFlow.drawHeight + 35
                i = -1
                for i in range(offset, len(flowableList)):
                    t = flowableList[i][2].getPlainText() if type(flowableList[i][2]) not in (str, unicode) else \
                    flowableList[i][2]
                    pixels += (int(len(t)/85) + getRows(flowableList[i][2]))*25
                    if pixels >= maxHeight:
                        break

                if pixels >= maxHeight:
                    span = i
                else:
                    diff = int((maxHeight - pixels) / 25)
                    for _ in range(diff):
                        flowableList.append(["", "", "", "", ""])

                styles = [("SPAN", (1, it), (3, it)) for it in range(offset)]
                styles.append(("SPAN", (3, offset), (3, span)))

                for j in range(i+1, len(flowableList)):
                    styles.append(("SPAN", (2, j), (3, j)))

                styles += [
                    ("TOPPADDING", (0, offset), (-1, offset), 5),
                    ("TOPPADDING", (1, offset), (1, offset), 6),
                    ("VALIGN", (1, offset), (-1, -1), "TOP"),
                    ("LEFTPADDING", (1, offset), (1, -1), 0),
                    ("RIGHTPADDING", (1, offset), (1, -1), 0),
                    ("BOTTOMPADDING", (1, offset), (1, -1), 5),
                    ("BOTTOMPADDING", (2, offset), (2, -1), 5),
                    ("LEFTPADDING", (3, offset), (3, -1), 0)
                ]

                styles += [
                    ("RIGHTPADDING", (0, 0), (0, -1), 10),
                    ("LEFTPADDING", (1, 0), (1, offset-1), 1),
                    ("TOPPADDING", (0, 0), (-1, 0), (20 if first else 15)),
                    ("ALIGN", (0, 0), (0, -1), "RIGHT"),
                    ("VALIGN", (0, 0), (0, 0), "TOP")
                ]

                if len(flowableList) > offset + 1:
                    styles += [
                        ("TOPPADDING", (1, offset + 1), (1, -1), 5)
                    ]

                question_height = computeHeight(flowableList)
                if question_height < self.MAX_HEIGHT:
                    styles.append(('NOSPLIT', (0, 0), (-1, -1)))

                if hasattr(imgFlow, "_img"):
                    imgFlow._img.fp.close()
                    del imgFlow._img

                t1 = Table(flowableList, splitByRow=1, colWidths=(self.LEFT_MARGIN, 19, 335, 140, self.RIGHT_MARGIN),
                           style=styles)
            else:

                for row in flowableList[:offset]:
                    row.append("")

                styles = [("SPAN", (1, i), (2, i)) for i in range(offset)]
                styles += [
                    ("TOPPADDING", (0, offset), (-1, offset), 5),
                    ("TOPPADDING", (1, offset), (1, offset), 6),
                    ("VALIGN", (1, offset), (-1, -1), "TOP"),
                    ("LEFTPADDING", (1, offset), (1, -1), 0),
                    ("RIGHTPADDING", (1, offset), (1, -1), 0),
                    ("BOTTOMPADDING", (1, offset), (1, -1), 5),
                    ("BOTTOMPADDING", (2, offset), (2, -1), 5),
                ]

                styles += [
                    ("RIGHTPADDING", (0, 0), (0, -1), 10),
                    ("LEFTPADDING", (1, 0), (1, offset - 1), 1),
                    ("TOPPADDING", (0, 0), (-1, 0), (20 if first else 15)),
                    ("ALIGN", (0, 0), (0, -1), "RIGHT"),
                    ("VALIGN", (0, 0), (0, 0), "TOP")
                ]

                if len(flowableList) > 1:
                    styles += [
                        ("TOPPADDING", (1, 1), (1, -1), 5)
                    ]

                question_height = computeHeight(flowableList)
                if question_height < self.MAX_HEIGHT:
                    styles.append(('NOSPLIT', (0, 0), (-1, -1)))

                t1 = Table(flowableList, splitByRow=1, colWidths=(self.LEFT_MARGIN, 19, 475, self.RIGHT_MARGIN),
                           style=styles)

            return t1

        fillColor = "#ffffff"
        textColor = "#555555"
        if correct is True:
            fillColor = "#6ea523"
            textColor = "#ffffff"
        elif correct is False:
            fillColor = "#e31212"
            textColor = "#ffffff"
        elif correct is None and chosen is True:
            fillColor = "#cccccc"
            textColor = "#ffffff"
        p1 = Circle("<font size=9 color=%s face=OpenSans> %s </font>""" % (textColor, getAlphaOrder(order)),
                    style=self.styles["Center"], fill=1 if correct is not None else 1 if chosen else 0, color=fillColor)

        answer = self.formatText(text, "OpenSans", formatString="size=10 color=#555555")

        p2 = Paragraph(answer, style=self.styles["Left"])
        paras = self.splitParagraph(p2, 298 if has_image else 475, height=(140 if has_image else None))
        flowableList.append(["", p1, paras[0], ""])
        for p in paras[1:]:
            flowableList.append(["", "", p, ""])

    def frAnswerTemplate(self, flowableList, text, img, correct=None, last=False, has_image=False, offset=None,
                         first=None):
        """
        :param flowableList:
        :param text:
        :param img:
        :param last:
        :param correct:
        :param has_image
        """

        if last is True:
            if img:
                imgFlow = Image(img, height=140, width=140, kind="proportional")

                for row in flowableList[:offset]:
                    row += ["", ""]

                if len(flowableList) == offset:
                    flowableList.append(["", "", "", ""])
                flowableList[offset].insert(3, imgFlow)

                for i in range(offset + 1, len(flowableList)):
                    flowableList[i].append("")

                # compute how many rows do we span for the image
                span = -1
                pixels = 0
                i = -1
                for i in range(offset, len(flowableList)):
                    t = flowableList[i][2].getPlainText() if type(flowableList[i][2]) not in (str, unicode) else flowableList[i][2]
                    pixels += (int(len(t) / 85) + getRows(flowableList[i][2])) * 14
                    if pixels >= 140:
                        break

                if pixels >= 140:
                    span = i
                else:
                    diff = int((140 - pixels) / 40)
                    for _ in range(diff):
                        flowableList.append(["", "", "", "", ""])

                styles = [("SPAN", (1, it), (3, it)) for it in range(offset)]
                styles.append(("SPAN", (3, offset), (3, span)))

                for j in range(i + 1, len(flowableList)):
                    styles.append(("SPAN", (2, j), (3, j)))

                styles += [
                    ("TOPPADDING", (0, offset), (-1, offset), 5),
                    ("TOPPADDING", (1, offset), (1, offset), 15),
                    ("VALIGN", (1, offset), (-1, -1), "TOP"),
                    ("LEFTPADDING", (1, offset), (1, -1), 0),
                    ("RIGHTPADDING", (1, offset), (1, -1), 0),
                    ("BOTTOMPADDING", (1, offset), (1, -1), 0),
                ]

                styles += [
                    ("RIGHTPADDING", (0, 0), (0, -1), 10),
                    ("LEFTPADDING", (1, 0), (1, offset - 1), 1),
                    ("TOPPADDING", (0, 0), (-1, 0), (20 if first else 15)),
                    ("ALIGN", (0, 0), (0, -1), "RIGHT"),
                    ("VALIGN", (0, 0), (0, 0), "TOP")
                ]

                if len(flowableList) > 1:
                    styles += [
                        # ("TOPPADDING", (0, 2), (-1, -1), 1),
                        ("TOPPADDING", (1, 1), (1, -1), 5)
                    ]

                if computeHeight(flowableList) < self.MAX_HEIGHT:
                    styles.append(('NOSPLIT', (0, 0), (-1, -1)))

                if hasattr(imgFlow, "_img"):
                    imgFlow._img.fp.close()
                    del imgFlow._img

                t1 = Table(flowableList, splitByRow=1, colWidths=(self.LEFT_MARGIN, 19, 335, 140, self.RIGHT_MARGIN),
                           style=styles)
            else:
                for row in flowableList[:offset]:
                    row.append("")

                styles = [("SPAN", (1, it), (2, it)) for it in range(offset)]

                if len(flowableList) > offset:
                    styles += [
                    ("TOPPADDING", (0, offset), (-1, offset), 5),
                    ("TOPPADDING", (1, offset), (1, offset), 15),
                    ("VALIGN", (1, offset), (-1, -1), "TOP"),
                    ("LEFTPADDING", (1, offset), (1, -1), 0),
                    ("RIGHTPADDING", (1, offset), (1, -1), 0),
                    ("BOTTOMPADDING", (1, offset), (1, -1), 0)
                ]

                styles += [
                    ("RIGHTPADDING", (0, 0), (0, -1), 10),
                    ("LEFTPADDING", (1, 0), (1, offset - 1), 1),
                    ("TOPPADDING", (0, 0), (-1, 0), (20 if first else 15)),
                    ("ALIGN", (0, 0), (0, -1), "RIGHT"),
                    ("VALIGN", (0, 0), (0, 0), "TOP")
                ]

                if len(flowableList) > offset + 1:
                    styles += [
                        # ("TOPPADDING", (0, 2), (-1, -1), 1),
                        ("TOPPADDING", (1, 1), (1, -1), 5)
                    ]

                if computeHeight(flowableList) < self.MAX_HEIGHT:
                    styles.append(('NOSPLIT', (0, 0), (-1, -1)))

                t1 = Table(flowableList, splitByRow=1, colWidths=(self.LEFT_MARGIN, 19, 475, self.RIGHT_MARGIN),
                           style=styles)

            return t1

        answer = self.formatText(text, "OpenSans", formatString="size=10 color=#555555")

        p2 = Paragraph(answer, style=self.styles["Left"])
        paras = self.splitParagraph(p2, 298 if has_image else 475, height=(140 if has_image else None))
        flowableList.append(["", "", paras[0], ""])
        for p in paras[1:]:
            flowableList.append(["", "", p, ""])

    def headerTemplate(self, studentName, quizName, activityDate, activityScore, correctTotalTuple):
        """
        :param studentName:
        :param quizName:
        :param activityDate:
        :param activityScore:
        :param correctTotalTuple: number of correct and number of total gradable questions
        """

        student = self.formatText(studentName, default="OpenSans", formatString="size=11 color=#555555")
        p1 = Paragraph(student, style=self.styles["Left"])

        p2 = Paragraph("<font face=OpenSans size=11 color=#555555>" + activityDate.strftime("%m/%d/%Y") + "</font>",
                       style=self.styles["Right"])

        if self.standardName is not None:
            p3 = Paragraph("<font face=OpenSans size=11 color=rgba(85,85,85,0.5)>" + self.standardName + "</font>",
                           style=self.styles["Left"])
        else:
            p3 = Paragraph(" ", style=self.styles["Left"])

        quiz = self.formatText(quizName, default='OpenSans', formatString="size=14 color=#555555")
        p4 = Paragraph(quiz, style=self.styles["Left"])
        p4_list = self.splitParagraph(p4, 300, 16, orphan=True)

        if correctTotalTuple[1] > 0:
            p5 = Paragraph("<font face=OpenSans size=14 color=#7899b2>%d%% (%d/%d)</font>" % (int(activityScore),
                                                                                              correctTotalTuple[0],
                                                                                              correctTotalTuple[1]),
                           style=self.styles["Right"])
        else:
            p5 = Paragraph("", style=self.styles["Right"])

        p6 = Image(self.LOGO_IMAGE_PATH, height=33, width=119)

        roomName = (self.activity.room_name[:18] + '...') if len(self.activity.room_name) > 18 else self.activity.room_name
        p7 = Paragraph(self.formatText(roomName.upper(), formatString="size=14 color=#555555"),
                       style=self.styles["Left"])

        t = Table(
            data=[["", p1, p6, p2, ""], ["", p7, "", "", ""], ["", p3, "", "", ""], ["", p4_list[0], "", p5,""]],
            colWidths=(self.LEFT_MARGIN, 189, 126, 186, self.RIGHT_MARGIN),
            style=[
                ("VALIGN", (2, 0), (2, -1), "MIDDLE"),
                ("SPAN", (2, 0), (2, 1)),
                ("SPAN", (1,2), (2, 2)),
                ("LEFTPADDING", (1, 0), (1, -1), 1),
                ("RIGHTPADDING", (3, 0), (3, -1), 1)
            ]
        )

        return t

    def add_header(self, studentName, quizName, activityDate, activityScore, correctTotalTuple, pdfStory):
        """
        :param studentName:
        :param quizName;
        :param activityDate:
        :param activityScore:
        :param pdfStory:
        :param correctTotalTuple:
        """

        template = self.headerTemplate(studentName, quizName, activityDate, activityScore, correctTotalTuple)
        line = Table(
            data=[["", "", "", "", ""]],
            colWidths=(self.LEFT_MARGIN, 189, 126, 186, self.RIGHT_MARGIN),
            rowHeights=5,
            style=[("LINEBELOW", (1, 0), (-2, 0), 1, self.socrativeGray)]
        )
        pdfStory.append(template)
        pdfStory.append(line)

    def addQuestion(self, question, studentUUID, pdfStory, firstQ=False, imgURLDict=None):
        """
        :param question:
        :param studentUUID:
        :param pdfStory:
        :param firstQ:
        :param imgURLDict:
        """

        resp = [r for r in self.qResponses.get(question.question_id, []) if r.user_uuid == studentUUID]
        resp = resp[0] if resp else None
        hasCorrectAnswers = None
        for answer in self.answersDict.get(question.question_id, []):
            if answer.is_correct is True:
                hasCorrectAnswers = True
                break
        is_correct = (resp.is_correct if resp else None) or (False if hasCorrectAnswers else None)

        data = self.questionTemplate(question.order, question.question_text, is_correct)
        offset = len(data)

        template = None

        media = self.imagesDict.get(question.question_id)
        img = None
        if media:
            img = imgURLDict.get(media.url)

        if question.type in ["TF", "MC"]:
            for answer in sorted(self.answersDict.get(question.question_id, []), key=lambda x: x.order):
                chosen = True if resp and answer.id in self.selectionChoiceDict.get(resp.id, []) else False
                correct = resp.is_correct if chosen else None
                self.answerTemplate(data, answer.order, correct, answer.text, None, last=False,
                                    has_image=img is not None, chosen=chosen)
        else:
            qResp = [resp for resp in self.qResponses.get(question.question_id, []) if resp.user_uuid == studentUUID]
            for resp in qResp:
                texts = self.textResponseDict.get(resp.id, [""])
                txt = texts[0]
                if self.ESCAPED_HTML.match(txt) is None:
                    txt = htmlEscape(texts[0])
                self.frAnswerTemplate(data, txt, None, resp.is_correct, has_image=img is not None)
                break

        if question.type in ["MC", "TF"]:
            template = self.answerTemplate(data, None, None, None, img, last=True, offset=offset, first=firstQ)
        else:
            if data or img:
                template = self.frAnswerTemplate(data, None, img, last=True, offset=offset, first=firstQ)
        if template:
            pdfStory.append(template)

    @statsd.timed('socrative.reports.pdf.student_pdf.time')
    def getStudentsPdfs(self):
        """
        construct the student pdf reports
        """

        threads = []
        resultQueue = Queue()
        imgQueue = Queue()
        for image in self.imagesDict.values():
            data = ujson.loads(image.data) if image.data else None
            urlS = data.get("S")["url"] if data and "S" in data else None
            imgQueue.put((image.url, urlS))

        threadCount = len(self.imagesDict)
        threadCount = threadCount if threadCount < 10 else (threadCount + int(threadCount/10))
        for i in range(threadCount):
            threads.append(ImageRetrieverThread(imgQueue, resultQueue))

        for t in threads:
            t.start()

        for t in threads:
            t.join()

        resultDict = dict()

        while True:
            try:
                res = resultQueue.get(block=True, timeout=1)
                resultDict[res[0]] = res[1] or None
            except Empty:
                break

        resp = dict()

        total_gradable = 0
        for q in self.questions:
            if self.qCADict.get(q.question_id) > 0:
                total_gradable += 1*q.grading_weight
                for r in self.qResponses.get(q.question_id, []):
                    self.studentScoreDict[r.user_uuid] = self.studentScoreDict.get(r.user_uuid, 0) +\
                                                         (1*q.grading_weight if r.is_correct is True else 0)

        for k in self.studentScoreDict:
            correct = self.studentScoreDict[k]
            self.studentScoreDict[k] = round(float(self.studentScoreDict[k]) * 100.0 / float(total_gradable if total_gradable > 0 else 1))
            self.studentScoreDict[k] = (self.studentScoreDict[k], (correct, total_gradable))

        # Fix the duplicate student name issue:
        #    - identify the duplicate names and append a number at the end to make them unique
        duplicateNames = dict()
        for student in self.students:
            if student.name.upper() in duplicateNames:
                duplicateNames[student.name.upper()].append(student)
            else:
                duplicateNames[student.name.upper()] = [student]

        self.students = []
        for k in duplicateNames:
            if len(duplicateNames[k]) > 1:
                index = 0
                for st in duplicateNames[k]:
                    st.name = st.name.upper() + "_%d" % index
                    index += 1
                    self.students.append(st)
            else:
                self.students.append(duplicateNames[k][0])

        # End of fix for the duplicate name issue
        try:
            for student in self.students:

                story = []
                self.add_header(student.name, self.quiz.name, self.localTime, self.studentScoreDict[student.user_uuid][0],
                                self.studentScoreDict[student.user_uuid][1], story)

                i = 0
                for q in self.questions:
                    self.addQuestion(q, student.user_uuid, story, firstQ=(i == 0), imgURLDict=resultDict)
                    i += 1

                pdfStream = self.buildPdf("Student PDF Report", story, NumberedCanvas, student_name=student.name)

                pdfStream.seek(0, 0)

                resp[(student.user_uuid, student.name.upper())] = pdfStream
            return resp
        finally:
            for filePath in resultDict.values():
                if filePath.startswith("/tmp/") and os.path.exists(filePath):
                    os.remove(filePath)


class WholeClassPdfReport(BasePdfClass):
    """
    class to handle whole class pdf reports
    """

    def __init__(self, daoRegistry, responses, questions, quiz, students, localTime, activity, context):
        """

        :param daoRegistry:
        :param responses:
        :param questions:
        :param quiz:
        :param students:
        :param localTime:
        :param activity:
        :param context:
        :return:
        """

        super(WholeClassPdfReport, self).__init__()

        self.questions = questions
        self.responses = responses
        self.quiz = quiz
        self.context = context
        self.students = students
        self.activity = activity
        self.localTime = localTime
        self.studentScoreDict = dict((a.user_uuid, 0) for a in self.students)
        self.qResponses = dict()

        for r in self.responses:
            if r.question_id in self.qResponses:
                self.qResponses[r.question_id].append(r)
            else:
                self.qResponses[r.question_id] = [r]

        responseIds = [a.id for a in responses]

        questionIds = [q.question_id for q in questions]

        if context.qCADict is None:
            self.qCADict = daoRegistry.answerDao.getNbCorrectAnswersForQuestions(questionIds)
            context.qCADict = self.qCADict
        else:
            self.qCADict = context.qCADict

        if context.names is None:
            self.studentNameDict = daoRegistry.studentNameDao.getStudentNames(responseIds)
        else:
            self.studentNameDict = dict(((sn.user_uuid, sn.activity_instance_id), sn.name) for sn in context.names)

        self.imagesDict = daoRegistry.mediaResourceDao.getImages(questionIds, _type=MediaResourceModel.IMAGE)
        self.answersDict = daoRegistry.answerDao.loadAnswersForQuestions(questionIds)

        if context.textResponseDict is None:
            self.textResponseDict = daoRegistry.studentResponseTextAnswerDao.getTexts(responseIds)
            context.textResponseDict = self.textResponseDict
        else:
            self.textResponseDict = context.textResponseDict

        # get text from answer selections
        answerSelectionDict = daoRegistry.studentResponseAnswerSelectionDao.getTexts(responseIds)

        self.selectionChoiceDict = daoRegistry.studentResponseAnswerSelectionDao.getChoiceList(responseIds)

        self.textResponseDict.update(answerSelectionDict)

        self.standardName = daoRegistry.standardsDao.getStandardName(quiz.id)

    def headerTemplate(self, quizName, quizStatsDict, activityDate, questionCount):
        """
        :param quizName:
        :param quizStatsDict:
        :param activityDate:
        :param questionCount:
        """

        p2 = Paragraph("<font face=OpenSans size=11 color=#555555>" + activityDate.strftime("%m/%d/%Y") + "</font>",
                       style=self.styles["Right"])

        if self.standardName is not None:
            p3 = Paragraph("<font face=OpenSans size=11 color=rgba(85,85,85,0.5)>" + self.standardName + "</font>",
                           style=self.styles["Left"])
        else:
            p3 = Paragraph(" ", style=self.styles["Left"])

        quiz = self.formatText(quizName, default='OpenSans', formatString="size=14 color=#555555")
        p4 = Paragraph(quiz, style=self.styles["Left"])
        p4_list = self.splitParagraph(p4, 310, 16, orphan=True)

        text = self.formatText(ugettext("Total Questions:")+ "&nbsp;", formatString="size=14 color=#555555") +\
               "<font face=OpenSans size=14 color=#7899b2>%d</font>" % questionCount
        p5 = Paragraph(text,
                       style=self.styles["Right"])

        p6 = Image(self.LOGO_IMAGE_PATH, height=33, width=119)

        text = self.formatText(ugettext("Most Correct Answers:") + "&nbsp;"*3, formatString="size=10 color=#555555")
        p7 = Paragraph(text + "<font face=OpenSans size=12 color=#7899b2><b>#%d</b>" % quizStatsDict.get("qmc", 0) +
                       "</font>", style=self.styles["Left"])

        text = self.formatText(ugettext("Least Correct Answers:") + "&nbsp;"*3, formatString="size=10 color=#555555")
        p8 = Paragraph(text + "<font face=OpenSans size=12 color=#7899b2><b>#%d</b>" % quizStatsDict.get("qlc", 0) +
                       "</font>", style=self.styles["Right"])

        roomName = (self.activity.room_name[:18] + '...') if len(self.activity.room_name) > 18 else self.activity.room_name
        p9 = Paragraph(self.formatText(roomName.upper(), formatString="size=14 color=#555555"), style=self.styles["Left"])

        t = Table(
            [["", p9, p6, p2, ""], ["", p3, "", "", ""], ["", p4_list[0], "", p5, ""], ["", p7, "", p8, ""]],
            colWidths=(50, 189, 126, 186, 50),
            style=[
                ("VALIGN", (0, 0), (-1, 0), "TOP"),
                ("VALIGN", (2, 0), (2, -1), "MIDDLE"),
                ("SPAN", (2, 0), (2, 1)),
                ("SPAN", (1, 2), (2, 2)),
                ("LEFTPADDING", (1, 0), (1, -1), 1),
                ("RIGHTPADDING", (3, 0), (3, -1), 1),
                ("BOTTOMPADDING", (1, 2), (3, 2), 10),
                ("LINEBELOW", (1, 2), (3, 2), 1, self.socrativeGray),
                ("BOTTOMPADDING", (-1, 0), (-1, -1), 15),
                ("TOPPADDING", (-1, 0), (-1, -1), -9)
            ]
        )

        return t

    def addQuestion(self, question, pdfStory, imgURLDict=None):
        """
        :param question:
        :param pdfStory:
        :param imgURLDict:
        """

        hasCorrectAnswers = None
        for answer in self.answersDict.get(question.question_id, []):
            if answer.is_correct is True:
                hasCorrectAnswers = True
                break

        data = self.questionTemplate(question.order, question.question_text, has_correct_answers=hasCorrectAnswers)

        offset = len(data)
        template = None

        qResp = self.qResponses.get(question.question_id, [])

        media = self.imagesDict.get(question.question_id)
        img = None
        if media:
            img = imgURLDict.get(media.url)

        if question.type == "TF":
            for answer in sorted(self.answersDict.get(question.question_id, []), key=lambda x: x.order):
                answers = [True if answer.id in self.selectionChoiceDict.get(resp.id, []) else -1 for resp in qResp]
                count = len(filter(lambda y: y != -1, answers))
                if hasCorrectAnswers:
                    #  if the question had 4 answer and 1 has is_correct as true and the rest are None, they should be
                    #  False
                    correct = answer.is_correct or False
                else:
                    correct = None
                self.answerTemplate(data, len(self.students), count, correct, answer.order, answer.text,
                                    None, last=False, has_image=img is not None, has_correct_answers=hasCorrectAnswers)
        elif question.type == "MC":
            for answer in sorted(self.answersDict.get(question.question_id, []), key=lambda x: x.order):
                answers = [True if answer.id in self.selectionChoiceDict.get(resp.id, []) else -1 for resp in qResp]
                count = len(filter(lambda y: y != -1, answers))
                if hasCorrectAnswers:
                    correct = answer.is_correct or False
                else:
                    correct = None
                self.answerTemplate(data, len(self.students), count, correct, answer.order, answer.text,
                                    None, last=False, has_image=img is not None, has_correct_answers=hasCorrectAnswers)
        else:
            for resp in qResp:
                name = self.studentNameDict.get((resp.user_uuid, self.activity.id), " ")
                texts = self.textResponseDict.get(resp.id, [])
                newTxt = list()
                for t in texts:
                    if self.ESCAPED_HTML.match(t) is None:
                        newTxt.append(htmlEscape(t))
                        continue
                    newTxt.append(t)

                self.frAnswerTemplate(data, name, newTxt, resp.is_correct, None, last=False, hasImg=img is not None,
                                      has_correct_answers=hasCorrectAnswers)

        if question.type == "MC":
            template = self.answerTemplate(data, None, None, None, None, None, img, last=True, offset=offset)
        elif question.type == "TF":
            template = self.answerTemplate(data, None, None, None, None, None, img, last=True, offset=offset)
        else:
            if data or img:
                template = self.frAnswerTemplate(data, None, [], None, img, last=True, offset=offset)

        if template:
            pdfStory.append(template)

    def questionTemplate(self, order, text, has_correct_answers=None):
        """

        :param order:
        :param text:
        :return:
        """
        question = self.formatText(text, default="OpenSans", formatString="size=12 color=#555555")
        question = question.replace(" face=OpenSans", " face=OpenSansSB")
        p1 = Paragraph("<font face=OpenSansSB size=12 color=#555555>%d." % order + "&nbsp;"*4 + "</font>%s" % question,
                       style=self.styles["Left"])

        if has_correct_answers is None:
            p0 = Image(self.NO_GRADE_IMAGE_PATH, width=17, height=17, hAlign='RIGHT')
        else:
            p0 = ""

        paras = self.splitParagraph(p1, 500, 280)
        data = [["", p0, paras[0], ""]]
        for p in paras[1:]:
            data.append(["", "", p, ""])

        return data

    def answerTemplate(self, flowableList, total, correct, isCorrect, order, text, img, last=False, has_image=False,
                       has_correct_answers=None, offset=None):
        """
        :param flowableList:
        :param total:
        :param correct:
        :param isCorrect:
        :param order:
        :param text:
        :param img:
        :param last:
        :param has_image:
        :param has_correct_answers:
        :param offset: the number of flowables coresponding to the question
        """

        if last is True:
            if img:

                imgFlow = Image(img, height=140, width=140, kind="proportional")

                for row in flowableList[:offset]:
                    row += ["", "", ""]

                if len(flowableList) == offset:
                    flowableList.append(["", "", "", "", "", ""])
                flowableList[offset].insert(5, imgFlow)

                for i in range(offset + 1, len(flowableList)):
                    flowableList[i].append("")

                # compute how many rows do we span for the image
                span = -1
                pixels = 0
                maxHeight = imgFlow.drawHeight + 35
                i = -1
                for i in range(offset, len(flowableList)):
                    t = flowableList[i][4].getPlainText() if type(flowableList[i][4]) not in (str, unicode) else flowableList[i][4]
                    pixels += (int(len(t) / 72) + getRows(flowableList[i][4])) * 25
                    if pixels >= maxHeight:
                        break

                if pixels >= maxHeight:
                    span = i
                else:
                    diff = int((maxHeight - pixels) / 25)
                    for _ in range(diff):
                        flowableList.append(["", "", "", "", "", "", ""])

                styles = [("SPAN", (5, offset), (5, span))]

                for j in range(i + 1, len(flowableList)):
                    styles.append(("SPAN", (4, j), (5, j)))

                styles += [("SPAN", (2, it), (5, it)) for it in range(offset)]
                styles += [
                    ("SPAN", (0, offset), (1, -1)),
                    ("TOPPADDING", (0, offset), (-1, offset), 5),
                    ("TOPPADDING", (2, offset), (3, offset), 5),
                    ("VALIGN", (2, offset), (-1, -1), "TOP"),
                    ("LEFTPADDING", (2, offset), (3, -1), 0),
                    ("RIGHTPADDING", (2, offset), (3, -1), 0),
                    ("BOTTOMPADDING", (2, offset), (3, -1), 0),
                    ("LEFTPADDING", (2, offset), (2, -1), 0),
                    ("RIGHTPADDING", (2, offset), (2, -1), 0),
                    ("BOTTOMPADDING", (2, offset), (2, -1), 5),
                    ("BOTTOMPADDING", (3, offset), (3, -1), 9),
                    ("BOTTOMPADDING", (4, offset), (4, -1), 5),
                    ("LEFTPADDING", (5, offset), (5, -1), 0),
                ]

                styles += [
                    ("TOPPADDING", (0, 0), (-1, offset-1), 15),
                    ("LEFTPADDING", (1, 0), (1, offset-1), 0),
                    ("BOTTOMPADDING", (0, offset-1), (-1, offset-1), 5),
                    ("VALIGN", (1, 0), (1, 0), "TOP")
                ]

                if len(flowableList) > 1:
                    styles += [
                        # ("TOPPADDING", (0, 2), (-1, -1), 1),
                        ("TOPPADDING", (2, offset + 1), (3, -1), 5)
                    ]

                # check the length of the text in the second column
                maxLen = 0
                for row in range(offset, len(flowableList)):
                    l = flowableList[row][2]
                    if type(l) not in (str, unicode):
                        l = l.getPlainText()
                    sLen = int(len(l)*8)
                    if sLen > maxLen:
                        maxLen = sLen

                diff = 56 - maxLen-6
                maxLen += 6
                if diff < 0:
                    maxLen = 56
                    diff = 0

                question_height = computeHeight(flowableList)
                if question_height < self.MAX_HEIGHT:
                    styles.append(('NOSPLIT', (0, 0), (-1, -1)))

                if hasattr(imgFlow, "_img"):
                    imgFlow._img.fp.close()
                    del imgFlow._img

                t1 = Table(flowableList, splitByRow=1, colWidths=(31, 19, maxLen, 19, 285 + diff, 140, 50), style=styles)
            else:

                for row in flowableList[:offset]:
                    row += ["", ""]

                styles = []
                if offset > 0:
                    styles += [("SPAN", (2, it), (4, it)) for it in range(offset)]

                styles += [
                    ("TOPPADDING", (1, offset), (-1, offset), 5),
                    ("TOPPADDING", (2, offset), (3, offset), 5),
                    ("VALIGN", (2, offset), (-1, -1), "TOP"),
                    ("LEFTPADDING", (2, offset), (3, -1), 0),
                    ("RIGHTPADDING", (2, offset), (3, -1), 0),
                    ("BOTTOMPADDING", (2, offset), (3, -1), 0),
                    ("LEFTPADDING", (2, offset), (2, -1), 0),
                    ("RIGHTPADDING", (2, offset), (2, -1), 0),
                    ("BOTTOMPADDING", (2, offset), (2, -1), 5),
                    ("BOTTOMPADDING", (3, offset), (3, -1), 9),
                    ("BOTTOMPADDING", (4, offset), (4, -1), 5),
                    ("VALIGN", (1, 0), (1, 0), "TOP")
                ]

                if offset > 0:
                    styles += [
                        ("TOPPADDING", (0, 0), (-1, offset - 1), 15),
                        ("LEFTPADDING", (1, 0), (1, offset - 1), 0),
                        ("BOTTOMPADDING", (0, offset - 1), (-1, offset - 1), 5)
                    ]

                if len(flowableList) > offset:
                    styles += [
                        # ("TOPPADDING", (0, 2), (-1, -1), 1),
                        ("TOPPADDING", (1, offset), (2, -1), 5)
                    ]

                # check the length of the text in the second column
                maxLen = 0
                for row in range(offset, len(flowableList)):
                    l = flowableList[row][2]
                    if type(l) not in (str, unicode):
                        l = l.getPlainText()
                    sLen = int(len(l)*8)
                    if sLen > maxLen:
                        maxLen = sLen

                diff = 56 - maxLen - 6
                maxLen += 6
                if diff < 0:
                    maxLen = 56
                    diff = 0

                question_height = computeHeight(flowableList)
                if question_height < self.MAX_HEIGHT:
                    styles.append(('NOSPLIT', (0, 0), (-1, -1)))

                t1 = Table(flowableList, colWidths=(31, 19, maxLen, 19, 425 + diff, 50), style=styles)

            return t1

        color = "#6ea523" if isCorrect is True else "#e31212" if isCorrect is False else "#ffffff"
        textColor = "#ffffff" if isCorrect else "#555555"
        countColor = "#6ea523" if isCorrect is True else "#e31212" if isCorrect is False else "#000000"

        p1 = Paragraph("<font size=12 color=%s face=OpenSansSB>%d/%d</font>" % (countColor, correct, total),
                       style=self.styles["Left"])
        p2 = Circle("<font size=8 color=%s face=OpenSans> %s </font>""" % (textColor, getAlphaOrder(order)),
                    style=self.styles["Center"],
                    fill=(1 if isCorrect else 0),
                    color=(color if isCorrect else None))

        answer = self.formatText(text, "OpenSans", "size=10 color=#555555")
        p3 = Paragraph(answer, style=self.styles["Left"])
        paras = self.splitParagraph(p3, 285 if has_image else 425, height=(140 if has_image else None))
        flowableList.append(["", "", p1, p2, paras[0], ""])
        for p in paras[1:]:
            flowableList.append(["", "", "", "", p, ""])

    def frAnswerTemplate(self, flowableList, student_name, texts, isCorrect, img, last=False, hasImg=False,
                         has_correct_answers=None, offset=None):
        """
        :param flowableList:
        :param student_name:
        :param texts:
        :param img:
        :param last:
        :param hasImg:
        :param has_correct_answers:
        """

        if last is True:
            if img:
                imgFlow = Image(img, height=140, width=140, kind="proportional")

                for row in flowableList[:offset]:
                    row += ["", ""]

                if len(flowableList) == offset:
                    flowableList.append(["", "", "", "", "", ""])
                flowableList[offset][4] = imgFlow

                for i in range(offset+1, len(flowableList)):
                    flowableList[i].append("")

                # compute how many rows do we span for the image
                span = -1
                pixels = 0
                maxHeight = imgFlow.drawHeight + 35
                i = -1
                for i in range(offset, len(flowableList)):
                    t = flowableList[i][3].getPlainText() if type(flowableList[i][3]) is not str else flowableList[i][3]
                    pixels += (int(len(t) / 72) + getRows(flowableList[i][3])) * 25
                    if pixels >= maxHeight:
                        break

                if pixels >= maxHeight:
                    span = i
                else:
                    diff = int((maxHeight-pixels)/25)
                    for _ in range(diff):
                        flowableList.append(["", "", "", "", "", ""])

                styles = [ ("SPAN", (2, it), (4, it)) for it in range(offset)]

                for i in range(offset, len(flowableList)):
                    if flowableList[i][1] != " ":
                        styles += [
                            ("SPAN", (2, i), (3, i)),
                            ("TOPPADDING", (0, i), (-1, i), 10)
                        ]
                    else:
                        flowableList[i][0] = ""
                        styles += [
                            ("LEFTPADDING", (3, i), (3, i), 10)
                        ]

                styles += [
                    ("SPAN", (4, offset), (4, span)),
                    ("VALIGN", (4, offset), (4, offset), "TOP"),
                    ("BOTTOMPADDING", (2, offset), (2, -1), 5),
                    ("BOTTOMPADDING", (3, offset), (3, -1), 5),
                    ("LEFTPADDING", (4, offset), (4, -1), 0)
                ]

                styles += [
                    ("TOPPADDING", (0, 0), (-1, offset - 1), 15),
                    ("LEFTPADDING", (1, 0), (1, offset - 1), 5),
                    ("BOTTOMPADDING", (1, 0), (-1, offset - 1), 5),
                    ("VALIGN", (1, 0), (1, 0), "TOP")
                ]

                question_height = computeHeight(flowableList)
                if question_height < self.MAX_HEIGHT:
                    styles.append(('NOSPLIT', (0, 0), (-1, -1)))

                if hasattr(imgFlow, "_img"):
                    # _img is created when the drawHeight property is read and this has issues when calling deepcopy over
                    # an opened file handler. we need to close the file handler and delete the member to work ok at rendering
                    imgFlow._img.fp.close()
                    del imgFlow._img

                t1 = Table(flowableList, colWidths=(31, 19, 19, 341, 140, 50), style=styles)
            else:

                for row in flowableList[:offset]:
                    row.append("")

                styles = [("SPAN", (2, it), (3, it)) for it in range(offset)]
                styles.append(("LEFTPADDING", (1, 0), (1,0), 0))

                for i in range(offset, len(flowableList)):
                    if flowableList[i][1] != " ":
                        styles += [
                            ("SPAN", (2, i), (3, i)),
                            ("TOPPADDING", (0, i), (-1, i), 10)
                        ]
                    else:
                        flowableList[i][1] = ""
                        styles += [
                            ("LEFTPADDING", (3, i), (3, i), 10)
                        ]

                    styles += [
                        ("BOTTOMPADDING", (2, offset), (2, -1), 5),
                        ("BOTTOMPADDING", (3, offset), (3, -1), 5)
                    ]

                styles += [
                    ("TOPPADDING", (1, 0), (-1, offset - 1), 15),
                    ("LEFTPADDING", (2, 0), (2, offset - 1), 5),
                    ("BOTTOMPADDING", (1, offset - 1), (-1, offset - 1), 5),
                    ("VALIGN", (2, 0), (3, 0), "TOP")
                ]

                # the question text is in column 2, the answer texts are in column 3
                question_height = computeHeight(flowableList)
                if question_height < self.MAX_HEIGHT:
                    styles.append(('NOSPLIT', (0, 0), (-1, -1)))

                t1 = Table(flowableList, colWidths=(31, 19, 17, 483, 50), style=styles)

            return t1

        mark = ""
        if isCorrect is True:
            mark = Image(self.CORRECT_IMAGE_PATH, height=12, width=17, hAlign='RIGHT')
        elif isCorrect is False:
            mark = Image(self.INCORRECT_IMAGE_PATH, height=12, width=11, hAlign='RIGHT')

        student = self.formatText(student_name, default="OpenSans",
                                  formatString="size=10 color=#555555")
        student = student.replace(" face=OpenSans", " face=OpenSansSB")
        p2 = Paragraph(student, style=self.styles["Left"])
        flowableList.append(["", "", p2, "", ""])
        for text in texts:
            formattedText = self.formatText(text, "OpenSans", "size=10 color=#555555")
            p = Paragraph(formattedText, style=self.styles["Left"])
            paras = self.splitParagraph(p, 343 if hasImg else 483, height=(140 if hasImg else None))
            i = 0
            for p in paras:
                if i == 0:
                    flowableList.append(["", " ", mark, p, ""])
                    i = 1
                else:
                    flowableList.append(["", " ", "", p, ""])

    def add_header(self, quizName, activityDate, quizStatsDict, questionCount, pdfStory):
        """
        :param quizName:
        :param activityDate:
        :param quizStatsDict:
        :param questionCount:
        :param pdfStory:
        """

        template = self.headerTemplate(quizName, quizStatsDict, activityDate, questionCount)

        pdfStory.append(template)

    @statsd.timed('socrative.reports.pdf.whole_class_pdf.time')
    def getQuestionPdf(self):
        """
        creates the whole class pdf report
        :return cString with pdf binary content
        """

        threads = []
        resultQueue = Queue()
        imgQueue = Queue()
        for image in self.imagesDict.values():
            data = ujson.loads(image.data) if image.data else None
            urlS = data.get("S")["url"] if data and "S" in data else None
            imgQueue.put((image.url, urlS))

        threadCount = len(self.imagesDict)
        threadCount = threadCount if threadCount < 10 else (threadCount + int(threadCount/10))
        for i in range(threadCount):
            threads.append(ImageRetrieverThread(imgQueue, resultQueue))

        for t in threads:
            t.start()

        for t in threads:
            t.join()

        resultDict = dict()

        while True:
            try:
                res = resultQueue.get(block=True, timeout=1)
                resultDict[res[0]] = res[1] or None
            except Empty:
                break

        question_num_correct = dict()
        quiz_stats = dict()
        for question in self.questions:
            if self.qCADict.get(question.question_id) > 0:
                num_correct = len(filter(lambda x: x.question_id == question.question_id and x.is_correct, self.responses))
                question_num_correct[question] = num_correct

        if question_num_correct:
            quiz_stats["qmc"] = max(question_num_correct.keys(), key=lambda x: question_num_correct[x]).order
            quiz_stats["qlc"] = min(question_num_correct.keys(), key=lambda x: question_num_correct[x]).order
        else:
            quiz_stats["qmc"] = 0
            quiz_stats["qlc"] = 0


        story = []
        self.add_header(self.quiz.name, self.localTime, quiz_stats, len(self.questions), story)

        for q in self.questions:
            self.addQuestion(q, story, imgURLDict=resultDict)

        try:
            pdfStream = self.buildPdf("Whole Class Report", story, NumberedCanvas)

            pdfStream.seek(0, 0)

            return pdfStream
        finally:
            for filePath in resultDict.values():
                if filePath.startswith("/tmp/") and os.path.exists(filePath):
                    os.remove(filePath)


class QuizPdfReport(BasePdfClass):
    """
    class to handle whole class pdf reports
    """
    MARGIN_LEFT = 50
    MARGIN_RIGHT = 50

    def __init__(self, daoRegistry, questions, quiz, context):
        """

        :param daoRegistry:
        :param questions:
        :param quiz:
        :param context:
        :return:
        """

        super(QuizPdfReport, self).__init__()

        self.questions = questions
        self.quiz = quiz
        self.context = context

        questionIds = [q.question_id for q in questions]

        self.imagesDict = daoRegistry.mediaResourceDao.getImages(questionIds, _type=MediaResourceModel.IMAGE)
        self.answersDict = daoRegistry.answerDao.loadAnswersForQuestions(questionIds)
        self.standardName = daoRegistry.standardsDao.getStandardName(quiz.id)

    def headerTemplate(self, quizName):
        """
        :param quizName:
        """

        if self.standardName is not None:
            p1 = Paragraph("<font face=OpenSans size=11 color=rgba(85,85,85,0.5)>" + self.standardName + "</font>",
                           style=self.styles["Left"])
        else:
            p1 = Paragraph(" ", style=self.styles["Left"])

        quiz = self.formatText(quizName, default='OpenSans', formatString="size=14 color=#555555")

        p2 = Paragraph(quiz, style=self.styles["Left"])
        p2_list = self.splitParagraph(p2, 382, 16, orphan=True)

        p3 = Image(self.LOGO_IMAGE_PATH, height=33, width=119)

        p4 = Paragraph(self.formatText(ugettext("Score:"), formatString="size=14 color=#555555"),
                       style=self.styles["Left"])

        t = Table(
            [["", "", p3, "", "", ""],
             ["", p1, "", "", "", ""],
             ["", p2_list[0], "", p4, "", ""],
             ["", "", "", "", "", ""]],
            colWidths=(self.MARGIN_LEFT, 128, 264, 68, 60, self.MARGIN_RIGHT),
            style=[
                ("VALIGN", (2, 0), (2, -1), "MIDDLE"),
                ("SPAN", (2, 0), (2, 1)),
                ("SPAN", (3, 0), (4, 0)),
                ("SPAN", (3, 1), (4, 1)),
                ("SPAN", (1, 2), (2, 2)),
                ("LEFTPADDING", (1, 0), (1, -1), 1),
                ("RIGHTPADDING", (3, 0), (3, -1), 1),
                ("LINEBELOW", (1, 0), (2, 0), 2, self.socrativeGray),
                ("LINEBELOW", (3, 0), (4, 0), 2, self.socrativeGray),
                ("LINEBELOW", (4, 2), (4, 2), 2, self.socrativeGray),
                ("LINEBELOW", (1, 3), (4, 3), 1, self.socrativeGray),
                ("ALIGN", (2, 0), (2, 1), "CENTER"),
                ("BOTTOMPADDING", (0, -1), (-1, -1), -10)
            ]
        )

        return t

    def add_header(self, quizName, pdfStory):
        """
        :param quizName;
        :param pdfStory:
        """

        template = self.headerTemplate(quizName)
        pdfStory.append(template)

    def addQuestion(self, question, pdfStory, imgURLDict=None):
        """
        :param question:
        :param pdfStory:
        :param imgURLDict:
        """

        data = self.questionTemplate(question.order, question.question_text)

        media = self.imagesDict.get(question.question_id)
        img = None
        if media:
            img = imgURLDict.get(media.url)

        offset = len(data)

        if question.type in ["TF", "MC"]:
            for answer in sorted(self.answersDict.get(question.question_id, []), key=lambda x: x.order):
                self.answerTemplate(data, answer.order, answer.text, None, last=False,
                                    has_image=img is not None)
        else:
            self.frAnswerTemplate(data, None, last=False, has_image=img is not None)

        if question.type in ["TF", "MC"]:
            template = self.answerTemplate(data, None, None, img, last=True, offset=offset)
        else:
            template = self.frAnswerTemplate(data, img, last=True, offset=offset)
        pdfStory.append(template)

    def questionTemplate(self, order, text):
        """

        :param order:
        :param text:
        :return:
        """
        question = self.formatText(text, default="OpenSans", formatString="size=12 color=#555555")
        question = question.replace(" face=OpenSans", " face=OpenSansSB")
        p1 = Paragraph("<font face=OpenSansSB size=12 color=#555555>%d." % order + "&nbsp;" * 4 + "</font>%s" % question,
                       style=self.styles["Left"])

        paras = self.splitParagraph(p1, 500)
        data = [["", paras[0], ""]]
        for p in paras[1:]:
            data.append(["", p, ""])

        return data

    def answerTemplate(self, flowableList, order, text, img, last=False, has_image=False, offset=None):
        """
        """

        if last is True:
            if img:
                imgFlow = Image(img, height=140, width=140, kind="proportional")

                if not flowableList:
                    flowableList.append(["", "", "", ""])
                else:
                    for row in flowableList[0:offset]:
                        row += ["", ""]

                flowableList[offset].insert(3, imgFlow)

                for i in range(offset+1, len(flowableList)):
                    flowableList[i].append("")

                # compute how many rows do we span for the image
                span = -1
                pixels = 0
                maxHeight = imgFlow.drawHeight + 35
                i = -1
                for i in range(offset, len(flowableList)):
                    t = flowableList[i][2].getPlainText() if type(flowableList[i][2]) not in (str, unicode) else \
                    flowableList[i][2]
                    pixels += (int(len(t) / 85) + getRows(flowableList[i][2])) * 25
                    if pixels >= maxHeight:
                        break

                if pixels >= maxHeight:
                    span = i
                else:
                    diff = int((maxHeight - pixels) / 25)
                    for _ in range(diff):
                        flowableList.append(["", "", "", "", ""])

                styles = [("SPAN", (3, offset), (3, span))]

                for j in range(i + 1, len(flowableList)):
                    styles.append(("SPAN", (2, j), (3, j)))

                styles += [("SPAN", (1, it), (3, it)) for it in range(offset)]
                styles += [
                    ("TOPPADDING", (0, offset), (-1, offset), 5),
                    ("TOPPADDING", (1, offset), (1, offset), 6),
                    ("VALIGN", (1, offset), (-1, -1), "TOP"),
                    ("LEFTPADDING", (1, offset), (1, -1), 0),
                    ("RIGHTPADDING", (1, offset), (1, -1), 0),
                    ("BOTTOMPADDING", (1, offset), (1, -1), 5),
                    ("BOTTOMPADDING", (2, offset), (2, -1), 5),
                    ("LEFTPADDING", (3, offset), (3, -1), 0),
                    ("TOPPADDING", (0, 0), (-1, 0), 10),
                ]

                if len(flowableList) > 1:
                    styles += [
                        # ("TOPPADDING", (0, 2), (-1, -1), 1),
                        ("TOPPADDING", (1, offset + 1), (1, -1), 5)
                    ]

                question_height = computeHeight(flowableList)
                if question_height < self.MAX_HEIGHT:
                    styles.append(('NOSPLIT', (0, 0), (-1, -1)))

                if hasattr(imgFlow, "_img"):
                    # _img is created when the drawHeight property is read and this has issues when calling deepcopy over
                    # an opened file handler. we need to close the file handler and delete the member to work ok at rendering
                    imgFlow._img.fp.close()
                    del imgFlow._img

                t1 = Table(flowableList, colWidths=(self.MARGIN_LEFT, 19, 340, 140, self.MARGIN_RIGHT), style=styles)
            else:

                for row in flowableList[0:offset]:
                    row.append("")

                styles = [("SPAN", (1, it), (2, it)) for it in range(offset)]
                styles += [
                    ("TOPPADDING", (1, offset), (-1, offset), 5),
                    ("TOPPADDING", (1, offset), (1, offset), 6),
                    ("VALIGN", (1, offset), (-1, -1), "TOP"),
                    ("LEFTPADDING", (1, offset), (1, -1), 0),
                    ("RIGHTPADDING", (1, offset), (1, -1), 0),
                    ("BOTTOMPADDING", (1, offset), (1, -1), 0),
                    ("TOPPADDING", (0, 0),(-1, 0), 10),
                ]
                if len(flowableList) > offset + 1:
                    styles += [
                        # ("TOPPADDING", (0, 2), (-1, -1), 1),
                        ("TOPPADDING", (1, offset+1), (1, -1), 5)
                    ]

                styles += [
                    ("BOTTOMPADDING", (1, offset), (1, -1), 5),
                    ("BOTTOMPADDING", (2, offset), (2, -1), 5)
                ]

                question_height = computeHeight(flowableList)
                if question_height < self.MAX_HEIGHT:
                    styles.append(('NOSPLIT', (0, 0), (-1, -1)))

                t1 = Table(flowableList, colWidths=(self.MARGIN_LEFT, 19, 480, self.MARGIN_RIGHT),
                           style=styles)

            return t1

        p1 = Circle("<font size=8 color=#555555 face=OpenSans> %s </font>""" % getAlphaOrder(order),
                    style=self.styles["Center"], fill=0)

        answer = self.formatText(text, "OpenSans", "size=10 color=#555555")
        p2 = Paragraph(answer, style=self.styles["Left"])
        paras = self.splitParagraph(p2, 335 if has_image else 475, height=(140 if has_image else None))

        flowableList.append(["", p1, paras[0], ""])
        for p in paras[1:]:
            flowableList.append(["", "", p, ""])

    def frAnswerTemplate(self, flowableList, img, last=False, has_image=False, offset=None):
        """
        :param flowableList:
        :param img:
        :param last:
        :param has_image:
        """

        if last is True:
            if img:
                imgFlow = Image(img, height=140, width=140, kind="proportional")

                flowableList.insert(offset, ["", "", ""])
                offset += 1
                flowableList[offset][2] = imgFlow

                for row in flowableList[0:offset]:
                    row.append("")

                span = -1
                pixels = 0
                maxHeight = imgFlow.drawHeight + 9
                i = -1
                for i in range(offset, len(flowableList)):
                    pixels += 25
                    if pixels >= maxHeight:
                        break

                if pixels >= maxHeight:
                    span = i
                else:
                    diff = int((maxHeight - pixels) / 25)
                    for _ in range(diff):
                        flowableList.append(["", "", "", ""])

                styles = [("SPAN", (2, offset), (2, span))]

                for j in range(i + 1, len(flowableList)):
                    styles += [
                        ("SPAN", (1, j), (2, j)),
                        ("LINEBELOW", (2, j), (2, j), 1, HexColor("#555555"))
                    ]

                styles += [("SPAN", (1, it), (2, it)) for it in range(offset)]
                styles += [
                    ("TOPPADDING", (1, offset), (1, -1), 9),
                    ("TOPPADDING", (2, offset), (2, offset), 9),
                    ("LEFTPADDING", (2, offset), (2, offset), 4),
                    ("VALIGN", (3, offset), (3, offset), "TOP"),
                    ('LINEBELOW', (1, offset), (1, -1), 1, HexColor("#555555")),
                ]

                if computeHeight(flowableList) < self.MAX_HEIGHT:
                    styles.append(('NOSPLIT', (0, 0), (-1, -1)))

                if hasattr(imgFlow, "_img"):
                    # _img is created when the drawHeight property is read and this has issues when calling deepcopy over
                    # an opened file handler. we need to close the file handler and delete the member to work ok at rendering
                    imgFlow._img.fp.close()
                    del imgFlow._img

                t1 = Table(flowableList, colWidths=(self.MARGIN_LEFT, 360, 144, self.MARGIN_RIGHT), style=styles)
            else:

                flowableList.insert(offset, ["", "", ""])
                offset += 1

                styles = [("SPAN", (1, it), (2, it)) for it in range(offset)]
                styles += [
                    ("TOPPADDING", (1, offset), (1, -1), 9),
                    ('LINEBELOW', (1, offset), (1, -1), 1, HexColor("#555555")),
                ]

                if computeHeight(flowableList) < self.MAX_HEIGHT:
                    styles.append(('NOSPLIT', (0, 0), (-1, -1)))

                t1 = Table(flowableList, colWidths=(self.MARGIN_LEFT, 500, self.MARGIN_RIGHT), style=styles)

            return t1

        if has_image:
            rows = 5
            cols = 4
        else:
            rows = 3
            cols = 3
        for i in range(0, rows):
            flowableList.append(["" for _ in range(cols)])

    @statsd.timed('socrative.reports.pdf.download_quiz.time')
    def getQuizPdf(self):
        """
        :return:
        """
        threads = []
        resultQueue = Queue()
        imgQueue = Queue()
        for image in self.imagesDict.values():
            data = ujson.loads(image.data) if image.data else None
            urlS = data.get("S")["url"] if data and "S" in data else None
            imgQueue.put((image.url, urlS))

        threadCount = len(self.imagesDict)
        threadCount = threadCount if threadCount < 10 else (threadCount + int(threadCount/10))
        for i in range(threadCount):
            threads.append(ImageRetrieverThread(imgQueue, resultQueue))

        for t in threads:
            t.start()

        for t in threads:
            t.join()

        resultDict = dict()

        while True:
            try:
                res = resultQueue.get(block=True, timeout=1)
                resultDict[res[0]] = res[1] or None
            except Empty:
                break

        story = []
        self.add_header(self.quiz.name, story)

        for q in self.questions:
            self.addQuestion(q, story, imgURLDict=resultDict)

        try:
            pdfStream = self.buildPdf("Blank Quiz", story, NumberedCanvas)

            pdfStream.seek(0, 0)

            return pdfStream
        finally:
            for filePath in resultDict.values():
                if filePath.startswith("/tmp/") and os.path.exists(filePath): # do not delete image files outside of the /tmp/ folder
                    os.remove(filePath)


class ReceiptPdf(BasePdfClass):
    """
    class to handle whole class pdf reports
    """

    def __init__(self, context):
        """

        :param daoRegistry:
        :param questions:
        :param quiz:
        :param context:
        :return:
        """

        super(ReceiptPdf, self).__init__()

        self.email = context.get("email")
        self.payment_id = context.get("payment_id")
        self.payment_price = "$" + str(context.get("price")*1.0/100)
        self.payment_type = context.get("type")
        self.last4_digits = context.get("last4")
        self.expiration_date = context.get("expiration_date")
        self.name = context.get("name") or ""
        self.organization = context.get("organization") or ""
        self.years = context.get("years")
        self.license_key = context.get("license_key")


    def headerTemplate(self):
        """
        """

        p1 = Image(self.LOGO_IMAGE_PATH, height=21, width=80)

        p2 = Paragraph(self.formatText(ugettext("Socrative <b>PRO</b> Receipt"), formatString="size=17 color=#333333"),
                       style=self.styles["Left"])
        p3 = Image(self.SOCRATIVE_PRO_IMAGE_PATH, height=111, width=106)

        p4 = Paragraph(self.formatText(ugettext("<b>Thank you for going PRO!</b>"), formatString="size=10 color=#333333"),
                       style=self.styles["Center"])

        p5 = Paragraph(self.formatText(ugettext("Here is a receipt for your records."),
                                       formatString="size=10 color=#333333"),
                       style=self.styles["Center"])
        p6 = Paragraph(self.formatText(ugettext("<b>Socrative, Inc</b>"), formatString="size=9 color=#333333"),
                       style=self.styles["Center"])
        p7 = Paragraph(self.formatText(ugettext("175 West 200 South, Suite 1000"), formatString="size=9 color=#333333"),
                       style=self.styles["Center"])
        p8 = Paragraph(self.formatText(ugettext("Salt Lake City, Utah 84101"), formatString="size=9 color=#333333"),
                       style=self.styles["Center"])
        p9 = Paragraph(self.formatText(ugettext("USA"), formatString="size=9 color=#333333"),
                       style=self.styles["Center"])

        t = Table(
            [["", "", "", "", "", "", ""],
             ["", "", "", "", "", p1, ""],
             ["", "", "", "", "", "", ""],
             ["", "", p2, "", "", "", ""],
             ["", "", "", "", "", "", ""],
             ["", "", "", "", p3, "", ""],
             ["", "", "", "", "", "", ""],
             ["", "", "", p4, "", "", ""],
             ["", "", "", p5, "", "", ""],
             ["", "", "", "", "", "", ""],
             ["", "", "", "", "", "", ""],
             ["", "", "", p6, "", "", ""],
             ["", "", "", p7, "", "", ""],
             ["", "", "", p8, "", "", ""],
             ["", "", "", p9, "", "", ""]],
            colWidths=(31, 68, 7, 35, 20, 201, 31),
            rowHeights=(15, 21, 21, 17, 21, 111, 26, 13, 13, 15, 18, 14, 14, 14, 14),
            style=[
                ("SPAN", (2, 3), (5, 3)),
                ("VALIGN", (2,3), (5,3), "TOP"),
                ("SPAN", (4, 5), (5, 5)),
                ("SPAN", (3, 7), (5, 7)),
                ("SPAN", (3, 8), (5, 8)),
                ("SPAN", (3, 11), (5,11)),
                ("SPAN", (3, 12), (5, 12)),
                ("SPAN", (3, 13), (5, 13)),
                ("SPAN", (3, 14), (5, 14)),
                ("VALIGN", (3,7), (5,7), "TOP"),
                ("VALIGN", (3, 8), (5, 8), "TOP"),
                ("VALIGN", (3, 11), (5, 11), "TOP"),
                ("VALIGN", (3, 12), (5, 12), "TOP"),
                ("VALIGN", (3, 13), (5, 13), "TOP"),
                ("VALIGN", (3, 14), (5, 14), "TOP"),
                ("LEFTPADDING", (0, 0), (-1, -1), 0),
                ("TOPPADDING", (0, 0), (-1, -1), 0),
                ("BOTTOMPADDING", (0, 0), (-1, -1), 0),
                ("RIGHTPADDING", (0, 0), (-1, -1), 0),
                ("RIGHTPADDING", (3, 8), (5, 8), 75),
                ("RIGHTPADDING", (3, 7), (5, 7), 75),
                ("RIGHTPADDING", (3, 11), (5, 11), 100),
                ("RIGHTPADDING", (3, 12), (5, 12), 100),
                ("RIGHTPADDING", (3, 13), (5, 13), 100),
                ("RIGHTPADDING", (3, 14), (5, 14), 100),
                ("LEFTPADDING", (3, 11), (5, 11), 19),
                ("LEFTPADDING", (3, 12), (5, 12), 19),
                ("LEFTPADDING", (3, 13), (5, 13), 19),
                ("LEFTPADDING", (3, 14), (5, 14), 19),
                ("LINEABOVE", (1, 10), (5, 10), 1, HexColor("0xf0f0f0")),
            ]
        )

        return t

    def add_header(self, pdfStory):
        """
        :param pdfStory:
        """

        template = self.headerTemplate()
        pdfStory.append(template)

    def receiptTemplate(self):
        """

        :return:
        """
        p1 = Paragraph(self.formatText(ugettext("<b>Email</b>"), formatString="size=11 color=#333333"),
                       style=self.styles["Left"])
        p2 = Paragraph(self.formatText(self.email, formatString="size=11 color=#333333"),
                       style=self.styles["Right"])
        p3 = Paragraph(self.formatText(ugettext("<b>Amount</b>"), formatString="size=11 color=#333333"),
                       style=self.styles["Left"])
        p4 = Paragraph(self.formatText(self.payment_price, formatString="size=11 color=#333333"),
                       style=self.styles["Right"])
        p5 = Paragraph(self.formatText(ugettext("<b>Order Number</b>"), formatString="size=11 color=#333333"),
                       style=self.styles["Left"])
        p6 = Paragraph(self.formatText("SOC" + str(self.payment_id), formatString="size=11 color=#333333"),
                       style=self.styles["Right"])
        p7 = Paragraph(self.formatText(ugettext("<b>Payment Method</b>"), formatString="size=11 color=#333333"),
                       style=self.styles["Left"])
        p8 = Paragraph(
            self.formatText(
                "%s ( **** %s )" % (self.payment_type, self.last4_digits),
                formatString="size=11 color=#333333"),
            style=self.styles["Right"]
        )
        p9 = Paragraph(self.formatText(ugettext("<b>Expires"), formatString="size=11 color=#333333"),
                       style=self.styles["Left"])
        p10 = Paragraph(self.formatText(self.expiration_date.strftime("%B %d, %Y"), formatString="size=11 color=#333333"),
                        style=self.styles["Right"])
        p11 = Paragraph(self.formatText(ugettext("<b>Name</b>"), formatString="size=11 color=#333333"),
                       style=self.styles["Left"])
        p12 = Paragraph(self.formatText(self.name, formatString="size=11 color=#333333"),
                       style=self.styles["Right"])
        p13 = Paragraph(self.formatText(ugettext("<b>Organization</b>"), formatString="size=11 color=#333333"),
                        style=self.styles["Left"])
        p14 = Paragraph(self.formatText(self.organization, formatString="size=11 color=#333333"),
                        style=self.styles["Right"])
        p15 = Paragraph(self.formatText(ugettext("<b>Subscription Length</b>"), formatString="size=11 color=#333333"),
                        style=self.styles["Left"])
        p16 = Paragraph(self.formatText("1 year" if self.years == 1 else "%d years" % self.years,
                                        formatString="size=11 color=#333333"),
                        style=self.styles["Right"])
        p17 = Paragraph(self.formatText(ugettext("<b>License Key</b>"), formatString="size=11 color=#333333"),
                        style=self.styles["Left"])
        p18 = Paragraph(self.formatText(self.license_key.upper(),
                                        formatString="size=11 color=#333333"),
                        style=self.styles["Right"])
        p19 = Paragraph(self.formatText("<b>%s</b>" % ugettext("Washington State Sales"), formatString="size=9 color=#333333"),
                        style=self.styles["Left"])
        p20 = Paragraph(self.formatText(ugettext("MasteryConnect is not required to, and does not, collect Washington \
            sales or use tax. Under Washington law, purchases are not tax-exempt merely because a seller is not required \
            to collect Washington's tax. Washington law requires Washington purchasers to review untaxed purchases and, \
            if any tax is owed, file a Washington use tax return and pay any tax due. Visit https://dor.wa.gov/consumerusetax \
            for more information."), formatString="size=8 color=#333333"),
                        style=self.styles["Left"])

        t = Table([
            ["", "", "", ""],
            ["", p11, p12, ""],
            ["", p13, p14, ""],
            ["", p1, p2, ""],
            ["", p3, p4, ""],
            ["", p5, p6, ""],
            ["", p7, p8, ""],
            ["", p17, p18, ""],
            ["", p15, p16, ""],
            ["", p9, p10, ""],
            ["", "", "", ""],
            ["", p19, "", ""],
            ["", p20, "", ""],
            ["", "", "", ""]
        ],
            colWidths=(31, 130, 201, 31),
            rowHeights=(15, 20, 20, 20, 20, 20, 20, 20, 20, 20, 10, 50, 58, 15),
            style=[
                ("LEFTPADDING", (1, 0), (1, -1), 0),
                ("LINEBELOW", (1, -4), (2, -4), 1, HexColor("0xf0f0f0")),
                ("SPAN", (1, 11), (2, 11)),
                ("SPAN", (1, 12), (2, 12)),
            ])

        return t

    def add_receipt_data(self, story):
        """

        :param story:
        :return:
        """
        template = self.receiptTemplate()
        story.append(template)

    def addFooterTemplate(self):
        """

        :return:
        """

        p1 = Paragraph(self.formatText(ugettext("Copyright &copy; 2017 MasteryConnect, Inc. All Rights Reserved"),
                                       formatString="size=8 color=#333333"),
                       style=self.styles["Center"])
        p2 = Paragraph(
            self.formatText(
                ugettext("Socrative, Inc. 175 West 200 South, Suite 1000, Salt Lake City, Utah 84101, USA"),
                formatString="size=8 color=#333333"
            ),
            style=self.styles["Center"])

        t = Table(
            [
                ["", "", ""],
                ["", p1, ""],
                ["", p2, ""],
                ["", "", ""]
            ],
            colWidths=(31, 361, 31),
            rowHeights=(14, 12, 12, 16),
            style=[("LEFTPADDING", (0, 0), (-1, -1), 0), ("RIGHTPADDING", (1, 1), (1, 2), 30)]
        )
        return t

    def add_footer(self):
        """

        :param story:
        :return:
        """
        return self.addFooterTemplate()

    def drawBackground(self, canv, doc):
        """
        
        :param canv: 
        :param doc: 
        :return: 
        """

        canv.saveState()
        color = canv._fillColorObj
        canv.setFillColor("#789ab3")
        canv.rect(0, 0, canv._pagesize[0], 8, fill=1)
        canv.setFillColor(color)
        canv.restoreState()

    @statsd.timed('receipt.pdf.time')
    def getReceiptPdf(self):
        """
        :return:
        """

        ioString = StringIO()

        doc = SimpleDocTemplate(ioString, rightMargin=0, leftMargin=0, topMargin=0,
                                bottomMargin=0, pagesize=letter)
        story = []
        self.add_header(story)

        self.add_receipt_data(story)

        footer = self.add_footer()

        doc.author = "socrative.com"
        doc.title = "Receipt"
        doc.creator = "socrative.com"

        t = Table(data=[["", "", ""],
                        ["", story, ""],
                        ["", "", ""],
                        ["", "", ""],
                        ["", footer, ""]],
                  colWidths=(121, 393, 98), rowHeights=(8, 672, 29, 3, 54),
                  style=[("BACKGROUND", (0, 0), (2, 0), HexColor("0x789ab3")),
                         # ("BACKGROUND", (0, 1), (0, 2), HexColor("0xffffff")),
                         ("TOPPADDING", (0, 0), (-1, -1), 0),
                         ("LEFTPADDING", (0, 0), (-1, -1), 0),
                         ("RIGHTPADDING", (0, 0), (-1, -1), 0),
                         ("BOTTOMPADDING", (0, 0), (-1, -1), 0),
                         ("TOPPADDING", (0, 0), (2, 0), 10),
                         ("BACKGROUND", (1, 3), (1, 3), HexColor("0x789ab3"))])

        doc.build([t])
        ioString.seek(0, 0)

        return ioString
