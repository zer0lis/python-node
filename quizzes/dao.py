# -*- coding: utf-8 -*-

import datetime
import logging
import time
from collections import OrderedDict

import pytz
from django.utils.translation import ugettext
from psycopg2 import OperationalError

from common.base_dao import BaseDao, retry
from common.base_dao import BaseModel
from common.dao import MediaResourceModel
from common.socrative_api import exceptionStack
from common.socrative_api import safeToInt, safeToBool
from common.counters import DatadogThreadStats


logger = logging.getLogger(__name__)
statsd = DatadogThreadStats.STATS

class StandardModel(BaseModel):
    """
    class that holds the model for socrative standards
    """

    ID = "id"
    QUIZ_ID = "quiz_id"
    SUBJECT_ID = "subject_id"
    CORE_ID = "core_id"
    GRADE_ID = "grade_id"
    STANDARD_ID = "standard_id"
    DESCRIPTION = "description"
    NAME = "name"

    FIELD_COUNT = 8

    def __init__(self):
        """
        Constructor
        :return:
        """

        self.id = None
        self.quiz_id = None
        self.subject_id = None
        self.core_id = None
        self.grade_id = None
        self.standard_id = None
        self.description = None
        self.name = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.QUIZ_ID] = self.quiz_id
        obj[self.SUBJECT_ID] = self.subject_id
        obj[self.CORE_ID] = self.core_id
        obj[self.GRADE_ID] = self.grade_id
        obj[self.STANDARD_ID] = self.standard_id
        obj[self.DESCRIPTION] = self.description
        obj[self.NAME] = self.name
        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        returns a model from
        :param dictObj
        """

        model = StandardModel()
        model.id = dictObj.get(cls.ID)
        model.quiz_id = dictObj.get(cls.QUIZ_ID)
        model.subject_id = dictObj.get(cls.SUBJECT_ID)
        model.core_id = dictObj.get(cls.CORE_ID)
        model.grade_id = dictObj.get(cls.GRADE_ID)
        model.standard_id = dictObj.get(cls.STANDARD_ID)
        model.description = dictObj.get(cls.DESCRIPTION)
        model.name = dictObj.get(cls.NAME)

        return model

    @classmethod
    def fromRequestDto(cls, dtoModel):
        """
        dto to dao model method
        :param dtoModel:
        """

        model = StandardModel()
        model.id = dtoModel.id
        model.quiz_id = dtoModel.quiz_id
        model.subject_id = dtoModel.subject_id
        model.core_id = dtoModel.core_id
        model.grade_id = dtoModel.grade_id
        model.standard_id = dtoModel.standard_id
        model.name = dtoModel.name
        model.description = dtoModel.description

        return model


class StandardDao(BaseDao):
    """
    dao class to handle standards
    """

    MODEL = StandardModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(StandardDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.standard.add_standard.time')
    def addStandard(self, standardObj, quizId):
        """
        load the number of correct answer per question in the specified quiz
        :param standardObj
        :param quizId
        """
        error = None
        counter = None
        start = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""INSERT INTO quizzes_standard (quiz_id, subject_id, core_id, grade_id, standard_id,"""
                               """ "name", description) VALUES (%s, %s, %s, %s, %s, %s, %s)""",
                               (quizId, standardObj.subject_id, standardObj.core_id, standardObj.grade_id,
                                standardObj.standard_id, standardObj.name, standardObj.description))

                if cursor.rowcount == 0:
                    raise BaseDao.NotInserted

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.standard.delete_standard.time')
    def deleteStandard(self, quizId):
        """
        load the number of correct answer per question in the specified quiz
        """
        counter = None
        start = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""DELETE FROM quizzes_standard WHERE quiz_id=%s""", (quizId, ))

            self.connection.commit()

            # TODO add counters

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise

        except Exception:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.standard.get_standard_name.time')
    def getStandardName(self, quizId):
        """
        load the number of correct answer per question in the specified quiz
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT "name" from quizzes_standard where quiz_id=%s""", (quizId, ))

                if cursor.rowcount == 0:
                    return None
                else:
                    return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.standard.get_standard_names.time')
    def getStandardNames(self, quizIds):
        """
        load the number of correct answer per question in the specified quiz
        """
        counter = None
        start = None
        error = None

        if not quizIds:
            return dict()

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT quiz_id,"name" from quizzes_standard where quiz_id in %s""", (tuple(quizIds),))

                return dict((res[0], res[1]) for res in cursor.fetchall())

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.debug(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.standard.get_standard_name_and_description.time')
    def getStandardNameAndDescription(self, quizId):
        """
        load the number of correct answer per question in the specified quiz
        """
        counter = None
        start = None
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT "name",description from quizzes_standard where quiz_id=%s""", (quizId, ))

                if cursor.rowcount == 0:
                    return None
                else:
                    return cursor.fetchone()[0]

            # TODO add counters

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.debug(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()


class QuizModel(BaseModel):
    """
    model holding quiz data
    """

    ID = "id"
    NAME = "name"
    SOC_NUMBER = "soc_number"
    IS_HIDDEN = "is_hidden"
    CREATED_BY_ID = "created_by_id"
    CREATED_DATE = "created_date"
    LAST_UPDATED = "last_updated"
    SHARABLE = "sharable"
    LEGACY_ID = "legacy_id"
    PARENT_ID = "parent_id"
    DELETED = "deleted"
    HIDDEN_DATE = "hidden_date"

    FIELD_COUNT = 12

    NAME_MAX_LENGTH = 128

    def __init__(self):
        """
        Constructor
        :return:
        """
        self.id = None
        self.name = None
        self.is_hidden = False
        self.created_by_id = None
        self.created_date = None
        self.last_updated = None
        self.sharable = True
        self.legacy_id = None
        self.soc_number = None
        self.parent_id = None
        self.deleted = None
        self.hidden_date = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.NAME] = self.name
        obj[self.CREATED_BY_ID] = self.created_by_id
        obj[self.CREATED_DATE] = self.created_date
        obj[self.LAST_UPDATED] = self.last_updated
        obj[self.SHARABLE] = self.sharable
        obj[self.SOC_NUMBER] = self.soc_number
        obj[self.IS_HIDDEN] = self.is_hidden
        obj[self.LEGACY_ID] = self.legacy_id
        obj[self.PARENT_ID] = self.parent_id
        obj[self.DELETED] = self.deleted
        obj[self.HIDDEN_DATE] = self.hidden_date

        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        :param dictObj - contains the data of the model
        :type dictObj: dict
        """

        model = QuizModel()
        model.id = dictObj.get(cls.ID)
        model.name = dictObj.get(cls.NAME)
        model.created_by_id = dictObj.get(cls.CREATED_BY_ID)
        model.created_date = dictObj.get(cls.CREATED_DATE)
        model.last_updated = dictObj.get(cls.LAST_UPDATED)
        model.sharable = dictObj.get(cls.SHARABLE, True)
        model.soc_number = dictObj.get(cls.SOC_NUMBER)
        model.legacy_id = dictObj.get(cls.LEGACY_ID)
        model.is_hidden = dictObj.get(cls.IS_HIDDEN, False)
        model.parent_id = dictObj.get(cls.PARENT_ID)
        model.deleted = dictObj.get(cls.DELETED)
        model.hidden_date = dictObj.get(cls.HIDDEN_DATE)

        return model

    @classmethod
    def fromRequestDto(cls, dtoModel):
        """
        constructs a db model from the http request data
        :param dtoModel
        """
        model = QuizModel()
        model.id = dtoModel.pk
        model.name = dtoModel.name
        model.created_by_id = dtoModel.user_id
        model.created_date = dtoModel.created_date
        model.last_updated = dtoModel.last_updated
        model.sharable = dtoModel.sharable
        model.soc_number = dtoModel.soc_number
        model.legacy_id = None
        model.is_hidden = None

        return model


class QuestionModel(BaseModel):
    """
    class that holds the socrative question model
    """

    MULTIPLE_CHOICE = "MC"
    FREE_RESPONSE = "FR"
    TRUE_FALSE = "TF"
    TYPE_CHOICES = (
        (MULTIPLE_CHOICE, u"Multiple Choice"),
        (FREE_RESPONSE, u"Free Response"),
        (TRUE_FALSE, u"True/False")
    )

    TYPE = "type"
    QUESTION_ID = "question_id"
    CREATED_BY_ID = "created_by_id"
    ORDER = "order"
    QUESTION_TEXT = "question_text"
    EXPLANATION = "explanation"
    CREATED_DATE = "created_date"
    GRADING_WEIGHT = "grading_weight"
    QUIZ_ID = "quiz_id"
    LEGACY_ID = "legacy_id"

    FIELD_COUNT = 10
    QUESTION_TEXT_MAX_LENGTH = 65536

    def __init__(self):
        """
        Constructor
        :return:
        """
        self.type = None
        self.question_id = None
        self.created_by_id = None
        self.order = None
        self.question_text = None
        self.explanation = ""
        self.created_date = datetime.datetime.utcnow()
        self.grading_weight = 1.0
        self.quiz_id = None
        self.legacy_id = None

    def toDict(self):
        """
        return: dict
        """

        obj = dict()
        obj[self.TYPE] = self.type
        obj[self.QUESTION_ID] = self.question_id
        obj[self.CREATED_BY_ID] = self.created_by_id
        obj[self.ORDER] = self.order
        obj[self.QUESTION_TEXT] = self.question_text
        obj[self.EXPLANATION] = self.explanation
        obj[self.CREATED_DATE] = self.created_date
        obj[self.GRADING_WEIGHT] = self.grading_weight
        obj[self.QUIZ_ID] = self.quiz_id
        obj[self.LEGACY_ID] = self.legacy_id
        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        :param dictObj:
        """

        model = QuestionModel()
        model.type = dictObj.get(cls.TYPE)
        model.question_id = dictObj.get(cls.QUESTION_ID)
        model.created_by_id = dictObj.get(cls.CREATED_BY_ID)
        model.order = dictObj.get(cls.ORDER)
        model.question_text = dictObj.get(cls.QUESTION_TEXT)
        model.explanation = dictObj.get(cls.EXPLANATION)
        model.created_date = dictObj.get(cls.CREATED_DATE)
        model.grading_weight = dictObj.get(cls.GRADING_WEIGHT)
        model.quiz_id = dictObj.get(cls.QUIZ_ID)
        model.legacy_id = dictObj.get(cls.LEGACY_ID)

        return model

    @classmethod
    def fromRequestDto(cls, dtoModel):
        """
        :param dtoModel
        """
        model = QuestionModel()
        model.type = dtoModel.type
        model.question_id = dtoModel.question_id
        model.created_by_id = None
        model.order = dtoModel.order
        model.question_text = dtoModel.question_text
        model.explanation = dtoModel.explanation
        model.created_date = pytz.utc.localize(datetime.datetime.utcnow())
        model.grading_weight = dtoModel.grading_weight
        model.quiz_id = dtoModel.quiz_id
        model.legacy_id = None

        return model


class QuizDao(BaseDao):
    """
    class that holds methods to access and modify the quizzes and data related to quizzes table
    """

    FETCH_MANY_SIZE = 100
    MODEL = QuizModel
    LEGACY_ID_LIMIT = 5000000

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(QuizDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.quiz.check_quiz.time')
    def checkQuiz(self, quiz_id, user_id):
        """

        :param quiz_id:
        :param user_id:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""select id from quizzes_quiz where id=%s and created_by_id=%s""", (quiz_id, user_id))

                if cursor.rowcount <= 0:
                    return None

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.quiz.get_active_quiz_list.time')
    def getActiveQuizList(self, user_id):
        """

        :param user_id:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""select * from quizzes_quiz where created_by_id=%s and last_updated is not NULL and """
                               """is_hidden is not TRUE""", (user_id,))

                if cursor.rowcount <=0:
                    return list()

                quizzes = self.fetchManyAsDict(cursor)

                ids = [quiz[QuizModel.ID] for quiz in quizzes]

                cursor.execute("""select quiz_id,COALESCE(count(question_id), 0) from quizzes_question where quiz_id """
                               """in %s and created_by_id=%s group by quiz_id""", (tuple(ids), user_id))

                questionCntDict = dict((result[0], result[1]) for result in cursor.fetchall())

                cursor.execute("""select quiz_id, "name" from quizzes_standard where quiz_id in %s""", (tuple(ids),))
                standardNameDict = dict((result[0], result[1]) for result in cursor.fetchall())

                for quiz in quizzes:
                    quiz["questions"] = questionCntDict.get(quiz[QuizModel.ID], 0)
                    quiz["standard"] = standardNameDict.get(quiz[QuizModel.ID])

                return quizzes

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.quizzes.get_quiz_owners_for_date_interval.time')
    def getQuizOwnersForDateInterval(self, start_date, end_date):
        """

        :param start_date:
        :param end_date:
        :return:
        """

        if not start_date:
            return dict()

        try:
            with self.readOnlyConnection.cursor() as cursor:
                query = cursor.mogrify("""SELECT created_by_id, MAX(last_updated) as last_login FROM """
                                       """quizzes_quiz WHERE last_updated >=%s""",
                                       (start_date, ))
                if end_date:
                    query += cursor.mogrify(""" AND last_updated < %s""", (end_date,))

                query += " GROUP BY created_by_id"
                cursor.execute(query)

                return dict((row[0], row[1]) for row in cursor.fetchall())

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise

        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.quiz.get_quizzes.time')
    def getQuizzes(self, userId, last_updated=None, is_hidden=False, order=BaseDao.DESCENDING,
                   sort_by=BaseDao.CREATED_DATE, limit=1):
        """
        get quizzes
        :param userId:
        :param last_updated:
        :param is_hidden:
        :param order:
        :param sort_by:
        :param limit:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:

                query = cursor.mogrify("""SELECT * FROM quizzes_quiz WHERE created_by_id=%s and is_hidden=%s""",
                                       (userId, is_hidden))
                if last_updated:
                    query = cursor.mogrify(query+""" AND last_updated=%s""", (last_updated,))
                elif last_updated is False:
                    query = cursor.mogrify(query+""" AND last_updated is NULL""")

                if sort_by == self.CREATED_DATE:
                    query += """ ORDER BY created_date"""
                elif sort_by == self.UPDATED_DATE and last_updated is not None:
                    query += """ ORDER BY last_updated"""
                elif sort_by == self.NAME:
                    query += """ ORDER BY name"""
                else:
                    logger.warn("no order clause could be applied")

                if order == self.DESCENDING:
                    query += """ DESC"""
                else:
                    query += """ ASC"""

                if limit:
                    query = cursor.mogrify(query + """ LIMIT %s""", (limit,))

                cursor.execute(query)

                return self.fetchManyAsModel(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.quiz.create_quiz.time')
    def createQuiz(self, userId, name):
        """
        create quiz method
        :param userId:
        :param name:
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute(""" INSERT INTO quizzes_quiz (created_by_id, created_date, soc_number, "name","""
                               """is_hidden, sharable) VALUES (%s, %s,(SELECT currval('quizzes_quiz_id_seq') + %s), %s,"""
                               """False, True) RETURNING id,soc_number""", (userId,
                                                                            pytz.utc.localize(datetime.datetime.utcnow()),
                                                                            self.LEGACY_ID_LIMIT,
                                                                            name))

                if cursor.rowcount != 1:
                    raise BaseDao.NotInserted

                res = cursor.fetchone()
                modelId = res[0]
                soc = res[1]

                if str(modelId + self.LEGACY_ID_LIMIT) != soc:
                    cursor.execute(""" UPDATE quizzes_quiz SET soc_number=%s WHERE id=%s""",
                                   (modelId + self.LEGACY_ID_LIMIT, modelId))

                if cursor.rowcount != 1:
                    raise BaseDao.NotInserted

                self.connection.commit()

                return modelId

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise

        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.quiz.hide_quiz.time')
    def hideQuiz(self, quizId, value):
        """
        hide/unhide quiz
        """
        try:
            with self.connection.cursor() as cursor:

                cursor.execute("""UPDATE quizzes_quiz SET is_hidden=%s WHERE id=%s""", (value, quizId))

                if cursor.rowcount != 1:
                    raise BaseDao.NotUpdated

                self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.quiz.hide_quizzes.time')
    def hideQuizzes(self, user_id, quiz_ids):
        """
        hide quizzes
        :param user_id:
        :param quiz_ids
        """
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE quizzes_quiz SET is_hidden=TRUE WHERE id in %s and created_by_id=%s""",
                               (tuple(quiz_ids), user_id))

                self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.quiz.save_quiz.time')
    def saveQuiz(self, quizModel):
        """
        save quiz
        :param quizModel:
        :return:
        """

        try:
            dct = quizModel.toDict()
            with self.connection.cursor() as cursor:
                cursor.execute("""INSERT INTO quizzes_quiz ("name", is_hidden, created_by_id, created_date,"""
                               """last_updated,sharable, soc_number) VALUES (%(name)s,%(is_hidden)s,%(created_by_id)s,"""
                               """%(created_date)s,%(last_updated)s, %(sharable)s,"""
                               """(SELECT currval('quizzes_quiz_id_seq'))) RETURNING *""", dct)

                if cursor.rowcount != 1:
                    raise self.NotInserted
                model = self.fetchOneAsModel(cursor)

                # to be on the safe side, if it doesn't happen great, if this happens, we need to fix it,
                #  that's why the update
                if str(model.id + self.LEGACY_ID_LIMIT) != model.soc_number:
                    cursor.execute("""UPDATE quizzes_quiz SET soc_number=%s WHERE id=%s RETURNING id""",
                                   (model.id + self.LEGACY_ID_LIMIT, model.id))
                    quiz_id = cursor.fetchone()[0]
                    return quiz_id

                self.connection.commit()
                return model.id
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.quiz.clone_quiz.time')
    def cloneQuiz(self, quizModel, userId, hideCloning=False, hideOldQuiz=True, addStandards=False, name=None):
        """
        clone a quiz with questions, answers and tags
        :param quizModel
        :param userId
        :param hideCloning
        :param hideOldQuiz
        :param name:
        :param addStandards:
        """
        try:
            model = None
            dct = quizModel.toDict()

            if hideCloning is False:
                dct[QuizModel.NAME] = dct[QuizModel.NAME][:240] + " (%s)" % ugettext("copy")
            else:
                dct[QuizModel.IS_HIDDEN] = False

            if name:
                dct[QuizModel.NAME] = name

            # this is for importing initial quiz
            if hideOldQuiz is False:
                dct[QuizModel.IS_HIDDEN] = False

            dct[QuizModel.CREATED_BY_ID] = userId
            d = datetime.datetime.utcnow()
            dct[QuizModel.LAST_UPDATED] = pytz.utc.localize(d)

            with self.connection.cursor() as cursor:
                if hideCloning is False or hideOldQuiz is False:
                    cursor.execute("""INSERT INTO quizzes_quiz ("name", is_hidden, created_by_id, created_date,"""
                                   """last_updated,sharable, soc_number, parent_id) VALUES (%(name)s,%(is_hidden)s,"""
                                   """%(created_by_id)s,%(created_date)s,%(last_updated)s, %(sharable)s,"""
                                   """(SELECT currval('quizzes_quiz_id_seq')),%(parent_id)s) RETURNING *""", dct)
                else:
                    cursor.execute("""INSERT INTO quizzes_quiz ("name", is_hidden, created_by_id, created_date,"""
                                   """last_updated,sharable, soc_number, parent_id) VALUES (%(name)s,%(is_hidden)s,"""
                                   """%(created_by_id)s,%(created_date)s,%(last_updated)s, %(sharable)s, """
                                   """%(soc_number)s, %(parent_id)s) RETURNING *""", dct)

                if cursor.rowcount != 1:
                    raise self.NotInserted
                model = self.fetchOneAsModel(cursor)

                # to be on the safe side, if it doesn't happen great, if this happens, we need to fix it,
                #  that's why the update
                if (hideCloning is False or hideOldQuiz is False) and str(model.id + self.LEGACY_ID_LIMIT) != model.soc_number:
                    cursor.execute("""UPDATE quizzes_quiz SET soc_number=%s WHERE id=%s RETURNING *""",
                                   (model.id + self.LEGACY_ID_LIMIT, model.id))
                    model = self.fetchOneAsModel(cursor)

                # clone questions
                self.daoRegistry.questionDao.cloneQuestions(model, quizModel.id, my_cursor=cursor)

                if cursor.closed:
                    cursor = self.connection.cursor()

                if addStandards:
                    # clone standard
                    query = cursor.mogrify("""INSERT INTO quizzes_standard (quiz_id, subject_id, core_id, grade_id, """
                                           """ standard_id, "name", description) SELECT %s,subject_id, core_id, """
                                           """ grade_id, standard_id, "name",description FROM quizzes_standard WHERE """
                                           """ quiz_id=%s""", (model.id, quizModel.id))
                    cursor.execute(query)
                    if cursor.rowcount < 0:
                        raise self.NotInserted

                if hideCloning is True and hideOldQuiz is True:
                    cursor.execute("""UPDATE quizzes_quiz SET is_hidden=%s WHERE id=%s""", (True, quizModel.id))
                    if cursor.rowcount <= 0:
                        raise self.NotUpdated

            self.connection.commit()

            return model

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.quiz.load_quiz_by_id.time')
    def loadQuizById(self, quizId, auth_token=None, isHidden=None, _type=BaseDao.TO_DICT):
        """
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                if auth_token is None:

                    if isHidden is None:
                        cursor.execute("""SELECT * FROM quizzes_quiz WHERE id=%s ORDER BY id DESC LIMIT 1""", (quizId,))
                    else:
                        cursor.execute("""SELECT * FROM quizzes_quiz WHERE id=%s AND is_hidden=%s ORDER BY id DESC"""
                                       """ LIMIT 1""", (quizId, isHidden))
                else:
                    if isHidden is None:
                        cursor.execute("""SELECT a.* FROM quizzes_quiz AS a INNER JOIN socrative_users_socrativeuser as b"""
                                   """ ON a.id=%s and a.created_by_id=b.id and b.auth_token=%s ORDER BY a.id DESC """
                                   """LIMIT 1""", (quizId, auth_token))
                    else:
                        cursor.execute("""SELECT a.* FROM quizzes_quiz AS a INNER JOIN socrative_users_socrativeuser as b"""
                                   """ ON a.id=%s and a.created_by_id=b.id and b.auth_token=%s  WHERE a.is_hidden=%s """
                                   """ORDER BY a.id DESC LIMIT 1""", (quizId, auth_token, isHidden))

                if cursor.rowcount <= 0:
                    raise self.NotFound

                if cursor.rowcount != 1:
                    raise self.FoundTooMany

                if _type == BaseDao.TO_DICT:
                    return self.fetchOneAsDict(cursor)
                else:
                    return self.fetchOneAsModel(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            error = 1
            raise

    @retry
    @statsd.timed('socrative.dao.quiz.update_quiz.time')
    def updateQuiz(self, quizDict):
        """
        """

        if len(quizDict) == 0:
            return
        try:
            with self.connection.cursor() as cursor:
                query = ",".join([key+"=%("+key+")s" for key in quizDict if key != QuizModel.ID])
                query = cursor.mogrify(query, quizDict)
                cursor.execute("""UPDATE quizzes_quiz SET """ + query + """ WHERE id=%d""" % quizDict[QuizModel.ID])

                if cursor.rowcount != 1:
                    raise self.NotUpdated

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.quiz.get_quizzes_from_soc.time')
    def getQuizzesFromSoc(self, soc_number, sharable, auth_token=None, orderBy=BaseDao.CREATED_DATE,
                          sortOrder=BaseDao.DESCENDING, limit=1, is_hidden=None, _type=BaseDao.TO_DICT):
        """

        :param soc_number:
        :param sharable:
        :param auth_token:
        :param orderBy:
        :param sortOrder:
        :param is_hidden:
        :param limit:
        :param _type:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                if auth_token is None:
                    query = """SELECT a.* FROM quizzes_quiz AS a WHERE a.soc_number=%s AND a.sharable=%s"""
                    query = cursor.mogrify(query, (soc_number, sharable))
                else:
                    query = """SELECT a.* FROM quizzes_quiz as a INNER JOIN socrative_users_socrativeuser as b ON""" +\
                            """ a.created_by_id = b.id WHERE a.soc_number=%s AND a.sharable=%s AND b.auth_token=%s"""
                    query = cursor.mogrify(query, (soc_number, sharable, auth_token))

                if is_hidden is not None:
                    query += cursor.mogrify(""" AND a.is_hidden=%s""", (is_hidden,))

                if orderBy == BaseDao.CREATED_DATE:
                    query += " ORDER BY a.%s" % QuizModel.CREATED_DATE
                    if sortOrder == BaseDao.DESCENDING:
                        query += " DESC"
                    elif sortOrder == BaseDao.ASCENDING:
                        query += " ASC"
                elif orderBy == BaseDao.UPDATED_DATE:
                    query += " ORDER BY a.%s" % QuizModel.LAST_UPDATED
                    if sortOrder == BaseDao.DESCENDING:
                        query += " DESC"
                    elif sortOrder == BaseDao.ASCENDING:
                        query += " ASC"

                query += " LIMIT %s" % limit

                cursor.execute(query)

                if _type == self.TO_MODEL:
                    return self.fetchManyAsModel(cursor)
                else:
                    return self.fetchOneAsDict(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.quiz.load_quiz_by_soc.time')
    def loadQuizBySoc(self, soc_number, auth_token=None, is_hidden=None, _type=BaseDao.TO_DICT):
        """

        :param soc_number:
        :param is_hidden:
        :param _type:
        :return:
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                if auth_token is None:
                    query = cursor.mogrify("""SELECT * FROM quizzes_quiz WHERE soc_number=%s""", (soc_number,))
                    if is_hidden is not None:
                        query += cursor.mogrify(""" AND is_hidden=%s""", (is_hidden,))

                    query += """ ORDER BY created_date DESC LIMIT 1"""
                else:
                    query = cursor.mogrify("""SELECT a.* FROM quizzes_quiz AS a INNER JOIN socrative_users_socrativeuser"""
                                           """ as b ON a.created_by_id = b.id WHERE a.soc_number=%s AND b.auth_token=%s""",
                                           (soc_number, auth_token))
                    if is_hidden is not None:
                        query += cursor.mogrify(""" AND is_hidden=%s""", (is_hidden,))

                    query += """ ORDER BY created_date DESC LIMIT 1"""

                cursor.execute(query)

                if cursor.rowcount <=0:
                    raise BaseDao.NotFound

                if _type == BaseDao.TO_MODEL:
                    return self.fetchOneAsModel(cursor)
                else:
                    return self.fetchOneAsDict(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.quiz.load_deep_quiz.time')
    def loadDeepQuiz(self, quiz_id=None, auth_token=None, soc_number=None, sharable=None, isHidden=None, orderBy=None,
                     sortBy=None, limit=None,  offset=None):
        """

        :param quiz_id:
        :param auth_token:
        :param soc_number:
        :param sharable:
        :param isHidden:
        :param orderBy:
        :param sortBy:
        :param limit:
        :param offset:
        :return:
        """
        error = None
        try:
            start = time.time()
            with self.connection.cursor() as cursor:
                query = cursor.mogrify("""SELECT * FROM (SELECT DISTINCT ON (qz.soc_number) qz.soc_number,qz.* FROM """
                                       """quizzes_quiz AS qz """)

                where = 0
                if auth_token is not None:
                    query += cursor.mogrify(""" INNER JOIN socrative_users_socrativeuser AS su ON"""
                                            """ qz.created_by_id=su.id WHERE su.auth_token=%s""", (auth_token,))
                    where = 1

                if quiz_id is not None:
                    if where:
                        query += cursor.mogrify(""" AND qz.id=%s""", (quiz_id,))
                    else:
                        query += cursor.mogrify(""" WHERE qz.id=%s""", (quiz_id,))
                        where = 1

                if soc_number is not None:
                    if where:
                        query += cursor.mogrify(""" AND qz.soc_number=%s""", (soc_number,))
                    else:
                        query += cursor.mogrify(""" WHERE qz.soc_number=%s""", (soc_number,))
                        where = 1

                if sharable is not None:
                    if where:
                        query += cursor.mogrify(""" AND qz.sharable=%s""", (sharable,))
                    else:
                        query += cursor.mogrify(""" WHERE qz.sharable=%s""", (sharable,))
                        where = 1

                if isHidden is not None:
                    if where:
                        query += cursor.mogrify(""" AND qz.is_hidden=%s""", (isHidden,))
                    else:
                        query += cursor.mogrify(""" WHERE qz.is_hidden=%s""", (isHidden,))

                order = "created_date DESC"
                if orderBy == self.CREATED_DATE and sortBy == self.ASCENDING:
                    query += " ORDER BY qz.soc_number, qz.created_date ASC "
                    order = "created_date ASC"
                elif orderBy == self.CREATED_DATE and sortBy == self.DESCENDING:
                    query += " ORDER BY qz.soc_number, qz.created_date DESC"
                elif orderBy == self.CREATED_DATE:
                    query += " ORDER BY qz.soc_number, qz.created_date"
                    order = "created_date"
                elif orderBy == self.UPDATED_DATE and sortBy == self.ASCENDING:
                    query += " ORDER BY qz.soc_number, qz.last_updated ASC"
                    order = "last_updated ASC"
                elif orderBy == self.UPDATED_DATE and sortBy == self.DESCENDING:
                    query += " ORDER BY qz.soc_number, qz.last_updated DESC"
                    order = "last_updated DESC"
                elif orderBy == self.UPDATED_DATE:
                    query += " ORDER BY qz.soc_number, qz.last_updated"
                    order = "last_updated"
                elif orderBy == self.NAME and sortBy == self.ASCENDING:
                    query += " ORDER BY qz.soc_number, qz.name ASC"
                    order = "name ASC"
                elif orderBy == self.NAME and sortBy == self.DESCENDING:
                    query += " ORDER BY qz.soc_number, qz.name DESC"
                    order = "name DESC"
                elif orderBy == self.NAME:
                    query += " ORDER BY qz.soc_number, qz.name"
                    order = "name"

                query += cursor.mogrify(""" ) AS subquery ORDER BY """ + order)

                if offset is not None:
                    query += cursor.mogrify(""" OFFSET %s""", (offset,))

                if limit is not None:
                    query += cursor.mogrify(""" LIMIT %s """, (limit,))

                cursor.execute(query)

                result = cursor.fetchall()

                quizDict = OrderedDict()
                questionDict = dict()
                answerDict = dict()
                tagsDict = dict()
                resourceDict = dict()

                for r in result:
                    quiz = self._loadDictFromResultTupleInterval(r, cursor.description, 0, len(cursor.description),
                                                                  QuizModel)
                    if quiz[QuizModel.ID] not in quizDict:
                        quizDict[quiz[QuizModel.ID]] = quiz

                if quizDict.keys():
                    cursor.execute("""SELECT * FROM quizzes_question WHERE quiz_id IN %s""", (tuple(quizDict.keys()),))
                    result = cursor.fetchall()

                    for r in result:
                        stop = len(cursor.description)
                        question = self._loadDictFromResultTupleInterval(r, cursor.description, 0, stop, QuestionModel)
                        question["has_correct_answer"] = False
                        questionDict[question[QuestionModel.QUESTION_ID]] = question

                if questionDict:
                    cursor.execute("""SELECT * FROM quizzes_answer WHERE question_id IN %s""", (tuple(questionDict.keys()),))
                    result = cursor.fetchall()

                    for r in result:
                        stop = len(cursor.description)
                        answer = self._loadDictFromResultTupleInterval(r, cursor.description, 0, stop, AnswerModel)
                        answerDict[answer[AnswerModel.ID]] = answer

                if questionDict:
                    cursor.execute("""SELECT q.question_id, m.* FROM common_mediaresource as m INNER JOIN quizzes_question_resources as q"""
                                   """ ON m.id=q.mediaresource_id WHERE q.question_id IN %s""", (tuple(questionDict.keys()),))
                    result = cursor.fetchall()

                    for r in result:
                        stop = len(cursor.description)
                        resource = self._loadDictFromResultTupleInterval(r, cursor.description, 1, stop,
                                                                         MediaResourceModel)
                        resource[QuestionModel.QUESTION_ID] = r[0]
                        resourceDict[resource[MediaResourceModel.ID]] = resource

                if quizDict.keys():
                    cursor.execute("""SELECT * FROM quizzes_standard WHERE quiz_id IN %s""", (tuple(quizDict.keys()),))
                    result = cursor.fetchall()

                    for r in result:
                        start = 0
                        stop = len(cursor.description)
                        standard = self._loadDictFromResultTupleInterval(r, cursor.description, start, stop, StandardModel)

                        standard = StandardModel.fromDict(standard).toDict()

                        tagsDict[standard[StandardModel.ID]] = standard

                # normaly we shouldn't have more than a standard per quiz.
                # if we have more, then we overwrite them
                for t in tagsDict.values():
                    if t["quiz_id"] in quizDict:
                        quizDict[t["quiz_id"]]["standard"] = t

                for r in resourceDict.values():
                    if r[QuestionModel.QUESTION_ID] in questionDict:
                        if "resources" in questionDict[r[QuestionModel.QUESTION_ID]]:
                            questionDict[r[QuestionModel.QUESTION_ID]]["resources"].append(r)
                        else:
                            questionDict[r[QuestionModel.QUESTION_ID]]["resources"] = [r]

                for a in answerDict.values():
                    if a[AnswerModel.QUESTION_ID] in questionDict:
                        if a.get(AnswerModel.IS_CORRECT) is True:
                            questionDict[a[AnswerModel.QUESTION_ID]]["has_correct_answer"] = True
                        if "answers" in questionDict[a[AnswerModel.QUESTION_ID]]:
                            questionDict[a[AnswerModel.QUESTION_ID]]["answers"].append(a)
                        else:
                            questionDict[a[AnswerModel.QUESTION_ID]]["answers"] = [a]

                for q in questionDict.values():
                    if q[QuestionModel.QUIZ_ID] in quizDict:
                        if "questions" in quizDict[q[QuestionModel.QUIZ_ID]]:
                            quizDict[q[QuestionModel.QUIZ_ID]]["questions"].append(q)
                        else:
                            quizDict[q[QuestionModel.QUIZ_ID]]["questions"] = [q]

            return quizDict.values()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.quiz.load_correct_answers.time')
    def loadCorrectAnswers(self, quizId, qType=None):
        """
        load the number of correct answer per question in the specified quiz
        :param quizId
        :param qType
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                if qType:
                    cursor.execute("""SELECT b.question_id, COUNT(d.is_correct) FROM quizzes_quiz as a LEFT JOIN """
                                   """quizzes_question AS b ON b.quiz_id=a.id LEFT JOIN quizzes_answer AS d ON """
                                   """d.question_id=b.question_id WHERE a.id=%s AND b.type=%s AND d.is_correct=TRUE GROUP"""
                                   """ BY b.question_id""", (quizId, qType))
                else:
                    cursor.execute("""SELECT b.question_id, COUNT(d.is_correct) FROM quizzes_quiz as a LEFT JOIN """
                                   """quizzes_question AS b ON b.quiz_id=a.id LEFT JOIN quizzes_answer AS d ON """
                                   """d.question_id=b.question_id WHERE a.id=%s AND d.is_correct=TRUE GROUP"""
                                   """ BY b.question_id""", (quizId, ))

                resp = dict()
                for d in cursor.fetchall():
                    resp[d[0]] = d[1]
                return resp

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.quiz.save_full_quiz.time')
    def saveFullQuiz(self, quizModel):
        """
        save quiz
        :param quizModel:
        :return:
        """
        error = None
        try:
            dct = quizModel.toDict()
            with self.connection.cursor() as cursor:
                cursor.execute("""INSERT INTO quizzes_quiz ("name", is_hidden, created_by_id, created_date,"""
                               """last_updated,sharable,soc_number) VALUES (%(name)s,%(is_hidden)s,%(created_by_id)s,"""
                               """%(created_date)s,%(last_updated)s, %(sharable)s,"""
                               """(SELECT currval('quizzes_quiz_id_seq'))) RETURNING *""", dct)

                if cursor.rowcount != 1:
                    raise self.NotInserted
                model = self.fetchOneAsModel(cursor)

                # to be on the safe side, if it doesn't happen great, if this happens, we need to fix it,
                #  that's why the update
                if str(model.id + self.LEGACY_ID_LIMIT) != model.soc_number:
                    cursor.execute("""UPDATE quizzes_quiz SET soc_number=%s WHERE id=%s RETURNING soc_number""",
                                   (model.id + self.LEGACY_ID_LIMIT, model.id))
                    model.soc_number = cursor.fetchone()[0]

                for question in quizModel.questions:
                    question.quiz_id = model.id
                    modDict = question.toDict()
                    query = ",".join(["\"%s\"" % key for key in modDict if key != QuestionModel.QUESTION_ID])
                    values = ",".join(["%("+key+")s" for key in modDict if key != QuestionModel.QUESTION_ID])
                    values = cursor.mogrify(values, modDict)

                    cursor.execute("""INSERT INTO quizzes_question (""" + query + """) VALUES (""" + values + """) """
                                   """RETURNING question_id""")
                    qId = cursor.fetchone()[0]
                    for answer in question.answers:
                        answer.question_id = qId
                        modDict = answer.toDict()

                        query = ",".join(["\"%s\"" % key for key in modDict if key != AnswerModel.ID])
                        values = ",".join(["%("+key+")s" for key in modDict if key != AnswerModel.ID])
                        values = cursor.mogrify(values, modDict)

                        # TODO check if we need all the values back
                        cursor.execute("""INSERT INTO quizzes_answer (""" + query + """) VALUES (""" + values + """)""")

                self.connection.commit()
                return model
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            error = 0
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()


class QuestionDao(BaseDao):
    """
    dao class for question related queries
    """
    FETCH_MANY_SIZE = 100
    MODEL = QuestionModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(QuestionDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.question.get_question_model_by_id.time')
    def getQuestionModelById(self, questionId):
        """
        get the model using the primary key
        :param questionId:
        :param key:
        :return:
        """

        if questionId is None:
            raise ValueError("questionId must be a valid int , not None")

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM quizzes_question WHERE question_id=%s""", (questionId,))
                return self.fetchOneAsModel(cursor)

        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.question.count.time')
    def count(self, quiz_id):
        """
        get the model using the primary key
        :param quiz_id:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT count(question_id) FROM quizzes_question WHERE quiz_id=%s""", (quiz_id,))

                if cursor.rowcount <=0:
                    return 0

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.debug(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.question.has_correct_answers.time')
    def hasCorrectAnswers(self, questionId):
        """
        get the number of correct answers
        :param questionId:
        :param key:
        :return:
        """
        if questionId is None:
            raise ValueError("questionId must be a valid int , not None")

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT COUNT(id) FROM quizzes_answer WHERE question_id=%s and is_correct=%s""",
                               (questionId, True))
                return cursor.fetchone()[0] > 0
        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.question.clone_questions.time')
    def cloneQuestions(self, quizModel, oldQuizId, my_cursor=None):
        """

        :param quizModel:
        :param oldQuizId:
        :param my_cursor:
        :return:
        """

        try:
            with (my_cursor or self.connection.cursor()) as cursor:
                cursor.execute("""SELECT question_id FROM quizzes_question where quiz_id=%s""", (oldQuizId,))

                if cursor.rowcount == 0:
                    return

                oldQIdList = [a[0] for a in cursor.fetchall()]

                newQIdList = list()
                for q in oldQIdList:
                    cursor.execute("""INSERT INTO quizzes_question ("type", created_by_id, "order", question_text,"""
                                   """explanation, created_date, grading_weight, quiz_id, legacy_id) SELECT "type","""
                                   """%s,"order",question_text,explanation,created_date, grading_weight, %s, legacy_id """
                                   """FROM quizzes_question WHERE question_id=%s RETURNING question_id""",
                                   (quizModel.created_by_id, quizModel.id, q))
                    newQIdList.append(cursor.fetchone()[0])

                logger.debug("number of cloned questions :%d , original questions %d" % (len(newQIdList), len(oldQIdList)))

                # clone resources
                self.daoRegistry.mediaResourceDao.cloneResources(oldQIdList, newQIdList, quizModel.created_by_id,
                                                                 my_cursor=cursor)
                if cursor.closed:
                    cursor = self.connection.cursor()

                # clone answers
                self.daoRegistry.answerDao.cloneAnswers(oldQIdList, newQIdList, quizModel.created_by_id,
                                                        my_cursor=cursor)

                self.connection.commit()
        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.question.load_question_by_id.time')
    def loadQuestionById(self, question_id, auth_token=None, _type=BaseDao.TO_DICT):
        """

        :param question_id:
        :param auth_token:
        :param _type:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                if auth_token is None:
                    cursor.execute("""SELECT * FROM quizzes_question WHERE question_id=%s""", (question_id,))
                else:
                    cursor.execute("""SELECT a.* FROM quizzes_question AS a INNER JOIN socrative_users_socrativeuser """
                                   """AS b ON a.question_id=%s AND a.created_by_id=b.id AND b.auth_token=%s""",
                                   (question_id, auth_token))

                if cursor.rowcount <= 0:
                    raise self.NotFound

                if cursor.rowcount != 1:
                    raise self.FoundTooMany

                if _type == BaseDao.TO_DICT:
                    return self.fetchOneAsDict(cursor)
                else:
                    return self.fetchOneAsModel(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.question.delete_all_resources.time')
    def deleteAllResources(self, question_id):
        """

        :param question_id:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                # delete images
                cursor.execute("""DELETE FROM common_mediaresource WHERE id IN (SELECT mediaresource_id FROM """
                               """quizzes_question_resources WHERE question_id=%s)""", (question_id,))

                cursor.execute("""DELETE FROM quizzes_question_resources WHERE question_id=%s""", (question_id,))

            self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.question.add_resource.time')
    def addResource(self, question_id, mediaId):
        """

        :param question_id:
        :param mediaId:
        :return:
        """
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""INSERT INTO quizzes_question_resources (question_id, mediaresource_id) VALUES """
                               """(%s, %s)""", (question_id, mediaId))

            self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.question.create_question.time')
    def createQuestion(self, dictModel):
        """

        :param dictModel:
        :return:
        """

        error = None
        try:
            start = time.time()
            with self.connection.cursor() as cursor:
                query = ",".join(["\"%s\"" % key for key in dictModel if key != QuestionModel.QUESTION_ID])
                values = ",".join(["%("+key+")s" for key in dictModel if key != QuestionModel.QUESTION_ID])
                values = cursor.mogrify(values, dictModel)

                cursor.execute("""INSERT INTO quizzes_question (""" + query + """) VALUES (""" + values + """) """
                               """RETURNING question_id""")

                if cursor.rowcount != 1:
                    raise self.NotInserted

                dictModel[QuestionModel.QUESTION_ID] = cursor.fetchone()[0]

                return dictModel

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.question.delete_question.time')
    def deleteQuestion(self, questionId):
        """

        :param questionId:
        :return:
        """
        try:
            with self.connection.cursor() as cursor:

                # delete answers
                cursor.execute("""DELETE FROM quizzes_answer WHERE question_id=%s""", (questionId,))
                # delete images
                cursor.execute("""DELETE FROM common_mediaresource WHERE id IN (SELECT mediaresource_id FROM """
                               """quizzes_question_resources WHERE question_id=%s)""", (questionId,))

                cursor.execute("""DELETE FROM quizzes_question_resources WHERE question_id=%s""", (questionId,))

                # delete question
                cursor.execute("""DELETE FROM quizzes_question WHERE question_id=%s""", (questionId,))

            self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise

        except Exception as e:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.question.update_question.time')
    def updateQuestion(self, questionDict):
        """

        :param questionDict:
        :return:
        """
        conn = None
        try:
            if not questionDict.get(QuestionModel.QUESTION_ID):
                raise Exception("Question missing question id")

            qId = safeToInt(questionDict[QuestionModel.QUESTION_ID])
            if qId is None or qId < 0:
                raise Exception("Question Id is not an integer value")

            with self.connection.cursor() as cursor:
                query = ",".join(["\""+key+"\""+"=%("+key+")s" for key in questionDict if key != QuestionModel.QUESTION_ID])
                query = cursor.mogrify(query, questionDict)
                conn = 1
                cursor.execute("""UPDATE quizzes_question SET """ + query + """ WHERE question_id=%d""" % qId)

                if cursor.rowcount != 1:
                    raise self.NotUpdated

            self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            self.connection = None
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            if conn:
                self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.question.get_questions.time')
    def getQuestions(self, quiz_id, limit=None, orderBy=None, sortOrder=None, _type=BaseDao.TO_MODEL):
        """

        :param quiz_id:
        :param limit:
        :param orderBy:
        :param sortOrder:
        :param _type:
        :return:
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                query = cursor.mogrify("""SELECT * FROM quizzes_question WHERE quiz_id=%s""", (quiz_id,))

                if orderBy == self.ORDER  and sortOrder == self.ASCENDING:
                    query += """ ORDER BY "order" ASC"""
                elif orderBy == self.ORDER and sortOrder == self.DESCENDING:
                    query += """ ORDER BY "order" DESC"""
                elif orderBy == self.ORDER:
                    query += """ ORDER BY "order" """

                if limit is not None and limit > 0:
                    query += cursor.mogrify(""" LIMIT %s""", (limit,))

                cursor.execute(query)

                if _type == self.TO_MODEL:
                    return self.fetchManyAsModel(cursor)
                else:
                    return self.fetchManyAsDict(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise

        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.question.get_question_stats.time')
    def getQuestionStats(self, quiz_id):
        """
        returns the total number of questions and the total number of neutral(no correct answer questions)
        :param quiz_id:
        :return: tuple (total, neutral)
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT q.question_id, (SELECT COUNT(id) FROM quizzes_answer WHERE """
                               """question_id=q.question_id and is_correct is TRUE) as neutral FROM quizzes_question"""
                               """ AS q WHERE quiz_id=%s""", (quiz_id,))
                if cursor.rowcount == 0:
                    return 0, 0

                respDict = dict((r[0], r[1]) for r in cursor.fetchall())

                total = len(respDict)
                neutral = 0

                for k in respDict:
                    if respDict[k] == 0:
                        neutral += 1

                return total, neutral

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            error = 1
            raise
        finally:
            if error:
                self.connection.rollback()
            else:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.question.count_resources.time')
    def countResources(self, questionIds):
        """
        returns the sum of the number of resources for all the question ids
        :param questionIds:
        :return: tuple (total, neutral)
        """
        if not questionIds:
            return 0

        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT SUM(cnt) FROM (SELECT COUNT(mediaresource_id) AS cnt FROM quizzes_question_resources"""
                               """ where question_id in %s) as subquery""", (tuple(questionIds), ))
                if cursor.rowcount == 0:
                    return 0

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise

        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.question.load_questions_for_quizzes.time')
    def loadQuestionsForQuizzes(self, quizIdList):
        """
        load all the questions for all the quizzes in the list
        :return dict: quizId->questionModel list
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * from quizzes_question WHERE quiz_id IN %s""", (tuple(quizIdList),))

                resp = self.fetchManyAsModel(cursor)
                respDict = dict()
                for r in resp:
                    if r.quiz_id in respDict:
                        respDict[r.quiz_id].append(r)
                    else:
                        respDict[r.quiz_id] = [r]

                return respDict

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.question.load_question_for_quiz.time')
    def loadQuestionForQuiz(self, quizId):
        """
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM quizzes_question WHERE "order"=(SELECT MIN("order") FROM """
                               """quizzes_question WHERE quiz_id=%s) AND quiz_id=%s""", (quizId, quizId))

                if cursor.rowcount <= 0:
                    raise BaseDao.NotFound

                return self.fetchOneAsModel(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.question.get_question_type_for_quiz.time')
    def getQuestionTypeForQuiz(self, quizId):
        """
        load all the questions for all the quizzes in the list
        :return dict: quizId->questionModel list
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT "type",question_text from quizzes_question WHERE quiz_id=%s LIMIT 1""",
                               (quizId,))

                if cursor.rowcount <= 0:
                    return None

                return cursor.fetchone()[0]
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise

        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.question.check_existing_question.time')
    def checkExistingQuestion(self, quizId, order, qType, qText):
        """
        :param quizId
        :param order
        :param qType
        :param qText
        :return tuple: (sameQuestionExists, questionId, availableOrder)
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""select question_id, "order","type",question_text from quizzes_question where """
                               """quiz_id=%s AND "order"=%s""", (quizId, order))

                if cursor.rowcount == 0:
                    return False, None, None
                else:
                    results = cursor.fetchone()
                    qId = results[0]
                    qOrder = results[1]
                    oldQType = results[2]
                    oldQText = results[3]

                    if order == qOrder and qType == oldQType and oldQText == qText:
                        logger.info("question with id %d already exists" % qId)
                        return True, qId, None
                    else:
                        cursor.execute("""select MAX("order") from quizzes_question where quiz_id=%s""", (quizId,))
                        newOrder = cursor.fetchone()[0] + 1
                        return True, None, newOrder
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.question.delete_by_order.time')
    def deleteByOrder(self, order, quizId):
        """
        :param quizId
        :param order
        :param qType
        :param qText
        :return tuple: (sameQuestionExists, questionId, availableOrder)
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT question_id FROM quizzes_question WHERE quiz_id=%s AND "order"=%s ORDER BY """
                               """ question_id DESC""", (quizId, order))
                if cursor.rowcount > 1:
                    qIds = [r[0] for r in cursor.fetchall()]
                    cursor.execute("""DELETE from quizzes_question_resources as qqr where question_id in %s AND """
                                   """(SELECT COUNT(id) FROM quizzes_answer where question_id=qqr.question_id)=0""",
                                   (tuple(qIds[1:]),))
                    cursor.execute("""DELETE from quizzes_question as b where question_id in %s AND (SELECT COUNT(id)"""
                                   """FROM quizzes_answer where question_id=b.question_id)=0""", (tuple(qIds[1:]),))

                    cursor.execute("""SELECT * FROM quizzes_question where question_id=%s""", (qIds[0],))

                    return self.fetchOneAsDict(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.question.load_resource_ids.time')
    def loadResourceIds(self, questionId):
        """
        :param: questionId
        :type: int,long
        :return dict with key url and value as the media resource id
        :rtype dict
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT m.url, m.id FROM common_mediaresource AS m INNER JOIN """
                               """quizzes_question_resources AS a ON m.id=a.mediaresource_id WHERE a.question_id=%s""",
                               (questionId,))

                resp = dict((a[0], a[1]) for a in cursor.fetchall())

                return resp

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.question.update_order.time')
    def updateOrder(self, quizId, deletedOrder):
        """
        """
        # TODO add counters
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE quizzes_question SET "order"="order"-1 WHERE quiz_id=%s AND "order">=%s""",
                               (quizId, deletedOrder))

            self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise

        except Exception as e:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.question.update_orders.time')
    def updateOrders(self, qOrdersList):
        """

        :param qOrdersList:
        :return:
        """
        questionIds = dict((a.question_id, a.order) for a in qOrdersList)

        if len(questionIds) != len(set(questionIds.values())):
            raise Exception("""There are two or more questions with the same order""")

        try:
            with self.connection.cursor() as cursor:
                for qId in questionIds:
                    cursor.execute("""UPDATE quizzes_question set "order"=%s where question_id=%s""",
                                   (questionIds[qId], qId))

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            self.connection = None
            raise

        except Exception as e:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.question.load_question_order.time')
    def loadQuestionOrder(self, quizId):
        """
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT question_id, "order" from quizzes_question where quiz_id=%s ORDER BY """
                               """ "order" ASC""", (quizId,))

                resp = dict((a[0], a[1]) for a in cursor.fetchall())

                return resp

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()


class AnswerModel(BaseModel):
    """
    class that holds the model for socrative Answer
    """

    ID = "id"
    CREATED_BY_ID = "created_by_id"
    TEXT = "text"
    IS_CORRECT = "is_correct"
    ORDER = "order"
    QUESTION_ID = "question_id"
    LEGACY_ID = "legacy_id"

    FIELD_COUNT = 7

    TEXT_MAX_LENGTH = 10240

    def __init__(self):
        """
        Constructor
        :return:
        """

        self.id = None
        self.created_by_id = None
        self.text = None
        self.is_correct = None
        self.order = None
        self.question_id = None
        self.legacy_id = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.QUESTION_ID] = self.question_id
        obj[self.CREATED_BY_ID] = self.created_by_id
        obj[self.ORDER] = self.order
        obj[self.TEXT] = self.text
        obj[self.IS_CORRECT] = self.is_correct
        obj[self.LEGACY_ID] = self.legacy_id
        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        returns a model from
        """

        model = AnswerModel()
        model.id = dictObj.get(cls.ID)
        model.question_id = dictObj.get(cls.QUESTION_ID)
        model.created_by_id = dictObj.get(cls.CREATED_BY_ID)
        model.order = dictObj.get(cls.ORDER)
        model.text = dictObj.get(cls.TEXT)
        model.is_correct = dictObj.get(cls.IS_CORRECT)
        model.legacy_id = dictObj.get(cls.LEGACY_ID)

        return model

    @classmethod
    def fromRequestDto(cls, dtoModel):
        """
        """

        model = AnswerModel()
        model.id = dtoModel.id
        model.question_id = None
        model.created_by_id = None
        model.order = dtoModel.order
        model.text = dtoModel.text
        model.is_correct = safeToBool(dtoModel.is_correct)
        model.legacy_id = None

        return model


class AnswerDao(BaseDao):
    """
    class that has methods to access and modify data related to quizzes_answer table
    """

    FETCH_MANY_SIZE = 100
    MODEL = AnswerModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(AnswerDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.answer.get_answer_models.time')
    def getAnswerModels(self, question_id, is_correct=None):
        """

        :param question_id:
        :param is_correct:
        :param key:
        :return:
        """

        if question_id is None:
            raise ValueError("question_id must be a valid int not None")

        error = None
        try:
            with self.connection.cursor() as cursor:
                if is_correct is None:
                    cursor.execute("""SELECT * FROM quizzes_answer WHERE question_id=%s""", (question_id,))
                else:
                    cursor.execute("""SELECT * FROM quizzes_answer WHERE question_id=%s and is_correct=%s""",
                                   (question_id, is_correct))

                return self.fetchManyAsModel(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.answer.get_nb_correct_answers_for_questions.time')
    def getNbCorrectAnswersForQuestions(self, questionIdList):
        """
        gets a list of mapping question_id -> number of correct answers
        :param questionIdList:
        :return:
        """

        if not questionIdList:
            return dict()

        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT a.question_id, COUNT(b.id) FROM quizzes_question as a INNER JOIN """
                               """quizzes_answer as b ON b.is_correct=TRUE and b.question_id = a.question_id WHERE """
                               """a.question_id in %s GROUP BY a.question_id""", (tuple(questionIdList),))
                respDict = dict((resp[0], resp[1]) for resp in cursor.fetchall())

                return respDict

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.answer.clone_answers.time')
    def cloneAnswers(self, oldQIdList, newQIdList, userId, my_cursor):
        """

        :param oldQIdList:
        :param newQIdList:
        :param userId:
        :param my_cursor:
        :return:
        """

        try:
            dOldQ = dict((oldQIdList[i], newQIdList[i]) for i in range(len(oldQIdList)))

            with (my_cursor or self.connection.cursor()) as cursor:
                cursor.execute("""SELECT created_by_id, "text",is_correct, "order","question_id","legacy_id" FROM """
                               """quizzes_answer WHERE question_id IN %s""", (tuple(oldQIdList),))

                if cursor.rowcount == 0:
                    return

                answers = self.fetchManyAsDict(cursor)

                query = cursor.mogrify("""INSERT INTO quizzes_answer (created_by_id, "text", "is_correct", "order","""
                                       """"question_id", legacy_id) VALUES""")
                valueLst = list()
                for a in answers:
                    a[AnswerModel.QUESTION_ID] = dOldQ[a[AnswerModel.QUESTION_ID]]
                    a[AnswerModel.CREATED_BY_ID] = userId
                    v = cursor.mogrify(""" (%(created_by_id)s, %(text)s, %(is_correct)s,%(order)s,%(question_id)s,"""
                                       """%(legacy_id)s)""", a)
                    valueLst.append(v)

                query += ','.join(valueLst)
                cursor.execute(query)

                if cursor.rowcount <= 0:
                    raise self.NotInserted

                if not my_cursor:
                    self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.answer.create_answer.time')
    def createAnswer(self, dictModel):
        """

        :param dictModel:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                query = ",".join(["\"%s\"" % key for key in dictModel if key != AnswerModel.ID])
                values = ",".join(["%("+key+")s" for key in dictModel if key != AnswerModel.ID])
                values = cursor.mogrify(values, dictModel)

                # TODO check if we need all the values back
                cursor.execute("""INSERT INTO quizzes_answer (""" + query + """) VALUES (""" + values + """) RETURNING id""")

                if cursor.rowcount != 1:
                    raise self.NotInserted

                dictModel[AnswerModel.ID] = cursor.fetchone()[0]

                return dictModel

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.answer.load_answer_by_id.time')
    def loadAnswerById(self, answerId, auth_token=None, _type=BaseDao.TO_DICT):
        """

        :param answerId:
        :param auth_token:
        :param _type:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                if auth_token is None:
                    cursor.execute("""SELECT * FROM quizzes_answer WHERE id=%s""", (answerId,))
                else:
                    cursor.execute("""SELECT a.* FROM quizzes_answer AS a INNER JOIN socrative_users_socrativeuser """
                                   """as b ON a.id=%s and a.created_by_id=b.id and b.auth_token=%s""",
                                   (answerId, auth_token))

                if cursor.rowcount <= 0:
                    raise self.NotFound

                if cursor.rowcount != 1:
                    raise self.FoundTooMany

                if _type == BaseDao.TO_DICT:
                    return self.fetchOneAsDict(cursor)
                else:
                    return self.fetchOneAsModel(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.answer.delete_answer.time')
    def deleteAnswer(self, answerId):
        """

        :param answerId:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""DELETE FROM quizzes_answer WHERE id=%s""", (answerId,))

            self.connection.commit()
        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.answer.delete_answer_with_same_order.time')
    def deleteAnswerWithSameOrder(self, questionId):
        """

        :param questionId:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""DELETE FROM quizzes_answer WHERE id IN (SELECT a.id FROM quizzes_answer AS a INNER """
                               """JOIN quizzes_answer AS b ON a.question_id=b.question_id AND a."order"=b."order" AND"""
                               """ a.text=b.text AND a.id!=b.id AND a.is_correct=FALSE WHERE a.question_id=%s)""",
                               (questionId,))

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.answer.update_answer.time')
    def updateAnswer(self, answerDict):
        """

        :param answerDict:
        :return:
        """
        try:
            aId = safeToInt(answerDict[AnswerModel.ID])
            if not aId:
                error = 0
                raise ValueError("answer Id invalid")

            with self.connection.cursor() as cursor:
                query = ",".join(["\""+key+"\""+"=%("+key+")s" for key in answerDict if key != AnswerModel.ID])
                query = cursor.mogrify(query, answerDict)
                cursor.execute("""UPDATE quizzes_answer SET """ + query + """ WHERE id=%d""" % aId)

                if cursor.rowcount != 1:
                    raise self.NotUpdated

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.answer.load_answers_for_questions.time')
    def loadAnswersForQuestions(self, questionIds):
        """

        :param questionIds:
        :return:
        """
        error = None
        if not questionIds:
            return dict()
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * from quizzes_answer WHERE question_id IN %s""", (tuple(questionIds),))

                resp = self.fetchManyAsModel(cursor)

                respDict = dict()

                for r in resp:
                    if r.question_id in respDict:
                        respDict[r.question_id].append(r)
                    else:
                        respDict[r.question_id] = [r]
                return respDict
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.answer.get_answer_count.time')
    def getAnswerCount(self, question_ids):
        """
        count the answers for a specific id
        :param question_id:
        :return:
        """
        if not question_ids:
            return dict()
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT question_id, COUNT(id) FROM quizzes_answer WHERE question_id in %s GROUP BY"""
                               """ question_id""", (tuple(question_ids),))
                result = cursor.fetchall()
                aCount = dict((r[0], safeToInt(r[1]) or 0) for r in result)
                return aCount
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.answer.load_answer_by_order_from_question.time')
    def loadAnswerByOrderFromQuestion(self, questionId, order, _type=BaseDao.TO_MODEL):
        """

        :param questionId:
        :param order:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * from quizzes_answer WHERE question_id=%s and "order"=%s LIMIT 1"""
                               , (questionId, order))
                if _type == BaseDao.TO_DICT:
                    return self.fetchOneAsDict(cursor)
                else:
                    return self.fetchOneAsModel(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.answer.get_answer_ids.time')
    def getAnswerIds(self, questionId):
        """
        returns a list of answer ids corresponding to the question Id
        :param questionId:
        :type: int, long
        :return: list of answerIds
        :rtype: list
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT id from quizzes_answer WHERE question_id=%s""", (questionId,))

                resp = [a[0] for a in cursor.fetchall()]

                return resp

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.answer.delete_answers.time')
    def deleteAnswers(self, answerIds):
        """
        deletes the answers specified by their answer id
        :param answerIds:
        :type: int, long
        :return: list of answerIds
        :rtype: list
        """
        if not answerIds:
            return

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""DELETE FROM quizzes_answer WHERE id in %s""", (tuple(answerIds),))

            self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

