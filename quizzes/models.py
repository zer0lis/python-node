# -*- coding: utf-8 -*-
from django.db import models
from common.models import MediaResource
from socrative_users.models import SocrativeUser


class Quiz(models.Model):
    """
    quiz model to represent quizzes
    """

    class Meta:
        verbose_name_plural = "Quizzes"
    
    legacy_id = models.IntegerField(blank=True, null=True)

    # The display name of the quiz
    name = models.CharField(db_index=True, max_length=255, default="", blank=True)

    # SOC Number used for sharing and lookup of this Quiz
    soc_number = models.CharField(db_index=True, max_length=255, default="", blank=True)

    # Never deleted, just hidden
    is_hidden = models.BooleanField(default=False)

    # User that created this quiz
    created_by = models.ForeignKey(SocrativeUser, related_name="quizzes")

    # When this quiz was created
    created_date = models.DateTimeField(auto_now_add=True)
    
    # Last time this quiz was edited
    last_updated = models.DateTimeField(null=True, blank=True)

    # Is this quiz available to be shared
    sharable = models.BooleanField(default=True)

    def __unicode__(self):
        return u"Quiz: {0}".format(self.name)


class Question(models.Model):
    """
    Question model to represent the model in the database
    """
    MULTIPLE_CHOICE = "MC"
    FREE_RESPONSE = "FR"
    TRUE_FALSE = "TF"
    type_choices = (
        (MULTIPLE_CHOICE, u"Multiple Choice"),
        (FREE_RESPONSE, u"Free Response"),
        (TRUE_FALSE, u"True/False")
    )
    type = models.CharField(max_length=2, choices=type_choices, default=MULTIPLE_CHOICE)

    legacy_id = models.IntegerField(blank=True, null=True)

    question_id = models.AutoField(db_index=True, primary_key=True)

    # User that created this question
    created_by = models.ForeignKey(SocrativeUser, related_name="created_questions")

    # Sorting for questions within a Quiz
    order = models.IntegerField()

    # The actual question being asked
    question_text = models.TextField(default="", max_length=65536, blank=True)

    # Message shown after a correct or incorrect answer to justify it
    explanation = models.TextField(default="", max_length=65536, blank=True)
    
    # Images, audio clips and/or videos for this question
    resources = models.ManyToManyField(MediaResource, blank=True)
    
    # When this question was created
    created_date = models.DateTimeField(auto_now_add=True)

    # 0 = no weight, 1 = average weight
    grading_weight = models.DecimalField(max_digits=8, decimal_places=4, default=1.0)
    
    quiz = models.ForeignKey(Quiz, related_name="questions", blank=True, null=True)

    def __unicode__(self):
        return u"{0}) {1}".format(self.order, self.question_text)


class Answer(models.Model):
    """
    Answer model structure to be stored in the database
    """
    legacy_id = models.IntegerField(blank=True, null=True)

    # User that created this question
    created_by = models.ForeignKey(SocrativeUser, related_name="created_answers")

    text = models.TextField(default="", max_length=10240, blank=True)

    # When used in a MC Question, this marks a potential correct answer.
    # In a FR Question this means this question has been graded to be correct or not
    is_correct = models.NullBooleanField()

    # Sorting for answers in an MC Question
    order = models.IntegerField()
    
    question = models.ForeignKey(Question, related_name="answers")

    def __unicode__(self):
        correct_text = ""
        if self.is_correct:
            correct_text = "(Correct)"

        return u"{0}) {1} {2}".format(self.order, self.text, correct_text)


class Standard(models.Model):
    """
    Standards model structure
    """

    # User that created this question
    quiz = models.ForeignKey(Quiz, related_name="quiz_id", db_index=True)

    subject_id = models.IntegerField(null=False)

    core_id = models.IntegerField(null=False)

    grade_id = models.IntegerField(null=False)

    standard_id = models.IntegerField(null=False)

    name = models.CharField(max_length=128, null=False)

    description = models.CharField(max_length=5120, null=True)
