# coding=utf-8
import logging
import ujson

from common import http_status as status
from common.base_view import BaseView
from common.counters import DatadogThreadStats
from common.socrative_api import exceptionStack
from common.socrative_errors import BaseError
from quizzes.counters import QuizCounters
from quizzes.serializers import GetQuizRequestDTO, GetQuizBySocRequestDTO, CreateQuestionRequestDTO
from quizzes.serializers import UpdateQuestionRequestDTO, QuestionReorderRequestDTO, PurgeQuizzesRequestDTO
from quizzes.serializers import UpdateQuizRequestDTO, ImportQuizBySOCRequestDTO, ListQuizzesRequestDTO

logger = logging.getLogger(__name__)
statsd = DatadogThreadStats.STATS


class CreateQuizView(BaseView):
    """
    class that handle the create quiz service
    """
    http_method_names = ["post"]

    @statsd.timed(QuizCounters.CREATE_QUIZ)
    def post(self, request, *args, **kwargs):
        """
        handler for HTTP POST for create quiz service
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        failCounter = QuizCounters.CREATE_QUIZ_FAIL

        data = dict()

        self.getAuthToken(data)

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST, failCounter=failCounter)

        response = request.quizService.createQuiz(request, data)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class TeacherQuizAPIView(BaseView):
    """
    class that handles quiz update/delete and retrieve
    """

    http_method_names = ["get", "put"]

    @statsd.timed(QuizCounters.GET_QUIZ)
    def get(self, request, *args, **kwargs):
        """
        handler for
        """
        failCounter = QuizCounters.GET_QUIZ_FAIL

        data = dict()

        for k in request.GET:
            data[k] = request.GET.get(k)

        self.getAuthToken(data)
        self.getStudentUUID(data)

        data.update(kwargs)
        dto = GetQuizRequestDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.quizService.getQuiz(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)

    @statsd.timed(QuizCounters.UPDATE_QUIZ)
    def put(self, request, *args, **kwargs):
        """
        handler for update of the quiz
        """

        failCounter = QuizCounters.UPDATE_QUIZ_FAIL

        data = dict()
        self.getAuthToken(data)

        data.update(kwargs)
        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)
        data.update(kwargs)

        dto = UpdateQuizRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.quizService.updateQuiz(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class DeleteQuizzesView(BaseView):
    """
    deletes quizzes
    """

    http_method_names = ["post"]

    @statsd.timed(QuizCounters.DELETE_QUIZ)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        failCounter = QuizCounters.DELETE_QUIZ_FAIL

        data = dict()
        self.getAuthToken(data)

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            return self.render_json_response(BaseError.INTERNAL_SERVER_ERROR,
                                             statusCode=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                             failCounter=failCounter)

        dto = PurgeQuizzesRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.quizService.purgeQuizzes(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class CreateQuestionView(BaseView):
    """
    class that handles the create question api
    """
    http_method_names = ["post"]

    @statsd.timed(QuizCounters.CREATE_QUESTION)
    def post(self, request, *args, **kwargs):
        """
        handle for HTTP POST request for Create Question api
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = QuizCounters.CREATE_QUESTION_FAIL

        try:
            dataDict = ujson.loads(request.body)
        except Exception as e:
            logger.debug(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        self.getAuthToken(dataDict)

        dto = CreateQuestionRequestDTO.fromDict(dataDict)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.quizService.createQuestion(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class TeacherQuestionAPIView(BaseView):
    """
    class that handles get/delete/update on questions , by providing their  id and auth token
    """

    http_method_names = ["delete", "put"]

    @statsd.timed(QuizCounters.DELETE_QUESTION)
    def delete(self, request, *args, **kwargs):
        """
        handler for delete question by question id
        """
        failCounter = QuizCounters.DELETE_QUESTION_FAIL

        data = dict()
        data.update(kwargs)

        self.getAuthToken(data)

        # call the hide response service method
        response = request.quizService.deleteQuestion(request, data)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)

    @statsd.timed(QuizCounters.UPDATE_QUESTION)
    def put(self, request, *args, **kwargs):
        """
        handler for delete question by question id
        """

        failCounter = QuizCounters.UPDATE_QUESTION_FAIL

        # call the hide response service method
        data = dict()

        try:
            data = ujson.loads(request.body)
        except Exception as e:
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)
        data.update(kwargs)
        self.getAuthToken(data)

        dto = UpdateQuestionRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.quizService.updateQuestion(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class ListQuizzesView(BaseView):
    """
    class that handles the quiz by user api
    """

    http_method_names = ["get", "put"]

    @statsd.timed(QuizCounters.LIST_QUIZZES)
    def get(self, request, *args, **kwargs):
        """
        handle for HTTP GET for quiz by user api
        :param request:
        :return:
        """
        failCounter = QuizCounters.LIST_QUIZZES_FAIL

        # call the hide response service method
        dataDict = dict()

        self.getAuthToken(dataDict)

        for k in request.GET:
            dataDict[k] = request.GET.get(k)

        dto = ListQuizzesRequestDTO.fromDict(dataDict)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.quizService.listQuizzes(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class TeacherQuizBySocView(BaseView):
    """
    class that handles the teacher quiz by soc api
    """
    http_method_names = ["get"]

    @statsd.timed(QuizCounters.EDIT_QUIZ)
    def get(self, request, *args, **kwargs):
        """
        handle for HTTP GET for teacher quiz by soc api
        :param request:
        :return:
        """

        failCounter = QuizCounters.EDIT_QUIZ_FAIL

        data = dict()
        data.update(kwargs)
        self.getAuthToken(data)

        dto = GetQuizBySocRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.quizService.getQuizBySoc(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class ActivityReportView(BaseView):
    """
    class that handles
    """
    @statsd.timed(QuizCounters.GET_ACTIVITY_REPORT)
    def get(self, request, *args, **kwargs):
        """
        handle for HTTP GET for Activity report api
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = QuizCounters.GET_ACTIVITY_REPORT_FAIL

        # call the hide response service method
        data = dict()
        self.getAuthToken(data)

        for k in request.GET:
            data[k] = request.GET.get(k)

        response = request.quizService.activityReport(request, data)

        if type(response[0]) is dict and response[0].get("resp") is not None:
            return response[0]["resp"]
        else:
            return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class ImportBySocView(BaseView):
    """
    class that handles import by soc api
    """
    http_method_names = ["post"]

    @statsd.timed(QuizCounters.IMPORT_QUIZ_BY_SOC)
    def post(self, request, *args, **kwargs):
        """
        handle for HTTP GET for Import by Soc api
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        failCounter = QuizCounters.IMPORT_QUIZ_BY_SOC_FAIL

        # call the hide response service method
        data = dict()

        self.getAuthToken(data)

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        dto = ImportQuizBySOCRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.quizService.importQuizBySoc(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class DownloadReportsView(BaseView):
    """

    """

    http_method_names = ["get"]

    @statsd.timed(QuizCounters.DOWNLOAD_REPORT)
    def get(self, request, *args, **kwargs):
        """
        handle for HTTP GET for Import by Soc api
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        failCounter = QuizCounters.DOWNLOAD_REPORT_FAIL
        data = dict()
        for k in request.GET:
            data[k] = request.GET.get(k)
        data.update(kwargs)

        self.getAuthToken(data)

        response = request.quizService.downloadReports(request, data)

        if type(response) is tuple:
            return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)
        else:
            return response


class ImportQuizByExcelView(BaseView):
    """
    class to handle create quiz by excel
    """
    http_method_names = ["post"]

    @statsd.timed(QuizCounters.IMPORT_QUIZ_BY_EXCEL)
    def post(self, request, *args, **kwargs):
        """
        handle for HTTP GET for Import by Soc api
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        failCounter = QuizCounters.IMPORT_QUIZ_BY_EXCEL_FAIL

        data = dict()

        self.getAuthToken(data)

        data.update(request.GET)
        data.update(kwargs)
        response = request.quizService.importQuizByExcel(request, data)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class ShareQuizUrlView(BaseView):
    """
    class that returns the url with the encrypted quiz soc number to be shared
    """
    http_method_names = ["get"]

    @statsd.timed(QuizCounters.GET_QUIZ_SHARE_URL)
    def get(self, request, *args, **kwargs):
        """
        handler for GET HTTP for returning the quiz share url
        """

        failCounter = QuizCounters.GET_QUIZ_SHARE_URL_FAIL

        data = dict()
        data.update(kwargs)
        self.getAuthToken(data)

        response = request.quizService.shareQuiz(request, data)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class ShareQuizByMailView(BaseView):
    """
    class that sends a mail with the quiz share url
    """

    http_method_names = ["post"]

    @statsd.timed(QuizCounters.SHARE_QUIZ_BY_MAIL)
    def post(self, request, *args, **kwargs):
        """
        creates a post
        """

        failCounter = QuizCounters.SHARE_QUIZ_BY_MAIL_FAIL
        try:
            data = ujson.loads(request.body)
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        self.getAuthToken(data)

        response = request.quizService.shareQuizByEmail(request, data)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class QuestionReorderView(BaseView):
    """
    reorder questions
    """
    http_method_names = ["post"]

    @statsd.timed(QuizCounters.REORDER_QUESTIONS)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = QuizCounters.REORDER_QUESTIONS_FAIL

        try:
            data = ujson.loads(request.body)
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        self.getAuthToken(data)

        dto = QuestionReorderRequestDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.quizService.reorderQuestions(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)
