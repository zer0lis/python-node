# -*- coding: utf-8 -*-
from django.conf.urls import url

from quizzes.views import ListQuizzesView, TeacherQuizBySocView, CreateQuizView
from quizzes.views import TeacherQuizAPIView, TeacherQuestionAPIView, CreateQuestionView, DeleteQuizzesView
from quizzes.views import ActivityReportView, ImportBySocView
from quizzes.views import DownloadReportsView, ImportQuizByExcelView, ShareQuizByMailView
from quizzes.views import ShareQuizUrlView, QuestionReorderView

urlpatterns = [
    url(r'^api/quizzes/?$', ListQuizzesView.as_view()),
    url(r'^api/quizzes/purge/?$', DeleteQuizzesView.as_view()),
    url(r'^api/get-quiz-by-soc/(?P<soc_number>[0-9]+)/?$', TeacherQuizBySocView.as_view()),

    url(r'^api/quiz/?$', CreateQuizView.as_view()),
    url(r'^api/quiz/(?P<pk>[0-9]{1,15})/?$', TeacherQuizAPIView.as_view()),

    url(r'^api/questions/(?P<question_id>[0-9]+)/?$', TeacherQuestionAPIView.as_view()),
    url(r'^api/questions/?$', CreateQuestionView.as_view()),

    url(r'^api/activity-report/?$', ActivityReportView.as_view() ),
    url(r'^api/import/?$', ImportBySocView.as_view()),
    url(r'^api/reports/download/(?P<filename>.[a-zA-Z0-9=\-+_/]+)/?$', DownloadReportsView.as_view()),
    url(r'^api/import-excel/?$', ImportQuizByExcelView.as_view()),
    url(r'^api/share-quiz/(?P<soc_number>[0-9]+)/?$', ShareQuizUrlView.as_view()),
    url(r'^api/share-by-mail/', ShareQuizByMailView.as_view()),
    url(r'^api/question-reorder/', QuestionReorderView.as_view()),
]
