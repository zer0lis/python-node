# -*- coding: utf-8 -*-

import datetime
import io
import logging
import re
import ujson
import urllib
import zipfile
from cStringIO import StringIO
from datetime import timedelta

import pytz
import xlrd
import xlsxwriter
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.translation import ugettext
from lxml import html
from lxml.etree import ParserError
from xlrd import XLRDError

from common import http_status as status
from common.base_dao import BaseDao
from common.base_services import BaseService
from common.cryptography import Cryptography
from common.dao import ActivityInstanceModel
from common.dao import MediaResourceModel, PartnerModel
from common.socrative_api import exceptionStack, isStringNotEmpty, safeToInt, remove_control_chars, isEmailValid
from common.socrative_api import htmlEscape, isString, convert, safeToBool
from common.socrative_config import ConfigRegistry
from common.socrative_errors import BaseError
from quizzes.dao import QuestionModel, QuizModel, AnswerModel
from quizzes.pdf_generator import StudentPdfReport, WholeClassPdfReport, QuizPdfReport
from quizzes.serializers import QuizDTO
from quizzes.serializers import TeacherQuiz, QuestionDTO, QuizListDTO
from socrative_tornado.publish import send_model
from students.dao import StudentModel, StudentActivityModel
from workers.task_types import TaskType, ActionTypes, EmailTypes

FILENAME_FORBIDDEN_CHARS = dict((ord(char), None) for char in '<>:\"/\\|?*')
WORD_PATTERN = re.compile(r'[\W_]+', re.UNICODE)
IS_ALL_DIGITS = re.compile(r'^.[0-9]*$')
ESCAPED_HTML = re.compile(r'.*(&amp;|&gt;|&lt;|&emsp;|&quot;|&#x27;).*')

logger = logging.getLogger(__name__)

SPACE_RACE_TEAMS = {
    0: convert(ugettext("Blue")),
    1: convert(ugettext("Magenta")),
    2: convert(ugettext("Lime")),
    3: convert(ugettext("Peach")),
    4: convert(ugettext("Violet")),
    5: convert(ugettext("Teal")),
    6: convert(ugettext("Indigo")),
    7: convert(ugettext("Orange")),
    8: convert(ugettext("Red")),
    9: convert(ugettext("Silver")),
    10: convert(ugettext("Green")),
    11: convert(ugettext("Rose")),
    12: convert(ugettext("Gold")),
    13: convert(ugettext("Maroon")),
    14: convert(ugettext("Yellow")),
    15: convert(ugettext("Tan")),
    16: convert(ugettext("Hot Pink")),
    17: convert(ugettext("Sienna")),
    18: convert(ugettext("Dimgray")),
    19: convert(ugettext("Crimson"))
}


class FilenameHelper(object):
    """
    class to create and store filenames for reports
    """

    _basename = "socrative-report"

    def __init__(self, file_format, path=None, extra=None, basename=None, prepend=None):
        """
        Constructor
        :param file_format:
        :param path:
        :param extra:
        :param basename:
        :param prepend:
        :return:
        """
        self.file_format = file_format
        self.path = path
        self.extra = extra.encode('utf-8') if type(extra) is unicode else extra
        self.prepend = prepend.encode('utf-8') if type(prepend) is unicode else prepend

        if basename:
            self._basename = basename.encode('utf-8')

        if self.extra:
            self.prettier = "{}-{}.{}".format(self._basename, self.extra, self.file_format)
        else:
            self.prettier = "{}.{}".format(self._basename, self.file_format)

        if self.prepend:
            self.prettier = self.prepend + "/" + self.prettier

    def make_alt_fmt(self, file_format, prepend=None):
        """

        :param file_format:
        :param prepend:
        :return:
        """
        return self.__class__(file_format, self.path, self.extra, prepend=prepend)

    def changePDFFilename(self, **kwargs):
        """
        will alter the folder name for students and student file name
        :param kwargs:
        :return:
        """

        fileParams = kwargs.get("fileParams")

        self.prepend = "Students_" + fileParams[2].strftime("%m_%d_%Y__%H_%M_") + fileParams[1].encode('utf-8') + "_"

        fileParams[0] = fileParams[0].encode('utf-8') if type(fileParams[0]) is unicode else fileParams[0]
        fileParams[1] = fileParams[1].encode('utf-8') if type(fileParams[1]) is unicode else fileParams[1]

        self.prepend += fileParams[0][:70]
        if type(self.prepend) is unicode:
            self.prepend = self.prepend.translate(FILENAME_FORBIDDEN_CHARS)
        else:
            self.prepend = self.prepend.translate(None, '<>:\"/\\|?*')

        baseName = "Student_" + self.extra + "_" + fileParams[2].strftime("%m_%d_%Y__%H_%M") + "_"

        baseName += fileParams[0][:70]
        if type(baseName) is unicode:
            baseName = baseName.translate(FILENAME_FORBIDDEN_CHARS)
        else:
            baseName = baseName.translate(None, '<>:\"/\\|?*')

        self._basename = baseName
        self.prettier = self.prepend + "/" + baseName + ".pdf"

    @classmethod
    def from_report_settings(cls, report_types, download_method, file_path=None, *args, **kwargs):
        """
        static method
        :param report_types
        :param download_method
        :param file_path
        :param args
        :param kwargs
        """
        configs = {
            "download": {
                "whole-class": "pdf",
                "whole-class-excel": "xlsx",
                "all-students": "pdf",
                "unanswered": "pdf"
            },
            "email": {
                "whole-class": "pdf",
                "whole-class-excel": "xlsx",
                "all-students": "pdf",
                "unanswered": "pdf"
            },
            "print": {
                "whole-class": "html",
                "whole-class-excel": "xlsx",
                "all-students": "html",
                "unanswered": "html"
            }
        }

        num_reports = kwargs.get("rLen") or len(report_types)
        if num_reports > 1:
            if download_method in ["download", "email"] or "whole-class-excel" in report_types:
                file_format = "zip"
                fileParams = kwargs.get("fileParams")

                baseName = "Socrative_" + fileParams[2].strftime("%m_%d_%Y__%H_%M_") + fileParams[1]+"_"

                if type(baseName) is unicode or type(fileParams[0]) is unicode:
                    baseName += fileParams[0][:70]
                    baseName = baseName.translate(FILENAME_FORBIDDEN_CHARS)
                else:
                    baseName += fileParams[0][:70]
                    baseName = baseName.translate(None, '<>:\"/\\|?*')

                kwargs["basename"] = baseName
                kwargs.pop("fileParams")
            else:
                file_format = "html"
        elif num_reports == 1:
            file_format = configs[download_method][report_types[0]]

            if kwargs.get("tLen") >= 1:
                fileParams = kwargs.get("fileParams")

                if "whole-class-excel" in report_types:
                    baseName = "Class_" + fileParams[2].strftime("%m_%d_%Y__%H_%M_") + fileParams[1] + "_"

                    if type(baseName) is unicode or type(fileParams[0]) is unicode:
                        baseName += fileParams[0][:70]
                        baseName = baseName.translate(FILENAME_FORBIDDEN_CHARS)
                    else:
                        baseName += fileParams[0][:70]
                        baseName = baseName.translate(None, '<>:\"/\\|?*')

                    kwargs["basename"] = baseName
                elif "whole-class" in report_types:
                    baseName = "Question_" + fileParams[2].strftime("%m_%d_%Y__%H_%M_") + fileParams[1]+"_"

                    if type(baseName) is unicode or type(fileParams[0]) is unicode:
                        baseName += fileParams[0][:70]
                        baseName = baseName.translate(FILENAME_FORBIDDEN_CHARS)
                    else:
                        baseName += fileParams[0][:70]
                        baseName = baseName.translate(None, '<>:\"/\\|?*')
                    kwargs["basename"] = baseName
                elif "unanswered" in report_types:
                    baseName = "Quiz_"

                    if type(baseName) is unicode or type(fileParams[0]) is unicode:
                        baseName += fileParams[0][:70]
                        baseName = baseName.translate(FILENAME_FORBIDDEN_CHARS)
                    else:
                        baseName += fileParams[0][:70]
                        baseName = baseName.translate(None, '<>:\"/\\|?*')
                    kwargs["basename"] = baseName
        else:
            raise Exception

        if "rLen" in kwargs:
            kwargs.pop("rLen")

        if "tLen" in kwargs:
            kwargs.pop("tLen")

        if "fileParams" in kwargs:
            kwargs.pop("fileParams")

        return cls(file_format, path=file_path, *args, **kwargs)


class QuizzesServices(BaseService):
    """
    class that holds methods for quiz services
    """

    REPORT_LIMIT = 1000


    def __init__(self, daoRegistry, cacheRegistry):
        """
        Constructor
        :return:
        """
        super(QuizzesServices, self).__init__(daoRegistry, cacheRegistry)

    def createQuiz(self, request, dataDict):
        """
        create quiz service
        :param request
        :param dataDict
        """

        user_uuid = dataDict.get('auth_token')

        if not isStringNotEmpty(user_uuid):
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

        try:
            update = False

            user = self.serviceRegistry.userService.validateUser(user_uuid)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            quizList = self.daoRegistry.quizDao.getQuizzes(user.id, last_updated=False, is_hidden=False,
                                                           order=BaseDao.DESCENDING, sort_by=BaseDao.CREATED_DATE,
                                                           limit=1)
            quiz = quizList[0] if quizList else None
            if quiz is None:
                quiz = QuizModel()
                quiz.id = self.daoRegistry.quizDao.createQuiz(userId=user.id,
                                                              name=(dataDict.get("name", "").strip() or ugettext("Untitled quiz")))
            else:
                activity = self.daoRegistry.activityInstanceDao.countActivities(activity_id=quiz.id, started_by=user.id)
                if activity > 0:
                    self.daoRegistry.quizDao.hideQuiz(quiz.id, True)
                    quiz = self.daoRegistry.quizDao.cloneQuiz(quiz, user.id, hideCloning=True)
                    update = False
                else:
                    update = True

            responseStatus = status.HTTP_200_OK if update else status.HTTP_201_CREATED

            respDict = self.daoRegistry.quizDao.loadDeepQuiz(quiz_id=quiz.id)

            return TeacherQuiz.fromSocrativeDict(respDict[0]), responseStatus

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def validateQuiz(self, quizId, auth_token):
        """

        :param quizId:
        :return:
        """
        resp = self.cacheRegistry.quizCache.get(quizId, auth_token is None)
        if resp:
            return ujson.loads(resp)

        try:
            quiz = self.daoRegistry.quizDao.loadDeepQuiz(quizId, auth_token=auth_token)

            if len(quiz) == 0:
                return None

            teacher_data = TeacherQuiz.fromSocrativeDict(quiz[0])
            student_data = QuizDTO.fromSocrativeDict(quiz[0])
            correctAnswersDict = self.daoRegistry.quizDao.loadCorrectAnswers(quizId)

            for data in [teacher_data, student_data]:
                for q in data.get("questions", []):
                    if q["type"] in [QuestionModel.TRUE_FALSE, QuestionModel.MULTIPLE_CHOICE]:
                        q["total_correct_answers"] = 0
                    if q["question_id"] in correctAnswersDict:
                        q["total_correct_answers"] = correctAnswersDict[q["question_id"]]
                    if id(data) == id(student_data) and q["type"] == QuestionModel.FREE_RESPONSE:
                        q["answers"] = []
                data["questions"] = sorted(data.get("questions", []), key=lambda x: x["order"])

            self.cacheRegistry.quizCache.cacheData(quizId, ujson.dumps(student_data), True)
            self.cacheRegistry.quizCache.cacheData(quizId, ujson.dumps(teacher_data), False)

            if auth_token:
                return teacher_data
            else:
                return student_data

        except Exception as e:
            logger.error(exceptionStack(e))
            return None

    def getQuiz(self, request, dto):
        """

        :param request:
        :param dto:
        """

        if dto.auth_token:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

        if dto.auth_token is None and dto.user_uuid is None:
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

        resp = self.validateQuiz(dto.pk, dto.auth_token)
        if resp is None:
            return BaseError.QUIZ_NOT_FOUND, status.HTTP_400_BAD_REQUEST

        if dto.auth_token is None:
            if dto.room_name is None:
                return BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST

            aid, room_name, rostered = self.daoRegistry.activityInstanceDao.getActivityDataForQuiz(dto.pk,
                                                                                                   resp["created_by"],
                                                                                                   dto.room_name
                                                                                                   )
            if aid is not None and rostered is True:
                try:
                    student_id, _ = self.daoRegistry.studentActivityDao.updateStatus(dto.user_uuid, aid,
                                                                                  StudentActivityModel.LOGGED_IN,
                                                                                  dto.room_name)
                    self.daoRegistry.studentDao.updateStatus(dto.user_uuid, room_name, StudentModel.LOGGED_IN)

                    send_model(room_name + '-teacher', key="studentStartedQuiz", data={"id": student_id,
                                                                                       "room": room_name})

                except BaseDao.NotFound:
                    return BaseError.STUDENT_NOT_FOUND, status.HTTP_400_BAD_REQUEST
                except Exception as e:
                    logger.error(exceptionStack(e))
                    return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

        return resp, status.HTTP_200_OK

    def __check_quiz(self, quizDict, errors):
        """

        :param quizDict:
        :param errors:
        :return:
        """

        name = quizDict.get("name", ugettext("Untitled_quiz"))
        soc_number = quizDict.get("soc_number")
        is_hidden = quizDict.get("is_hidden", False)
        sharable = quizDict.get("is_sharable", True)

        if not isStringNotEmpty(name):
            errors.append("Invalid quiz")
            return False

        if soc_number is not None and not isStringNotEmpty(soc_number):
            errors.append("invalid soc_number")
            return False

        if type(is_hidden) is not bool:
            errors.append("Invalid is_hidden field")
            return False

        if type(sharable) is not bool:
            errors.append("Invalid is_sharable field")
            return False

        return True

    def updateQuiz(self, request, dtoObj):
        """
        """
        try:

            quiz = self.daoRegistry.quizDao.loadQuizById(dtoObj.pk, dtoObj.auth_token, isHidden=False,
                                                         _type=BaseDao.TO_MODEL)

            dtoObj.user_id = quiz.created_by_id
            dtoObj.soc_number = quiz.soc_number

            if dtoObj.create is False:
                model = QuizModel.fromRequestDto(dtoObj).toDict()

                if quiz.name == "":
                    quiz.name = ugettext("Untitled quiz")

                oldModel = quiz.toDict()
                keysToRemove = [k for k in model if k in oldModel and oldModel[k] == model[k] or model[k] is None]
                for k in keysToRemove:
                    if k != QuizModel.ID:
                        model.pop(k)

                model[QuizModel.ID] = quiz.id
                if QuizModel.CREATED_DATE in model and type(model[QuizModel.CREATED_DATE]) in (int, long):
                    model[QuizModel.CREATED_DATE] = datetime.datetime.fromtimestamp(model[QuizModel.CREATED_DATE])

                d = datetime.datetime.utcnow()
                model[QuizModel.LAST_UPDATED] = pytz.utc.localize(d)
                quizDict = model

                if len(model) > 2 or QuizModel.LAST_UPDATED in model and QuizModel.ID in model:
                    self.daoRegistry.quizDao.updateQuiz(quizDict)

                self.daoRegistry.standardsDao.deleteStandard(quiz.id)

                if dtoObj.standard:
                    self.daoRegistry.standardsDao.addStandard(dtoObj.standard, quiz.id)

                self.cacheRegistry.quizCache.invalidateData(quiz.id)
                return {}, status.HTTP_200_OK

            # load the activity instance and check it's ended
            activityInstance = self.daoRegistry.activityInstanceDao.loadActivityByQuizId(quiz_id=quiz.id,
                                                                                         started_by_id=quiz.created_by_id,
                                                                                         limit=1,
                                                                                         _type=BaseDao.TO_MODEL)
            if activityInstance is not None and activityInstance.end_time is None:
                return BaseError.QUIZ_IS_RUNNING, status.HTTP_403_FORBIDDEN

            if activityInstance is not None:
                return BaseError.QUIZ_HAS_BEEN_RUN, status.HTTP_400_BAD_REQUEST

            # added a check in case the client sends int for datetime
            dtoObj.last_updated = datetime.datetime.utcnow()
            if dtoObj.name == "":
                dtoObj.name = ugettext("Untitled quiz")

            model = QuizModel.fromRequestDto(dtoObj).toDict()
            oldModel = quiz.toDict()
            keysToRemove = [k for k in model if k in oldModel and type(oldModel[k]) is datetime and oldModel[k] == model[k] or model[k] is None]

            if QuizModel.IS_HIDDEN in keysToRemove and QuizModel.IS_HIDDEN not in model:
                keysToRemove.remove(QuizModel.IS_HIDDEN)

            for k in keysToRemove:
                if k != QuizModel.ID:
                    model.pop(k)

            model[QuizModel.ID] = quiz.id

            self.daoRegistry.standardsDao.deleteStandard(quiz.id)

            if dtoObj.standard is not None:
                self.daoRegistry.standardsDao.addStandard(dtoObj.standard, quiz.id)

            # if nothing changed but last update then don't update the quiz model in the db
            if len(model) == 2 and QuizModel.LAST_UPDATED in model and QuizModel.ID in model:
                return {}, status.HTTP_200_OK

            self.daoRegistry.quizDao.updateQuiz(model)
            self.cacheRegistry.quizCache.invalidateData(quiz.id)

            return {}, status.HTTP_200_OK

        except BaseDao.NotFound:
            return BaseError.QUIZ_NOT_FOUND, status.HTTP_404_NOT_FOUND
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def createQuestion(self, request, dto):
        """
        :param request
        :param dto
        """

        try:

            errors = list()
            self._check_question(dto, errors)
            if len(errors) > 0:
                return errors, status.HTTP_400_BAD_REQUEST

            quiz = self.daoRegistry.quizDao.loadQuizById(dto.quiz_id, dto.auth_token, _type=BaseDao.TO_MODEL)

            activity = self.daoRegistry.activityInstanceDao.loadActivityByQuizId(quiz_id=dto.quiz_id,
                                                                                 started_by_id=quiz.created_by_id,
                                                                                 limit=1,
                                                                                 _type=BaseDao.TO_MODEL)

            if activity is not None and activity.end_time is None:
                return BaseError.QUIZ_IS_RUNNING, status.HTTP_403_FORBIDDEN
            if activity is not None:
                return BaseError.QUIZ_HAS_BEEN_RUN, status.HTTP_400_BAD_REQUEST


            # make sure weird fields are discarded and only valid fields remain
            model = QuestionModel.fromRequestDto(dto)

            exists, qId, new_order = self.daoRegistry.questionDao.checkExistingQuestion(quiz.id, model.order,
                                                                                        model.type, model.question_text)

            # if the question with the order exists already but is not a duplicate ,
            #  we get a new order to add the question
            # else we get the old id from the database and return 201 with the old data.
            if dto.duplicate is False:
                if exists and new_order is not None:
                    model.order = new_order
                elif exists is True:
                    model.question_id = qId
                    return QuestionDTO.fromSocrativeDict(model.toDict()), status.HTTP_201_CREATED
            else:
                if exists and new_order is not None:
                    model.order = new_order

            model.question_text = model.question_text if model.question_text is not None else ""
            model.explanation = model.explanation if model.explanation is not None else ""
            model = model.toDict()
            # this is the case when exists is false

            if QuestionModel.QUESTION_ID in model:
                model.pop(QuestionModel.QUESTION_ID)

            model[QuestionModel.CREATED_BY_ID] = quiz.created_by_id

            modelDict = self.daoRegistry.questionDao.createQuestion(model)
            answers = list()
            for a in dto.answers:
                answerModel = AnswerModel.fromRequestDto(a)
                answerModel.created_by_id = quiz.created_by_id
                answerModel.question_id = modelDict[QuestionModel.QUESTION_ID]
                dictModel = self.daoRegistry.answerDao.createAnswer(answerModel.toDict())
                answers.append({"id": dictModel[AnswerModel.ID], "order": dictModel[AnswerModel.ORDER]})

            if len(dto.resources) > 0:
                mediaId = self.daoRegistry.mediaResourceDao.createResource(owner=quiz.created_by_id,
                                                                           name=dto.resources[0].name,
                                                                           media_type=MediaResourceModel.IMAGE,
                                                                           url=dto.resources[0].url)

                request.tasksClient.add_tasks(TaskType.IMAGE_RESIZE, [{"url": dto.resources[0].url,
                                                                      "id": mediaId,
                                                                      "formats": "*"}])

                self.daoRegistry.questionDao.addResource(model[QuestionModel.QUESTION_ID], mediaId)

            # update the quiz
            d = datetime.datetime.utcnow()
            d = pytz.utc.localize(d)
            self.daoRegistry.quizDao.updateQuiz({QuizModel.ID: quiz.id, QuizModel.LAST_UPDATED: d})

            mDict = self.daoRegistry.questionDao.deleteByOrder(dto.order, quiz.id)
            if mDict is not None:
                mDict["answers"] = answers
                return QuestionDTO.fromSocrativeDict(mDict), status.HTTP_201_CREATED

            modelDict["answers"] = answers

            self.cacheRegistry.quizCache.invalidateData(quiz.id)

            return QuestionDTO.fromSocrativeDict(modelDict), status.HTTP_201_CREATED

        except BaseDao.NotFound:
            return BaseError.QUIZ_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def deleteQuestion(self, request, dataDict):
        """
        :param request
        :param dataDict
        """
        question_id = dataDict.get("question_id")
        auth_token = dataDict.get("auth_token")

        if not isStringNotEmpty(question_id):
            return BaseError.QUESTION_ID_MISSING, status.HTTP_400_BAD_REQUEST

        if not isStringNotEmpty(auth_token):
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

        try:
            # TODO reduce the number of calls to db
            question = self.daoRegistry.questionDao.loadQuestionById(question_id, auth_token, _type=BaseDao.TO_MODEL)

            activity = self.daoRegistry.activityInstanceDao.loadActivityByQuizId(quiz_id=question.quiz_id,
                                                                                 started_by_id=question.created_by_id,
                                                                                 limit=1,
                                                                                 _type=BaseDao.TO_MODEL)

            if activity is not None and activity.end_time is None:
                return BaseError.QUIZ_IS_RUNNING, status.HTTP_403_FORBIDDEN

            if activity is not None:
                return BaseError.QUIZ_HAS_BEEN_RUN, status.HTTP_400_BAD_REQUEST

            self.daoRegistry.questionDao.deleteQuestion(question_id)

            self.daoRegistry.questionDao.updateOrder(question.quiz_id, question.order)

            resp = self.daoRegistry.questionDao.loadQuestionOrder(question.quiz_id)

            #update the quiz
            d = datetime.datetime.utcnow()
            d = pytz.utc.localize(d)
            self.daoRegistry.quizDao.updateQuiz({QuizModel.ID: question.quiz_id, QuizModel.LAST_UPDATED: d})

            self.cacheRegistry.quizCache.invalidateData(question.quiz_id)

            return resp, status.HTTP_200_OK

        except BaseDao.NotFound:
            return BaseError.QUESTION_NOT_FOUND, status.HTTP_404_NOT_FOUND
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def updateQuestion(self, request, dto):
        """
        :param request
        :param dto
        """

        try:
            errors = list()
            self._check_question(dto, errors)
            if len(errors) > 0:
                return errors, status.HTTP_400_BAD_REQUEST

            question = self.daoRegistry.questionDao.loadQuestionById(dto.question_id, dto.auth_token,
                                                                     _type=BaseDao.TO_MODEL)

            activity = self.daoRegistry.activityInstanceDao.loadActivityByQuizId(quiz_id=question.quiz_id,
                                                                                 started_by_id=question.created_by_id,
                                                                                 limit=1,
                                                                                 _type=BaseDao.TO_MODEL)

            if activity is not None and activity.end_time is None:
                return BaseError.QUIZ_IS_RUNNING, status.HTTP_400_BAD_REQUEST

            if activity is not None:
                return BaseError.QUIZ_HAS_BEEN_RUN, status.HTTP_400_BAD_REQUEST

            existingAnswerIds = self.daoRegistry.answerDao.getAnswerIds(question.question_id)
            requestAnswerIds = [a.id for a in dto.answers if a.id is not None]

            # check for foreign answer ids
            diff = set(requestAnswerIds) - set(existingAnswerIds)
            if len(diff) > 0:
                return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

            model = QuestionModel.fromRequestDto(dto).toDict()
            oldModel = question.toDict()
            keysToRemove = [k for k in model if k in oldModel and oldModel[k] == model[k] or model[k] is None]
            for k in keysToRemove:
                if k != QuestionModel.QUESTION_ID:
                    model.pop(k)

            if QuestionModel.QUESTION_ID not in model:
                model[QuestionModel.QUESTION_ID] = question.question_id

            # if nothing changed but last update then don't update the quiz model in the db
            if len(model) == 1 and QuestionModel.QUESTION_ID in model:
                return {}, status.HTTP_200_OK

            self.daoRegistry.questionDao.updateQuestion(model)

            # update the quiz
            d = datetime.datetime.utcnow()
            d = pytz.utc.localize(d)

            deleteAnswerIds = list(set(existingAnswerIds) - set(requestAnswerIds))

            self.daoRegistry.answerDao.deleteAnswers(deleteAnswerIds)

            answers = list()
            for a in dto.answers:
                a = AnswerModel.fromRequestDto(a)
                a.question_id = question.question_id
                a.created_by_id = question.created_by_id
                if a.id is not None:
                    self.daoRegistry.answerDao.updateAnswer(a.toDict())
                    answers.append({"id": a.id, "order": a.order})
                else:
                    modelDict = self.daoRegistry.answerDao.createAnswer(a.toDict())
                    answers.append({"id": modelDict[AnswerModel.ID], "order": a.order})

            if len(dto.resources) == 0:
                self.daoRegistry.questionDao.deleteAllResources(question.question_id)
            else:
                existingResourceDict = self.daoRegistry.questionDao.loadResourceIds(question.question_id)
                if dto.resources[0].url not in existingResourceDict:
                    self.daoRegistry.questionDao.deleteAllResources(question.question_id)
                    mediaId = self.daoRegistry.mediaResourceDao.createResource(owner=question.created_by_id,
                                                                               name=dto.resources[0].name,
                                                                               media_type=MediaResourceModel.IMAGE,
                                                                               url=dto.resources[0].url)

                    request.tasksClient.add_tasks(
                        TaskType.IMAGE_RESIZE,
                        [
                            {
                                "url": dto.resources[0].url,
                                "id": mediaId,
                                "formats": "*"
                            }
                        ]
                    )

                    self.daoRegistry.questionDao.addResource(question.question_id, mediaId)

            self.daoRegistry.quizDao.updateQuiz({QuizModel.ID: question.quiz_id, QuizModel.LAST_UPDATED: d})

            self.cacheRegistry.quizCache.invalidateData(question.quiz_id)

            oldModel.update(model)
            oldModel["answers"] = answers

            return QuestionDTO.fromSocrativeDict(oldModel), status.HTTP_200_OK

        except BaseDao.NotFound:
            return BaseError.QUESTION_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def importQuizBySoc(self, request, dto):
        """
        :param request
        :param dto
        """
        try:

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if dto.soc_number.startswith("SN_"):
                dto.soc_number = Cryptography.decrypt(dto.soc_number)

            quizList = self.daoRegistry.quizDao.getQuizzesFromSoc(soc_number=dto.soc_number,
                                                                  sharable=True,
                                                                  is_hidden=False,
                                                                  orderBy=BaseDao.UPDATED_DATE,
                                                                  sortOrder=BaseDao.DESCENDING,
                                                                  limit=1,
                                                                  _type=BaseDao.TO_MODEL)

            quiz = quizList[0] if quizList else None

            if quiz is None:
                return BaseError.QUIZ_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            self.daoRegistry.quizDao.cloneQuiz(quiz, user.id, addStandards=True)

            return {}, status.HTTP_201_CREATED

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def listQuizzes(self, request, dto):
        """
        :param request
        :param dto
        """

        try:

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            quizzes = self.daoRegistry.quizDao.getActiveQuizList(user.id)

            resp = [QuizListDTO.fromSocrativeDict(quiz) for quiz in quizzes]

            return {"quizzes": resp}, status.HTTP_200_OK
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def purgeQuizzes(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            blocked_ids = self.daoRegistry.activityInstanceDao.getRunningQuizIds(user.id, dto.ids)

            if blocked_ids:
                error = BaseError.QUIZZES_RUNNING
                error["error"]["ids"] = blocked_ids
                return error, status.HTTP_400_BAD_REQUEST

            self.daoRegistry.quizDao.hideQuizzes(user.id, dto.ids)

            self.cacheRegistry.quizCache.invalidateMulti(dto.ids)

            return {}, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def getQuizBySoc(self, request, dto):
        """
        :param request:
        :param dto:
        """
        try:

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            quiz = self.daoRegistry.quizDao.loadQuizBySoc(dto.soc_number, dto.auth_token, is_hidden=False,
                                                          _type=BaseDao.TO_MODEL)

            # check if it is already used and has a report -> activity_id == quiz_id
            is_used = self.daoRegistry.activityInstanceDao.countActivities(activity_id=quiz.id,
                                                                           started_by=user.id) > 0

            if is_used:
                self.cacheRegistry.quizCache.invalidateData(quiz.id)
                quiz = self.daoRegistry.quizDao.cloneQuiz(quiz, user.id, hideCloning=True, addStandards=True)
            else:
                qz = self.cacheRegistry.quizCache.get(quiz.id, False)
                if qz is not None:
                    return ujson.loads(qz), status.HTTP_200_OK

            quiz = self.daoRegistry.quizDao.loadDeepQuiz(quiz_id=quiz.id)

            data = TeacherQuiz.fromSocrativeDict(quiz[0])
            return data, status.HTTP_200_OK

        except BaseDao.NotFound:
            return BaseError.QUIZ_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def _check_question(self, dto, errors):
        """
        :param errors: list
        :param dto: object
        """

        minAnswers = 0 if dto.type == QuestionModel.FREE_RESPONSE else 2

        if len(dto.answers) < minAnswers:
            errors.append("There are too few answers in the question")
            return

        if len(dto.answers) > 1:
            tmp = set([a.order for a in dto.answers])
            if len(dto.answers) != len(tmp):
                errors.append("Duplicate orders in the answer list")
                return

        if dto.order < 0:
            errors.append("Invalid question order")
            return

        if dto.grading_weight is None:
            errors.append("Invalid grading weight")
            return

        if not isStringNotEmpty(dto.question_text):
            errors.append("Invalid question text")
            return

        if not isString(dto.explanation):
            errors.append("Invalid explanation value")
            return

        for a in dto.answers:
            if not isStringNotEmpty(a.text):
                errors.append("invalid answer text")
                return

            if a.is_correct is not None and type(a.is_correct) is not bool:
                errors.append("invalid is_correct value")
                return

            if a.order < 0:
                errors.append("invalid answer order value")
                return

    def whole_class_excel_report(self, request, quiz, responses, questions, names, local_time, activity):
        """

        :param request:
        :param quiz:
        :param responses:
        :param questions:
        :param names:
        :param local_time:
        :param activity
        :return:
        """

        activity_instance = activity

        if activity and activity_instance.activity_type in (ActivityInstanceModel.SINGLE_QUESTION,
                                                            ActivityInstanceModel.SINGLE_QUESTION_VOTING):
            output = StringIO()
            ret = self.free_response_report_file(output, quiz, activity_instance, request.user, request)
            if ret is not None:
                return ret
            output.seek(0)
            return output

        # Variables used in tallying responses
        order_mapping = {}
        correct_list = []
        current_row = 6
        total_correct = 0
        total_gradable = 0
        student_count = 0
        question_count = len(questions)

        output = StringIO()
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet()

        # Formatting options
        basic_formatting = workbook.add_format({'font_name': 'Arial', 'font_size': 16})
        bold_left = workbook.add_format({'bold': True, 'font_name': 'Arial', 'font_size': 16})
        plain_centered = workbook.add_format({'align': 'center', 'font_name': 'Arial', 'font_size': 16})
        blue_header = workbook.add_format({'bg_color': '#89BFFB', 'text_wrap': True, 'align': 'center',
                                           'font_name': 'Arial', 'font_size': 16})
        grey_left = workbook.add_format({'bg_color': '#B1B1B1', 'border': 2, 'font_name': 'Arial', 'font_size': 16})
        grey_percentage = workbook.add_format({'bg_color': '#B1B1B1', 'border': 2, 'align': 'center',
                                               'font_name': 'Arial', 'font_size': 16, 'num_format': '0.0%'})
        grey_decimal = workbook.add_format({'bg_color': '#B1B1B1', 'border': 2, 'align': 'center',
                                            'font_name': 'Arial', 'font_size': 16, 'num_format': '0.00'})
        right_answer = workbook.add_format({'bg_color': '#C0FAC1', 'font_name': 'Arial', 'font_size': 16,
                                            'text_wrap': True})
        wrong_answer = workbook.add_format({'bg_color': '#F3BFC0', 'font_name': 'Arial', 'font_size': 16,
                                            'text_wrap': True})
        null_answer = workbook.add_format({'font_name': 'Arial', 'font_size': 16, 'text_wrap': True})

        # COLORS FOR SPACE RACE
        teamCellFormat = {
            0: workbook.add_format({'bg_color': '#276baa', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            1: workbook.add_format({'bg_color': '#ca2692', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            2: workbook.add_format({'bg_color': '#a2d35d', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            3: workbook.add_format({'bg_color': '#fcbc89', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            4: workbook.add_format({'bg_color': '#7d3990', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            5: workbook.add_format({'bg_color': '#258a8c', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            6: workbook.add_format({'bg_color': '#5f82c2', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            7: workbook.add_format({'bg_color': '#d76d2c', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            8: workbook.add_format({'bg_color': '#b83b24', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            9: workbook.add_format({'bg_color': '#5c646e', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                    'font_size': 16}),
            10: workbook.add_format({'bg_color': '#4fa45e', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            11: workbook.add_format({'bg_color': '#ec7c90', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            12: workbook.add_format({'bg_color': '#ba8324', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            13: workbook.add_format({'bg_color': '#9d3444', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            14: workbook.add_format({'bg_color': '#fbdb45', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            15: workbook.add_format({'bg_color': '#D2B48C', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            16: workbook.add_format({'bg_color': '#e602e7', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            17: workbook.add_format({'bg_color': '#e18942', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            18: workbook.add_format({'bg_color': '#696969', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16}),
            19: workbook.add_format({'bg_color': '#990000', 'text_wrap': True, 'align': 'center', 'font_name': 'Arial',
                                     'font_size': 16})
        }

        # Set column widths and default formatting
        worksheet.set_column(0, 2 + question_count, 30, basic_formatting)
        worksheet.set_column(1, 2, 15)
        # Write main header
        if IS_ALL_DIGITS.match(quiz.name[:30]):
            text = quiz.name[:30].strip()
            worksheet.write(0, 0, """=concatenate("%s","")""" %
                                  html.fromstring(text).text_content() if text != "" else "",
                            bold_left)
        else:
            text = quiz.name[:30].strip()
            worksheet.write(0, 0, html.fromstring(text).text_content() if text != "" else "", bold_left)
        worksheet.write(1, 0, local_time.strftime("%A, %B %d %Y %I:%M %p"))
        if request.room_name is None:
            request.room_name = activity_instance.room_name
            roomName = activity_instance.room_name
        else:
            roomName = request.room_name

        worksheet.write(2, 0, convert(ugettext("Room:")) + " " + roomName)

        standardName = self.daoRegistry.standardsDao.getStandardName(quiz.id)
        if standardName:
            # Write common core standards
            worksheet.write(4, 0, convert(ugettext("Standard:")), bold_left)
            worksheet.write(4, 1, standardName, basic_formatting)

        require_names = safeToBool(self.__getActivitySettings(activity_instance.id).get("require_names", False))

        col = 0

        studentIDDict = dict()
        studentsDict = dict()
        presenceCol = None

        if require_names:

            studentsList = self.daoRegistry.studentActivityDao.getPresence(activity_instance.id)
            studentsDict = dict()
            user_uuid = 0
            for student in studentsList:
                if student.get("user_uuid"):
                    studentsDict[student.get("user_uuid")] = [student.get("name"),
                                                              student.get("student_id"),
                                                              student.get("present") or False
                                                              ]
                else:
                    studentsDict[user_uuid] = [student.get("name"),
                                               student.get("student_id"),
                                               student.get("present") or False
                                               ]
                    user_uuid += 1

            if studentsDict:
                worksheet.write(current_row, col, convert(ugettext('Presence')), blue_header)
                col += 1
                presenceCol = 0
            else:
                presenceCol = None

        # Write column headings

        isSR = False
        teamCol = 0
        totalScoreCol = 0
        nbCorrectCol = 0

        if presenceCol is not None:
            worksheet.write(current_row-1, 0, convert(ugettext("Column calculations only reflect present students")))
        if activity_instance.activity_type == activity_instance.SPACE_RACE:
            worksheet.write(current_row, col, convert(ugettext("Team")), blue_header)
            teamCol = col
            col += 1
            isSR = True

        worksheet.write(current_row, col, convert(ugettext("Student Names")), blue_header)
        nameCol = col
        col += 1

        worksheet.write(current_row, col, convert(ugettext('Student ID')), blue_header)
        studentIDDict = self.daoRegistry.studentDao.getStudentIDs(activity_instance.id)
        studentIdColumn = col
        col += 1

        worksheet.write(current_row, col, convert(ugettext("Total Score (0 - 100)")), blue_header)
        totalScoreCol = col
        col += 1
        worksheet.write(current_row, col, convert(ugettext("Number of correct answers")), blue_header)
        nbCorrectCol = col
        q_n = col + 1
        questionCol = q_n

        # Write question headers and record number of gradable responses
        # qCADict - question:number of correct answers dictionary
        qids = [q.question_id for q in questions]
        if request.qCADict is None:
            qCADict = self.daoRegistry.answerDao.getNbCorrectAnswersForQuestions(qids)
            request.qCADict = qCADict
        else:
            qCADict = request.qCADict

        answerCountDict = self.daoRegistry.answerDao.getAnswerCount(qids)
        for question in questions:
            try:
                text = html.fromstring(question.question_text).text_content() if question.question_text.strip() != "" else ""
            except ParserError:
                text = question.question_text
            except ValueError as e:
                text = question.question_text

            worksheet.write(current_row, q_n, text, blue_header)
            order_mapping[question.question_id] = q_n
            correct_list.append(0)
            q_n += 1

            # Count number of gradable answers in
            if qCADict.get(question.question_id, 0) > 0:
                total_gradable += 1
            elif question.type == "FR" and answerCountDict.get(question.question_id, 0) > 0:
                total_gradable += 1

        # get text responses for text answers
        responseIds = [r.id for r in responses]
        if request.textResponseDict is None:
            textResponseDict = self.daoRegistry.studentResponseTextAnswerDao.getTexts(responseIds)
            request.textResponseDict = textResponseDict
        else:
            textResponseDict = request.textResponseDict

        # get text from answer selections
        if request.answerSelectionDict is None:
            answerSelectionDict = self.daoRegistry.studentResponseAnswerSelectionDao.getTexts(responseIds)
            request.answerSelectionDict = answerSelectionDict
        else:
            answerSelectionDict = request.answerSelectionDict

        textResponseDict.update(answerSelectionDict)

        responseDict = {}
        for response in responses:
            if response.user_uuid in responseDict:
                if response.question_id in responseDict[response.user_uuid]:
                    responseDict[response.user_uuid][response.question_id].append(response)
                else:
                    responseDict[response.user_uuid][response.question_id] = [response]
            else:
                responseDict[response.user_uuid] = {response.question_id: [response]}

        if not studentsDict:
            for name in names:
                studentsDict[name.user_uuid] = [name.name, studentIDDict.get(name.user_uuid), True]
        else:
            # set the present students based on the presence in student activity table

            for name in names:
                if name.user_uuid in studentsDict:
                    studentsDict[name.user_uuid][2] = True
                else:
                    studentsDict[name.user_uuid] = [name.name, '-', True]

                if name.user_uuid in studentIDDict:
                    studentsDict[name.user_uuid][1] = studentIDDict[name.user_uuid]

            remove_keys = list()
            for user_uuid in studentsDict:
                if user_uuid not in studentIDDict:
                    for uid, student_id in studentIDDict.items():
                        if studentsDict[user_uuid][1] == student_id:
                            remove_keys.append((user_uuid, uid))
                            break

            # remove newer user_uuid that were not in the activity
            for k, new_k in remove_keys:
                if k in responseDict:
                    studentsDict[new_k] = studentsDict[k]
                else:
                    studentsDict[new_k] = studentsDict.pop(k)

        nameIdDict = dict()

        for name in names:
            if name.name not in nameIdDict:
                nameIdDict[name.name] = list()

            if name.user_uuid in studentIDDict:
                nameIdDict[name.name].append(studentIDDict[name.user_uuid])

        for user_uuid in studentsDict:
            if user_uuid not in responseDict:
                responseDict[user_uuid] = {}
                for question in questions:
                    responseDict[user_uuid][question.question_id] = []
            else:
                for question in questions:
                    if question.question_id not in responseDict[user_uuid]:
                        responseDict[user_uuid][question.question_id] = []

        orderedNaming = sorted(studentsDict.items(), key=lambda x: x[1][0])

        # Record student responses
        for user_uuid, student_details in orderedNaming:

            student_name = student_details[0]
            student_id = student_details[1] or '-'
            presence = student_details[2]

            questionDict = responseDict.get(user_uuid)

            # Reset counters
            num_correct = 0

            # Increment for new student
            current_row += 1
            if presenceCol is not None and presence:
                student_count += 1
            elif presenceCol is None:
                student_count += 1

            # Record new student name
            if require_names:
                sName = remove_control_chars(student_name).strip()
                worksheet.write(current_row, nameCol,
                                html.fromstring(sName).text_content() if sName != "" else "")

                if student_id == '-':
                    student_ids = nameIdDict.get(student_name, [])
                    if len(student_ids) != 1:
                        student_id = '-'
                    else:
                        student_id = student_ids[0]

                worksheet.write(current_row, studentIdColumn,
                                (html.fromstring(student_id).text_content() if student_id != "" else ""),
                                plain_centered)
                if presenceCol is not None:
                    worksheet.write(current_row, presenceCol, (ugettext("Yes") if presence else ugettext("No")))
            else:
                worksheet.write(current_row, nameCol, convert(ugettext("Student names disabled")))
                worksheet.write(current_row, studentIdColumn, "-", plain_centered)

            firstPass = True
            for question_id in questionDict:

                if question_id not in order_mapping:
                    logger.error("Question ID %d not in the order mapping for activity id: %d" % (question_id,
                                                                                                  activity.id))
                    continue

                response = max(questionDict[question_id], key=lambda x: x.id) if questionDict[question_id] else None

                if isSR and response and firstPass:
                    worksheet.write(current_row, teamCol, SPACE_RACE_TEAMS.get(response.team) or "",
                                    teamCellFormat.get(response.team))
                    firstPass = False

                # Determine correctness and formatting for cell
                cell_formatting = None
                if response and response.is_correct is True:
                    cell_formatting = right_answer
                    num_correct += 1
                    correct_list[order_mapping[response.question_id] - questionCol] += 1
                elif response and response.is_correct is False:
                    cell_formatting = wrong_answer
                elif response and response.is_correct is None:
                    cell_formatting = null_answer
                elif response is None and qCADict.get(question_id, 0) > 0:
                    cell_formatting = wrong_answer
                elif response is None and qCADict.get(question_id, 0) == 0:
                    cell_formatting = null_answer
                else:
                    logger.error("this shouldn't happend. Activity id : %d" % activity_instance.id)

                # Get text to display student response
                if response:
                    display_text = ", ".join(textResponseDict.get(response.id, []))
                    try:
                        # if ESCAPED_HTML.match(display_text):
                        display_text = html.fromstring(display_text).text_content() if display_text.strip() != "" else ""
                    except ParserError:
                        logger.warn("This text is making issues with lxml: %s", display_text)
                    except ValueError as e:
                        logger.warn(exceptionStack(e) + "\n||DISPLAY_TEXT|| %s ||" % display_text)
                else:
                    display_text = ""
                # Write appropriate response with correct formatting
                worksheet.write(current_row, order_mapping[question_id], display_text,
                                null_answer if presenceCol is not None and presence is False else cell_formatting)

            if total_gradable != 0:
                worksheet.write(current_row, totalScoreCol,
                                int(round((100.0 * num_correct) / total_gradable)), plain_centered)
            else:
                worksheet.write(current_row, totalScoreCol, 0, plain_centered)
            worksheet.write(current_row, nbCorrectCol, num_correct, plain_centered)

            # Record final student grade
            if student_count != 0:
                if total_gradable != 0:
                    worksheet.write(current_row, totalScoreCol, int(round((100.0 * num_correct) / total_gradable)),
                                    plain_centered)
                else:
                    worksheet.write(current_row, totalScoreCol, 0, plain_centered)
                worksheet.write(current_row, nbCorrectCol, num_correct, plain_centered)

            # Last increment of total correct
            total_correct += num_correct

        # Final row stats
        current_row += 1
        worksheet.write(current_row, 0, convert(ugettext("Class Scoring")), grey_left)

        if presenceCol is not None:
            worksheet.write(current_row, nameCol, "", grey_left)

        if teamCol:
            worksheet.write(current_row, teamCol, "", grey_left)

        if isSR:
            worksheet.write(current_row, nameCol, "", grey_left)

        worksheet.write(current_row, studentIdColumn, "", grey_left)

        # Average class score
        if total_gradable != 0 and student_count != 0:
            worksheet.write(current_row, totalScoreCol, (float(total_correct) / (total_gradable * student_count)),
                            grey_percentage)
        else:
            worksheet.write(current_row, totalScoreCol, 0, grey_percentage)

        # Percent correct by question
        q_n = questionCol
        if student_count != 0:
            worksheet.write(current_row, nbCorrectCol, float(total_correct) / student_count, grey_decimal)
            for n in range(question_count):
                worksheet.write(current_row, n + q_n, float(correct_list[n]) / student_count, grey_percentage)
        else:
            worksheet.write(current_row, nbCorrectCol, 0, grey_decimal)
            for n in range(question_count):
                worksheet.write(current_row, n + q_n, 0, grey_percentage)
                n += 1

        workbook.close()

        output.seek(0, 0)

        return output

    def do_report_download(self, excelReport, quizPdf, questionsPdf, studentReports, report_types, quiz, activity,
                           localTime):
        """
        Downloads a given report.
        :param excelReport:
        :param quizPdf:
        :param questionsPdf:
        :param studentReports:
        :param report_types:
        :param quiz:
        :param activity:
        :param localTime:
        :return:
        """

        quiz_name = WORD_PATTERN.sub('', quiz.name).lower()
        quiz_type = activity.activity_type if activity else None

        if not quizPdf:
            assets = dict()
            for student in studentReports:
                phile = FilenameHelper("pdf", extra=student[1])
                phile.changePDFFilename(extra=student[1], fileParams=[quiz_name, quiz_type, localTime])
                assets[phile] = studentReports[student].read()
                studentReports[student].close()

            if questionsPdf:
                phile = FilenameHelper.from_report_settings(["whole-class"], "download", rLen=1, tLen=1,
                                                            fileParams=[quiz_name, quiz_type, localTime])
                assets[phile] = questionsPdf.read()
                questionsPdf.close()

            if excelReport:
                phile = FilenameHelper.from_report_settings(["whole-class-excel"], "download", rLen=1, tLen=1,
                                                            fileParams=[quiz_name, quiz_type, localTime])
                assets[phile] = excelReport.read()
                excelReport.close()

            if len(assets) + len(studentReports) > 1:
                payload = self._zip_array(assets)
            else:
                payload = assets.values()[0]

            # the exact number doesn't count, 2 it's enough to force a zip archive
            rLen = len(assets) + len(studentReports)
            phile = FilenameHelper.from_report_settings(report_types, "download",
                                                        rLen=rLen, tLen=2,
                                                        fileParams=[quiz_name, quiz_type, localTime])

        else:
            phile = FilenameHelper.from_report_settings(["unanswered"], "download", rLen=1, tLen=1,
                                                        fileParams=[quiz_name, quiz_type, localTime])
            payload = quizPdf.read()

            rLen = 1

        response = HttpResponse(payload, content_type=self._report_mime_type(report_types, "download",
                                                                             rLen=rLen))
        response['Content-Disposition'] = "attachment; filename={}".format(urllib.quote(phile.prettier))

        return {"resp": response}, 200

    def do_upload_to_google(self, reports, report_types, *args, **kwargs):
        """
        uploads to google drive
        :param reports:
        :param report_types:
        :param args:
        :param kwargs:
        :return:
        """

        mimetype = self._report_mime_type(report_types, "download", rLen=2 if kwargs["atend"] != '' else 1)
        localTime = kwargs.get("localTime")

        if "quiz" in kwargs.keys():
            quiz = kwargs["quiz"]

            quiz_name = WORD_PATTERN.sub('', quiz.name).lower()
            quiz_type = kwargs.get("activity").activity_type
            local_time = kwargs.get("localTime")

            phile = FilenameHelper.from_report_settings(report_types, "download", basename=quiz_name,
                                                        extra=localTime.isoformat() or None,
                                                        rLen=2 if kwargs["atend"] != '' else 1, tLen=2,
                                                        fileParams=[quiz_name, quiz_type, local_time])
            filename = phile._basename + kwargs["atend"] + "." + phile.file_format
            return self.uploadToGoogleDrive(kwargs["request"], kwargs["user"], kwargs["activity"], filename, mimetype,
                                            kwargs["is_pro"])

        else:
            return BaseError.ACTIVITY_NOT_FOUND, status.HTTP_400_BAD_REQUEST

    def do_report_email(self, reports, report_types, *args, **kwargs):
        """
        Emails a given report to the teacher.
        """
        try:

            quiz = kwargs["quiz"]

            quiz_name = WORD_PATTERN.sub('', quiz.name).lower()
            quiz_type = kwargs.get("activity").activity_type
            local_time = kwargs.get("localTime")

            phile = FilenameHelper.from_report_settings(report_types, "email", rLen=2 if kwargs["atend"] != '' else 1,
                                                        tLen=2, fileParams=[quiz_name, quiz_type, local_time])

            filename = phile._basename + kwargs["atend"] + "." + phile.file_format

            taskDict = dict()
            taskDict["id"] = kwargs["activity"].id
            taskDict["user_id"] = kwargs["user"].id
            taskDict["email_address"] = kwargs["email"]
            taskDict["filename"] = filename
            taskDict["mimetype"] = self._report_mime_type(report_types, "email", rLen=2 if kwargs["atend"] != '' else 1)
            taskDict["action"] = ActionTypes.SEND_VIA_EMAIL
            taskDict["url"] = kwargs["request"].build_absolute_uri("/")
            taskDict["is_pro"] = kwargs["is_pro"]

            kwargs["request"].tasksClient.add_tasks(TaskType.REPORT_ACTIONS, [taskDict])

            return {}, status.HTTP_200_OK
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def _zip_array(self, assets):
        """
        Construct a zip in memory, and add data to the archive.
        """
        mem = io.BytesIO()
        archive = zipfile.ZipFile(mem, 'w')

        for phile, data in assets.items():
            archive.writestr(phile.prettier, data)

        archive.close()

        return mem.getvalue()

    def _report_mime_type(self, report_types, download_method, rLen=None):
        """
        Returns an appropriate MIME type for a report and
        download method.
        """

        configs = {
            "download": {
                "whole-class": "application/pdf",
                "whole-class-excel": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "all-students": "application/pdf",
                "unanswered": "application/pdf"
            },
            "email": {
                "whole-class": "application/pdf",
                "whole-class-excel": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "all-students": "application/pdf",
                "unanswered": "application/pdf"
            },
            "print": {
                "whole-class": "text/html",
                "whole-class-excel": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "all-students": "text/html",
                "unanswered": "text/html"
            }
        }

        num_reports = rLen or len(report_types)
        if num_reports > 1:
            if download_method in ["download", "email"] or "whole-class-excel" in report_types:
                mime_type = "application/zip"
            else:
                mime_type = "text/html"
        elif num_reports == 1:
            mime_type = configs[download_method][report_types[0]]
        else:
            raise Exception

        return mime_type

    def __getActivitySettings(self, activity_instance_id):
        """

        :param activity_instance_id:
        :return:
        """
        try:
            settings_dict = self.cacheRegistry.activitySettingsCache.get(activity_instance_id)

            if not settings_dict:
                settings_dict = self.daoRegistry.activitySettingsDao.getAllSettings(activity_instance_id)

            return settings_dict
        except Exception as e:
            logger.error(exceptionStack(e))
            raise

    def free_response_student_details(self, **kwargs):
        """
        
        :return: 
        """
        presence_column = kwargs.get("presence_column")
        presence = kwargs.get("presence")
        current_row = kwargs.get("current_row")
        student_name = kwargs.get("student_name")
        worksheet = kwargs.get("worksheet")
        student_name_column = kwargs.get("student_name_column")
        students_dict = kwargs.get("students_dict")
        student_id = kwargs.get("student_id")
        student_id_column = kwargs.get("student_id_column")
        style = kwargs.get("style")
        name_id_dict = kwargs.get("name_id_dict")

        if presence_column is not None and presence is not None:
            worksheet.write(current_row, presence_column, (ugettext("Yes") if presence else ugettext("No")))

        if student_name is not None:
            sName = student_name.strip()
            worksheet.write(current_row, student_name_column,
                            (html.fromstring(sName).text_content() if sName != "" else ""))

        if students_dict:
            if student_id == '-':
                student_ids = name_id_dict.get(student_name, [])
                if len(student_ids) != 1:
                    student_id = '-'
                else:
                    student_id = student_ids[0]
            worksheet.write(current_row, student_id_column,
                            (html.fromstring(student_id).text_content() if student_id else ""),
                            style)

    def free_response_report_file(self, output, quiz, activity_instance, user, request):
        """
        report for single question free response activities
        """

        if request.questions is None:
            questions = self.daoRegistry.questionDao.getQuestions(quiz.id)
        else:
            questions = request.questions

        if len(questions) > 1:
            logger.info("this is not a 1Q activity for a FR response question")
            return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

        if activity_instance.activity_type == ActivityInstanceModel.SINGLE_QUESTION:
            # check to see if there is a voting activity after this (activity_type = 1V)
            sec_activity = self.daoRegistry.activityInstanceDao.checkVoteActivity(activity_instance)
            fr_activity = activity_instance
            vote_activity = self.daoRegistry.activityInstanceDao.getActivityById(sec_activity) if sec_activity is not None else None

        elif activity_instance.activity_type == ActivityInstanceModel.SINGLE_QUESTION_VOTING:
            # find the activity with the fr responses
            sec_activity = self.daoRegistry.activityInstanceDao.checkVoteActivity(activity_instance)
            vote_activity = activity_instance
            fr_activity = self.daoRegistry.activityInstanceDao.getActivityById(sec_activity) if sec_activity is not None else None
        else:
            return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

        if fr_activity is None:
            return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

        if request.responses is None or questions[0].type == QuestionModel.MULTIPLE_CHOICE:
            responses = self.daoRegistry.studentResponseDao.getAllResponses(fr_activity.id,
                                                                            order_by=BaseDao.USER_UUID,
                                                                            sortOrder=BaseDao.ASCENDING,
                                                                            hidden=False)
        else:
            responses = request.responses

        if request.names is None or questions[0].type == QuestionModel.MULTIPLE_CHOICE:
            names = self.daoRegistry.studentNameDao.getAllNamesByActivity(fr_activity.id)
        else:
            names = dict((sn.user_uuid, sn.name) for sn in request.names)

        if request.questions is None or questions[0].type == QuestionModel.MULTIPLE_CHOICE:
            questions = self.daoRegistry.questionDao.getQuestions(fr_activity.activity_id)
        else:
            questions = request.questions

        require_names = False
        name_setting = safeToBool(self.__getActivitySettings(fr_activity.id).get("require_names"))

        votes = None
        if vote_activity is not None:
            # votes is a dictionary with answer_text, number of votes
            votes = self.daoRegistry.studentResponseDao.getVotes(vote_activity.id, vote_activity.activity_id)

        if name_setting is not None:
            require_names = name_setting

        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet()

        basic_formatting = workbook.add_format({'font_name': 'Arial', 'font_size': 16})
        bold_left = workbook.add_format({'bold': True, 'font_name': 'Arial', 'font_size': 16})
        plain_centered = workbook.add_format({'align': 'center', 'font_name': 'Arial', 'font_size': 16})
        blue_header = workbook.add_format({'bg_color': '#89BFFB', 'text_wrap': True, 'align': 'center',
                                           'font_name': 'Arial', 'font_size': 16})
        null_answer = workbook.add_format({'font_name': 'Arial', 'font_size': 16, 'text_wrap': True, 'align': 'left'})

        worksheet.set_column(0, 0, 30, basic_formatting)

        if user.tz_offset is None:
            local_time = fr_activity.start_time
        else:
            local_time = fr_activity.start_time - timedelta(minutes=user.tz_offset)

        qDict = dict((q.question_id, q) for q in questions)
        qText = questions[0].question_text.strip()
        activity_name = (html.fromstring(qText).text_content() if qText != "" else "") if len(questions) > 0 and\
                                                                                          len(questions[0].question_text) > 0 else ""

        if request.room_name is None:
            room_name = activity_instance.room_name
            request.room_name = room_name
        else:
            room_name = request.room_name

        responseIds = [r.id for r in responses]
        if request.textAnswerDict is None:
            textAnswerDict = self.daoRegistry.studentResponseTextAnswerDao.getTexts(responseIds)
            request.textAnswerDict = textAnswerDict
        else:
            textAnswerDict = request.textAnswerDict

        activity_name = activity_name[:30]
        if IS_ALL_DIGITS.match(activity_name):
            activity_name = """=concatenate("%s","")""" % activity_name
        if activity_name == "":
            activity_name = convert(ugettext("Short Answer Activity"))

        worksheet.write(0, 0, activity_name, bold_left)
        worksheet.write(1, 0, local_time.strftime("%A, %B %d %Y %I:%M %p"))
        room_name = convert(ugettext("Room:")).encode('utf-8') + " " + room_name.encode('utf-8') if type(room_name) is unicode else room_name
        room_name = room_name + " (" + (user.email.encode('utf-8') if type(user.email) is unicode else user.email) + ")"
        room_name = room_name.decode('utf-8')
        worksheet.write(2, 0, room_name)

        current_row = 4
        response_col = 0

        studentIDDict = dict()
        studentsDict = dict()
        studentList = None

        student_name_column = None
        student_id_column = None
        presence_column = None
        votes_column = None

        if require_names:
            studentsList = self.daoRegistry.studentActivityDao.getPresence(activity_instance.id)
            studentsDict = dict()
            user_uuid = 0
            for student in studentsList:
                if student.get("user_uuid"):
                    studentsDict[student.get("user_uuid")] = [student.get("name"),
                                                              student.get("student_id"),
                                                              student.get("present") or False
                                                              ]
                else:
                    studentsDict[user_uuid] = [student.get("name"),
                                               student.get("student_id"),
                                               student.get("present") or False
                                               ]
                    user_uuid += 1

            if studentsDict:
                worksheet.write(current_row, response_col, convert(ugettext('Presence')), blue_header)
                presence_column = response_col
                response_col += 1

            worksheet.write(current_row, response_col, convert(ugettext('Student Name')), blue_header)
            student_name_column = response_col
            response_col += 1

            if studentsDict:
                worksheet.write(current_row, response_col, convert(ugettext('Student ID')), blue_header)
                studentIDDict = self.daoRegistry.studentDao.getStudentIDs(activity_instance.id)
                student_id_column = response_col
                response_col += 1

        if votes is not None:
            worksheet.write(current_row, response_col, convert(ugettext('Votes')), blue_header)
            votes_column = response_col
            response_col += 1

        worksheet.set_column(response_col, response_col, 60, basic_formatting)

        worksheet.write(current_row, response_col, convert(ugettext('Student Answer')), blue_header)

        nameIdDict = dict()

        responseDict = dict()
        for response in responses:
            if response.user_uuid in responseDict:
                responseDict[response.user_uuid].append(response)
            else:
                responseDict[response.user_uuid] = [response]


        if not studentsDict:
            for user_uuid in names:
                studentsDict[user_uuid] = [names[user_uuid], studentIDDict.get(user_uuid), True]
        else:
            for user_uuid in names:
                if user_uuid in studentsDict:
                    studentsDict[user_uuid][2] = True
                else:
                    studentsDict[user_uuid] = [names[user_uuid], '-', True]

                if user_uuid in studentIDDict:
                    studentsDict[user_uuid][1] = studentIDDict[user_uuid]

            remove_keys = list()
            for user_uuid in studentsDict:
                if user_uuid not in studentIDDict:
                    for uid, student_id in studentIDDict.items():
                        if studentsDict[user_uuid][1] == student_id:
                            remove_keys.append((user_uuid, uid))
                            break

            # remove newer user_uuid that were not in the activity
            for k, new_k in remove_keys:
                if k in responseDict:
                    studentsDict[new_k] = studentsDict[k]
                else:
                    studentsDict[new_k] = studentsDict.pop(k)

        orderedNaming = sorted(studentsDict.items(), key=lambda x: x[1][0])

        current_row += 1
        for user_uuid, student_details in orderedNaming:
            student_name = student_details[0]
            student_id = student_details[1]
            presence = student_details[2]

            used_row = False
            start_row = current_row
            if require_names:
                self.free_response_student_details(style=plain_centered,
                                                   student_name=student_name,
                                                   student_id=student_id,
                                                   presence=presence,
                                                   student_name_column=student_name_column,
                                                   student_id_column=student_id_column,
                                                   presence_column=presence_column,
                                                   current_row=current_row,
                                                   students_dict=studentsDict,
                                                   name_id_dict=nameIdDict,
                                                   worksheet=worksheet)
                used_row = True

            for response in responseDict.get(user_uuid, []):
                question = qDict.get(response.question_id)
                if question and question.type == "FR":
                    if require_names and start_row != current_row:
                        self.free_response_student_details(style=plain_centered,
                                                           student_name=student_name,
                                                           student_id=student_id,
                                                           presence=presence,
                                                           student_name_column=student_name_column,
                                                           student_id_column=student_id_column,
                                                           presence_column=presence_column,
                                                           current_row=current_row,
                                                           students_dict=studentsDict,
                                                           name_id_dict=nameIdDict,
                                                           worksheet=worksheet)
                    text_answers = []
                    for text_answer in textAnswerDict.get(response.id, []):
                        text_answers.append(text_answer)
                    display_text = ", ".join(text_answers)
                    if ESCAPED_HTML.match(display_text):
                        display_text = html.fromstring(display_text).text_content() if display_text.strip() != "" else ""
                    worksheet.write(current_row, response_col, display_text, null_answer)
                    if votes:
                        value = str(votes.get(htmlEscape(text_answers[0]), 0) or votes.get(text_answers[0], 0))
                        worksheet.write(current_row, votes_column, value)
                    current_row += 1

            if used_row is True and start_row == current_row:
                current_row += 1

        workbook.close()

    def uploadToGoogleDrive(self, request, user, activity, filename, mimetype, is_pro=False):
        """

        :param request:
        :param user:
        :param activity:
        :param filename:
        :param mimetype:
        :return:
        """

        try:
            self.daoRegistry.partnerDao.getPartner(user.id, PartnerModel.GOOGLE)

            taskDict = dict()
            taskDict["id"] = activity.id
            taskDict["user_id"] = user.id
            taskDict["filename"] = filename
            taskDict["mimetype"] = mimetype
            taskDict["action"] = ActionTypes.UPLOAD_TO_GOOGLE_DRIVE
            taskDict["is_pro"] = is_pro

            request.tasksClient.add_tasks(TaskType.REPORT_ACTIONS, [taskDict])

            return {}, status.HTTP_200_OK

        except BaseDao.NotFound:
            logger.warn("user %s is not logged via google oauth, but it accessed upload to drive", user.email)
            return BaseError.USER_NOT_LOGGED_IN_VIA_GOOGLE, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def __getFileFromS3(self, bucket, activityId, filename):
        """
        :param bucket:
        :param activityId:
        :param filename:
        :return:
        """
        k = bucket.get_key("reports/%d/" % activityId + filename)
        if k:
            return k

        for k in bucket.list(prefix="reports/%d/" % activityId):
            if k.name[-6:] == filename[-6:]:
                return k

        return None

    def activityReport(self, request, dataDict):
        """

        :param request:
        :param dataDict:
        :return:
        """
        document_id = dataDict.get("di")
        auth_token = dataDict.get("auth_token")
        delivery_type = dataDict.get("dt")
        report_types = dataDict.get('rt')

        if report_types is None:
            return BaseError.REPORT_TYPE_MISSING, status.HTTP_400_BAD_REQUEST
        report_types = report_types.split("+")
        report_types = list(set(str(report_types[0]).split(' ')))

        report_method_table = {
            "whole-class-excel": self.whole_class_excel_report,
            "all-students": None,
            "whole-class": None,
            "unanswered": None
        }

        delivery_method_table = {
            "dl": self.do_report_download,
            "em": self.do_report_email,
            "go": self.do_upload_to_google,
        }

        try:
            user = self.serviceRegistry.userService.validateUser(auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if "unanswered" not in report_types:
                activity = self.daoRegistry.activityInstanceDao.getActivityById(safeToInt(document_id), _type=BaseDao.TO_MODEL)
                if activity is None:
                    return BaseError.ACTIVITY_NOT_FOUND, status.HTTP_400_BAD_REQUEST

                if activity.started_by_id != user.id:
                    return BaseError.USER_DOESNT_OWN_ACTIVITY, status.HTTP_403_FORBIDDEN

                if activity.activity_type == ActivityInstanceModel.SINGLE_QUESTION_NO_REPORT:
                    return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

                quiz = self.daoRegistry.quizDao.loadQuizById(activity.activity_id, _type=BaseDao.TO_MODEL)
            else:
                if len(report_types) > 1:
                    return BaseError.INVALID_REPORT_TYPE, status.HTTP_400_BAD_REQUEST

                activity = None
                quiz = self.daoRegistry.quizDao.loadQuizBySoc(document_id, is_hidden=False, _type=BaseDao.TO_MODEL)

            for report_type in report_types:
                if report_type not in report_method_table:
                    return BaseError.INVALID_REPORT_TYPE, status.HTTP_400_BAD_REQUEST

            atend = ''
            if 'all-students' in report_types:
                if 'whole-class' in report_types and 'whole-class-excel' in report_types:
                    atend = "E"
                elif 'whole-class' in report_types:
                    atend = "D"
                elif 'whole-class-excel' in report_types:
                    atend = "C"
                else:
                    atend = "A"
            elif 'whole-class-excel' in report_types and 'whole-class' in report_types:
                atend = "B"

            if delivery_type not in delivery_method_table:
                return BaseError.INVALID_DELIVERY_TYPE, status.HTTP_400_BAD_REQUEST

            if activity and activity.end_time is None:
                return BaseError.ACTIVITY_NOT_ENDED, status.HTTP_400_BAD_REQUEST

            if activity:
                if user.tz_offset is None:
                    local_time = activity.start_time
                else:
                    local_time = activity.start_time - timedelta(minutes=user.tz_offset)
            else:
                local_time = None

            reports = []
            # store data inside the request object
            request.activity = activity
            request.room_name = activity.room_name if activity else None
            request.textAnswerDict = None
            request.textResponseDict = None
            request.answerSelectionDict = None
            request.qCADict = None

            pdfsDict = {}
            questionsPdf = None
            quizPdf = None
            excelReport = None

            if len(report_types) == 1 and activity is not None and activity.activity_type == "1Q" and \
                    ("all-students" in report_types or "whole-class" in report_types or "unanswered" in report_types):
                return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

            if activity is not None and activity.activity_type == "1Q" and "whole-class-excel" not in report_types:
                return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

            if delivery_type not in ["go", "em"]:
                if delivery_type == 'dl':
                    try:

                        quiz_name = WORD_PATTERN.sub('', quiz.name).lower()
                        phile = FilenameHelper.from_report_settings(report_types, "download", quiz_name,
                                                                    extra=local_time.isoformat() if local_time else None,
                                                                    rLen=2 if atend != '' else 1, tLen=2,
                                                                    fileParams=[quiz_name,
                                                                                activity.activity_type if activity else None,
                                                                                local_time])
                        filename = phile._basename + atend + "." + phile.file_format

                        if activity:
                            k = self.__getFileFromS3(request.s3bucket, activity.id, filename)
                            if not k:
                                self.daoRegistry.activityInstanceDao.updateReportStatus(activity.id,
                                                                                        ActivityInstanceModel.REPORT_NOT_DONE,
                                                                                        )
                                raise Exception("report is not on s3 yet")

                            urlExipreTime = safeToInt(ConfigRegistry.getItem("S3_URL_EXPIRE_TIME_SECONDS"))

                            disposition = 'attachment; filename="%s"' % filename
                            response_headers = {
                                'response-content-disposition': disposition,
                            }

                            return {"resp": HttpResponseRedirect(k.generate_url(urlExipreTime, query_auth=True,
                                                                                response_headers=response_headers))
                                   }, 200

                    except Exception as e:
                        logger.info(exceptionStack(e))

                reports = []

                if "unanswered" not in report_types:
                    responses = self.daoRegistry.studentResponseDao.getAllResponses(activity.id,
                                                                                    order_by=BaseDao.USER_UUID,
                                                                                    sortOrder=BaseDao.ASCENDING,
                                                                                    hidden=False)
                    names = self.daoRegistry.studentNameDao.getStudentNamesForActivityId(auth_token, activity.id,
                                                                                         _type=BaseDao.TO_MODEL)

                    if len(responses) == 0 and len(report_types) == 1 and report_types[0] == 'all-students':
                        return BaseError.NO_STUDENT_RESPONSES_FOR_ACTIVITY, status.HTTP_400_BAD_REQUEST
                else:
                    responses = []
                    names = []

                questions = self.daoRegistry.questionDao.getQuestions(quiz.id, orderBy=BaseDao.ORDER,
                                                                      sortOrder=BaseDao.ASCENDING)

                rValues = [dict(id=media.id,
                                url=media.url,
                                formats="*") for media in self.daoRegistry.mediaResourceDao.loadMediaForQuiz(quiz.id)]

                request.tasksClient.add_tasks(TaskType.IMAGE_RESIZE, rValues)

                request.responses = responses
                request.names = names
                request.questions = questions
                request.user = user

                for report_type in report_types:
                    if report_type == "all-students":
                        c = StudentPdfReport(self.daoRegistry, responses, questions, quiz, names, local_time, activity,
                                             request)
                        pdfsDict = c.getStudentsPdfs()
                    elif report_type == "whole-class":
                        c = WholeClassPdfReport(self.daoRegistry, responses, questions, quiz, names, local_time,
                                                activity, request)
                        questionsPdf = c.getQuestionPdf()
                    elif report_type == "unanswered":
                        c = QuizPdfReport(self.daoRegistry, questions, quiz, request)
                        quizPdf = c.getQuizPdf()
                    elif report_type == "whole-class-excel":
                        excelReport = self.whole_class_excel_report(request, quiz, responses, questions, names,
                                                                    local_time, activity)
                        if type(excelReport) is tuple:
                            return excelReport
                    else:
                        raise Exception("Unknown report type: %s" % report_type)

            if delivery_type == "dl" or ("whole-class-excel" in report_types and delivery_type == "pr"):
                return self.do_report_download(excelReport, quizPdf, questionsPdf, pdfsDict, report_types,
                                               localTime=local_time, quiz=quiz, activity=activity)
            else:
                isPro = self.serviceRegistry.userService.is_user_premium(user)
                return delivery_method_table[delivery_type](reports, report_types, email=user.email, request=request,
                                                            user=user, localTime=local_time, quiz=quiz,
                                                            activity=activity, atend=atend, is_pro=isPro)
        except BaseDao.NotFound:
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def downloadReports(self, request, dataDict):
        """

        :param request:
        :param dataDict
        :return:
        """

        try:
            encrypted_filename = dataDict.get("filename")
            if not isStringNotEmpty(encrypted_filename):
                return BaseError.FILENAME_PARAM_MISSING, status.HTTP_400_BAD_REQUEST

            msg = Cryptography.decrypt(encrypted_filename)
            keys = msg.split("|")
            userId = safeToInt(keys[0]) or -1
            if len(keys) == 2:
                return BaseError.FILE_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            activity_instance_id = keys[1]
            filename = keys[2]
            self.daoRegistry.usersDao.loadUserById(userId)

            k = request.s3bucket.lookup("reports/%s/" % activity_instance_id + filename)
            if k is None:
                return BaseError.FILE_NOT_FOUND, status.HTTP_404_NOT_FOUND

            urlExipreTime = safeToInt(ConfigRegistry.getItem("S3_URL_EXPIRE_TIME_SECONDS"))

            return HttpResponseRedirect(k.generate_url(urlExipreTime, query_auth=True))
        except BaseDao.NotFound:
            logger.warn("Bad url %s. User doesn't exist", request.path)
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def importQuizByExcel(self, request, dataDict):
        """
        handler for import quiz by excel
        :param request:
        :param dataDict:
        :return:
        """

        try:
            auth_token = dataDict.get("auth_token")

            user = self.serviceRegistry.userService.validateUser(auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if len(request.FILES) != 1:
                return BaseError.INVALID_NUMBER_OF_FILES, status.HTTP_400_BAD_REQUEST

            f = request.FILES['0']

            if f.content_type not in ["application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                      "application/vnd.ms-excel", "application/octet-stream"]:
                logger.debug("File type: %s" % f.content_type)
                return BaseError.INVALID_FILE_TYPE, status.HTTP_400_BAD_REQUEST

            w = xlrd.open_workbook(file_contents=f.read())

            errors = list()
            for sheet in w.sheets():
                if sheet.nrows < 6:
                    errors.append(ugettext("There are not enough rows in the excel template"))
                    break

                if sheet.nrows > 1000:
                    errors.append(ugettext("Oh snap, there are too many rows in your excel: %d, try to have under 1000") % sheet.nrows)

                if len(sheet.row(2)) < 2:
                    errors.append(ugettext("There are too few columns on row 2. Please use the template provided by Socrative"))
                    break

                quiz_name = sheet.row(2)[1].value

                # TODO move this into a validator
                if len(quiz_name) > QuizModel.NAME_MAX_LENGTH:
                    errors.append(ugettext("Quiz names cannot exceed 128 characters. Please rename your quiz."))
                    break

                if quiz_name == "" or quiz_name == "Enter the name of the quiz here":
                    quiz_name = "Untitled quiz"

                quiz = QuizModel()
                quiz.name = htmlEscape(quiz_name) if type(quiz_name) in (str, unicode) else str(quiz_name)
                quiz.created_by_id = user.id
                quiz.is_hidden = False
                quiz.sharable = True

                questions = list()
                qOrder = 1
                for rowx in range(6, sheet.nrows):
                    cells = sheet.row(rowx)
                    q_type = cells[0].value
                    qtext = cells[1].value
                    if q_type == "" or qtext == "":
                        errors.append(ugettext("Question type or question text missing at row %d") % (rowx + 1))
                        break
                    question = QuestionModel()
                    question.created_by_id = user.id

                    if q_type == "Open-ended":
                        question.type = QuestionModel.FREE_RESPONSE
                        qText = (safeToInt(qtext) or qtext) if type(qtext) not in (str, unicode, float) else qtext
                        question.question_text = str(qText) if type(qText) not in (str, unicode) else qText
                        question.order = qOrder
                        qOrder += 1
                        question.ans = []
                        questions.append(question)
                    elif q_type == "Multiple choice":

                        correctAnswers = []
                        for cell in cells[7:12]:
                            if cell.value in ["A", "B", "C", "D", "E"]:
                                correctAnswers.append(ord(cell.value) - ord("A") + 2)

                        correctAnswers = list(set(correctAnswers))

                        question.type = QuestionModel.MULTIPLE_CHOICE
                        question.question_text = str(qtext) if type(qtext) is not unicode else qtext.encode('utf-8')

                        answers = list()
                        order = 1
                        for index in range(2, 7):
                            if cells[index].value == "":
                                continue
                            a = AnswerModel()
                            if type(cells[index].value) in (str, unicode):
                                val = cells[index].value
                                a.text = str(safeToInt(val)) if safeToInt(val) else val
                            elif cells[index].ctype == 4:
                                a.text = str(bool(cells[index].value))
                            elif cells[index].ctype == 2:
                                val = int(cells[index].value)
                                val1 = float(cells[index].value)
                                if val == val1:
                                    a.text = str(val)
                                else:
                                    a.text = str(val1)
                            else:
                                a.text = str(cells[index].value)

                            a.created_by_id = user.id
                            if index in correctAnswers:
                                a.is_correct = True
                                # isTrue = True
                            else:
                                a.is_correct = False
                            a.order = order
                            a.text = htmlEscape(a.text)

                            # TODO move this to the answer validator
                            if len(a.text) > AnswerModel.TEXT_MAX_LENGTH:
                                errors.append(ugettext("the answer at column %d row %d is too long") % (index+1, rowx+1))
                                break

                            order += 1
                            answers.append(a)
                        if len(answers) < 2:
                            errors.append(ugettext("The are too few answers for MC type question at row %d") % (rowx + 1))
                            break

                        question.ans = answers
                        question.order = qOrder
                        question.question_text = htmlEscape(question.question_text)
                        qOrder += 1
                        questions.append(question)
                    else:
                        errors.append(ugettext("question at row %d doesn't have a correct type") % (rowx + 1))
                        break

                    # TODO move this to the answer validator
                    if len(question.question_text) > QuestionModel.QUESTION_TEXT_MAX_LENGTH:
                        errors.append(ugettext("the text for the question at row %d is too long") % (rowx + 1))
                        break

                if not questions or len(questions) == 0:
                    errors.append(ugettext("the quiz '%s' doesn't have any valid questions" % quiz_name))
                    break
                if errors:
                    break
                quiz.created_date = datetime.datetime.utcnow()
                quiz.last_updated = datetime.datetime.utcnow()
                quizId = self.daoRegistry.quizDao.saveQuiz(quiz)
                for q in questions:
                    q.quiz_id = quizId
                    q.explanation = q.explanation if q.explanation is not None else ""
                    qDict = self.daoRegistry.questionDao.createQuestion(q.toDict())
                    for a in q.ans:
                        a.question_id = qDict[QuestionModel.QUESTION_ID]
                        self.daoRegistry.answerDao.createAnswer(a.toDict())

                # read from the first sheet
                break

            if errors:
                return errors, status.HTTP_400_BAD_REQUEST

            return {}, status.HTTP_201_CREATED

        except BaseDao.NotFound:
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

        except XLRDError:
            return [ugettext("The excel file is broken")], status.HTTP_400_BAD_REQUEST

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def shareQuiz(self, request, dataDict):
        """

        :param request:
        :param dataDict:
        :return:
        """

        soc_number = dataDict.get("soc_number")
        auth_token = dataDict.get("auth_token")

        if not isStringNotEmpty(soc_number):
            return BaseError.SOC_NUMBER_IS_MISSING, status.HTTP_400_BAD_REQUEST

        if not isStringNotEmpty(auth_token):
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

        try:
            quiz = self.daoRegistry.quizDao.loadQuizBySoc(soc_number, auth_token, is_hidden=False,
                                                          _type=BaseDao.TO_MODEL)

            if quiz is None:
                return BaseError.QUIZ_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            url = ("https://" if request.is_secure() else "http://") + \
                  request.META.get('HTTP_HOST')+"/import-quiz/"+Cryptography.encryptDES_CBC("SOC_", quiz.soc_number)

            return {"url": url}, status.HTTP_200_OK

        except BaseDao.NotFound:
            return BaseError.QUIZ_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def shareQuizByEmail(self, request, dataDict):
        """

        :param request:
        :param dataDict:
        :return:
        """

        url = dataDict.get("url")
        email = dataDict.get("email", "").strip()
        auth_token = dataDict.get("auth_token")

        if not isStringNotEmpty(url):
            return BaseError.SHARE_QUIZ_URL_MISSING, status.HTTP_400_BAD_REQUEST

        if not isStringNotEmpty(email) or not isEmailValid(email):
            return BaseError.INVALID_EMAIL_ADDRESS, status.HTTP_400_BAD_REQUEST

        if not isStringNotEmpty(auth_token):
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

        try:
            user = self.serviceRegistry.userService.validateUser(auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            context = {"display_name": user.display_name, "email_address": user.email, "url": url, "user_id": user.id,
                       "email_type": EmailTypes.QUIZ_SHARE, "to": email}

            request.tasksClient.add_tasks(TaskType.SEND_EMAIL, [context])

            return {}, status.HTTP_200_OK
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def reorderQuestions(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            # check that the quiz wasn't ran or is currently running
            activity = self.daoRegistry.activityInstanceDao.loadActivityByQuizId(quiz_id=dto.quiz_id,
                                                                                 started_by_id=user.id,
                                                                                 limit=1,
                                                                                 _type=BaseDao.TO_MODEL)

            if activity is not None and activity.end_time is None:
                return BaseError.QUIZ_IS_RUNNING, status.HTTP_403_FORBIDDEN

            if activity is not None:
                return BaseError.QUIZ_HAS_BEEN_RUN, status.HTTP_400_BAD_REQUEST

            self.daoRegistry.questionDao.updateOrders(dto.orders)

            return {}, status.HTTP_200_OK

        except Exception as e:
            logger.exception(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR
