# coding=utf-8
from common.base_cache import BaseCache
from common.socrative_api import exceptionStack, safeToInt
import logging

logger = logging.getLogger(__name__)


class QuizCache(BaseCache):
    """
    base class to handle caching of dto json responses
    """

    def __init__(self, redisPool, expireTime):
        """

        :param redisPool:
        :param expireTime:
        :return:
        """

        super(QuizCache, self).__init__(redisPool)
        self.expireTime = safeToInt(expireTime) or 84100

    def cacheData(self, quizId, data, isStudent):
        """
        :param quizId:
        :param data:
        :param isStudent
        :return:
        """

        try:
            if isStudent is not None:
                key = "quiz_%d" % quizId
                key += "_t" if isStudent is False else "_s"
                self.getConnection(False).set(key, data, expireTime=self.expireTime)
            else:
                key1 = "quiz_%s_s" % quizId
                key2 = "quiz_%s_t" % quizId
                self.getConnection(False).set_multi({key1: data, key2: data}, expireTime=self.expireTime)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateTeacherData(self, quizId):
        """

        :param quizId:
        :return:
        """
        try:
            key = "quiz_%s_t" % quizId
            self.getConnection(False).delete(key)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateStudentData(self, quizId):
        """

        :param quizId:
        :return:
        """
        try:
            key = "quiz_%s_s" % quizId
            self.getConnection(False).delete(key)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateData(self, quizId):
        """

        :param quizId:
        :return:
        """
        try:
            key1 = "quiz_%s_s" % quizId
            key2 = "quiz_%s_t" % quizId
            self.getConnection(False).delete_multi(key1, key2)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateMulti(self, quiz_ids):
        """

        :param quizId:
        :return:
        """
        try:
            keys1 = ["quiz_%s_s" % quizId for quizId in quiz_ids]
            keys2 = ["quiz_%s_t" % quizId for quizId in quiz_ids]
            self.getConnection(False).delete_multi(tuple(keys1+keys2))
        except Exception as e:
            logger.error(exceptionStack(e))

    def get(self, quizId, isStudent):
        """
        get the current activity
        :param roomName:
        :param isStudent:
        :return:
        """

        try:
            key = "quiz_" + str(quizId)
            key += "_t" if isStudent is False else "_s"
            obj = self.getConnection(True).get(key)
            return obj
        except Exception as e:
            logger.error(exceptionStack(e))
