# coding=utf-8
from common.base_dto import DTOConverter, ItemConverter, RequestDTOValidator, ItemValidator
from common.socrative_api import safeToInt, isNotNone
from common.socrative_errors import BaseError
from common import http_status as status


class LongMessage(DTOConverter):
    """
    serializer for long message
    """

    stringifyDict = {
        "id": ItemConverter(),
        "message": ItemConverter()
    }


class GetLongMessageIdDTO(RequestDTOValidator):
    """
    validates the data for find student response request
    """

    rulesDict = {
        "pk": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.INVALID_REQUEST,
                                                                        status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.pk = None