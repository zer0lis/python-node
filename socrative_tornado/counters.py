# coding=utf-8


class PubnubCounters(object):
    """
    class to hold the pubnub counter names
    """

    PUBNUB_MESSAGE_SENT_OK = 'pubnub.message.sent'
    PUBNUB_MESSAGE_SENT_FAIL = 'pubnub.message.fail'
    PUBNUB_MESSAGE_SENT = 'pubnub.message.time'
    PUBNUB_MESSAGE_RETRIES = 'pubnub.message.retries'
    PUBNUB_GET_LONG_MESSAGE = 'socrative.views.pubnub.get_long_message.time'
    PUBNUB_GET_LONG_MESSAGE_FAIL = 'socrative.views.pubnub.get_long_message.fail'