# -*- coding: utf-8 -*-
import logging

from common.base_view import BaseView

logger = logging.getLogger(__name__)

from common.socrative_api import exceptionStack
from socrative_tornado.serializers import GetLongMessageIdDTO
from common.counters import DatadogThreadStats
from socrative_tornado.counters import PubnubCounters

statsd = DatadogThreadStats.STATS

class LongMessageView(BaseView):
    """
    class to handle publish subscribe messages
    """

    http_method_names = ["get"]

    @statsd.timed(PubnubCounters.PUBNUB_GET_LONG_MESSAGE)
    def get(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        failCounter = PubnubCounters.PUBNUB_GET_LONG_MESSAGE_FAIL

        data = dict()
        data.update(request.GET)
        data.update(kwargs)

        dto = GetLongMessageIdDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        try:
            obj = request.tornadoService.cacheRegistry.longMessageCache.get(dto.pk)
            if obj:
                return self.render_response(obj, statusCode=200)

        except Exception as e:
            logger.error(exceptionStack(e))
            pass

        response = request.tornadoService.getLongMessage(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


