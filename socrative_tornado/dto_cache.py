# coding=utf-8
from common.base_cache import BaseCache
from common.socrative_api import exceptionStack, safeToInt
import logging

logger = logging.getLogger(__name__)


class LongMessageCache(BaseCache):
    """
    base class to handle caching of dto json responses
    """

    def __init__(self, redisPool, expireTime):
        """

        :param redisPool:
        :param expireTime:
        :return:
        """

        super(LongMessageCache, self).__init__(redisPool)
        self.expireTime = safeToInt(expireTime) or 84100

    def cacheData(self, longMessageId, data):
        """
        :param longMessageId:
        :param data:
        :return:
        """

        try:
            key = "long_message_%d" % longMessageId
            self.getConnection(False).set(key, data, expireTime=self.expireTime)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateData(self, longMessageId):
        """

        :param longMessageId:
        :return:
        """
        try:
            key = "long_message_%d" % longMessageId
            self.getConnection(False).delete(key)
        except Exception as e:
            logger.error(exceptionStack(e))

    def get(self, longMessageId):
        """
        get the current activity
        :param longMessageId:
        :return:
        """

        try:
            key = "long_message_%d" % longMessageId
            obj = self.getConnection(True).get(key)
            return obj
        except Exception as e:
            logger.error(exceptionStack(e))