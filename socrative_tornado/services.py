# -*- coding: utf-8 -*-
from common import http_status as status
from common.socrative_errors import BaseError
from common.socrative_api import exceptionStack

import logging
logger = logging.getLogger(__name__)
from common.base_services import BaseService
from socrative_tornado.serializers import LongMessage
from common.base_dao import BaseDao


class TornadoServices(BaseService):

    def __init__(self, daoRegistry, cacheRegistry):
        """
        Constructor
        :return:
        """
        super(TornadoServices, self).__init__(daoRegistry, cacheRegistry)

    def getLongMessage(self, request, dto):

        try:
            message = self.daoRegistry.longMessageDao.getLongMessageById(dto.pk)
            respDict = LongMessage.fromSocrativeDict(message)

            self.cacheRegistry.longMessageCache.cacheData(dto.pk, respDict)

            return respDict, status.HTTP_200_OK
        except BaseDao.NotFound:
            return BaseError.LONG_MESSAGE_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR
