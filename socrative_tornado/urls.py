# coding=utf-8
from django.conf.urls import url

from socrative_tornado.views import LongMessageView

urlpatterns = [
    url(r'^api/long-message/(?P<pk>[0-9]+)/?$', LongMessageView.as_view()),
]