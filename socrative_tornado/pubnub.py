# -*- coding: utf-8 -*-
## www.pubnub.com - PubNub Real-time push service in the cloud.
# coding=utf8

## PubNub Real-time Push APIs and Notifications Framework
## Copyright (c) 2010 Stephen Blum
## http://www.pubnub.com/

## -----------------------------------
## PubNub 3.0 Real-time Push Cloud API
## -----------------------------------

import ujson
import time
import hashlib
import uuid
import logging
import requests
import datetime
from datadog import statsd
from socrative_tornado.counters import PubnubCounters

logger = logging.getLogger(__name__)


class LongRespTimeException(Exception):

    def __init__(self, *args, **kwargs):
        """

        """
        super(LongRespTimeException, self).__init__(*args, **kwargs)


class Pubnub(object):
    def __init__(
        self,
        publish_key,
        subscribe_key,
        secret_key=False,
        ssl_on=False,
        origin='pubsub.pubnub.com',
        pres_uuid=None
    ):
        """
        #**
        #* Pubnub
        #*
        #* Init the Pubnub Client API
        #*
        #* @param string publish_key required key to send messages.
        #* @param string subscribe_key required key to receive messages.
        #* @param string secret_key optional key to sign messages.
        #* @param boolean ssl required for 2048 bit encrypted messages.
        #* @param string origin PUBNUB Server Origin.
        #* @param string pres_uuid optional identifier for presence (auto-generated if not supplied)
        #**

        ## Initiat Class
        pubnub = Pubnub( 'PUBLISH-KEY', 'SUBSCRIBE-KEY', 'SECRET-KEY', False )

        """
        self.origin        = origin
        self.limit         = 1800
        self.publish_key   = publish_key
        self.subscribe_key = subscribe_key
        self.secret_key    = secret_key
        self.ssl           = ssl_on

        self.fallback_url = "https://pubsub-napac.pubnub.com"
        self.fallback = False
        self.retryTime = None
        self.consecutiveLongRequests = 0

        if self.ssl:
            self.origin = 'https://' + self.origin
        else:
            self.origin = 'http://' + self.origin

        self.session = requests.session()
        
        self.uuid = pres_uuid or str(uuid.uuid4())
        
        if not isinstance(self.uuid, basestring):
            raise AttributeError("pres_uuid must be a string")

    def publish(self, args):
        """
        #**
        #* Publish
        #*
        #* Send a message to a channel.
        #*
        #* @param array args with channel and message.
        #* @return array success information.
        #**

        ## Publish Example
        info = pubnub.publish({
            'channel' : 'hello_world',
            'message' : {
                'some_text' : 'Hello my World'
            }
        })
        print(info)

        """
        ## Fail if bad input.
        if not (args['channel'] and args['message']):
            return [0, 'Missing Channel or Message']

        ## Capture User Input
        channel = str(args['channel']) if type(args['channel']) is str else args['channel'].encode('utf-8')
        message = ujson.dumps(args['message'])

        ## Sign Message
        if self.secret_key:
            signature = hashlib.md5('/'.join([
                self.publish_key,
                self.subscribe_key,
                self.secret_key,
                channel,
                message
            ])).hexdigest()
        else:
            signature = '0'

        ## Send Message
        return self._request([
            'publish',
            self.publish_key,
            self.subscribe_key,
            signature,
            channel,
            '0',
            message
        ])

    def subscribe(self, args):
        """
        #**
        #* Subscribe
        #*
        #* This is BLOCKING.
        #* Listen for a message on a channel.
        #*
        #* @param array args with channel and callback.
        #* @return false on fail, array on success.
        #**

        ## Subscribe Example
        def receive(message) :
            print(message)
            return True

        pubnub.subscribe({
            'channel'  : 'hello_world',
            'callback' : receive 
        })

        """

        ## Fail if missing channel
        if not 'channel' in args:
            raise Exception('Missing Channel.')

        ## Fail if missing callback
        if not 'callback' in args:
            raise Exception('Missing Callback.')

        ## Capture User Input
        channel = str(args['channel'])
        callback = args['callback']
        subscribe_key = args.get('subscribe_key') or self.subscribe_key

        ## Begin Subscribe
        while True:

            timetoken = 'timetoken' in args and args['timetoken'] or 0
            try:
                ## Wait for Message
                response = self._request(self._encode([
                    'subscribe',
                    subscribe_key,
                    channel,
                    '0',
                    str(timetoken)
                ])+['?uuid='+self.uuid], encode=False)

                messages = response[0]
                args['timetoken'] = response[1]

                ## If it was a timeout
                if not len(messages):
                    continue

                ## Run user Callback and Reconnect if user permits.
                for message in messages:
                    if not callback(message):
                        return
            except Exception as e:
                logger.debug(str(e))
                time.sleep(1)

        return True
    
    def presence(self, args):
        """
        #**
        #* presence
        #*
        #* This is BLOCKING.
        #* Listen for presence events on a channel.
        #*
        #* @param array args with channel and callback.
        #* @return false on fail, array on success.
        #**

        ## Presence Example
        def pres_event(message) :
            print(message)
            return True

        pubnub.presence({
            'channel'  : 'hello_world',
            'callback' : receive 
        })
        """

        ## Fail if missing channel
        if not 'channel' in args:
            raise Exception('Missing Channel.')

        ## Fail if missing callback
        if not 'callback' in args:
            raise Exception('Missing Callback.')

        ## Capture User Input
        channel = str(args['channel'])
        callback = args['callback']
        subscribe_key = args.get('subscribe_key') or self.subscribe_key
        
        return self.subscribe({'channel': channel+'-pnpres', 'subscribe_key': subscribe_key, 'callback': callback})

    def here_now(self, args):
        """
        #**
        #* Here Now
        #*
        #* Load current occupancy from a channel.
        #*
        #* @param array args with 'channel'.
        #* @return mixed false on fail, array on success.
        #*

        ## Presence Example
        here_now = pubnub.here_now({
            'channel' : 'hello_world',
        })
        print(here_now['occupancy'])
        print(here_now['uuids'])

        """
        channel = str(args['channel'])
        
        ## Fail if bad input.
        if not channel:
            raise Exception('Missing Channel')
        
        ## Get Presence Here Now
        return self._request([
            'v2', 'presence',
            'sub_key', self.subscribe_key,
            'channel', channel
        ])

    def history(self, args):
        """
        #**
        #* History
        #*
        #* Load history from a channel.
        #*
        #* @param array args with 'channel' and 'limit'.
        #* @return mixed false on fail, array on success.
        #*

        ## History Example
        history = pubnub.history({
            'channel' : 'hello_world',
            'limit'   : 1
        })
        print(history)

        """
        ## Capture User Input
        limit = 'limit' in args and int(args['limit']) or 10
        channel = str(args['channel'])

        ## Fail if bad input.
        if not channel:
            raise Exception('Missing Channel')

        ## Get History
        return self._request([
            'history',
            self.subscribe_key,
            channel,
            '0',
            str(limit)
        ])

    def detailedHistory(self, args):
        """
        #**
        #* Detailed History
        #*
        #* Load Detailed history from a channel.
        #*
        #* @param array args with 'channel', optional: 'start', 'end', 'reverse', 'count'
        #* @return mixed false on fail, array on success.
        #*

        ## History Example
        history = pubnub.detailedHistory({
            'channel' : 'hello_world',
            'count'   : 5
        })
        print(history)

        """
        ## Capture User Input
        channel = str(args['channel'])

        params = [] 
        count = 100    
        
        if 'count' in args:
            count = int(args['count'])

        params.append('count' + '=' + str(count))    
        
        if 'reverse' in args:
            params.append('reverse' + '=' + str(args['reverse']).lower())

        if 'start' in args:
            params.append('start' + '=' + str(args['start']))

        if 'end' in args:
            params.append('end' + '=' + str(args['end']))

        ## Fail if bad input.
        if not channel:
            raise Exception('Missing Channel')

        ## Get History
        return self._request([
            'v2',
            'history',
            'sub-key',
            self.subscribe_key,
            'channel',
            channel,
        ], params=params)

    def time(self):
        """
        #**
        #* Time
        #*
        #* Timestamp from PubNub Cloud.
        #*
        #* @return int timestamp.
        #*

        ## PubNub Server Time Example
        timestamp = pubnub.time()
        print(timestamp)

        """
        return self._request([
            'time',
            '0'
        ])[0]

    def _encode(self, request):
        return [
            "".join([' ~`!@#$%^&*()+=[]\\{}|;\':",./<>?'.find(ch) > -1 and
                hex(ord(ch)).replace('0x', '%').upper() or
                ch for ch in list(bit)
            ]) for bit in request]

    @statsd.timed(PubnubCounters.PUBNUB_MESSAGE_SENT)
    def _request(self, request, origin=None, encode=True, params=None):
        ## Build URL

        now = datetime.datetime.utcnow()
        if self.fallback and self.retryTime < now:
            url = self.fallback_url + "/"
        else:
            url = (origin or self.origin) + "/"

        url += "/".join(encode and self._encode(request) or request)

        # # Add query params
        if params is not None and len(params) > 0:
            url = url + "?" + "&".join(params)

        # # Send Request Expecting JSONP Response
        try:
            # retry 5 times
            retry = 0
            response = None
            while True:
                st_time = datetime.datetime.utcnow()
                response = self.session.get(url, data=None, timeout=10)
                end_time = datetime.datetime.utcnow()

                if (end_time-st_time).seconds >= 1:
                    logger.warn("PUBNUB slow response: Start time: %s | End time: %s | URL Length: %d" % (str(st_time),
                                                                                                          str(end_time),
                                                                                                          len(url)))
                    self.consecutiveLongRequests += 1

                else:
                    self.consecutiveLongRequests = 0

                if response.status_code == 200:
                    statsd.increment(PubnubCounters.PUBNUB_MESSAGE_SENT_OK)
                    return ujson.loads(response.content)

                retry += 1
                statsd.increment(PubnubCounters.PUBNUB_MESSAGE_RETRIES)

                # if it failed 5 times make sure we're preparing to switch if it goes on
                self.consecutiveLongRequests += 1

                if retry > 5:
                    break


            logger.warn("Pubnub message failed after 5 failed attempts: %s " % url)

        except Exception as e:
            statsd.increment(PubnubCounters.PUBNUB_MESSAGE_SENT_FAIL)
            logger.error(str(e))
            # renew the session if something went wrong
            self.session = requests.session()
            return None
        finally:
            # if there are more than 5 consecutive long requests - switch to the fallback url, or
            # if the fallback url is causing problems switch back to the original url
            if self.consecutiveLongRequests >= 5 and self.fallback is False:
                self.fallback = True
                self.retryTime = datetime.datetime.utcnow() + datetime.timedelta(hours=1)
                self.consecutiveLongRequests = 0
            elif self.consecutiveLongRequests >=5 and self.fallback is True:
                self.fallback = False
                self.retryTime = None
                self.consecutiveLongRequests = 0
