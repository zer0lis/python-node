# coding=utf-8
from django.db import models


class LongMessage(models.Model):
    """
    Long message model structure to be stored in the database
    """

    message = models.TextField(max_length=70000)
