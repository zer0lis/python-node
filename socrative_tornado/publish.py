# -*- coding: utf-8 -*-
import ujson
from django.conf import settings
import logging
from socrative_tornado.pubnub import Pubnub
from socrative_tornado.models import LongMessage

from common.spring_container import SpringItem
from common.socrative_api import isStringNotEmpty, safeToBool, byteLen

MAX_PUBNUB_MESSAGE_LENGTH = 4096

logger = logging.getLogger(__name__)


class PubnubWrapperSpringInitializer(SpringItem):
    """
    spring initializer
    """

    def __init__(self, publish_key, subscribe_key, secret_key, ssl_on):
        """
        Constructor
        :param publish_key:
        :param subscribe_key:
        :param secret_key:
        :param ssl_on:
        :return:
        """

        super(PubnubWrapperSpringInitializer, self).__init__()

        if not isStringNotEmpty(publish_key):
            raise ValueError("pubnub publish key is None or not string: %s" % str(type(publish_key)))

        if not isStringNotEmpty(subscribe_key):
            raise ValueError("pubnub subscribe key is None or not string: %s" % str(type(subscribe_key)))

        if not isStringNotEmpty(secret_key):
            raise ValueError("pubnub secret key is None or not string: %s" % str(type(secret_key)))


        self.__publish_overwritten = None
        self.__publishKey = publish_key
        self.__subscribeKey = subscribe_key
        self.__secretKey = secret_key
        self.__sslOn = safeToBool(ssl_on) or False

        logger.info("Updating the PubnubWrapper with configuration settings")

    def get_publish_overwritten(self):
        """
        GETTER
        :return:
        """
        return self.__publish_overwritten

    def set_publish_overwritten(self, value):
        """
        SETTER
        :return:
        """
        self.__publish_overwritten = safeToBool(value) or False

    def destroy(self):
        """
        Required for Spring

        :return Nothing
        """
        pass

    def after_properties_set(self):
        """
        called after publish_overwritten is set
        :return:
        """
        PubnubWrapper.publish_key = self.__publishKey
        PubnubWrapper.subscribe_key = self.__subscribeKey
        PubnubWrapper.secret_key = self.__secretKey
        PubnubWrapper.ssl_on = self.__sslOn
        PubnubWrapper.setup_finished = True

        PubnubWrapper._pubnub = Pubnub(PubnubWrapper.publish_key, PubnubWrapper.subscribe_key, PubnubWrapper.secret_key,
                                       PubnubWrapper.ssl_on)
        logger.info("Pubnub wrapper set up")

    publish_overwritten = property(get_publish_overwritten, set_publish_overwritten)


class PubnubWrapper(object):
    publish_overwritten = False
    publish_key = None
    subscribe_key = None
    secret_key = None
    ssl_on = None
    _pubnub = None
    setup_finished = False

    if setup_finished:
        _pubnub = Pubnub(
            publish_key,  # publish key
            subscribe_key,  # subscribe key
            secret_key,  # secret key
            ssl_on  # SSL
        )

    @staticmethod
    def override_publish(func):
        PubnubWrapper.setup_finished = True
        PubnubWrapper._pubnub = Pubnub("test", "test", "test", False)
        PubnubWrapper.publish_overwritten = True
        PubnubWrapper._pubnub.publish = func


def send_model(channel, **kwargs):
    if settings.TEST and not PubnubWrapper.publish_overwritten:
        raise Exception("Attempting to use the pubnub service from a unit test."
                        " You should probably be using MockPubnubHandler")

    #startTime = time.time()

    data = {
        "key": kwargs["key"],
        "data": kwargs["data"]
    }

    # jsonData = json.dumps(data, cls=DateSupportedEncoder)

    jsonData = ujson.dumps(data)

    if byteLen(channel) + byteLen(jsonData) >= MAX_PUBNUB_MESSAGE_LENGTH:
        longMessage = LongMessage(message=jsonData)
        longMessage.save()

        publishData = {
            'channel': channel,
            'message': "{\"long_message_id\": " + str(longMessage.pk) + "}"
        }
    else:
        publishData = {
            'channel': channel,
            'message': jsonData
        }

    # publish the message
    PubnubWrapper._pubnub.publish(publishData)