# coding=utf-8
from common.base_dao import BaseDao, BaseModel, retry
from common.socrative_api import exceptionStack
from psycopg2 import OperationalError
import logging
from common.counters import DatadogThreadStats

logger = logging.getLogger(__name__)
statsd = DatadogThreadStats.STATS


class LongMessageModel(BaseModel):
    """

    """

    ID = "id"
    MESSAGE = "message"

    def __init__(self):
        """
        Constructor
        """
        self.id = None
        self.message = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.MESSAGE] = self.message
        obj[self.ID] = self.id
        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = LongMessageModel()
        model.id = dictObj.get(cls.ID)
        model.message = dictObj.get(cls.MESSAGE)

        return model


class LongMessageDao(BaseDao):
    """

    """

    FETCH_MANY_SIZE = 100
    MODEL = LongMessageModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(LongMessageDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.long_message.get_long_message_by_id.time')
    def getLongMessageById(self, mId):
        """
        get the message by id
        :param mId:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM socrative_tornado_longmessage WHERE id=%s""", (mId,))

                return self.fetchOneAsDict(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()