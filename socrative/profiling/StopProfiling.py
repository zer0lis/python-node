# -*- coding: utf-8 -*-
__author__ = 'bogdan'

import logging

from django.views.generic import View
from django.http import HttpResponse
from socrative.profiling import base_profiling
from common.socrative_api import byteLen
logger = logging.getLogger(__name__)


class StopProfilingView(View):
    """
    view for stopping the profiling
    """
    allowed_methods = ('GET',)
    exclude = ()

    #noinspection PyUnusedLocal
    def get(self, request, *args, **kwargs):
        """
        :param request: HttpRequest
        :param args: List of variable arguments
        :param kwargs: Key-value variable list of arguments
        """

        logger.info("YappiDump IN")

        try:
            base_profiling.stopProfiling("a", "b")
            responseData = "YappiDump OK\n"
        except Exception as e:
            responseData = str(e)

        httpResp = HttpResponse(responseData, "application/json")
        httpResp['Content-Length'] = byteLen(responseData)

        # Over
        return httpResp