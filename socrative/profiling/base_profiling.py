# -*- coding: utf-8 -*-
"""
    Yappi
    From http://code.google.com/p/yappi/

    * *name*: name of the function being profiled
    * *ccnt*: is the total callcount of the function.
    * *tsub*: total time spent in the function excluding sub-calls.
    * *ttot*: total time spent in the function.
    * *tavg*: is same as ttot / ccnt. Average total time.

    * *tname*: name of the thread object.
      * *tstart*: start time of the profiler.
      * *fname*: name of the last executed function in this thread.
      * *schedc*: number of times this thread is scheduled.
      * *ttot*: total time spent in this thread.



"""
import sys
#noinspection PyPackageRequirements
import signal
import yappi

callCount = 0

import os

def StartProfiling():
    """
    Start the profile
    """

    currentPath = os.path.join(*os.path.split(os.path.abspath(__file__))[:-1])
    os.sys.path.insert(0, os.path.abspath(os.path.join(currentPath, '..')))

    from django.core.management import execute_from_command_line
    import imp
    try:
        imp.find_module('settings')  # Assumed to be in the same directory.
    except ImportError:
        import sys
        sys.stderr.write("Error: Can't find the file 'settings.py' in the directory containing %r."
                         " It appears you've customized things.\nYou'll have to run django-admin.py,"
                         " passing it your settings module.\n" % __file__)
        sys.exit(1)
    #noinspection PyUnresolvedReferences
    import settings
    execute_from_command_line(settings)

def runProfile():
    """
    Run
    """

    signal.signal(signal.SIGTERM, stopProfiling)
    signal.signal(signal.SIGINT, stopProfiling)

    # Start yappi
    yappi.start()

    try:
        StartProfiling()
    except KeyboardInterrupt:
        print "stopped"
    finally:
        stopProfiling(1, 2)

def stopProfiling(a, b):
    """
    Stop
    :param a: Test
    :param b: Test

    """
    # Stop
    yappi.stop()

    outFile = open("socrative_profiling.txt", "w")

    outFile.write("")
    outFile.write("=======================")
    outFile.write("= Yappi :  SORTTYPE_NAME")
    outFile.write("=======================")

    printCustomStat(outFile, yappi.SORT_TYPES_FUNCSTATS["name"], yappi.SORT_ORDERS["desc"])

    outFile.write("")
    outFile.write("=======================")
    outFile.write("= Yappi :  SORTTYPE_NCALL")
    outFile.write("=======================")

    printCustomStat(outFile, yappi.SORT_TYPES_FUNCSTATS["ncall"], yappi.SORT_ORDERS["desc"])

    outFile.write("")
    outFile.write("=======================")
    outFile.write("= Yappi :  SORTTYPE_TTOT")
    outFile.write("=======================")

    printCustomStat(outFile, yappi.SORT_TYPES_FUNCSTATS["ttot"], yappi.SORT_ORDERS["desc"])

    outFile.write("")
    outFile.write("=======================")
    outFile.write("= Yappi :  SORTTYPE_TSUB")
    outFile.write("=======================")

    printCustomStat(outFile, yappi.SORT_TYPES_FUNCSTATS["tsub"], yappi.SORT_ORDERS["desc"])

    outFile.write("")
    outFile.write("=======================")
    outFile.write("= Yappi :  SORTTYPE_TAVG")
    outFile.write("=======================")

    printCustomStat(outFile, yappi.SORT_TYPES_FUNCSTATS["tavg"], yappi.SORT_ORDERS["desc"])

    outFile.close()

    outFile = open("socrative_profiling.csv", "w")
    printCsv(outFile, yappi.SORT_TYPES_FUNCSTATS["name"], yappi.SORT_ORDERS["desc"])
    outFile.close()
    yappi.clear_stats()

def printCustomStat(out=sys.stdout, sort_type=yappi.SORT_TYPES_FUNCSTATS["ncall"], sort_order=yappi.SORT_ORDERS["desc"],
                    thread_stats_on=True):
    """
    Print custom stat.
    :param out: Out target.
    :param sort_type: Sort type.
    :param sort_order: Sort order.
    :param thread_stats_on: Thread stat on/off.
    :return: Nothing.
    """
    stats = yappi.get_func_stats()

    FUNC_NAME_LEN = 64 + 8
    THREAD_FUNC_NAME_LEN = 64 + 8
    CALLCOUNT_LEN = 16
    TIME_COLUMN_LEN = 12 # 0.000000, 12345.98, precision is microsecs
    COLUMN_GAP = 6
    THREAD_NAME_LEN = 32
    THREAD_ID_LEN = 16
    THREAD_SCHED_CNT_LEN = 16

    out.write(yappi.CRLF)
    out.write(yappi.StatString("name").ltrim(FUNC_NAME_LEN))
    out.write(" " * COLUMN_GAP)
    out.write(yappi.StatString("#callcount").rtrim(CALLCOUNT_LEN))
    out.write(" " * COLUMN_GAP)
    out.write(yappi.StatString("self").rtrim(TIME_COLUMN_LEN))
    out.write(" " * COLUMN_GAP)
    out.write(yappi.StatString("all").rtrim(TIME_COLUMN_LEN))
    out.write(" " * COLUMN_GAP)
    out.write(yappi.StatString("self.avg").rtrim(TIME_COLUMN_LEN))
    out.write(" " * COLUMN_GAP)
    out.write(yappi.StatString("all.avg").rtrim(TIME_COLUMN_LEN))
    out.write(yappi.CRLF)

    out.write(yappi.CRLF)
    for stat in stats:
        out.write(yappi.StatString(stat.name).ltrim(FUNC_NAME_LEN))
        out.write(" " * COLUMN_GAP)
        out.write(yappi.StatString(stat.ncall).rtrim(CALLCOUNT_LEN))
        out.write(" " * COLUMN_GAP)
        out.write(yappi.StatString("%0.6f" % stat.tsub).rtrim(TIME_COLUMN_LEN))
        out.write(" " * COLUMN_GAP)
        out.write(yappi.StatString("%0.6f" % stat.ttot).rtrim(TIME_COLUMN_LEN))
        out.write(" " * COLUMN_GAP)
        out.write(yappi.StatString("%0.6f" % (stat.tsub / stat.ncall)).rtrim(TIME_COLUMN_LEN))
        out.write(" " * COLUMN_GAP)
        out.write(yappi.StatString("%0.6f" % stat.tavg).rtrim(TIME_COLUMN_LEN))
        out.write(yappi.CRLF)

    if thread_stats_on:
        out.write(yappi.CRLF)
        out.write(yappi.StatString("name").ltrim(THREAD_NAME_LEN))
        out.write(" " * COLUMN_GAP)
        out.write(yappi.StatString("tid").ltrim(THREAD_ID_LEN))
        out.write(" " * COLUMN_GAP)
        out.write(yappi.StatString("fname").ltrim(THREAD_FUNC_NAME_LEN))
        out.write(" " * COLUMN_GAP)
        out.write(yappi.StatString("ttot").ltrim(TIME_COLUMN_LEN))
        out.write(" " * COLUMN_GAP)
        out.write(yappi.StatString("schedc").ltrim(THREAD_SCHED_CNT_LEN))
        out.write(yappi.CRLF)
        for stat in yappi.get_thread_stats():
            out.write(yappi.StatString(stat.name).ltrim(THREAD_NAME_LEN))
            out.write(" " * COLUMN_GAP)
            out.write(yappi.StatString(stat.id).rtrim(THREAD_ID_LEN))
            out.write(" " * COLUMN_GAP)
            out.write(yappi.StatString("%0.6f" % stat.ttot).rtrim(TIME_COLUMN_LEN))
            out.write(" " * COLUMN_GAP)
            out.write(yappi.StatString(stat.sched_count).rtrim(THREAD_SCHED_CNT_LEN))
            out.write(yappi.CRLF)

def printCsv(out=sys.stdout, sort_type=yappi.SORT_TYPES_FUNCSTATS["ncall"], sort_order=yappi.SORT_ORDERS["desc"],
                    thread_stats_on=False):
    """
    Print custom stat.
    :param out: Out target.
    :param sort_type: Sort type.
    :param sort_order: Sort order.
    :param thread_stats_on: Thread stat on/off.
    :return: Nothing.
    """
    stats = yappi.get_func_stats()

    out.write(yappi.StatString("name")._s)
    out.write(";")
    out.write(yappi.StatString("#callcount")._s)
    out.write(";")
    out.write(yappi.StatString("self")._s)
    out.write(";")
    out.write(yappi.StatString("all")._s)
    out.write(";")
    out.write(yappi.StatString("self.avg")._s)
    out.write(";")
    out.write(yappi.StatString("all.avg")._s)
    out.write(yappi.CRLF)

    for stat in stats:
        out.write(yappi.StatString(stat.name)._s)
        out.write(";")
        out.write(yappi.StatString(stat.ncall)._s)
        out.write(";")
        out.write(yappi.StatString("%0.6f" % stat.tsub)._s)
        out.write(";")
        out.write(yappi.StatString("%0.6f" % stat.ttot)._s)
        out.write(";")
        out.write(yappi.StatString("%0.6f" % (stat.tsub / stat.ncall))._s)
        out.write(";")
        out.write(yappi.StatString("%0.6f" % stat.tavg)._s)
        out.write(yappi.CRLF)

    if thread_stats_on:
        out.write(yappi.StatString("name")._s)
        out.write(";")
        out.write(yappi.StatString("tid")._s)
        out.write(";")
        out.write(yappi.StatString("fname")._s)
        out.write(";")
        out.write(yappi.StatString("ttot")._s)
        out.write(";")
        out.write(yappi.StatString("schedc")._s)
        out.write(yappi.CRLF)
        for stat in yappi.get_thread_stats():
            out.write(yappi.StatString(stat.name)._s)
            out.write(";")
            out.write(yappi.StatString(stat.id)._s)
            out.write(";")
            out.write(yappi.StatString("%0.6f" % stat.ttot)._s)
            out.write(";")
            out.write(yappi.StatString(stat.sched_count)._s)
            out.write(yappi.CRLF)


