__author__ = 'bogdan'

# -*- coding: utf-8 -*-
"""
..  py:module:: CreateUserView

"""
from django.views.generic import View
from django.http import HttpResponse
from common.socrative_api import byteLen
import yappi
import logging

logger = logging.getLogger(__name__)


class StartProfilingView(View):
    """
    Handler for ValidateTicket REST method.
    """
    allowed_methods = ('GET',)
    exclude = ()

    #noinspection PyUnusedLocal
    def get(self, request, *args, **kwargs):
        """
        :param request: HttpRequest
        :param args: List of variable arguments
        :param kwargs: Key-value variable list of arguments
        """

        logger.info("StartProfiling IN")

        try:
            yappi.start()
            responseData = "YappiStart OK\n"
        except Exception as e:
            responseData = str(e)

        httpResp = HttpResponse(content=responseData, content_type="application/json")
        httpResp['Content-Length'] = byteLen(responseData)

        # Over
        return httpResp
