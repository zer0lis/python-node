# -*- coding: utf-8 -*-
from django.conf.urls import include, url

urlpatterns = [
    url(r'^', include('common.urls')),
    url(r'^rooms/', include('rooms.urls')),
    url(r'^students/', include('students.urls')),
    url(r'^users/', include('socrative_users.urls')),
    url(r'^quizzes/', include('quizzes.urls')),
    url(r'^lecturers/', include('lecturers.urls')),
    url(r'^socrative-tornado/', include('socrative_tornado.urls'))
]
