# -*- coding: utf-8 -*-
from django.conf import settings
from django.utils.cache import add_never_cache_headers
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseNotAllowed
import os
import ujson

import logging

from boto.s3.connection import S3Connection
from common.socrative_api import exceptionStack, initDefaultLogger
from common.socrative_logging import SocrativeSyslogHandler
from common.socrative_errors import BaseError
from common.spring_container import SpringContainer
from common.socrative_config import ConfigRegistry

logger = logging.getLogger(__name__)


def DisableClientSideCachingMiddleware(get_response):
    def middleware(request):
        response = get_response(request)
        add_never_cache_headers(response)
        return response

    return middleware


XS_SHARING_ALLOWED_ORIGINS = "*"
XS_SHARING_ALLOWED_METHODS = 'POST, PUT, GET, DELETE, OPTIONS'
XS_SHARING_ALLOWED_HEADERS = 'Origin,Content-Type,Accept,X-CSRFToken'


def XsSharing(get_response):
    """
    This middleware allows cross-domain XHR using the html5 postMessage API.
    Access-Control-Allow-Origin: http://foo.example
    Access-Control-Allow-Methods: POST, GET, OPTIONS
    """

    def middleware(request):
        """
        Process request
        :param request: Request
        """

        response = None
        if 'HTTP_ACCESS_CONTROL_REQUEST_METHOD' in request.META:
            request.isIE = False
            response = HttpResponse()
            response['Access-Control-Allow-Origin'] = ConfigRegistry.getItem("CORS_ALLOWED_HOST") or "*"
            response['Access-Control-Allow-Methods'] = XS_SHARING_ALLOWED_METHODS
            response['Access-Control-Allow-Headers'] = XS_SHARING_ALLOWED_HEADERS
            response['Access-Control-Allow-Credentials'] = 'true'
            response['Access-Control-Max-Age'] = 86400
            response.status_code = 204

        if not response:
            response = get_response(request)

        # Process outgoing HttpResponse.
        if "HTTP_ORIGIN" in request.META:
            response["Origin"] = request.META["HTTP_ORIGIN"]
        response['Access-Control-Allow-Origin'] = ConfigRegistry.getItem("CORS_ALLOWED_HOST") or "*"
        response['Access-Control-Allow-Credentials'] = 'true'

        return response

    return middleware


class SpringInitializer(object):
    """
    class used to initialize the spring objects and to insert the container object into the request
    """

    # Init flag
    def __init__(self, get_response):
        """

        :param get_response:
        """

        self.get_response = get_response

        self.initOk = False

        self.firstRequest = True

        # Current path
        self.currentDir = os.path.dirname(os.path.abspath(__file__)) + os.sep + "../conf/platform/"

    def __call__(self, request):
        """
        inserts the container inside the request
        :param request: the HTTP request , on which we add the spring container
        :return: None
        """

        resp = None

        if self.firstRequest:
            try:

                initDefaultLogger(SocrativeSyslogHandler)

                logger.info("__instantiate spring objects : currentDir=%s", self.currentDir)

                # Get container
                logger.info("Getting container")
                self.container = SpringContainer(os.path.join(self.currentDir, "socrative.conf"),
                                            os.path.join(self.currentDir, "socrative.%s.property" % settings.PLATFORM))

                logger.info("Storing email thread pool in request")
                self.tasksClient = self.container.getItem('workerTaskClient')
                self.s3Connection = S3Connection(ConfigRegistry.getItem("AWS_ACCESS_KEY_ID"),
                                            ConfigRegistry.getItem("AWS_SECRET_ACCESS_KEY"))
                self.s3Bucket = self.s3Connection.get_bucket(ConfigRegistry.getItem("AWS_STORAGE_BUCKET_NAME"))
                self.studentService = self.container.getItem('studentService')
                self.quizService = self.container.getItem('quizService')
                self.commonService = self.container.getItem('commonService')
                self.roomService = self.container.getItem('roomService')
                self.lecturerService = self.container.getItem('lecturerService')
                self.userService = self.container.getItem('usersService')
                self.tornadoService = self.container.getItem('tornadoService')

                # Init flag
                self.initOk = True

            except Exception as e:
                print exceptionStack(e)
                logger.error("__SpringInitializer.init :  Exception, ex=%s", exceptionStack(e))
            finally:
                self.firstRequest = False

        if self.initOk is False:
            # Not initialized or error during init
            resp = HttpResponse(status=502, content="Bad spring configuration")

        if self.initOk and request is not None:
            request.tasksClient = self.tasksClient
            request.s3bucket = self.s3Bucket
            request.studentService = self.studentService
            request.quizService = self.quizService
            request.commonService = self.commonService
            request.roomService = self.roomService
            request.lecturerServices = self.lecturerService
            request.usersService = self.userService
            request.tornadoService = self.tornadoService

        if not resp:
            resp = self.get_response(request)

        if type(resp) is HttpResponseNotFound:
            resp.content = ujson.dumps(BaseError.RESOURCE_NOT_FOUND)
            resp['Content-Length'] = len(resp.content)
        elif type(resp) is HttpResponseNotAllowed:
            resp.content = ujson.dumps(BaseError.METHOD_NOT_ALLOWED)
            resp['Content-Length'] = len(resp.content)
        return resp

