# -*- coding: utf-8 -*-
import os
import sys
import dj_database_url
from datadog import initialize


DATADOG_API_KEY = os.environ.get("DATADOG_API_KEY")
DATADOG_APP_KEY = os.environ.get("DATADOG_APP_KEY")

if not DATADOG_API_KEY or not DATADOG_APP_KEY:
    raise Exception("Please set the DATADOG_API_KEY and DATADOG_APP_KEY and try again")

initialize(api_key=DATADOG_API_KEY, app_key=DATADOG_APP_KEY)

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

DATABASES = {}
DATABASES['default'] = dj_database_url.config()

PLATFORM = os.environ.get("SOCRATIVE_PLATFORM")
if PLATFORM is None:
    raise Exception("Please set SOCRATIVE_PLATFORM. Eg: DEV, PROD, QA, QA1 etc...")


TEST = 'test' in sys.argv
if TEST or bool(os.environ.get("SOCRATIVE_NO_DB", False)):
    DATABASES['default'] = {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'test_db'
    }
else:
    DATABASES['default']['CONN_MAX_AGE'] = 3600

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

DEBUG = PLATFORM == "LOCAL"
TEST = PLATFORM == "JENKINS"
if not TEST:
    ADMINS = (
        ('Rob Johansen', 'rjohansen@masteryconnect.com'),
    )

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']
LANGUAGE_CODE = 'en-us'
SITE_ID = 1
USE_I18N = True
USE_L10N = True

SECRET_KEY = '4f70ow-bwn*3dy8a8a$#2q03984^o&w@$%f78w*6ubr)klb#'

MIDDLEWARE = [
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'socrative.middleware.SpringInitializer',
    'socrative.middleware.XsSharing',
    'django.middleware.csrf.CsrfViewMiddleware',
    'socrative.middleware.DisableClientSideCachingMiddleware',
]


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
        ],
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.i18n',
            ],
            'loaders': [
            ]
        },
        # 'DEBUG': DEBUG,
    },
]

ROOT_URLCONF = 'socrative.urls'
WSGI_APPLICATION = 'socrative.wsgi.application'


INSTALLED_APPS = (
    # Socrative Apps
    'rooms',
    'lecturers',
    'students',
    'quizzes',
    'common',
    'socrative_users',
    'socrative_tornado'
)

APPEND_SLASH = False

LOGGING = {
    'disable_existing_loggers': False,
    'version': 1
    }

LOCALE_PATHS = (
    os.path.abspath(os.path.dirname(__file__)+"/../conf/locale"),
)

LANGUAGE_COOKIE_NAME = "socrative_lang"
USER_AUTH_TOKEN = "ua"
STUDENT_AUTH_TOKEN = "sa"

LANGUAGES = [
    ["nl", "Dutch"],
    ["en", "English"],
    ["en-gb", "English (UK)"],
    ["fr-ca", "French (Canadian)"],
    ["fr", "French"],
    ["ko", "Korean"],
    ["pt-br", "Portuguese (Brazil)"],
    ["es", "Spanish"],
    ["es-mx", "Spanish (Mexico)"],
    ["zh-cn", "Chinese (Simplified)"],
    ["da", "Danish"],
    ["fi", "Finnish"],
    ["de", "German"],
    ["ms", "Malay"],
    ["sv", "Swedish"],
    ["th", "Thai"],
    ["tr", "Turkish"]
]

RELEASE_NUMBER = 6