# coding=utf-8

# coding=utf-8
from common.base_cache import BaseCache
from common.socrative_api import exceptionStack, safeToInt
from rooms.dao import RoomModel
import logging
import ujson

logger = logging.getLogger(__name__)


class RoomModelCache(BaseCache):
    """

    """

    def __init__(self, redisPool, expireTime):
        """

        :param redisPool:
        :param expireTime:
        :return:
        """

        super(RoomModelCache, self).__init__(redisPool)
        self.expireTime = safeToInt(expireTime) or 84100

    def cacheData(self, room_name, data):
        """
        :param data:
        :param room_name
        :return:
        """

        try:
            key = "rot_%s" % room_name.lower()
            self.getConnection(False).set(key, ujson.dumps(data.toDict()),
                                          expireTime=self.expireTime)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateData(self, room_name):
        """

        :param room_name:
        :return:
        """

        try:
            key = "rot_%s" % room_name.lower()
            self.getConnection(False).delete(key)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateMulti(self, room_names):
        """
        """
        try:
            keys = ["rot_%s" % name.lower() for name in room_names]
            self.getConnection(False).delete_multi(keys)
        except Exception as e:
            logger.error(exceptionStack(e))

    def get(self, room_name):
        """
        get the current activity
        :param room_name:
        :return:
        """
        try:

            key = "rot_%s" % room_name

            obj = self.getConnection(True).get(key)
            if obj is None:
                return None

            resp = RoomModel.fromDict(ujson.loads(obj))
            return resp
        except Exception as e:
            logger.error(exceptionStack(e))


class FreeRoomAttendenceLimiter(BaseCache):
    """
    limits the number of attendees in a free room
    """

    def __init__(self, redisPool, expireTime):
        """

        :param redisPool:
        :param expireTime:
        :return:
        """

        super(FreeRoomAttendenceLimiter, self).__init__(redisPool)
        self.expireTime = safeToInt(expireTime) or 604800  # a week

    def cacheData(self, room_name, user_uuid):
        """
        :param user_uuid:
        :param room_name
        :return:
        """

        try:
            key = "rlim_%s" % room_name
            self.getConnection(False).addToSet(key, user_uuid,  expireTime=self.expireTime)
        except Exception as e:
            logger.error(exceptionStack(e))

    def isMember(self, room_name, user_uuid):
        """

        :param room_name:
        :param user_uuid:
        :return:
        """
        try:
            key = "rlim_%s" % room_name
            res = self.getConnection(True).sismember(key, user_uuid)

            if res:
                return True

            return False
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateData(self, room_name):
        """

        :param room_name:
        :return:
        """

        try:
            key = "rlim_%s" % room_name
            self.getConnection(False).delete(key)
        except Exception as e:
            logger.error(exceptionStack(e))

    def get(self, room_name):
        """
        get the current activity
        :param room_name
        :return:
        """
        try:

            key = "rlim_%s" % room_name

            obj = self.getConnection(True).scard(key)
            if obj is None:
                return 0

            return obj
        except Exception as e:
            logger.error(exceptionStack(e))


class RoomListCache(BaseCache):
    """

    """

    def __init__(self, redisPool, expireTime):
        """

        :param redisPool:
        :param expireTime:
        :return:
        """

        super(RoomListCache, self).__init__(redisPool)
        self.expireTime = safeToInt(expireTime) or 1209600  # two weeks

    def cacheData(self, user_id, data):
        """
        :param data:
        :param user_id
        :return:
        """

        try:
            key = "rl_%d" % user_id
            self.getConnection(False).set(key, ujson.dumps(data),
                                          expireTime=self.expireTime)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateData(self, user_id):
        """

        :param user_id:
        :return:
        """

        try:
            key = "rl_%d" % user_id
            self.getConnection(False).delete(key)
        except Exception as e:
            logger.error(exceptionStack(e))

    def invalidateMulti(self, user_id_list):
        """

        :param user_id_list:
        :return:
        """

        try:
            keys = ["rl_%d" % user_id for user_id in user_id_list]
            if keys:
                self.getConnection(False).delete_multi(keys)
        except Exception as e:
            logger.error(exceptionStack(e))

    def get(self, user_id):
        """
        get the user's room list
        :param user_id:
        :return:
        """
        try:

            key = "rl_%d" % user_id

            obj = self.getConnection(True).get(key)
            if obj is None:
                return None

            resp = ujson.loads(obj)
            return resp
        except Exception as e:
            logger.error(exceptionStack(e))
