# coding=utf-8

from common.base_dto import DTOConverter, ItemConverter
from common.base_dto import RequestDTOValidator, ItemValidator
from string import strip, lower, upper
from common.socrative_api import isStringNotEmpty, safeToInt, isNotNone, room_name_validator, isString, serializerToBool
from common.socrative_api import isBoolOrNone, roster_order_validator, roster_type_validator
from common.socrative_api import isSocrativeStorageUrl
from common.socrative_errors import BaseError
from common import http_status as status
from common import base_limits
from common import soc_constants


class Room(DTOConverter):

    stringifyDict = {
        "id": ItemConverter(),
        "name_lower": ItemConverter(dtoName="name"),
        "created_by_id": ItemConverter(dtoName="created_by")
    }


class GetCurrentActivityRequestDTO(RequestDTOValidator):
    """
    """

    rulesDict = {
        "room_name": ItemValidator(funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.INVALID_ROOM_NAME, status.HTTP_400_BAD_REQUEST)),
        "user_uuid": ItemValidator(mandatory=False, funcList=[isString, strip, isStringNotEmpty],
                                   errorCode=(BaseError.USER_ID_MISSING, status.HTTP_400_BAD_REQUEST)),
        "auth_token": ItemValidator(mandatory=False, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),

    }

    def __init__(self):
        """

        :return:
        """
        self.room_name = None
        self.user_uuid = None
        self.auth_token = None


class UpdateActivityStateRequestDTO(RequestDTOValidator):
    """
    validator for the request data
    """

    rulesDict = {
        "auth_token": ItemValidator(funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.INVALID_ROOM_NAME, status.HTTP_400_BAD_REQUEST)),
        "state_data": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.STATE_DATA_MISSING,
                                                                                status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.auth_token = None
        self.room_name = None
        self.state_data = None


class JoinRoomRequestDTO(RequestDTOValidator):
    """
    validator for the request data
    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=False, defaultValue='', funcList=[isString, strip],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "name": ItemValidator(mandatory=False, defaultValue="", funcList=[isString, strip, lower, room_name_validator],
                              errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST)),
        "tz_offset": ItemValidator(mandatory=False, funcList=[safeToInt, isNotNone], defaultValue=0,
                                   errorCode=(BaseError.INVALID_TZ_OFFSET, status.HTTP_400_BAD_REQUEST)),
        "role": ItemValidator(mandatory=False, funcList=[isString, strip, isStringNotEmpty, lower],
                                   errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.auth_token = None
        self.name = None
        self.tz_offset = 0
        self.role = None


class CreateRoomRequestDTO(RequestDTOValidator):
    """
    validator for the request data
    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=False, funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.auth_token = None
        self.room_name = None


class DeleteRoomDTO(RequestDTOValidator):
    """
    validator for the request data
    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=True, funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.ROOM_ID_MISSING, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.auth_token = None
        self.room_name = None


class UpdateRoomDTO(RequestDTOValidator):
    """
    validator for the request data
    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=True, funcList=[isString, strip, lower, isStringNotEmpty,
                                                             room_name_validator],
                                   errorCode=(BaseError.OLD_ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST)),
        "new_room_name": ItemValidator(mandatory=True, funcList=[isString, strip, lower, isStringNotEmpty,
                                                                 room_name_validator],
                                       errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.auth_token = None
        self.room_name = None
        self.pk = None


class GetRoomListRequestDTO(RequestDTOValidator):
    """
    validator for the request data
    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "in_menu": ItemValidator(mandatory=False, defaultValue=False, funcList=[serializerToBool, isBoolOrNone],
                                 errorCode=(BaseError.INVALID_IN_MENU_FIELD,status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.auth_token = None
        self.in_menu = False


class RoomDTO(DTOConverter):
    """

    """

    stringifyDict = {
        "id": ItemConverter(),
        "name_lower": ItemConverter(dtoName="name"),
        "rostered": ItemConverter(),
        "in_menu": ItemConverter(),
        "default": ItemConverter(),
        "active": ItemConverter(),
        "student_count": ItemConverter(optional=True),
        "students_logged_in": ItemConverter(optional=True),
        "share_url": ItemConverter(optional=True),
    }


class RoomListDTO(DTOConverter):
    """

    """

    stringifyDict = {
        "rooms": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=RoomDTO)
    }


class RoomStatusDTO(RequestDTOValidator):
    """

    """
    rulesDict = {
        "room_id": ItemValidator(funcList=[safeToInt, isNotNone],
                                 errorCode=(BaseError.ROOM_ID_MISSING, status.HTTP_400_BAD_REQUEST)),
        "in_menu": ItemValidator(funcList=[serializerToBool, isNotNone],
                                 errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.room_id = None
        self.in_menu = None


class RoomStatusListDTO(RequestDTOValidator):
    """

    """
    rulesDict = {
        "statuses": ItemValidator(iterator=ItemValidator.LIST, itClass=RoomStatusDTO,
                                  errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST)),
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
    }

    def __init__(self):
        """

        :return:
        """
        self.statuses = []
        self.auth_token = None


class StudentRequestDTO(RequestDTOValidator):
    """
    """

    rulesDict = {
        "last_name": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                   errorCode=(BaseError.LAST_NAME_IS_INVALID, status.HTTP_400_BAD_REQUEST),
                                   funcLimits=[(len, base_limits.STUDENT_NAME_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                      status.HTTP_400_BAD_REQUEST))]),
        "first_name": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.FIRST_NAME_IS_INVALID, status.HTTP_400_BAD_REQUEST),
                                    funcLimits=[(len, base_limits.STUDENT_NAME_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                       status.HTTP_400_BAD_REQUEST))]
                                    ),
        "student_id": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty, upper],
                                    errorCode=(BaseError.STUDENT_ID_MISSING, status.HTTP_400_BAD_REQUEST),
                                    funcLimits=[(len, base_limits.STUDENT_ROSTER_ID_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                            status.HTTP_400_BAD_REQUEST)
                                                 )]
                                    ),
    }

    def __init__(self):
        """

        """

        self.first_name = None
        self.last_name = None
        self.student_id = None


class AddRosteredStudentRequestDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=True, funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST)),
        "students": ItemValidator(mandatory=True, itClass=StudentRequestDTO, iterator=ItemValidator.LIST,
                                  errorCode=(BaseError.STUDENTS_MISSING, status.HTTP_400_BAD_REQUEST),
                                  funcLimits=[(len, base_limits.MAX_STUDENTS_IN_ROSTER, (BaseError.TOO_MANY_STUDENTS,
                                                                                         status.HTTP_400_BAD_REQUEST))])
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.students = []
        self.room_name = None


class DeleteStudentFromRosterRequestDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "id": ItemValidator(mandatory=True, funcList=[safeToInt, isNotNone],
                            errorCode=(BaseError.STUDENT_ID_MISSING, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=True, funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.id = None
        self.room_name = None


class UpdateStudentFromRosterRequestDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "last_name": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                   errorCode=(BaseError.LAST_NAME_IS_INVALID, status.HTTP_400_BAD_REQUEST)),
        "first_name": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.FIRST_NAME_IS_INVALID, status.HTTP_400_BAD_REQUEST)),
        "student_id": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty, upper],
                                    errorCode=(BaseError.STUDENT_ID_MISSING, status.HTTP_400_BAD_REQUEST)),
        "id": ItemValidator(mandatory=True, funcList=[safeToInt, isNotNone],
                            errorCode=(BaseError.STUDENT_ID_MISSING, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=True, funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.last_name = None
        self.first_name = None
        self.student_id = None
        self.old_student_id = None
        self.room_name = None


class GetRosteredStudentsRequestDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=True, funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.room_name = None


class ImportRosterRequestDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=True, funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.room_name = None


class DeleteRosterRequestDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=True, funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        """
        self.auth_token = None
        self.room_name = None


class ExportRosterRequestDTO(RequestDTOValidator):
    """

    """
    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=True, funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST)),
        "type": ItemValidator(mandatory=False, funcList=[isString, strip, lower, isStringNotEmpty],
                              errorCode=(BaseError.INVALID_EXPORT_TYPE, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        """
        self.auth_token = None
        self.room_name = None
        self.type = None


class EndCurrentActivityRequestDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=True, funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.room_name = None


class RoomShareUrlRequestDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=True, funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST)),
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.room_name = None


class HandraiseRoomStatusRequestsDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=True, funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST)),
        "active": ItemValidator(mandatory=True, funcList=[serializerToBool, isNotNone],
                                errorCode=(BaseError.INVALID_ACTIVE_FIELD, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.room_name = None
        self.active = None


class CreateRosterSettingsRequestDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "import_order": ItemValidator(mandatory=False, funcList=[safeToInt, isNotNone, roster_order_validator],
                                      defaultValue=soc_constants.ROSTER_LAST_FIRST_ID,
                                      errorCode=(BaseError.ROSTER_IMPORT_ORDER_INVALID, status.HTTP_400_BAD_REQUEST)),
        "import_type": ItemValidator(mandatory=False, funcList=[safeToInt, isNotNone, roster_type_validator],
                                     defaultValue=soc_constants.ROSTER_FILE_CSV,
                                     errorCode=(BaseError.ROSTER_IMPORT_TYPE_INVALID, status.HTTP_400_BAD_REQUEST)),
        "import_header": ItemValidator(mandatory=False, funcList=[serializerToBool], defaultValue=True,
                                       errorCode=(BaseError.ROSTER_IMPORT_HEADER_INVALID, status.HTTP_400_BAD_REQUEST)),
        "export_order": ItemValidator(mandatory=False, funcList=[safeToInt, isNotNone, roster_order_validator],
                                      defaultValue=soc_constants.ROSTER_LAST_FIRST_ID,
                                      errorCode=(BaseError.ROSTER_EXPORT_ORDER_INVALID, status.HTTP_400_BAD_REQUEST)),
        "export_type": ItemValidator(mandatory=False, funcList=[safeToInt, isNotNone, roster_type_validator],
                                     defaultValue=soc_constants.ROSTER_FILE_CSV,
                                     errorCode=(BaseError.ROSTER_EXPORT_TYPE_INVALID, status.HTTP_400_BAD_REQUEST)),
        "export_header": ItemValidator(mandatory=False, funcList=[serializerToBool], defaultValue=True,
                                       errorCode=(BaseError.ROSTER_EXPORT_HEADER_INVALID, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.import_order = None
        self.import_type = None
        self.import_header = None
        self.export_order = None
        self.export_type = None
        self.export_header = None


class UpdateRosterImportSettingsRequestDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "import_order": ItemValidator(mandatory=False, funcList=[safeToInt, isNotNone, roster_order_validator],
                                      errorCode=(BaseError.ROSTER_IMPORT_ORDER_INVALID, status.HTTP_400_BAD_REQUEST)),
        "import_type": ItemValidator(mandatory=False, funcList=[safeToInt, isNotNone, roster_type_validator],
                                     errorCode=(BaseError.ROSTER_IMPORT_TYPE_INVALID, status.HTTP_400_BAD_REQUEST)),
        "import_header": ItemValidator(mandatory=False, funcList=[serializerToBool],
                                       errorCode=(BaseError.ROSTER_IMPORT_HEADER_INVALID, status.HTTP_400_BAD_REQUEST)),
        "pk": ItemValidator(funcList=[safeToInt, isNotNone],
                            errorCode=(BaseError.INVALID_ROSTER_SETTING_ID, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.import_order = None
        self.import_type = None
        self.import_header = None
        self.pk = None


class UpdateRosterExportSettingsRequestDTO(RequestDTOValidator):
    """

    """

    rulesDict = {
        "auth_token": ItemValidator(mandatory=True, funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "export_order": ItemValidator(mandatory=False, funcList=[safeToInt, isNotNone, roster_order_validator],
                                      errorCode=(BaseError.ROSTER_EXPORT_ORDER_INVALID, status.HTTP_400_BAD_REQUEST)),
        "export_type": ItemValidator(mandatory=False, funcList=[safeToInt, isNotNone, roster_type_validator],
                                     errorCode=(BaseError.ROSTER_EXPORT_TYPE_INVALID, status.HTTP_400_BAD_REQUEST)),
        "export_header": ItemValidator(mandatory=False, funcList=[serializerToBool],
                                       errorCode=(BaseError.ROSTER_EXPORT_HEADER_INVALID, status.HTTP_400_BAD_REQUEST)),
        "pk": ItemValidator(funcList=[safeToInt, isNotNone],
                            errorCode=(BaseError.INVALID_ROSTER_SETTING_ID, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """

        self.auth_token = None
        self.pk = None
        self.export_order = None
        self.export_type = None
        self.export_header = None