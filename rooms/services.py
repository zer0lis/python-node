# -*- coding: utf-8 -*-
# Rooms Services
import random
from psycopg2 import IntegrityError
import string
import datetime
import re
import ujson
import pytz
import logging
import requests
import cStringIO
import magic
import xlrd
import csv
import xlsxwriter

from common import http_status as status
from socrative import settings as projSettings
from common.socrative_errors import BaseError
from common.socrative_api import isStringNotEmpty, exceptionStack, safeToInt, safeToString, room_name_validator
from common.socrative_config import ConfigRegistry
from common.base_services import BaseService
from common.base_dao import BaseDao
from common.dao import PartnerModel
from students.dao import StudentModel
from common import soc_constants
from common.cryptography import Cryptography
from common import base_limits

from rooms.serializers import Room, RoomListDTO
from socrative_users.serializers import UserSystemMessage
from common.serializers import FullActivityReportDTO, StudentActivityInstance
from socrative_users.dao import UserSystemMessageModel, SystemMessageModel
from rooms.dao import RoomModel,  RosterModel, RoomCodeModel, RosterFileSettingModel
from students.serializers import StudentDTO
from rooms.serializers import AddRosteredStudentRequestDTO
from workers.task_types import TaskType


logger = logging.getLogger(__name__)

from socrative_tornado import publish


class RoomServices(BaseService):
    """
    Room services class
    """

    ALPHABET = ''.join([a[0] for a in (string.letters + string.digits) if a not in ('1', '0', 'O', 'o', 'l', 'I')])
    roomRegex = re.compile("^[a-zA-Z0-9]{1,32}$")

    def __init__(self, daoRegistry, cacheRegistry, roomFullLimiter):
        """
        Constructor
        """
        super(RoomServices, self).__init__(daoRegistry, cacheRegistry)

        self.MAX_FREE_ROOMS = 1
        self.MAX_PREMIUM_ROOMS = 10
        self.MAX_ROSTERED_ROOMS = 10
        self.storageSession = None
        self.roomFullLimiter = roomFullLimiter

        self.MAX_ROSTER_FILE_SIZE = 2*1024**2  # max file size to be downloaded

        self.EXTERNAL_REQUESTS_TIMEOUT = 30  # 30 seconds

        self.ALLOWED_ROSTER_MIMETYPES = (
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.ms-excel",
            "application/vnd.ms-office",
            "application/octet-stream",
            "application/excel",
            "application/x-excel",
            "application/x-msexcel",
            "application/zip",
            "text/plain",
            "text/csv"
        )

    def after_properties_set(self):
        """

        :return:
        """

        self.MAX_FREE_ROOMS = ConfigRegistry.getItem("MAX_FREE_ROOMS") or 1
        self.MAX_PREMIUM_ROOMS = ConfigRegistry.getItem("MAX_PREMIUM_ROOMS") or 10
        self.MAX_ROSTERED_ROOMS = ConfigRegistry.getItem("MAX_ROSTERED_ROOMS") or 10
        self.storageSession = requests.session()

    @classmethod
    def check_user(cls, user):
        """
        function used to check if the user has all the fields saved in his profile
            aka: the nagging function
        :param user: User model
        """

        if not isStringNotEmpty(user.organization_type):
            return False

        if not isStringNotEmpty(user.first_name):
            return False

        if not isStringNotEmpty(user.last_name):
            return False

        if not isStringNotEmpty(user.country):
            return False

        if not isStringNotEmpty(user.user_role) or user.user_role == "[]":
            return False

        return True

    def __createNewRoom(self, userId, last_name, first_name, status):
        """

        :param last_name:
        :param status:
        :param userId:
        :param first_name:
        :return:
        """

        #  if the first name and the last name of the user are empty strings, just return none and default to random
        #  strings
        if type(last_name) in (str, unicode) and not isStringNotEmpty(last_name.strip()) and\
                        type(first_name) in (str, unicode) and not isStringNotEmpty(first_name.strip()):
            return None

        user_name = last_name.strip().lower()

        if room_name_validator(user_name) is False:
            user_name = first_name.strip().lower()
            if room_name_validator(user_name) is False:
                return None

        retries = 0
        max_retries = 100
        name = user_name
        name_lower = name.lower()
        room = RoomModel()
        room.created_by_id = userId
        room.status = status

        triedNames = dict()

        while retries < max_retries:
            room.name = name
            room.name_lower = name_lower

            if name_lower not in triedNames and self.daoRegistry.roomDao.checkRoomName(name_lower) is False:
                try:
                    room.id = self.daoRegistry.roomDao.createRoom(room.toDict())
                    break
                except Exception as e:
                    logger.warn(exceptionStack(e))

            triedNames[name_lower] = None

            name = user_name.encode('utf-8') if type(user_name) is str else user_name + str(random.randint(0, 10000))
            name_lower = name.lower()
            retries += 1

        if retries >= max_retries:
            return None

        return room

    def joinRoom(self, request, dto):
        """
        service to handle the join room api
        :param request:
        :param dto:
        """

        successStatus = status.HTTP_200_OK

        user_uuid = dto.auth_token
        roomName = dto.name
        tz_offset = dto.tz_offset
        user = None

        if user_uuid == roomName == "":
            return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST, None, None

        try:

            isStudent = True
            if isStringNotEmpty(user_uuid):
                try:
                    user = self.serviceRegistry.userService.validateUser(user_uuid)
                    if user is None:
                        raise BaseDao.NotFound

                    isStudent = False
                    if user.tz_offset != tz_offset:
                        user.tz_offset = tz_offset
                        self.daoRegistry.usersDao.updateTimezone(user.id, tz_offset)
                        self.cacheRegistry.userModelCache.cacheModelData(user_uuid, ujson.dumps(user.toDict()))
                except BaseDao.NotFound:
                    pass

            room = None
            if user and roomName:
                room = self.validateRoom(roomName.lower())
                if room is None or room.created_by_id != user.id:
                    return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST, None, None
            elif user and user.level == soc_constants.PREMIUM:
                room = self.daoRegistry.roomDao.loadRoomByCreator(user.id)
            elif user and user.level == soc_constants.FREE:
                room_name = self.daoRegistry.roomDao.getDefaultRoom(user.id)
                room = self.validateRoom(room_name)
            else:
                if not isStringNotEmpty(roomName) and dto.role == 'student':
                    return BaseError.STUDENT_AUTH_FAILURE, status.HTTP_400_BAD_REQUEST, None, None
                elif not isStringNotEmpty(roomName) and dto.role == 'teacher':
                    return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST, None, None

                room = self.daoRegistry.roomDao.getRoomByName(roomName.strip().lower())

                if room and room.status & RoomModel.DEFAULT != RoomModel.DEFAULT:
                    # check if the owner is PRO
                    pro_expiration_date = self.daoRegistry.usersDao.getProExpirationDate(room.created_by_id)
                    if pro_expiration_date is None:
                        # the user is not PRO, so we will return an error
                        return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST, None, None

            if room is None:
                if user is not None:
                    # If room doesn't exist, create it for an authenticated user

                    room = self.__createNewRoom(user.id, user.last_name, user.first_name,
                                                RoomModel.DEFAULT | RoomModel.LAST_USED)

                    if room is None:
                        retries = 0
                        max_retries = 100
                        name = user.display_name
                        name_lower = name.lower()
                        room = RoomModel()
                        room.created_by_id = user.id
                        room.status = RoomModel.DEFAULT | RoomModel.LAST_USED

                        while retries < max_retries:
                            room.name = name
                            room.name_lower = name_lower
                            if self.daoRegistry.roomDao.checkRoomName(name_lower) is False:
                                try:
                                    room.id = self.daoRegistry.roomDao.createRoom(room.toDict())
                                    break
                                except Exception as e:
                                    logger.warn(exceptionStack(e))

                            name = ''.join([self.ALPHABET[random.randint(0, len(self.ALPHABET)-1)] for _ in range(7)])
                            name_lower = name.lower()
                            retries += 1

                        if retries >= max_retries:
                            return BaseError.ROOM_ALREADY_EXISTS, status.HTTP_400_BAD_REQUEST, None, None

                    successStatus = status.HTTP_201_CREATED
                else:
                    return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST, None, None

            lang = None
            if isStudent:
                language = None

                try:
                    language = self.daoRegistry.usersDao.getUserLanguage(room.created_by_id)
                except BaseDao.NotFound:
                    pass

                if language:
                    lang = language or request.COOKIES.get(projSettings.LANGUAGE_COOKIE_NAME) or "en"
                else:
                    lang = request.COOKIES.get(projSettings.LANGUAGE_COOKIE_NAME) or "en"

            responseData = Room.fromSocrativeModel(room)
            
            if user is not None:
                msg = None
                # load system messages
                try:

                    if self.serviceRegistry.userService.is_user_premium(user):
                        level = SystemMessageModel.PRO
                    else:
                        level = SystemMessageModel.FREE

                    sysMsg = self.daoRegistry.systemMessageDao.loadLastMessage(level)

                    if sysMsg and (sysMsg.audience == SystemMessageModel.ALL or sysMsg.audience == level):

                        usrMsg = self.daoRegistry.userMessageDao.loadMessage(sysMsg.id, user.id)

                        if usrMsg is None:
                            usrMsg = UserSystemMessageModel()
                            usrMsg.user_id = user.id
                            usrMsg.sys_message_id = sysMsg.id
                            usrMsg.status = UserSystemMessageModel.UNREAD
                            usrMsg.show_date = None

                            usrMsg.id = self.daoRegistry.userMessageDao.createUserMessage(usrMsg)
                        elif usrMsg.status >= UserSystemMessageModel.READ:
                            usrMsg = None

                        if usrMsg:
                            data = sysMsg.toDict()
                            data.update(usrMsg.toDict())
                            msg = UserSystemMessage.fromSocrativeDict(data)

                except Exception as e:
                    logger.debug(exceptionStack(e))

                # remove last used status
                if room.status & RoomModel.LAST_USED != RoomModel.LAST_USED:
                    old_room_name = self.daoRegistry.roomDao.getLastUsedRoom(user.id, soc_constants.LAST_STATUS_OFF)
                    if old_room_name:
                        self.cacheRegistry.roomModelCache.invalidateData(old_room_name)
                self.daoRegistry.roomDao.updateLastUsed(RoomModel.LAST_USED, room.id)
                self.cacheRegistry.roomModelCache.invalidateData(room.name_lower)

                if room.status & RoomModel.ROSTERED == RoomModel.ROSTERED:
                    studentCountDict = self.daoRegistry.rosterDao.getStudentCount([room.id])
                    loggedInStudentCountDict = self.daoRegistry.studentDao.getLoggedInStudentCount([room.id])
                    responseData["student_count"] = studentCountDict.get(room.id, 0)
                    responseData["students_logged_in"] = loggedInStudentCountDict.get(room.id, 0)

                responseData['complete_user'] = self.check_user(user)
                responseData['message'] = msg
                cookie = (projSettings.USER_AUTH_TOKEN, Cryptography.encryptUserAuth(dto.auth_token))
                responseData['mastery_connect'] = self.daoRegistry.partnerDao.checkPartner(user.id,
                                                                                           PartnerModel.MASTERY_CONNECT)
                responseData['google_user'] = self.daoRegistry.partnerDao.checkPartner(user.id, PartnerModel.GOOGLE)
                responseData["premium"] = self.serviceRegistry.userService.is_user_premium(user)
                responseData['first_name'] = user.first_name
                lang = user.language or request.COOKIES.get(projSettings.LANGUAGE_COOKIE_NAME) or "en"
                responseData["lang"] = lang

                responseData["notifications"] = self.serviceRegistry.lecturerService.getNotifications(user)
            else:
                responseData["lang"] = lang
                # check if the student cookie exists already , if not generate one
                uuid = None
                try:
                    val = request.COOKIES.get(projSettings.STUDENT_AUTH_TOKEN)
                    uuid = Cryptography.decrypt(val) if val else None
                except Exception:
                    pass

                if uuid is None:
                    uuid = "anon" + Cryptography.generate_auth_token()[:16]
                    cookie = (projSettings.STUDENT_AUTH_TOKEN, Cryptography.encryptStudentAuth(uuid))
                else:
                    cookie = None
                responseData["user_uuid"] = uuid

            responseData["rostered"] = room.status & RoomModel.ROSTERED == RoomModel.ROSTERED
            responseData["handraise"] = room.status & RoomModel.HANDRAISE == RoomModel.HANDRAISE

            return responseData, successStatus, lang, cookie

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR, None, None

    def clearRoom(self, request, dataDict):
        """
        service for clear room api
        :param request:
        :param dataDict
        """

        user_uuid = dataDict.get("auth_token")
        room_name = dataDict.get("room_name")

        if not isStringNotEmpty(user_uuid):
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

        if not isStringNotEmpty(room_name):
            return BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST

        try:
            self.daoRegistry.roomDao.checkRoomPermission(user_uuid, room_name.lower())

            room = self.serviceRegistry.roomService.validateRoom(room_name.lower())
            if room.status < RoomModel.ARCHIVED and room.status & RoomModel.ROSTERED == RoomModel.ROSTERED:
                self.daoRegistry.studentDao.logoutStudents(room.id)

            publish.send_model(room_name.lower() + '-student', key="student", data="room_cleared")
            if room.status & RoomModel.DEFAULT == RoomModel.DEFAULT:
                self.roomFullLimiter.invalidateData(room.name_lower)

            return {}, status.HTTP_200_OK

        except BaseDao.NotFound:
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def getCurrentActivity(self, request, dto):
        """
        get the current activity data
        :param request:
        :param dto
        """

        # Find room
        try:

            room = self.validateRoom(dto.room_name)

            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            activityIds = self.daoRegistry.activityInstanceDao.getActivityIdsByRoomName(room.created_by_id,
                                                                                        room.name_lower)
            self.daoRegistry.activityInstanceDao.closeActivities(activityIds[1:])

            activityId = activityIds[0] if activityIds else None

            if activityId is not None:

                user = None
                if dto.auth_token:
                    user = self.serviceRegistry.userService.validateUser(dto.auth_token)
                    if user is None and dto.user_uuid is None:
                        return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

                if user is None:
                    activity = self.daoRegistry.activityInstanceDao.loadActivityForStudent(activityId)
                    resultData = StudentActivityInstance.fromSocrativeDict(activity)
                else:

                    if user.id != room.created_by_id:
                        return BaseError.PERMISION_DENIED, status.HTTP_400_BAD_REQUEST

                    activity = self.daoRegistry.activityInstanceDao.loadDeepActivity(dto.auth_token, activityId)
                    resultData = FullActivityReportDTO.fromSocrativeDict(activity)

                if "students_finished" in resultData:
                    if not isStringNotEmpty(resultData.get("students_finished")):
                        resultData['students_finished'] = []
                    else:
                        resultData['students_finished'] = ujson.loads(resultData['students_finished'])
                else:
                    resultData['students_finished'] = []

                if user is None:
                    resultData['finished'] = dto.user_uuid in ujson.loads(activity["students_finished"])

                if "settings" in resultData:
                    # check if there is a seconds key
                    found = False
                    for dictObj in resultData.get("settings", []):
                        if dictObj.get("key") == "seconds":
                            found = True
                            break

                    if found is False:
                        resultData["settings"].append({"key": "seconds", "value": "-1"})

                return resultData, status.HTTP_200_OK
            else:
                return {}, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def endCurrentActivity(self, request, dto):
        """
        ends the current activity
        :param request:
        :param dto:
        """

        try:
            activity = self.daoRegistry.activityInstanceDao.getActivityByRoomName(dto.auth_token, dto.room_name)

            if activity is None:
                return BaseError.ACTIVITY_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            self.daoRegistry.activityInstanceDao.closeActivitiesByRoom(activity.started_by_id, activity.room_name)

            rValues = [dict(id=media.id,
                            url=media.url,
                            formats="*") for media in self.daoRegistry.mediaResourceDao.loadMediaForQuiz(activity.activity_id)]

            request.tasksClient.add_tasks(TaskType.IMAGE_RESIZE, rValues)

            # this is not a priority , because the user didn't asked the report yet
            request.tasksClient.add_tasks_to_set(TaskType.GENERATE_REPORTS, [{"id": activity.id,
                                                                      "user_id": activity.started_by_id,
                                                                      "formats": "*"}]
            )

            publish.send_model(dto.room_name + '-student', key="localActivity", data="activity_ended")
            publish.send_model(dto.room_name + '-teacher', key="localActivity", data="activity_ended")

            self.roomFullLimiter.invalidateData(dto.room_name)

            return {}, status.HTTP_200_OK
        except BaseDao.NotFound:
            return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def updateActivityState(self, request, dto):
        """
        handler
        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            room = self.validateRoom(dto.room_name)
            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.created_by_id != user.id:
                return BaseError.PERMISION_DENIED, status.HTTP_400_BAD_REQUEST

            self.daoRegistry.activityInstanceDao.updateActivityState(dto.room_name, dto.auth_token, dto.state_data)

            publish.send_model(dto.room_name + '-student', key="localActivity", data={u"activity_state": dto.state_data})
            publish.send_model(dto.room_name + '-teacher', key="localActivity", data={u"activity_state": dto.state_data})

            return {}, status.HTTP_200_OK
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def checkRoomName(self, request, dataDict):
        """
        check room name api
        """

        new_room_name = dataDict.get("new_room_name")

        if new_room_name is None or len(new_room_name) > base_limits.ROOM_NAME_LIMIT:
            return BaseError.INVALID_ROOM_NAME, status.HTTP_400_BAD_REQUEST
        
        # try an get the user
        try:

            if self.roomRegex.match(new_room_name.strip()) is None:
                return BaseError.INVALID_ROOM_NAME, status.HTTP_400_BAD_REQUEST

            found = self.daoRegistry.roomDao.checkRoomName(new_room_name.strip().lower())
            if found is True:
                return BaseError.ROOM_ALREADY_EXISTS, status.HTTP_400_BAD_REQUEST

            return {},status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def createRoom(self, request, dto):
        """
        service to handle the join room api
        :param request:
        :param dto:
        """

        try:

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            is_user_pro = self.serviceRegistry.userService.is_user_premium(user)
            if not is_user_pro:
                rooms = self.daoRegistry.roomDao.getRoomsStatus(user.id, default=True)
                if len(rooms) >= self.MAX_FREE_ROOMS:
                    return BaseError.MAX_ROOM_LIMIT_ERROR, status.HTTP_400_BAD_REQUEST
            if is_user_pro:
                rooms = self.daoRegistry.roomDao.getRoomsStatus(user.id, default=True, inmenu=True, rostered=True)
                if len(rooms) >= self.MAX_PREMIUM_ROOMS:
                    return BaseError.MAX_ROOM_LIMIT_ERROR, status.HTTP_400_BAD_REQUEST

            model = None
            if dto.room_name is None:

                #  attempt to create friendly names
                model = self.__createNewRoom(user.id, user.last_name, user.first_name, RoomModel.INMENU)

                if model is None:

                    retries = 0
                    max_retries = 100
                    dto.room_name = ''.join([self.ALPHABET[random.randint(0, len(self.ALPHABET) - 1)] for _ in range(7)])
                    name_lower = dto.room_name.lower()

                    while retries < max_retries:
                        if self.daoRegistry.roomDao.checkRoomName(name_lower) is False:
                            break

                        dto.room_name = ''.join([self.ALPHABET[random.randint(0, len(self.ALPHABET) - 1)] for _ in range(7)])
                        name_lower = dto.room_name.lower()
                        retries += 1
                    if retries >= max_retries:
                        return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if model is None:
                model = RoomModel()
                model.id = None
                model.name = dto.room_name
                model.name_lower = dto.room_name.lower()
                model.status = RoomModel.INMENU
                model.legacy_id = -1
                model.created_by_id = user.id

                model.id = self.daoRegistry.roomDao.createRoom(model.toDict())

            self.cacheRegistry.roomListCache.invalidateData(user.id)

            resp_data = Room.fromSocrativeModel(model)
            resp_data["in_menu"] = True
            resp_data["default"] = False
            resp_data["active"] = False
            resp_data["rostered"] = False
            resp_data["handraise"] = False

            return resp_data, status.HTTP_201_CREATED
        except IntegrityError as e:
            logger.info(exceptionStack(e))
            return BaseError.ROOM_ALREADY_EXISTS, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR, None

    def deleteRoom(self, request, dto):
        """
        service to handle the join room api
        :param request:
        :param dto:
        """

        try:

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            room = self.validateRoom(dto.room_name)

            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.created_by_id != user.id:
                return BaseError.PERMISION_DENIED, status.HTTP_400_BAD_REQUEST

            if room.status & RoomModel.DEFAULT == RoomModel.DEFAULT:
                return BaseError.PERMISION_DENIED, status.HTTP_400_BAD_REQUEST

            is_pro_user = self.serviceRegistry.userService.is_user_premium(user)
            if not is_pro_user:
                return BaseError.PERMISION_DENIED, status.HTTP_400_BAD_REQUEST

            elif is_pro_user:
                #  rooms that don't have rosters and are not in menu have status 0, make sure we can delete them too
                if room.status & soc_constants.DELETABLE_ROOM != 0 or room.status == 0:
                    rosterIds = self.daoRegistry.rosterDao.getRosterIds(room.id)
                    if rosterIds:
                        self.daoRegistry.studentDao.deleteStudents(rosterIds)
                        self.daoRegistry.rosterDao.deleteRosters(rosterIds)
                    self.daoRegistry.roomDao.deleteRoom(room.id)
                    self.daoRegistry.roomHistoryDao.insert(user.id, room.name_lower, room.id, room.status)
                    self.cacheRegistry.roomModelCache.invalidateData(room.name_lower)
                    publish.send_model(room.name_lower + '-student', key="student", data="room_cleared")

                    self.cacheRegistry.roomListCache.invalidateData(user.id)

                    return {}, status.HTTP_200_OK
                else:
                    return BaseError.PERMISION_DENIED, status.HTTP_400_BAD_REQUEST

            else:
                return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

        except BaseDao.NotUpdated as e:
            logger.info(exceptionStack(e))
            return BaseError.ROOM_NOT_DELETED, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR, None

    def updateRoom(self, request, dto):
        """
        service to handle the join room api
        :param request:
        :param dto:
        """

        try:

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            room = self.validateRoom(dto.room_name)

            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            new_room = self.validateRoom(dto.new_room_name)
            if new_room is not None:
                return BaseError.ROOM_NAME_ALREADY_TAKEN, status.HTTP_400_BAD_REQUEST

            if room.created_by_id != user.id:
                return BaseError.PERMISION_DENIED, status.HTTP_400_BAD_REQUEST

            self.daoRegistry.roomDao.updateRoomName(dto.room_name, dto.new_room_name)
            self.daoRegistry.roomHistoryDao.insert(user.id, dto.room_name, room.id, room.status)

            publish.send_model(room.name_lower + '-student', key="student", data="room_cleared")

            if room.status < RoomModel.ARCHIVED and room.status & RoomModel.ROSTERED == RoomModel.ROSTERED:
                self.daoRegistry.studentDao.logoutStudents(room.id)

            if room.status & RoomModel.DEFAULT == RoomModel.DEFAULT:
                self.roomFullLimiter.invalidateData(room.name_lower)

            # remove the old room name from cache
            self.cacheRegistry.roomModelCache.invalidateData(dto.room_name.lower())
            self.cacheRegistry.roomListCache.invalidateData(user.id)

            return {}, status.HTTP_200_OK
        except BaseDao.NotUpdated:
            logger.warn("Room name %s wasn't found" % dto.room_name)
            return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except IntegrityError as e:
            logger.info(exceptionStack(e))
            return BaseError.ROOM_ALREADY_EXISTS, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR, None

    def __getAllRooms(self, user_id):
        """

        :param user_id:
        :return:
        """

        try:
            room_list = self.cacheRegistry.roomListCache.get(user_id)

            if room_list is None:
                room_list = self.daoRegistry.roomDao.loadRooms(user_id)
                self.cacheRegistry.roomListCache.cacheData(user_id, room_list)

            return room_list
        except Exception as e:
            logger.error(exceptionStack(e))
            raise

    def getRoomList(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            room_list = self.__getAllRooms(user.id)
            room_names = [r["name_lower"] for r in room_list]

            activityDict = self.daoRegistry.activityInstanceDao.getActivityIdsByRoomNames(user.id, room_names)
            for r in room_list:
                r["active"] = activityDict.get(r["name_lower"]) is not None
                r["default"] = r["status"] & RoomModel.DEFAULT == RoomModel.DEFAULT
                r["rostered"] = r["status"] & RoomModel.ROSTERED == RoomModel.ROSTERED
                r["default"] = r["status"] & RoomModel.DEFAULT == RoomModel.DEFAULT
                r["in_menu"] = r["status"] & RoomModel.INMENU == RoomModel.INMENU or r["default"] is True
                r["handraise"] = r["status"] & RoomModel.HANDRAISE == RoomModel.HANDRAISE
                if r["status"] == 0 or r["status"] == RoomModel.LAST_USED:
                    r["default"] = False
                    r["rostered"] = False
                    r["in_menu"] = False

            if self.serviceRegistry.userService.is_user_premium(user):
                roomIds = [r['id'] for r in room_list]
                if dto.in_menu is False:
                    studentCountDict = self.daoRegistry.rosterDao.getStudentCount(roomIds)
                    loggedInStudentCountDict = self.daoRegistry.studentDao.getLoggedInStudentCount(roomIds)
                else:
                    room_list = [r for r in room_list if r["status"] != 0 and r["status"] != RoomModel.LAST_USED]
                    studentCountDict = dict()
                    loggedInStudentCountDict = dict()

                shareUrlDict = self.daoRegistry.roomCodeDao.getCodes(roomIds)

            else:
                room_list = [r for r in room_list if r["default"] is True]
                studentCountDict = dict()
                loggedInStudentCountDict = dict()
                shareUrlDict = dict()

            url = ("https://" if request.is_secure() else "http://") + request.get_host() + "/rc/"
            for room in room_list:
                if room["id"] in studentCountDict and room["rostered"] is True:
                    room["student_count"] = studentCountDict.get(room["id"], 0)

                # not accurate to get this value, we will use pubnub for this
                if room["rostered"] is True:
                    room["students_logged_in"] = loggedInStudentCountDict.get(room["id"], 0)

                if room["id"] in shareUrlDict:
                    room["share_url"] = url + shareUrlDict[room["id"]]
                else:
                    room["share_url"] = None
            resp = RoomListDTO.fromSocrativeDict({"rooms": room_list})

            return resp, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def validateRoom(self, room_name):
        """

        :param room_name:
        :return:
        """

        try:

            if not isStringNotEmpty(room_name):
                return None

            roomModel = self.cacheRegistry.roomModelCache.get(room_name)
            if roomModel:
                return roomModel

            model = self.daoRegistry.roomDao.getRoomByName(room_name)

            if not model:
                return None

            self.cacheRegistry.roomModelCache.cacheData(room_name.lower(), model)

            return model
        except Exception as e:
            logger.error(exceptionStack(e))
            raise

    def updateRoomStatus(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            isPremium = self.serviceRegistry.userService.is_user_premium(user)

            inmenu_on = []
            inmenu_off = []
            for dto in dto.statuses:
                if dto.in_menu is False:
                    inmenu_off.append((dto.room_id, soc_constants.INMENU_OFF))
                else:
                    inmenu_on.append((dto.room_id, RoomModel.INMENU))
            # TODO move the transaction block outside of DAO - > will be done when using the gevent branch
            roomIds = self.daoRegistry.roomDao.updateStatusList(inmenu_on, inmenu_off, isPremium, user.id)

            # TODO invalidate the updated rooms
            ids = set([r[0] for r in inmenu_off] + [r[0] for r in inmenu_on])

            room_names = self.daoRegistry.roomDao.getRoomNames(ids)

            self.cacheRegistry.roomModelCache.invalidateMulti(room_names)
            self.cacheRegistry.roomListCache.invalidateData(user.id)

            return {"room_ids": roomIds}, status.HTTP_200_OK
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def addStudents(self, request, dto):
        """
        adds a student into a rostered room, or transforms a room into a rostered room if it's not the default one
        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if not user:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            room = self.validateRoom(dto.room_name)
            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.created_by_id != user.id:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.status & RoomModel.DEFAULT == RoomModel.DEFAULT:
                return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

            if room.status >= RoomModel.ARCHIVED:
                return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

            activity = self.daoRegistry.activityInstanceDao.getActivityByRoomName(dto.auth_token, room.name_lower)
            if activity:
                return BaseError.ACTIVITY_NOT_ENDED, status.HTTP_400_BAD_REQUEST

            roster = self.daoRegistry.rosterDao.getOrInsertRoster(room.id, sync_type=StudentModel.MANUAL)

            student_ids = [student.student_id for student in dto.students]

            if len(set(student_ids)) != len(student_ids):
                return BaseError.DUPLICATED_STUDENT_ID, status.HTTP_400_BAD_REQUEST

            existing_student_ids = self.daoRegistry.studentDao.verifyStudentIds(student_ids, roster.id)

            if existing_student_ids:
                return BaseError.STUDENT_ID_ALREADY_EXISTS, status.HTTP_400_BAD_REQUEST

            if roster.student_count + len(student_ids) > base_limits.MAX_STUDENTS_IN_ROSTER:
                return BaseError.TOO_MANY_STUDENTS, status.HTTP_400_BAD_REQUEST

            studentDict = self.daoRegistry.studentDao.insertStudents(dto.students, roster.id)

            self.daoRegistry.rosterDao.incrementStudentCount(roster.id, len(studentDict))

            clear_room = room.status & RoomModel.ROSTERED == 0

            newStatus = room.status | RoomModel.ROSTERED

            self.daoRegistry.roomDao.updateStatus(newStatus, room.name_lower, room.created_by_id)
            self.cacheRegistry.roomModelCache.invalidateData(room.name_lower)
            self.cacheRegistry.roomListCache.invalidateData(user.id)

            resp = {"students": [{"student_id": item[1][0],
                                  "id": item[0],
                                  "last_name": item[1][1],
                                  "first_name": item[1][2]} for item in studentDict.items()]}

            if clear_room:
                publish.send_model(room.name_lower + '-student', key="student", data="room_cleared")

            return resp, status.HTTP_201_CREATED

        except BaseDao.NotInserted:
            return BaseError.STUDENTS_NOT_ADDED, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def deleteStudent(self, request, dto):
        """
        adds a student into a rostered room, or transforms a room into a rostered room if it's not the default one
        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if not user:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            room = self.validateRoom(dto.room_name)

            if room.created_by_id != user.id:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.status & RoomModel.ROSTERED == 0:
                return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

            if room.status >= RoomModel.ARCHIVED:
                return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

            activity = self.daoRegistry.activityInstanceDao.getActivityByRoomName(dto.auth_token, room.name_lower)
            if activity:
                return BaseError.ACTIVITY_NOT_ENDED, status.HTTP_400_BAD_REQUEST

            roster = self.daoRegistry.rosterDao.getRoster(room.id)
            if not roster:
                return BaseError.ROOM_DOESNT_HAVE_ROSTER, status.HTTP_400_BAD_REQUEST

            student_uuid = self.daoRegistry.studentDao.deleteStudent(dto.id)

            # decrement the student count
            self.daoRegistry.rosterDao.incrementStudentCount(roster.id, -1)

            if roster.student_count - 1 == 0:
                newStatus = room.status & soc_constants.ROSTERED_OFF
                self.daoRegistry.roomDao.updateStatus(newStatus, room.name_lower, room.created_by_id)
                self.daoRegistry.rosterDao.updateStatus(RosterModel.ARCHIVED, room.id)
                self.cacheRegistry.roomModelCache.invalidateData(room.name_lower)
                self.cacheRegistry.roomListCache.invalidateData(user.id)

            if student_uuid:
                publish.send_model(room.name_lower + '-student', key="student", data="{\"kick\":\"%s\"}" % student_uuid)

            return {}, status.HTTP_200_OK
        except BaseDao.NotDeleted:
            logger.debug("student with id %s wasn't found" % dto.id)
            return BaseError.STUDENT_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def updateStudent(self, request, dto):
        """
        adds a student into a rostered room, or transforms a room into a rostered room if it's not the default one
        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if not user:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            room = self.validateRoom(dto.room_name)

            if room.created_by_id != user.id:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.status & RoomModel.DEFAULT == RoomModel.DEFAULT:
                return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

            if room.status >= RoomModel.ARCHIVED:
                return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST

            activity = self.daoRegistry.activityInstanceDao.getActivityByRoomName(dto.auth_token, room.name_lower)
            if activity:
                return BaseError.ACTIVITY_NOT_ENDED, status.HTTP_400_BAD_REQUEST

            roster = self.daoRegistry.rosterDao.getRoster(room.id)
            if not roster:
                return BaseError.ROOM_DOESNT_HAVE_ROSTER, status.HTTP_400_BAD_REQUEST

            studentExists = self.daoRegistry.studentDao.verifyStudentId(dto.student_id, dto.id, roster.id)
            if studentExists:
                return BaseError.STUDENT_ID_ALREADY_EXISTS, status.HTTP_400_BAD_REQUEST

            studentModel = self.daoRegistry.studentDao.updateStudent(dto.first_name, dto.last_name, dto.student_id,
                                                                     dto.id)

            if studentModel.user_uuid:
                publish.send_model(room.name_lower + '-student',
                                   key="student",
                                   data="{\"kick\":\"%s\"}" % studentModel.user_uuid)

            return StudentDTO.fromSocrativeModel(studentModel), status.HTTP_200_OK

        except BaseDao.NotUpdated:
            return BaseError.STUDENT_NOT_FOUND, status.HTTP_400_BAD_REQUEST

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def getRosteredStudents(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            now = pytz.utc.localize(datetime.datetime.utcnow())
            if not self.serviceRegistry.userService.is_user_premium(user):
                return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

            room = self.validateRoom(dto.room_name)

            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.status & RoomModel.ROSTERED != RoomModel.ROSTERED or room.status >= RoomModel.ARCHIVED:
                return BaseError.ROOM_DOESNT_HAVE_ROSTER, status.HTTP_400_BAD_REQUEST

            studentList = self.daoRegistry.studentDao.getStudents(room.id)

            for studentDict in studentList:
                studentDict["logged_in"] = studentDict.get(StudentModel.STATUS) & StudentModel.LOGGED_IN == StudentModel.LOGGED_IN
                studentDict["handraise"] = studentDict.get(StudentModel.STATUS) & StudentModel.HANDRAISE == StudentModel.HANDRAISE
                if StudentModel.STATUS in studentDict:
                    studentDict.pop(StudentModel.STATUS)

            return studentList, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def __readRosterFromCSV(self, ioBuffer, importSettings):
        """

        :param ioBuffer:
        :param importSettings:
        :return:
        """
        try:

            student_list = []

            buf_lines = ioBuffer.read().replace('\x0d\x0a', '\x0a').replace('\x0d','\x0a')
            if buf_lines[-1] == '\x0a':
                buf_lines = buf_lines.split('\x0a')[:-1]
            else:
                buf_lines = buf_lines.split('\x0a')

            lines = csv.reader(buf_lines)

            errorDict = {
                "last_name": [],
                "first_name": [],
                "student_id": [],
                "duplicate_student_id": []
            }

            errors = list()

            student_ids = dict()

            i = 0
            importHeader = importSettings.get(RosterFileSettingModel.IMPORT_HEADER, True)
            importOrder = importSettings.get(RosterFileSettingModel.IMPORT_ORDER, soc_constants.ROSTER_LAST_FIRST_ID)

            for l in lines:
                if importHeader and i == 0:
                    i += 1
                    continue

                i += 1
                if i > 151:
                    errors = [BaseError.TOO_MANY_STUDENTS_TO_BE_IMPORTED]
                    return [], errors

                if importOrder == soc_constants.ROSTER_LAST_FIRST_ID:
                    last_name = l[0] if len(l) >= 1 else None
                    first_name = l[1] if len(l) >= 2 and l[1] else None
                elif importOrder == soc_constants.ROSTER_FIRST_LAST_ID:
                    first_name = l[0] if len(l) >= 1 else None
                    last_name = l[1] if len(l) >= 2 and l[1] else None
                else:
                    return [], BaseError.ROSTER_IMPORT_ORDER_INVALID

                student_id = l[2] if len(l) >= 3 and l[2] else None

                if type(last_name) in (str, unicode):
                    last_name = last_name.strip()

                if isStringNotEmpty(last_name) is False or len(last_name) > base_limits.STUDENT_NAME_LIMIT:
                    errorDict["last_name"].append(i)

                if type(first_name) in (str, unicode):
                    first_name = first_name.strip()

                if isStringNotEmpty(first_name) is False or len(first_name) > base_limits.STUDENT_NAME_LIMIT:
                    errorDict["first_name"].append(i)

                if type(student_id) in (str, unicode):
                    student_id = student_id.strip().upper()
                    if student_id.startswith("'") and student_id.endswith("'") and safeToInt(student_id[1:-1]) is not None:
                        student_id = student_id[1:-1].upper()

                if isStringNotEmpty(student_id) is False or len(student_id) > base_limits.STUDENT_ROSTER_ID_LIMIT:
                    errorDict["student_id"].append(i)
                    continue

                if student_id in student_ids:
                    errorDict["duplicate_student_id"].append(i)
                    continue
                else:
                    student_ids[student_id] = None

                student_list.append({"last_name": last_name, "first_name": first_name, "student_id": student_id})

            # format the error list in a consistent way => {"error": [{"key":"value"},{"key":"value"}...]}
            for k in errorDict:
                if len(errorDict[k]) > 0:
                    errors.append({k: errorDict[k]})

            return student_list, errors
        except Exception as e:
            logger.error(exceptionStack(e))
            return [], [BaseError.CANT_READ_CSV_ROSTER]

    def __readRosterFromExcel(self, ioBuffer, importSettings):
        """

        :param ioBuffer:
        :param importSettings:
        :return:
        """
        try:

            student_list = []
            w = xlrd.open_workbook(file_contents=ioBuffer.read())

            errorDict = {
                "last_name": [],
                "first_name": [],
                "student_id": [],
                "duplicate_student_id": []
            }

            student_ids = {}
            hasHeader = importSettings.get(RosterFileSettingModel.IMPORT_HEADER, True)

            errors = list()
            for sheet in w.sheets():
                if sheet.nrows < 1:
                    errors.append(BaseError.NOT_ENOUGH_ROWS_IN_FILE)
                    return [], errors

                if (hasHeader and sheet.nrows > soc_constants.MAX_ROSTERED_STUDENTS + 1) or\
                        (not hasHeader and sheet.nrows > soc_constants.MAX_ROSTERED_STUDENTS):
                    errors.append(BaseError.TOO_MANY_STUDENTS_TO_BE_IMPORTED)
                    return [], errors

                start = 1 if hasHeader else 0
                importOrder = importSettings.get(RosterFileSettingModel.IMPORT_ORDER, soc_constants.ROSTER_LAST_FIRST_ID)
                for rowx in range(start, sheet.nrows):

                    cells = sheet.row(rowx)
                    if importOrder == soc_constants.ROSTER_LAST_FIRST_ID:
                        last_name = cells[0].value if len(cells) >= 1 else None
                        first_name = cells[1].value if len(cells) >= 2 else None
                    elif importOrder == soc_constants.ROSTER_FIRST_LAST_ID:
                        first_name = cells[0].value if len(cells) >= 1 else None
                        last_name = cells[1].value if len(cells) >= 2 else None
                    else:
                        logger.info("Invalid roster order. This shouldn't happen.")

                    if len(cells) >= 3:
                        val = safeToInt(cells[2].value) or safeToString(cells[2].value)
                    else:
                        val = None
                    student_id = safeToString(val) if len(cells) >= 3 else None

                    if type(last_name) in (str, unicode):
                        last_name = last_name.strip()
                    elif type(last_name) in (long, int, float):
                        last_name = str(safeToInt(last_name))

                    if isStringNotEmpty(last_name) is False or len(last_name) > base_limits.STUDENT_NAME_LIMIT:
                        errorDict["last_name"].append(rowx)

                    if type(first_name) in (str, unicode):
                        first_name = first_name.strip()
                    elif type(first_name) in (int, long, float):
                        first_name = str(safeToInt(first_name))

                    if isStringNotEmpty(first_name) is False or len(first_name) > base_limits.STUDENT_NAME_LIMIT:
                        errorDict["first_name"].append(rowx)

                    if type(student_id) in (str, unicode):
                        student_id = student_id.strip()
                        if student_id.startswith("'") and student_id.endswith("'") and safeToInt(
                                student_id[1:-1]) is not None:
                            student_id = student_id[1:-1]
                    elif type(student_id) in (int, long, float):
                        student_id = str(safeToInt(student_id))

                    if isStringNotEmpty(student_id) is False or len(student_id) > base_limits.STUDENT_ROSTER_ID_LIMIT:
                        errorDict["student_id"].append(rowx)
                        continue

                    student_id = student_id.upper()

                    if student_id in student_ids:
                        errorDict["duplicate_student_id"].append(rowx)
                        continue
                    else:
                        student_ids[student_id] = None
                    student_list.append({"last_name": last_name, "first_name": first_name, "student_id": student_id})

                break

            # format the error list in a consistent way => {"error": [{"key":"value"},{"key":"value"}...]}
            for k in errorDict:
                if len(errorDict[k]) > 0:
                    errors.append({k: errorDict[k]})

            return student_list, errors
        except Exception as e:
            logger.error(exceptionStack(e))
            return [], [BaseError.CANT_READ_EXCEL_ROSTER]

    def importRoster(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            # authenticate the user, check for premium status
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if not self.serviceRegistry.userService.is_user_premium(user):
                return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

            # get the room, check that it's not rostered
            room = self.validateRoom(dto.room_name)
            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.status >= RoomModel.ARCHIVED:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.status & RoomModel.ROSTERED == RoomModel.ROSTERED:
                return BaseError.ROOM_IS_ROSTERED, status.HTTP_400_BAD_REQUEST

            activity = self.daoRegistry.activityInstanceDao.getActivityByRoomName(dto.auth_token, room.name_lower)
            if activity:
                return BaseError.ACTIVITY_NOT_ENDED, status.HTTP_400_BAD_REQUEST

            # all good until now, try to get the csv/xls/xlsx file
            try:
                f = request.FILES['0']

                if safeToInt(request.META.get("CONTENT_LENGTH", 0)) > self.MAX_ROSTER_FILE_SIZE:
                    return BaseError.FILE_TOO_BIG, status.HTTP_400_BAD_REQUEST

                memoryFile = cStringIO.StringIO(f.read())

                # check the mimetype
                mime = magic.from_buffer(memoryFile.read(), mime=True)

                # go at the beginning of the file
                memoryFile.seek(0, 0)

                if mime not in self.ALLOWED_ROSTER_MIMETYPES:
                    logger.error(mime)
                    return BaseError.ROSTER_FILE_NOT_IMPORTABLE, status.HTTP_400_BAD_REQUEST

            except Exception as e:
                logger.error(exceptionStack(e))
                return BaseError.ROSTER_CONTENT_NOT_AVAILABLE, status.HTTP_400_BAD_REQUEST

            importSettings = self.daoRegistry.rosterFileSettingsDao.getImportSettings(user.id)

            csv = False
            if mime.startswith("text"):
                studentList, errorList = self.__readRosterFromCSV(memoryFile, importSettings)
                csv = True
            else:
                studentList, errorList = self.__readRosterFromExcel(memoryFile, importSettings)

            if errorList:
                if "error" in errorList[0]:
                    return errorList[0], status.HTTP_400_BAD_REQUEST

                resp = {"error": {}}
                for error in errorList:
                    resp["error"].update(error)
                return resp, status.HTTP_400_BAD_REQUEST

            if not studentList:
                return BaseError.EMPTY_ROSTER_FILE, status.HTTP_400_BAD_REQUEST

            resp_dto = AddRosteredStudentRequestDTO.fromDict({"students": studentList,
                                                              "auth_token": dto.auth_token,
                                                              "room_name": dto.room_name})

            if type(resp_dto) is tuple:
                return resp_dto

            roster = self.daoRegistry.rosterDao.getOrInsertRoster(room.id,
                                                                  sync_type=(StudentModel.CSV if csv else StudentModel.XLS),
                                                                  sync_id=None,
                                                                  data='')

            studentDict = self.daoRegistry.studentDao.insertStudents(resp_dto.students, roster.id)

            self.daoRegistry.rosterDao.incrementStudentCount(roster.id, len(studentDict))

            newStatus = room.status | RoomModel.ROSTERED

            self.daoRegistry.roomDao.updateStatus(newStatus, room.name_lower, room.created_by_id)
            self.cacheRegistry.roomModelCache.invalidateData(room.name_lower)
            self.cacheRegistry.roomListCache.invalidateData(user.id)

            resp = {"students": [{"student_id": item[1][0],
                                  "id": item[0],
                                  "last_name": item[1][1],
                                  "first_name": item[1][2]} for item in studentDict.items()]}

            publish.send_model(room.name_lower + '-student', key="student", data="room_cleared")

            return resp, status.HTTP_201_CREATED

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def deleteRoster(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            now = pytz.utc.localize(datetime.datetime.utcnow())

            if not self.serviceRegistry.userService.is_user_premium(user):
                return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

            room = self.validateRoom(dto.room_name)

            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.status & RoomModel.ROSTERED != RoomModel.ROSTERED:
                return BaseError.ROOM_DOESNT_HAVE_ROSTER, status.HTTP_400_BAD_REQUEST

            activity = self.daoRegistry.activityInstanceDao.getActivityByRoomName(dto.auth_token, room.name_lower)
            if activity:
                return BaseError.ACTIVITY_NOT_ENDED, status.HTTP_400_BAD_REQUEST

            roster = self.daoRegistry.rosterDao.getRoster(room.id)
            if roster:
                self.daoRegistry.roomDao.updateStatus(soc_constants.ROSTERED_OFF & room.status, room.name_lower, user.id)
                self.daoRegistry.studentDao.deleteStudents([roster.id])
                self.daoRegistry.rosterDao.deleteRosters([roster.id])
                self.cacheRegistry.roomModelCache.invalidateData(room.name_lower)
                self.cacheRegistry.roomListCache.invalidateData(user.id)
                publish.send_model(room.name_lower + '-student', key="student", data="room_cleared")

            return {}, status.HTTP_200_OK

        except BaseDao.NotDeleted:
            return BaseError.ROSTER_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def __buildCSVBuffer(self, student_model_list, exportSettings):
        """
s
        :param student_model_list:
        :param exportSettings:
        :return:
        """

        buf = cStringIO.StringIO()
        rosterOrder = exportSettings.get(RosterFileSettingModel.EXPORT_ORDER, soc_constants.ROSTER_LAST_FIRST_ID)

        if exportSettings.get(RosterFileSettingModel.EXPORT_HEADER):
            if rosterOrder == soc_constants.ROSTER_LAST_FIRST_ID:
                buf.write("Last name, First name, Student ID\n")
            elif rosterOrder == soc_constants.ROSTER_FIRST_LAST_ID:
                buf.write("First name, Last name, Student ID\n")
            else:
                logger.info("Invalid roster order. This shouldn't happen")

        for studentModel in student_model_list:
            last_name = studentModel.get("last_name")
            first_name = studentModel.get("first_name")
            student_id = studentModel.get("student_id")

            if rosterOrder == soc_constants.ROSTER_LAST_FIRST_ID:
                if safeToInt(student_id) is not None:
                    buf.write('''%s,%s,'%s'\n''' % (last_name, first_name, student_id))
                else:
                    buf.write('''%s,%s,%s\n''' % (last_name, first_name, student_id))
            elif rosterOrder == soc_constants.ROSTER_FIRST_LAST_ID:
                if safeToInt(student_id) is not None:
                    buf.write('''%s,%s,'%s'\n''' % (first_name, last_name, student_id))
                else:
                    buf.write('''%s,%s,%s\n''' % (first_name, last_name, student_id))
            else:
                logger.info("Invalid roster order...this shouldn't happen")

        return buf

    def __buildXLSBuffer(self, student_model_list, exportSettings):
        """

        :param student_model_list:
        :param exportSettings
        :return:
        """

        buf = cStringIO.StringIO()
        workbook = xlsxwriter.Workbook(buf)
        worksheet = workbook.add_worksheet("Roster")
        rosterOrder = exportSettings.get(RosterFileSettingModel.EXPORT_ORDER)
        i = 0
        if exportSettings.get(RosterFileSettingModel.EXPORT_HEADER):
            if rosterOrder == soc_constants.ROSTER_LAST_FIRST_ID:
                worksheet.write(i, 0, "Last name")
                worksheet.write(i, 1, "First name")
            elif rosterOrder == soc_constants.ROSTER_FIRST_LAST_ID:
                worksheet.write(i, 0, "First name")
                worksheet.write(i, 1, "Last name")
            else:
                logger.info("Invalid roster order. This shouldn't happen")

            worksheet.write(i, 2, "Student ID")
            i += 1

        for studentModel in student_model_list:
            last_name = studentModel.get("last_name")
            first_name = studentModel.get("first_name")
            student_id = studentModel.get("student_id")
            if rosterOrder == soc_constants.ROSTER_LAST_FIRST_ID:
                worksheet.write(i, 0, last_name)
                worksheet.write(i, 1, first_name)
            elif rosterOrder == soc_constants.ROSTER_FIRST_LAST_ID:
                worksheet.write(i, 0, first_name)
                worksheet.write(i, 1, last_name)
            else:
                logger.info("Invalid roster order. This shouldn't happen.")

            if safeToInt(student_id) is not None:
                worksheet.write(i, 2, "'%s'" % student_id)
            else:
                worksheet.write(i, 2, student_id)
            i += 1

        workbook.close()

        return buf

    def exportRoster(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            # check for premium
            if not self.serviceRegistry.userService.is_user_premium(user):
                return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

            room = self.validateRoom(dto.room_name)

            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            # check for rostered
            if room.status & RoomModel.ROSTERED != RoomModel.ROSTERED:
                return BaseError.ROOM_DOESNT_HAVE_ROSTER, status.HTTP_400_BAD_REQUEST

            studentDictList = self.daoRegistry.studentDao.getStudents(room.id)

            exportSettings = self.daoRegistry.rosterFileSettingsDao.getExportSettings(user.id)

            if (dto.type and dto.type == soc_constants.ROSTER_FILE_CSV) or\
                    exportSettings.get(RosterFileSettingModel.EXPORT_TYPE, soc_constants.ROSTER_FILE_CSV) == soc_constants.ROSTER_FILE_CSV:

                buf = self.__buildCSVBuffer(studentDictList, exportSettings)

            elif (dto.type and dto.type == soc_constants.ROSTER_FILE_XLSX) or\
                    exportSettings.get(RosterFileSettingModel.EXPORT_TYPE, soc_constants.ROSTER_FILE_CSV) == soc_constants.ROSTER_FILE_XLSX:
                buf = self.__buildXLSBuffer(studentDictList, exportSettings)
            else:
                return BaseError.INVALID_EXPORT_TYPE, status.HTTP_400_BAD_REQUEST

            return buf, status.HTTP_200_OK, "%s_roster.%s" % (room.name_lower, dto.type)

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_400_BAD_REQUEST

    def shareUrl(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """
        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            room = self.validateRoom(dto.room_name)

            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if not self.serviceRegistry.userService.is_user_premium(user):
                return BaseError.FEATURE_NOT_AVAILABLE, status.HTTP_400_BAD_REQUEST

            url = "https://" if request.is_secure() else "http://"
            url += request.get_host() + "/rc/"

            code = Cryptography.generate_room_code(base_limits.MAX_ROOM_SHARE_CODE_LENGTH)
            retry_count = 1000
            model = RoomCodeModel()
            model.room_id = room.id
            model.valid = True
            model.code = code

            inserted = False
            while retry_count > 0:
                try:
                    self.daoRegistry.roomCodeDao.insert(model)
                    inserted = True
                    break
                except IntegrityError as e:
                    logger.debug(exceptionStack(e))
                    code = Cryptography.generate_room_code(base_limits.MAX_ROOM_SHARE_CODE_LENGTH)
                    model.code = code
                finally:
                    retry_count -= 1

            if not inserted:
                return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

            url += code

            return {"url": url}, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_400_BAD_REQUEST

    def updateRoomHandraiseStatus(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if self.serviceRegistry.userService.is_user_premium(user) is False:
                return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

            room = self.validateRoom(dto.room_name)

            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.status & RoomModel.ROSTERED != RoomModel.ROSTERED:
                return BaseError.ROOM_NOT_ROSTERED, status.HTTP_400_BAD_REQUEST

            if room.status & RoomModel.HANDRAISE == RoomModel.HANDRAISE:
                active = True
            else:
                active = False

            if dto.active is active:
                return {}, 200

            if dto.active is False:
                room_status = room.status & soc_constants.ROOM_HANDSRAISE_OFF
                self.daoRegistry.roomDao.updateStatus(room_status, room.name_lower, user.id,
                                                      no_if_activity_running=False)
                student_status = soc_constants.STUDENT_HANDRAISE_OFF
                self.daoRegistry.studentDao.updateHandraise(student_status, room.id, off=True)
                self.cacheRegistry.roomModelCache.invalidateData(dto.room_name)
                self.cacheRegistry.roomListCache.invalidateData(user.id)
                publish.send_model(dto.room_name + '-student', key="handraise", data=ujson.dumps({"state":"disabled"}))
            else:
                room_status = room.status | RoomModel.HANDRAISE
                self.daoRegistry.roomDao.updateStatus(room_status, room.name_lower, user.id,
                                                      no_if_activity_running=False)
                self.cacheRegistry.roomModelCache.invalidateData(dto.room_name)
                self.cacheRegistry.roomListCache.invalidateData(user.id)
                publish.send_model(dto.room_name + '-student', key="handraise", data=ujson.dumps({"state": "enabled"}))

            return {}, 200

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def getRosterFileSettings(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if not user:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if not self.serviceRegistry.userService.is_user_premium(user):
                return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

            roster_settings = self.daoRegistry.rosterFileSettingsDao.getSettings(user.id, _type=BaseDao.TO_DICT) or {}

            if RosterFileSettingModel.TEACHER_ID in roster_settings:
                roster_settings.pop(RosterFileSettingModel.TEACHER_ID)

            return roster_settings, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def createRosterFileSettings(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if not user:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if not self.serviceRegistry.userService.is_user_premium(user):
                return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

            # check to see if there are settings already

            exists = self.daoRegistry.rosterFileSettingsDao.checkSettings(user.id)

            if exists is not None:
                return BaseError.ROSTER_FILE_SETTINGS_ALREADY_EXIST, status.HTTP_400_BAD_REQUEST

            model = RosterFileSettingModel.fromRequestDto(dto)
            model.teacher_id = user.id
            model.id = self.daoRegistry.rosterFileSettingsDao.create(model.toDict())

            return {"id": model.id}, status.HTTP_201_CREATED
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def updateRosterSettings(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if not user:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if not self.serviceRegistry.userService.is_user_premium(user):
                return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

            # load the existing roster settings

            model = self.daoRegistry.rosterFileSettingsDao.load(dto.pk, user.id)

            if not model:
                return BaseError.ROSTER_FILE_SETTINGS_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            updateDict = dict()
            if hasattr(dto, "import_order") and model.import_order != dto.import_order:
                updateDict[RosterFileSettingModel.IMPORT_ORDER] = dto.import_order

            if hasattr(dto, "import_type") and model.import_type != dto.import_type:
                updateDict[RosterFileSettingModel.IMPORT_TYPE] = dto.import_type

            if hasattr(dto, "import_header") and model.import_header != dto.import_header:
                updateDict[RosterFileSettingModel.IMPORT_HEADER] = dto.import_header

            if hasattr(dto, "export_order") and model.export_order != dto.export_order:
                updateDict[RosterFileSettingModel.EXPORT_ORDER] = dto.export_order

            if hasattr(dto, "export_type") and model.export_type != dto.export_type:
                updateDict[RosterFileSettingModel.EXPORT_TYPE] = dto.export_type

            if hasattr(dto, "export_header") and model.export_header != dto.export_header:
                updateDict[RosterFileSettingModel.EXPORT_HEADER] = dto.export_header

            if updateDict:
                self.daoRegistry.rosterFileSettingsDao.updateSettings(updateDict, model.id)

            return {}, status.HTTP_200_OK
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR
