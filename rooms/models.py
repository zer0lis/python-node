# -*- coding: utf-8 -*-
# Rooms Models

from django.db import models
from socrative_users.models import SocrativeUser


class Room(models.Model):
    """
    Room model structure to be stored in the database
    """

    class Meta:
        verbose_name_plural = "Rooms"

    legacy_id = models.IntegerField(blank=True, null=True)

    name = models.CharField(db_index=False, unique=False, max_length=32)
    name_lower = models.CharField(db_index=True, unique=True, max_length=32)

    # user who created room
    created_by = models.ForeignKey(SocrativeUser, related_name="rooms_created", null=True, blank=True)

    # binary flag - null or 0 means no special status
    status = models.IntegerField(null=False, default=0)

    def __unicode__(self):
        return self.name


class Roster(models.Model):
    """
    Roster model structure to be stored in the database
    """

    class Meta:
        verbose_name_plural = "Rosters"

    sync_type = models.CharField(max_length=2, null=True)
    sync_id = models.IntegerField(null=True)
    status = models.IntegerField(null=False, default=0)
    data = models.CharField(max_length=2048, null=True)
    room = models.ForeignKey(Room, null=False, db_index=True, db_constraint=True)
    student_count = models.IntegerField(null=False, default=0)


class RoomCode(models.Model):
    """
    Table that will keep the share codes for rooms
    """

    code = models.TextField(max_length=16, db_index=True, unique=True)
    room_id = models.IntegerField(db_index=True, null=True)
    valid = models.BooleanField(default=True)


class RoomHistory(models.Model):
    """

    """

    room_name = models.TextField(max_length=32, db_index=True, null=False)
    room_id = models.IntegerField(null=False)
    user = models.ForeignKey(SocrativeUser)
    status = models.IntegerField(null=True)


class RosterFileSettings(models.Model):
    """

    """

    import_order = models.IntegerField(null=True)
    import_type = models.IntegerField(null=True)
    import_header = models.BooleanField(default=True)
    export_order = models.IntegerField(null=True)
    export_type = models.IntegerField(null=True)
    export_header = models.BooleanField(default=True)
    teacher = models.ForeignKey(SocrativeUser)
