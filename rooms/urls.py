# coding=utf-8
from django.conf.urls import url

from rooms.views import JoinRoomView, ClearRoomView, EndCurrentActivityView, GetCurrentActivity, CheckRoomNameView
from rooms.views import UpdateActivityInstanceStateView, TeacherRoomView, GetRoomListView, RoomStatusView
from rooms.views import RosterManagementView, GetRosteredStudentsView, AddStudentsView, ImportRosterView
from rooms.views import RosterDeleteView, ExportRosterView, RoomShareUrlView, HandraiseRoomStatusView
from rooms.views import RosterSettingsView, RosterImportSettingsView, RosterExportSettingsView

urlpatterns = [
    url(r'^api/join/?$', JoinRoomView.as_view()),
    url(r'^api/clear/?$', ClearRoomView.as_view()),
    url(r'^api/room/?$', TeacherRoomView.as_view()),
    url(r'^api/room/(?P<room_name>.[a-zA-Z0-9]{0,32})/?$', TeacherRoomView.as_view()),
    url(r'^api/list/?$', GetRoomListView.as_view()),
    url(r'^api/status/?$', RoomStatusView.as_view()),
    url(r'^api/share/?$', RoomShareUrlView.as_view()),
    
    url(r'^api/end-current-activity/?$', EndCurrentActivityView.as_view()),
    
    url(r'^api/current-activity/(?P<room_name>[^/]+)/?$', GetCurrentActivity.as_view()),
    
    url(r'^api/update-activity-instance-state/?$', UpdateActivityInstanceStateView.as_view()),
    url(r'^api/check-room-name/?$', CheckRoomNameView.as_view()),
    url(r'^api/students/?$', AddStudentsView.as_view()),
    url(r'^api/student/(?P<sid>.[0-9]{0,14})/(?P<room_name>[A-Za-z0-9]+)/?$', RosterManagementView.as_view()),
    url(r'^api/(?P<room_name>[a-zA-Z0-9]{1,32})/students/?$', GetRosteredStudentsView.as_view()),
    url(r'^api/(?P<room_name>[a-zA-Z0-9]{1,32})/roster/?$', RosterDeleteView.as_view()),
    url(r'^api/students/import/?$', ImportRosterView.as_view()),
    url(r'^api/students/export/(?P<room_name>[a-zA-Z0-9]{1,32})/?$', ExportRosterView.as_view()),
    url(r'^api/handraise/?$', HandraiseRoomStatusView.as_view()),
    url(r'^api/roster-settings/?$', RosterSettingsView.as_view()),
    url(r'^api/roster-settings/import/(?P<pk>[0-9]+)/?$', RosterImportSettingsView.as_view()),
    url(r'api/roster-settings/export/(?P<pk>[0-9]+)/?$', RosterExportSettingsView.as_view())

]
