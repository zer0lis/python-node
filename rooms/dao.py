# -*- coding: utf-8 -*-
import logging

from psycopg2 import OperationalError

from common import soc_constants
from common.base_dao import BaseDao, retry
from common.base_dao import BaseModel
from common.counters import DatadogThreadStats
from common.socrative_api import exceptionStack, isStringNotEmpty

logger = logging.getLogger(__name__)
statsd = DatadogThreadStats.STATS


class RoomModel(BaseModel):
    """

    """

    ID = "id"
    NAME = "name"
    CREATED_BY_ID = "created_by_id"
    LEGACY_ID = "legacy_id"
    NAME_LOWER = "name_lower"
    STATUS = "status"

    FIELD_COUNT = 6

    NO_STATUS = 0
    DEFAULT = 1
    INMENU = 2
    ROSTERED = 4
    LAST_USED = 8
    HANDRAISE = 16 # 0 off , 1 - on
    ARCHIVED = 128 # leave room for other stuff
    EXPIRED = 256

    def __init__(self):
        """
        Constructor
        :return:
        """
        self.id = None
        self.name = None
        self.created_by_id = None
        self.legacy_id = None
        self.name_lower = None
        self.status = 0

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.NAME] = self.name
        obj[self.CREATED_BY_ID] = self.created_by_id
        obj[self.LEGACY_ID] = self.legacy_id
        obj[self.NAME_LOWER] = self.name_lower
        obj[self.STATUS] = self.status

        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = RoomModel()
        model.id = dictObj.get(cls.ID)
        model.name = dictObj.get(cls.NAME)
        model.created_by_id = dictObj.get(cls.CREATED_BY_ID)
        model.legacy_id = dictObj.get(cls.LEGACY_ID)
        model.name_lower = dictObj.get(cls.NAME_LOWER)
        model.status = dictObj.get(cls.STATUS)

        return model


class RoomDao(BaseDao):
    """

    """
    FETCH_MANY_SIZE = 100
    MODEL = RoomModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(RoomDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.room.get_room_model_by_id.time')
    def getRoomModelById(self, roomId):
        """
        get the model using the pk
        """

        if roomId is None:
            raise ValueError("roomId must be a valid int , not None")

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM rooms_room WHERE id=%s""", (roomId,))
                if cursor.rowcount <= 0:
                    return None
                return self.fetchOneAsModel(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room.get_room_names.time')
    def getRoomNames(self, roomIds):
        """

        :param roomIds:
        :return:
        """

        if not roomIds:
            return list()

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT "name" FROM rooms_room WHERE id in %s""", (tuple(roomIds),))

                return [result[0] for result in cursor.fetchall()]
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room.load_room_by_creator.time')
    def loadRoomByCreator(self, userId, _type=BaseDao.TO_MODEL):
        """
        get the model using the pk
        """

        if userId is None:
            raise ValueError("userId must be a valid int , not None")

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM rooms_room WHERE created_by_id=%s and status & %s=%s ORDER BY id """
                               """ASC LIMIT 1""", (userId, RoomModel.LAST_USED, RoomModel.LAST_USED))

                if cursor.rowcount <= 0:
                    cursor.execute("""select * from rooms_room where created_by_id=%s and status & %s=%s ORDER BY id """
                                   """ASC LIMIT 1""", (userId, RoomModel.DEFAULT, RoomModel.DEFAULT))
                    if cursor.rowcount <= 0:
                        return None

                if _type == BaseDao.TO_MODEL:
                    return self.fetchOneAsModel(cursor)
                else:
                    return self.fetchOneAsDict(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.debug(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room.check_room_name.time')
    def checkRoomName(self, nameLower):
        """
        checks to see if a room with the nameLower provided already exists
        :param nameLower:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT id FROM rooms_room WHERE name_lower=%s LIMIT 1""", (nameLower,))
                if cursor.rowcount == 0:
                    return False
                else:
                    return True
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room.create_room.time')
    def createRoom(self, model):
        """
        """

        if model is None or type(model) is not dict:
            raise ValueError("model must be a room model dictionary")

        error = None
        try:
            with self.connection.cursor() as cursor:
                query = ",".join(["\"%s\"" % key for key in model if key != RoomModel.ID])
                values = ",".join(["%("+key+")s" for key in model if key != RoomModel.ID])
                values = cursor.mogrify(values, model)

                cursor.execute("""INSERT INTO rooms_room (""" + query + """) VALUES (""" + values + """) """
                               """RETURNING id""")

                return cursor.fetchone()[0]
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room.check_room_permission.time')
    def checkRoomPermission(self, auth_token, name_lower):
        """
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT a.id FROM rooms_room AS a INNER JOIN socrative_users_socrativeuser AS u ON"""
                               """ a.created_by_id=u.id WHERE a.name_lower=%s and u.auth_token=%s""", (name_lower,
                                                                                                       auth_token))
                if cursor.rowcount <= 0:
                    raise BaseDao.NotFound

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room.get_default_room.time')
    def getDefaultRoom(self, userId):
        """

        :param userId:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT name_lower FROM rooms_room WHERE created_by_id=%s AND status & %s = %s LIMIT 1""",
                               (userId, RoomModel.DEFAULT, RoomModel.DEFAULT))

                if cursor.rowcount <= 0:
                    return None

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room.get_room_by_name.time')
    def getRoomByName(self, name_lower, userId=None):
        """
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                if userId is None:
                    cursor.execute("""SELECT * FROM rooms_room WHERE name_lower=%s  and status < %s LIMIT 1""",
                                   (name_lower, RoomModel.ARCHIVED))
                else:
                    cursor.execute("""SELECT * FROM rooms_room WHERE name_lower=%s AND created_by_id=%s and status < %s LIMIT 1""",
                                   (name_lower, userId, RoomModel.ARCHIVED))

                if cursor.rowcount <= 0:
                    return None

                return self.fetchOneAsModel(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.room.get_room_by_name_or_user_id.time')
    def getRoomByNameOrUserId(self, name_lower, userId=None):
        """
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                if userId is None:
                    cursor.execute("""SELECT * FROM rooms_room WHERE name_lower=%s LIMIT 1""", (name_lower,))
                elif not isStringNotEmpty(name_lower):
                    cursor.execute("""SELECT * FROM rooms_room WHERE created_by_id=%s LIMIT 1""",
                                   (name_lower, userId))
                else:
                    cursor.execute("""SELECT * FROM rooms_room WHERE name_lower=%s AND created_by_id=%s LIMIT 1""",
                                   (name_lower, userId))

                if cursor.rowcount <= 0:
                    raise BaseDao.NotFound

                return self.fetchOneAsModel(cursor)

        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room.update_room_name.time')
    def updateRoomName(self, old_room_name, room_name):
        """
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE rooms_room SET "name"=%s,name_lower=%s WHERE name_lower=%s""",
                               (room_name, room_name.lower(), old_room_name.lower()))
                if cursor.rowcount == 0:
                    raise BaseDao.NotUpdated()
            # update the counter
            self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.room.delete_room.time')
    def deleteRoom(self, roomId):
        """

        :param roomId:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""DELETE from rooms_room where id=%s""", (roomId,))
                if cursor.rowcount == 0:
                    raise BaseDao.NotDeleted()
            # update the counter
            self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.room.load_rooms.time')
    def loadRooms(self, user_id):
        """

        :param user_id:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM rooms_room WHERE created_by_id=%s  and status < %s""",
                               (user_id, RoomModel.ARCHIVED))

                if cursor.rowcount <= 0:
                    return []

                return self.fetchManyAsDict(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room.get_all_active_rooms.time')
    def getAllActiveRooms(self, user_id):
        """

        :param user_id:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""select name_lower from rooms_room where created_by_id=%s and status < %s""",
                               (user_id, RoomModel.ARCHIVED))

                if cursor.rowcount <= 0:
                    return list()

                return [result[0] for result in cursor.fetchall()]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room.update_status_list.time')
    def updateStatusList(self, in_menu_onlist, in_menu_offlist, user_level, user_id):
        """

        :param in_menu_onlist:
        :param in_menu_offlist:
        :param user_level:
        :return:
        """
        try:
            error_ids = []
            with self.connection.cursor() as cursor:
                for room_id, status in in_menu_onlist:
                    max_status = RoomModel.ARCHIVED
                    if user_level == soc_constants.FREE:
                        max_status = RoomModel.ROSTERED
                    cursor.execute("""UPDATE rooms_room SET status=status | %s where id=%s and created_by_id=%s and """
                                   """status < %s and status & %s = 0""",(status, room_id, user_id, max_status,
                                                                          RoomModel.DEFAULT))
                    if cursor.rowcount == 0:
                        error_ids.append(room_id)

                for room_id, status in in_menu_offlist:
                    max_status = RoomModel.ARCHIVED
                    if user_level == soc_constants.FREE:
                        max_status = RoomModel.ROSTERED
                    cursor.execute("""UPDATE rooms_room SET status=status & %s where id=%s and created_by_id=%s and """
                                   """status < %s and status & %s = 0""",(status, room_id, user_id, max_status,
                                                                          RoomModel.DEFAULT))
                    if cursor.rowcount == 0:
                        error_ids.append(room_id)

            self.connection.commit()

            return error_ids

        except OperationalError as e:
            logger.error(exceptionStack(e))
            self.connection = 0
            raise
        except Exception as e:
            logger.debug(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.room.get_rooms_status.time')
    def getRoomsStatus(self, user_id, default=None, inmenu=None, rostered=None):
        """

        :param user_id:
        :param default:
        :param inmenu:
        :param active:
        :param rostered:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                status = 0
                if default is True:
                    status = status | 1
                if inmenu is True:
                    status = status | 2
                if rostered is True:
                    status = status | 4

                cursor.execute("""select id,status from rooms_room where created_by_id=%s and (status & %s != 0 or """
                               """ status = 0) and status < %s""", (user_id, status, RoomModel.ARCHIVED))
                roomStatusList = [(result[0], result[1]) for result in cursor.fetchall()]

                return roomStatusList

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room.get_last_used_room.time')
    def getLastUsedRoom(self, user_id, used_off):
        """

        :param user_id:
        :param used_off:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:

                cursor.execute("""update rooms_room set status=status & %s where created_by_id=%s and status & %s=%s"""
                               """ RETURNING name_lower""", (used_off,
                                                             user_id,
                                                             RoomModel.LAST_USED,
                                                             RoomModel.LAST_USED))
                if cursor.rowcount == 0:
                    return None

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room.update_last_used.time')
    def updateLastUsed(self, status, room_id):
        """

        :param status
        :param room_id
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE rooms_room SET status=status | %s WHERE id=%s """, (status, room_id))

                if cursor.rowcount != 1:
                    raise BaseDao.NotUpdated

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room.update_status.time')
    def updateStatus(self, status, room_name, user_id, no_if_activity_running=True):
        """

        :param status
        :param room_name
        :param user_id
        :param no_if_activity_running: don't update if there is a running activity in the room
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                if no_if_activity_running:
                    cursor.execute("""UPDATE rooms_room SET status=%s WHERE name_lower=%s AND (SELECT count(id) FROM """
                                   """common_activityinstance WHERE started_by_id=%s AND room_name=%s AND end_time is NULL)=0""",
                                   (status, room_name, user_id, room_name))
                else:
                    cursor.execute("""UPDATE rooms_room SET status=%s WHERE name_lower=%s AND (SELECT count(id) FROM """
                                   """common_activityinstance WHERE started_by_id=%s AND room_name=%s AND end_time is NULL)>=0""",
                                   (status, room_name, user_id, room_name))

                if cursor.rowcount != 1:
                    raise BaseDao.NotUpdated

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()


class RosterModel(BaseModel):
    """
    roster model
    """

    ID = "id"
    SYNC_TYPE = "sync_type"
    SYNC_ID = "sync_id"
    STATUS = "status"
    DATA = "data"
    STUDENT_COUNT = "student_count"
    ROOM_ID = "room_id"

    FIELD_COUNT = 7
    ARCHIVED = 4
    DEFAULT = 0

    def __init__(self):
        """
        Constructor
        :return:
        """
        self.id = None
        self.sync_type = None
        self.sync_id = None
        self.status = 0
        self.data = None
        self.student_count = 0
        self.room_id = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.SYNC_TYPE] = self.sync_type
        obj[self.SYNC_ID] = self.sync_id
        obj[self.STATUS] = self.status
        obj[self.DATA] = self.data
        obj[self.STUDENT_COUNT] = self.student_count

        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = RoomModel()
        model.id = dictObj.get(cls.ID)
        model.sync_type = dictObj.get(cls.SYNC_TYPE)
        model.sync_id = dictObj.get(cls.SYNC_ID)
        model.status = dictObj.get(cls.STATUS)
        model.data = dictObj.get(cls.DATA)
        model.student_count = dictObj.get(cls.STUDENT_COUNT)
        model.room_id = dictObj.get(cls.ROOM_ID)

        return model


class RosterDao(BaseDao):
    """

    """
    FETCH_MANY_SIZE = 100
    MODEL = RosterModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(RosterDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.roster.update_status.time')
    def updateStatus(self, status, room_id):
        """
        get the model using the pk
        :param status:
        :param room_id:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE rooms_roster SET "status"=%s WHERE room_id=%s and status < %s RETURNING id""",
                               (status, room_id, RosterModel.ARCHIVED))
                if cursor.rowcount != 1:
                    logger.info("there is no roster for room %d or the update failed" % room_id)
                    return None
                else:
                    return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            raise
        finally:
            if error != 0:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.roster.get_student_count.time')
    def getStudentCount(self, roomIds):
        """

        :param roomIds:
        :return:
        """
        if not roomIds:
            return dict()

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""select room_id, student_count from rooms_roster where room_id in %s and status < %s""",
                               (tuple(roomIds), RosterModel.ARCHIVED))
                return dict((a[0], a[1]) for a in cursor.fetchall())

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.roster.get_or_insert_roster.time')
    def getOrInsertRoster(self, room_id, sync_type=None, sync_id=None, status=0, data='', student_count=0):
        """

        :param room_id:
        :param sync_type:
        :param sync_id:
        :param status:
        :param data:
        :param student_count:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM rooms_roster WHERE status < %s AND room_id=%s""",
                               (RosterModel.ARCHIVED, room_id))
                if cursor.rowcount == 0:
                    cursor.execute("""INSERT INTO rooms_roster (sync_type, sync_id, status, "data", student_count, """
                                   """room_id) VALUES (%s, %s, %s, %s, %s, %s) RETURNING *""", (sync_type, sync_id,
                                                                                                status, data,
                                                                                                student_count,
                                                                                                room_id))
                    if cursor.rowcount != 1:
                        raise BaseDao.NotInserted()

                return self.fetchOneAsModel(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.roster.get_roster.time')
    def getRoster(self, room_id):
        """

        :param room_id:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM rooms_roster WHERE status < %s AND room_id=%s""",
                               (RosterModel.ARCHIVED, room_id))

                if cursor.rowcount == 0:
                    return None

                return self.fetchOneAsModel(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.roster.get_roster_ids.time')
    def getRosterIds(self, room_id):
        """
        get all the roster ids associated with a room
        :param room_id:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT id FROM rooms_roster WHERE room_id=%s""", (room_id, ))

                return [result[0] for result in cursor.fetchall()]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.roster.increment_student_count.time')
    def incrementStudentCount(self, roster_id, studentIncrement):
        """

        :param roster_id:
        :param studentIncrement:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""update rooms_roster set student_count = student_count + %s where id=%s""",
                               (studentIncrement, roster_id))
                if cursor.rowcount != 1:
                    raise BaseDao.NotUpdated()

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.roster.delete_rosters.time')
    def deleteRosters(self, roster_ids):
        """

        :param roster_ids:
        :return:
        """
        if not roster_ids:
            return

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""delete from rooms_roster where id in %s""", (tuple(roster_ids),))
                if cursor.rowcount <= 0:
                    raise BaseDao.NotDeleted()

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise


class RoomCodeModel(BaseModel):
    """
    model for the RoomCode table
    """

    ID = "id"
    CODE = "code"
    ROOM_ID = "room_id"
    VALID = "valid"

    FIELD_COUNT = 4

    def __init__(self):
        """
        Constructor
        :return:
        """
        self.id = None
        self.code = None
        self.room_id = None
        self.valid = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.CODE] = self.code
        obj[self.ROOM_ID] = self.room_id
        obj[self.VALID] = self.valid

        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = RoomCodeModel()
        model.id = dictObj.get(cls.ID)
        model.room_id= dictObj.get(cls.ROOM_ID)
        model.code = dictObj.get(cls.CODE)
        model.valid = dictObj.get(cls.VALID)

        return model


class RoomCodeDao(BaseDao):
    """

    """
    FETCH_MANY_SIZE = 100
    MODEL = RoomCodeModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(RoomCodeDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.room_code.insert.time')
    def insert(self, model):
        """
        get the model using the pk
        :param model
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE rooms_roomcode set "valid"=FALSE where room_id=%s and "valid"=TRUE""",
                               (model.room_id,))
                cursor.execute("""INSERT INTO rooms_roomcode (room_id, code, "valid") values (%s, %s, TRUE)""",
                               (model.room_id, model.code))

                self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            raise
        finally:
            if error != 0:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room_code.get_code.time')
    def getCode(self, room_id):
        """
        get all the roster ids associated with a room
        :param room_name:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT code FROM rooms_roomcode WHERE room_id=%s and "valid" is TRUE""", (room_id,))

                if cursor.rowcount <= 0:
                    return None

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room_code.get_codes.time')
    def getCodes(self, room_ids):
        """
        get all the roster ids associated with a room
        :param room_ids:
        :return:
        """
        error = None

        if not room_ids:
            return dict()

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT code,room_id FROM rooms_roomcode WHERE room_id in %s and "valid" is TRUE""",
                               (tuple(room_ids),))

                if cursor.rowcount <= 0:
                    return dict()

                return dict((result[1], result[0]) for result in cursor.fetchall())

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room_code.get_room_name.time')
    def getRoomName(self, room_code):
        """
        get the room name specific for a room code
        :param room_code:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT rr.name_lower FROM rooms_room as rr inner join rooms_roomcode as rc on """
                               """rr.id=rc.room_id WHERE rc.code=%s and rc."valid" is TRUE""", (room_code,))

                if cursor.rowcount <= 0:
                    return None

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()


class RoomHistoryModel(BaseModel):
    """
    model for the RoomCode table
    """

    ID = "id"
    ROOM_NAME = "room_name"
    ROOM_ID = "room_id"
    USER_ID = "user_id"
    STATUS = "status"

    FIELD_COUNT = 5

    def __init__(self):
        """
        Constructor
        :return:
        """
        self.id = None
        self.room_name = None
        self.room_id = None
        self.user_id = None
        self.status = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.ROOM_NAME] = self.room_name
        obj[self.ROOM_ID] = self.room_id
        obj[self.USER_ID] = self.user_id
        obj[self.STATUS] = self.status

        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = RoomCodeModel()
        model.id = dictObj.get(cls.ID)
        model.room_id = dictObj.get(cls.ROOM_ID)
        model.room_name = dictObj.get(cls.ROOM_NAME)
        model.user_id = dictObj.get(cls.USER_ID)
        model.status = dictObj.get(cls.STATUS)

        return model


class RoomHistoryDao(BaseDao):
    """
    keeps the history for the room name changes
    """

    FETCH_MANY_SIZE = 100
    MODEL = RoomHistoryModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(RoomHistoryDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.room_history.insert.time')
    def insert(self, user_id, room_name, room_id, status):
        """
        get the model using the pk
        :param model
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""insert into rooms_roomhistory (user_id, room_id, room_name, "status") values (%s, """
                               """%s, %s, %s)""", (user_id, room_id, room_name, status))

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            raise
        finally:
            if error != 0:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.room_history.get_room_id.time')
    def getRoomId(self, user_id, room_name):
        """
        get the room id from the room history table
        :param room_name:
        :return:
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT room_id, status from rooms_roomhistory where user_id=%s and room_name=%s""",
                               (user_id, room_name))

                if cursor.rowcount <= 0:
                    return None, None

                return cursor.fetchone()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise


class RosterFileSettingModel(BaseModel):
    """
    model for the RoomCode table
    """

    ID = "id"
    IMPORT_ORDER = "import_order"
    IMPORT_TYPE = "import_type"
    IMPORT_HEADER = "import_header"
    EXPORT_ORDER = "export_order"
    EXPORT_TYPE = "export_type"
    EXPORT_HEADER = "export_header"
    TEACHER_ID = "teacher_id"

    FIELD_COUNT = 8

    def __init__(self):
        """
        Constructor
        :return:
        """
        self.id = None
        self.import_order = None
        self.import_type = None
        self.import_header = None
        self.export_order = None
        self.export_type = None
        self.export_header = None
        self.teacher_id = None

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.EXPORT_HEADER] = self.export_header
        obj[self.IMPORT_HEADER] = self.import_header
        obj[self.IMPORT_TYPE] = self.import_type
        obj[self.EXPORT_TYPE] = self.export_type
        obj[self.IMPORT_ORDER] = self.import_order
        obj[self.EXPORT_ORDER] = self.export_order
        obj[self.TEACHER_ID] = self.teacher_id

        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = RosterFileSettingModel()
        model.id = dictObj.get(cls.ID)
        model.import_order = dictObj.get(cls.IMPORT_ORDER)
        model.import_type = dictObj.get(cls.IMPORT_TYPE)
        model.import_header = dictObj.get(cls.IMPORT_HEADER)
        model.export_order = dictObj.get(cls.EXPORT_ORDER)
        model.export_type = dictObj.get(cls.EXPORT_TYPE)
        model.export_header = dictObj.get(cls.EXPORT_HEADER)
        model.teacher_id = dictObj.get(cls.TEACHER_ID)

        return model

    @classmethod
    def fromRequestDto(cls, dtoModel):
        """

        :param dtoModel:
        :return:
        """
        model = RosterFileSettingModel()
        model.id = dtoModel.pk if hasattr(dtoModel, "pk") else None
        model.teacher_id = None
        model.import_order = dtoModel.import_order
        model.import_type = dtoModel.import_type
        model.import_header = dtoModel.import_header
        model.export_order = dtoModel.export_order
        model.export_type = dtoModel.export_type
        model.export_header = dtoModel.export_header

        return model


class RosterFileSettingsDao(BaseDao):
    """
    keeps the history for the room name changes
    """

    FETCH_MANY_SIZE = 100
    MODEL = RosterFileSettingModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(RosterFileSettingsDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.roster_file_settings.get_settings.time')
    def getSettings(self, user_id, _type=BaseDao.TO_MODEL):
        """

        :param user_id:
        :param _type:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM rooms_rosterfilesettings WHERE teacher_id=%s""", (user_id,))

                if _type == BaseDao.TO_DICT:
                    return self.fetchOneAsDict(cursor)
                else:
                    return self.fetchOneAsModel(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.roster_file_settings.get_export_settings.time')
    def getExportSettings(self, user_id):
        """

        :param user_id:
        :param _type:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT export_type,export_order, export_header FROM rooms_rosterfilesettings WHERE"""
                               """ teacher_id=%s""", (user_id,))
                d = dict()
                r = cursor.fetchone()
                for i in range(len(cursor.description)):
                    d[cursor.description[i][0]] = r[i]

                return d

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.roster_file_settings.get_import_settings.time')
    def getImportSettings(self, user_id):
        """

        :param user_id:
        :param _type:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT import_type,import_order,import_header FROM rooms_rosterfilesettings WHERE"""
                               """ teacher_id=%s""", (user_id,))
                d = dict()
                r = cursor.fetchone()
                for i in range(len(cursor.description)):
                    d[cursor.description[i][0]] = r[i]

                return d

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.roster_file_settings.check_settings.time')
    def checkSettings(self, user_id):
        """

        :param user_id:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT id FROM rooms_rosterfilesettings WHERE teacher_id=%s""", (user_id,))

                if cursor.rowcount <=0:
                    return None

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.roster_file_settings.create.time')
    def create(self, dictModel):
        """

        :param dictModel:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                query = ",".join(["\"%s\"" % key for key in dictModel if key != RosterFileSettingModel.ID])
                values = ",".join(["%(" + key + ")s" for key in dictModel if key != RosterFileSettingModel.ID])
                values = cursor.mogrify(values, dictModel)

                # TODO check if we need all the values back
                cursor.execute("""INSERT INTO rooms_rosterfilesettings (""" + query + """) VALUES (""" + values + """)"""
                               """RETURNING id""")

                if cursor.rowcount != 1:
                    raise self.NotInserted

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.roster_file_settings.load.time')
    def load(self, settings_id, user_id):
        """

        :param user_id:
        :param settings_id:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM rooms_rosterfilesettings WHERE teacher_id=%s and id=%s""",
                               (user_id, settings_id))

                if cursor.rowcount <= 0:
                    return None

                return self.fetchOneAsModel(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.roster_file_settings.update_settings.time')
    def updateSettings(self, updateDict, settings_id):
        """

        :param updateDict:
        :param settings_id:
        :return:
        """
        conn = None
        try:
            with self.connection.cursor() as cursor:
                query = ",".join(["\"" + key + "\"" + "=%(" + key + ")s" for key in updateDict])
                query = cursor.mogrify(query, updateDict)
                conn = 1
                cursor.execute("""UPDATE rooms_rosterfilesettings SET """ + query + """ WHERE id=%s""", (settings_id, ))

                if cursor.rowcount != 1:
                    raise self.NotUpdated

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            self.connection = None
            raise

        except Exception as e:
            logger.debug(exceptionStack(e))
            if conn:
                self.connection.rollback()
            raise