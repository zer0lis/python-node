# coding=utf-8
import logging
import ujson

from common import soc_constants
from common.base_view import BaseView
from common.counters import DatadogThreadStats
from common.serializers import AuthenticatedRequestDTO
from common.socrative_api import exceptionStack
from common.socrative_errors import BaseError
from rooms.counters import RoomCounters
from rooms.serializers import CreateRoomRequestDTO, GetRoomListRequestDTO, DeleteRoomDTO, RoomShareUrlRequestDTO
from rooms.serializers import CreateRosterSettingsRequestDTO, UpdateRosterImportSettingsRequestDTO
from rooms.serializers import DeleteStudentFromRosterRequestDTO, UpdateStudentFromRosterRequestDTO
from rooms.serializers import ExportRosterRequestDTO, EndCurrentActivityRequestDTO, HandraiseRoomStatusRequestsDTO
from rooms.serializers import GetCurrentActivityRequestDTO, UpdateActivityStateRequestDTO, JoinRoomRequestDTO
from rooms.serializers import GetRosteredStudentsRequestDTO, ImportRosterRequestDTO, DeleteRosterRequestDTO
from rooms.serializers import UpdateRoomDTO, RoomStatusListDTO, AddRosteredStudentRequestDTO
from rooms.serializers import UpdateRosterExportSettingsRequestDTO

logger = logging.getLogger(__name__)
statsd = DatadogThreadStats.STATS


class JoinRoomView(BaseView):
    """

    """

    http_method_names = ["post"]

    @statsd.timed(RoomCounters.JOIN_ROOM)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.JOIN_ROOM_FAIL

        data = dict()

        self.getAuthToken(data)

        for k in request.POST:
            data[k] = request.POST.get(k)

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            pass

        dto = JoinRoomRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        if dto.auth_token == "" and dto.name:
            # studCount = request.roomService.roomFullLimiter.get(dto.name)
            studCount = 0
            if studCount >= soc_constants.MAX_FREE_STUDENTS:
                d = dict()
                uuid = self.getStudentUUID(d)
                if d.get("user_uuid") is None:
                    statsd.increment(RoomCounters.JOIN_ROOM_FULL)
                    return self.render_json_response(BaseError.FREE_ROOM_FULL, statusCode=400, failCounter=failCounter)
                else:
                    isMember = request.roomService.roomFullLimiter.isMember(dto.name, uuid)
                    if isMember is False:
                        statsd.increment(RoomCounters.JOIN_ROOM_FULL)
                        return self.render_json_response(BaseError.FREE_ROOM_FULL, statusCode=400,
                                                         failCounter=failCounter)

        response = request.roomService.joinRoom(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], language=response[2], cookie=response[3],
                                         failCounter=failCounter)


class ClearRoomView(BaseView):
    """
    class that handle the clear room api
    """
    http_method_names = ["post"]

    @statsd.timed(RoomCounters.CLEAR_ROOM)
    def post(self, request, *args, **kwargs):
        """
        handler for HTTP POST for clear room api
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.CLEAR_ROOM_FAIL

        data = dict()
        for k in request.POST:
            data[k] = request.POST.get(k)

        self.getAuthToken(data)

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            pass
        response = request.roomService.clearRoom(request, data)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class GetCurrentActivity(BaseView):
    """
    class that handles the get current activity api
    """
    http_method_names = ["get"]

    @statsd.timed(RoomCounters.GET_CURRENT_ACTIVITY)
    def get(self, request, *args, **kwargs):
        """
        handler for HTTP GET for get current activity api
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.GET_CURRENT_ACTIVITY_FAIL

        data = dict()
        data.update(kwargs)

        self.getAuthToken(data)
        self.getStudentUUID(data)

        dto = GetCurrentActivityRequestDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.getCurrentActivity(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class EndCurrentActivityView(BaseView):
    """
    class to handle the end current activity api
    """
    http_method_names = ["post"]

    @statsd.timed(RoomCounters.END_CURRENT_ACTIVITY)
    def post(self, request, *args, **kwargs):
        """
        handler for HTTP POST request for end current activity api
        """

        failCounter = RoomCounters.END_CURRENT_ACTIVITY_FAIL

        data = dict()
        for k in request.POST:
            data[k] = request.POST.get(k)

        self.getAuthToken(data)

        dto = EndCurrentActivityRequestDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.endCurrentActivity(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class UpdateActivityInstanceStateView(BaseView):
    """
    class that handles the update activity instance state api
    """

    http_method_names = ["post"]

    @statsd.timed(RoomCounters.UPDATE_ACTIVITY_STATE)
    def post(self, request, *args, **kwargs):
        """
        handler for HTTP POST for update activity instance state
        """

        failCounter = RoomCounters.UPDATE_ACTIVITY_STATE_FAIL

        data = dict()
        self.getAuthToken(data)

        for k in request.POST:
            data[k] = request.POST.get(k)

        dto = UpdateActivityStateRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.updateActivityState(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class CheckRoomNameView(BaseView):
    """
    class for changing a teachers' room name
        request:
            auth_token
            new_room_name
    """

    @statsd.timed(RoomCounters.CHECK_ROOM_NAME)
    def post(self, request, *args, **kwargs):
        """
        Handler for check room name api
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.CHECK_ROOM_NAME_FAIL

        data = dict()
        for k in request.POST:
            data[k] = request.POST.get(k)
        response = request.roomService.checkRoomName(request, data)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class TeacherRoomView(BaseView):
    """

    """

    http_method_names = ["post", "delete", "put"]

    @statsd.timed(RoomCounters.CREATE_ROOM)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.CREATE_ROOM_FAIL

        data = dict()
        self.getAuthToken(data)
        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            pass

        dto = CreateRoomRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.createRoom(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)

    @statsd.timed(RoomCounters.DELETE_ROOM)
    def delete(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.DELETE_ROOM_FAIL

        data = dict()
        self.getAuthToken(data)

        data.update(kwargs)

        dto = DeleteRoomDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.deleteRoom(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)

    @statsd.timed(RoomCounters.UPDATE_ROOM)
    def put(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.UPDATE_ROOM_FAIL

        data = dict()
        self.getAuthToken(data)
        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            pass

        data.update(kwargs)

        dto = UpdateRoomDTO.fromDict(data)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.updateRoom(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class GetRoomListView(BaseView):
    """

    """
    http_method_names = ["get"]

    @statsd.timed(RoomCounters.GET_ROOM_LIST)
    def get(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.GET_ROOM_LIST_FAIL

        data = dict()
        self.getAuthToken(data)

        for k in request.GET:
            data[k] = request.GET.get(k)

        dto = GetRoomListRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.getRoomList(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class RoomStatusView(BaseView):
    """
    class to handle the update for single and multiple rooms (in menu or not in menu)
    """

    http_method_names = ["post"]

    @statsd.timed(RoomCounters.UPDATE_ROOM_STATUS)
    def post(self, request, *args, **kwargs):
        """
        method to handle the http request to update the room's status
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.UPDATE_ROOM_FAIL

        data = dict()
        self.getAuthToken(data)

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            pass

        dto = RoomStatusListDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.updateRoomStatus(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class AddStudentsView(BaseView):
    """
    """

    http_method_names = ["post"]

    @statsd.timed(RoomCounters.ROSTER_ADD_STUDENTS)
    def post(self, request, *args, **kwargs):
        """
        Adds a student into a rostered room , or if the room is not rostered and it's not default it becomes rostered
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.ROSTER_ADD_STUDENTS_FAIL

        data = dict()
        self.getAuthToken(data)

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            pass

        dto = AddRosteredStudentRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.addStudents(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class RosterManagementView(BaseView):
    """

    """

    http_method_names = ["put", "delete"]

    @statsd.timed(RoomCounters.ROSTER_DELETE_STUDENT)
    def delete(self, request, *args, **kwargs):
        """
        removes students from roster
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.ROSTER_DELETE_STUNDET_FAIL

        data = dict()
        self.getAuthToken(data)

        try:
            data.update(kwargs)
            data["id"] = kwargs.get("sid")
        except Exception as e:
            logger.debug(exceptionStack(e))
            pass

        dto = DeleteStudentFromRosterRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.deleteStudent(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)

    @statsd.timed(RoomCounters.ROSTER_UPDATE_STUDENT)
    def put(self, request, *args, **kwargs):
        """
        removes students from roster
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.ROSTER_UPDATE_STUDENT_FAIL

        data = dict()
        self.getAuthToken(data)

        try:
            data.update(kwargs)
            data["id"] = kwargs.get("sid")
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            pass

        dto = UpdateStudentFromRosterRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.updateStudent(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class GetRosteredStudentsView(BaseView):
    """
    view for getting the student list
    """

    http_method_names = ["get"]

    @statsd.timed(RoomCounters.GET_ROSTER)
    def get(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.GET_ROSTER_FAIL

        data = dict()
        self.getAuthToken(data)

        try:
            data.update(kwargs)
        except Exception as e:
            logger.debug(exceptionStack(e))
            pass

        dto = GetRosteredStudentsRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.getRosteredStudents(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class ImportRosterView(BaseView):
    """
    view for getting the student list
    """

    http_method_names = ["post"]

    @statsd.timed(RoomCounters.IMPORT_ROSTER)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.IMPORT_ROSTER_FAIL

        data = dict()
        self.getAuthToken(data)

        try:
            for k in request.POST:
                data[k] = request.POST.get(k)
        except Exception as e:
            logger.debug(exceptionStack(e))
            pass

        dto = ImportRosterRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.importRoster(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class RosterDeleteView(BaseView):
    """
    view to delete a roster
    """

    http_method_names = ["delete"]

    @statsd.timed(RoomCounters.DELETE_ROSTER)
    def delete(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.DELETE_ROSTER_FAIL

        data = dict()
        self.getAuthToken(data)

        data.update(kwargs)

        dto = DeleteRosterRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.deleteRoster(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class ExportRosterView(BaseView):
    """
    view to export a roster
    """

    http_method_names = ["get"]

    @statsd.timed(RoomCounters.EXPORT_ROSTER)
    def get(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.EXPORT_ROSTER_FAIL

        data = dict()
        self.getAuthToken(data)

        for k in request.GET:
            data[k] = request.GET.get(k)

        data.update(kwargs)

        dto = ExportRosterRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.exportRoster(request, dto)

        if response[1] == 200:  # if it's 200 OK, then we send the file
            return self.return_streaming_file(response[0], filename=response[2], statusCode=response[1])
        else:
            return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class RoomShareUrlView(BaseView):
    """
    view for getting the student list
    """

    http_method_names = ["post"]

    @statsd.timed(RoomCounters.SHARE_ROOM_URL)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.SHARE_ROOM_URL_FAIL

        data = dict()
        self.getAuthToken(data)

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            pass

        dto = RoomShareUrlRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.shareUrl(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class HandraiseRoomStatusView(BaseView):
    """
    view for setting the room handraise status
    """

    http_method_names = ["post"]

    @statsd.timed(RoomCounters.UPDATE_ROOM_HANDRAISE)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.UPDATE_ROOM_HANDRAISE_FAIL

        data = dict()
        self.getAuthToken(data)

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST, statusCode=400, failCounter=failCounter)

        dto = HandraiseRoomStatusRequestsDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.updateRoomHandraiseStatus(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class RosterSettingsView(BaseView):
    """
    class to handle roster settings view
    """

    http_method_names = ["get", "post"]

    @statsd.timed(RoomCounters.GET_ROSTER_FILE_SETTINGS)
    def get(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.GET_ROSTER_FILE_SETTINGS_FAIL

        data = dict()
        self.getAuthToken(data)

        dto = AuthenticatedRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.getRosterFileSettings(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)

    @statsd.timed(RoomCounters.CREATE_ROSTER_FILE_SETTINGS)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.CREATE_ROSTER_FILE_SETTINGS_FAIL

        data = dict()
        self.getAuthToken(data)

        try:
            data.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            return BaseError.INVALID_REQUEST, 400

        dto = CreateRosterSettingsRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.createRosterFileSettings(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class RosterImportSettingsView(BaseView):
    """

    """

    http_method_names = ["put"]

    @statsd.timed(RoomCounters.UPDATE_IMPORT_ROSTER_SETTINGS)
    def put(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.UPDATE_IMPORT_ROSTER_SETTINGS_FAIL

        data = dict()
        self.getAuthToken(data)

        try:
            data.update(ujson.loads(request.body))
            data.update(kwargs)
        except Exception as e:
            logger.debug(exceptionStack(e))
            return BaseError.INVALID_REQUEST, 400

        dto = UpdateRosterImportSettingsRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.updateRosterSettings(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)


class RosterExportSettingsView(BaseView):
    """

    """

    http_method_names = ["put"]

    @statsd.timed(RoomCounters.UPDATE_EXPORT_ROSTER_SETTINGS)
    def put(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = RoomCounters.UPDATE_EXPORT_ROSTER_SETTINGS_FAIL

        data = dict()
        self.getAuthToken(data)
        try:
            data.update(ujson.loads(request.body))
            data.update(kwargs)
        except Exception as e:
            logger.debug(exceptionStack(e))
            return BaseError.INVALID_REQUEST, 400

        dto = UpdateRosterExportSettingsRequestDTO.fromDict(data)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        response = request.roomService.updateRosterSettings(request, dto)

        return self.render_json_response(response[0], statusCode=response[1], failCounter=failCounter)
