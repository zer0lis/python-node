# -*- coding: utf-8 -*-
import logging
import time

from datadog import statsd
from psycopg2 import OperationalError

from common.base_dao import BaseDao, retry
from common.base_dao import BaseModel
from common.socrative_api import isStringNotEmpty, exceptionStack, safeToInt
from students.dao import StudentNameModel
from students.dao import StudentResponseModel

logger = logging.getLogger(__name__)


class MediaResourceModel(BaseModel):
    """
    model for media resource
    """

    ID = "id"
    OWNER_ID = "owner_id"
    NAME = "name"
    TYPE = "type"
    URL = "url"
    DATA = "data"

    IMAGE = "IM"
    VIDEO = "VD"
    AUDIO_CLIP = "AC"
    YOUTUBE_LINK = "YT"

    FIELD_COUNT = 6

    def __init__(self):
        """
        Constructor
        """
        self.id = None
        self.owner_id = None
        self.name = None
        self.type = None
        self.url = None
        self.data = "{}"

    def toDict(self):
        """
        return: StudentNameModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.OWNER_ID] = self.owner_id
        obj[self.NAME] = self.name
        obj[self.TYPE] = self.type
        obj[self.URL] = self.url
        obj[self.DATA] = self.data if self.data != "" else "{}"
        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = MediaResourceModel()
        model.id = dictObj.get(cls.ID)
        model.owner_id = dictObj.get(cls.OWNER_ID)
        model.name = dictObj.get(cls.NAME)
        model.type = dictObj.get(cls.TYPE)
        model.url = dictObj.get(cls.URL)
        model.data = dictObj.get(cls.DATA, "{}")
        if model.data == "":
            model.data = "{}"

        return model


class MediaResourceDao(BaseDao):
    """
    dao class for media resource
    """

    FETCH_MANY_SIZE = 100
    MODEL = MediaResourceModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(MediaResourceDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.media_resource.clone_resources.time')
    def cloneResources(self, oldQIdList, newQIdList, userId, my_cursor):
        """

        :param oldQIdList:
        :param newQIdList:
        :param userId:
        :param my_cursor:
        :return:
        """

        try:

            dOld = dict((oldQIdList[i], newQIdList[i]) for i in range(len(oldQIdList)))

            with (my_cursor or self.connection.cursor()) as cursor:
                #get media resource id's to clone
                cursor.execute(""" SELECT mediaresource_id, question_id FROM quizzes_question_resources WHERE """
                               """question_id in %s""", (tuple(oldQIdList),))

                if cursor.rowcount == 0:
                    return

                mIdDict = dict((a[0], a[1]) for a in cursor.fetchall())

                cursor.execute("""SELECT * from common_mediaresource WHERE id IN %s""", (tuple(mIdDict.keys()),))
                resourcesDict = self.fetchManyAsDict(cursor)
                for model in resourcesDict:
                    model[MediaResourceModel.OWNER_ID] = userId

                orderedList = [a[MediaResourceModel.ID] for a in resourcesDict]

                newIds = list()
                for resourceDict in resourcesDict:
                    cursor.execute("""INSERT INTO common_mediaresource (owner_id,"name","type","url") VALUES """
                                   """(%(owner_id)s,%(name)s,%(type)s,%(url)s) RETURNING id""", resourceDict)
                    newIds.append(cursor.fetchone()[0])

                dNewMediaResourceId = dict((orderedList[i], newIds[i]) for i in range(len(orderedList)))

                listOfValues = [(dNewMediaResourceId[key], dOld[mIdDict[key]]) for key in dNewMediaResourceId]

                q = """INSERT INTO quizzes_question_resources (mediaresource_id, question_id) VALUES """ + \
                    ",".join(["(%d, %d)" % (a[0], a[1]) for a in listOfValues])
                cursor.execute(q)

                if cursor.rowcount <= 0:
                    raise self.NotInserted

                if not my_cursor:
                    self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.media_resource.create_resource.time')
    def createResource(self, owner, name, media_type, url):
        """
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""INSERT INTO common_mediaresource (owner_id, "name", "type", url, "data") VALUES """
                               """(%s, %s, %s, %s, '{}') RETURNING id""", (owner, name, media_type, url))

                if cursor.rowcount != 1:
                    raise self.NotInserted

                return safeToInt(cursor.fetchone()[0])
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error == 1:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.media_resource.get_images.time')
    def getImages(self, questionIds, _type=MediaResourceModel.IMAGE):
        """
        """
        error = None

        if not questionIds:
            return dict()

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT a.question_id,c.* FROM quizzes_question as a INNER JOIN """
                               """quizzes_question_resources AS b ON a.question_id=b.question_id INNER JOIN """
                               """common_mediaresource as c ON b.mediaresource_id=c.id WHERE a.question_id in %s """
                               """and c.type=%s""", (tuple(questionIds), _type))
                resp = cursor.fetchall()

                respDict = dict()
                for tup in resp:
                    respDict[tup[0]] = self._loadModelFromResult(tup, cursor.description)

                return respDict

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.media_resource.load_media_for_quiz.time')
    def loadMediaForQuiz(self, quizId):
        """

        :param quizId:
        :return:
        """

        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT mr.* from common_mediaresource as mr INNER JOIN quizzes_question_resources AS"""
                               """ qq ON mr.id=qq.mediaresource_id INNER JOIN quizzes_question as q ON """
                               """qq.question_id=q.question_id INNER JOIN quizzes_quiz as qz on q.quiz_id=qz.id"""
                               """ WHERE qz.id=%s""", (quizId,))
                return self.fetchManyAsModel(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.media_resource.get_media_by_id.time')
    def getMediaById(self, mediaResourceId, auth_token=None, _type=BaseDao.TO_MODEL):
        """
        :param _type
        :param mediaResourceId:
        :return:
        """

        try:
            with self.readOnlyConnection.cursor() as cursor:
                if auth_token:
                    cursor.execute("""SELECT a.* FROM common_mediaresource as a INNER JOIN socrative_users_socrativeuser"""
                                   """ as b ON b.id = a.owner_id WHERE a.id=%s and b.auth_token=%s""",
                                   (mediaResourceId, auth_token))
                else:
                    cursor.execute("""SELECT * FROM common_mediaresource WHERE id=%s""", (mediaResourceId,))

                if cursor.rowcount == 0:
                    raise BaseDao.NotFound

                if _type == BaseDao.TO_DICT:
                    return self.fetchOneAsDict(cursor)
                else:
                    return self.fetchOneAsModel(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except BaseDao.NotFound:
            logger.debug("No media resource found for id %d" % mediaResourceId)
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.media_resource.update_data.time')
    def updateData(self, mediaId, data):
        """
        """
        try:
            start = time.time()
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE common_mediaresource SET "data"=%s WHERE id=%s""", (data, mediaId))

                if cursor.rowcount != 1:
                    raise self.NotUpdated

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.media_resource.delete_resource.time')
    def deleteResource(self, mediaId, question_id=None, auth_token=None):
        """
        """
        try:
            with self.connection.cursor() as cursor:

                if question_id is not None and auth_token is None:
                    cursor.execute("""DELETE FROM quizzes_question_resources WHERE mediaresource_id=%s AND """
                                   """question_id=%s""", (mediaId, question_id))

                    if cursor.rowcount != 1:
                        raise BaseDao.NotFound

                    cursor.execute("""DELETE FROM common_mediaresource  WHERE id=%s""", (mediaId, ))
                elif question_id is not None and auth_token is not None:
                    cursor.execute("""DELETE FROM quizzes_question_resources WHERE mediaresource_id=(SELECT a.id FROM """
                                   """common_mediaresource as a INNER JOIN socrative_users_socrativeuser as b ON """
                                   """a.owner_id=b.id WHERE a.id=%s AND b.auth_token=%s) AND question_id=%s""",
                                   (mediaId, auth_token, question_id))

                    if cursor.rowcount != 1:
                        raise BaseDao.NotFound

                    cursor.execute("""DELETE FROM common_mediaresource  WHERE id=(SELECT a.id FROM common_mediaresource"""
                                   """ AS a INNER JOIN socrative_users_socrativeuser as b on a.owner_id=b.id AND """
                                   """b.auth_token=%s WHERE a.id=%s)""", (auth_token, mediaId))
                else:
                    cursor.execute("""DELETE FROM common_mediaresource  WHERE id=%s""", (mediaId, ))

            if cursor.rowcount != 1:
                raise BaseDao.NotDeleted

            self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.media_resource.update_media_resource.time')
    def updateMediaResource(self, mediaDict):
        """

        :param  mediaDict:
        :return None
        """

        if len(mediaDict) == 0:
            return
        try:
            with self.connection.cursor() as cursor:
                query = ",".join([key+"=%("+key+")s" for key in mediaDict if key != MediaResourceModel.ID])
                query = cursor.mogrify(query, mediaDict)
                cursor.execute("""UPDATE common_mediaresource SET """ + query + """ WHERE id=%d""" % mediaDict[MediaResourceModel.ID])

                if cursor.rowcount != 1:
                    raise self.NotUpdated

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise


class PartnerModel(BaseModel):
    """

    """

    # Type of resource that's being linked to
    EDMODO = "ED"
    ALWAYS_PREPPED = "AP"
    GOOGLE = "GO"
    PREMIUM = "PR"
    TWITTER = "TW"
    MASTERY_CONNECT = "MC"
    MASTERY_CONNECT_FREE = "MC"
    MASTERY_CONNECT_PAID = "MP"
    BACKUP_EMAIL = "BK"
    TYPE_CHOICES = (
        (EDMODO, u"Edmodo"),
        (ALWAYS_PREPPED, u"Always Prepped"),
        (GOOGLE, u"Google"),
        (PREMIUM, u"Socrative Premium"),
        (TWITTER, u'Twitter'),
        (MASTERY_CONNECT, u'Mastery Connect'),
        (BACKUP_EMAIL, u'Backup email')
    )

    ID = "id"
    USER_ID = "user_id"
    TYPE = "type"
    DATA = "data"

    def __init__(self):
        """
        Constructor
        """
        self.id = None
        self.user_id = None
        self.type = None
        self.data = None

    def toDict(self):
        """
        model to dict
        """
        obj = dict()
        obj[self.ID] = self.id
        obj[self.USER_ID] = self.user_id
        obj[self.TYPE] = self.type
        obj[self.DATA] = self.data
        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = PartnerModel()
        model.id = dictObj.get(cls.ID)
        model.user_id = dictObj.get(cls.USER_ID)
        model.type = dictObj.get(cls.TYPE)
        model.data = dictObj.get(cls.DATA)

        return model


class PartnerDao(BaseDao):
    """
    dao class to manage partner objects
    """

    FETCH_MANY_SIZE = 100
    MODEL = PartnerModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        """

        super(PartnerDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.partner.get_partner.time')
    def getPartner(self, userId, pType, _type=BaseDao.TO_MODEL, silent=False):
        """
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT * FROM common_partner WHERE user_id=%s AND "type"=%s""", (userId, pType))
                if cursor.rowcount <= 0:
                    if silent is False:
                        raise BaseDao.NotFound
                    else:
                        return None

                if _type == self.TO_MODEL:
                    return self.fetchOneAsModel(cursor)
                else:
                    return self.fetchOneAsDict(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise

        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.partner.check_partner.time')
    def checkPartner(self, userId, pType):
        """
        """

        error = None
        try:

            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT id FROM common_partner WHERE user_id=%s AND type=%s""", (userId, pType))
                if cursor.rowcount <=0:
                    return False

                return True
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.partner.update_partner.time')
    def updatePartner(self, partnerDict):
        """
        """
        try:
            with self.connection.cursor() as cursor:
                query = ",".join([key+"=%("+key+")s" for key in partnerDict if key != PartnerModel.ID])
                query = cursor.mogrify(query, partnerDict)
                cursor.execute("""UPDATE common_partner SET """ + query + """ WHERE id=%d""" % partnerDict[PartnerModel.ID])

                if cursor.rowcount != 1:
                    raise self.NotUpdated

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.partner.update_data.time')
    def updateData(self, partnerId, data):
        """
        """
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE common_partner SET "data"=%s WHERE id=%s""", (data, partnerId))

                if cursor.rowcount != 1:
                    raise self.NotUpdated

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.partner.create_partner.time')
    def createPartner(self, modelDict):
        """
        :param modelDict - dictionary representing the partner object model
        """

        try:
            with self.connection.cursor() as cursor:
                query = ",".join(["\"%s\"" % key for key in modelDict if key != PartnerModel.ID])
                values = ",".join(["%("+key+")s" for key in modelDict if key != PartnerModel.ID])
                values = cursor.mogrify(values, modelDict)

                cursor.execute("""INSERT INTO common_partner (""" + query + """) VALUES (""" + values +
                               """)""")

                if cursor.rowcount != 1:
                    raise self.NotInserted("partner object not inserted in the database")

            self.connection.commit()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            raise


class ActivitySettingModel(BaseModel):
    """
    model class
    """

    ID = "id"
    KEY = "key"
    VALUE = "value"

    FIELD_COUNT = 3

    def __init__(self):
        """
        Constructor
        :return:
        """

        self.id = None
        self.key = None
        self.value = None

    def toDict(self):
        """
        model to dict
        """
        obj = dict()
        obj[self.ID] = self.id
        obj[self.KEY] = self.key
        obj[self.VALUE] = self.value
        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = PartnerModel()
        model.id = dictObj.get(cls.ID)
        model.key = dictObj.get(cls.KEY)
        model.value = dictObj.get(cls.VALUE)

        return model


class ActivitySettingsDao(BaseDao):
    """
    dao class to handle activity settings
    """

    FETCH_MANY_SIZE = 100
    MODEL = ActivitySettingModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """

        :param dbConfigCtx:
        :param connMgr:
        :return:
        """

        super(ActivitySettingsDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.activity_settings.update_team_assignment.time')
    def updateTeamAssignment(self, activityId):
        """
        returns teamCount, increments next team assignment and returns next team assignment
        """

        if activityId is None:
            raise ValueError("activityId must be a valid integer not None")

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE common_activitysetting SET value=value::int + 1 WHERE key=%s AND id IN"""
                               """(SELECT activitysetting_id FROM common_activityinstance_settings WHERE """
                               """activityinstance_id=%s) RETURNING value""", ("next_team_assignment", activityId))
                next_team_assignment = cursor.fetchone()[0]
                self.connection.commit()

                return next_team_assignment
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            logger.error(exceptionStack(e))
            raise

    @retry
    @statsd.timed('socrative.dao.activity_settings.get_setting.time')
    def getSetting(self, activity_id, setting_key):
        """
        :param activity_id: activity instance id
        :param setting_key: string for the setting name
        :return bool
        """
        if activity_id is None:
            raise ValueError("activity id must be a valid int")

        if not isStringNotEmpty(setting_key):
            raise ValueError("setting_key must be a valid string")

        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT a.value FROM common_activitysetting AS a INNER JOIN """
                               """common_activityinstance_settings AS b ON a.id=b.activitysetting_id WHERE a.key=%s"""
                               """ AND b.activityinstance_id=%s""", (setting_key, activity_id))
                if cursor.rowcount > 0:
                    return cursor.fetchone()[0]
                else:
                    return None
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.activity_settings.get_settings.time')
    def getSettings(self, activity_id, setting_keys):
        """
        :param activity_id: activity instance id
        :param setting_key: string for the setting name
        :return bool
        """

        error = None
        if activity_id is None:
            raise ValueError("activity id must be a valid int")

        if not setting_keys:
            raise ValueError("setting_keys must be a non empty list of strings")

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT cas.key,cas.value FROM common_activitysetting AS cas INNER JOIN """
                               """common_activityinstance_settings AS cais ON cas.id=cais.activitysetting_id WHERE """
                               """ cais.activityinstance_id=%s AND cas.key in %s""", (activity_id, tuple(setting_keys)))
                resp = dict()
                if cursor.rowcount <= 0:
                    return resp

                return dict((result[0], result[1]) for result in cursor.fetchall())
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.debug(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_settings.get_all_settings.time')
    def getAllSettings(self, activity_id):
        """
        :param activity_id: activity instance id
        :return bool
        """

        error = None
        if activity_id is None:
            raise ValueError("activity id must be a valid int")

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT cas.key,cas.value FROM common_activitysetting AS cas INNER JOIN """
                               """common_activityinstance_settings AS cais ON cas.id=cais.activitysetting_id WHERE """
                               """ cais.activityinstance_id=%s""", (activity_id,))
                resp = dict()
                if cursor.rowcount <= 0:
                    return resp

                return dict((result[0], result[1]) for result in cursor.fetchall())
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.debug(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_settings.create_settings.time')
    def createSettings(self, activityInstanceId, settingsDict):
        """
        :param activityInstanceId:
        :param settingsDict:
        """
        if len(settingsDict) == 0:
            return

        try:
            with self.connection.cursor() as cursor:
                query = ",".join([cursor.mogrify("""(%s,%s)""", (s, settingsDict[s])) for s in settingsDict.keys()])
                cursor.execute("""INSERT INTO common_activitysetting ("key","value") VALUES """ + query +\
                               """ RETURNING id""")
                ids = [i[0] for i in cursor.fetchall()]

                query = ",".join([cursor.mogrify("""(%s,%s)""", (activityInstanceId, i)) for i in ids])
                cursor.execute("""INSERT INTO common_activityinstance_settings ("activityinstance_id","activitysetting_id")""" +
                               """ VALUES """ + query)

                self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise


class ActivityInstanceModel(BaseModel):
    """
    activity instance model
    """

    # what activity was run
    QUIZ = "QZ"
    SINGLE_QUESTION = "1Q"
    SINGLE_QUESTION_NO_REPORT = "NR"  # single question no report
    SINGLE_QUESTION_VOTING = "1V"
    SPACE_RACE = "SR"
    TYPE_CHOICES = (
        (QUIZ, u"Quiz"),
        (SINGLE_QUESTION, u"Single Question Activity"),
        (SPACE_RACE, u"Space Race"),
        (SINGLE_QUESTION_NO_REPORT, u"Single Question Activity without reports available")
    )

    ID = "id"
    START_TIME = "start_time"
    END_TIME = "end_time"
    ACTIVITY_TYPE = "activity_type"
    ACTIVITY_ID = "activity_id"
    STARTED_BY_ID = "started_by_id"
    ROOM_NAME = "room_name"
    ACTIVITY_STATE = "activity_state"
    LEGACY_ID = "legacy_id"
    HIDE_REPORT = "hide_report"
    REPORT_STATUS = "report_status"
    STUDENTS_FINISHED = "students_finished"
    STATE = "state"
    LAST_UPDATED = "last_updated"
    # ACTIVITY_SETTING = "activity_setting"

    REPORT_IGNORE = 3
    REPORT_DONE = 2
    REPORT_IN_PROGRESS = 1
    REPORT_NOT_DONE = 0

    # STATE VALUES
    ACTIVE = 0
    ARCHIVED = 16
    TRASHED = 128

    # QUIZ = 1
    # SPACE_RACE = 2
    # SHORT_ANSWER = 4
    # MULTIPLE_CHOICE = 8
    # TRUE_FALSE = 16
    # VOTE = 32
    # EXIT_TICKET = 64
    #
    # PACING = 65536
    # STUDENT_NAVIGATION = 131072
    # REQUIRE_STUDENT_NAMES = 262144
    # RANDOMIZE_QUESTION_ORDER = 524288
    # RANDOMIZE_ANSWER_ORDER = 1048576
    # SHOW_QUESTION_FEEDBACK = 2097152
    # SHOW_FINAL_SCORE = 4194304

    FIELD_COUNT = 14

    def __init__(self):
        """
        Constructor
        """
        self.id = None
        self.start_time = None
        self.end_time = None
        self.activity_type = None
        self.activity_id = None
        self.started_by_id = None
        self.room_name = None
        self.activity_state = None
        self.legacy_id = None
        self.hide_report = None
        self.report_status = None
        self.students_finished = "[]"
        self.state = None
        self.last_updated = None

    def toDict(self):
        """
        model to dict
        """
        obj = dict()
        obj[self.ID] = self.id
        obj[self.START_TIME] = self.start_time
        obj[self.END_TIME] = self.end_time
        obj[self.ACTIVITY_TYPE] = self.activity_type
        obj[self.ACTIVITY_ID] = self.activity_id
        obj[self.STARTED_BY_ID] = self.started_by_id
        obj[self.ACTIVITY_STATE] = self.activity_state
        obj[self.LEGACY_ID] = self.legacy_id
        obj[self.HIDE_REPORT] = self.hide_report
        obj[self.REPORT_STATUS] = self.report_status
        obj[self.STUDENTS_FINISHED] = self.students_finished
        obj[self.ROOM_NAME] = self.room_name
        obj[self.STATE] = self.state
        obj[self.LAST_UPDATED] = self.last_updated

        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = ActivityInstanceModel()
        model.id = dictObj.get(cls.ID)
        model.start_time = dictObj.get(cls.START_TIME)
        model.end_time = dictObj.get(cls.END_TIME)
        model.activity_type = dictObj.get(cls.ACTIVITY_TYPE)
        model.activity_id = dictObj.get(cls.ACTIVITY_ID)
        model.started_by_id = dictObj.get(cls.STARTED_BY_ID)
        model.activity_state = dictObj.get(cls.ACTIVITY_STATE)
        model.legacy_id = dictObj.get(cls.LEGACY_ID)
        model.hide_report = dictObj.get(cls.HIDE_REPORT)
        model.report_status = dictObj.get(cls.REPORT_STATUS)
        model.students_finished = dictObj.get(cls.STUDENTS_FINISHED, "[]")
        model.room_name = dictObj.get(cls.ROOM_NAME)
        model.state = dictObj.get(cls.STATE)
        model.last_updated = dictObj.get(cls.LAST_UPDATED)

        return model


# A record of a single 'run' of a Quiz
class ActivityInstanceDao(BaseDao):
    """
    class to handle activity instance database access
    """

    FETCH_MANY_SIZE = 100
    MODEL = ActivityInstanceModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """

        :param dbConfigCtx:
        :param connMgr:
        :return:
        """

        super(ActivityInstanceDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.activity_instance.get_activity_by_id.time')
    def getActivityById(self, activityInstanceId, _type=BaseDao.TO_MODEL):
        """
        retrieves an activity based on it's id
        """

        if activityInstanceId is None:
            raise ValueError("activityInstanceId must be a valid int, not None")

        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT * from common_activityinstance WHERE id=%s""", (activityInstanceId,))
                if _type == self.TO_MODEL:
                    return self.fetchOneAsModel(cursor)
                else:
                    return self.fetchOneAsDict(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            raise


    @retry
    @statsd.timed('socrative.dao.activity_instance.count_activities.time')
    def countActivities(self, activity_id, started_by):
        """
        counts the number of activities for a specific user and quiz
        :param activity_id:
        :param started_by:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT COUNT(id) FROM common_activityinstance WHERE activity_id=%s AND """
                               """started_by_id=%s""", (activity_id, started_by))
                return cursor.fetchone()[0]
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_instance.load_activity_by_quiz_id.time')
    def loadActivityByQuizId(self, quiz_id, started_by_id, limit, _type=BaseDao.TO_DICT):
        """
        load an activity based on the quiz_id=activity_id, started by a specific user and return a specific amount "limit"
        of results
        :param quiz_id:
        :param started_by_id:
        :param limit:
        :param _type:
        :return:
        """

        error = None
        try:

            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM common_activityinstance WHERE activity_id=%s AND started_by_id=%s LIMIT %s""",
                               (quiz_id, started_by_id, limit))

                if _type == BaseDao.TO_DICT:
                    return self.fetchManyAsDict(cursor)
                else:
                    return self.fetchOneAsModel(cursor)

        except OperationalError as e:
            #the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise

        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error > 0:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_instance.get_activity_data_for_quiz.time')
    def getActivityDataForQuiz(self, quiz_id, user_id, room_name):
        """

        :param quiz_id:
        :param user_id:
        :param room_name
        :return:
        """
        from rooms.dao import RoomModel

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""select ca.id, ca.room_name, r.status from common_activityinstance as ca INNER JOIN """
                               """ rooms_room as r on ca.started_by_id=r.created_by_id and ca.room_name=r.name_lower """
                               """ where ca.started_by_id=%s and ca.activity_id=%s and r.name_lower=%s and end_time is NULL""",
                               (user_id, quiz_id, room_name))

                if cursor.rowcount <= 0:
                    return None, None, None
                else:
                    _id, room_name, status = cursor.fetchone()
                    rostered = status & RoomModel.ROSTERED == RoomModel.ROSTERED
                    return _id, room_name, rostered

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            error = 0
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.activity_instance.update_report_status.time')
    def updateReportStatus(self, activityId, reportStatus):
        """

        :param activityId:
        :param reportStatus:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE common_activityinstance SET report_status=%s WHERE id=%s""", (reportStatus,
                                                                                                       activityId))
                if cursor.rowcount != 1:
                    raise BaseDao.NotUpdated

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.activity_instance.update_finished_students.time')
    def updateFinishedStudents(self, activityId, studentIds):
        """
        update the finished_students field in the activity instance model
        :param activityId:
        :param studentIds:
        :return:
        """

        if not isStringNotEmpty(studentIds):
            raise ValueError("studentIds is not a string, but : %s" % str(type(studentIds)))

        if type(activityId) not in (int, long) or activityId <= 0:
            raise ValueError("activityId is not a valid integer, but : %s" % str(type(activityId)))

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE common_activityinstance SET students_finished=%s WHERE id=%s""", (studentIds,
                                                                                                           activityId))
                if cursor.rowcount != 1:
                    raise BaseDao.NotUpdated

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.activity_instance.get_filtered_activity_ids.time')
    def getFilteredActivityIds(self, user_id, state, offset, limit, terms, roomName=None, notInRooms=None):
        """
        get filtered activity ids depending on state
        :param user_id:
        :param state:
        :param offset:
        :param limit:
        :param terms:
        :param roomName:
        :param notInRooms:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""select count(ca.id), ca.state from common_activityinstance as ca INNER JOIN quizzes_quiz"""
                               """ as qz on ca.activity_id=qz.id inner join quizzes_question as qq on qq.quiz_id=qz.id"""
                               """ and qq.order=(select "order" from quizzes_question where quiz_id=qz.id limit 1) """
                               """ where ca.started_by_id=%s and ca.end_time is not NULL and ca.activity_type not IN %s"""
                               """ group by ca.state""", (user_id, (ActivityInstanceModel.SINGLE_QUESTION_NO_REPORT,
                                                                 ActivityInstanceModel.SINGLE_QUESTION_VOTING)))

                metadata = dict((result[1], result[0]) for result in cursor.fetchall())

                cursor.execute("""select count(ca.id), ca.room_name from common_activityinstance as ca INNER JOIN """
                               """quizzes_quiz as qz on ca.activity_id=qz.id inner join quizzes_question as qq on """
                               """qq.quiz_id=qz.id and qq.order=(select "order" from quizzes_question where """
                               """quiz_id=qz.id limit 1) where ca.started_by_id=%s and ca.end_time is not NULL and """
                               """ ca.activity_type not IN %s and (ca.state=%s OR ca.state is NULL)"""
                               """ group by ca.room_name""", (user_id, (ActivityInstanceModel.SINGLE_QUESTION_NO_REPORT,
                                                                        ActivityInstanceModel.SINGLE_QUESTION_VOTING),
                                                              ActivityInstanceModel.ACTIVE))
                room_distrib = dict((result[1], result[0]) for result in cursor.fetchall())

                if state == ActivityInstanceModel.ACTIVE:
                    stateStr = cursor.mogrify(""" and (ca.state=%s OR ca.state is NULL) """, (state, ))
                elif state is not None:
                    stateStr = cursor.mogrify(""" and ca.state=%s """, (state, ))
                else:
                    stateStr = ""

                selectedFields = cursor.mogrify("""select ca.id, ca.start_time, ca.end_time, ca.last_updated, """
                                                """ ca.room_name, ca.activity_id, ca.activity_type, qz.name, """
                                                """ qq.question_text""")
                query = cursor.mogrify(""" from common_activityinstance as ca INNER JOIN quizzes_quiz as qz on """
                                       """ ca.activity_id=qz.id inner join quizzes_question as qq on qq.quiz_id=qz.id"""
                                       """ and qq.order=(select "order" from quizzes_question where quiz_id=qz.id """
                                       """ limit 1) where ca.started_by_id=%s and ca.activity_type not in %s and """
                                       """ca.end_time is not NULL """,
                                       (user_id, (ActivityInstanceModel.SINGLE_QUESTION_NO_REPORT,
                                                  ActivityInstanceModel.SINGLE_QUESTION_VOTING)))

                query += stateStr

                if terms:
                    query += """and qz.name ILIKE '%%%s%%' """ % terms

                if roomName:
                    query += cursor.mogrify(""" and ca.room_name=%s""", (roomName, ))

                if notInRooms:
                    query += cursor.mogrify(""" and ca.room_name not in %s""", (tuple(notInRooms), ))

                orderQuery = cursor.mogrify(" order by ca.start_time DESC offset %s LIMIT %s""", (offset, limit))


                cursor.execute(selectedFields + query + orderQuery)

                resultData = dict((
                                      res[0],
                                      (res[5], res[6], res[7], res[1], res[2], res[3], res[4], res[8])
                                   )
                                  for res in cursor.fetchall()
                                  )

                # count only the results
                countResults = """select count(ca.id) """
                cursor.execute(countResults + query)
                total_results = cursor.fetchone()[0]

                metadata["rooms"] = room_distrib
                resultData["metadata"] = metadata
                resultData["total_results"] = total_results
                return resultData

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.debug(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error == 1:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_instance.purge_reports.time')
    def purgeReports(self, activity_instance_ids):
        """
        remove from database the reports with the ids received in the list
        :param activity_instance_ids:
        :return:
        """

        if not activity_instance_ids:
            return

        try:
            with self.connection.cursor() as cursor:
                # step 1 - delete the activity settings mappings
                cursor.execute("""delete from common_activityinstance_settings where activityinstance_id in %s """
                               """returning activitysetting_id""",
                               (tuple(activity_instance_ids),))
                activity_settings_ids = [res[0] for res in cursor.fetchall()]

                # step 2 - delete the activity settings
                if activity_settings_ids:
                    cursor.execute("""delete from common_activitysetting where id in %s""",
                                   (tuple(activity_settings_ids),))

                # step 3 - delete students_activitystudent
                cursor.execute("""delete from students_activitystudent where activity_instance_id in %s""",
                               (tuple(activity_instance_ids),))

                # step 4 - delete students_name
                cursor.execute("""delete from students_studentname where activity_instance_id in %s returning """
                               """activity_instance_id, user_uuid""", (tuple(activity_instance_ids),))
                cache_keys = [(res[1], res[0]) for res in cursor.fetchall()]

                # step 5 - get the response ids
                cursor.execute("""select id from student_responses where activity_instance_id in %s """,
                               (tuple(activity_instance_ids), ))
                resp_ids = [res[0] for res in cursor.fetchall()]

                if resp_ids:
                    # step 6 - delete the student response text answer
                    cursor.execute("""delete from student_responses_text_answer where student_response_id in %s""",
                                   (tuple(resp_ids), ))

                    # step 7 - delete the student response answer selection
                    cursor.execute("""delete from student_responses_answer_selection where student_response_id in %s""",
                                   (tuple(resp_ids),))

                    # step 8 - delete the student responses
                    cursor.execute("""delete from student_responses where id in %s""", (tuple(resp_ids),))

                # step 9 - delete common_activityinstance
                cursor.execute("""delete from common_activityinstance where id in %s""",
                               (tuple(activity_instance_ids),))

                # DONE

            self.connection.commit()

            return cache_keys
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.activity_instance.load_deep_reports.time')
    def loadDeepReports(self, activityIds):
        """
        load deep reports
        :return:
        """
        error = None
        if not activityIds:
            return list()
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM common_activityinstance WHERE id IN %s""", (tuple(activityIds),))
                responses = cursor.fetchall()
                activities = dict()
                for response in responses:
                    offset = ActivityInstanceModel.FIELD_COUNT
                    activity = self._loadDictFromResultTupleInterval(response, cursor.description, 0, offset,
                                                                     ActivityInstanceModel)
                    activity["student_names"] = []
                    activity["responses"] = []
                    activities[activity["id"]] = activity

                cursor.execute("""SELECT * FROM students_studentname WHERE activity_instance_id IN %s""",
                               (tuple(activityIds),))
                students = cursor.fetchall()
                for student in students:
                    offset = StudentNameModel.FIELD_COUNT
                    studentName = self._loadDictFromResultTupleInterval(student, cursor.description, 0, offset,
                                                                        StudentNameModel)
                    activity = activities[studentName["activity_instance_id"]]
                    activity["student_names"].append(studentName)

                cursor.execute("""SELECT * FROM student_responses WHERE activity_instance_id IN %s""",
                               (tuple(activityIds),))
                responses = cursor.fetchall()

                for response in responses:
                    offset = StudentResponseModel.FIELD_COUNT
                    studentResponse = self._loadDictFromResultTupleInterval(response, cursor.description, 0, offset,
                                                                            StudentResponseModel)
                    activity = activities[studentResponse["activity_instance_id"]]
                    activity["responses"].append(studentResponse)


                cursor.execute("""SELECT a.id,qz.name,qs.question_text, qs.type FROM common_activityinstance  AS a """
                               """INNER JOIN quizzes_quiz AS qz ON a.activity_id=qz.id INNER JOIN """
                               """quizzes_question AS qs ON qs.quiz_id=a.activity_id AND qs.order=(SELECT MIN("order")"""
                               """ FROM quizzes_question WHERE quiz_id=qz.id) WHERE a.id IN %s""", (tuple(activityIds),)
                )

                responses = cursor.fetchall()

                for response in responses:

                    activity = activities[response[0]]
                    offset = 1
                    if "name" not in activity or (activity["name"] == "" and response[offset] != ""):
                        activity["name"] = response[offset] or ""

                    if "question" not in activity:
                        activity["question"] = dict()

                    if "question_text" not in activity["question"] or (activity["question"]["question_text"] == "" and
                                                                       response[offset+1] != ""):
                        activity["question"]["question_text"] = response[offset+1]

                    if "type" not in activity["question"] or (activity["question"]["type"] == "" and
                                                              response[offset+2] != ""):
                        activity["question"]["type"] = response[offset+2]

                cursor.execute("""SELECT a.*, b.activityinstance_id from common_activitysetting AS a INNER JOIN """
                               """common_activityinstance_settings as b ON a.id = b.activitysetting_id INNER JOIN """
                               """common_activityinstance AS d ON b.activityinstance_id=d.id WHERE d.id in %s""",
                               (tuple(activityIds),))
                settings = cursor.fetchall()
                sDict = dict()

                for s in settings:
                    setting = self._loadDictFromResultTupleInterval(s, cursor.description, 0, 3, ActivitySettingModel)
                    if s[3] in sDict:
                        sDict[s[3]].append(setting)
                    else:
                        sDict[s[3]] = [setting]

                for a in activities.values():
                    a["settings"] = sDict[a["id"]]

                return activities.values()

        except OperationalError as e:
            logger.error(exceptionStack(e))
            error = 0
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error == 1:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_instance.get_trashed_ids.time')
    def getTrashedIds(self, user_id, activityIds):
        """
        load deep reports
        :return:
        """
        error = None
        if not activityIds:
            return list()

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT id FROM common_activityinstance where id in %s and state=%s and """
                               """started_by_id=%s""", (tuple(activityIds), ActivityInstanceModel.TRASHED, user_id))

                activity_ids = [res[0] for res in cursor.fetchall()]

                return activity_ids

        except OperationalError as e:
            logger.error(exceptionStack(e))
            error = 0
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error == 1:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_instance.load_simple_reports.time')
    def loadSimpleReports(self, activityIds):
        """
        load deep reports
        :return:
        """
        error = None
        if not activityIds:
            return list()
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT a.id, a.start_time, a.activity_id, a.activity_type, a.hide_report, qz.name, """
                               """qs.question_text, qs.type FROM common_activityinstance AS a INNER JOIN quizzes_quiz"""
                               """ AS qz ON a.activity_id=qz.id INNER JOIN quizzes_question AS qs ON """
                               """qs.quiz_id=a.activity_id AND qs.order=(SELECT MIN("order") from quizzes_question """
                               """WHERE quiz_id=qz.id) WHERE a.id IN %s""", (tuple(activityIds),))

                responses = cursor.fetchall()

                activities = dict()

                for response in responses:
                    start = 0
                    offset = 5
                    activity = self._loadDictFromResultTupleInterval(response, cursor.description, start, offset,
                                                                     ActivityInstanceModel)

                    if "name" not in activity or (activity["name"] == "" and response[offset] != ""):
                        activity["name"] = response[offset] or ""

                    if "question" not in activity:
                        activity["question"] = dict()

                    if "question_text" not in activity["question"] or (activity["question"]["question_text"] == "" and
                                                                       response[offset+1] != ""):
                        activity["question"]["question_text"] = response[offset+1]

                    if "type" not in activity["question"] or (activity["question"]["type"] == "" and
                                                              response[offset+2] != ""):
                        activity["question"]["type"] = response[offset+2]

                    if activity["id"] not in activities:
                        activities[activity["id"]] = activity

                return activities.values()

        except OperationalError as e:
            logger.error(exceptionStack(e))
            error = 0
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error == 1:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_instance.update_state.time')
    def updateState(self, user_id, instance_id, state):
        """

        :param user_id:
        :param instance_id:
        :param state:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE common_activityinstance SET state=%s, last_updated=CURRENT_TIMESTAMP WHERE """
                               """id=%s and started_by_id=%s""", (state, instance_id, user_id))

                if cursor.rowcount < 1:
                    raise BaseDao.NotUpdated

                self.connection.commit()
        except OperationalError as e:
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.activity_instance.update_states.time')
    def updateStates(self, user_id, instance_ids, state):
        """

        :param user_id:
        :param instance_ids:
        :param state:
        :return:
        """

        if not instance_ids:
            return

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE common_activityinstance SET state=%s, last_updated=CURRENT_TIMESTAMP WHERE """
                               """id in %s and started_by_id=%s""", (state, tuple(instance_ids), user_id))

                self.connection.commit()
        except OperationalError as e:
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.activity_instance.get_running_quiz_ids.time')
    def getRunningQuizIds(self, user_id, quiz_ids):
        """
        load deep reports
        :return:
        """
        error = None
        if not quiz_ids:
            return list()

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT activity_id FROM common_activityinstance where started_by_id=%s and """
                               """end_time is NULL and activity_id in %s""", (user_id, tuple(quiz_ids)))

                quiz_ids = [res[0] for res in cursor.fetchall()]

                return quiz_ids

        except OperationalError as e:
            logger.error(exceptionStack(e))
            error = 0
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error == 1:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_instance.load_deep_activity.time')
    def loadDeepActivity(self, auth_token, activityId):
        """
        load deep reports
        :return:
        """
        from students.dao import StudentNameModel
        from students.dao import StudentResponseModel
        error = None

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT a.*, qz.name, qs.question_text, qs.type FROM common_activityinstance AS a """
                               """INNER JOIN quizzes_quiz AS qz ON a.activity_id=qz.id INNER JOIN quizzes_question AS"""
                               """ qs ON qs.quiz_id=a.activity_id AND qs.order=(SELECT MIN("order") FROM """
                               """quizzes_question WHERE quiz_id=qz.id) INNER JOIN """
                               """socrative_users_socrativeuser as u ON a.started_by_id=u.id WHERE a.id=%s AND """
                               """u.auth_token=%s""", (activityId, auth_token)
                )

                if cursor.rowcount <= 0:
                    raise BaseDao.NotFound
                # load all the results:
                response = cursor.fetchone()

                activity = None
                start = 0
                offset = ActivityInstanceModel.FIELD_COUNT
                activity = self._loadDictFromResultTupleInterval(response, cursor.description, start, offset,
                                                                 ActivityInstanceModel)
                activity["name"] = response[offset] or ""

                activity["question"] = dict()

                activity["question"]["question_text"] = response[offset+1]

                activity["question"]["type"] = response[offset+2]

                cursor.execute("""SELECT * FROM students_studentname WHERE activity_instance_id=%s""", (activityId,))

                students = cursor.fetchall()

                activity["student_names"] = []

                for student in students:
                    offset = StudentNameModel.FIELD_COUNT
                    studentName = self._loadDictFromResultTupleInterval(student, cursor.description, 0, offset,
                                                                        StudentNameModel)
                    activity["student_names"].append(studentName)

                cursor.execute("""SELECT * FROM student_responses WHERE activity_instance_id=%s""", (activityId,)
                )

                sResponses = dict()
                responses = cursor.fetchall()
                for response in responses:
                    offset = StudentResponseModel.FIELD_COUNT
                    studentResponse = self._loadDictFromResultTupleInterval(response, cursor.description, start, offset,
                                                                            StudentResponseModel)
                    studentResponse["selection_answers"] = list()
                    studentResponse["text_answers"] = list()
                    sResponses[studentResponse["id"]] = studentResponse
                if len(sResponses) > 0:
                    cursor.execute("""SELECT student_response_id,answer_text FROM student_responses_text_answer """
                                   """WHERE student_response_id IN %s""", (tuple(sResponses.keys()),)
                    )

                    textAnswers = cursor.fetchall()
                    for textAnswer in textAnswers:
                        sResponse = sResponses[textAnswer[0]]
                        sResponse["text_answers"].append({"answer_text": textAnswer[1] or ""})

                    cursor.execute("""SELECT student_response_id, answer_id FROM student_responses_answer_selection"""
                                   """ WHERE student_response_id IN %s""", (tuple(sResponses.keys()),))

                    selections = cursor.fetchall()
                    for selection in selections:
                        sResponse = sResponses[selection[0]]
                        sResponse["selection_answers"].append({"answer_id": selection[1]})


                cursor.execute("""SELECT a.* from common_activitysetting AS a INNER JOIN """
                               """common_activityinstance_settings as b ON a.id = b.activitysetting_id INNER JOIN """
                               """common_activityinstance AS d ON b.activityinstance_id=d.id WHERE d.id=%s""",
                               (activityId,))
                settings = cursor.fetchall()
                sList = list()

                for s in settings:
                    setting = self._loadDictFromResultTupleInterval(s, cursor.description, 0, 3, ActivitySettingModel)
                    sList.append(setting)

                activity["responses"] = sResponses.values()

                activity["settings"] = sList

                return activity

        except OperationalError as e:
            logger.error(exceptionStack(e))
            error = 0
            self.connection = None
            raise
        except BaseDao.NotFound:
            logger.debug("activity not found for %d", activityId)
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error == 1:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_instance.check_vote_activity.time')
    def checkVoteActivity(self, ai_model):
        """
        return the activity for 1Q
        :param ai_model:
        :return:
        """

        if ai_model.activity_type not in (ActivityInstanceModel.SINGLE_QUESTION_VOTING,
                                          ActivityInstanceModel.SINGLE_QUESTION):
            return None
        try:
            with self.readOnlyConnection.cursor() as cursor:
                if ai_model.activity_type == ActivityInstanceModel.SINGLE_QUESTION:
                    cursor.execute("""SELECT a.id, a.activity_type FROM """
                                   """common_activityinstance as a INNER JOIN quizzes_question as q ON """
                                   """a.activity_id=q.quiz_id WHERE a.end_time>%s AND started_by_id=%s and room_name=%s"""
                                   """ ORDER BY a.end_time DESC LIMIT 1""", (ai_model.end_time,
                                                                             ai_model.started_by_id,
                                                                             ai_model.room_name))
                else:
                    cursor.execute("""SELECT a.id, a.activity_type FROM """
                                   """common_activityinstance as a INNER JOIN quizzes_question as q ON """
                                   """a.activity_id=q.quiz_id WHERE end_time<%s AND started_by_id=%s and room_name=%s"""
                                   """ ORDER BY a.end_time DESC LIMIT 1""", (ai_model.end_time,
                                                                             ai_model.started_by_id,
                                                                             ai_model.room_name))

                if cursor.rowcount != 1:
                    return None

                result = cursor.fetchone()
                if result[1] not in (ActivityInstanceModel.SINGLE_QUESTION, ActivityInstanceModel.SINGLE_QUESTION_VOTING):
                    return None

                if ai_model.activity_type == ActivityInstanceModel.SINGLE_QUESTION and result[1] != ActivityInstanceModel.SINGLE_QUESTION_VOTING:
                    return None

                elif ai_model.activity_type == ActivityInstanceModel.SINGLE_QUESTION_VOTING and result[1] != ActivityInstanceModel.SINGLE_QUESTION:
                    return None

                return safeToInt(result[0])
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.activity_instance.get_activity_ids_by_room_name.time')
    def getActivityIdsByRoomName(self, userId, room_name):
        """

        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT id FROM common_activityinstance WHERE started_by_id=%s AND room_name=%s AND """
                               """end_time is NULL ORDER BY id DESC""", (userId, room_name))
                ids = [a[0] for a in cursor.fetchall()]

                return ids
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error == 1:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_instance.get_activity_ids_by_room_names.time')
    def getActivityIdsByRoomNames(self, userId, room_names):
        """

        :return:
        """
        error = None
        if not room_names:
            return dict()

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT room_name,id FROM common_activityinstance WHERE started_by_id=%s AND """
                               """room_name in %s AND end_time is NULL """, (userId, tuple(room_names)))
                respDict = dict((resp[0], resp[1]) for resp in cursor.fetchall())

                return respDict
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error == 1:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_instance.close_activities.time')
    def closeActivities(self, activityIds):
        """

        :return:
        """

        if not activityIds:
            return

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE common_activityinstance SET end_time=CURRENT_TIMESTAMP WHERE id IN %s AND """
                               """end_time is NULL""", (tuple(activityIds), ))
            # update counter
            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.activity_instance.get_activity_by_room_name.time')
    def getActivityByRoomName(self, auth_token, name_lower):
        """

        :param auth_token:
        :param name_lower:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT a.* FROM common_activityinstance AS a INNER JOIN socrative_users_socrativeuser"""
                               """ AS u ON a.started_by_id=u.id WHERE u.auth_token=%s AND a.room_name=%s AND """
                               """a.end_time is NULL ORDER BY a.id DESC LIMIT 1""", (auth_token, name_lower))

                if cursor.rowcount == 0:
                    return None

                return self.fetchOneAsModel(cursor)
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error == 1:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_instance.close_activities_by_room.time')
    def closeActivitiesByRoom(self, userId, room_name):
        """

        :param userId:
        :param room_name:
        :return:
        """
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE common_activityinstance SET end_time=CURRENT_TIMESTAMP, """
                               """last_updated = CURRENT_TIMESTAMP, state=%s WHERE started_by_id=%s"""
                               """ AND room_name=%s AND end_time is NULL""", (ActivityInstanceModel.ACTIVE,
                                                                              userId,
                                                                              room_name))

            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.debug(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.activity_instance.update_activity_state.time')
    def updateActivityState(self, name_lower, auth_token, state_data):
        """

        :param name_lower:
        :param auth_token:
        :param state_data
        :return:
        """
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE common_activityinstance SET activity_state=%s WHERE id=(SELECT a.id FROM """
                               """common_activityinstance as a INNER JOIN socrative_users_socrativeuser as u ON """
                               """ a.started_by_id=u.id WHERE a.room_name=%s and u.auth_token=%s ORDER BY a.id DESC """
                               """ LIMIT 1)""", (state_data, name_lower, auth_token))
            # update counter
            self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.activity_instance.load_activity_for_student.time')
    def loadActivityForStudent(self, activityId):
        """

        :param activityId:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT * FROM common_activityinstance WHERE id=%s""", (activityId,))
                if cursor.rowcount <=0:
                    raise BaseDao.NotFound

                activity = self.fetchOneAsDict(cursor)

                cursor.execute("""SELECT a.* FROM common_activitysetting as a INNER JOIN common_activityinstance_settings"""
                               """ AS b ON a.id=b.activitysetting_id WHERE b.activityinstance_id=%s""", (activityId,))

                activity["settings"] = self.fetchManyAsDict(cursor)

                return activity
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error == 1:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_instance.create_activity.time')
    def createActivity(self, activityModel):
        """
        load an activity based on the quiz_id=activity_id, started by a specific user and return a specific amount "limit"
        of results
        :param activityModel:
        :return:
        """

        if activityModel is None or type(activityModel) is not dict:
            raise ValueError("model must be an activity instance model dictionary")

        error = None
        try:
            with self.connection.cursor() as cursor:
                query = ",".join(["\"%s\"" % key for key in activityModel if key != ActivityInstanceModel.ID])
                values = ",".join(["%("+key+")s" for key in activityModel if key != ActivityInstanceModel.ID])
                values = cursor.mogrify(values, activityModel)

                cursor.execute("""INSERT INTO common_activityinstance (""" + query + """) VALUES (""" + values + """)"""
                               """ RETURNING id""")

                return cursor.fetchone()[0]
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.activity_instance.load_activity_for_lecturer.time')
    def loadActivityForLecturer(self, activityId):
        """

        :param activityId:
        :return:
        """
        error = None
        try:
            with self.connection.cursor() as cursor:
                # load the activity model
                cursor.execute("""SELECT * FROM common_activityinstance WHERE id=%s""", (activityId,))
                if cursor.rowcount <= 0:
                    raise BaseDao.NotFound

                activity = self.fetchOneAsDict(cursor)

                # load the setting model
                cursor.execute("""SELECT a.* FROM common_activitysetting as a INNER JOIN common_activityinstance_settings"""
                               """ AS b ON a.id=b.activitysetting_id WHERE b.activityinstance_id=%s""", (activityId,))
                if cursor.rowcount == 0:
                    activity["settings"] = list()
                else:
                    activity["settings"] = self.fetchManyAsDict(cursor)

                # load responses
                cursor.execute("""SELECT * FROM student_responses WHERE activity_instance_id=%s""",
                               (activityId,))

                if cursor.rowcount != 0:
                    activity["responses"] = self.fetchManyAsDict(cursor)
                else:
                    activity["responses"] = list()

                # load student names
                cursor.execute("""SELECT * from students_studentname WHERE activity_instance_id=%s""", (activityId, ))

                if cursor.rowcount != 0:
                    activity["student_names"] = list()
                else:
                    activity["student_names"] = self.fetchManyAsDict(cursor)

                return activity
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            error = 1
            logger.error(exceptionStack(e))
            self.connection.rollback()
            raise
        finally:
            if error is None:
                self.connection.commit()
            elif error == 1:
                self.connection.rollback()

    @retry
    @statsd.timed('socrative.dao.activity_instance.get_activity_owners_during_period.time')
    def getActivityOwnersDuringPeriod(self, start_date, end_date):
        """

        :param start_date:
        :param end_date:
        :return:
        """
        if not start_date:
            return dict()

        try:
            with self.readOnlyConnection.cursor() as cursor:
                query = cursor.mogrify("""SELECT started_by_id, GREATEST(start_time, end_time) as last_login FROM """
                                       """common_activityinstance WHERE (start_time >= %s OR end_time >= %s)""",
                                       (start_date, start_date))
                if end_date:
                    query += cursor.mogrify(""" AND (end_time <%s or (end_time is NULL and start_time <%s))""",
                                            (end_date, end_date))

                query = cursor.mogrify("SELECT started_by_id, MAX(last_login) FROM (" + query + ") AS subquery GROUP BY started_by_id")
                cursor.execute(query)

                return dict((row[0], row[1]) for row in cursor.fetchall())

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise