# -*- coding: utf-8 -*-
import os
import sys
import inspect
import datetime
import logging
from logging.config import fileConfig
import platform
import ujson
from django.utils.safestring import SafeText
from lxml import html as htmlParser
from lxml.etree import ParserError
from django.core.validators import validate_email, ValidationError
from common import soc_constants

import unicodedata
import re

all_chars = (unichr(i) for i in xrange(0x10000))
control_chars = ''.join(c for c in all_chars if unicodedata.category(c) == 'Cc')
# or equivalently and much more efficiently
control_chars1 = ''.join(map(unichr, range(0, 32) + range(127, 160)))

control_char_re = re.compile('[%s]' % re.escape(control_chars1))

logger = logging.getLogger(__name__)


def __safeExceptionToString(exception):
        """
        Safe exception to str
        :param exception: Exception
        :return: str
        """

        try:
            return str(exception)
        except UnicodeEncodeError:
            # Mantis 1825
            return str(repr(unicode(exception)))
        except Exception as e:
            logger.warn("Exception, ex=%s", e)


def exceptionStack(exception, maxLevel=10, maxPathLevel=5):
    """
    ..  py:function:: exceptionStack(e, maxLevel=10, maxPathLevel=5)

    Exception to string.
    *static* : ENABLE_EXCEPTION_LOGGER
    *. Control if the exception logger is enable or not.
    *. If enabled, exception are logged as error logs, in a dedicated logger.

    :param exception: The exception.
    :param maxLevel: Max call stack level (default 10)
    :param maxPathLevel: Max path level (default 5)
    :return: A string representing the exception.

    """
    # Path separator
    pathSep = os.sep

    # Go
    listFrame = None
    try:
        myStr = "\n"

        # Class type
        myStr += "e.class:["
        myStr += exception.__class__.__name__
        myStr += "]"

        # To string
        myStr += " e.str:["
        myStr += __safeExceptionToString(exception)
        myStr += "]"

        # Traceback
        listFrame = inspect.trace()
        if listFrame is None:
            myStr += "\n e.cs=[none]"
        else:
            myStr += "\n e.cs=["

            # List
            listFrame.reverse()

            # Fix first line
            firstLineNo = listFrame[0][2]

            # Enumerate f_back from index 0 only
            listFrameTemp = list()
            frameCur = listFrame[0][0]
            while frameCur is not None:
                # Create TUPLE
                # 0 : Nothing
                # 1 : File
                # 2 : Line
                # 3 : Method

                # Alloc and append (with fix for first frame)
                if firstLineNo == -1:
                    tempTuple = ("", frameCur.f_code.co_filename, frameCur.f_lineno, frameCur.f_code.co_name)
                else:
                    tempTuple = ("", frameCur.f_code.co_filename, firstLineNo, frameCur.f_code.co_name)
                    firstLineNo = -1

                # Add
                listFrameTemp.append(tempTuple)

                # Next
                frameCur = frameCur.f_back

            # Re-assign
            listFrame = listFrameTemp

            # Browse
            curIdx = 0
            for curFrame in listFrame:
                curLine = str(curFrame[2])
                curFile = curFrame[1]
                curMethod = curFrame[3]

                # Handle max path level
                arToken = curFile.rsplit(pathSep, maxPathLevel)
                if len(arToken) > maxPathLevel:
                    # Remove head
                    arToken.pop(0)
                    # Join
                    curFile = "..." + pathSep.join(arToken)

                # Format
                localStr = ""
                localStr += "in:" + curMethod
                localStr += "#" + curFile + "@" + curLine
                localStr += "\n"
                myStr += localStr

                curIdx += 1
                if curIdx >= maxLevel:
                    myStr += "..."
                    break

            # Close
            myStr += "]"

        # Ok
        return myStr
    finally:
        if listFrame is not None:
            del listFrame


def initLoggerFromFile(confFileName, forceReinit=False):
    """
    Init or re-init loggers from file (fileConfig format). Raise exception upon error.
    :param confFileName Configuration file name (key/value)
    :param forceReinit: For test only.
    :return Nothing.
    """

    try:
        logger.debug("fileConfig : entering, confFileName=%s", confFileName)
        fileConfig(confFileName, None, True)
        logger.warn("fileConfig : done")
    except Exception as e:
        logger.error("initLoggerFromFile failed, ex=%s", exceptionStack(e))
        raise


def initDefaultLogger(rsyslogger, myLevel="INFO", forceReinit=False, debugCallback=None):
        """
        Init default logger.
        :param rsyslogger:
        :param myLevel Log level ("DEBUG", "INFO", "WARN" or "ERROR")
        :param forceReinit: For test only.
        :param debugCallback: For unit test
        :return Nothing.
        """

        # Default
        logging.basicConfig(level=myLevel)

        # Console handler
        console = logging.StreamHandler(sys.stdout)
        console.setLevel(logging.getLevelName(myLevel))

        # Rsyslog handler
        # Need to import run time because of cyclic dependencies

        try:
            rsyslog = rsyslogger(debugCallback=debugCallback)
            rsyslog.setLevel(logging.getLevelName(myLevel))
        except Exception as e:
            logger.error("Unable to import SocrativeSyslogHandler, ex=%s", exceptionStack(e))
            rsyslog = None

        # Formatter
        formatter = logging.Formatter(
            "%(asctime)s | %(levelname)s | %(module)s@%(funcName)s@%(lineno)d | %(message)s | %(thread)d:%(threadName)s | %(process)d:%(processName)s")
        console.setFormatter(formatter)
        if rsyslog:
            rsyslog.setFormatter(formatter)

        # Add the handler to the root logger with full reinit
        root = logging.getLogger()
        root.setLevel(logging.getLevelName(myLevel))
        root.handlers = []
        root.addHandler(console)
        if rsyslog:
            root.addHandler(rsyslog)
        logger.info("Logger initialized (console/rsyslog), myLevel=%s", myLevel)


def isString(obj):
    """

    :param obj:
    :return:
    """
    if type(obj) in (str, unicode):
        return True
    else:
        return False

def isInteger(obj):
    """

    :param obj:
    :return:
    """
    if type(obj) in (int, long):
        return True
    else:
        return False


def isStringNotEmpty(obj):
    """
    :param obj:
    :return:
    """
    if type(obj) in (str, unicode) and obj != "":
        return True
    else:
        return False

def getMachineName():
    """
    return the name of the machine the code is running on
    :return:
    """
    return platform.uname()[1].upper()


def safeToInt(obj):
    """

    :param obj:
    :return:
    """

    try:
        if obj is None:
            return None
        return int(obj)
    except Exception as e:
        logger.debug(exceptionStack(e))
        return None


def safeToBool(obj):
    """

    :param obj:
    :return:
    """
    try:
        if obj is None:
            return None
        else:
            return ujson.loads(str(obj).lower()) is True
    except Exception as e:
        logger.debug(exceptionStack(e))
        return None


def safeToFloat(obj):
    """

    :param obj:
    :return:
    """
    try:
        return float(obj)
    except Exception as e:
        logger.error(exceptionStack(e))
        return None


def safeToDate(obj):
    """
    """
    try:
        if type(obj) is datetime.date:
            return obj
        elif type(obj) in (str, unicode):
            return datetime.datetime.strptime(obj,'%Y-%m-%d').date()

        return None
    except Exception as e:
        logger.error(exceptionStack(e))
        return None


def safeToDateTime(obj):
    """

    :param obj:
    :return:
    """
    try:
        if type(obj) is datetime:
            return obj
        elif type(obj) in (int, long):
            return datetime.datetime.fromtimestamp(obj)
        return None
    except Exception as e:
        logger.error(exceptionStack(e))
        return None


def safeToString(obj):
    try:
        return str(obj)
    except UnicodeEncodeError:
        return unicode(obj).encode('utf-8')
    except Exception as e:
        logger.error(exceptionStack(e))
        return None


def isNotNone(obj):
    """
    :param obj:
    :return:
    """
    return obj is not None


def isBool(obj):
    return type(obj) is bool


def isBoolOrNone(obj):
    return type(obj) is bool or obj is None


def serializerToBool(obj):
    return safeToBool(obj), True


def remove_control_chars(s):
    """
    removes control chars from a string using regex
    :param s: the string to be modified
    :return:
    """
    return control_char_re.sub('', s)

def byteLen(s):
    if type(s) in (unicode, SafeText):
        return len(s.encode('utf-8'))
    else:
        return len(s)

def htmlUnescape(text):
    """
    :param text:
    :return:
    """
    try:
        txt = htmlParser.fromstring(text.strip()).text_content() if text.strip() != "" else ""
    except ParserError:
        txt = text

    return txt

def htmlEscape(text):
    """

    :param text:
    :return:
    """

    if text != '':
        text = text.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        text = text.replace("\t", "&emsp;").replace("\"","&quot;").replace("'", "&#x27;")

    return text

ROOM_NAME_REGEX = re.compile(r'^[a-z0-9]{1,32}$')
S3_STORAGE_URL_REGEX = re.compile(r'^https://(uploads|(andrei|bogdan|catalin|rob|matt|appstaging)-assets).socrative.com/.{1,128}$')


def room_name_validator(room_name):
    """
    length lower than 32 chars, no spaces no
    :param room_name:
    :return:
    """
    if room_name != "":
        return ROOM_NAME_REGEX.match(room_name) is not None
    else:
        return False


def getNextYear():
    """
    returns the date object which is one year from now
    """
    obj = datetime.datetime.today()
    obj = obj.replace(year=obj.year+1)

    obj = obj.date()

    limitDate = datetime.date(2017, 8, 1)
    if obj < limitDate:
        return limitDate
    return obj


def isSocrativeStorageUrl(url):
    """
    checks if the url is from our s3 storage
    :param url:
    :return:
    """

    if S3_STORAGE_URL_REGEX.match(url):
        return True

    return False


def gteZero(obj):
    """
    returns True if the obj is a positive number or a default -1
    :param nb:
    :return:
    """

    if type(obj) in (int, long):
        return obj >= 0 or obj == -1
    else:
        return False


def convert(strObj):
    if type(strObj) is str:
        return str.decode('utf-8')
    elif type(strObj) is unicode:
        return strObj
    else:
        logger.error("Weird type string %s" % str(type(strObj)))
        return strObj


def isEmailValid(email):
    """
    checks if the email field is valid
    :param email:
    :return:
    """
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False
    except Exception as e:
        logger.error(exceptionStack(e))
        return False


def roster_order_validator(obj):
    """

    :param obj:
    :return:
    """

    if obj not in (soc_constants.ROSTER_FIRST_LAST_ID, soc_constants.ROSTER_LAST_FIRST_ID):
        return False

    return True


def roster_type_validator(obj):
    """

    :param obj:
    :return:
    """

    if obj not in (soc_constants.ROSTER_FILE_CSV, soc_constants.ROSTER_FILE_XLSX):
        return False

    return True
