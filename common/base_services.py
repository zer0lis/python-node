# -*- coding: utf-8 -*-
from common.spring_container import SpringItem


class BaseService(SpringItem):
    """
    Base class for services
    """

    def __init__(self, daoRegistry, cacheRegistry):
        """
        Constructor
        """

        super(BaseService, self).__init__()

        self.daoRegistry = daoRegistry
        self.serviceRegistry = None
        self.cacheRegistry = cacheRegistry

    def destroy(self):
        """
        nothing to do here
        """
        pass


class ServiceRegistry(SpringItem):
    """
    Registry of service classes
    """

    def __init__(self):
        """

        :return:
        """
        super(ServiceRegistry, self).__init__()

        self.__studentServices = None
        self.__quizServices = None
        self.__usersServices = None
        self.__commonServices = None
        self.__roomServices = None
        self.__lecturerServices = None
        self.__tornadoServices = None

    def getStudentServices(self):
        """
        Getter
        :return:
        """
        return self.__studentServices

    def setStudentServices(self, studentService):
        """
        Setter
        :param studentService:
        :return:
        """

        self.__studentServices = studentService
        self.__studentServices.serviceRegistry = self

    def getQuizServices(self):
        """
        Getter
        :return:
        """
        return self.__quizServices

    def setQuizServices(self, quizService):
        """
        Setter
        :param quizService:
        :return:
        """

        self.__quizServices = quizService
        self.__quizServices.serviceRegistry = self

    def getUsersServices(self):
        """
        Getter
        :return:
        """
        return self.__usersServices

    def setUsersServices(self, usersService):
        """
        Setter
        :param usersService:
        :return:
        """

        self.__usersServices = usersService
        self.__usersServices.serviceRegistry = self

    def getCommonServices(self):
        """
        Getter
        :return:
        """
        return self.__commonServices

    def setCommonServices(self, commonService):
        """
        Setter
        :param commonService:
        :return:
        """

        self.__commonServices = commonService
        self.__commonServices.serviceRegistry = self

    def getRoomServices(self):
        """
        Getter
        :return:
        """
        return self.__roomServices

    def setRoomServices(self, roomService):
        """
        Setter
        :param roomService:
        :return:
        """

        self.__roomServices = roomService
        self.__roomServices.serviceRegistry = self

    def getLecturerServices(self):
        """
        Getter
        :return:
        """
        return self.__lecturerServices

    def setLecturerServices(self, lecturerServices):
        """
        Setter
        :param lecturerServices:
        :return:
        """

        self.__lecturerServices = lecturerServices
        self.__lecturerServices.serviceRegistry = self

    def getTornadoServices(self):
        """
        Getter
        :return:
        """
        return self.__tornadoServices

    def setTornadoServices(self, tornadoService):
        """
        Setter
        :param tornadoService:
        :return:
        """

        self.__tornadoServices = tornadoService
        self.__tornadoServices.serviceRegistry = self

    def destroy(self):
        """
        called by spring python when the server stops
        :return:
        """
        #do nothing, the services will be destroyed separately
        pass

    #==============================
    # PROPERTIES
    #==============================
    studentService = property(getStudentServices, setStudentServices)
    quizService = property(getQuizServices, setQuizServices)
    userService = property(getUsersServices, setUsersServices)
    commonService = property(getCommonServices, setCommonServices)
    roomService = property(getRoomServices, setRoomServices)
    lecturerService = property(getLecturerServices, setLecturerServices)
    tornadoService = property(getTornadoServices, setTornadoServices)
