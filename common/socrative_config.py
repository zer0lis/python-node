# -*- coding: utf-8 -*-
"""
    Code.
"""
import logging
from common.spring_container import SpringItem

logger = logging.getLogger(__name__)


class ConfigRegistrySpringInitializer(SpringItem):
    """
    ConfigRegistryInitializer

    """

    def __init__(self, **kwargs):
        """
        Populates the ConfigRegistry with kwargs.
        :return Nothing
        """

        super(ConfigRegistrySpringInitializer, self).__init__()

        configDict = dict()

        for k in kwargs:
            configDict[k] = kwargs[k]

        logger.info("Attempting to populate ConfigRegistry")

        ConfigRegistry.populate(configDict)

    def destroy(self):
        """
        Required for Spring

        :return Nothing
        """
        pass


class ConfigRegistry(object):
    """
    This class hodls configuration fields that are required across the Business.
    It is initialized via by Spring using ConfigRegistrySpringInitializer.

    The config items are populated statically inside the class in an internal dict,
    using ConfigRegistry.populate().
    This method should be called only once upon ConfigRegistry initialization.

    The ConfigRegistry is not thread safe.

    """

    # Internal dict that holds configuration items
    __configDict = dict()

    @classmethod
    def populate(cls, cfgDict):
        """
        Populates internal config dict with params received in **kwargs.
        :param cls: Class
        :param cfgDict: Input config items :type dict
        :return:
        """
        if cls.__configDict is not None and len(cls.__configDict) > 0:
            #raise Exception("ConfigRegistry is already initialized!")
            logger.warn("ConfigRegistry was reinitialized!")

        if cfgDict is None:
            logger.error("Cannot initialize ConfigRegistry! None type given on populate()!")

        logger.info("Initialized ConfigRegistry with %s items : SUCCESS", len(cfgDict))

        cls.__configDict = cfgDict

    @classmethod
    def getItem(cls, name):
        """
        Returns a configuration item.
        :param cls: Class
        :param name: The config item name (key).
        :type name: str
        :return:
        """

        try:
            return cls.__configDict[name]
        except KeyError:
            logger.error("ConfigRegistry did not find key=%s", name)
            return None
