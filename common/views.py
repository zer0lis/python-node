# -*- coding: utf-8 -*-
from common.socrative_api import exceptionStack

import ujson

from base_view import BaseView
from common.socrative_errors import BaseError
from common import http_status as status
from common.serializers import GetReportListDTORequest, GetReportDTORequest, UpdateReportDTORequest
from common.serializers import TrashReportDTORequest, UpdateReportListDTORequest, PurgeReportsDTORequest

import logging

logger = logging.getLogger(__name__)

from common.counters import CommonCounters, DatadogThreadStats

statsd = DatadogThreadStats.STATS

class ReportListAPIView(BaseView):
    """
    retrieve reports api
    """

    http_method_names = ["get", "put"]

    @statsd.timed(CommonCounters.GET_REPORT_LIST)
    def get(self, request, *args, **kwargs):
        """
        handler for HTTP GET
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = CommonCounters.GET_REPORT_LIST_FAIL

        dataDict = dict()
        dataDict.update(kwargs)
        for k in request.GET:
            dataDict[k] = request.GET.get(k)

        self.getAuthToken(dataDict)

        dto = GetReportListDTORequest.fromDict(dataDict)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.commonService.getReportList(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)

    @statsd.timed(CommonCounters.UPDATE_REPORT_LIST_STATES)
    def put(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = CommonCounters.UPDATE_REPORT_LIST_STATES_FAIL

        dataDict = dict()
        dataDict.update(kwargs)
        self.getAuthToken(dataDict)

        try:
            dataDict.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST, statusCode=400, failCounter=failCounter)

        dto = UpdateReportListDTORequest.fromDict(dataDict)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.commonService.updateReportList(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class PurgeReportsAPIView(BaseView):
    """
    deletes from the database the activity reports
    """

    http_method_names = ["post"]

    @statsd.timed(CommonCounters.PURGE_REPORTS)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        dataDict = dict()

        failCounter = CommonCounters.PURGE_REPORTS_FAIL

        try:
            dataDict.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST, statusCode=400, failCounter=failCounter)

        self.getAuthToken(dataDict)

        dto = PurgeReportsDTORequest.fromDict(dataDict)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.commonService.purgeReports(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class TeacherReportAPIView(BaseView):
    """
    will have to document it
    """

    http_method_names = ["get", "put", "delete"]

    @statsd.timed(CommonCounters.GET_ACTIVITY_DATA)
    def get(self, request, *args, **kwargs):
        """
        handler for HTTP POST

        :return:
        """

        dataDict = dict()
        dataDict.update(kwargs)
        for k in request.POST:
            dataDict[k] = request.POST.get(k)

        self.getAuthToken(dataDict)

        failCounter = CommonCounters.GET_ACTIVITY_DATA_FAIL

        dto = GetReportDTORequest.fromDict(dataDict)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.commonService.getActivityById(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)

    @statsd.timed(CommonCounters.UPDATE_ACTIVITY)
    def put(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        dataDict = dict()
        dataDict.update(kwargs)
        self.getAuthToken(dataDict)

        failCounter = CommonCounters.UPDATE_ACTIVITY_FAIL

        try:
            dataDict.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST, statusCode=400, failCounter=failCounter)

        dto = UpdateReportDTORequest.fromDict(dataDict)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], dto[1], failCounter=failCounter)

        resp = request.commonService.updateReport(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)

    @statsd.timed(CommonCounters.DELETE_REPORT)
    def delete(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        dataDict = dict()
        dataDict.update(kwargs)
        self.getAuthToken(dataDict)

        failCounter = CommonCounters.DELETE_REPORT_FAIL

        dto = TrashReportDTORequest.fromDict(dataDict)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], dto[1], failCounter=failCounter)

        # same call as update, only the state changes to trash
        setattr(dto, "state", "trashed")

        resp = request.commonService.updateReport(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class S3ImageDataView(BaseView):
    """
    class that generates the necessary data for uploading images to S3
    """
    http_method_names = ["get"]

    @statsd.timed(CommonCounters.GET_S3_UPLOAD_URL)
    def get(self, request, *args, **kwargs):
        """
        handle HTTP GET for S3 image data
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        failCounter = CommonCounters.GET_S3_UPLOAD_URL_FAIL

        data = dict()
        for k in request.GET:
            data[k] = request.GET.get(k)

        response = request.commonService.getS3ImageData(request, data)

        return self.render_json_response(response,
                                         statusCode=status.HTTP_200_OK,
                                         failCounter=failCounter)


class PingView(BaseView):
    """
    view to test connection
    """

    http_method_names = ["get"]

    @statsd.timed(CommonCounters.PING)
    def get(self, request, *args, **kwargs):
        """
        handler for GET
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        return self.render_json_response("OK", statusCode=200)


class JoinRoomByCodeView(BaseView):
    """
    class that will redirect to a join room url
    """

    http_method_names = ["get"]

    @statsd.timed(CommonCounters.JOIN_ROOM_BY_CODE)
    def get(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        data = dict()
        data.update(kwargs)

        url, cookie = request.commonService.joinRoomByCode(request, data)

        return self.render_redirect_response(url, cookie=cookie)
