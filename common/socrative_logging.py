# -*- coding: utf-8 -*-

from logging.handlers import SysLogHandler
import socket
from common.socrative_api import getMachineName, exceptionStack, initLoggerFromFile, safeToInt

import hashlib

import logging
from threading import RLock, Timer
import os
from common.spring_container import SpringItem

logger = logging.getLogger(__name__)


class SocrativeSyslogHandler(SysLogHandler):
    """
    A handler class which allows the cursor to stay on
    one line for selected messages
    """

    # Tag
    __EXTRA_SOCRATIVE_TRAFFIC_TAG = 10

    # TRAFFIC LOG Compo name suffix
    _SOCRATIVE_TRAFFIC_LOG_SUFFIX = ".traffic"

    # Log is a traffic log (provide as extra=EXTRA_TRAFFIC in logger.debug and related methods)
    EXTRA_TRAFFIC = {__EXTRA_SOCRATIVE_TRAFFIC_TAG: __EXTRA_SOCRATIVE_TRAFFIC_TAG}

    def __init__(self, address="/dev/log",
                 facility=SysLogHandler.LOG_LOCAL1, socktype=socket.SOCK_DGRAM, debugCallback=None,
                 componentName="Socrative"):
        """
        Init
        :param address: tuple ('ip', port) or string "target"
        :type address:  str, tuple
        :param facility: Facility
        :type facility: int
        :param socktype: Socket type
        :type socktype: int
        """

        # Base
        if type(address) in (str,unicode) and os.path.exists(address) is False:
            address = "/var/run/syslog"
        SysLogHandler.__init__(self, address, facility, socktype)

        self.componentName = componentName

        # Callback
        self.__debugCallback = debugCallback

    def debugNotify(self, msg, isTraffic):
        """
        Debug
        :param msg: Message.
        :type msg: unicode,str
        :param isTraffic: Extra value, true if traffic log
        :type isTraffic: bool
        """
        if self.__debugCallback is None:
            return

        try:
            self.__debugCallback(msg, isTraffic)
        except:
            pass

    def emit(self, record):
        """
        Emit a record.

        The record is formatted, and then sent to the syslog server. If
        exception information is present, it is NOT sent to the server.
        :param record: The record to log
        :type record: logging.LogRecord
        """

        # Write
        try:
            # Format
            msg = self.format(record) + '\000'
            cn = self.componentName or "Socrative"

            # Check the extra
            if record.__dict__.get(SocrativeSyslogHandler.__EXTRA_SOCRATIVE_TRAFFIC_TAG) == SocrativeSyslogHandler.__EXTRA_SOCRATIVE_TRAFFIC_TAG:
                # Traffic log
                isTraffic = True
                cn += SocrativeSyslogHandler._SOCRATIVE_TRAFFIC_LOG_SUFFIX
            else:
                isTraffic = False

            # We append the machine+component name at the beginning
            if type(msg) is unicode:
                msg = u"{0} | {1} | {2}".format(getMachineName(), cn, msg)
            else:
                msg = "{0} | {1} | {2}".format(getMachineName(), cn, msg)

            # Message is a string. Convert to bytes as required by RFC 5424
            if type(msg) is unicode:
                msg = msg.encode('utf-8')

            # Add priority + facility (int)
            prio = '<%d>' % self.encodePriority(self.facility, self.mapPriority(record.levelname))
            msg = prio + cn + ": " + msg

            # Format :
            # <PRIORITY>IDENT: Message"

            # Notify
            if not self.__debugCallback is None:
                self.debugNotify(msg, isTraffic)

            if self.unixsocket:
                try:
                    self.socket.send(msg)
                except socket.error:
                    self._connect_unixsocket(self.address)
                    self.socket.send(msg)
            elif self.socktype == socket.SOCK_DGRAM:
                self.socket.sendto(msg, self.address)
            else:
                self.socket.sendall(msg)
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)


class LoggingSpringInitializer(SpringItem):
    """
    This call will handle logger configuration reload.
    """

    def __init__(self, configFileName, checkIntervalMs=1000, blockSize=65536, onReloadCallback=None,
                 onNothingCallback=None):
        """
        Initializing constructor.
        :param configFileName: The configuration file name.
        :param checkIntervalMs: The file check interval in ms.
        :param blockSize: Block size in bytes while reading file.
        :param onReloadCallback: Callback called if file has been reloaded, called as "callback(fileName)"
        :param onReloadCallback: Callback called if file has NOT been reloaded, called as "callback(fileName)"
        """

        super(LoggingSpringInitializer, self).__init__()

        configFileName = os.path.dirname(os.path.abspath(__file__)) + os.sep + configFileName

        logger.info("Entering, id=%s", id(self))
        logger.info("Entering, configFileName=%s", configFileName)
        logger.info("Entering, checkIntervalMs=%s", checkIntervalMs)
        logger.info("Entering, blockSize=%s", blockSize)
        logger.info("Entering, onReloadCallback=%s", onReloadCallback)
        logger.info("Entering, onNothingCallback=%s", onNothingCallback)

        # Casting as required
        self._checkIntervalMs = safeToInt(checkIntervalMs)

        # Casting as required
        self._blockSize = safeToInt(blockSize)

        # Store
        self._configFileName = configFileName
        self._onReloadCallback = onReloadCallback
        self._onNothingCallback = onNothingCallback

        # Check
        if os.path.exists(self._configFileName) is False:
            logger.warn("File not found or not accessible, logger watchers may be useless, _configFileName=%s, curDir=%s",
                        self._configFileName,
                        os.getcwd())

        # Lock
        self._locker = RLock()

        # Previous checksum
        self._prevChecksum = None
        self._prevChecksumPrintable = None

        # Running
        self._isStarted = False

        # # Greenlet
        self._runGreenlet = None

        # Start now
        self._startWatch()

    def destroy(self):
        """
        For spring python.
        :return: Nothing.
        """

        logger.info("Entering now")
        self._stopWatch()

    def _startWatch(self):
        """
        Start watching
        :return: Nothing
        """
        with self._locker:
            try:
                logger.info("Starting now")

                # Check
                if self._isStarted is True:
                    logger.warn("Already started, doing nothing")
                    return

                # Go
                self._isStarted = True

                # Run
                self._checkNow()

                # Done
                logger.info("Started")
            except Exception as e:
                logger.warn("Exception, ex=%s", exceptionStack(e))
                raise

    def _stopWatch(self):
        """
        Stop watching.
        :return: Nothing
        """
        with self._locker:
            try:
                logger.info("Stopping now")

                # Check
                if self._isStarted is False:
                    logger.warn("Already stopped, doing nothing")
                    return

                # Signal
                self._isStarted = False

                # Stop
                if not self._runGreenlet is None:
                    logger.info("Stopping greenlet")
                    # self._runGreenlet.kill()
                    self._runGreenlet.cancel()
                    self._runGreenlet.join(timeout=1)
                    self._runGreenlet = None
                    logger.info("Stopping greenlet : done")

                # Done
                logger.info("Stopped")
            except Exception as e:
                logger.warn("Exception, ex=%s", exceptionStack(e))
            finally:
                # Reset
                self._isStarted = False

    def _scheduleNextCheck(self):
        """
        Schedule next check
        :return: Nothing
        """

        with self._locker:
            # Check
            if self._isStarted is False:
                return

            # Schedule
            # self._runGreenlet = gevent.spawn_later(self._checkIntervalMs * 0.001, self._checkNow)
            if self._runGreenlet is None:
                self._runGreenlet = Timer(self._checkIntervalMs * 0.001, self._checkNow)
                self._runGreenlet.setDaemon(True)
                self._runGreenlet.start() # fixes rechecking logging configuration
                logger.info("started Timer")

    def __hashfile(self):
        """
        Hash a file
        :return: A list (2 items : first is the digest binary, second if the digest printable)
        """

        f = None
        try:
            # Open
            f = open(self._configFileName, "r")
            # Alloc
            hasher = hashlib.md5()
            # Read / Hash
            buf = f.read(self._blockSize)
            while len(buf) > 0:
                hasher.update(buf)
                buf = f.read(self._blockSize)
                # Done
            return [hasher.digest(), hasher.hexdigest()]
        except Exception as e:
            logger.debug("Exception, ex=%s", e)
            return 0
        finally:
            if not f is None:
                f.close()

    def _checkNow(self):
        """
        Check now and reschedule if required.
        :return: Nothing.
        """
        with self._locker:
            try:
                # Check
                if self._isStarted is False:
                    logger.info("logging thread not started")
                    return
                elif os.path.exists(self._configFileName) is False:
                    logger.warn("file %s is missing" % self._configFileName)
                    return

                # Get the checksum of the file
                checkSum, checkSumPrintable = self.__hashfile()

                logger.info("PrevChkSum is None: %s, chkSum %s" % (str(self._prevChecksum is None), checkSumPrintable))

                # Check
                if self._prevChecksum is None:
                    # First time : Store
                    self._prevChecksum = checkSum
                    self._prevChecksumPrintable = checkSumPrintable

                    # Reload
                    initLoggerFromFile(self._configFileName, True)

                    # Callback
                    if not self._onReloadCallback is None:
                        self._onReloadCallback(self._configFileName)
                elif self._prevChecksum != checkSum:
                    # Changes : Store
                    self._prevChecksum = checkSum
                    self._prevChecksumPrintable = checkSumPrintable

                    # Reload
                    initLoggerFromFile(self._configFileName, True)

                    # Callback
                    if not self._onReloadCallback is None:
                        self._onReloadCallback(self._configFileName)
                else:
                    # Nothing
                    if not self._onNothingCallback is None:
                        self._onNothingCallback(self._configFileName)
            except Exception as e:
                logger.warn("Exception, ex=%s", exceptionStack(e))
            finally:
                # Reschedule
                self._scheduleNextCheck()