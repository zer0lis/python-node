# -*- coding: utf-8 -*-

from common.spring_container import SpringItem
from socrative_api import safeToInt, safeToBool, isStringNotEmpty


class PostgreConnectionContext(SpringItem):
    """
    class to hold the configuration context for connections to the postgre db
    """

    def __init__(self, database, user, password, host, port, async, autocommit, readOnly):
        """
        constructor for the postgre connection context
        """

        super(PostgreConnectionContext, self).__init__()

        self.database = database
        self.user = user
        self.password = password
        self.host = host
        self.port = safeToInt(port)
        self.async = safeToBool(async)
        self.autocommit = safeToBool(autocommit)
        self.readOnly = safeToBool(readOnly)

        #call to super
        super(PostgreConnectionContext, self).__init__()

        if not isStringNotEmpty(self.database):
            raise ValueError("database is not a valid string, now it's %s" % str(type(database)))

        if not isStringNotEmpty(self.user):
            raise ValueError("user is not a valid string, now it's %s" % str(type(user)))

        if not isStringNotEmpty(self.password):
            raise ValueError("password is not a valid string, now it's %s" % str(type(password)))

        if self.port is None:
            raise ValueError("port is not a valid string representation of an integer, now it's %s" % str(type(port)))

        if self.async is None:
            raise ValueError("async is not a valid string for a bool, now it's %s" % str(type(async)))

        if self.autocommit is None:
            raise ValueError("autocommit is not a valid string for a bool, now it's %s" % str(type(autocommit)))

        if self.readOnly is None:
            raise ValueError("readOnly is not a valid string for a bool, now it's %s" % str(type(readOnly)))

    def destroy(self):
        """
        method that DisposableObject calls when deleting the objects
        """
        # nothing to force to close or delete here
        pass