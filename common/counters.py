# coding=utf-8
from datadog import ThreadStats

class DatadogThreadStats():
    """
    wrapper
    """
    STATS = ThreadStats()
    STATS.start()

class HttpCounters(object):
    """
    class to hold http counter names
    """

    INTERNAL_SERVER_ERROR = 'socrative.views.requests.internal_server_error'
    BAD_REQUESTS = 'socrative.views.requests.bad_requests'
    REDIRECTED_REQUESTS = 'socrative.views.requests.redirected'
    TOTAL_REQUESTS = 'socrative.views.requests.total'
    STREAMING_REQUESTS = 'socrative.views.requests.streaming'


class RedisCounters(object):
    """
    class to store the name of the Redis counters
    """
    SET_ONE = 'socrative.redis.set_one.time'
    SET_MULTI = 'socrative.redis.set_multi.time'
    DELETE_ONE = 'socrative.redis.delete_one.time'
    DELETE_MULTI = 'socrative.redis.delete_multi.time'
    GET_ONE_CACHE_HIT = 'socrative.redis.get_one.cache_hit.time'
    GET_ONE_CACHE_MISS = 'socrative.redis.get_one.cache_miss.time'
    GET_MULTI_CACHE_MISS = 'socrative.redis.get_multi.cache_miss.time'
    GET_MULTI_CACHE_HIT = 'socrative.redis.get_multi.cache_hit.time'
    ADD_TO_SET = 'socrative.redis.add_to_set.time'
    SISMEMBER = 'socrative.redis.sismember.time'
    INCREMENT = 'socrative.redis.increment.time'
    SCARD = 'socrative.redis.scard.time'


class EmailCounters(object):
    """
    class to handle counter names for email
    """

    CONNECTIONS = 'socrative.email.connections'
    EMAIL_SENT_OK = 'socrative.email.sent.ok'
    EMAIL_SENT_FAIL = 'socrative.email.sent.fail'
    EMAIL_RETRIES = 'socrative.email.retries'
    EMAIL_CONNECTION_FAILED = 'socrative.email.connections.failed'
    EMAILS_SENT = 'socrative.email.sent.time'


class CommonCounters(object):
    """

    """

    JOIN_ROOM_BY_CODE = 'socrative.views.common.join_room_by_code.time'
    GET_REPORT_LIST = 'socrative.views.common.get_report_list.time'
    GET_REPORT_LIST_FAIL = 'socrative.views.common.get_report_list.fail'
    UPDATE_REPORT_LIST_STATES = 'socrative.views.common.update_report_list_states.time'
    UPDATE_REPORT_LIST_STATES_FAIL = 'socrative.views.common.update_report_list_states.fail'
    PURGE_REPORTS = 'socrative.views.common.purge_reports.time'
    PURGE_REPORTS_FAIL = 'socrative.views.common.purge_reports.fail'
    GET_ACTIVITY_DATA = 'socrative.views.common.get_activity_data.time'
    GET_ACTIVITY_DATA_FAIL = 'socrative.views.common.get_activity_data.fail'
    UPDATE_ACTIVITY = 'socrative.views.common.update_activity_state.time'
    UPDATE_ACTIVITY_FAIL = 'socrative.views.common.update_activity_state.fail'
    DELETE_REPORT = 'socrative.views.common.delete_report.time'
    DELETE_REPORT_FAIL = 'socrative.views.common.delete_report.fail'
    GET_S3_UPLOAD_URL = 'socrative.views.common.get_s3_upload_url.time'
    GET_S3_UPLOAD_URL_FAIL = 'socrative.views.common.get_s3_upload_url.fail'
    PING = 'socrative.views.common.ping.time'

