# -*- coding: utf-8 -*-
from common.spring_container import SpringItem
from common.socrative_api import exceptionStack
import psycopg2
import logging
import os

logger = logging.getLogger(__name__)


class PostgreConnectionManager(SpringItem):
    """
    will return the connection for the right table
    """

    def __init__(self):
        """
        constructor
        """

        #  call the constructor from the disposable object
        super(PostgreConnectionManager, self).__init__()

        self.__idleConnections = list()
        self.__idleROConnections = list()
        self.__busyROConnections = dict()
        self.__busyConnections = dict()


    def getConnection(self, connectionContext):
        """
        adds a connection or returns an existing connection
        """

        pid = os.getpid()

        if connectionContext.readOnly is True:
            pool = self.__busyROConnections
            idlePool = self.__idleROConnections
        else:
            pool = self.__busyConnections
            idlePool = self.__idleConnections

        if pid in pool:
            conn = pool[pid]
            if conn.closed == 0:
                return conn
        elif self.__idleConnections:
            conn = idlePool.pop(0)
            pool[pid] = conn
            return conn

        conn = psycopg2.connect(database=connectionContext.database,
                                user=connectionContext.user,
                                password=connectionContext.password,
                                host=connectionContext.host,
                                port=connectionContext.port,
                                async=connectionContext.async)
        pool[pid] = conn

        conn.autocommit = connectionContext.autocommit

        return conn

    def closeConnection(self, readOnly):
        """
        closes the connection corresponding to keyName
        """
        pid = os.getpid()

        if readOnly:
            pool = self.__busyROConnections
        else:
            pool = self.__busyConnections

        try:
            if pid in pool:
                conn = pool[pid]
                conn.close()
                pool.pop(pid)
        except Exception as e:
            logger.error(exceptionStack(e))
            if pid in pool:
                pool.pop(pid)

    def destroy(self):
        """
        called by springpython when deleting objects
        """

        for conn in self.__busyConnections.values():
            conn.close()

        for conn in self.__busyROConnections.values():
            conn.close()

        for conn in self.__idleConnections:
            conn.close()

        for conn in self.__idleROConnections:
            conn.close()
