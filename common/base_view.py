# -*- coding: utf-8 -*-
__author__ = 'bogdan'
import logging
import ujson
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponse, HttpResponseRedirect
from socrative.settings import LANGUAGE_COOKIE_NAME
from common.socrative_api import byteLen
from common.socrative_config import ConfigRegistry
from socrative import settings as projSettings
from common.cryptography import Cryptography
from common.socrative_api import exceptionStack
from common.counters import HttpCounters, DatadogThreadStats


logger = logging.getLogger(__name__)
statsd = DatadogThreadStats.STATS


class BaseView(View):
    """
        Handle a web view
    """

    CONTENT_JSON = "application/json"
    WWW_CONTENT = "application/x-www-form-urlencoded"

    def render_json_response(self, outData, inHeaders=None, statusCode=None, language=None, failCounter=None,
                             cookie=None):
        """
        render a json HTTP response
        :param outData: Base response or dictionary to convert in json
        :type outData: BaseResponse, dict
        :param inHeaders: dictionary to append http headers
        :type inHeaders: dic
        :param statusCode: http status code
        :type statusCode: int, str
        :param language:
        :type language: str,unicode
        :param failCounter
        :param cookie
        :return: Http Response
        :rtype: HttpResponse
        """

        serializedResp = ujson.dumps(outData)

        httpResp = HttpResponse(serializedResp)
        httpResp['Content-Length'] = byteLen(serializedResp)
        httpResp['Content-Type'] = "application/json; charset=utf-8"
        if inHeaders is not None:
            for key in inHeaders.keys():
                httpResp[key] = inHeaders[key]

        if language is not None:
            httpResp.set_cookie(LANGUAGE_COOKIE_NAME, language, max_age=259200000)

        if cookie is not None:
            if cookie[0].startswith(projSettings.USER_AUTH_TOKEN):
                maxage = 2592000  # 30 days
            elif cookie[0].startswith(projSettings.STUDENT_AUTH_TOKEN):
                maxage = 2147483647  # exipre in 68 years...aka never in short terms view
            else:
                maxage = 604800
            httpResp.set_cookie(cookie[0], cookie[1], httponly=True, secure=self.request.is_secure(), path="/",
                                max_age=maxage, domain=ConfigRegistry.getItem("COOKIE_DOMAIN"))

        httpResp.status_code = statusCode

        if failCounter and statusCode not in (201, 200):
            statsd.increment(failCounter)

        if statusCode >= 500:
            statsd.increment(HttpCounters.INTERNAL_SERVER_ERROR)

        if 400 <= statusCode < 500:
            statsd.increment(HttpCounters.BAD_REQUESTS)

        statsd.increment(HttpCounters.TOTAL_REQUESTS)

        return httpResp

    def render_response(self, outData, statusCode=None, successCounter=None, failCounter=None):
        """
        render a json HTTP response
        :param outData: Base response or dictionary to convert in json
        :type outData: BaseResponse, dict
        :param statusCode: http status code
        :type statusCode: int, str
        :param successCounter
        :param failCounter
        :return: Http Response
        :rtype: HttpResponse
        """

        httpResp = HttpResponse(outData)
        httpResp['Content-Length'] = len(outData) if outData else 0
        httpResp['Content-Type'] = "application/json; charset=utf-8"

        httpResp.status_code = statusCode

        if failCounter and statusCode not in (201, 200):
            statsd.increment(failCounter)

        if statusCode >= 500:
            statsd.increment(HttpCounters.INTERNAL_SERVER_ERROR)

        if 400 <= statusCode < 500:
            statsd.increment(HttpCounters.BAD_REQUESTS)

        statsd.increment(HttpCounters.TOTAL_REQUESTS)

        return httpResp

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        """
        overwritten
        """
        # Try to dispatch to the right method; if a method doesn't exist,
        # defer to the error handler. Also defer to the error handler if the
        # request method isn't on the approved list.
        if request.method.lower() in self.http_method_names:
            handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        else:
            handler = self.http_method_not_allowed
        return handler(request, *args, **kwargs)

    def render_redirect_response(self, url, cookie=None):
        """
        redirects to a page that displays the errors or
        :param url:
        :return:
        """

        statsd.increment(HttpCounters.TOTAL_REQUESTS)
        statsd.increment(HttpCounters.REDIRECTED_REQUESTS)

        resp = HttpResponseRedirect(redirect_to=url)

        if cookie is not None:
            if cookie[0].startswith(projSettings.USER_AUTH_TOKEN):
                maxage = 2592000  # 30 days
            elif cookie[0].startswith(projSettings.STUDENT_AUTH_TOKEN):
                maxage = 2147483647  # 1 day for the student cookie
            else:
                maxage = 604800
            resp.set_cookie(cookie[0], cookie[1], httponly=True, secure=self.request.is_secure(), path="/",
                            max_age=maxage, domain=ConfigRegistry.getItem("COOKIE_DOMAIN"))

        return resp

    def return_streaming_file(self, fileBuffer, filename, statusCode):
        """

        :param fileBuffer:
        :param filename:
        :param statusCode:
        :return:
        """

        fileBuffer.seek(0, 0)

        httpResp = HttpResponse(fileBuffer.read())
        httpResp.status_code = statusCode
        httpResp["Content-Disposition"] = """attachment; filename="%s" """ % filename
        httpResp["Content-Length"] = byteLen(httpResp.content)

        statsd.increment(HttpCounters.STREAMING_REQUESTS)
        statsd.increment(HttpCounters.TOTAL_REQUESTS)
        return httpResp

    def getAuthToken(self, dataDict):
        """

        :param dataDict:
        :return:
        """

        try:
            val = self.request.COOKIES.get(projSettings.USER_AUTH_TOKEN)
            if val:
                dataDict["auth_token"] = Cryptography.decrypt(val)

        except Exception as e:
            logger.debug(exceptionStack(e))
            pass

    def getStudentUUID(self, dataDict):
        """

        :param dataDict:
        :return:
        """
        try:
            val = self.request.COOKIES.get(projSettings.STUDENT_AUTH_TOKEN)
            if val:
                dataDict["user_uuid"] = Cryptography.decrypt(val)
        except Exception as e:
            logger.debug(exceptionStack(e))
            pass