# -*- coding: utf-8 -*-
import logging
import time
from abc import ABCMeta

from psycopg2 import OperationalError

from common.postgre_connection_context import PostgreConnectionContext
from common.postgre_connection_manager import PostgreConnectionManager
from common.spring_container import SpringItem
from socrative_api import exceptionStack

logger = logging.getLogger(__name__)


class BaseModel(object):
    """
    abstract class for dao models
    """
    meta = ABCMeta

    def toDict(self):
        """
        implement this in subclass to transform the model to dictionary
        :return:
        """
        raise NotImplementedError("Please implement this in a subclass")

    @classmethod
    def fromDict(cls, dictObj):
        """
        static method to return a dao model from an object stored as a dict
        :param dictObj:
        :return:
        """
        raise NotImplementedError("Please implement this in a subclass")

    @classmethod
    def fromRequestDto(cls, dtoModel):
        """
        """
        raise NotImplementedError("Please implement this in a subclass")


class BaseDao(SpringItem):
    """
    Abstract class for the DAO
    """

    MODEL = BaseModel
    FETCH_MANY_SIZE = 1

    DESCENDING = -1
    ASCENDING = 1

    CREATED_DATE = 0
    UPDATED_DATE = 1
    NAME = 2
    USER_UUID = 3
    ORDER = 4

    TO_DICT = 0
    TO_MODEL = 1

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connectionManager):
        """
        constructor for Base dao
        """

        super(BaseDao, self).__init__()

        if type(dbConfigCtx) is not PostgreConnectionContext:
            raise TypeError("the dbConfigCtx param is not of type PostgreConnectionContext, but of type %s" %
                            str(type(dbConfigCtx)))

        if type(dbConfigCtxReadOnly) is not PostgreConnectionContext:
            raise TypeError("the dbConfigCtxReadOnly param is not of type PostgreConnectionContext, but of type %s" %
                            str(type(dbConfigCtxReadOnly)))

        if type(connectionManager) is not PostgreConnectionManager:
            raise TypeError("the connectionManager is not of type PostgreConnectionManager, but of type %s" %
                            str(type(connectionManager)))

        self._dbConfigCtx = dbConfigCtx
        self._dbConfigCtxReadOnly = dbConfigCtxReadOnly
        self._connMgr = connectionManager
        self.keyName = None
        self.daoRegistry = None

    def _getConnection(self):
        """
        getter for the connection
        """

        return self._connMgr.getConnection(self._dbConfigCtx)

    def _removeConnection(self, value):
        """
        close and remove the connection
        :param value:
        :return:
        """
        self._connMgr.closeConnection(readOnly=False)

    def _getReadOnlyConnection(self):
        """
        getter for the connection
        """

        return self._connMgr.getConnection(self._dbConfigCtxReadOnly)

    def _removeReadOnlyConnection(self, value):
        """
        close and remove the connection
        :param value:
        :return:
        """
        self._connMgr.closeConnection(self, readOnly=True)

    def __loadDictFromResult(self, resultTuple, cursorDescription):
        """

        :param resultTuple:
        :param cursorDescription:
        :return:
        """

        dataDict = dict()

        for i in range(len(cursorDescription)):
            description = cursorDescription[i]
            dataDict[description.name] = resultTuple[i]

        return dataDict

    def __loadDictFromResultFromTupleInterval(self, resultTuple, cursorDescription, start, stop):
        """

        :param resultTuple:
        :param cursorDescription:
        :param start:
        :param stop:
        :return:
        """
        dataDict = dict()
        if 0 <= start < len(cursorDescription) and 0 < stop <= len(cursorDescription):
            for i in range(start, stop):
                description = cursorDescription[i]
                dataDict[description.name] = resultTuple[i]
            return dataDict
        else:
            logger.warn("Received bad start of stop values.Cursor description %s , start %s, stop %s" %
                        (str(cursorDescription), str(start), str(stop)))
            return dataDict

    def _loadModelFromResult(self, resultTuple, cursorDescription):
        """
        method to store data into a model
        :param resultTuple:
        :param cursorDescription:
        :return:
        """

        dataDict = self.__loadDictFromResult(resultTuple, cursorDescription)

        return self.MODEL.fromDict(dataDict)

    def _loadDictFromResultTupleInterval(self, resultTuple, cursorDescription, start, stop, model):
        """
        """
        dataDict = self.__loadDictFromResultFromTupleInterval(resultTuple, cursorDescription, start, stop)
        return dataDict

    def fetchManyAsDict(self, cursor):
        """
        """
        respList = list()

        if cursor.rowcount <= 0:
            return respList
        else:
            count = cursor.rowcount
            while count > 0:
                listTuple = cursor.fetchmany(self.FETCH_MANY_SIZE)

                if not listTuple:
                    return respList
                else:
                    for tup in listTuple:
                        respList.append(self.__loadDictFromResult(tup, cursor.description))

                    count -= len(listTuple)

            return respList

    def fetchOneAsDict(self, cursor):
        """
        """
        resp = None

        if cursor.rowcount <= 0:
            return resp
        else:

            respTuple = cursor.fetchone()
            resp = (self.__loadDictFromResult(respTuple, cursor.description))
            return resp

    def fetchOneAsModel(self, cursor):
        """
        """
        resp = None

        if cursor.rowcount <= 0:
            return resp
        else:

            respTuple = cursor.fetchone()
            resp = (self._loadModelFromResult(respTuple, cursor.description))

            return resp

    def fetchManyAsModel(self, cursor):
        """
        """
        respList = list()

        if cursor.rowcount <= 0:
            return respList
        else:
            count = cursor.rowcount
            while count > 0:
                listTuple = cursor.fetchmany(self.FETCH_MANY_SIZE)

                if not listTuple:
                    return respList
                else:
                    for tup in listTuple:
                        respList.append(self._loadModelFromResult(tup, cursor.description))

                    count -= len(listTuple)
            return respList

    class NotUpdated(Exception):
        pass

    class NotInserted(Exception):
        pass

    class NotDeleted(Exception):
        pass

    class NotUpserted(Exception):
        pass

    class NotFound(Exception):
        pass

    class FoundTooMany(Exception):
        pass

    def destroy(self):
        """
        nothing to do here
        """
        pass

    #===========================
    # PROPERTIES
    #===========================

    connection = property(_getConnection, _removeConnection)
    readOnlyConnection = property(_getReadOnlyConnection, _removeReadOnlyConnection)


class DaoRegistry(SpringItem):
    """
    registry to keep the dao classes
    """

    def __init__(self):
        """
        Constructor
        :return:
        """

        super(DaoRegistry, self).__init__()

        self.__studentNameDao = None
        self.__studentResponseDao = None
        self.__studentResponseTextAnswerDao = None
        self.__studentResponseAnswerSelectionDao = None
        self.__mediaResourceDao = None
        self.__partnerDao = None
        self.__activitySettingsDao = None
        self.__activityInstanceDao = None
        # quizzes
        self.__questionDao = None
        self.__answerDao = None
        self.__quizDao = None
        # room
        self.__roomDao = None
        self.__systemMessageDao = None
        # users
        self.__usersDao = None
        self.__userSystemMessageDao = None
        self.__tempTokenDao = None
        # tags
        self.__standardsDao = None
        # pubnub
        self.__longMessageDao = None
        self.__rosterDao = None
        self.__studentDao = None
        self.__studentActivityDao = None

        self.__rosterFileSettingsDao = None

        self.__scoreSettingsDao = None
        self.__licenseDao = None
        self.__notificationDao = None
        self.__licenseTransactionDao = None
        self.__licenseActivationDao = None
        self.__couponDao = None
        self.__folderDao = None

        super(DaoRegistry, self).__init__()

    def getStudentNameDao(self):
        """
        getter
        :return:
        """
        return self.__studentNameDao

    def setStudentNameDao(self, dao):
        """
        setter
        :return:
        """

        self.__studentNameDao = dao
        self.__studentNameDao.daoRegistry = self

    def getStudentResponseDao(self):
        """
        getter
        :return:
        """
        return self.__studentResponseDao

    def setStudentResponseDao(self, dao):
        """
        setter
        :return:
        """
        self.__studentResponseDao = dao
        self.__studentResponseDao.daoRegistry = self

    def getStudentResponseTextAnswerDao(self):
        """
        getter
        :return:
        """
        return self.__studentResponseTextAnswerDao

    def setStudentResponseTextAnswerDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """
        self.__studentResponseTextAnswerDao = dao
        self.__studentResponseTextAnswerDao.daoRegistry = self

    def getStudentResponseAnswerSelectionDao(self):
        """

        :return:
        """
        return self.__studentResponseAnswerSelectionDao

    def setStudentResponseAnswerSelectionDao(self, dao):
        """

        :param dao:
        :return:
        """

        self.__studentResponseAnswerSelectionDao = dao
        self.__studentResponseAnswerSelectionDao.daoRegistry = self

    def getMediaResourceDao(self):
        """
        getter
        :return:
        """
        return self.__mediaResourceDao

    def setMediaResourceDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """
        self.__mediaResourceDao = dao
        self.__mediaResourceDao.daoRegistry = self

    def getPartnerDao(self):
        """
        getter
        :return:
        """
        return self.__partnerDao

    def setPartnerDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """
        self.__partnerDao = dao
        self.__partnerDao.daoRegistry = self

    def getActivitySettingsDao(self):
        """
        getter
        :return:
        """
        return self.__activitySettingsDao

    def setActivitySettingsDao(self, dao):
        """
        setter
        :return:
        """
        self.__activitySettingsDao = dao
        self.__activitySettingsDao.daoRegistry = self

    def getActivityInstanceDao(self):
        """
        getter
        :return:
        """
        return self.__activityInstanceDao

    def setActivityInstanceDao(self, dao):
        """

        :param dao:
        :return:
        """
        self.__activityInstanceDao = dao
        self.__activityInstanceDao.daoRegistry = self

    def getQuestionDao(self):
        """

        :return:
        """
        return self.__questionDao

    def setQuestionDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """
        self.__questionDao = dao
        self.__questionDao.daoRegistry = self

    def getAnswerDao(self):
        """
        getter
        :return:
        """
        return self.__answerDao

    def setAnswerDao(self, dao):
        """
        setter
        :return:
        """
        self.__answerDao = dao
        self.__answerDao.daoRegistry = self

    def getQuizDao(self):
        """
        getter
        :return:
        """
        return self.__quizDao

    def setQuizDao(self, dao):
        """
        setter
        :return:
        """
        self.__quizDao = dao
        self.__quizDao.daoRegistry = self

    def getRoomDao(self):
        """
        getter
        :return:
        """
        return self.__roomDao

    def setRoomDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__roomDao = dao
        self.__roomDao.daoRegistry = self

    def getUsersDao(self):
        """
        getter
        :return:
        """
        return self.__usersDao

    def setUsersDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__usersDao = dao
        self.__usersDao.daoRegistry = self

    def getStandardsDao(self):
        """
        getter
        :return:
        """
        return self.__standardsDao

    def setStandardsDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__standardsDao = dao
        self.__standardsDao.daoRegistry = self

    def getUserSystemMessageDao(self):
        """
        getter
        :return:
        """
        return self.__userSystemMessageDao

    def setUserSystemMessageDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__userSystemMessageDao = dao
        self.__userSystemMessageDao.daoRegistry = self

    def getSystemMessageDao(self):
        """
        getter
        :return:
        """
        return self.__systemMessageDao

    def setSystemMessageDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__systemMessageDao = dao
        self.__systemMessageDao.daoRegistry = self

    def getTempTokenDao(self):
        """
        getter
        :return:
        """
        return self.__tempTokenDao

    def setTempTokenDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__tempTokenDao = dao
        self.__tempTokenDao.daoRegistry = self

    def getLongMessageDao(self):
        """
        getter
        :return:
        """
        return self.__longMessageDao

    def setLongMessageDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__longMessageDao = dao
        self.__longMessageDao.daoRegistry = self

    def getRosterDao(self):
        """
        getter
        :return:
        """
        return self.__rosterDao

    def setRosterDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__rosterDao = dao
        self.__rosterDao.daoRegistry = self

    def getStudentDao(self):
        """
        getter
        :return:
        """
        return self.__studentDao

    def setStudentDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__studentDao = dao
        self.__studentDao.daoRegistry = self

    def getStudentActivityDao(self):
        """
        getter
        :return:
        """
        return self.__studentActivityDao

    def setStudentActivityDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__studentActivityDao = dao
        self.__studentActivityDao.daoRegistry = self

    def getRosterSettings(self):
        """
        getter
        :return:
        """
        return self.__rosterFileSettingsDao

    def setRosterSettings(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__rosterFileSettingsDao = dao
        self.__rosterFileSettingsDao.daoRegistry = self

    def getScoreSettingsDao(self):
        """
        getter
        :return:
        """
        return self.__scoreSettingsDao

    def setScoreSettingsDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__scoreSettingsDao = dao
        self.__scoreSettingsDao.daoRegistry = self

    def getLicenseDao(self):
        """
        getter
        :return:
        """
        return self.__licenseDao

    def setLicenseDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__licenseDao = dao
        self.__licenseDao.daoRegistry = self

    def getNotificationDao(self):
        """
        getter
        :return:
        """
        return self.__notificationDao

    def setNotificationDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__notificationDao = dao
        self.__notificationDao.daoRegistry = self

    def getLicenseTransactionDao(self):
        """
        getter
        :return:
        """
        return self.__licenseTransactionDao

    def setLicenseTransactionDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__licenseTransactionDao = dao
        self.__licenseTransactionDao.daoRegistry = self

    def getLicenseActivationDao(self):
        """
        getter
        :return:
        """
        return self.__licenseActivationDao

    def setLicenseActivationDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__licenseActivationDao = dao
        self.__licenseActivationDao.daoRegistry = self

    def getCouponDao(self):
        """
        getter
        :return:
        """
        return self.__couponDao

    def setCouponDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__couponDao = dao
        self.__couponDao.daoRegistry = self

    def getFolderDao(self):
        """
        getter
        :return:
        """
        return self.__folderDao

    def setFolderDao(self, dao):
        """
        setter
        :param dao:
        :return:
        """

        self.__folderDao = dao
        self.__folderDao.daoRegistry = self

    def destroy(self):
        """
        called by spring python when destroying objects
        :return:
        """

        # nothing to do, each dao object will be deleted in another place
        pass

    # =============================
    # PROPERTIES
    # =============================

    studentNameDao = property(getStudentNameDao, setStudentNameDao)
    studentResponseDao = property(getStudentResponseDao, setStudentResponseDao)
    studentResponseTextAnswerDao = property(getStudentResponseTextAnswerDao, setStudentResponseTextAnswerDao)
    studentResponseAnswerSelectionDao = property(getStudentResponseAnswerSelectionDao, setStudentResponseAnswerSelectionDao)
    mediaResourceDao = property(getMediaResourceDao, setMediaResourceDao)
    partnerDao = property(getPartnerDao, setPartnerDao)
    activitySettingsDao = property(getActivitySettingsDao, setActivitySettingsDao)
    activityInstanceDao = property(getActivityInstanceDao, setActivityInstanceDao)
    questionDao = property(getQuestionDao, setQuestionDao)
    answerDao = property(getAnswerDao, setAnswerDao)
    quizDao = property(getQuizDao, setQuizDao)
    roomDao = property(getRoomDao, setRoomDao)
    usersDao = property(getUsersDao, setUsersDao)
    standardsDao = property(getStandardsDao, setStandardsDao)
    userMessageDao = property(getUserSystemMessageDao, setUserSystemMessageDao)
    systemMessageDao = property(getSystemMessageDao, setSystemMessageDao)
    tempTokenDao = property(getTempTokenDao, setTempTokenDao)
    longMessageDao = property(getLongMessageDao, setLongMessageDao)
    rosterDao = property(getRosterDao, setRosterDao)
    studentDao = property(getStudentDao, setStudentDao)
    studentActivityDao = property(getStudentActivityDao, setStudentActivityDao)
    rosterFileSettingsDao = property(getRosterSettings, setRosterSettings)
    scoreSettingsDao = property(getScoreSettingsDao, setScoreSettingsDao)
    licenseDao = property(getLicenseDao, setLicenseDao)
    notificationDao = property(getNotificationDao, setNotificationDao)
    licenseTransactionDao = property(getLicenseTransactionDao, setLicenseTransactionDao)
    getLicenseActivationDao = property(getLicenseActivationDao, setLicenseActivationDao)
    couponDao = property(getCouponDao, setCouponDao)
    folderDao = property(getFolderDao, setFolderDao)


def retry(dao_method):
    """
    decorator for a getter that has 1 value returned
    :param dao_method:
    :return:
    """

    def inner(*args, **kwargs):
        """
        inner function that does the job
        :param args:
        :param kwargs:
        :return:
        """
        try:
            return dao_method(*args, **kwargs)
        except OperationalError as e:
            logger.error(exceptionStack(e))
            time.sleep(1)
            return dao_method(*args, **kwargs)
        except BaseDao.NotFound:
            raise
        except BaseDao.NotDeleted:
            raise
        except BaseDao.NotUpdated:
            raise
        except BaseDao.NotInserted:
            raise
        except BaseDao.NotUpserted:
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            raise

    return inner