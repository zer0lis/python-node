# -*- coding: utf-8 -*-
# Common Models
from django.db import models


from socrative_users.models import SocrativeUser
from rooms.models import Room


class MediaResource(models.Model):
    class Meta:
        verbose_name_plural = "Media Resources"

    owner = models.ForeignKey(SocrativeUser)
    
    # short user-readable name to remember this resource by 
    # ex: 'Pic of a Tree'
    name = models.CharField(max_length=255, blank=True, default="")
    data = models.CharField(max_length=2048, blank=True, null=True, default="{}")

    # Type of resource that's being linked to
    IMAGE = "IM"
    VIDEO = "VD"
    AUDIO_CLIP = "AC"
    YOUTUBE_LINK = "YT"
    type_choices = (
        (IMAGE, u"Image"),
        (VIDEO, u"Video"),
        (AUDIO_CLIP, u"Audio Clip"),
        (YOUTUBE_LINK, u"YouTube Link"),
    )
    type = models.CharField(max_length=2, choices=type_choices, default=IMAGE)

    # link for the resource
    url = models.URLField(max_length=1024)
    
    def __unicode__(self):
        type_name = ""

        for type_tuple in self.type_choices:
            if self.type == type_tuple[0]:
                type_name = type_tuple[1]

        return u"{0}: {1}".format(self.name, type_name)

class Partner(models.Model):
    class Meta:
        verbose_name_plural = "Partners"

    user = models.ForeignKey(SocrativeUser, related_name="partners")
    
    # Type of resource that's being linked to
    EDMODO = "ED"
    ALWAYS_PREPPED = "AP"
    GOOGLE = "GO"
    PREMIUM = "PR"
    MASTERY_CONNECT = "MC"
    type_choices = (
        (EDMODO, u"Edmodo"),
        (ALWAYS_PREPPED, u"Always Prepped"),
        (GOOGLE, u"Google"),
        (PREMIUM, u"Socrative Premium"),
        (MASTERY_CONNECT, u"Mastery Connect")
    )
    type = models.CharField(max_length=2, choices=type_choices, default=PREMIUM)

    # Stringified JSON of required data (if any)
    data = models.TextField(default="", max_length=5120, blank=True)
    
    def __unicode__(self):
        type_name = ""

        for type_tuple in self.type_choices:
            if self.type == type_tuple[0]:
                type_name = type_tuple[1]

        return u"{0}".format(type_name)


# ActivitySettings are key value pairs that contain extra-settings for
# quizzes (and other activity types in the future)
class ActivitySetting(models.Model):
    class Meta:
        verbose_name_plural = "Activity Settings"

    key = models.CharField(db_index=True, max_length=255, blank=False, default="")
    value = models.CharField(max_length=255, blank=False, default="")

    def __unicode__(self):
        return u"ActivitySetting {0} = {1}".format(self.key, self.value)


# A record of a single 'run' of a Quiz
class ActivityInstance(models.Model):
    class Meta:
        verbose_name_plural = "Activity Instances"

    legacy_id = models.IntegerField(blank=True, null=True)

    # When this activity began
    start_time = models.DateTimeField(auto_now_add=True)

    # If set, this is when the activity ended
    end_time = models.DateTimeField(db_index=True, blank=True, null=True)

    # what activity was run
    QUIZ = "QZ"
    SINGLE_QUESTION = "1Q"
    SPACE_RACE = "SR"
    type_choices = (
        (QUIZ, u"Quiz"),
        (SINGLE_QUESTION, u"Single Question Activity"),
        (SPACE_RACE, u"Space Race")
    )
    activity_type = models.CharField(max_length=2, choices=type_choices)
    activity_id = models.IntegerField()
    activity_state = models.IntegerField(default=-1)
    hide_report = models.BooleanField(default=False)

    REPORT_NOT_DONE = 0
    REPORT_IN_PROGRESS = 1
    REPORT_DONE = 2
    report_status = models.IntegerField(null=True, default=0)

    students_finished = models.TextField(max_length=65536, default="[]")
        
    # Which user ran the activity
    started_by = models.ForeignKey(SocrativeUser, related_name="activities_run")
    
    room_name = models.TextField(max_length=32, null=True, db_index=True)

    # Collection of Key Value pairs for settings for this activity
    settings = models.ManyToManyField(ActivitySetting, blank=True)

    state = models.IntegerField(null=True)
    last_updated = models.DateTimeField(null=True)

    def __unicode__(self):
        return u"ActivityInstance[pk: %s, Activity: %s%s, start:%s, end:%s]" % (self.pk, self.activity_type,
                                                                                self.activity_id, self.start_time,
                                                                                self.end_time)
