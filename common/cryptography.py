# -*- coding: utf-8 -*-
"""
    Cryptography module
"""
__author__ = 'bogdan'

from Crypto.Cipher import DES
import base64
import logging
import string
import random
import uuid
logger = logging.getLogger(__name__)
from socrative.settings import PLATFORM


class Cryptography(object):
    """
    class that will have static methods to encrypt and decrypt
    """

    # it has to be the same vector all the time
    IV_VECTOR = '\x8a\xb8\xb1\xf7\xe1\x8b\x5a\x5b'

    PKCS5_1_BYTE = "\x01"
    PKCS5_2_BYTES = "\x02\x02"
    PKCS5_3_BYTES = "\x03\x03\x03"
    PKCS5_4_BYTES = "\x04\x04\x04\x04"
    PKCS5_5_BYTES = "\x05\x05\x05\x05\x05"
    PKCS5_6_BYTES = "\x06\x06\x06\x06\x06\x06"
    PKCS5_7_BYTES = "\x07\x07\x07\x07\x07\x07\x07"
    PKCS5_8_BYTES = "\x08\x08\x08\x08\x08\x08\x08\x08"

    TOKEN = "TT_"
    DOWNLOAD = "DL_"
    SHARE = "SOC_"
    USER_AUTH = "UA_" if PLATFORM in ("PROD", "JENKINS") else "UA%s_" % PLATFORM[:2]
    STUDENT_AUTH = "SA_" if PLATFORM in ("PROD", "JENKINS") else "SA%s_" % PLATFORM[:2]
    SIGNUP_PREMIUM = "SP_"

    PREFIXES = [TOKEN, DOWNLOAD, SHARE, USER_AUTH, STUDENT_AUTH, SIGNUP_PREMIUM]

    KEY_DICT = {
        TOKEN: "\xd2\xd4\xe1\xef\x0b\x6b\x5b\x8c",
        DOWNLOAD: "\xa4\xc0\x01\x3d\x8a\xe2\x1f\x4c",
        SHARE: "\xe1\xd0\x43\xf1\x29\x8e\x4f\xbc",
        USER_AUTH: "\xff\xf7\xc7\xae\xff\x55\x74\x24",
        STUDENT_AUTH: "\xb8\x3b\x45\x62\xce\xc8\xee\x88",
        SIGNUP_PREMIUM: "\x2c\x07\x79\x21\xa4\x67\x55\x7a",
    }

    SALT = 'DSFJKO$#NJF'

    ALPHABET = string.digits+string.letters
    DISPLAY_ALPHABET = ''.join([a[0] for a in ALPHABET if a not in ('1', '0', 'O', 'o', 'l', 'I')])

    @classmethod
    def padPkcs5(cls, message):
        """
        added padding at the end of the message to make it's length multiple of 8
        :param cls:
        :param message: original message
        :return: padded message
        """

        # I didn't put try catch because it doesn't get here if the message is not a string

        # get how many bytes has over a multiple of 8.
        l = len(message) % 8

        if l == 0:
            # add 8 bytes padding
            return message + cls.PKCS5_8_BYTES
        if l == 1:
            # add 7 bytes padding
            return message + cls.PKCS5_7_BYTES
        if l == 2:
            # add 6 bytes padding
            return message + cls.PKCS5_6_BYTES
        if l == 3:
            # add 5 bytes padding
            return message + cls.PKCS5_5_BYTES
        if l == 4:
            # add 4 bytes padding
            return message + cls.PKCS5_4_BYTES
        if l == 5:
            # add 3 bytes padding
            return message + cls.PKCS5_3_BYTES
        if l == 6:
            # add 2 bytes padding
            return message + cls.PKCS5_2_BYTES
        if l == 7:
            # add 1 byte padding
            return message + cls.PKCS5_1_BYTE

    @classmethod
    def encryptDES_CBC(cls, prefix, message):
        """
        encrypts the message with DES_CBC encryption algorithm
        :param message: the message to be encrypted
        :return: the encrypted message in hexadecimal format
        :rtype string
        """
        if prefix not in cls.PREFIXES:
            raise Exception("Encryption unknown with prefix %s" % prefix)

        paddedMessage = cls.padPkcs5(message)
        temp = base64.b64encode(DES.new(cls.KEY_DICT[prefix],
                                        DES.MODE_CBC,
                                        cls.IV_VECTOR).encrypt(paddedMessage))
        temp = temp.replace('+','-').replace('/', '_')
        return prefix + temp

    @classmethod
    def encryptListDES_CBC(cls, prefix, messageList):
        """
        encrypts the message with DES_CBC encryption algorithm
        :param messageList: the list of messages to be encrypted
        :return: the encrypted message list in hexadecimal format
        :rtype list
        """

        if prefix not in cls.PREFIXES:
            raise Exception("Encryption unknown with prefix %s" % prefix)

        encryptedMessageList = []
        for message in messageList:
            paddedMessage = cls.padPkcs5(message)

            temp = base64.b64encode(DES.new(cls.KEY_DICT[prefix],
                                            DES.MODE_CBC,
                                            cls.IV_VECTOR).encrypt(paddedMessage))
            temp = temp.replace('+', '-').replace('/', '_')

            encryptedMessageList.append(prefix + temp)
        return encryptedMessageList

    @classmethod
    def __decryptDES_CBC(cls, prefix, encryptedText):
        """
        decrypts the received text using
        :param encryptedText: the text do be decrypted
        :return: the original message
        :rtype str, unicode
        """
        encryptedText = encryptedText.replace('-', '+').replace('_', '/')
        temp = base64.b64decode(encryptedText)
        deciphered = DES.new(cls.KEY_DICT[prefix], DES.MODE_CBC, cls.IV_VECTOR).decrypt(temp)
        return deciphered[:-1*ord(deciphered[-1])]

    @classmethod
    def decrypt(cls, encryptedText):
        """
        handles matching of algorithm and decryption using the correct key and algo
        :param cls:
        :param encryptedText: XX_ciphered_text, where XX is the code for the algorithm to be used for decryption
        :return: original message
        :rtype str, unicode
        """

        if "_" in encryptedText:
            index = encryptedText.index("_")
            prefix = encryptedText[:index+1]
            if prefix not in cls.PREFIXES:
                raise Exception("unknown encryption method")

            return cls.__decryptDES_CBC(prefix, encryptedText[len(prefix):])

        raise ValueError("Bad encrypted token")

    @classmethod
    def decryptList(cls, encryptedTextList):
        """
        handles matching of algorithm and decryption using the correct key and algo
        :param cls:
        :param encryptedTextList: List of XX_ciphered_text, where XX is the code for the algorithm to be used for decryption
        :return: List of original messages
        :rtype list
        """

        originalMessageList = []
        for encryptedText in encryptedTextList:
            index = encryptedText.index("_")
            prefix = encryptedText[:index+1]
            if prefix not in cls.PREFIXES:
               raise("unknown encryption method for %s", encryptedText)

            originalMessageList.append(cls.__decryptDES_CBC(prefix, encryptedText[len(cls.KEY_DICT[prefix]):]))

        return originalMessageList

    @classmethod
    def generate_random_password(cls, passwordLen):
        """

        :param passwordLen:
        :return:
        """
        return ''.join([cls.ALPHABET[random.randint(0, len(cls.ALPHABET)-1)] for _ in range(passwordLen)])

    @classmethod
    def generate_display_name(cls, displayNameLen):
        """

        :param displayNameLen:
        :return:
        """
        return ''.join([cls.DISPLAY_ALPHABET[random.randint(0, len(cls.DISPLAY_ALPHABET)-1)] for _ in range(displayNameLen)])

    @classmethod
    def generate_room_code(cls, roomCodeLen):
        """

        :param roomCodeLen:
        :return:
        """
        return ''.join(
            [cls.DISPLAY_ALPHABET[random.randint(0, len(cls.DISPLAY_ALPHABET) - 1)] for _ in range(roomCodeLen)])

    @classmethod
    def generate_auth_token(cls, authLen=20):
        """

        :param authLen:
        :return:
        """
        return uuid.uuid4().hex[:authLen]

    @classmethod
    def encryptUserAuth(cls, message):
        """

        :param message:
        :return:
        """

        return cls.encryptDES_CBC(cls.USER_AUTH, message)

    @classmethod
    def encryptStudentAuth(cls, message):
        """

        :param message:
        :return:
        """

        return cls.encryptDES_CBC(cls.STUDENT_AUTH, message)