# -*- coding: utf-8 -*-
import logging
import os
import time

from redis.client import Redis
from redis.exceptions import ReadOnlyError

from common.counters import DatadogThreadStats
from common.counters import RedisCounters
from common.socrative_api import safeToBool, safeToInt, exceptionStack
from common.spring_container import SpringItem

statsd = DatadogThreadStats.STATS
logger = logging.getLogger(__name__)


class RedisConfigSettings(SpringItem):
    """
    class that stores the configuration for redis servers
    """

    def __init__(self, hostname, port, database, password, socket_timeout, socket_connect_timeout,
                 socket_keepalive):
        """
        Constructor
        :param hostname:
        :param port:
        :param database:
        :param password:
        :param socket_timeout:
        :param socket_connect_timeout:
        :param socket_keepalive:
        """

        super(RedisConfigSettings, self).__init__()

        self.hostname = hostname
        self.port = safeToInt(port)
        self.database = safeToInt(database)
        self.password = password if password is not None and password != "None" else None
        self.socket_timeout = safeToInt(socket_timeout)
        self.socket_connect_timeout = safeToInt(socket_connect_timeout)
        self.socket_keepalive = safeToBool(socket_keepalive)

    def destroy(self):
        """
        pass , nothing to do here, leave spring python to take care of this
        """
        pass


class SocrativeClient(Redis):
    """
    wrapper for redis client
    """

    def __init__(self, host, port, db, password, socket_timeout, socket_connect_timeout, socket_keepalive):
        """
        Constructor
        """
        super(SocrativeClient, self).__init__(host, port, db, password, socket_timeout, socket_connect_timeout,
                                              socket_keepalive)

        self.parentPool = None

    def disconnect(self):
        """

        :return:
        """
        self.connection_pool.disconnect()

    @statsd.timed(RedisCounters.SET_ONE)
    def set(self, name, value, **kwargs):
        """
        set one wrapper
        :param name:
        :param value:
        :param kwargs:
        """

        res = None
        try:
            res = super(SocrativeClient, self).setex(name, value, kwargs.get("expireTime", None))
        except ReadOnlyError as e:
            logger.warn("the slave that we're connected became read only: %s" % exceptionStack(e))
            self.parentPool.remakeConnections(self)

        return res

    @statsd.timed(RedisCounters.SET_MULTI)
    def set_multi(self, *args, **kwargs):
        """
        set one wrapper
        :param args:
        :param kwargs:
        """
        res = None
        try:
            res = super(SocrativeClient, self).mset(**args[0])
            for k in args[0]:
                self.expire(k, kwargs.get("expireTime"))
        except ReadOnlyError as e:
            logger.exception("The slave that we're connected became read only: %s" % exceptionStack(e))
            self.parentPool.remakeConnections(self)

        return res

    @statsd.timed(RedisCounters.DELETE_ONE)
    def delete(self, *names):
        """
        :param names: key to be deleted
        """

        res = None
        try:
            res = super(SocrativeClient, self).delete(*names)
        except ReadOnlyError as e:
            logger.warn("the slave that we're connected became read only: %s" % exceptionStack(e))
            self.parentPool.remakeConnections(self)

        return res

    @statsd.timed(RedisCounters.DELETE_MULTI)
    def delete_multi(self, *names):
        """
        :param names: tuple with key names to be deleted
        """

        start = time.time()
        res = None
        try:
            res = super(SocrativeClient, self).delete(*names)
        except ReadOnlyError as e:
            logger.warn("the slave that we're connected became read only: %s" % exceptionStack(e))
            self.parentPool.remakeConnections(self)

        return res

    def get(self, name):
        """
        :param name:
        :return cache obj
        """

        start = time.time()
        res = super(SocrativeClient, self).get(name)
        if res:
            statsd.histogram(RedisCounters.GET_ONE_CACHE_HIT, time.time()-start)
        else:
            statsd.histogram(RedisCounters.GET_ONE_CACHE_MISS, time.time()-start)

        return res

    def get_multi(self, keys):
        """
        :param keys:
        """

        start = time.time()
        res = super(SocrativeClient, self).mget(keys)
        if res:
            statsd.histogram(RedisCounters.GET_MULTI_CACHE_HIT, time.time()-start)
        diff = len(keys) - len(res)
        if diff > 0:
            statsd.histogram(RedisCounters.GET_MULTI_CACHE_MISS, time.time()-start)

        return res

    @statsd.timed(RedisCounters.ADD_TO_SET)
    def addToSet(self, key, member, expireTime=None):
        """

        :param key:
        :param member:
        :param expireTime:
        :return:
        """
        res = None
        try:
            res = super(SocrativeClient, self).sadd(key, member)
            super(SocrativeClient, self).expire(key, expireTime)
        except ReadOnlyError as e:
            logger.warn("the slave that we're connected became read only: %s" % exceptionStack(e))
            self.parentPool.remakeConnections(self)

        return res

    @statsd.timed(RedisCounters.INCREMENT)
    def increment(self, key, expireTime=None):
        """

        :param key:
        :param expireTime:
        :return:
        """
        res = None
        try:
            res = super(SocrativeClient, self).incr(key)
            super(SocrativeClient, self).expire(key, expireTime)
        except ReadOnlyError as e:
            logger.warn("the slave that we're connected became read only: %s" % exceptionStack(e))
            self.parentPool.remakeConnections(self)

        return res

    @statsd.timed(RedisCounters.SISMEMBER)
    def sismember(self, key, member):
        """

        :param key:
        :param member:
        :return:
        """
        res = super(SocrativeClient, self).sismember(key, member)

        return res

    @statsd.timed(RedisCounters.SCARD)
    def scard(self, key):
        """

        :param key:
        :return:
        """
        res = super(SocrativeClient, self).scard(key)

        return res


class RedisConnectionPool(SpringItem):
    """
    class that will handle the number of connections to the redis server
    """

    def __init__(self, redisReadConfigList, redisWriteConfigList):
        """
        Constructor
        :param redisReadConfigList:
        :param redisWriteConfigList:
        :return:
        """

        super(RedisConnectionPool, self).__init__()

        self.__redisReadConfigList = redisReadConfigList
        self.__redisWriteConfigList = redisWriteConfigList

        self.__connectionReadDict = dict()
        self.__connectionWriteDict = dict()
        self.__writeIndex = 1
        self.__readIndex = 1
        self.__maxWrite = len(self.__redisWriteConfigList)
        self.__maxRead = len(self.__redisReadConfigList)

    def remakeConnections(self, conn):
        """

        :param conn:
        :return:
        """

        # move the current write connection to the end of the read only connection list
        # then move the first connection from the read only to the write connection list
        self.__writeIndex = 1
        self.__readIndex = 1
        self.__maxWrite = 1
        self.__redisWriteConfigList.append(self.__redisReadConfigList[0])
        config = self.__redisWriteConfigList.pop(0)
        self.__redisReadConfigList.append(config)

        # close all connections
        for conn in self.__connectionWriteDict.values():
            conn.disconnect()

        for conn in self.__connectionReadDict.values():
            conn.disconnect()

        self.__connectionReadDict = {}
        self.__connectionWriteDict = {}

    def getConnection(self, read=False):
        """
        It will provide a cached connection if it exists one otherwise it will create a new connection and add it to the pool
        Each time a connection is created it will use the next configuration from the redis configuration list
        Basically we try to connect to a new node if there are more nodes in the cluster.
        So the first connection will be made towards node 1, the second towards node 2, the third to node 3 if there are 3 nodes
        otherwise it will connect to node 1 if there are only 2 nodes
        :param read: if you need a connection  to a read replica or to the write master
        :return:
        """

        pid = os.getpid()

        if read is True:
            dct = self.__connectionReadDict
        else:
            dct = self.__connectionWriteDict

        if pid in dct:
            client = dct[pid]
            return client

        if read is True:
            # create a read only connection towards the next configuration node in the config list
            self.__readIndex = (self.__readIndex + 1) % len(self.__redisReadConfigList)
            client = SocrativeClient(self.__redisReadConfigList[self.__readIndex].hostname,
                                     self.__redisReadConfigList[self.__readIndex].port,
                                     self.__redisReadConfigList[self.__readIndex].database,
                                     self.__redisReadConfigList[self.__readIndex].password,
                                     self.__redisReadConfigList[self.__readIndex].socket_timeout,
                                     self.__redisReadConfigList[self.__readIndex].socket_connect_timeout,
                                     self.__redisReadConfigList[self.__readIndex].socket_keepalive)
            client.parentPool = self

            self.__connectionReadDict[pid] = client
        else:
            # create a read & write  connection towards the next configuration node in the writeable config list
            self.__writeIndex = (self.__writeIndex + 1) % len(self.__redisWriteConfigList)
            client = SocrativeClient(self.__redisWriteConfigList[self.__writeIndex].hostname,
                                     self.__redisWriteConfigList[self.__writeIndex].port,
                                     self.__redisWriteConfigList[self.__writeIndex].database,
                                     self.__redisWriteConfigList[self.__writeIndex].password,
                                     self.__redisWriteConfigList[self.__writeIndex].socket_timeout,
                                     self.__redisWriteConfigList[self.__writeIndex].socket_connect_timeout,
                                     self.__redisWriteConfigList[self.__writeIndex].socket_keepalive)

            client.parentPool = self

            self.__connectionWriteDict[pid] = client

        return client

    def destroy(self):
        """
        clean up
        :return:
        """

        for value in self.__connectionReadDict.values():
            value.disconnect()

        for value in self.__connectionWriteDict.values():
            value.disconnect()


class BaseCache(SpringItem):
    """
    base class for redis connections
    """

    def __init__(self, redisPool):
        """
        Constructor
        :param redisPool:
        :return:
        """
        super(BaseCache, self).__init__()

        self.__redisPool = redisPool
        self.expireTime = None

    def getConnection(self, read=False):
        """
        gets a connection
        :param read:
        :return:
        """
        return self.__redisPool.getConnection(read)

    def destroy(self):
        """
        let python handle it
        """
        pass


class BaseLimiter(BaseCache):
    """
    same as BaseCache but with different logic on item validation
    """


class CacheRegistry(object):
    """
    registry with all the cache objects
    """

    def __init__(self):
        """

        :return:
        """

        self.__quizCache = None
        self.__studentFindResponseCache = None
        self.__longMessageCache = None
        self.__userModelCache = None
        self.__roomModelCache = None
        self.__roomListCache = None
        self.__activitySettingsCache = None

    def __getQuizCache(self):
        """

        :return:
        """
        return self.__quizCache

    def __setQuizCache(self, cache):
        """

        :param cache:
        :return:
        """
        self.__quizCache = cache
        self.__quizCache.cacheRegistry = self

    def __getStudentFindResponseCache(self):
        """

        :return:
        """
        return self.__studentFindResponseCache

    def __setStudentFindResponseCache(self, cache):
        """

        :param cache:
        :return:
        """

        self.__studentFindResponseCache = cache
        self.__studentFindResponseCache.cacheRegistry = self

    def __getLongMessageCache(self):
        """

        :return:
        """
        return self.__longMessageCache

    def __setLongMessageCache(self, cache):
        """

        :param cache:
        :return:
        """

        self.__longMessageCache = cache
        self.__longMessageCache.cacheRegistry = self

    def __getUserModelCache(self):
        """

        :return:
        """
        return self.__userModelCache

    def __setUserModelCache(self, cache):
        """

        :param cache:
        :return:
        """

        self.__userModelCache = cache
        self.__userModelCache.cacheRegistry = self

    def __getRoomModelCache(self):
        """

        :return:
        """
        return self.__roomModelCache

    def __setRoomModelCache(self, cache):
        """

        :param cache:
        :return:
        """

        self.__roomModelCache = cache
        self.__roomModelCache.cacheRegistry = self

    def __getRoomListCache(self):
        """

        :return:
        """
        return self.__roomListCache

    def __setRoomListCache(self, cache):
        """

        :param cache:
        :return:
        """

        self.__roomListCache = cache
        self.__roomListCache.cacheRegistry = self

    def __getActivitySettingsCache(self):
        """

        :return:
        """
        return self.__activitySettingsCache

    def __setActivitySettingsCache(self, cache):
        """

        :param cache:
        :return:
        """

        self.__activitySettingsCache = cache
        self.__activitySettingsCache.cacheRegistry = self

    def after_properties_set(self):
        """

        :return:
        """
        pass

    def destroy(self):
        """

        :return:
        """
        pass

    ########################################
    #       PROPERTIES
    ########################################
    quizCache = property(__getQuizCache, __setQuizCache)
    studentFindResponseCache = property(__getStudentFindResponseCache, __setStudentFindResponseCache)
    userModelCache = property(__getUserModelCache, __setUserModelCache)
    longMessageCache = property(__getLongMessageCache, __setLongMessageCache)
    roomModelCache = property(__getRoomModelCache, __setRoomModelCache)
    roomListCache = property(__getRoomListCache, __setRoomListCache)
    activitySettingsCache = property(__getActivitySettingsCache, __setActivitySettingsCache)
