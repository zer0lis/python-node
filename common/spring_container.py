# coding=utf-8
import os
from abc import ABCMeta
import ast
import re
import logging
import importlib

logger = logging.getLogger(__name__)


class SpringContainer(object):
    """
    class to instantiate objects from a json config file and store them
    """

    REGEX = re.compile(r'{{.[\ ]*(?P<key>.[A-Za-z0-9_]*).[\ ]*}}')

    CYCLE = {}

    def __init__(self, jsonFileName, propertyFileName):
        """
        constructor
        :param jsonFileName:
        :param propertyFileName:
        :return:
        """

        if os.path.exists(jsonFileName) is False:
            raise Exception("File %s was not found" % jsonFileName)

        if os.path.exists(propertyFileName) is False:
            raise Exception("File %s was not found" % propertyFileName)

        self.__jsonTemplate = jsonFileName
        self.__propertyFile = propertyFileName

        # load the json in the property file
        with open(self.__propertyFile) as f:
            self.__values = ast.literal_eval(f.read())

        self.__keys = []
        with open(self.__jsonTemplate) as f:
            self.__template = f.read()
            result = self.REGEX.findall(self.__template)

            if result:
                self.__keys = result
                missing = list(set(self.__keys)-set(self.__values.keys()))
                if missing:
                    raise Exception("The following keys are missing from the property file %s: %s" %
                                    (propertyFileName, str(missing)))
                extra = list(set(self.__values.keys()) - set(self.__keys))
                if extra:
                    logger.info("There are extra keys in the property file %s which are not used anymore: %s" %
                                (propertyFileName, str(extra)))

        # fill in the gaps in the template
        for k in self.__keys:
            self.__template = self.__template.replace("{{ %s }}" % k, str(self.__values[k]))

        self.objJson = ast.literal_eval(self.__template)

        self.__objectStore = dict()
        for obj in self.objJson:
            self.__constructObj(obj)
            self.CYCLE[obj["name"]] = 0

        self.CYCLE = {}

    def __findAndConstructObj(self, name):
        """
        find the name in the json template and create it
        :param name:
        :return:
        """
        if name in self.__objectStore:
            return self.__objectStore[name]

        for obj in self.objJson:
            if obj["name"] == name:
                self.__constructObj(obj)
                break

    def __constructObj(self, obj):
        """
        create the objects
        :return:
        """
        if max(self.CYCLE.values() or (0, 0)) > 2:
            raise Exception("There is a cyclic dependency for object %s" % str(obj))
        if "name" not in obj:
            raise Exception("name is missing in the json template for object %s" % str(obj))

        if obj["name"] in self.CYCLE:
            self.CYCLE[obj["name"]] += 1
        else:
            self.CYCLE[obj["name"]] = 1

        if "class" not in obj:
            raise Exception("the class is missing for the object in json template: %s" % str(obj))
        if "constructor-args" in obj:
            for ctr in obj["constructor-args"]:
                if ctr["type"] == "ref":
                    if type(ctr["value"]) is not list:
                        self.__findAndConstructObj(ctr["value"])
                    else:
                        for name in ctr["value"]:
                            self.__findAndConstructObj(name)

        if "properties" in obj:
            for prop in obj["properties"]:
                if prop["type"] == "ref":
                    if type(prop["value"]) is not list:
                        self.__findAndConstructObj(prop["value"])
                    else:
                        for name in prop["value"]:
                            self.__findAndConstructObj(name)

        ctrDict = dict()
        for ctr in obj["constructor-args"]:
            if ctr["type"] == "val" or type(ctr["value"]) is not list:
                ctrDict[ctr["name"]] = ctr["value"] if ctr["type"] == "val" else self.__objectStore[ctr["value"]]
            else:
                ctrDict[ctr["name"]] = list()
                for name in ctr["value"]:
                    ctrDict[ctr["name"]].append(self.__objectStore[name])

        module_name, class_name = obj["class"].rsplit(".", 1)
        objInstance = getattr(importlib.import_module(module_name), class_name)(**ctrDict)
        logger.info("Object name %s of class %s created" % (obj["name"], class_name))

        if objInstance is None:
            raise Exception("Failed to create object %s" % str(obj))
        for prop in obj["properties"]:
            val = prop["value"] if prop["type"] == "val" else self.__objectStore[prop["value"]]
            setattr(objInstance, prop["name"], val)

        if "properties" in obj:
            objInstance.after_properties_set()

        self.__objectStore[obj["name"]] = objInstance

    def getItem(self, name):
        """
        returns an object from the store
        :param name:
        :return:
        """
        return self.__objectStore.get(name)

    def shutdown_hook(self):
        """
        called at exit
        :return:
        """

        keys = self.__objectStore.keys()

        for k in keys:
            obj = self.__objectStore.pop(k)
            obj.destroy()
            logger.info("Destroyed %s" % k)
            del obj
            logger.info("Deleted object %s" % k)

        del self


class SpringItem(object):
    """
    base class for objects to be stored in config files
    """

    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    def destroy(self):
        """
        can be implemented by child class
        :return:
        """
        pass

    def after_properties_set(self):
        """
        called after the properties of the object are set
        :return:
        """
        pass