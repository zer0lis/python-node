# coding=utf-8
from django.conf.urls import url

from common.views import ReportListAPIView, TeacherReportAPIView, PurgeReportsAPIView, JoinRoomByCodeView
from common.views import PingView, S3ImageDataView
from socrative_users.views import ImportQuizForUserView

urlpatterns = [
    url(r'^rc/(?P<room_code>[0-9a-zA-Z]{4,6})/?$', JoinRoomByCodeView.as_view()),
    url(r'^activities/api/reports/$', ReportListAPIView.as_view()),
    url(r'^activities/api/reports/purge/', PurgeReportsAPIView.as_view()),
    url(r'^activities/api/report/(?P<activity_instance_id>[0-9]+)/?$', TeacherReportAPIView.as_view()),

    url(r'^api/ping/?$', PingView.as_view()),
    url(r'^media/api/get-s3-upload-data/?$', S3ImageDataView.as_view()),
    url(r'^import-quiz/(?P<soc_number>.+)/?$', ImportQuizForUserView.as_view()),
    ]
