# -*- coding: utf-8 -*-
from common import http_status as status
from socrative import settings as projSettings
from common.base_dao import BaseDao
from common.socrative_errors import BaseError
from common.socrative_api import exceptionStack, isStringNotEmpty
from common.base_services import BaseService
from common.serializers import FullActivityReportDTO, ReportListDTO
from common.socrative_config import ConfigRegistry
from common.dao import ActivityInstanceModel
from cryptography import Cryptography
import pytz
import logging
logger = logging.getLogger(__name__)

import uuid
import base64
import hmac
import hashlib
import datetime


class CommonServices(BaseService):
    """
    Service class for common
    """

    MAX_RETURNED_RECORDS_LIMIT = 50
    ACTIVE = "active"
    ARCHIVED = "archived"
    TRASHED = "trashed"

    def __init__(self, daoRegistry, cacheRegistry):
        """
        Constructor
        :param daoRegistry:
        :return:
        """
        super(CommonServices, self).__init__(daoRegistry, cacheRegistry)

        self.__allowedS3ContentTypes = (
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.ms-excel",
            "application/octet-stream",
            "application/excel",
            "application/x-excel",
            "application/x-msexcel"
            "text/plain",
            "text/csv"
        )


    def getReportList(self, request, dto):
        """
        handler for HTTP GET
        :param request:
        :param dto:
        :return:
        """

        try:

            if dto.limit <= 0 or dto.limit > self.MAX_RETURNED_RECORDS_LIMIT:
                return BaseError.INVALID_LIMIT, status.HTTP_400_BAD_REQUEST

            if dto.offset < 0:
                return BaseError.INVALID_OFFSET, status.HTTP_400_BAD_REQUEST

            if dto.state not in (None, self.ACTIVE, self.ARCHIVED, self.TRASHED):
                return BaseError.INVALID_STATE, status.HTTP_400_BAD_REQUEST

            if dto.state is None and dto.room_name is None and dto.deleted_rooms is None and dto.terms is None:
                return BaseError.NO_FILTERS_SENT, status.HTTP_400_BAD_REQUEST

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if dto.state == self.ACTIVE:
                state = ActivityInstanceModel.ACTIVE
            elif dto.state == self.ARCHIVED:
                state = ActivityInstanceModel.ARCHIVED
            elif dto.state == self.TRASHED:
                state = ActivityInstanceModel.TRASHED
            elif dto.state is None:
                state = None
            else:
                return BaseError.INVALID_STATE, status.HTTP_400_BAD_REQUEST

            roomName = None
            if dto.room_name is not None:
                roomName = dto.room_name

            notInRooms = []

            valid_rooms = self.daoRegistry.roomDao.getAllActiveRooms(user.id)

            if dto.deleted_rooms:
                notInRooms = valid_rooms

            activityMap = self.daoRegistry.activityInstanceDao.getFilteredActivityIds(user.id, state, dto.offset,
                                                                                      dto.limit, dto.terms,
                                                                                      roomName=roomName,
                                                                                      notInRooms=notInRooms)

            metadata = activityMap.pop("metadata")
            roomData = metadata.pop("rooms")
            total_results = activityMap.pop("total_results")
            meta = dict(archived=0, active=0, trashed=0)
            for k in metadata:
                if k == ActivityInstanceModel.ACTIVE or k is None:
                    meta["active"] += metadata[k]
                elif k == ActivityInstanceModel.ARCHIVED:
                    meta["archived"] += metadata[k]
                elif k == ActivityInstanceModel.TRASHED:
                    meta["trashed"] += metadata[k]

            meta["rooms"] = dict()
            meta["rooms"]["deleted_rooms"] = 0
            for k in roomData:
                if k in valid_rooms:
                    meta["rooms"][k] = roomData[k]
                else:
                    meta["rooms"]["deleted_rooms"] += roomData[k]

            # valid rooms that are not returned have 0 reports
            for room in valid_rooms:
                if room not in roomData:
                    meta["rooms"][room] = 0

            quiz_ids = [value[0] for value in activityMap.values()]
            standards = self.daoRegistry.standardsDao.getStandardNames(list(set(quiz_ids)))

            resp = []
            now = pytz.utc.localize(datetime.datetime.utcnow())
            for _id in activityMap:
                respItem = dict()
                respItem["id"] = _id
                respItem["activity_id"] = activityMap[_id][0]
                respItem["room"] = activityMap[_id][6].lower()
                respItem["name"] = activityMap[_id][2]
                respItem["start_time"] = activityMap[_id][3]
                if activityMap[_id][0] in standards:
                    respItem["standard"] = standards[activityMap[_id][0]]
                else:
                    respItem["standard"] = None

                activity_type = activityMap[_id][1]
                if activity_type == ActivityInstanceModel.SPACE_RACE:
                    _type = "space_race"
                elif activity_type == ActivityInstanceModel.QUIZ:
                    if respItem["name"].startswith("Exit Ticket Quiz"):
                        _type = "exit_ticket"
                    else:
                        _type = "quiz"
                elif activity_type == ActivityInstanceModel.SINGLE_QUESTION:
                    _type = "short_answer"
                    respItem["name"] = activityMap[_id][7]
                else:
                    logger.error("Bad activity type :%s" % activity_type)
                    continue
                respItem["activity_type"] = _type
                respItem["last_updated"] = activityMap[_id][5] if activityMap[_id][5] else activityMap[_id][4]
                if state == ActivityInstanceModel.TRASHED:
                    remaining_days = (respItem["last_updated"] - now).days + 31
                    respItem["days_remaining"] = remaining_days if remaining_days >= 0 else 0

                resp.append(respItem)

            responseList = sorted(resp, key=lambda x: x["last_updated"], reverse=True)
            obj = dict()
            obj["metadata"] = meta
            obj["reports"] = responseList
            obj["total_results"] = total_results

            return ReportListDTO.fromSocrativeDict(obj), status.HTTP_200_OK
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def getActivityById(self, request, dto):
        """
        handler for HTTP POST
        :param request
        :param dto
        :return:
        """

        try:

            if dto.activity_instance_id < 0:
                return BaseError.INVALID_ACTIVITY_ID, status.HTTP_400_BAD_REQUEST

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            activity_instance = self.daoRegistry.activityInstanceDao.loadDeepActivity(dto.auth_token,
                                                                                      dto.activity_instance_id)

            if activity_instance["state"] == ActivityInstanceModel.ACTIVE:
                activity_instance["state"] = "active"
            elif activity_instance["state"] == ActivityInstanceModel.ARCHIVED:
                activity_instance["state"] = "archived"
            elif activity_instance["state"] == ActivityInstanceModel.TRASHED:
                activity_instance["state"] = "trashed"

            rostered_activity = self.daoRegistry.studentActivityDao.checkRostered(activity_instance["id"])

            if rostered_activity:
                # add the presence field
                activity_instance["student_names"] = self.daoRegistry.studentActivityDao.getPresence(dto.activity_instance_id)

            data = FullActivityReportDTO.fromSocrativeDict(activity_instance)

            return data, status.HTTP_200_OK
        except BaseDao.NotFound:
            return BaseError.ACTIVITY_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def purgeReports(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if not dto.activity_instance_ids:
                return BaseError.ACTIVITY_INSTANCE_IDS_MISSING, status.HTTP_400_BAD_REQUEST

            aids = self.daoRegistry.activityInstanceDao.getTrashedIds(user.id, dto.activity_instance_ids)

            # return user_uuid and aids pair to remove them from cache
            cache_tuples = self.daoRegistry.activityInstanceDao.purgeReports(aids)

            self.cacheRegistry.studentFindResponseCache.invalidateMulti(cache_tuples)

            return {}, status.HTTP_200_OK
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def updateReport(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            if dto.activity_instance_id < 0:
                return BaseError.INVALID_ACTIVITY_ID, status.HTTP_400_BAD_REQUEST

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if dto.state not in ("active", "archived", "trashed"):
                return BaseError.INVALID_STATE, status.HTTP_400_BAD_REQUEST

            state = ActivityInstanceModel.ACTIVE

            if dto.state == "archived":
                state = ActivityInstanceModel.ARCHIVED
            elif dto.state == "trashed":
                state = ActivityInstanceModel.TRASHED

            self.daoRegistry.activityInstanceDao.updateState(user.id, dto.activity_instance_id, state)

            return {}, status.HTTP_200_OK
        except BaseDao.NotUpdated:
            return BaseError.ACTIVITY_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def updateReportList(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if dto.state not in ("active", "archived", "trashed"):
                return BaseError.INVALID_STATE, status.HTTP_400_BAD_REQUEST

            state = ActivityInstanceModel.ACTIVE

            if dto.state == "archived":
                state = ActivityInstanceModel.ARCHIVED
            elif dto.state == "trashed":
                state = ActivityInstanceModel.TRASHED

            self.daoRegistry.activityInstanceDao.updateStates(user.id, dto.activity_instance_ids, state)

            return {}, status.HTTP_200_OK
        except BaseDao.NotUpdated:
            return BaseError.ACTIVITY_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def __signForS3(self, key, msg):
        """

        :param key:
        :param msg:
        :return:
        """
        return hmac.new(key, msg.encode("utf-8"), hashlib.sha256).digest()

    def __getSigningKeyForS3(self, key, dateStamp, regionName, serviceName):
        """

        :param key:
        :param dateStamp:
        :param regionName:
        :param serviceName:
        :return:
        """
        kDate = self.__signForS3(("AWS4" + key).encode("utf-8"), dateStamp)
        kRegion = self.__signForS3(kDate, regionName)
        kService = self.__signForS3(kRegion, serviceName)
        kSigning = self.__signForS3(kService, "aws4_request")
        return kSigning

    def getS3ImageData(self, request, dataDict):
        """

        :param request:
        :param dataDict
        :return:
        """
        # Create the properties needed for the policy
        expiration = datetime.datetime.utcnow() + datetime.timedelta(minutes=2)
        expirationString = expiration.isoformat() + 'Z'
        uploadDate = expiration.strftime('%Y%m%d')
        amzDate = expiration.strftime('%Y%m%dT%H%M%SZ')
        key = str(uuid.uuid4())
        accessKeyId = ConfigRegistry().getItem("AWS_UPLOAD_ACCESS_KEY_ID")
        content_type = dataDict.get("ct")

        contentTypeRule = """["starts-with", "$Content-Type", "image/"],"""
        contentLengthRule = """["content-length-range", 1, 52428800],"""  # 50MB for images
        if content_type:
            if content_type in self.__allowedS3ContentTypes:
                contentTypeRule = """["eq", "$Content-Type", "%s"],""" % content_type
                contentLengthRule = """["content-length-range", 30, 2097152],"""  # limit to 2MB if it's xls

        # Create the policy and base64 encode it as the string to sign. Note the file size restriction of 50 MB.
        policy = """{{
            "expiration": "{0}",
            "conditions": [
                {{"acl": "public-read"}},
                {{"bucket": "socrative"}},""" + contentLengthRule +\
                 """{{"key": "{1}"}},""" + contentTypeRule +\
                 """{{"x-amz-algorithm": "AWS4-HMAC-SHA256"}},
                {{"x-amz-credential": "{2}/{3}/us-east-1/s3/aws4_request"}},
                {{"x-amz-date": "{4}"}}
            ]
        }}"""
        policy = policy.format(expirationString, key, accessKeyId, uploadDate, amzDate)
        stringToSign = base64.b64encode(policy)

        # Get the signing key and use it to create the signature
        secretAccessKey = ConfigRegistry().getItem("AWS_UPLOAD_SECRET_ACCESS_KEY")
        signingKey = self.__getSigningKeyForS3(secretAccessKey, uploadDate, 'us-east-1', 's3')
        signature = hmac.new(signingKey, (stringToSign).encode('utf-8'), hashlib.sha256).hexdigest()

        return {
            'key': key,
            'policy': stringToSign,
            'access': accessKeyId,
            'uploadDate': uploadDate,
            'amzDate': amzDate,
            'signature': signature
        }

    def joinRoomByCode(self, request, dataDict):
        """
        redirect the url for joining a room
        :param request:
        :param dataDict:
        :return:
        """

        url = "https://" if request.is_secure() else "http://"
        url += ConfigRegistry.getItem("FRONTEND_HOST")

        try:

            room_code = dataDict.get("room_code")

            if not isStringNotEmpty(room_code):
                url += "/login/student/#state=error&code=%d" % BaseError.INVALID_ROOM_CODE["error"]["code"]
                return url, None

            room_name = self.daoRegistry.roomCodeDao.getRoomName(room_code)

            if not room_name:
                url += "/login/student/#state=error&code=%d" % BaseError.ROOM_NOT_FOUND["error"]["code"]
                return url, None

            room_model = self.serviceRegistry.roomService.validateRoom(room_name)
            if not room_model:
                url += "/login/student/#state=error&code=%d" % BaseError.ROOM_NOT_FOUND["error"]["code"]
                return url, None

            url += "/login/student/#join-room/" + room_model.name_lower

            uuid = "anon" + Cryptography.generate_auth_token()[:16]
            cookie = (projSettings.STUDENT_AUTH_TOKEN, Cryptography.encryptStudentAuth(uuid))

            return url, cookie

        except Exception as e:
            logger.error(exceptionStack(e))
            url += "/login/student/#state=error&code=%d" % BaseError.INTERNAL_SERVER_ERROR["error"]["code"]
            return url, None