# -*- coding: utf-8 -*-

from django.core.mail.message import sanitize_address
from django.utils.encoding import force_bytes
from time import sleep
import logging
import time
from boto.ses.connection import BotoServerError
from boto.ses import connect_to_region
from common.spring_container import SpringItem
from common.counters import DatadogThreadStats

from common.socrative_api import exceptionStack
from common.counters import EmailCounters

logger = logging.getLogger(__name__)
statsd = DatadogThreadStats.STATS

MAX_NUMBER_OF_RETRIES = 3


class EmailClient(SpringItem):
    """

    """

    def __init__(self, username, password, region):
        """
        """

        super(EmailClient, self).__init__()
        self.connection = None
        self.aws_access_key = username
        self.aws_secret_key = password
        self.region = region

        if self.aws_access_key is None or self.aws_secret_key is None:
            raise Exception("username and password are params are missing. Check the configuration")

        logger.info("email_client:Email Client created")

    @statsd.timed(EmailCounters.EMAILS_SENT)
    def sendEmail(self, emailObj):
        """
        sending emails synchronously
        """

        if not emailObj.recipients():
            logger.warn("email_client:the email has no recipients : %s\nskipping sending email" % str(emailObj))
            return

        try:

            logger.info("email_client:preparing mail for sending")
            from_email = sanitize_address(emailObj.from_email, emailObj.encoding)
            recipients = [sanitize_address(addr, emailObj.encoding) for addr in emailObj.recipients()]
            message = emailObj.message()
            charset = message.get_charset().get_output_charset() if message.get_charset() else 'utf-8'
        except Exception as e:
            statsd.increment(EmailCounters.EMAIL_SENT_FAIL)
            logger.error(exceptionStack(e))
            return

        retries = 0
        while retries < MAX_NUMBER_OF_RETRIES:
            try:
                logger.debug("email_thread: before checking the connection")
                if self.connection is None:
                    start = time.time()
                    logger.warn("email_thread: connection is None, we have to re-connect. Timeout 30 seconds")
                    self.connection = connect_to_region(self.region, aws_access_key_id=self.aws_access_key,
                                                        aws_secret_access_key=self.aws_secret_key)
                    statsd.increment(EmailCounters.CONNECTIONS)

                    logger.debug("email_thread: connection established. time spent %f. Logging in" %
                                 (time.time() - start))

                start = time.time()
                logger.debug("email_thread: before sendmail towards %s", emailObj.recipients())
                retVal = self.connection.send_raw_email(force_bytes(message.as_string(), charset), from_email, recipients)

                if retVal.get("SendRawEmailResponse", {}).get("SendRawEmailResult", {}).get("MessageId"):
                    logger.info("email_thread:mail sent successfully to %s. Time spent %f. Return value %s",
                                 emailObj.recipients(), time.time()-start,
                                 retVal.get("SendRawEmailResponse", {}).get("SendRawEmailResult", {}).get("MessageId"))
                    statsd.increment(EmailCounters.EMAIL_SENT_OK)
                    break
                else:
                    logger.error("email_thread: error happened  for recipients %s: %s",
                                 str(emailObj.recipients()), retVal)
                    retries += 1
                    statsd.increment(EmailCounters.EMAIL_RETRIES)
                    sleep(0.1)
            except BotoServerError as e:
                logger.error(exceptionStack(e))
                break
            except Exception as e:
                logger.error("email_thread:Unexpected error occured: %s" + exceptionStack(e))
                self.connection = None
                statsd.increment(EmailCounters.EMAIL_CONNECTION_FAILED)
                retries += 1
                sleep(0.1)

        if retries >= MAX_NUMBER_OF_RETRIES:
            statsd.increment(EmailCounters.EMAIL_SENT_FAIL)