# -*- coding: utf-8 -*-
from students.serializers import StudentName as StudentNameDTO
from students.serializers import StudentResponse as StudentResponseDTO
from common import base_limits
from common.base_dto import DTOConverter, ItemConverter
from common.base_dto import RequestDTOValidator, ItemValidator
from common.socrative_api import isString, isStringNotEmpty, safeToInt, isNotNone, room_name_validator, serializerToBool
from common.socrative_errors import BaseError
from string import strip, lower
from common import http_status as status


class ActivitySetting(DTOConverter):
    """
    activity setting serializer
    """
    stringifyDict = {
        "key": ItemConverter(),
        "value": ItemConverter()
    }


class MediaResourceDTO(DTOConverter):
    """
    serializer for media resource
    """
    stringifyDict = {
        "id": ItemConverter(),
        "type": ItemConverter(),
        "url": ItemConverter(),
        "data": ItemConverter(optional=False, default="{}")
    }


class StudentActivityInstance(DTOConverter):
    """
    serializer for student activity
    """
    stringifyDict = {
        "id": ItemConverter(),
        "start_time": ItemConverter(),
        "end_time": ItemConverter(),
        "activity_type": ItemConverter(),
        "activity_id": ItemConverter(),
        "activity_state": ItemConverter(),
        "started_by_id": ItemConverter(dtoName="started_by"),
        "room_name": ItemConverter(),
        "settings": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=ActivitySetting)
    }


class ActivityAditionalInfo(DTOConverter):
    """
    serializer for question
    """
    stringifyDict = {
        "question_text": ItemConverter(),
        "type": ItemConverter()
    }


class ReportListMetadataDTO(DTOConverter):
    """
    serializer for question
    """
    stringifyDict = {
        "active": ItemConverter(),
        "trashed": ItemConverter(),
        "archived": ItemConverter()
    }


class FullActivityReportDTO(DTOConverter):
    """
    serializer for student
    """
    stringifyDict = {
        "id": ItemConverter(),
        "start_time": ItemConverter(),
        "end_time": ItemConverter(),
        "activity_type": ItemConverter(),
        "activity_id": ItemConverter(),
        "activity_state": ItemConverter(),
        "settings": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=ActivitySetting),
        "student_names": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=StudentNameDTO),
        "hide_report": ItemConverter(),
        "students_finished": ItemConverter(),
        "responses": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=StudentResponseDTO),
        "name": ItemConverter(optional=True, default=""),
        "question": ItemConverter(itemClass=ActivityAditionalInfo),
        "state": ItemConverter(),
        "last_updated": ItemConverter(),

    }


class SimpleActivityReportDTO(DTOConverter):
    """
    serializer for student
    """
    stringifyDict = {
        "id": ItemConverter(),
        "start_time": ItemConverter(),
        "activity_type": ItemConverter(),
        "activity_id": ItemConverter(),
        "hide_report": ItemConverter(),
        "name": ItemConverter(optional=True, default=""),
        "finished": ItemConverter(optional=True),
        "question": ItemConverter(itemClass=ActivityAditionalInfo)
    }


class ReportItemDTO(DTOConverter):
    """
    serializer for student
    """
    stringifyDict = {
        "id": ItemConverter(),
        "start_time": ItemConverter(),
        "activity_type": ItemConverter(),
        "activity_id": ItemConverter(),
        "last_updated": ItemConverter(),
        "name": ItemConverter(optional=True, default=""),
        "standard": ItemConverter(),
        "room": ItemConverter(),
        "days_remaining": ItemConverter(optional=True)
    }


class ReportListDTO(DTOConverter):
    """

    """
    stringifyDict = {
        "metadata": ItemConverter(itemClass=ReportListMetadataDTO),
        "reports": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=ReportItemDTO),
        "total_results": ItemConverter()
    }


class StartActivityInstanceDTO(DTOConverter):
    stringifyDict = {
        "started_by_id": ItemConverter(dtoName="started_by"),
        "settings": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=ActivitySetting, default=[]),
        "student_names": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=StudentNameDTO, default=[]),
        "responses": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=StudentResponseDTO, default=[]),
        "id": ItemConverter(),
        "activity_type": ItemConverter(),
        "activity_id": ItemConverter(),
        "activity_state": ItemConverter(),
        "start_time": ItemConverter(),
    }


class StudentStartActivityInstanceDTO(DTOConverter):
    stringifyDict = {
        "started_by_id": ItemConverter(dtoName="started_by"),
        "settings": ItemConverter(objType=ItemConverter.ITERATOR, itemClass=ActivitySetting, default=[]),
        "id": ItemConverter(),
        "activity_type": ItemConverter(),
        "activity_id": ItemConverter(),
        "activity_state": ItemConverter(),
    }


class GetReportListDTORequest(RequestDTOValidator):
    """
    validator for the request data
    """

    rulesDict = {
        "auth_token": ItemValidator(funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "terms": ItemValidator(mandatory=False, funcList=[isString, strip, lower, isStringNotEmpty],
                               funcLimits=[(len, base_limits.MAX_REPORTS_TERMS,
                                            (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST))],
                               errorCode=(BaseError.INVALID_TERMS_FIELD, status.HTTP_400_BAD_REQUEST)),
        "state": ItemValidator(mandatory=False, funcList=[isString, strip, lower, isStringNotEmpty],
                               errorCode=(BaseError.INVALID_STATE_FIELD, status.HTTP_400_BAD_REQUEST)),
        "room_name": ItemValidator(mandatory=False, funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.INVALID_ROOM_NAME, status.HTTP_400_BAD_REQUEST)),
        "deleted_rooms": ItemValidator(mandatory=False, funcList=[serializerToBool, isNotNone],
                                       errorCode=(BaseError.INVALID_DELETED_ROOMS, status.HTTP_400_BAD_REQUEST)),
        "offset": ItemValidator(funcList=[safeToInt, isNotNone],
                                errorCode=(BaseError.INVALID_OFFSET, status.HTTP_400_BAD_REQUEST)),
        "limit": ItemValidator(funcList=[safeToInt, isNotNone],
                               errorCode=(BaseError.INVALID_LIMIT, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.auth_token = None
        self.terms = None
        self.state = None
        self.offset = None
        self.limit = None
        self.deleted_rooms = None
        self.room_name = None


class GetReportDTORequest(RequestDTOValidator):
    """
    validator for the request data
    """

    rulesDict = {
        "auth_token": ItemValidator(funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "activity_instance_id": ItemValidator(funcList=[safeToInt, isNotNone],
                                              errorCode=(BaseError.INVALID_ACTIVITY_ID, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        :return:
        """
        self.auth_token = None
        self.activity_instance_id = None


class UpdateReportListDTORequest(RequestDTOValidator):
    rulesDict = {
        "auth_token": ItemValidator(funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "activity_instance_ids": ItemValidator(iterator=ItemValidator.LIST,
                                               funcList=[safeToInt, isNotNone],
                                               errorCode=(BaseError.INVALID_ACTIVITY_IDS, status.HTTP_400_BAD_REQUEST)),
        "state": ItemValidator(funcList=[isString, strip, lower, isStringNotEmpty],
                               errorCode=(BaseError.INVALID_STATE, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        """
        self.auth_token = None
        self.activity_instance_ids = None
        self.state = None


class PurgeReportsDTORequest(RequestDTOValidator):
    rulesDict = {
        "auth_token": ItemValidator(funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST)),
        "activity_instance_ids": ItemValidator(iterator=ItemValidator.LIST,
                                               funcList=[safeToInt, isNotNone],
                                               errorCode=(BaseError.INVALID_ACTIVITY_IDS, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        """
        self.auth_token = None
        self.activity_instance_ids = None


class UpdateReportDTORequest(GetReportDTORequest):

    rulesDict = dict(GetReportDTORequest.rulesDict)
    rulesDict.update({
        "state": ItemValidator(funcList=[isString, strip, lower, isStringNotEmpty],
                               errorCode=(BaseError.INVALID_STATE, status.HTTP_400_BAD_REQUEST))
    })

    def __init__(self):
        """
        constructor
        """
        super(UpdateReportDTORequest, self).__init__()

        self.state = None


class TrashReportDTORequest(GetReportDTORequest):
    """

    """

    def __init__(self):
        """

        """
        super(TrashReportDTORequest, self).__init__()


class AuthenticatedRequestDTO(RequestDTOValidator):
    rulesDict = {
        "auth_token": ItemValidator(funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST))
    }

    def __init__(self):
        """

        """

        super(AuthenticatedRequestDTO, self).__init__()

        self.auth_token = None
