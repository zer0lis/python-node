# coding=utf-8
from common.base_dto import RequestDTOValidator, ItemValidator
from common.socrative_api import isStringNotEmpty, serializerToBool, isNotNone, isBoolOrNone, safeToInt, gteZero
from common.socrative_api import isString, room_name_validator
from common import base_limits
from common.socrative_errors import BaseError
from common import http_status as status
from string import lower, strip


class AuthRequestDTO(RequestDTOValidator):
    """

    """
    rulesDict = {
        "auth_token": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.AUTH_TOKEN_MISSING,
                                                                            status.HTTP_400_BAD_REQUEST),
                                    funcLimits=[(len, base_limits.AUTH_TOKEN_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                     status.HTTP_400_BAD_REQUEST))])
    }

    def __init__(self):
        """

        """
        self.auth_token = None


class BaseActivityRequestDTO(AuthRequestDTO):
    """
    base serializer for take quiz
    """
    rulesDict = AuthRequestDTO.rulesDict.copy()

    rulesDict.update({
        "room_name": ItemValidator(funcList=[isStringNotEmpty, lower], errorCode=(BaseError.ROOM_NAME_MISSING,
                                                                                  status.HTTP_400_BAD_REQUEST),
                                   funcLimits=[(len, base_limits.ROOM_NAME_LIMIT, (BaseError.STRING_TOO_LONG,
                                                                                   status.HTTP_400_BAD_REQUEST))]),
        "pacing": ItemValidator(mandatory=False, funcList=[isStringNotEmpty], defaultValue='teacher',
                                errorCode=(), funcLimits=[(len, base_limits.QUIZ_SETTINGS_STRING_LIMIT,
                                                           (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST))]),
        "allow_student_nav": ItemValidator(mandatory=False, funcList=[serializerToBool, isNotNone, str],
                                           defaultValue='False',
                                           errorCode=()),
        "one_attempt": ItemValidator(mandatory=False, funcList=[serializerToBool, isBoolOrNone, str],
                                     defaultValue="True", errorCode=())

    })

    def __init__(self):
        """

        """
        super(BaseActivityRequestDTO, self).__init__()

        self.room_name = None
        self.pacing = None
        self.allow_student_nav = None
        self.one_attempt = None


class BaseQuizActivityRequestDTO(BaseActivityRequestDTO):
    """
    base serializer for take quiz
    """
    rulesDict = BaseActivityRequestDTO.rulesDict.copy()

    rulesDict.update({
        "require_names": ItemValidator(mandatory=False, defaultValue='True',
                                       funcList=[serializerToBool, isBoolOrNone, str],
                                       errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST)),
        "random_answers": ItemValidator(mandatory=False, funcList=[serializerToBool, isBoolOrNone, str],
                                        defaultValue='False',
                                        errorCode=()),
        "random_questions": ItemValidator(mandatory=False, funcList=[serializerToBool, isBoolOrNone, str],
                                          defaultValue='False',
                                          errorCode=()),
        "show_feedback": ItemValidator(mandatory=False, funcList=[serializerToBool, isBoolOrNone, str],
                                       defaultValue='True',
                                       errorCode=()),
        "show_results": ItemValidator(mandatory=False, funcList=[serializerToBool, isBoolOrNone, str],
                                      defaultValue='False',
                                      errorCode=()),
        "soc_num": ItemValidator(mandatory=False, funcList=[isStringNotEmpty], defaultValue=None,
                                 errorCode=(BaseError.INVALID_SOC_NUMBER, status.HTTP_400_BAD_REQUEST),
                                 funcLimits=[(len, base_limits.QUIZ_SETTINGS_STRING_LIMIT,
                                              (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST))])
    })

    def __init__(self):
        """

        """
        super(BaseQuizActivityRequestDTO, self).__init__()

        self.require_names = None
        self.random_answers = None
        self.random_questions = None
        self.show_feedback = None
        self.show_results = None
        self.soc_num = None


class StartQuickQuestionRequestDTO(BaseActivityRequestDTO):
    """
    validates the request for starting a quick question
    """
    rulesDict = BaseActivityRequestDTO.rulesDict.copy()
    rulesDict.update({
        "question_type": ItemValidator(funcList=[isStringNotEmpty], errorCode=(BaseError.QUESTION_TYPE_MISSING,
                                                                               status.HTTP_400_BAD_REQUEST),
                                       funcLimits=[(len, base_limits.QUESTION_TYPE_LIMIT,
                                                    (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST))]),
        "question_text": ItemValidator(mandatory=False, defaultValue="",
                                       funcLimits=[(len, base_limits.QUESTION_TEXT_LIMIT,
                                                    (BaseError.STRING_TOO_LONG, status.HTTP_400_BAD_REQUEST))]),

        "show_feedback": ItemValidator(mandatory=False, funcList=[serializerToBool, isBoolOrNone, str],
                                       defaultValue="False", errorCode=()),
        "allow_repeat_responses": ItemValidator(mandatory=False, funcList=[serializerToBool, isBoolOrNone, str],
                                                defaultValue=None, errorCode=()),
        "require_names": ItemValidator(mandatory=False, defaultValue=None,
                                       funcList=[serializerToBool, isBoolOrNone, str],
                                       errorCode=(BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST)),

    })

    def __init__(self):
        """

        :return:
        """
        super(StartQuickQuestionRequestDTO, self).__init__()

        self.question_type = None
        self.question_text = ""
        self.show_feedback = None
        self.allow_repeat_responses = None
        self.require_names = None


class StartQuizRequestDTO(BaseQuizActivityRequestDTO):

    rulesDict = BaseQuizActivityRequestDTO.rulesDict.copy()

    def __init__(self):
        """

        :return:
        """
        super(StartQuizRequestDTO, self).__init__()


class StartExitTicketRequestDTO(StartQuizRequestDTO):
    """
    validates the request for starting a quick question
    """

    def __init__(self):
        """

        :return:
        """
        super(StartExitTicketRequestDTO, self).__init__()


class StartSpaceRaceRequestDTO(BaseQuizActivityRequestDTO):

    rulesDict = BaseQuizActivityRequestDTO.rulesDict.copy()
    rulesDict.update({


        "team_count": ItemValidator(mandatory=False, funcList=[safeToInt, isNotNone, str], defaultValue='2',
                                    errorCode=()),

        "team_assignment_type": ItemValidator(mandatory=False, funcList=[isStringNotEmpty], defaultValue='auto',
                                              errorCode=(), funcLimits=[(len, base_limits.QUIZ_SETTINGS_STRING_LIMIT,
                                                                         (BaseError.STRING_TOO_LONG,
                                                                          status.HTTP_400_BAD_REQUEST))]),

        "next_team_assignment": ItemValidator(mandatory=False, funcList=[safeToInt, isNotNone, str],
                                              defaultValue='0',
                                              errorCode=()),

        "icon_type": ItemValidator(mandatory=False, funcList=[isStringNotEmpty], defaultValue='default',
                                   errorCode=(), funcLimits=[(len, base_limits.QUIZ_SETTINGS_STRING_LIMIT,
                                                              (BaseError.STRING_TOO_LONG,
                                                               status.HTTP_400_BAD_REQUEST))]),

        "seconds": ItemValidator(mandatory=False, defaultValue=-1, funcList=[safeToInt, isNotNone, gteZero],
                                 errorCode=(BaseError.INVALID_DURATION, status.HTTP_400_BAD_REQUEST)),

    })

    def __init__(self):
        """

        :return:
        """
        super(StartSpaceRaceRequestDTO, self).__init__()

        self.team_count = None
        self.team_assignment_type = None
        self.next_team_assignment = None
        self.icon_type = None
        self.seconds = -1
        self.one_attempt = None


class ConvertQuickQuestionRequestDTO(BaseActivityRequestDTO):
    """
    validates the request for starting a quick question
    """
    rulesDict = BaseActivityRequestDTO.rulesDict.copy()
    rulesDict.update({
        "require_names": ItemValidator(mandatory=False, funcList=[serializerToBool, isBoolOrNone, str],
                                       defaultValue="True", errorCode=()),
        "show_feedback": ItemValidator(mandatory=False, funcList=[serializerToBool, isBoolOrNone, str],
                                       defaultValue="False", errorCode=()),
        "allow_repeat_responses": ItemValidator(mandatory=False, funcList=[serializerToBool, isBoolOrNone, str],
                                                defaultValue=None, errorCode=())

    })

    def __init__(self):
        """

        :return:
        """
        super(ConvertQuickQuestionRequestDTO, self).__init__()

        self.question_type = None
        self.question_text = ""
        self.require_names = None
        self.show_feedback = None
        self.allow_repeat_responses = None
        self.one_attempt = None


class DuplicateQuizRequestDTO(AuthRequestDTO):
    """
    validates the request for starting a quick question
    """
    rulesDict = AuthRequestDTO.rulesDict.copy()
    rulesDict.update({
        "soc_number": ItemValidator(funcList=[isString, strip, isStringNotEmpty],
                                    errorCode=(BaseError.SOC_NUMBER_IS_MISSING, status.HTTP_400_BAD_REQUEST)),
        "name": ItemValidator(mandatory=False, funcList=[isString, strip, isStringNotEmpty], defaultValue=None,
                              errorCode=(BaseError.QUIZ_NAME_MISSING, status.HTTP_400_BAD_REQUEST))

    })

    def __init__(self):
        """

        :return:
        """
        super(DuplicateQuizRequestDTO, self).__init__()

        self.soc_number = None
        self.name = None


class TeacherHandraiseRequestDTO(AuthRequestDTO):
    """

    """
    rulesDict = AuthRequestDTO.rulesDict.copy()
    rulesDict.update({
        "room_name": ItemValidator(mandatory=True, funcList=[isString, strip, lower, room_name_validator],
                                   errorCode=(BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST)),
        "active": ItemValidator(mandatory=True, funcList=[serializerToBool, isNotNone],
                                errorCode=(BaseError.INVALID_ACTIVE_FIELD, status.HTTP_400_BAD_REQUEST)),
        "id": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.INVALID_STUDENT_ID,
                                                                        status.HTTP_400_BAD_REQUEST)),

    })

    def __init__(self):
        """

        :return:
        """
        super(TeacherHandraiseRequestDTO, self).__init__()

        self.room_name = None
        self.active = None
        self.id = None


class PaymentRequestDTO(AuthRequestDTO):
    """

    """

    rulesDict = AuthRequestDTO.rulesDict.copy()
    rulesDict.update({
        "action": ItemValidator(funcList=[isString, strip, isStringNotEmpty], mandatory=False, defaultValue="email",
                                errorCode=(BaseError.INVALID_RECEIPT_ACTION, status.HTTP_400_BAD_REQUEST))
    })

    def __init__(self):
        """

        """
        super(PaymentRequestDTO, self).__init__()
        self.action = None


class GetScoreSettingsRequest(AuthRequestDTO):
    """

    """


class CreateScoreSettingsRequest(AuthRequestDTO):
    """

    """
    rulesDict = AuthRequestDTO.rulesDict.copy()
    rulesDict.update({
        "key": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.INVALID_GRADECAM_KEY,
                                                                         status.HTTP_400_BAD_REQUEST)),
        "next_line": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.INVALID_GRADECAM_NEXT_LINE,
                                                                               status.HTTP_400_BAD_REQUEST)),
        "speed": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.INVALID_GRADECAM_SPEED,
                                                                           status.HTTP_400_BAD_REQUEST)),
        "prefix": ItemValidator(funcList=[isString], errorCode=(BaseError.INVALID_GRADECAM_PREFIX,
                                                                status.HTTP_400_BAD_REQUEST)),
        "method": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.INVALID_GRADECAM_METHOD,
                                                                            status.HTTP_400_BAD_REQUEST))

    })

    def __init__(self):
        """

        :return:
        """
        super(CreateScoreSettingsRequest, self).__init__()

        self.key = None
        self.next_line = None
        self.speed = None
        self.prefix = None
        self.method = None


class UpdateScoreSettingsRequest(CreateScoreSettingsRequest):
    """

    """
    rulesDict = CreateScoreSettingsRequest.rulesDict.copy()
    rulesDict.update({
        "pk": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.INVALID_EXPORT_SCORE_ID,
                                                                        status.HTTP_400_BAD_REQUEST))

    })

    def __init__(self):
        """

        :return:
        """
        super(CreateScoreSettingsRequest, self).__init__()

        self.pk = None


class PostponeNotificationRequestDTO(AuthRequestDTO):
    """

    """
    rulesDict = AuthRequestDTO.rulesDict.copy()
    rulesDict.update({
        "id": ItemValidator(funcList=[safeToInt, isNotNone], errorCode=(BaseError.INVALID_NOTIFICATION_ID,
                                                                        status.HTTP_400_BAD_REQUEST))
    })

    def __init__(self):
        """

        :return:
        """
        super(PostponeNotificationRequestDTO, self).__init__()

        self.id = None
