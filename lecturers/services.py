# -*- coding: utf-8 -*-
import logging
import datetime
import pytz
import ujson
import ast

from django.utils.translation import ugettext
from django.utils import translation
from django.contrib.auth.hashers import PBKDF2PasswordHasher
from common.base_services import BaseService
from socrative import settings as projSettings
from django.http import HttpResponse

from common import soc_constants
from common import http_status as status
from socrative_tornado.publish import send_model
from common.socrative_errors import BaseError
from common.socrative_api import exceptionStack, isStringNotEmpty, isString, isInteger, htmlEscape, isEmailValid
from common.dao import PartnerModel, ActivityInstanceModel
from rooms.dao import RoomModel
from common.base_dao import BaseDao
from common.serializers import StartActivityInstanceDTO
from quizzes.dao import QuizModel, QuestionModel, AnswerModel
from quizzes.pdf_generator import ReceiptPdf
from common.serializers import FullActivityReportDTO, SimpleActivityReportDTO, StudentStartActivityInstanceDTO
from socrative_users.dao import SocrativeUserModel, ScoreSettingsModel
from lecturers.dao import NotificationModel
from students.dao import StudentModel
from socrative_users.serializers import User
from socrative.settings import LANGUAGES
from workers.task_types import TaskType, EmailTypes

logger = logging.getLogger(__name__)


class LecturerServices(BaseService):
    """
    service for lecturers api
    """

    MIN_PASSWORD_LENGTH = 8
    PBK_ITERATIONS = 12000

    def __init__(self, daoRegistry, cacheRegistry):
        """

        :param daoRegistry:
        :return:
        """
        super(LecturerServices, self).__init__(daoRegistry, cacheRegistry)

        self.passwordHasher = PBKDF2PasswordHasher()

    def getProfileInfo(self, request, dataDict):
        """

        :param request:
        :param dataDict:
        :return:
        """
        try:
            context = dict()
            auth_token = dataDict.get("auth_token")

            userModel = self.serviceRegistry.userService.validateUser(auth_token)
            if userModel is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            user = userModel.toDict()

            isMCUser = self.daoRegistry.partnerDao.checkPartner(user["id"], PartnerModel.MASTERY_CONNECT)

            context["mastery_connect"] = isMCUser

            if type(user["school_data"]) in [str, unicode]:
                try:
                    user["school_data"] = ast.literal_eval(user["school_data"])
                except Exception:
                    pass

            context.update(user)

            if context["user_role"] is not None:
                context["role"] = ujson.loads(context.get("user_role"))
            else:
                context["role"] = []

            context["list_of_languages"] = {l[0]: l[1] for l in LANGUAGES}
            context["org_name"] = user.get("university", "")

            #
            context["org_id"] = user["school_id"]
            context.pop("password")
            context.pop("legacy_id")
            context.pop("id")
            context.pop("is_active")
            context.pop("display_name")
            context.pop("user_role")
            context.pop("auth_token")

            context["org_zip"] = user.get("zip_code")
            context["org_type"] = user.get("organization_type")
            context["org_state"] = user.get("state", "")
            context.pop("zip_code")

            if context.get("org_type") in ("CORP", "OTHR"):
                context["org_name"] = user.get("description")

            if user.get("organization_type") == "USHE":
                context["org_name"] = user.get("university", "")

            context.pop("university")
            context.pop("school_id")
            context.pop("organization_type")
            context.pop("state")
            context.pop("description")
            context["level"] = "pro" if self.serviceRegistry.userService.is_user_premium(userModel) else "free"
            context["expiration_date"] = None

            if userModel.proExpiration:
                context["expiration_date"] = (userModel.proExpiration + datetime.timedelta(minutes=userModel.tz_offset))

            context["payment_receipt"] = self.daoRegistry.licenseDao.hasReceipt(userModel.id)

            return context, status.HTTP_200_OK

        except BaseDao.NotFound:
            return BaseError.USER_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def parse_quiz_settings(self, dto, activity_instance, overrideSettings):
        """
        get the settings for an activity or some default values
        :param dto:
        :param activity_instance:
        :param overrideSettings:
        :return:
        """

        settings = {
            "pacing": dto.pacing,
            "allow_student_nav": dto.allow_student_nav,
            "random_answers": dto.random_answers,
            "random_questions": dto.random_questions,
            "show_feedback": dto.show_feedback,
            "require_names": dto.require_names,
            "show_results": dto.show_results,
            "one_attempt": dto.one_attempt
        }

        # no need to make extra db calls if we can update it in the dict
        settings.update(overrideSettings)

        self.daoRegistry.activitySettingsDao.createSettings(activity_instance.id, settings)
        self.cacheRegistry.activitySettingsCache.cacheData(activity_instance.id, settings)

    def parse_space_race_settings(self, dto, activity_instance, overrideSettings):
        """
        parse settings for space race activity
        :param dto:
        :param activity_instance:
        :param overrideSettings:
        :return:
        """
        settings = {
            "pacing": dto.pacing,
            "allow_student_nav": dto.allow_student_nav,
            "random_answers": dto.random_answers,
            "random_questions": dto.random_questions,
            "show_feedback": dto.show_feedback,
            "team_count": dto.team_count,
            "team_assignment_type": dto.team_assignment_type,
            "next_team_assignment": dto.next_team_assignment,
            "icon_type": dto.icon_type,
            "show_results": dto.show_results,
            "seconds": dto.seconds,
            "require_names": dto.require_names,
            "one_attempt": dto.one_attempt
        }

        # no need to make extra db calls if we can update it in the dict
        settings.update(overrideSettings)

        self.daoRegistry.activitySettingsDao.createSettings(activity_instance.id, settings)
        self.cacheRegistry.activitySettingsCache.cacheData(activity_instance.id, settings)

    def build_quick_question_settings(self, dto, activity_instance, overrideSettings):
        """
        build default settings for quick question activity
        :param dto:
        :param activity_instance:
        :return:
        """
        settings = {
            "pacing": dto.pacing,
            "allow_student_nav": dto.allow_student_nav,
            "show_feedback": dto.show_feedback,
            "one_attempt": dto.one_attempt
        }

        if dto.allow_repeat_responses is not None:
            settings["allow_repeat_responses"] = dto.allow_repeat_responses

        if dto.require_names is not None:
            settings["require_names"] = dto.require_names

        if overrideSettings:
            settings.update(overrideSettings)

        self.daoRegistry.activitySettingsDao.createSettings(activity_instance.id, settings)
        self.cacheRegistry.activitySettingsCache.cacheData(activity_instance.id, settings)

    def build_quick_question_vote_settings(self, dto, activity_instance, overrideSettings=None):
        """
        build default settings for quick question activity
        :param dto:
        :param activity_instance:
        :return:
        """
        settings = {
            "pacing": dto.pacing,
            "allow_student_nav": dto.allow_student_nav,
            "show_feedback": dto.show_feedback,
            "require_names": dto.require_names,
            "one_attempt": dto.one_attempt
        }

        if overrideSettings:
            settings.update(overrideSettings)

        if dto.allow_repeat_responses is not None:
            settings["allow_repeat_responses"] = dto.allow_repeat_responses

        self.daoRegistry.activitySettingsDao.createSettings(activity_instance.id, settings)
        self.cacheRegistry.activitySettingsCache.cacheData(activity_instance.id, settings)

    def startQuizActivity(self, dto, activity_type, **kwargs):
        """

        :param activity_type:
        :param dataDict:
        :return:
        """

        # check if user is authenticated
        user = kwargs.get("user")

        authToken = dto.auth_token or user.auth_token

        if user is None:
            user = self.serviceRegistry.userService.validateUser(authToken)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

        quiz = kwargs.get("quiz")
        if quiz is None:
            socNum = dto.soc_num

            if socNum is None:
                return BaseError.SOC_NUMBER_IS_MISSING, status.HTTP_400_BAD_REQUEST

            quiz = self.daoRegistry.quizDao.loadQuizBySoc(socNum, authToken, False, _type=BaseDao.TO_MODEL)

        room = kwargs.get("room")

        if room is None:
            room = self.serviceRegistry.roomService.validateRoom(dto.room_name)

            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.created_by_id != user.id:
                return BaseError.PERMISION_DENIED, status.HTTP_400_BAD_REQUEST

        rostered_room = False
        if room.status & RoomModel.ROSTERED == RoomModel.ROSTERED and room.status < RoomModel.ARCHIVED:
            if self.serviceRegistry.userService.is_user_premium(user) is False:
                return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

            self.daoRegistry.studentDao.logoutStudents(room.id)

            rostered_room = True

        elif room.status >= RoomModel.ARCHIVED:
            return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

        activityInstance = ActivityInstanceModel()
        activityInstance.activity_type = activity_type
        activityInstance.activity_id = quiz.id
        activityInstance.started_by_id = user.id
        activityInstance.room_name = room.name_lower
        activityInstance.start_time = pytz.utc.localize(datetime.datetime.utcnow())
        activityInstance.activity_state = -1
        activityInstance.students_finished = "[]"
        activityInstance.hide_report = False
        activityInstance.report_status = 0

        self.daoRegistry.activityInstanceDao.closeActivitiesByRoom(user.id, room.name_lower)

        activityInstance.id = self.daoRegistry.activityInstanceDao.createActivity(activityInstance.toDict())

        settingsOverrides = kwargs.get("settings", {})

        if rostered_room:
            # don't allow this to be false for rostered activities because they will get empty reports
            settingsOverrides[soc_constants.REQUIRE_NAMES] = 'True'
            roster = self.daoRegistry.rosterDao.getRoster(room.id)
            self.daoRegistry.studentActivityDao.addRoster(roster.id, activityInstance.id)
        else:
            # make sure that free users don't have one attempt setting enabled
            settingsOverrides[soc_constants.ONE_ATTEMPT] = 'False'

        if activity_type == ActivityInstanceModel.QUIZ:
            self.parse_quiz_settings(dto, activityInstance, settingsOverrides)
        elif activity_type == ActivityInstanceModel.SPACE_RACE:
            self.parse_space_race_settings(dto, activityInstance, settingsOverrides)

        d = datetime.datetime.utcnow()
        quiz.last_updated = pytz.utc.localize(d)
        self.daoRegistry.quizDao.updateQuiz(quiz.toDict())

        activityData = self.daoRegistry.activityInstanceDao.loadActivityForLecturer(activityInstance.id)
        respData = StartActivityInstanceDTO.fromSocrativeDict(activityData)
        studData = StudentStartActivityInstanceDTO.fromSocrativeDict(activityData)

        send_model(room.name_lower + "-student", key="localActivity", data=studData)
        send_model(room.name_lower + '-teacher', key="localActivity", data=respData)

        self.cacheRegistry.quizCache.invalidateData(quiz.id)

        return respData, status.HTTP_200_OK

    def startExitTicket(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        authToken = dto.auth_token

        try:
            user = self.serviceRegistry.userService.validateUser(authToken)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            # activate translation
            lang_activated = False
            if user.language in [a[0] for a in projSettings.LANGUAGES]:
                translation.activate(user.language)
                lang_activated = True

            # Generate the exit ticket quiz
            quiz = self.generate_exit_ticket_quiz(user.id)

            # deactivate translation
            if lang_activated:
                translation.deactivate()

            settings = {
                "pacing": 'student',
                "require_names": 'True',
                "allow_student_nav": 'False',
                "show_feedback": 'False',
                soc_constants.ONE_ATTEMPT: 'True' # it will be disabled for free users, and enabled for exit tickets for pro users
            }

            return self.startQuizActivity(dto, ActivityInstanceModel.QUIZ, user=user, quiz=quiz, settings=settings)

        except BaseDao.NotFound:
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def convertQuickQuestionToVote(self, request, dto):
        """
        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            room = self.serviceRegistry.roomService.validateRoom(dto.room_name)

            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.created_by_id != user.id:
                return BaseError.PERMISION_DENIED, status.HTTP_400_BAD_REQUEST

            rostered_room = False
            if room.status & RoomModel.ROSTERED == RoomModel.ROSTERED and room.status < RoomModel.ARCHIVED:
                if self.serviceRegistry.userService.is_user_premium(user) is False:
                    return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

                self.daoRegistry.studentDao.logoutStudents(room.id)

                rostered_room = True

            elif room.status >= RoomModel.ARCHIVED:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if user.tz_offset is None:
                local_time = datetime.datetime.utcnow()
            else:
                local_time = datetime.datetime.utcnow() - datetime.timedelta(minutes=user.tz_offset)

            # Create a new Quiz
            d = datetime.datetime.utcnow()
            quiz = QuizModel()
            quiz.name = "Vote %s - %s:%s" % (str(local_time.date()), str(local_time.hour), str(local_time.minute))
            quiz.is_hidden = True
            quiz.created_by_id = user.id
            quiz.last_updated = pytz.utc.localize(d)
            quiz.sharable = False
            quiz.created_date = pytz.utc.localize(d)

            currentActivity = self.daoRegistry.activityInstanceDao.getActivityByRoomName(user.auth_token,
                                                                                         room.name_lower)
            if currentActivity.activity_type != ActivityInstanceModel.SINGLE_QUESTION:
                return BaseError.ACTIVITY_TYPE_NOT_SINGLE_QUESTION, status.HTTP_400_BAD_REQUEST

        # if the old question had text, incorporate it in the new question
            oldQuestion = self.daoRegistry.questionDao.loadQuestionForQuiz(currentActivity.activity_id)
            questionText = ugettext("(Vote) {0}").format(oldQuestion.question_text)

            question = QuestionModel()
            question.type = QuestionModel.MULTIPLE_CHOICE
            question.created_by_id = user.id
            question.order = 1
            question.question_text = questionText

            quiz.questions = [question]

            answers = []
            order = 0
            responses = self.daoRegistry.studentResponseTextAnswerDao.getAllTextResponses(currentActivity.id)
            for responseText in responses:
                order += 1
                answer = AnswerModel()
                answer.created_by_id = user.id
                answer.text = htmlEscape(responseText)
                answer.order = order
                answers.append(answer)

            question.answers = answers

            quiz = self.daoRegistry.quizDao.saveFullQuiz(quiz)

            activityInstance = ActivityInstanceModel()
            activityInstance.activity_type = ActivityInstanceModel.SINGLE_QUESTION_VOTING
            activityInstance.activity_id = quiz.id
            activityInstance.started_by_id = user.id
            activityInstance.room_name = room.name_lower
            activityInstance.start_time = datetime.datetime.utcnow()
            activityInstance.activity_state = -1
            activityInstance.students_finished = "[]"
            activityInstance.hide_report = False
            activityInstance.report_status = 0

            self.daoRegistry.activityInstanceDao.closeActivitiesByRoom(user.id, room.name_lower)

            activityInstance.id = self.daoRegistry.activityInstanceDao.createActivity(activityInstance.toDict())

            overrideSettings = None
            if rostered_room:
                overrideSettings = {soc_constants.REQUIRE_NAMES: 'True'}
                roster = self.daoRegistry.rosterDao.getRoster(room.id)
                self.daoRegistry.studentActivityDao.addRoster(roster.id, activityInstance.id)
            else:
                overrideSettings = {soc_constants.ONE_ATTEMPT: 'False'}

            self.build_quick_question_vote_settings(dto, activityInstance, overrideSettings)

            activityData = self.daoRegistry.activityInstanceDao.loadDeepActivity(user.auth_token, activityInstance.id)
            activityData["finished"] = False
            respData = FullActivityReportDTO.fromSocrativeDict(activityData)

            studentData = SimpleActivityReportDTO.fromSocrativeDict(activityData)
            send_model(room.name_lower + '-student', key="localActivity", data=studentData)
            send_model(room.name_lower + '-teacher', key="localActivity", data=studentData)

            return respData, status.HTTP_200_OK

        except BaseDao.NotFound:
            return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def startQuickQuestion(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)
            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            room = self.serviceRegistry.roomService.validateRoom(dto.room_name)

            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.created_by_id != user.id:
                return BaseError.PERMISION_DENIED, status.HTTP_400_BAD_REQUEST

            rostered_room = False
            if room.status & RoomModel.ROSTERED == RoomModel.ROSTERED and room.status < RoomModel.ARCHIVED:
                if self.serviceRegistry.userService.is_user_premium(user) is False:
                    return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

                self.daoRegistry.studentDao.logoutStudents(room.id)

                rostered_room = True

            elif room.status >= RoomModel.ARCHIVED:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if user.tz_offset is None:
                local_time = datetime.datetime.utcnow()
            else:
                local_time = datetime.datetime.utcnow() - datetime.timedelta(minutes=user.tz_offset)

            # Create a new Quiz
            d = datetime.datetime.utcnow()
            quiz = QuizModel()
            quiz.name = "Quick Question %s - %s:%s" % (str(local_time.date()), str(local_time.hour),
                                                       str(local_time.minute))
            quiz.is_hidden = True
            quiz.created_by_id = user.id
            quiz.last_updated = pytz.utc.localize(d)
            quiz.sharable = False
            quiz.created_date = pytz.utc.localize(d)

            # add the a question of the requested type
            self.generate_question(dto.question_type, user, dto.question_text, quiz)

            quiz = self.daoRegistry.quizDao.saveFullQuiz(quiz)

            # create the new activity instance
            atype = ActivityInstanceModel.SINGLE_QUESTION
            if dto.question_type.upper() in (QuestionModel.TRUE_FALSE, QuestionModel.MULTIPLE_CHOICE):
                atype = ActivityInstanceModel.SINGLE_QUESTION_NO_REPORT

            activityInstance = ActivityInstanceModel()
            activityInstance.activity_type = atype
            activityInstance.activity_id = quiz.id
            activityInstance.started_by_id = user.id
            activityInstance.room_name = room.name_lower
            activityInstance.start_time = pytz.utc.localize(datetime.datetime.utcnow())
            activityInstance.activity_state = -1
            activityInstance.students_finished = "[]"
            activityInstance.hide_report = False
            activityInstance.report_status = 0

            self.daoRegistry.activityInstanceDao.closeActivitiesByRoom(user.id, room.name_lower)
            activityInstance.id = self.daoRegistry.activityInstanceDao.createActivity(activityInstance.toDict())

            if rostered_room:
                roster = self.daoRegistry.rosterDao.getRoster(room.id)
                self.daoRegistry.studentActivityDao.addRoster(roster.id, activityInstance.id)

            overrideSettings = {}
            if dto.question_type in (QuestionModel.TRUE_FALSE, QuestionModel.MULTIPLE_CHOICE):
                if dto.require_names is None:
                    dto.require_names = False
                if dto.one_attempt is None:
                    dto.one_attempt = False
            elif rostered_room:
                overrideSettings = {soc_constants.REQUIRE_NAMES: 'True'}

            if rostered_room is False:
                overrideSettings[soc_constants.ONE_ATTEMPT] = 'False'

            self.build_quick_question_settings(dto, activityInstance, overrideSettings)

            activityData = self.daoRegistry.activityInstanceDao.loadActivityForLecturer(activityInstance.id)
            respData = StartActivityInstanceDTO.fromSocrativeDict(activityData)
            studData = StudentStartActivityInstanceDTO.fromSocrativeDict(activityData)

            send_model(room.name_lower + '-student', key="localActivity", data=studData)
            send_model(room.name_lower + '-teacher', key="localActivity", data=respData)

            return respData, status.HTTP_200_OK

        except BaseDao.NotFound:
            return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def startQuiz(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """
        try:
            return self.startQuizActivity(dto, ActivityInstanceModel.QUIZ)
        except BaseDao.NotFound:
            return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def startSpaceRace(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """
        try:
            return self.startQuizActivity(dto, ActivityInstanceModel.SPACE_RACE)
        except BaseDao.NotFound:
            return BaseError.INVALID_REQUEST, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def updateTeacherProfile(self, request, dataDict):
        """

        :param request:
        :param dataDict:
        :return:
        """
        try:

            auth_token = dataDict.get("auth_token")
            firstName = dataDict.get("first_name", "")
            lastName = dataDict.get("last_name", "")
            
            email = dataDict.get("email", "").strip()
            password = dataDict.get("password", "")
            old_password = dataDict.get("old_password", "")
            
            country = dataDict.get("country", "")
            language = dataDict.get("language")

            organization_name = dataDict.get("org_name", "")
            organization_type = dataDict.get("org_type", "")
            organization_zip = dataDict.get("org_zip", "")
            organization_id = dataDict.get("org_id")
            organization_state = dataDict.get("org_state")

            school_name = dataDict.get("school_name", "")
            district_name = dataDict.get("district_name", "")
            school_data = dataDict.get("school_data", "")
            grade = dataDict.get("grade", "")
            subject = dataDict.get("subject", "")
            core = dataDict.get("core", "")

            role = dataDict.get("role", [])

            # xor old password and new password for empty strings.
            # Don't allow them unless they're both empty or both full
            if (not isStringNotEmpty(old_password)) is not (not isStringNotEmpty(password)):
                return BaseError.INVALID_PASSWORD_COMBO, status.HTTP_400_BAD_REQUEST

            # basic checks for received data
            if not isStringNotEmpty(auth_token):
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if not isStringNotEmpty(country):
                return BaseError.INVALID_COUNTRY, status.HTTP_400_BAD_REQUEST

            if not isStringNotEmpty(email):
                return BaseError.INVALID_EMAIL_ADDRESS, status.HTTP_400_BAD_REQUEST

            if not isEmailValid(email):
                return BaseError.INVALID_EMAIL_ADDRESS, status.HTTP_400_BAD_REQUEST

            if not isStringNotEmpty(firstName):
                return BaseError.FIRST_NAME_IS_INVALID, status.HTTP_400_BAD_REQUEST

            if not isStringNotEmpty(lastName):
                return BaseError.LAST_NAME_IS_INVALID, status.HTTP_400_BAD_REQUEST

            if not isStringNotEmpty(language):
                return BaseError.INVALID_LANGUAGE, status.HTTP_400_BAD_REQUEST

            if not isStringNotEmpty(organization_type):
                return BaseError.ORGANIZATION_TYPE_INVALID, status.HTTP_400_BAD_REQUEST

            if not role:
                return BaseError.INVALID_ROLES_SENT, status.HTTP_400_BAD_REQUEST

            user = self.daoRegistry.usersDao.loadUser(auth_token)

            # fail if the old password doesn't match
            if isStringNotEmpty(old_password) and not self.passwordHasher.verify(old_password, user.password):
                return BaseError.INVALID_PASSWORD_COMBO, status.HTTP_400_BAD_REQUEST

            orgType = [a[0] for a in SocrativeUserModel.organization_choices if organization_type in a]
            if orgType:
                orgType = orgType[0]
            else:
                orgType = None

            # for each type of org make some additional checks
            if orgType == "K12":

                if not isStringNotEmpty(school_name):
                    return BaseError.INVALID_SCHOOL_NAME, status.HTTP_400_BAD_REQUEST
                
                # check country
                if country in "USA":
                    if not isStringNotEmpty(organization_zip):
                        return BaseError.INVALID_ZIP_CODE, status.HTTP_400_BAD_REQUEST
                    
                    if type(organization_id) not in (int, long):
                        return BaseError.INVALID_ORGRANIZATION_ID, status.HTTP_400_BAD_REQUEST

                    if organization_id != -1 and not isString(district_name):
                        return BaseError.INVALID_DISTRICT_NAME, status.HTTP_400_BAD_REQUEST

                    if not isStringNotEmpty(organization_state):
                        return BaseError.INVALID_STATE, status.HTTP_400_BAD_REQUEST

            elif orgType == "USHE":

                # check if we receive the university name
                # independent of country
                if not isStringNotEmpty(organization_name):
                    return BaseError.INVALID_UNIVERSITY_SENT, status.HTTP_400_BAD_REQUEST

                if country in "USA":
                    if not isStringNotEmpty(organization_state):
                        return BaseError.INVALID_STATE, status.HTTP_400_BAD_REQUEST

            elif orgType == "OTHR" or orgType == "CORP":
                # if org is corp the name comes in description
                if not isStringNotEmpty(organization_name):
                    return BaseError.INVALID_SCHOOL_NAME, status.HTTP_400_BAD_REQUEST
            else:
                return BaseError.ORGANIZATION_TYPE_INVALID, status.HTTP_400_BAD_REQUEST

            # if we received a password then the user wants to change it
            if password != "":
                user.password = self.passwordHasher.encode(password, self.passwordHasher.salt(), self.PBK_ITERATIONS)

            clear_room = user.language != language

            user.email = email.strip().lower() if email != "" else user.email
            user.first_name = firstName
            user.last_name = lastName
            user.organization_type = orgType
            user.zip_code = organization_zip if organization_zip != "" else None
            user.university = organization_name if orgType == "USHE" else None
            user.country = country
            user.language = language
            user.description = organization_name if orgType in ("OTHR", "CORP") else None
            
            user.school_id = organization_id if isInteger(organization_id) else None
            user.school_name = school_name if orgType == "K12" else None
            
            role = list(set(role))
            good_roles = list()
            if role:
                sRoles = ["teacher", "administrator", "it-technology", "other"]
                for i in range(len(role)):
                    if role[i]:
                        role[i] = role[i].lower()
                good_roles = list(set(sRoles) & set(role))
            user.user_role = ujson.dumps(good_roles)
            
            user.state = organization_state if organization_state != "" else None
            user.district_name = district_name if district_name != "" else None
            user.school_data = ujson.dumps(school_data) if school_data is not None else None

            dictModel = user.toDict()
            if user.password is None:
                dictModel.pop(SocrativeUserModel.PASSWORD)

            self.daoRegistry.usersDao.updateUser(dictModel)

            self.cacheRegistry.userModelCache.invalidateData(auth_token)

            if clear_room:
                room_name = self.daoRegistry.roomDao.getLastUsedRoom(user.id, soc_constants.NO_ROOM_STATUS_CHANGE)
                send_model(room_name + '-student', key="student", data="room_cleared")

            return {}, status.HTTP_200_OK

        except BaseDao.NotFound:
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def displayTeacherProfile(self, request, dataDict):
        """

        :param request:
        :param dataDict:
        :return:
        """

        auth_token = dataDict.get("auth_token")

        if not isStringNotEmpty(auth_token):
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

        try:
            user = self.serviceRegistry.userService.validateUser(auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            respData = User.fromSocrativeModel(user)
            return respData, status.HTTP_200_OK
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def changePassword(self, request, dataDict):
        """

        :param request:
        :param dataDict:
        :return:
        """

        auth_token = dataDict.get('auth_token')
        password = dataDict.get('password')
        new_password = dataDict.get('new_password')

        if not isStringNotEmpty(auth_token):
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

        if not isStringNotEmpty(new_password):
            return BaseError.NEW_PASSWORD_MISSING, status.HTTP_400_BAD_REQUEST

        if not isString(password):
            return BaseError.INVALID_PASSWORD, status.HTTP_400_BAD_REQUEST

        try:
            user = self.daoRegistry.usersDao.loadUser(auth_token)

            if len(new_password) < self.MIN_PASSWORD_LENGTH:
                return BaseError.INVALID_PASSWORD, status.HTTP_400_BAD_REQUEST

            # if the password is correct
            if user.password == '' or self.passwordHasher.verify(password, user.password):

                newPass = self.passwordHasher.encode(new_password, self.passwordHasher.salt(), self.PBK_ITERATIONS)

                # # change it
                self.daoRegistry.usersDao.updatePassword(user.id, newPass)

                self.cacheRegistry.userModelCache.invalidateData(auth_token)

                return {}, status.HTTP_200_OK

            else:
                return BaseError.INVALID_PASSWORD, status.HTTP_400_BAD_REQUEST

        except BaseDao.NotFound:
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def changeEmail(self, request, dataDict):
        """

        :param request:
        :param dataDict:
        :return:
        """
        auth_token = dataDict.get('auth_token')
        new_email = dataDict.get('new_email', "").strip()

        if not isStringNotEmpty(auth_token):
            return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

        if not isStringNotEmpty(new_email) or not isEmailValid(new_email):
            return BaseError.INVALID_EMAIL_ADDRESS, status.HTTP_400_BAD_REQUEST

        # try an get the user
        try:
            user = self.serviceRegistry.userService.validateUser(auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            new_email = new_email.lower()

            is_available = self.daoRegistry.usersDao.checkEmail(new_email)
            if not is_available:
                return BaseError.EMAIL_ALREADY_REGISTERED, status.HTTP_400_BAD_REQUEST
            else:
                self.daoRegistry.usersDao.updateEmail(user.id, new_email)

                self.cacheRegistry.userModelCache.invalidateData(auth_token)

                return {}, status.HTTP_200_OK
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def duplicateQuiz(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """
        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            originalQuiz = self.daoRegistry.quizDao.loadQuizBySoc(dto.soc_number, dto.auth_token, is_hidden=False,
                                                                  _type=BaseDao.TO_MODEL)

            newQuiz = self.daoRegistry.quizDao.cloneQuiz(originalQuiz, user.id, hideCloning=False, addStandards=True,
                                                         name=dto.name)

            standardName = self.daoRegistry.standardsDao.getStandardName(newQuiz.id)

            resp = {
                       "quiz":
                           {
                               "soc": newQuiz.soc_number,
                               "name": newQuiz.name,
                               "id": newQuiz.id,
                               "date": newQuiz.last_updated,
                               "questions": self.daoRegistry.questionDao.count(newQuiz.id),
                               "sharable": newQuiz.sharable
                           }
                   }

            # send back also the standard name if it exists
            if standardName is not None:
                resp["quiz"]["standard"] = {"name": standardName}

            return resp, status.HTTP_200_OK

        except BaseDao.NotFound:
            return BaseError.INVALID_SOC_NUMBER, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def generate_question(self, inType, user, questionText, quiz):
        """
        generate a new question
        :param inType: avoid overwriting type
        :param user:
        :param questionText:
        :param quiz:
        :return:
        """
        inType = inType.lower()

        if inType == "tf":
            return self._generate_tf_question(user, questionText, quiz)
        elif inType == "mc":
            return self._generate_mc_question(user, questionText, quiz)
        elif inType == "fr":
            return self._generate_fr_question(user, questionText, quiz)
        else:
            raise ValueError("Unexpected question type %s" % str(inType))

    def generate_exit_ticket_quiz(self, user_id):
        """
        generate an exit ticket quiz
        :param user_id: user model
        :return:
        """

        quiz = QuizModel()
        quiz.name = "Exit Ticket Quiz"
        quiz.is_hidden = True
        quiz.sharable = False
        quiz.created_by_id = user_id
        quiz.last_updated = datetime.datetime.utcnow()
        quiz.created_date = datetime.datetime.utcnow()

        q1 = QuestionModel()
        q1.created_by_id = user_id
        q1.order = 1
        q1.question_text = ugettext("How well did you understand today's material?")
        q1.type = QuestionModel.MULTIPLE_CHOICE
        q1.created_date = datetime.datetime.utcnow()

        a1 = AnswerModel()
        a1.created_by_id = user_id
        a1.text = ugettext("Totally got it")
        a1.order = 1
        a1.is_correct = True

        a2 = AnswerModel()
        a2.created_by_id = user_id
        a2.text = ugettext("Pretty well")
        a2.order = 2
        a2.is_correct = None

        a3 = AnswerModel()
        a3.created_by_id = user_id
        a3.text = ugettext("Not very well")
        a3.order = 3
        a3.is_correct = None

        a4 = AnswerModel()
        a4.created_by_id = user_id
        a4.text = ugettext("Not at all")
        a4.order = 4
        a4.is_correct = None

        q1.answers = [a1, a2, a3, a4]

        q2 = QuestionModel()
        q2.created_by_id = user_id
        q2.order = 2
        q2.question_text = ugettext("What did you learn in today's class?")
        q2.type = QuestionModel.FREE_RESPONSE
        q2.created_date = datetime.datetime.utcnow()
        q2.answers = list()

        q3 = QuestionModel()
        q3.created_by_id = user_id
        q3.order = 3
        q3.question_text = ugettext("Please answer the teacher's question.")
        q3.type = QuestionModel.FREE_RESPONSE
        q3.created_date = datetime.datetime.utcnow()
        q3.answers = list()

        quiz.questions = [q1, q2, q3]

        quiz = self.daoRegistry.quizDao.saveFullQuiz(quiz)

        return quiz

    def _generate_tf_question(self, user, questionText, quiz):
        """
        generate a quick quiz
        :param user:
        :param questionText:
        :param quiz:
        :return:
        """
        q = QuestionModel()
        q.type = QuestionModel.TRUE_FALSE
        q.created_by_id = user.id,
        q.order = 1
        q.question_text = questionText
        q.created_date = datetime.datetime.utcnow()

        a1 = AnswerModel()
        a1.text = "True"
        a1.is_correct = None
        a1.order = 1
        a1.created_by_id = user.id

        a2 = AnswerModel()
        a2.text = "False"
        a2.is_correct = None
        a2.order = 2
        a2.created_by_id = user.id

        q.answers = [a1, a2]

        quiz.questions = [q]

    def _generate_mc_question_answers(self, count, user):
        """
        generate mc answers
        :param count:
        :param user:
        :return:
        """
        for i in range(count):
            a = AnswerModel()
            a.text = ""
            a.is_correct = None
            a.order = i + 1
            a.created_by_id = user.id

            yield a

    def _generate_mc_question(self, user, questionText, quiz):
        """
        generate a multiple choice question
        :param user:
        :param questionText:
        :param quiz:
        :return:
        """
        question = QuestionModel()
        question.type = QuestionModel.MULTIPLE_CHOICE
        question.created_by_id = user.id
        question.order = 1
        question.question_text = questionText
        question.created_date = datetime.datetime.utcnow()

        question.answers = [a for a in self._generate_mc_question_answers(5, user)]

        quiz.questions = [question]

    def _generate_fr_question(self, user, questionText, quiz):
        """
        generate fr question
        :param user:
        :param questionText:
        :param quiz:
        :return:
        """
        q = QuestionModel()
        q.type = QuestionModel.FREE_RESPONSE
        q.created_by_id = user.id
        q.order = 1
        q.question_text = questionText
        q.created_date = datetime.datetime.utcnow()
        q.answers = list()

        quiz.questions = [q]

    def handleHandraise(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if self.serviceRegistry.userService.is_user_premium(user) is False:
                return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

            room = self.serviceRegistry.roomService.validateRoom(dto.room_name)

            if room is None:
                return BaseError.ROOM_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            if room.status & RoomModel.ROSTERED != RoomModel.ROSTERED:
                return BaseError.ROOM_NOT_ROSTERED, status.HTTP_400_BAD_REQUEST

            if dto.active is False:
                # acknowledge the student handraise
                self.daoRegistry.studentDao.updateHandraiseStatus(soc_constants.STUDENT_HANDRAISE_OFF,
                                                                  dto.id,
                                                                  off=True)

                send_model(room.name_lower + "-student", key="handraise", data=ujson.dumps({"state": "off",
                                                                                            "id": dto.id}))
            else:
                self.daoRegistry.studentDao.updateHandraiseStatus(StudentModel.HANDRAISE,
                                                                  dto.id)

                send_model(room.name_lower + "-student", key="handraise", data=ujson.dumps({"state": "on",
                                                                                            "id": dto.id}))

            return {}, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def requestPaymentReceipt(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:

            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if dto.action == "email":
                request.tasksClient.add_tasks(TaskType.SEND_EMAIL, [{"user_id": user.id,
                                                                     "email_type": EmailTypes.RECEIPTS,
                                                                     "email_address": user.email.lower()}])
                return {}, status.HTTP_200_OK
            elif dto.action == "download":

                tmp, license_id, years, license_key, expiration_date = self.daoRegistry.licenseDao.getLicenseData(user.id)
                stripeData = ujson.loads(tmp) if tmp else {}

                if not stripeData:
                    return BaseError.INVALID_RECEIPT_DATA, status.HTTP_400_BAD_REQUEST

                data = {}
                if expiration_date.date() < datetime.date(2017, 8, 1):
                    expiration_date = datetime.datetime(2017, 8, 1)  # this is to avoid some initial bugs from last year
                data["email"] = user.email
                data["payment_id"] = stripeData.get("receipt_id", license_id)
                data["price"] = stripeData["amount"]
                data["type"] = stripeData["source"]["brand"]
                data["last4"] = stripeData["source"]["last4"]
                data["expiration_date"] = expiration_date
                data["name"] = "%s, %s" % ('' if user.last_name is None else user.last_name,
                                           '' if user.first_name is None else user.first_name)
                if user.organization_type in ("OTHR", "CORP"):
                    data["organization"] = user.description
                elif user.organization_type == "USHE":
                    data["organization"] = user.university
                elif user.organization_type == "K12":
                    data["organization"] = user.school_name
                else:
                    data["organization"] = ''

                data["years"] = years
                data["license_key"] = license_key

                obj = ReceiptPdf(data)

                memFile = obj.getReceiptPdf()

                response = HttpResponse(memFile, content_type="application/pdf")
                response['Content-Disposition'] = "attachment; filename=Socrative_PRO_Receipt.pdf"

                return {"resp": response}, 200

            else:
                return BaseError.INVALID_RECEIPT_ACTION, status.HTTP_400_BAD_REQUEST

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_400_BAD_REQUEST

    def getScoreSettings(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if self.serviceRegistry.userService.is_user_premium(user) is False:
                return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

            resp = self.daoRegistry.scoreSettingsDao.getScoreSettings(user.id)

            if not resp:
                return {}, status.HTTP_200_OK

            if resp and resp["prefix"] is None:
                resp["prefix"] = ''

            return resp, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def createScoreSettings(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if self.serviceRegistry.userService.is_user_premium(user) is False:
                return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

            # make sure it's between f1 and f12
            if dto.key <= 0 or dto.key > 12:
                return BaseError.INVALID_GRADECAM_KEY, status.HTTP_400_BAD_REQUEST

            if dto.speed not in (ScoreSettingsModel.FAST, ScoreSettingsModel.MEDIUM, ScoreSettingsModel.SLOW,
                                 ScoreSettingsModel.VERY_SLOW):
                return BaseError.INVALID_GRADECAM_SPEED, status.HTTP_400_BAD_REQUEST

            if dto.method not in (ScoreSettingsModel.NUMERIC, ScoreSettingsModel.PERCENTAGE):
                return BaseError.INVALID_GRADECAM_METHOD, status.HTTP_400_BAD_REQUEST

            if dto.next_line not in (ScoreSettingsModel.ENTER, ScoreSettingsModel.ENTER_TWICE, ScoreSettingsModel.TAB,
                                     ScoreSettingsModel.TAB_TWICE, ScoreSettingsModel.DOWN_ARROW):
                return BaseError.INVALID_GRADECAM_NEXT_LINE, status.HTTP_400_BAD_REQUEST

            # check to see if there are already saved settings for this user
            exists = self.daoRegistry.scoreSettingsDao.exists(user.id)

            if exists:
                return BaseError.EXPORT_SCORE_SETTINGS_ALREADY_EXIST, status.HTTP_400_BAD_REQUEST

            model = ScoreSettingsModel()
            model.teacher_id = user.id
            model.key = dto.key
            model.next_line = dto.next_line
            model.speed = dto.speed
            model.prefix = dto.prefix or ''
            model.method = dto.method

            score_id = self.daoRegistry.scoreSettingsDao.create(model.toDict())

            return {"id": score_id}, status.HTTP_201_CREATED

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def updateScoreSettings(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if self.serviceRegistry.userService.is_user_premium(user) is False:
                return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

            # make sure it's between f1 and f12
            if dto.key <= 0 or dto.key > 12:
                return BaseError.INVALID_GRADECAM_KEY, status.HTTP_400_BAD_REQUEST

            if dto.speed not in (ScoreSettingsModel.FAST, ScoreSettingsModel.MEDIUM, ScoreSettingsModel.SLOW,
                                 ScoreSettingsModel.VERY_SLOW):
                return BaseError.INVALID_GRADECAM_SPEED, status.HTTP_400_BAD_REQUEST

            if dto.method not in (ScoreSettingsModel.NUMERIC, ScoreSettingsModel.PERCENTAGE):
                return BaseError.INVALID_GRADECAM_METHOD, status.HTTP_400_BAD_REQUEST

            if dto.next_line not in (ScoreSettingsModel.ENTER, ScoreSettingsModel.ENTER_TWICE, ScoreSettingsModel.TAB,
                                     ScoreSettingsModel.TAB_TWICE, ScoreSettingsModel.DOWN_ARROW):
                return BaseError.INVALID_GRADECAM_NEXT_LINE, status.HTTP_400_BAD_REQUEST

            # check to see if there are already saved settings for this user
            old_model = self.daoRegistry.scoreSettingsDao.getScoreSettings(user.id)

            objModel = ScoreSettingsModel.fromRequestDto(dto)
            objModel.teacher_id = user.id
            model = objModel.toDict()

            if not old_model or old_model[ScoreSettingsModel.ID] != dto.pk:
                return BaseError.EXPORT_SCORE_SETTINGS_NOT_FOUND, status.HTTP_400_BAD_REQUEST

            keysToRemove = [k for k in model if k in old_model and old_model[k] == model[k] or model[k] is None]
            for k in keysToRemove:
                if k != ScoreSettingsModel.ID:
                    model.pop(k)

            if ScoreSettingsModel.ID not in model:
                model[ScoreSettingsModel.ID] = old_model[ScoreSettingsModel.ID]

            # if nothing changed but last update then don't update the quiz model in the db
            if len(model) == 1 and ScoreSettingsModel.ID in model:
                return {}, status.HTTP_200_OK

            self.daoRegistry.scoreSettingsDao.update(model)

            return {}, status.HTTP_200_OK

        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    def getNotifications(self, userModel):
        """

        :param userModel:
        :return:
        """

        notifications = None

        # check expiration
        if self.daoRegistry.licenseDao.hasLicense(userModel.id):

            # this represents a tuple with 2 expiration dates, one when the user id is treated as a normal user
            # the other case is when the user id is the buyer
            expiration_dates = self.daoRegistry.licenseDao.getExpirationDates(userModel.id)

            expiration_dates = [expiration_date for expiration_date in expiration_dates if expiration_date is not None]

            if not expiration_dates:
                return None

            # notify only when the user risks loosing his pro status - > FIX for SOC-1834
            expiration_date = max(expiration_dates)

            delta_days = (expiration_date.date() - datetime.datetime.utcnow().date()).days

            if delta_days < -soc_constants.MAX_DAYS_AFTER_PRO_EXPIRATION:
                self.daoRegistry.notificationDao.hide([NotificationModel.PRO_ENDING,
                                                       NotificationModel.PRO_ENDED],
                                                      userModel.id)
                return None

            if -soc_constants.MAX_DAYS_AFTER_PRO_EXPIRATION < delta_days <= 0:

                self.daoRegistry.notificationDao.hide([NotificationModel.PRO_ENDING], userModel.id)
                notification = self.daoRegistry.notificationDao.getNotification([NotificationModel.PRO_ENDED],
                                                                                userModel.id)
                if notification is None:
                    notification = NotificationModel()
                    notification.show = True
                    notification.type = NotificationModel.PRO_ENDED
                    notification.is_hidden = False
                    notification.user_id = userModel.id
                    notification.recheck_at = pytz.utc.localize(datetime.datetime.utcnow() + datetime.timedelta(
                        hours=soc_constants.PRO_NOTIFICATION_RECHECK_DURATION))

                    notification.id = self.daoRegistry.notificationDao.insert(notification)

                elif notification.show is False and notification.recheck_at <= pytz.utc.localize(datetime.datetime.utcnow()):
                    notification.recheck_at = pytz.utc.localize(datetime.datetime.utcnow() + datetime.timedelta(
                        hours=soc_constants.PRO_NOTIFICATION_RECHECK_DURATION))
                    notification.show = True
                    self.daoRegistry.notificationDao.showNotification(notification)

                notifications = {
                    "id": notification.id,
                    "type": notification.type,
                    "expires_at": expiration_date,
                    "show": notification.show
                }

            elif delta_days > 0 and delta_days < soc_constants.MAX_DAYS_BEFORE_PRO_EXPIRATION \
                    and not self.daoRegistry.licenseDao.hasAutoRenew(userModel.id):

                self.daoRegistry.notificationDao.hide([NotificationModel.PRO_ENDED], userModel.id)
                notification = self.daoRegistry.notificationDao.getNotification([NotificationModel.PRO_ENDING],
                                                                                userModel.id)
                if notification is None:
                    notification = NotificationModel()
                    notification.show = True
                    notification.type = NotificationModel.PRO_ENDING
                    notification.is_hidden = False
                    notification.user_id = userModel.id
                    notification.recheck_at = pytz.utc.localize(datetime.datetime.utcnow() + datetime.timedelta(
                        hours=soc_constants.PRO_NOTIFICATION_RECHECK_DURATION))

                    notification.id = self.daoRegistry.notificationDao.insert(notification)

                elif notification.show is False and notification.recheck_at <= pytz.utc.localize(
                        datetime.datetime.utcnow()):
                    notification.recheck_at = pytz.utc.localize(datetime.datetime.utcnow() + datetime.timedelta(
                        hours=soc_constants.PRO_NOTIFICATION_RECHECK_DURATION))
                    self.daoRegistry.notificationDao.showNotification(notification)

                notifications = {
                    "id": notification.id,
                    "type": notification.type,
                    "expires_at": expiration_date,
                    "show": notification.show
                }

            else:
                self.daoRegistry.notificationDao.hide([NotificationModel.PRO_ENDING, NotificationModel.PRO_ENDED], userModel.id)
                return None

        return notifications

    def postponeNotification(self, request, dto):
        """

        :param request:
        :param dto:
        :return:
        """

        try:
            user = self.serviceRegistry.userService.validateUser(dto.auth_token)

            if user is None:
                return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST

            if self.serviceRegistry.userService.is_user_premium(user) is False:
                return BaseError.USER_NOT_PREMIUM, status.HTTP_400_BAD_REQUEST

            recheck_at = pytz.utc.localize(datetime.datetime.utcnow() +
                                           datetime.timedelta(hours=soc_constants.PRO_NOTIFICATION_RECHECK_DURATION))
            self.daoRegistry.notificationDao.postponeNotification(dto.id, recheck_at)

            return {}, status.HTTP_200_OK

        except BaseDao.NotUpdated:
            return BaseError.NOTIFICATION_NOT_FOUND, status.HTTP_400_BAD_REQUEST
        except Exception as e:
            logger.error(exceptionStack(e))
            return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR