# coding=utf-8


class LecturerCounters(object):
    """
    class that holds the name of the lecturer views counters
    """

    START_EXIT_TICKET_FAIL = 'socrative.views.lecturers.start_exit_ticket.fail'
    START_EXIT_TICKET = 'socrative.views.lecturers.start_exit_ticket.time'
    CONVERT_QUICK_QUESTION_TO_VOTE_FAIL = 'socrative.views.lecturers.convert_quick_question_to_vote.fail'
    CONVERT_QUICK_QUESTION_TO_VOTE = 'socrative.views.lecturers.convert_quick_question_to_vote.time'
    START_QUICK_QUESTION_FAIL = 'socrative.views.lecturers.start_quick_question.fail'
    START_QUICK_QUESTION = 'socrative.views.lecturers.start_quick_question.time'
    START_QUIZ_FAIL = 'socrative.views.lecturers.start_quiz.fail'
    START_QUIZ = 'socrative.views.lecturers.start_quiz.time'
    START_SPACE_RACE_FAIL = 'socrative.views.lecturers.start_space_race.fail'
    START_SPACE_RACE = 'socrative.views.lecturers.start_space_race.time'
    GET_TEACHER_PROFILE = 'socrative.views.lecturers.get_teacher_profile.time'
    GET_TEACHER_PROFILE_FAIL = 'socrative.views.lecturers.get_teacher_profile.fail'
    UPDATE_TEACHER_PROFILE_FAIL = 'socrative.views.lecturers.update_teacher_profile.fail'
    UPDATE_TEACHER_PROFILE = 'socrative.views.lecturers.update_teacher_profile.time'
    CHANGE_PASSWORD_FAIL = 'socrative.views.lecturers.change_password.fail'
    CHANGE_PASSWORD = 'socrative.views.lecturers.change_password.time'
    CHANGE_EMAIL_FAIL = 'socrative.views.lecturers.change_email.fail'
    CHANGE_EMAIL = 'socrative.views.lecturers.change_email.time'
    DUPLICATE_QUIZ_FAIL = 'socrative.views.lecturers.duplicate_quiz.fail'
    DUPLICATE_QUIZ = 'socrative.views.lecturers.duplicate_quiz.time'
    HANDLE_HANDRAISE_FAIL = 'socrative.views.lecturers.handle_handraise.fail'
    HANDLE_HANDRAISE = 'socrative.views.lecturers.handle_handraise.time'
    GET_PAYMENT_RECEIPT = 'socrative.views.lecturers.get_payment_receipt.time'
    GET_PAYMENT_RECEIPT_FAIL = 'socrative.views.lecturers.get_payment_receipt.fail'
    GET_SCORE_SETTINGS = 'socrative.views.lecturers.get_score_settings.time'
    GET_SCORE_SETTINGS_FAIL = 'socrative.views.lecturers.get_score_settings.fail'
    CREATE_SCORE_SETTINGS = 'socrative.views.lecturers.create_score_settings.time'
    CREATE_SCORE_SETTINGS_FAIL = 'socrative.views.lecturers.create_score_settings.fail'
    UPDATE_SCORE_SETTINGS_FAIL = 'socrative.views.lecturers.update_score_settings.fail'
    UPDATE_SCORE_SETTINGS = 'socrative.views.lecturers.update_score_settings.time'
    POSTPONE_NOTIFICATIONS_FAIL = 'socrative.views.lecturers.postpone_notifications.fail'
    POSTPONE_NOTIFICATIONS = 'socrative.views.lecturers.postpone_notifications.time'
