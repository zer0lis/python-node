from django.db import models

class Notification(models.Model):
    """

    """
    class Meta:
        db_table = "notifications"

    type = models.IntegerField(null=False)
    show = models.BooleanField(null=False, default=True)
    is_hidden = models.BooleanField(null=False, default=False)
    user = models.ForeignKey(to="socrative_users.SocrativeUser")
    recheck_at = models.DateTimeField()