# -*- coding: utf-8 -*-
import logging
import ujson

from common.counters import DatadogThreadStats

from common import http_status as status
from common.base_view import BaseView
from common.socrative_api import exceptionStack
from common.socrative_errors import BaseError
from lecturers.counters import LecturerCounters
from lecturers.serializers import ConvertQuickQuestionRequestDTO, StartQuizRequestDTO, StartSpaceRaceRequestDTO
from lecturers.serializers import StartQuickQuestionRequestDTO, StartExitTicketRequestDTO, DuplicateQuizRequestDTO
from lecturers.serializers import TeacherHandraiseRequestDTO, GetScoreSettingsRequest, CreateScoreSettingsRequest
from lecturers.serializers import UpdateScoreSettingsRequest, PaymentRequestDTO, PostponeNotificationRequestDTO

logger = logging.getLogger(__name__)

statsd = DatadogThreadStats.STATS

class StartExitTicketView(BaseView):
    """
    class that handles the start exit ticket api
    """
    http_method_names = ["post"]

    @statsd.timed(LecturerCounters.START_EXIT_TICKET)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        dataDict = dict()

        failCounter = LecturerCounters.START_EXIT_TICKET_FAIL

        try:
            dataDict.update(ujson.loads(request.body))
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        self.getAuthToken(dataDict)

        dto = StartExitTicketRequestDTO.fromDict(dataDict)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.lecturerServices.startExitTicket(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class ConvertQuickQuestionToVoteView(BaseView):
    """
    class that handles the convert quick question to vote api
    """
    http_method_names = ["post"]

    @statsd.timed(LecturerCounters.CONVERT_QUICK_QUESTION_TO_VOTE)
    def post(self, request, *args, **kwargs):
        """
        handler for HTTP POST for convert quick question to vote view
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        dataDict = dict()
        failCounter = LecturerCounters.CONVERT_QUICK_QUESTION_TO_VOTE_FAIL

        try:
            dataDict.update(ujson.loads(request.body))
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        self.getAuthToken(dataDict)

        dto = ConvertQuickQuestionRequestDTO.fromDict(dataDict)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.lecturerServices.convertQuickQuestionToVote(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class StartQuickQuestionView(BaseView):
    """
    class that handles the start quick question api
    """
    http_method_names = ["post"]

    @statsd.timed(LecturerCounters.START_QUICK_QUESTION)
    def post(self, request, *args, **kwargs):
        """
        handler for HTTP POST for start quick question api
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        dataDict = dict()

        failCounter = LecturerCounters.START_QUICK_QUESTION_FAIL

        try:
            dataDict.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        self.getAuthToken(dataDict)

        dto = StartQuickQuestionRequestDTO.fromDict(dataDict)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.lecturerServices.startQuickQuestion(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class StartQuizView(BaseView):
    """
    class that handles the start quiz activity api
    """
    http_method_names = ["post"]

    @statsd.timed(LecturerCounters.START_QUIZ)
    def post(self, request, *args, **kwargs):
        """
        handler for HTTP POST for start quiz view
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        dataDict = dict()
        for k in request.POST:
            dataDict[k] = request.POST.get(k)

        try:
            dataDict.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            pass

        self.getAuthToken(dataDict)

        failCounter = LecturerCounters.START_QUIZ_FAIL

        dto = StartQuizRequestDTO.fromDict(dataDict)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.lecturerServices.startQuiz(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class StartSpaceRaceView(BaseView):
    """
    class that handles the start space race api
    """
    http_method_names = ["post"]

    @statsd.timed(LecturerCounters.START_SPACE_RACE)
    def post(self, request, *args, **kwargs):
        """
        handler for post for start space race api
        :param request:
        :param args:
        :param kwargs:
        """

        failCounter = LecturerCounters.START_SPACE_RACE_FAIL

        dataDict = dict()

        try:
            dataDict.update(ujson.loads(request.body))
        except Exception as e:
            logger.error(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        self.getAuthToken(dataDict)

        dto = StartSpaceRaceRequestDTO.fromDict(dataDict)
        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.lecturerServices.startSpaceRace(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class TeacherProfileView(BaseView):
    """
    class that handles the update teacher profile
    """
    http_method_names = ["post", "get"]

    @statsd.timed(LecturerCounters.GET_TEACHER_PROFILE)
    def get(self, request, *args, **kwargs):
        """
        handler for HTTP GET for display teacher profile
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        dataDict = dict()
        self.getAuthToken(dataDict)

        dataDict.update(request.GET)
        dataDict.update(kwargs)

        failCounter = LecturerCounters.GET_TEACHER_PROFILE_FAIL

        resp = request.lecturerServices.getProfileInfo(request, dataDict)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)

    @statsd.timed(LecturerCounters.UPDATE_TEACHER_PROFILE)
    def post(self, request, *args, **kwargs):
        """
        handler for HTTP POST for update teacher profile
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = LecturerCounters.UPDATE_TEACHER_PROFILE_FAIL
        try:
            dataDict = ujson.loads(request.body)
        except Exception as e:
            return self.render_json_response(BaseError.INVALID_REQUEST, statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        self.getAuthToken(dataDict)

        resp = request.lecturerServices.updateTeacherProfile(request, dataDict)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class ChangePasswordView(BaseView):
    """service to change the user password
        request:
            auth_token
            password
            new_password
    """
    http_method_names = ["post"]

    @statsd.timed(LecturerCounters.CHANGE_PASSWORD)
    def post(self, request, *args, **kwargs):
        """
        :params request:

        :return HttpResponse
        """
        dataDict = dict()

        self.getAuthToken(dataDict)

        for k in request.POST:
            dataDict[k] = request.POST.get(k)

        failCounter = LecturerCounters.CHANGE_PASSWORD_FAIL

        resp = request.lecturerServices.changePassword(request, dataDict)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class ChangeEmailView(BaseView):
    """service to change a user's email address
        request:
            new_email
            auth_token
    """
    @statsd.timed(LecturerCounters.CHANGE_EMAIL)
    def post(self, request, *args, **kwargs):
        """
        """

        dataDict = dict()
        for k in request.POST:
            dataDict[k] = request.POST.get(k)

        self.getAuthToken(dataDict)

        failCounter = LecturerCounters.CHANGE_EMAIL_FAIL

        resp = request.lecturerServices.changeEmail(request, dataDict)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class DuplicateQuizView(BaseView):
    """
    class that handles the duplicate quiz api
    """
    http_method_names = ["post"]

    @statsd.timed(LecturerCounters.DUPLICATE_QUIZ)
    def post(self, request, *args, **kwargs):
        """
        handler for HTTP POST for duplicate quiz api
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = LecturerCounters.DUPLICATE_QUIZ_FAIL

        dataDict = dict()
        self.getAuthToken(dataDict)

        try:
            dataDict.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        dto = DuplicateQuizRequestDTO.fromDict(dataDict)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.lecturerServices.duplicateQuiz(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class TeacherHandRaiseView(BaseView):
    """

    """

    http_method_names = ["post"]

    @statsd.timed(LecturerCounters.HANDLE_HANDRAISE)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        dataDict = dict()
        self.getAuthToken(dataDict)

        failCounter = LecturerCounters.HANDLE_HANDRAISE_FAIL

        try:
            dataDict.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        dto = TeacherHandraiseRequestDTO.fromDict(dataDict)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.lecturerServices.handleHandraise(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class PaymentReceiptView(BaseView):
    """

    """

    http_method_names = ["get"]

    @statsd.timed(LecturerCounters.GET_PAYMENT_RECEIPT)
    def get(self, request, *args, **kwargs):
        """
        download or email the receipt
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        dataDict = dict()
        self.getAuthToken(dataDict)

        failCounter = LecturerCounters.GET_PAYMENT_RECEIPT_FAIL

        for k in request.GET:
            dataDict[k] = request.GET.get(k)

        dto = PaymentRequestDTO.fromDict(dataDict)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.lecturerServices.requestPaymentReceipt(request, dto)

        if type(resp[0]) is dict and resp[0].get("resp") is not None:
            return resp[0]["resp"]
        else:
            return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class ScoreSettingsView(BaseView):
    """

    """

    http_method_names = ["get", "post", "put"]

    @statsd.timed(LecturerCounters.GET_SCORE_SETTINGS)
    def get(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        failCounter = LecturerCounters.GET_SCORE_SETTINGS_FAIL
        dataDict = dict()
        self.getAuthToken(dataDict)

        for k in request.GET:
            dataDict[k] = request.GET.get(k)

        dto = GetScoreSettingsRequest.fromDict(dataDict)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.lecturerServices.getScoreSettings(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)

    @statsd.timed(LecturerCounters.CREATE_SCORE_SETTINGS)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        dataDict = dict()
        self.getAuthToken(dataDict)

        failCounter = LecturerCounters.CREATE_SCORE_SETTINGS_FAIL

        try:
            dataDict.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        dto = CreateScoreSettingsRequest.fromDict(dataDict)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.lecturerServices.createScoreSettings(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)

    @statsd.timed(LecturerCounters.UPDATE_SCORE_SETTINGS)
    def put(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        dataDict = dict()
        self.getAuthToken(dataDict)

        failCounter = LecturerCounters.UPDATE_SCORE_SETTINGS_FAIL

        try:
            dataDict.update(kwargs)
            dataDict.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        dto = UpdateScoreSettingsRequest.fromDict(dataDict)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.lecturerServices.updateScoreSettings(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)


class PostponeNotificationsView(BaseView):
    """

    """

    http_method_names = ["post"]

    @statsd.timed(LecturerCounters.POSTPONE_NOTIFICATIONS)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        dataDict = dict()
        self.getAuthToken(dataDict)

        failCounter = LecturerCounters.POSTPONE_NOTIFICATIONS_FAIL

        try:
            dataDict.update(ujson.loads(request.body))
        except Exception as e:
            logger.debug(exceptionStack(e))
            return self.render_json_response(BaseError.INVALID_REQUEST,
                                             statusCode=status.HTTP_400_BAD_REQUEST,
                                             failCounter=failCounter)

        dto = PostponeNotificationRequestDTO.fromDict(dataDict)

        if type(dto) is tuple:
            return self.render_json_response(dto[0], statusCode=dto[1], failCounter=failCounter)

        resp = request.lecturerServices.postponeNotification(request, dto)

        return self.render_json_response(resp[0], statusCode=resp[1], failCounter=failCounter)