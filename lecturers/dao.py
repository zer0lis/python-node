# coding=utf-8
from common.base_dao import BaseModel, BaseDao, retry
from common.socrative_api import exceptionStack

from psycopg2 import OperationalError
import logging
from common.counters import DatadogThreadStats

statsd = DatadogThreadStats.STATS

logger = logging.getLogger(__name__)


class LicenseModel(BaseModel):
    """
    model for licenses
    """

    ID = "id"
    BUYER_ID = "buyer_id"
    KEY = "key"
    EXPIRATION_DATE = "expiration_date"
    COUPON_ID = "coupon_id"
    AUTO_RENEW = "auto_renew"
    YEARS = "years"
    PRICE_PER_SEAT = "price_per_seat"
    CUSTOMER_ID = "customer_id"

    FIELD_COUNT = 10

    def __init__(self):
        """
        Constructor
        """
        self.id = None
        self.buyer_id = None
        self.key = None
        self.expiration_date = None
        self.coupon_id = None
        self.auto_renew = None
        self.years = None
        self.price_per_seat = None
        self.customer_id = None

    def toDict(self):
        """
        return: LicenseModel
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.BUYER_ID] = self.buyer_id
        obj[self.KEY] = self.key
        obj[self.EXPIRATION_DATE] = self.expiration_date
        obj[self.COUPON_ID] = self.coupon_id
        obj[self.AUTO_RENEW] = self.auto_renew
        obj[self.YEARS] = self.years
        obj[self.PRICE_PER_SEAT] = self.price_per_seat
        obj[self.CUSTOMER_ID] = self.customer_id
        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """
        construct the model
        """

        model = LicenseModel()
        model.id = dictObj.get(cls.ID)
        model.buyer_id = dictObj.get(cls.BUYER_ID)
        model.key = dictObj.get(cls.KEY)
        model.expiration_date = dictObj.get(cls.EXPIRATION_DATE)
        model.coupon_id = dictObj.get(cls.COUPON_ID)
        model.auto_renew = dictObj.get(cls.AUTO_RENEW, "{}")
        model.years = dictObj.get(cls.YEARS)
        model.price_per_seat = dictObj.get(cls.PRICE_PER_SEAT)
        model.customer_id = dictObj.get(cls.CUSTOMER_ID)

        return model


class LicenseDao(BaseDao):
    """
    dao class for handling license queries
    """

    FETCH_MANY_SIZE = 100
    MODEL = LicenseModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(LicenseDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.license.has_receipt.time')
    def hasReceipt(self, user_id):
        """
        checks if the user has bought a PRO license
        :param user_id:
        :return: True if there is a payment for this user, False otherwise
        :rtype: bool
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT l.id from licenses as l join license_transactions as t on t.license_id = l.id """
                               """where l.buyer_id=%s and t.stripe_charge_response IS NOT NULL LIMIT 1""",
                               (user_id,))
                return cursor.rowcount == 1

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.license.has_license.time')
    def hasLicense(self, user_id):
        """
        checks if the user has bought a PRO license
        :param user_id:
        :return: True if there is a payment for this user, False otherwise
        :rtype: bool
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT l.id from licenses as l left outer join license_activations as la on """
                               """la.license_id=l.id where l.expiration_date >= CURRENT_DATE - INTERVAL '30 days' and"""
                               """ (l.buyer_id=%s or la.user_id=%s) LIMIT 1""", (user_id, user_id))
                return cursor.rowcount == 1

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.license.has_auto_renew.time')
    def hasAutoRenew(self, user_id):
        """
        gets the value of the auto_renew column for the user's license
        :param user_id:
        :return: True if auto_renew is TRUE, False otherwise
        :rtype: bool
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT l.auto_renew FROM licenses AS l LEFT OUTER JOIN license_activations AS la ON """
                               """la.license_id = l.id WHERE l.expiration_date >= CURRENT_DATE - INTERVAL '30 days' AND"""
                               """ (l.buyer_id = %s OR la.user_id = %s) LIMIT 1""", (user_id, user_id))
                if cursor.rowcount < 1:
                    return None

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise
    
    @retry
    @statsd.timed('socrative.dao.license.get_license_data.time')
    def getLicenseData(self, user_id):
        """
        gets the payment data for building the receipt
        :param user_id:
        :return: True if there is a payment for this user, False otherwise
        :rtype: bool
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT t.stripe_charge_response, l.id, l.years, l.key, l.expiration_date from licenses as l """
                               """join license_transactions as t on t.license_id = l.id """
                               """where l.buyer_id=%s and """
                               """t.stripe_charge_response IS NOT NULL ORDER BY l.id DESC """
                               """LIMIT 1""", (user_id,))
                if cursor.rowcount < 1:
                    return None

                return cursor.fetchone()

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.license.get_last_license_key.time')
    def getLastLicenseKey(self, user_id):
        """
        gets the payment data for building the receipt
        :param user_id:
        :return: True if there is a payment for this user, False otherwise
        :rtype: bool
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT "key" from licenses as l LEFT join license_activations as la """
                               """on la.license_id=l.id where la.user_id=%s OR l.buyer_id = %s ORDER BY"""
                               """ l.expiration_date DESC LIMIT 1""",  (user_id, user_id,))
                if cursor.rowcount < 1:
                    return None

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.license.get_expiration_dates.time')
    def getExpirationDates(self, user_id):
        """
        returns the expiration date when the user  can be both a regular user and/or buyer of pro licenses
        :param user_id:
        :return: True if there is a payment for this user, False otherwise
        :rtype: bool
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""SELECT l.expiration_date from licenses as l left outer join license_activations as """
                               """la on la.license_id=l.id where la.user_id=%s ORDER BY l.expiration_date DESC """
                               """LIMIT 1""", (user_id,))

                user_expiration_date = None
                buyer_expiration_date = None
                if cursor.rowcount == 1:
                    user_expiration_date = cursor.fetchone()[0]

                cursor.execute("""SELECT expiration_date from licenses where buyer_id = %s ORDER BY expiration_date"""
                               """ DESC LIMIT 1""", (user_id,))
                if cursor.rowcount == 1:
                    buyer_expiration_date = cursor.fetchone()[0]

                return user_expiration_date, buyer_expiration_date

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.license.get_licenses_by_ids.time')
    def getLicensesByIds(self, licenseIds):
        """
        :param licenseIds:
        :return: dict
        :rtype: dict
        """

        if not licenseIds:
            return dict()

        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute(
                    """select l.id, l.buyer_id, l.coupon_id, l.auto_renew, l.years, l.price_per_seat, l.key, l.expiration_date, """
                    """t.stripe_charge_response from licenses as l left join license_transactions as t on t.license_id = l.id where l.id in %s""",
                    (tuple(licenseIds),))
                return dict((row[0],
                             (row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8])
                             ) for row in cursor.fetchall())

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    @statsd.timed('socrative.dao.license.get_licenses_for_ids.time')
    def getLicensesForIds(self, licenseIds):
        """
        returns license data for the licenses specified in the licenseIds list
        :param user_ids:
        :return: dict
        :rtype: dict
        """

        if not licenseIds:
            return dict()

        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute(
                    """select l.id, la.user_id, l."key", l.expiration_date, l.buyer_id FROM licenses  AS l INNER JOIN"""
                    """ license_activations as la on la.license_id=l.id WHERE l.id in %s""",
                    (tuple(licenseIds),))
                return dict((row[0], (row[1], row[2], row[3], row[4])) for row in cursor.fetchall())

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    def getSeats(self, license_id):
        """
        gets the number of seats for one license
        :param user_id:
        :return: True if there is a payment for this user, False otherwise
        :rtype: bool
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""SELECT seats FROM license_transactions WHERE license_id=%s""", (license_id,))
                if cursor.rowcount < 1:
                    return None

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise


class NotificationModel(BaseModel):
    """

    """

    ID = "id"
    TYPE = "type"
    SHOW = "show"
    IS_HIDDEN = "is_hidden"
    USER_ID = "user_id"
    RECHECK_AT = "recheck_at"

    PRO_ENDING = 0
    PRO_ENDED = 1

    FIELD_COUNT = 6

    def __init__(self):
        """

        """
        self.id = None
        self.type = None
        self.show = None
        self.is_hidden = None
        self.user_id = None
        self.recheck_at = None

    def toDict(self):
        """

        :return:
        """

        obj = dict()
        obj[self.ID] = self.id
        obj[self.TYPE] = self.type
        obj[self.SHOW] = self.show
        obj[self.IS_HIDDEN] = self.is_hidden
        obj[self.USER_ID] = self.user_id
        obj[self.RECHECK_AT] = self.recheck_at

        return obj

    @classmethod
    def fromDict(cls, dictObj):
        """

        :param dictObj:
        :return:
        """
        model = NotificationModel()
        model.id = dictObj.get(cls.ID)
        model.type = dictObj.get(cls.TYPE)
        model.show = dictObj.get(cls.SHOW)
        model.is_hidden = dictObj.get(cls.IS_HIDDEN)
        model.user_id = dictObj.get(cls.USER_ID)
        model.recheck_at = dictObj.get(cls.RECHECK_AT)

        return model


class NotificationDao(BaseDao):
    """
    dao class for payment notifications
    """

    FETCH_MANY_SIZE = 100
    MODEL = NotificationModel

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(NotificationDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.notification.hide.time')
    def hide(self, notification_type_list, user_id):
        """

        :param notification_type_list:
        :param user_id:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE notifications SET is_hidden=TRUE where user_id = %s AND "type" in %s and is_hidden is FALSE""",
                               (user_id, tuple(notification_type_list)))

                self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.notification.get_notification.time')
    def getNotification(self, notification_types, user_id):
        """

        :param notification_types:
        :param user_id:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute(
                    """SELECT * from notifications where user_id=%s and "type" in %s and is_hidden is FALSE""",
                    (user_id, tuple(notification_types)))
                if cursor.rowcount < 1:
                    return None
                if cursor.rowcount > 1:
                    logger.warn("There are too many notifications available for user %d" % user_id)

                return self.fetchOneAsModel(cursor)

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.notification.insert.time')
    def insert(self, notification):
        """

        :param notification:
        :return:
        """

        error = None
        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""INSERT INTO notifications ("type", "show", "is_hidden", "user_id", "recheck_at") """
                               """VALUES (%s, TRUE, FALSE, %s, %s) RETURNING id""",
                               (notification.type,
                                notification.user_id,
                                notification.recheck_at))

                return cursor.fetchone()[0]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            error = 0
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            error = 1
            raise
        finally:
            if error > 0:
                self.connection.rollback()
            elif error is None:
                self.connection.commit()

    @retry
    @statsd.timed('socrative.dao.notification.show_notification.time')
    def showNotification(self, notificationModel):
        """

        :param notificationModel:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE notifications SET show=TRUE, recheck_at=%s where id=%s and is_hidden is FALSE""",
                               (notificationModel.recheck_at, notificationModel.id))

                self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.notification.reset_notifications.time')
    def resetNotifications(self, user_id, notification_types):
        """

        :param notification_types:
        :param user_id:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE notifications SET show=TRUE WHERE user_id=%s and "type" in %s and"""
                               """ is_hidden is FALSE""", (user_id, tuple(notification_types)))

                self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            raise

    @retry
    @statsd.timed('socrative.dao.notification.postpone_notification.time')
    def postponeNotification(self, notification_id, recheck_at):
        """

        :param notification_id:
        :param recheck_at:
        :return:
        """

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""UPDATE notifications SET show=FALSE, recheck_at=%s WHERE id=%s and is_hidden is FALSE""",
                               (recheck_at, notification_id))

                if cursor.rowcount < 1:
                    raise BaseDao.NotUpdated

                self.connection.commit()
        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            self.connection.rollback()
            raise


# =================================
#       Dao classes for accounting methods
# =================================

class LicenseTransactionDao(BaseDao):
    """
    dao class for payment notifications
    """

    FETCH_MANY_SIZE = 100

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(LicenseTransactionDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    def getTransactions(self, start_date=None, end_date=None):
        """
        gets all the transactions from the license transaction table between start date and end date
        :param start_date:
        :param end_date:
        :return:
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                # get all license ids purchased between start_date and end_date
                query = cursor.mogrify("""select license_id, seats, price, purchase_date, id from license_transactions""")
                if start_date or end_date:
                    query += " where"

                if start_date and end_date:
                    query += cursor.mogrify(""" purchase_date >= %s and purchase_date < %s""", (start_date, end_date))
                elif start_date:
                    query += cursor.mogrify(""" purchase_date >= %s""", (start_date,))
                elif end_date:
                    query += cursor.mogrify(""" purchase_date < %s""", (end_date,))

                cursor.execute(query)
                if cursor.rowcount == 0:
                    return dict()

                # {<license_id>: (<seats>,<price>,<purchase_date>,<id>),...}
                return dict((row[0], (row[1], row[2], row[3], row[4])) for row in cursor.fetchall())

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise

    @retry
    def getExpiringLicenses(self, start_date=None, end_date=None):
        """
        gets all the transactions from the license transaction table between start date and end date
        :param start_date:
        :param end_date:
        :return:
        """
        try:
            with self.readOnlyConnection.cursor() as cursor:
                # get all license ids purchased between start_date and end_date
                query = cursor.mogrify(
                    """select license_id from license_transactions""")
                if start_date or end_date:
                    query += " where"

                if start_date and end_date:
                    query += cursor.mogrify(""" purchase_date >= %s and purchase_date < %s""", (start_date, end_date))
                elif start_date:
                    query += cursor.mogrify(""" purchase_date >= %s""", (start_date,))
                elif end_date:
                    query += cursor.mogrify(""" purchase_date < %s""", (end_date,))

                cursor.execute(query)
                if cursor.rowcount == 0:
                    return dict()

                # {<license_id>: None}
                return [row[0] for row in cursor.fetchall()]

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise


class LicenseActivationDao(BaseDao):
    """
    dao class for payment notifications
    """

    FETCH_MANY_SIZE = 100

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(LicenseActivationDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.license_activation.get_license_activations.time')
    def getLicenseActivations(self, licenseIds):
        """
        gets the latest license a user activated
        :param licenseIds:
        :return:
        """

        if not licenseIds:
            return dict()

        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute(
                    """select user_id, max(license_id), count(id) from license_activations where user_id in """
                    """(select distinct user_id from license_activations where license_id in %s) group by user_id""",
                    (tuple(licenseIds), ))

                return dict((row[0], (row[1], row[2])) for row in cursor.fetchall())

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception:
            raise


class CouponDao(BaseDao):
    """
    dao class for payment notifications
    """

    FETCH_MANY_SIZE = 100

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(CouponDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.coupons.get_coupon_names.time')
    def getCouponNames(self, couponIds):
        """
        gets the coupon names by their ids
        :param couponIds
        :return:
        """

        if not couponIds:
            return dict()
        try:
            with self.readOnlyConnection.cursor() as cursor:
                cursor.execute("""select id, name from coupons where id in %s""", (tuple(couponIds),))

                return dict((row[0], row[1]) for row in cursor.fetchall())

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise
        except Exception as e:
            logger.error(exceptionStack(e))
            raise


class FolderDao(BaseDao):
    """
    dao class for payment notifications
    """

    FETCH_MANY_SIZE = 100

    def __init__(self, dbConfigCtx, dbConfigCtxReadOnly, connMgr):
        """
        constructor
        """

        super(FolderDao, self).__init__(dbConfigCtx, dbConfigCtxReadOnly, connMgr)

    @retry
    @statsd.timed('socrative.dao.folder.get_folder_owners_during_period.time')
    def getFolderOwnersDuringPeriod(self, start_date, end_date):
        """

        :param start_date:
        :param end_date:
        :return:
        """

        if not start_date:
            return dict()

        try:
            with self.readOnlyConnection.cursor() as cursor:
                query = cursor.mogrify("""SELECT user_id, MAX(last_updated) as last_login FROM """
                                       """folders WHERE (created_date >=%s OR last_updated >=%s)""",
                                       (start_date, start_date))
                if end_date:
                    query += cursor.mogrify(""" AND last_updated < %s""", (end_date, ))

                query += " GROUP BY user_id"
                cursor.execute(query)

                return dict((row[0], row[1]) for row in cursor.fetchall())

        except OperationalError as e:
            # the connection is dead
            logger.error(exceptionStack(e))
            self.connection = None
            raise

        except Exception:
            raise