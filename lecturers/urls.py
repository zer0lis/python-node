# -*- coding: utf-8 -*-
from django.conf.urls import url
from lecturers.views import TeacherProfileView, ChangePasswordView, PaymentReceiptView
from lecturers.views import ChangeEmailView, StartQuizView, StartSpaceRaceView, StartQuickQuestionView
from lecturers.views import ConvertQuickQuestionToVoteView, StartExitTicketView, DuplicateQuizView, TeacherHandRaiseView
from lecturers.views import ScoreSettingsView, PostponeNotificationsView

urlpatterns = [
    url(r'^teacher-profile/?$', TeacherProfileView.as_view()),
    url(r'^api/payment/receipt/', PaymentReceiptView.as_view()),

    # apis for each call
    url(r'^api/change-password/$', ChangePasswordView.as_view()),
    url(r'^api/change-email/$', ChangeEmailView.as_view()),

    url(r'^api/start-quiz/?$', StartQuizView.as_view()),
    url(r'^api/start-space-race/?$', StartSpaceRaceView.as_view()),
    url(r'^api/start-quick-question/?$', StartQuickQuestionView.as_view()),
    url(r'^api/convert-quick-question-to-vote/?$', ConvertQuickQuestionToVoteView.as_view()),
    url(r'^api/start-exit-ticket/?$', StartExitTicketView.as_view()),
    url(r'^api/duplicate-quiz/?$', DuplicateQuizView.as_view()),
    url(r'^api/handraise/?$', TeacherHandRaiseView.as_view()),
    url(r'^api/score-settings/?$', ScoreSettingsView.as_view()),
    url(r'^api/score-settings/(?P<pk>[0-9]+)/?$', ScoreSettingsView.as_view()),
    url(r'^api/notifications/postpone/', PostponeNotificationsView.as_view())
]