#!/usr/bin/which python -B

import pyinotify
import os
import datetime
from multiprocessing import Process
import sys
import logging

class EventHandler(pyinotify.ProcessEvent):
    """

    """
    def __init__(self, pevent=None, **kwargs):
        """
        """
        super(EventHandler, self).__init__(pevent, **kwargs)
        self.my_handler = kwargs["my_callback"]

    def process_default(self, event):
        """
        """
        self.my_handler(event)


class FileWatcher(object):
    """

    """

    def __init__(self):
        """

        :return:
        """

        base_dir= os.path.abspath(os.path.dirname(__file__))
        self.extensions = set([".property", ".conf", ".py", ".css", ".js", ".less", ".html"])
        self.watch_dirs = os.listdir(base_dir)
        exclude_names = ["identity.pub", "requirements.txt", ".git", "Art", "admin_reports", ".idea", "identity",
                         "tools", ".gitignore", "exclude_list.txt", "pylint.cfg", "manager.py"]
        self.ignore_events = False
        for name in exclude_names:
            try:
                self.watch_dirs.remove(name)
            except Exception:
                pass
        for i in range(len(self.watch_dirs)):
            self.watch_dirs[i] = base_dir + "/" + self.watch_dirs[i]

        self.watch_dirs = dict((a, None) for a in self.watch_dirs)

        self.evt_notifier = EventHandler(my_callback=self.handler)
        self.notifier = None
        self.process = None
        self.arguments = []
        self.startDate = None

    def handler(self, event):
        """

        :param event:
        :return:
        """
        if (datetime.datetime.utcnow() - self.startDate).seconds < 20:
            return

        if self.ignore_events is False:
            for path in self.watch_dirs:
                if path in event.path:
                    if event.mask in (128, 2, 256, 512):
                        for ext in self.extensions:
                            if event.name.endswith(ext):
                                print event.name
                                self.ignore_events = True
                                self.process.terminate()
                                self.process.join()
                                self.process = Process(target=target, args=self.arguments)
                                self.process.start()
                                self.startDate = datetime.datetime.utcnow()
                                self.ignore_events = False
                                break

    def start(self, process, arguments):
        """
        :return:
        """
        wm = pyinotify.WatchManager()

        # mask = pyinotify.EventsCodes.FLAG_COLLECTIONS["OP_FLAGS"].get("IN_MODIFY")

        self.notifier = pyinotify.ThreadedNotifier(wm, self.evt_notifier)

        for d in self.watch_dirs:
            wm.add_watch(d, 898, rec=True)

        self.notifier.start()
        self.process = process
        self.arguments = arguments

    def stop(self):
        """

        :return:
        """

        self.notifier.stop()

def target(*arguments):
    """

    :param arguments:
    :return:
    """

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "socrative.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(list(arguments))

def startServer(args):

    p = Process(target=target, args=args)
    p.start()
    return p

if __name__ == "__main__":
    import time
    fw = FileWatcher()
    p = startServer(sys.argv)
    fw.startDate = datetime.datetime.utcnow()
    fw.start(p, sys.argv)
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break
        except Exception:
            break
    fw.stop()