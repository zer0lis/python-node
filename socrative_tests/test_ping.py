from django.test.client import Client
from unittest import TestCase
import django

# django.setup()

class TestPing(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.client = Client()

    @classmethod
    def tearDownClass(cls):
        cls.client = None # not needed, just to show how tearDownClass and setUpClass work together.

    def test_ping_ok(self):
        resp = self.client.get("/api/ping/")
        self.assertEquals(resp.status_code, 200)
        self.assertEquals(resp.content, '"OK"')