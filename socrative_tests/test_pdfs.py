# coding=utf-8

from unittest import TestCase, skip
from quizzes.pdf_generator import BasePdfClass


class TestPdfFormatting(TestCase):
    """
    test the string formatting methods
    """

    @classmethod
    def setUpClass(cls):
        """

        :return:
        """
        cls.obj = BasePdfClass()

    def test_utf8_latin_string(self):
        """

        :return:
        """

        result = self.obj.formatText("Example 1")
        self.assertEqual(result, "<div><font  face=OpenSans>Example 1</font></div>")

    def test_unicode_normal_strings(self):
        """

        :return:
        """

        s = u"Example 1"
        # format with default font
        result = self.obj.formatText(s)

        # the results should be like <font face=OpenSans>Example 1</font>
        self.assertEquals(result, "<div><font  face=OpenSans>Example 1</font></div>")

    def test_unicode_russian_normal_strings(self):
        """

        :return:
        """

        result = self.obj.formatText(u"Российская Федерация")
        self.assertEquals(result, u"<div><font  face=OpenSans>Российская Федерация</font></div>")

    def test_unicode_chineese_normal_strings(self):
        """

        :return:
        """

        result = self.obj.formatText(u"汉语/漢語; Hànyǔ or 中文; Zhōngwén")
        self.assertEqual(result, u"<div><font  face=chinese>汉语/漢語; Hànyǔ or 中文; Zhōngwén</font></div>")

    def test_unicode_normal_latin_in_p_tags(self):
        """

        :return:
        """

        result = self.obj.formatText(u"<p>Example 1</p>")

        self.assertEqual(result, u"<div><font  face=OpenSans>Example 1<br></br></font></div>")

    def test_unicode_normal_latin_from_wysiwyg(self):
        """

        :return:
        """

        result = self.obj.formatText(u"<p>Example 1<br data-mce-bogus=\"1\"></p>")

        self.assertEqual(result, "<div><font  face=OpenSans>Example 1<br></br></font></div>")

    def test_unicode_chinese_in_p_tags(self):
        """

        :return:
        """

        result = self.obj.formatText(u"<p>汉语/漢語; Hànyǔ or 中文; Zhōngwén</p>")

        self.assertEqual(result, u"<div><font  face=chinese>汉语/漢語; Hànyǔ or 中文; Zhōngwén<br></br></font></div>")

    def test_unicode_latin_entire_text_bold(self):
        """

        :return:
        """
        result = self.obj.formatText(u"<b>Example 1</b>")
        self.assertEqual(result, "<font  face=OpenSans><b>Example 1</b></font>")

    def test_unicode_latin_middle_text_bold(self):
        """

        :return:
        """
        result = self.obj.formatText(u"Exa<b>mp</b>le 1")
        self.assertEqual(result, "<div><font  face=OpenSans>Exa<b>mp</b>le 1</font></div>")

    def test_scenario1_latin_text_in_div(self):
        """

        :return:
        """
        result = self.obj.formatText(u"<div><u><i>tyy</i></u><br></br></div>")
        self.assertEqual(result, u"<div><font  face=OpenSans><u><i>tyy</i></u><br></br></font></div>")

    def test_scenario2_hebrew_text_formatted_in_div(self):
        """

        :return:
        """
        result = self.obj.formatText(u"<div><i>italics תשובה זו היא באותיות נטויות .<br></br></i><br></br></div>")
        self.assertEqual(result, u"<div><font  face=hebrew><i>italics תשובה זו היא באותיות נטויות .<br></br></i><br></br></font></div>")

    def test_weird_infinite_loop(self):
        """

        :return:
        """

        result = self.obj.formatText(u"<div>Kiln, <i><b></b><u>Humanity,</u><b> </b></i>1910, Art Nouveau, he re-worked the skull many times<br></br></div>")
        self.assertEqual(result, u"<div><font  face=OpenSans>Kiln, <i><b></b><u>Humanity,</u><b> </b></i>1910, Art Nouveau, he re-worked the skull many times<br></br></font></div>")

        result = self.obj.formatText(u"<div>For an equation of the form <i>a<sup></sup>x<sup>2 </sup>+ bx + c</i>,<sup></sup> the graph will open up if :<br></br></div>")
        self.assertEqual(result, u"<div><font  face=OpenSans>For an equation of the form <i>a<sup></sup>x<sup>2 </sup>+ bx + c</i>,<sup></sup> the graph will open up if :<br></br></font></div>")

        result = self.obj.formatText(u"""<p><em>It in the</em>.</p><p><br data-mce-bogus="1"></p><p><strong>The reading above explores the foundations of:</strong> <br> </p>""")
        self.assertEqual(result, "<div><font  face=OpenSans><i>It in the</i>.<br></br><br></br><b>The reading above explores the foundations of:</b> <br></br> <br></br></font></div>")

        result = self.obj.formatText(u"""<p><strong>Based on the chart, what most likely occurred?</strong><br> </p>""")
        self.assertEqual(result, "<div><font  face=OpenSans><b>Based on the chart, what most likely occurred?</b><br></br> <br></br></font></div>")

        result = self.obj.formatText(u"""<p><em>Colonies ing.&nbsp; hed pro sold their resources to.</em></p><p><em><br data-mce-bogus="1"></em></p><p><span id="_mce_caret" data-mce-bogus="true"><strong>The above is an example of:</strong>\ufeff</span></p><p></p>""")
        self.assertEqual(result, u"<div><font  face=OpenSans><i>Colonies ing.  hed pro sold their resources to.</i><br></br><i><br></br></i><br></br><b>The above is an example of:</b>\ufeff<br></br><br></br></font></div>")

        result = self.obj.formatText(u"""<p><em>"My  good"</em></p><p><em>- Thomas Paine in: Rights of Man, 1791</em></p><p><em><br data-mce-bogus="1"></em></p><p><strong>Which  reflect?<em><br> </em></strong></p>""")
        self.assertEqual(result, """<div><font  face=OpenSans><i>"My  good"</i><br></br><i>- Thomas Paine in: Rights of Man, 1791</i><br></br><i><br></br></i><br></br><b>Which  reflect?<i><br></br> </i></b><br></br></font></div>""")

        result = self.obj.formatText(u"""<p><em>In e activities of </em></p><p><em><br data-mce-bogus="1"></em></p><p><strong>The passage above best describes the:<em><br> </em></strong></p>""")
        self.assertEqual(result, """<div><font  face=OpenSans><i>In e activities of </i><br></br><i><br></br></i><br></br><b>The passage above best describes the:<i><br></br> </i></b><br></br></font></div>""")

        result = self.obj.formatText(u"""'<p><strong>Forming colonies, s of:</strong><br> </p>""")
        self.assertEqual(result, u"""<div><font  face=OpenSans>'<b>Forming colonies, s of:</b><br></br> <br></br></font></div>""")

        result = self.obj.formatText(u"""'<p><strong>The list above are all__________that could be attributed to__________</strong><br> </p>""")
        self.assertEqual(result, u"""<div><font  face=OpenSans>'<b>The list above are all__________that could be attributed to__________</b><br></br> <br></br></font></div>""")

        result = self.obj.formatText(u"""<p><em>It so advantage.</em></p><p><br data-mce-bogus="1"></p><p><strong>The reading above explores the foundations of:</strong></p>""")
        self.assertEqual(result, u"""<div><font  face=OpenSans><i>It so advantage.</i><br></br><br></br><b>The reading above explores the foundations of:</b><br></br></font></div>""")

        result = self.obj.formatText(u"""<p><strong>The group o as a(n)</strong><br> </p>""")
        self.assertEqual(result, u"""<div><font  face=OpenSans><b>The group o as a(n)</b><br></br> <br></br></font></div>""")

        result = self.obj.formatText(u"""<p><em>The \'Scramble for Africa\' or \'Race for Africa\' was a t  nations.</em></p><p><em><br data-mce-bogus="1"></em></p><p><span id="_mce_caret" data-mce-bogus="true">\ufeff<strong>Imperialism  source,</strong></span></p>""")
        self.assertEqual(result, u"""<div><font  face=OpenSans><i>The 'Scramble for Africa' or 'Race for Africa' was a t  nations.</i><br></br><i><br></br></i><br></br>\ufeff<b>Imperialism  source,</b><br></br></font></div>""")

        result = self.obj.formatText(u"""'<p><strong>What were some of the cons (negatives) of industrialization and the shift to factory work?</strong><br><strong> Check ALL that are correct.</strong><br> </p>""")
        self.assertEqual(result, u"""<div><font  face=OpenSans>'<b>What were some of the cons (negatives) of industrialization and the shift to factory work?</b><br></br><b> Check ALL that are correct.</b><br></br> <br></br></font></div>""")

    @skip("fails on appstaging, but works locally. to investigate")
    def test_text_with_less_and_greater_signs(self):
        """

        :return:
        """
        result = self.obj.formatText("<3mm")
        self.assertEqual(result, u"<div><font  face=OpenSans>&lt;3mm</font></div>")

        result = self.obj.formatText(u"<3mm")
        self.assertEqual(result, u"<div><font  face=OpenSans>&lt;3mm</font></div>")

        result = self.obj.formatText(u"&lt;3mm")
        self.assertEqual(result, "<div><font  face=OpenSans>&lt;3mm</font></div>")

        result = self.obj.formatText(u"<汉语/漢語;")
        self.assertEqual(result, u"<font  face=chinese>&lt;\u6c49\u8bed/\u6f22\u8a9e;</font>")

        result = self.obj.formatText(">3mm")
        self.assertEqual(result, "<div><font  face=OpenSans>&gt;3mm</font></div>")

        result = self.obj.formatText(u">3mm")
        self.assertEqual(result, u"<div><font  face=OpenSans>&gt;3mm</font></div>")

        result = self.obj.formatText(u"&gt;3mm")
        self.assertEqual(result, u"<div><font  face=OpenSans>&gt;3mm</font></div>")

        result = self.obj.formatText(u">汉语/漢語;")
        self.assertEqual(result, u"<div><font  face=chinese>&gt;\u6c49\u8bed/\u6f22\u8a9e;</font></div>")

    def test_text_with_font_tags(self):
        """

        :return:
        """

        result = self.obj.formatText(u"""<div><font><font>Wh is a <u>PREPOSITION</u> in the  sentence?</font></font><br></br><font><b><font><br></br></font></b></font><br></br><font><b><font>The  class.</font></b></font><br></br><br></br><b><font></font></b><br></br></div>""")
        self.assertEqual(result, u"<div><font  face=OpenSans>Wh is a <u>PREPOSITION</u> in the  sentence?<br></br><b><br></br></b><br></br><b>The  class.</b><br></br><br></br></font><b></b><br></br></div>")


    def test_string_in_utf8_which_loops_in_regex(self):
        """

        :return:
        """

        result = self.obj.formatText("""<p></p>\n<p><span class="s1">The officer who will control and deploy the resources of their respective service within a functional or geographical area and implement direction provided by the Tactical Commander is called the Operational Commander.True or false?</span></p>""")
        self.assertEqual(result, u"<div><font  face=OpenSans><br></br><br></br>The officer who will control and deploy the resources of their respective service within a functional or geographical area and implement direction provided by the Tactical Commander is called the Operational Commander.True or false?<br></br></font></div>")