# coding=utf-8
import django

django.setup()

from django.test.client import Client
from unittest import TestCase
import time
import ujson
from socrative_tornado.publish import PubnubWrapper
import logging
import datetime
from socrative_users.models import SocrativeUser
from rooms.models import Room
from quizzes.models import Quiz, Question, Answer
from students.models import StudentResponses
import string
import random
from common.cryptography import Cryptography
alphabet = string.digits + string.lowercase

logger = logging.getLogger(__name__)


class TestStudents(TestCase):
    """
    test student services
    """
    FIND_QUESTION_RESPONSE_URL = '/students/api/responses/'
    SUBMIT_STUDENT_RESPONSE_URL = '/students/api/responses/'
    HIDE_RESPONSE_URL = '/students/api/hide-response/'
    SET_NAME_URL = '/students/api/set-name/'
    AUTO_ASSIGN_TEAM = '/students/api/auto-assign-team/'
    STUDENT_ACTIVITY_STATUS_URL = '/students/api/student-activity-status/'
    FINISH_ACTIVITY_URL = "/students/api/finish-activity/"
    STUDENT_SCORE_URL = "/students/api/student-score/"

    JOIN_URL = "/rooms/api/join/"
    REGISTER_URL = "/users/register/"
    LOGIN_URL = "/users/login/"
    UPDATE_ROOM_NAME_URL = '/rooms/api/update-room-name'
    START_QUIZ_URL = '/lecturers/api/start-quiz'
    START_SPACE_RACE_URL = '/lecturers/api/start-space-race/'
    START_QUICK_QUESTION_URL = '/lecturers/api/start-quick-question/'
    END_ACTIVITY_URL = '/rooms/api/end-current-activity/'

    @classmethod
    def tearDownClass(cls):
        """

        :param cls:
        :return:
        """
        pass

    @classmethod
    def setUpClass(cls):
        """

        :param cls:
        :return:
        """
        cls.client = Client()
        cls.messages = list()

    def mock_publish(self, message):
        self.messages.append(message)
        return True

    def setUp(self):
        email = "test%s@socrativetest.com" % str(time.time())
        password = "12345678"

        user = SocrativeUser(email=email, password=password)
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.date_joined = datetime.datetime.utcnow()
        user.last_login = datetime.datetime.utcnow()
        user.save()

        room_name = str(time.time()).replace('.','')
        room = Room(name=room_name, name_lower=room_name, created_by=user, status=1)
        room.save()

        quiz = Quiz(created_by=user, name=str(time.time()), is_hidden=False, last_updated=datetime.datetime.utcnow())
        quiz.save()
        question1 = Question(question_text="tf question text", created_by=user, type="TF", order=1)
        question2 = Question(question_text="mc question text", created_by=user, type="MC", order=2)
        question3 = Question(question_text="fr question text", created_by=user, type="FR", order=3)
        question4 = Question(question_text="fr question text with correct answer", created_by=user, type="FR", order=4)
        question5 = Question(question_text="mc question text without correct answer", created_by=user, type="MC", order=5)
        answer11 = Answer(is_correct=False, text="False", created_by=user, order=1)
        answer12 = Answer(is_correct=True, text="True", created_by=user, order=2)
        answer21 = Answer(is_correct=True, text="343", created_by=user, order=1)
        answer22 = Answer(is_correct=True, text="434234", created_by=user, order=2)
        answer23 = Answer(is_correct=False, text="adasdsa", created_by=user, order=3)
        answer41 = Answer(is_correct=True, text="true", created_by=user, order=1)
        answer51 = Answer(is_correct=False, text="343", created_by=user, order=1)
        answer52 = Answer(is_correct=False, text="434234", created_by=user, order=2)

        question1.save()
        question2.save()
        question3.save()
        question4.save()
        question5.save()

        quiz.questions.add(question1, question2, question3, question4, question5)
        quiz.soc_number = str(5000000+quiz.id)
        quiz.save()
        answer11.question = question1
        answer12.question = question1
        answer12.save()
        answer11.save()
        question1.answers.add(answer11, answer12)
        question1.quiz_id = quiz.id
        question1.save()
        answer21.question = question2
        answer22.question = question2
        answer23.question = question2
        answer21.save()
        answer22.save()
        answer23.save()
        question2.answers.add(answer21, answer22, answer23)
        question2.quiz_id = quiz.id
        question2.save()
        question3.quiz_id = quiz.id
        question3.save()
        answer41.question = question4
        answer41.save()
        question4.answers.add(answer41)
        question4.quiz_id = quiz.id
        question4.save()
        answer51.question = question5
        answer52.question = question5
        answer51.save()
        answer52.save()
        question5.answers.add(answer51, answer52)
        question5.quiz_id = quiz.id
        question5.save()

        self.auth_token_user = user.auth_token
        self.soc_number = quiz.soc_number
        self.room_name = room.name
        self.question1 = question1
        self.question2 = question2
        self.question3 = question3
        self.question4 = question4
        self.question5 = question5
        self.answer11 = answer11
        self.answer12 = answer12
        self.answer21 = answer21
        self.answer22 = answer22
        self.answer23 = answer23
        self.answer41 = answer41
        self.quiz = quiz
        PubnubWrapper.override_publish(self.mock_publish)

    def tearDown(self):
        """

        :return:
        """
        self.messages = []
        if "ua" in self.client.cookies:
            self.client.cookies.pop("ua")
        if "sa" in self.client.cookies:
            self.client.cookies.pop("sa")

    def checkErrResp(self, resp, statusCode):
        """

        :param resp:
        :param statusCode:
        :return:
        """

        logger.info("RAW OUTPUT: %s, STATUS CODE: %d" % (resp.content, resp.status_code))
        self.assertEqual(resp.status_code, statusCode)
        if statusCode not in (404, 405):
            self.assertIn("error", resp.content)

    def checkOkResp(self, resp, statusCode, obj=None):
        """
        """

        logger.info("RAW OUTPUT: %s, STATUS CODE: %d" % (resp.content if resp._headers.get("content-type")[1] == "application/json" else "{\"msg\":\"Content OK\"}", resp.status_code))
        self.assertEqual(resp.status_code, statusCode)
        self.assertNotIn("error", ujson.loads(resp.content) if resp._headers.get("content-type")[1] == "application/json" else "{\"msg\":\"Content OK\"}")

    def test_student_activity_status_all_questions_missing(self):
        """

        :return:
        """

        PubnubWrapper.override_publish(self.mock_publish)
        d = 'auth_token=%s&soc_num=%s&room_name=%s' % (self.auth_token_user,
                                                       self.soc_number, self.room_name)
        resp = self.client.post(self.START_QUIZ_URL, data=d,
                                content_type='application/x-www-form-urlencoded')

        self.checkOkResp(resp, 200)
        self.activi_id = ujson.loads(resp.content)['id']
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth("anon" + Cryptography.generate_auth_token()[:16])
        resp = self.client.get(self.STUDENT_ACTIVITY_STATUS_URL + "%d/" % self.activi_id)

        self.checkOkResp(resp, 200)

        qList = ujson.loads(resp.content)

        self.assertEqual(len(qList), self.quiz.questions.count())

        self.assertIn(self.question1.question_id, qList)
        self.assertIn(self.question2.question_id, qList)
        self.assertIn(self.question3.question_id, qList)
        self.assertIn(self.question4.question_id, qList)

    def test_student_activity_status_3_questions_missing(self):
        """

        :return:
        """

        PubnubWrapper.override_publish(self.mock_publish)
        d = 'auth_token=%s&soc_num=%s&room_name=%s' % (self.auth_token_user,
                                                       self.soc_number, self.room_name)
        resp = self.client.post(self.START_QUIZ_URL, data=d,
                                content_type='application/x-www-form-urlencoded')

        self.checkOkResp(resp, 200)
        activity_id = ujson.loads(resp.content)['id']

        studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

        name = "Josh Baptiste"
        data = {
            "student_name": name,
            "activity_instance_id": activity_id
        }
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
        resp = self.client.post(self.SET_NAME_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkOkResp(resp, 200)

        d = {'question_id': self.question3.question_id, 'answer_text': "whatever",
             'activity_instance_id': activity_id, 'multi_response': False}
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 201)

        resp = self.client.get(self.STUDENT_ACTIVITY_STATUS_URL + "%d/" % activity_id)

        self.checkOkResp(resp, 200)

        qList = ujson.loads(resp.content)

        self.assertEqual(len(qList), self.quiz.questions.count()-1)

        self.assertIn(self.question1.question_id, qList)
        self.assertIn(self.question2.question_id, qList)
        self.assertNotIn(self.question3.question_id, qList)
        self.assertIn(self.question4.question_id, qList)

        self.client.cookies["sa"] = Cryptography.encryptStudentAuth("anotheruuid")
        resp = self.client.get(self.STUDENT_ACTIVITY_STATUS_URL + "%d/" % activity_id)

        self.checkOkResp(resp, 200)

        qList = ujson.loads(resp.content)

        self.assertEqual(len(qList), self.quiz.questions.count())

        self.assertIn(self.question1.question_id, qList)
        self.assertIn(self.question2.question_id, qList)
        self.assertIn(self.question3.question_id, qList)
        self.assertIn(self.question4.question_id, qList)

    def test_student_activity_status_all_questions_answered(self):
        """

        :return:
        """

        PubnubWrapper.override_publish(self.mock_publish)
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        d = 'soc_num=%s&room_name=%s' % (self.soc_number, self.room_name)
        resp = self.client.post(self.START_QUIZ_URL, data=d,
                                content_type='application/x-www-form-urlencoded')

        self.checkOkResp(resp, 200)
        activity_id = ujson.loads(resp.content)['id']

        studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

        name = "Josh Baptiste"
        data = {
            "student_name": name,
            "activity_instance_id": activity_id
        }
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
        resp = self.client.post(self.SET_NAME_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkOkResp(resp, 200)

        d = {'question_id': self.question3.question_id, 'answer_text': "whatever",
             'activity_instance_id': activity_id, 'multi_response': False}
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 201)

        d = {'question_id': self.question4.question_id, 'answer_text': "whatever too",
             'activity_instance_id': activity_id, 'multi_response': False}
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 201)

        d = {'question_id': self.question1.question_id, 'answer_ids': "%d" % self.answer11.id,
             'activity_instance_id': activity_id, 'multi_response': False}
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 201)

        d = {'question_id': self.question2.question_id, 'answer_ids': "%d" % self.answer21.id,
             'activity_instance_id': activity_id, 'multi_response': False}
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 201)

        resp = self.client.get(self.STUDENT_ACTIVITY_STATUS_URL + "%d/" % activity_id)

        self.checkOkResp(resp, 200)

        qList = ujson.loads(resp.content)

        self.assertEqual(len(qList), 1)

        self.client.cookies["sa"] = Cryptography.encryptStudentAuth("anotheruuid")
        resp = self.client.get(self.STUDENT_ACTIVITY_STATUS_URL + "%d/" % activity_id)

        self.checkOkResp(resp, 200)

        qList = ujson.loads(resp.content)

        self.assertEqual(len(qList), self.quiz.questions.count())

        self.assertIn(self.question1.question_id, qList)
        self.assertIn(self.question2.question_id, qList)
        self.assertIn(self.question3.question_id, qList)
        self.assertIn(self.question4.question_id, qList)

    def test_student_activity_status_error_cases(self):
        """

        :return:
        """

        url = self.STUDENT_ACTIVITY_STATUS_URL
        resp = self.client.get(url)
        self.checkErrResp(resp, 404)

        self.client.cookies["sa"] = Cryptography.encryptStudentAuth("unoarecareid")
        url = self.STUDENT_ACTIVITY_STATUS_URL + "0/"
        resp = self.client.get(url)
        self.checkErrResp(resp, 400)

        url = self.STUDENT_ACTIVITY_STATUS_URL + "100/"
        resp = self.client.post(url)
        self.checkErrResp(resp, 405)

        url = self.STUDENT_ACTIVITY_STATUS_URL + "100/"
        resp = self.client.put(url)
        self.checkErrResp(resp, 405)

    def test_hide_response_ok(self):
        """
        test the ok case for hide response
        :return:
        """

        PubnubWrapper.override_publish(self.mock_publish)
        d = 'auth_token=%s&soc_num=%s&room_name=%s' % (self.auth_token_user,
                                                       self.soc_number, self.room_name)
        resp = self.client.post(self.START_QUIZ_URL, data=d,
                                content_type='application/x-www-form-urlencoded')

        self.checkOkResp(resp, 200)
        activity_id = ujson.loads(resp.content)['id']

        studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

        name = "Josh Baptiste"
        data = {
            "student_name": name,
            "activity_instance_id": activity_id
        }
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
        resp = self.client.post(self.SET_NAME_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkOkResp(resp, 200)

        d = {'question_id': self.question3.question_id, 'answer_text': "whatever",
             'activity_instance_id': activity_id, 'multi_response': False}
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")
        self.checkOkResp(resp, 201)

        sResp = StudentResponses.objects.filter(user_uuid=studentUuid, question_id=self.question3.question_id,
                                               activity_instance_id=activity_id)
        self.assertEqual(sResp.count(), 1)
        self.assertFalse(sResp.first().hidden)
        sRespId = sResp.first().pk

        d = "response_id=%d&room_name=%s" % (sRespId, self.room_name)
        resp = self.client.post(self.HIDE_RESPONSE_URL, data=d, content_type="application/x-www-form-urlencoded")

        self.checkOkResp(resp, 200)

        resp = StudentResponses.objects.get(pk=sRespId)
        self.assertTrue(resp.hidden)

    def test_find_student_responses_ok(self):
        """
        test the ok case for hide response
        :return:
        """

        PubnubWrapper.override_publish(self.mock_publish)
        d = 'auth_token=%s&soc_num=%s&room_name=%s' % (self.auth_token_user,
                                                       self.soc_number, self.room_name)
        resp = self.client.post(self.START_QUIZ_URL, data=d,
                                content_type='application/x-www-form-urlencoded')

        self.checkOkResp(resp, 200)
        activity_id = ujson.loads(resp.content)['id']

        studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

        name = "Josh Baptiste"
        data = {
            "student_name": name,
            "activity_instance_id": activity_id
        }
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
        resp = self.client.post(self.SET_NAME_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkOkResp(resp, 200)

        d = {'question_id': self.question3.question_id, 'answer_text': "whatever",
             'activity_instance_id': activity_id, 'multi_response': False}
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")
        self.checkOkResp(resp, 201)

        sResp = StudentResponses.objects.filter(user_uuid=studentUuid, question_id=self.question3.question_id,
                                               activity_instance_id=activity_id)
        self.assertEqual(sResp.count(), 1)
        self.assertFalse(sResp.first().hidden)
        sRespId = sResp.first().pk

        resp = self.client.get(self.FIND_QUESTION_RESPONSE_URL + "%d/" % activity_id)

        self.checkOkResp(resp, 200)

        resp = ujson.loads(resp.content)
        self.assertEqual(len(resp),1)
        resp = resp[0]
        self.assertEqual(resp["activity_instance"], activity_id)
        self.assertEqual(resp["question"], self.question3.question_id)
        self.assertEqual(resp["id"], sRespId)
        self.assertEqual(resp["user_uuid"], studentUuid)

    def test_student_submit_response_multi_true_ok(self):
        """

        :return:
        """

        PubnubWrapper.override_publish(self.mock_publish)
        d = 'auth_token=%s&soc_num=%s&room_name=%s' % (self.auth_token_user,
                                                       self.soc_number, self.room_name)
        resp = self.client.post(self.START_QUIZ_URL, data=d,
                                content_type='application/x-www-form-urlencoded')

        self.checkOkResp(resp, 200)
        activity_id = ujson.loads(resp.content)['id']

        studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

        name = "Josh Baptiste"
        data = {"student_name": name, "activity_instance_id": activity_id}
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
        resp = self.client.post(self.SET_NAME_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkOkResp(resp, 200)

        d = {'question_id': self.question3.question_id, 'answer_text': "whatever",
             'activity_instance_id': activity_id, 'multi_response': False}
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 201)

        d = {'question_id': self.question4.question_id, 'answer_text': "whatever too",
             'activity_instance_id': activity_id, 'multi_response': True}
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 201)

    def test_auto_assign_team_ok(self):
        """

        :return:
        """

        PubnubWrapper.override_publish(self.mock_publish)
        d = {
            "auth_token": self.auth_token_user,
            "soc_num": self.soc_number,
            "room_name": self.room_name,
            "team_count": 3
        }
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_SPACE_RACE_URL, data=ujson.dumps(d),
                                content_type='application/json')

        self.checkOkResp(resp, 200)
        activity_id = ujson.loads(resp.content)['id']

        for i in range(6):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet) - 1)] for _ in range(20)])
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.get(self.AUTO_ASSIGN_TEAM+"%d/?rn=%s" % (activity_id, self.room_name))

            self.checkOkResp(resp, 200)
            self.assertEqual(ujson.loads(resp.content)["team_num"], i % 3)

    def test_finish_activity_ok(self):
        """

        :return:
        """

        PubnubWrapper.override_publish(self.mock_publish)
        d = 'soc_num=%s&room_name=%s' % (self.soc_number, self.room_name)
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUIZ_URL, data=d,
                                content_type='application/x-www-form-urlencoded')

        self.checkOkResp(resp, 200)
        activity_id = ujson.loads(resp.content)['id']

        studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

        name = "Josh Baptiste"
        data = {
            "student_name": name,
            "activity_instance_id": activity_id
        }
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
        resp = self.client.post(self.SET_NAME_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkOkResp(resp, 200)

        d = {'question_id': self.question3.question_id, 'answer_text': "whatever",
             'activity_instance_id': activity_id, 'multi_response': False}
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 201)

        d = {"activity_instance_id": activity_id}
        resp = self.client.post(self.FINISH_ACTIVITY_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

    def test_submit_response_without_student_name_when_required(self):
        """

        :return:
        """

        PubnubWrapper.override_publish(self.mock_publish)
        d = 'auth_token=%s&soc_num=%s&room_name=%s' % (self.auth_token_user,
                                                       self.soc_number, self.room_name)
        resp = self.client.post(self.START_QUIZ_URL, data=d,
                                content_type='application/x-www-form-urlencoded')

        self.checkOkResp(resp, 200)
        activity_id = ujson.loads(resp.content)['id']

        studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

        d = {'user_uuid': studentUuid, 'question_id': self.question3.question_id, 'answer_text': "whatever",
             'activity_instance_id': activity_id, 'multi_response': False}
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkErrResp(resp, 400)

    def test_check_team_for_student_response(self):
        """

        :return:
        """

        PubnubWrapper.override_publish(self.mock_publish)
        d = {
            "auth_token": self.auth_token_user,
            "soc_num": self.soc_number,
            "room_name": self.room_name,
            "team_count": 3
        }
        resp = self.client.post(self.START_SPACE_RACE_URL, data=ujson.dumps(d),
                                content_type='application/json')

        self.checkOkResp(resp, 200)
        activity_id = ujson.loads(resp.content)['id']

        for i in range(6):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet) - 1)] for _ in range(20)])
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.get(self.AUTO_ASSIGN_TEAM+"%d/?rn=%s" % (activity_id, self.room_name))

            self.checkOkResp(resp, 200)
            self.assertEqual(ujson.loads(resp.content)["team_num"], i % 3)

        studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

        name = "Josh Baptiste"
        data = {
            "student_name": name,
            "activity_instance_id": activity_id
        }
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
        resp = self.client.post(self.SET_NAME_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkOkResp(resp, 200)

        for i in range(1):
            d = {'question_id': self.question3.question_id, 'answer_text': "whatever",
                 'activity_instance_id': activity_id, 'multi_response': False, "team": i}
            resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

            self.checkOkResp(resp, 201)

            studResponse = StudentResponses.objects.get(user_uuid=studentUuid, activity_instance=activity_id)
            self.assertEqual(studResponse.team, i)

    def test_respond_to_true_false_quick_question(self):
        """

        :return:
        """

        PubnubWrapper.override_publish(self.mock_publish)
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        d = {
            "soc_num": self.soc_number,
            "room_name": self.room_name,
            "question_type": 'TF'
        }
        resp = self.client.post(self.START_QUICK_QUESTION_URL, data=ujson.dumps(d), content_type='application/json')

        self.checkOkResp(resp, 200)
        activity_id = ujson.loads(resp.content)['id']

        studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

        self.checkOkResp(resp, 200)

        d = {'question_id': self.question3.question_id, 'answer_text': "whatever",
             'activity_instance_id': activity_id, 'multi_response': False}
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 201)

    def test_student_score_space_race(self):
        """

        :return:
        """

        PubnubWrapper.override_publish(self.mock_publish)
        d = {
             "soc_num": self.soc_number,
             "room_name": self.room_name,
             "team_count": 3,
             "show_results": True
             }
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_SPACE_RACE_URL, data=ujson.dumps(d), content_type='application/json')

        self.checkOkResp(resp, 200)
        activity_id = ujson.loads(resp.content)['id']

        for i in range(6):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet) - 1)] for _ in range(20)])
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.get(self.AUTO_ASSIGN_TEAM+"%d/?rn=%s" % (activity_id, self.room_name))

            self.checkOkResp(resp, 200)
            self.assertEqual(ujson.loads(resp.content)["team_num"], i % 3)

        studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

        name = "Josh Baptiste"
        data = {
            "student_name": name,
            "activity_instance_id": activity_id
        }
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
        resp = self.client.post(self.SET_NAME_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkOkResp(resp, 200)

        for i in range(1):
            d = {'question_id': self.question3.question_id, 'answer_text': "whatever",
                 'activity_instance_id': activity_id, 'multi_response': False, "team": i}
            resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

            self.checkOkResp(resp, 201)

            studResponse = StudentResponses.objects.get(user_uuid=studentUuid, activity_instance=activity_id)
            self.assertEqual(studResponse.team, i)

        resp = self.client.get(self.STUDENT_SCORE_URL + '%s/' % str(activity_id))

        self.checkOkResp(resp, 200)
        data = ujson.loads(resp.content)
        self.assertEqual(data["total"], 5)
        self.assertEqual(data["neutral"], 2)
        self.assertEqual(data["correct"], 0)

    def test_student_score_space_race_unanswered(self):
        """

        :return:
        """

        PubnubWrapper.override_publish(self.mock_publish)
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        d = {
            "soc_num": self.soc_number,
            "room_name": self.room_name,
            "team_count": 3,
            "show_results": True
            }

        resp = self.client.post(self.START_SPACE_RACE_URL, data=ujson.dumps(d),
                                content_type='application/x-www-form-urlencoded')

        self.checkOkResp(resp, 200)
        activity_id = ujson.loads(resp.content)['id']

        for i in range(6):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet) - 1)] for _ in range(20)])
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.get(self.AUTO_ASSIGN_TEAM+"%d/?rn=%s" % (activity_id, self.room_name))

            self.checkOkResp(resp, 200)
            self.assertEqual(ujson.loads(resp.content)["team_num"], i % 3)

        studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

        name = "Josh Baptiste"
        data = {"student_name": name, "activity_instance_id": activity_id}
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
        resp = self.client.post(self.SET_NAME_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkOkResp(resp, 200)

        resp = self.client.get(self.STUDENT_SCORE_URL + '%s/' % str(activity_id))

        self.checkOkResp(resp, 200)
        data = ujson.loads(resp.content)
        self.assertEqual(data["total"], 5)
        self.assertEqual(data["neutral"], 2)
        self.assertEqual(data["correct"], 0)