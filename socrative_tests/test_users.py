# coding=utf-8

import django
django.setup()
from django.test.client import Client
from unittest import TestCase
import time
import ujson
import logging
import datetime
from django.contrib.auth.hashers import PBKDF2PasswordHasher
from socrative_users.models import UserSysMsg, SocrativeUser
from common.cryptography import Cryptography

logger = logging.getLogger(__name__)

PBK_ITERATIONS = 12000

def set_password(password):
    passwordHasher = PBKDF2PasswordHasher()
    return passwordHasher.encode(password, passwordHasher.salt(), PBK_ITERATIONS)


class TestSocrativeUser(TestCase):
    """
    class to test the socrative_users api's
    """
    REGISTER_URL = "/users/register/v1/"
    LOGIN_URL = "/users/login/"
    ROOMS_JOIN_URL = "/rooms/api/join/"
    RESET_PASSWORD_URL = '/users/reset-password/'
    CHANGE_PASSWORD_URL = '/users/change-password/'
    VALIDATE_AUTH_URL = '/users/validate-auth/'
    VALIDATE_CREDENTIALS_URL = '/users/validate-credentials/'
    MC_LOGIN_URL = '/users/api/mc-login/'
    CHECK_EMAIL_URL = '/users/check-email/'

    @classmethod
    def tearDownClass(cls):
        """

        :param cls:
        :return:
        """
        UserSysMsg.objects.all().delete()

    @classmethod
    def setUpClass(cls):
        """

        :param cls:
        :return:
        """
        cls.client = Client()

    def checkErrResp(self, resp, statusCode):
        """

        :param resp:
        :param statusCode:
        :return:
        """

        logger.info("RAW OUTPUT: %s, STATUS CODE: %d" % (resp.content, resp.status_code))
        self.assertEqual(resp.status_code, statusCode)
        self.assertIn("error", resp.content)

    def checkOkResp(self, resp, statusCode, obj=None):
        """
        """

        logger.info("RAW OUTPUT: %s, STATUS CODE: %d" % (resp.content if resp._headers.get("content-type")[1] == "application/json" else "{\"msg\":\"Content OK\"}", resp.status_code))
        self.assertEqual(resp.status_code, statusCode)
        if statusCode != 204:
            self.assertNotIn("error", ujson.loads(resp.content) if resp._headers.get("content-type")[1] == "application/json" else "{\"msg\":\"Content OK\"}")

    def tearDown(self):
        """

        :return:
        """
        if "ua" in self.client.cookies:
            self.client.cookies.pop("ua")

    def setUp(self):
        """

        :return:
        """
        self.email = "test%s@socrativetest.com" % str(time.time())
        self.password = "12345678"

        user = SocrativeUser(email=self.email)
        user.password = set_password(self.password)
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = user.last_login
        user.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        rooms = self.client.post(self.ROOMS_JOIN_URL, data={}, content_type='application/json')

        self.checkOkResp(rooms, 201)
        logger.info("setup finished")
        self.client.cookies.pop("ua")

    def checkUser(self, resp, statusCode, email=""):
        """
        generic method to check the user fields if the statusCode is 2xx or check the statusCode
        :param resp:
        :param statusCode:
        :return:
        """

        logger.info("RESPONSE :%s, STATUS CODE: %d", resp.content, resp.status_code)
        self.assertEqual(resp.status_code, statusCode)
        resp = ujson.loads(resp.content)

        if statusCode in (201, 200):
            # check the response
            self.assertEqual(resp["email"], email)
            self.assertNotIn("auth_token", resp)
            self.assertIsNotNone(resp.get("id"))
            self.assertIsNotNone(resp.get("display_name"))
            self.assertIsNotNone(resp.get("language"))
            self.assertEquals(resp.get("language"), "en")
            self.assertIsNotNone(resp.get("register_date"))
            self.assertEquals(resp.get("tz_offset"), 0)
        elif statusCode == 204:
            self.assertEquals(resp.content, "{}")
        elif statusCode == 500:
            self.assertLess(len(resp.content), 100)
        else:
            logger.info("already verified the status code")


    def test_register_json_ok(self):
        """
        checks the case when the user is registering with all the information
        :return:
        """
        email = "test%s@socrativetest.com" % str(time.time())
        password = "12345678"

        d = {"email": email, "password": password}
        d["school"] = {"id": 10, "school_name": "my school"}
        d["org_zip"] = "10142"
        d["role"] = ["teacher"]
        d["last_name"] = "mylastname"
        d["first_name"] = "myfirstname"
        d["org_type"] = u'K-12'
        d["country"] = "mata"
        d["org_state"] = "nebraska"
        d["org_name"] = "highschool"

        logger.info("INPUT : %s", str(d))
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")

        self.checkUser(resp, 201, email)

    def test_register_form_error(self):
        """
        test error cases
        :return:
        """
        email = "test%s@socrativetest.com" % str(time.time())
        password = "12345678"
        d = {"email": email, "password": password}
        d["firstName"] = 1

        logger.info("Input: %s", str(d))
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d['lastName'] = None
        d['role'] = ['sa',]
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d = {"email": email}
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

    def test_register_bad_orgType(self):
        """
        test the case when the orgType
        :return:
        """
        email = "test%s@socrativetest.com" % str(time.time())
        password = "12345678"
        d = {"email": email, "password": password}
        d['orgType'] = 2121
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d['orgType'] = "US Highe Ed"
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d['orgType'] = []
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d['orgType'] = "OTHR"
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

    def test_register_incomplete_data(self):
        """
        test the case when the orgType
        :return:
        """
        email = "test%s@socrativetest.com" % str(time.time())
        password = "12345678"
        d = {"email": email, "password": password}
        d['orgType'] = "US Higher Ed"
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        email = "test%s@socrativetest.com" % str(time.time())
        password = "12345678"
        d = {"email": email, "password": password}
        d['firstName'] = "myFirstName"
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        email = "test%s@socrativetest.com" % str(time.time())
        password = "12345678"
        d = {"email": email, "password": password}
        d['firstName'] = "myFirstName"
        d['lastName'] = "lastName"
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d["school"] = {"id": 10, "school_name":"my school"}
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d["school"] = {"id": "sa", "school_name":"my school"}
        d["zip"] = "10142"
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d["school"] = {"id": "-1", "school_name": ""}
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d["school"] = {"id": "-1", "school_name": "test"}
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d = {"email": email, "password": password}
        d["role"] = []
        d["lastName"] = ""
        d["firstName"] = ""
        d["orgType"] = ""

        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d = {"email": "", "password": password}
        d["role"] = ["teacher"]
        d["lastName"] = "ba"
        d["firstName"] = "ab"
        d["orgType"] = "K-12"
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d = {"email": email, "password": password}
        d["role"] = ["teachers"]
        d["lastName"] = "ba"
        d["firstName"] = "ab"
        d["orgType"] = "K-12"
        d["school"] = {}
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d["school"] = {"school_id": -2}
        d["role"] = ["teacher"]
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d["school"] = {"school_id": -1}
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d["school"] = {"school_id": -1, "school_name": ""}
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d["school"] = None
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d["orgType"] = "US Higher Ed"
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d["orgType"] = "International"
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d["orgType"] = "Other"
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

    def test_register_email_exist(self):
        email = "test%s@socrativetest.com" % str(time.time())
        password = "12345678"
        d = {"email": email, "password": password, "role": ["teacher"], "org_type": "OTHR",
             "first_name":"aaa", "last_name": "bbb", "country": "US", "org_name": "P5"}
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")

        self.checkUser(resp, 201, email)
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.checkUser(resp, 400)

        d = {"email": email.upper(), "password": password}
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")

        self.checkUser(resp, 400, email)

    def test_login_user(self):
        """

        :return:
        """
        d = "email=%s&password=12345678" % self.email
        resp = self.client.post(self.LOGIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")

        self.checkUser(resp, 200, self.email)

        d = "email=%s&password=12345678" % self.email.upper()
        resp = self.client.post(self.LOGIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")

        self.checkUser(resp, 200, self.email)

        from socrative_users.models import SocrativeUser
        user = SocrativeUser.objects.get(email=self.email)
        user.password = set_password("123456")
        user.save(update_fields=["password"])

        d = "email=%s&password=123456" % self.email
        resp = self.client.post(self.LOGIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")

        self.checkUser(resp, 200, self.email)

        d = "email=%s&password=12345678" % self.email
        resp = self.client.post(self.LOGIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")

        self.checkUser(resp, 400)

    def test_login_user_error(self):
        d = ""
        resp = self.client.post(self.LOGIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")
        self.checkUser(resp, 400)

        d = "email=%s" % self.email
        resp = self.client.post(self.LOGIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")
        self.checkUser(resp, 400)

        email = "test%s@socrativetest.com" % str(time.time())
        password = "12345678"
        d = "email=%s&password=%s" % (email, password)
        resp = self.client.post(self.LOGIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")
        self.checkUser(resp, 400)

        email = "test%s@socrativetest.com" % str(time.time())
        password = ""
        d = "email=%s&password=%s" % (email, password)
        resp = self.client.post(self.LOGIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")
        self.checkUser(resp, 400)

        email = ""
        password = "12345678"
        d = "email=%s&password=%s" % (email, password)
        resp = self.client.post(self.LOGIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")
        self.checkUser(resp, 400)

    def test_reset_password(self):
        d = "email=%s" % self.email
        resp = self.client.post(self.RESET_PASSWORD_URL,data=d,
                                content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 200)

    def test_reset_password_error(self):
        d = "test%s@socrativetest.com" % str(time.time())
        resp = self.client.post(self.RESET_PASSWORD_URL,data=d,
                                content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 400)

        d = "email="
        resp = self.client.post(self.RESET_PASSWORD_URL, data=d,
                                content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 400)

    def test_validate_auth(self):
        """

        :return:
        """
        d = "email=%s&password=12345678" % self.email
        resp = self.client.post(self.LOGIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")
        logger.info(resp.content)
        url = self.VALIDATE_AUTH_URL + '%s/' % Cryptography.decrypt(self.client.cookies["ua"].value)
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    def test_validate_auth_error(self):
        """
        test the error cases for auth
        :return:
        """
        url = self.VALIDATE_AUTH_URL + '%s/' % "sdadasdasdasdasdada"
        resp = self.client.get(url)

        self.checkUser(resp, 404)

        url = self.VALIDATE_AUTH_URL + '%s/' % "fdadafdafdafdafdada"
        resp = self.client.get(url)

        self.checkUser(resp, 404)

    def test_mc_login(self):
        d = {
            'email': 'test1%s@socrativetest.com' % str(time.time()),
            'api_key': "testingkeymastery",
            'mc_id': "%s" % str(int(time.time()))
        }
        resp = self.client.post(self.MC_LOGIN_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.assertEqual(resp.status_code, 200)

    def test_change_password(self):
        """
        test the bad case
        :return:
        """

        d = {}
        resp = self.client.post(self.CHANGE_PASSWORD_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkUser(resp, 400)

        d = {"newPassword": "12345678"}
        resp = self.client.post(self.CHANGE_PASSWORD_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkUser(resp, 400)

        d = {"newPassword": "12345678", "token": "adasdasdasdasdasdas"}
        resp = self.client.post(self.CHANGE_PASSWORD_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkUser(resp, 403)

        d = {"newPassword": "12345", "token": "aasdasdasdasdas"}
        resp = self.client.post(self.CHANGE_PASSWORD_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkUser(resp, 400)

        d = {"newPassword": "", "token":"blablabla"}
        resp = self.client.post(self.CHANGE_PASSWORD_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkUser(resp, 400)

        d = {"newPassword": "12345678", "token":""}
        resp = self.client.post(self.CHANGE_PASSWORD_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkUser(resp, 400)

        d = {"newPassword":"12345678", "token":""}
        resp = self.client.post(self.CHANGE_PASSWORD_URL, data=d)

        self.checkUser(resp, 400)

        d = "newPassword=12345678&token=blablabla"
        resp = self.client.post(self.CHANGE_PASSWORD_URL, data=d, content_type="application/x-www-form-urlencoded")

        self.checkUser(resp, 400)

    def test_mc_login_error(self):
        d = {}
        resp = self.client.post(self.MC_LOGIN_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.assertEqual(resp.status_code, 400)
        d = {
             'email':'test1@socrativetest.com',
        }
        resp = self.client.post(self.MC_LOGIN_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.assertEqual(resp.status_code, 400)
        d = {
            'email':'test1@socrativetest.com',
            'mc_id':1
        }
        resp = self.client.post(self.MC_LOGIN_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.assertEqual(resp.status_code, 400)
        d = {
            'email': 'test1@socrativetest.com',
            'mc_id': 1,
            'api_key': "testingkeyma",

        }
        resp = self.client.post(self.MC_LOGIN_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.assertEqual(resp.status_code, 400)

    def test_check_email(self):
        """
        test the check email api
        :return:
        """
        resp = self.client.get(self.CHECK_EMAIL_URL+"test@test.com/")

        print resp.content
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.content, "{}")

        resp = self.client.get(self.CHECK_EMAIL_URL + "tEst@test.com")

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.content, "{}")

    def test_check_email_error(self):
        """

        :return:
        """

        resp = self.client.get(self.CHECK_EMAIL_URL + self.email)

        self.checkErrResp(resp, 400)

        resp = self.client.get(self.CHECK_EMAIL_URL + self.email.upper())

        self.checkErrResp(resp, 400)

        resp = self.client.get(self.CHECK_EMAIL_URL + "/")

        self.checkErrResp(resp, 404)

        resp = self.client.get(self.CHECK_EMAIL_URL + self.email.upper())

        self.checkErrResp(resp, 400)

        resp = self.client.get(self.CHECK_EMAIL_URL +'/')

        self.checkErrResp(resp, 404)
