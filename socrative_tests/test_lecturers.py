# coding=utf-8

import django
django.setup()
from django.test.client import Client
from unittest import TestCase
import time
import ujson
from common.cryptography import Cryptography
from common import soc_constants
from socrative_tornado.publish import PubnubWrapper
import logging
import string
import random
import datetime
from quizzes.models import Quiz, Question, Answer
from socrative_users.models import SocrativeUser, Licenses, License_Activations
logger = logging.getLogger(__name__)

alphabet = string.digits + string.lowercase


class TestLecturers(TestCase):
    """
    class to test the socrative_users api's
    """
    UPDATE_TEACHER_PROFILE = '/lecturers/teacher-profile/'
    DISPLAY_TEACHER_PROFILE = '/lecturers/teacher-profile/'
    CHANGE_PASSWORD = '/lecturers/api/change-password/'
    CHANGE_EMAIL = '/lecturers/api/change-email/'
    START_QUIZ = '/lecturers/api/start-quiz'
    START_SPACE_RACE = '/lecturers/api/start-space-race/'
    START_QUICK_QUESTION = '/lecturers/api/start-quick-question/'
    CONVERT_QUICK_QUESTION_TO_VOTE = '/lecturers/api/convert-quick-question-to-vote/'
    DUPLICATE_QUIZ_URL = "/lecturers/api/duplicate-quiz/"
    START_EXIT_TICKET_QUIZ_URL = "/lecturers/api/start-exit-ticket/"
    SCORE_EXPORT_URL = "/lecturers/api/score-settings/"

    REGISTER_URL = "/users/register/v1/"
    LOGIN_URL = "/users/login/"
    CREATE_QUIZ_URL = "/quizzes/api/quiz/"
    QUESTIONS_URL = "/quizzes/api/questions/"
    JOIN_URL = "/rooms/api/join/"
    UPDATE_ROOM_NAME = '/rooms/api/room/'
    ANSWERS_URL = '/quizzes/api/answers/'
    SET_NAME_URL = '/students/api/set-name/'
    SUBMIT_STUDENT_RESPONSE_URL = '/students/api/responses/'
    LOGOUT_URL = '/users/api/logout/'

    @classmethod
    def tearDownClass(cls):
        """

        :param cls:
        :return:
        """
        pass

    @classmethod
    def setUpClass(cls):
        """

        :param cls:
        :return:
        """
        cls.client = Client()
        cls.messages = list()

    def mock_publish(self, message):
        self.messages.append(message)
        return True

    def checkOkResp(self, resp, statusCode, obj=None):
        """
        """

        logger.info("RAW OUTPUT: %s, STATUS CODE: %d" % (resp.content if resp._headers.get("content-type")[1] == "application/json" else "{\"msg\":\"Content OK\"}", resp.status_code))
        self.assertEqual(resp.status_code, statusCode)
        if statusCode != 204:
            self.assertNotIn("error", ujson.loads(resp.content) if resp._headers.get("content-type")[1] == "application/json" else "{\"msg\":\"Content OK\"}")

    def checkErrResp(self, resp, statusCode):
        """

        :param resp:
        :param statusCode:
        :return:
        """

        logger.info("RAW OUTPUT: %s, STATUS CODE: %d" % (resp.content, resp.status_code))
        self.assertEqual(resp.status_code, statusCode)
        self.assertIn("error", resp.content)

    def setUp(self):
        self.email = "test%s@socrativetest.com" % str(time.time())
        self.password = "12345678"

        d = {"email": self.email, "password": self.password, "role": ["teacher"], "org_type": "OTHR",
             "first_name":"aaa", "last_name": "bbb", "country": "US", "org_name": "P5"}
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")

        self.checkOkResp(resp, 201)

        d = "email=%s&password=12345678" % self.email
        resp = self.client.post(self.LOGIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 200)

        self.auth_token_user = Cryptography.decrypt(resp.cookies["ua"].value)
        d = "{\"tz_offset\":120}"
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps(d), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 201)
        self.room_name = ujson.loads(resp.content)['name']
        self.created_by_id = ujson.loads(resp.content)['created_by']

        resp = self.client.post(self.CREATE_QUIZ_URL, data=ujson.dumps({}), content_type="application/json")

        self.checkOkResp(resp, 201)

        self.quiz_id = ujson.loads(resp.content).get("id")
        self.soc_number = ujson.loads(resp.content).get("soc_number")

        d = {
            'auth_token':self.auth_token_user,
            'quiz_id':self.quiz_id,
            'type':'TF',
            'order':1,
            'question_text':'question text',
            'answers':[{"order":1, "is_correct": False, "text":"True"},
                       {"order":2, "is_correct": True, "text":"False"}]
        }
        resp = self.client.post(self.QUESTIONS_URL, data=ujson.dumps(d),
                                content_type='application/json')

        self.checkOkResp(resp, 201)

        self.question_id = ujson.loads(resp.content)['question_id']

        self.answers_id = ujson.loads(resp.content)['answers'][0]['id']

    def tearDown(self):
        """

        :return:
        """
        if "ua" in self.client.cookies:
            self.client.cookies.pop("ua")

        if "sa" in self.client.cookies:
            self.client.cookies.pop("sa")

    def test_update_teacher_profile(self):
        """
        :return:
        """
        d = {"first_name": "sdasdasd", "last_name": "asdasdad",
             "email": "%s" % self.email, "password": "", "country": "Azerbaijan",
             "language": "en", "org_type": "USHE", "org_state": "Nebraska", "org_zip": "10110", "orgId": "-1",
             "org_name": "asdasdasad", "school_data": {}, "role": ["teacher"]}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.UPDATE_TEACHER_PROFILE, data=ujson.dumps(d), content_type='application/json')
        self.checkOkResp(resp, 200)

    def test_display_teacher_profile(self):
        """

        :return:
        """
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(self.DISPLAY_TEACHER_PROFILE)
        self.checkOkResp(resp, 200)

    def test_change_password(self):
        """

        :return:
        """
        d = 'password=%s&new_password=%s' % (self.password, 'testing1')
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.CHANGE_PASSWORD, data=d,
                                content_type='application/x-www-form-urlencoded')

        self.checkOkResp(resp, 200)

        d = "email=%s&password=testing1" % self.email
        resp = self.client.post(self.LOGIN_URL, data=d, content_type='application/x-www-form-urlencoded')

        self.checkOkResp(resp, 200)

    def test_change_email(self):
        """

        :return:
        """
        d = 'new_email=%s' % ('test%s@yahoo.com' % str(time.time()))
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)

        resp = self.client.post(self.CHANGE_EMAIL, data=d,
                                content_type='application/x-www-form-urlencoded')
        self.checkOkResp(resp, 200)

    def test_start_quizzes(self):
        PubnubWrapper.override_publish(self.mock_publish)
        d = 'auth_token=%s&soc_num=%s&room_name=%s' % (self.auth_token_user,
                                                       self.soc_number, self.room_name)
        resp = self.client.post(self.START_QUIZ, data=d,
                                content_type='application/x-www-form-urlencoded')
        self.checkOkResp(resp, 200)

    def test_start_quick_question_ok(self):
        PubnubWrapper.override_publish(self.mock_publish)
        d = {
            "room_name": self.room_name,
            "question_text": "1q text sample",
            "question_type": "MC"
        }
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUICK_QUESTION, data=ujson.dumps(d),
                                content_type='application/json')
        self.checkOkResp(resp, 200)

        d = {
            "room_name": self.room_name,
            "question_text": "1q text sample",
            "question_type": "TF"
        }
        resp = self.client.post(self.START_QUICK_QUESTION, data=ujson.dumps(d),
                                content_type='application/json')
        self.checkOkResp(resp, 200)

        d = {
            "room_name": self.room_name,
            "question_text": "1q text sample",
            "question_type": "FR"
        }
        resp = self.client.post(self.START_QUICK_QUESTION, data=ujson.dumps(d),
                                content_type='application/json')
        self.checkOkResp(resp, 200)

    def test_convert_quick_question_to_vote_ok(self):
        PubnubWrapper.override_publish(self.mock_publish)

        d = {
            "room_name": self.room_name,
            "question_text": "1q text sample",
            "question_type": "FR"
        }
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUICK_QUESTION, data=ujson.dumps(d), content_type='application/json')
        self.checkOkResp(resp, 200)

        activity_id = ujson.loads(resp.content)['id']
        quiz_id = ujson.loads(resp.content)['activity_id']

        studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

        name = "Josh Baptiste"
        data = {"student_name": name, "activity_instance_id":activity_id}
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
        resp = self.client.post(self.SET_NAME_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkOkResp(resp, 200)
        question_id = Quiz.objects.get(pk=quiz_id).questions.first().question_id

        d = {'question_id': question_id, 'answer_text': "whatever",
             'activity_instance_id': activity_id, 'multi_response': False}
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 201)

        data = {"room_name": self.room_name}
        resp = self.client.post(self.CONVERT_QUICK_QUESTION_TO_VOTE, data=ujson.dumps(data),
                                content_type='application/json')

        self.checkOkResp(resp, 200)

    def test_start_space_race(self):
        PubnubWrapper.override_publish(self.mock_publish)
        d = {"soc_num": self.soc_number, "room_name": self.room_name}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_SPACE_RACE, data=ujson.dumps(d),
                                content_type='application/json')
        self.checkOkResp(resp, 200)

    def test_duplicate_quiz_ok(self):
        """

        :return:
        """
        PubnubWrapper.override_publish(self.mock_publish)
        d = {"auth_token": self.auth_token_user, "soc_number": self.soc_number}
        resp = self.client.post(self.DUPLICATE_QUIZ_URL, data=ujson.dumps(d), content_type='application/json')
        self.checkOkResp(resp, 200)

    def test_start_exit_ticket_quiz_ok(self):
        """

        :return:
        """
        PubnubWrapper.override_publish(self.mock_publish)
        d = {"room_name": self.room_name}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_EXIT_TICKET_QUIZ_URL, data=ujson.dumps(d),
                                content_type='application/json')
        self.checkOkResp(resp, 200)

    def test_50_students_responding_to_exit_ticket_quiz_ok(self):
        """

        :return:
        """
        PubnubWrapper.override_publish(self.mock_publish)
        d = {"room_name": self.room_name}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_EXIT_TICKET_QUIZ_URL, data=ujson.dumps(d),
                                content_type='application/json')
        self.checkOkResp(resp, 200)

        activity_id = ujson.loads(resp.content)["id"]

        self.checkOkResp(resp, 200)
        qz = Quiz.objects.filter().last()
        qId1 = Question.objects.filter(quiz = qz, order=1)[0].pk
        qId2 = Question.objects.filter(quiz = qz, order=2)[0].pk

        for i in range(50):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet) - 1)] for _ in range(20)])
            name = "Josh Baptiste %d" % i
            data = {
                "student_name": name,
                "activity_instance_id": activity_id
            }
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)

            resp = self.client.post(self.JOIN_URL, data=ujson.dumps({"name": self.room_name}),
                                    content_type='application/json')

            self.checkOkResp(resp, 200)

            resp = self.client.post(self.SET_NAME_URL, data=ujson.dumps(data), content_type="application/json")
            self.checkOkResp(resp, 200)

            d = {'question_id': qId1, 'answer_ids': "%d" % Answer.objects.filter(question_id=qId1, order=1)[0].pk,
                 'activity_instance_id': activity_id, 'multi_response': False}

            resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")
            self.checkOkResp(resp, 201)

            d = {'question_id': qId2, 'answer_text': "whatever",
                 'activity_instance_id': activity_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")
            self.checkOkResp(resp, 201)

            resp = self.client.post(self.LOGOUT_URL, data=ujson.dumps({"room_name": self.room_name}),
                                    content_type='application/json')
            self.assertEqual(resp.status_code, 200)
            try:
                content = ujson.loads(resp.content)
            except Exception:
                self.assertTrue(False)


    def test_create_export_score_settings_ok(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)

        data = {"key": 1, "next_line": 1, "speed": 50, "method": 1, "prefix": "M"}
        resp = self.client.post(self.SCORE_EXPORT_URL, data=ujson.dumps(data), content_type="application/json")

        logger.debug(resp.content)
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("id", content)


    def test_create_export_score_settings_fail_user_premium(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)

        # key 0
        data = {"key": 0, "next_line": 1, "speed": 50, "method": 1, "prefix": "M"}
        resp = self.client.post(self.SCORE_EXPORT_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkErrResp(resp, 400)

        # key 13
        data = {"key": 13, "next_line": 1, "speed": 50, "method": 1, "prefix": "M"}
        resp = self.client.post(self.SCORE_EXPORT_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkErrResp(resp, 400)

        # next line 0
        data = {"key": 5, "next_line": 0, "speed": 50, "method": 1, "prefix": "M"}
        resp = self.client.post(self.SCORE_EXPORT_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkErrResp(resp, 400)

        # next line 6
        data = {"key": 9, "next_line": 6, "speed": 50, "method": 1, "prefix": "M"}
        resp = self.client.post(self.SCORE_EXPORT_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkErrResp(resp, 400)

        # speed 49
        data = {"key": 8, "next_line": 5, "speed": 49, "method": 1, "prefix": "M"}
        resp = self.client.post(self.SCORE_EXPORT_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkErrResp(resp, 400)

        # speed 51
        data = {"key": 4, "next_line": 3, "speed": 51, "method": 1, "prefix": "M"}
        resp = self.client.post(self.SCORE_EXPORT_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkErrResp(resp, 400)

        # method 0
        data = {"key": 1, "next_line": 1, "speed": 50, "method": 0, "prefix": "M"}
        resp = self.client.post(self.SCORE_EXPORT_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkErrResp(resp, 400)

        # method 3
        data = {"key": 1, "next_line": 1, "speed": 50, "method": 3, "prefix": "M"}
        resp = self.client.post(self.SCORE_EXPORT_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkErrResp(resp, 400)

        # no prefix
        data = {"key": 1, "next_line": 1, "speed": 50, "method": 1}
        resp = self.client.post(self.SCORE_EXPORT_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkErrResp(resp, 400)

        # prefix number
        data = {"key": 4, "next_line": 1, "speed": 50, "method": 1, "prefix": 4}
        resp = self.client.post(self.SCORE_EXPORT_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkErrResp(resp, 400)

    def test_create_export_score_settings_fail_user_free(self):
        """

        :return:
        """

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)

        data = {"key": 1, "next_line": 1, "speed": 50, "method": 1, "prefix": "M"}
        resp = self.client.post(self.SCORE_EXPORT_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkErrResp(resp, 400)

    def test_get_export_score_settings(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)

        data = {"key": 1, "next_line": 1, "speed": 50, "method": 1, "prefix": "M"}
        resp = self.client.post(self.SCORE_EXPORT_URL, data=ujson.dumps(data), content_type="application/json")

        logger.debug(resp.content)
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("id", content)

        score_id = content.get("id")

        ####
        # get the settings
        ####

        resp = self.client.get(self.SCORE_EXPORT_URL)

        self.checkOkResp(resp, 200)

        content = resp.json()

        self.assertEqual(content["id"], score_id)
        for key in data:
            self.assertEqual(content[key], data[key])


    def test_update_score_settings_ok(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)

        data = {"key": 1, "next_line": 1, "speed": 50, "method": 1, "prefix": "M"}
        resp = self.client.post(self.SCORE_EXPORT_URL, data=ujson.dumps(data), content_type="application/json")

        logger.debug(resp.content)
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("id", content)

        score_id = content.get("id")

        # ##########
        # update scores
        # ##########
        data = {"key": 3, "next_line": 3, "speed": 500, "method": 2, "prefix": ""}
        resp = self.client.put(self.SCORE_EXPORT_URL + "%d/" % score_id, data=ujson.dumps(data), content_type="application/json")
        logger.debug(resp.content)
        self.checkOkResp(resp, 200)

        ####
        # get the settings
        ####

        resp = self.client.get(self.SCORE_EXPORT_URL)

        self.checkOkResp(resp, 200)

        content = resp.json()

        self.assertEqual(content["id"], score_id)
        for key in data:
            self.assertEqual(content[key], data[key])

    def test_check_user_pro_license_ok_user_same_as_buyer(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=200)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("notifications", content)

        self.assertIsNone(content["notifications"])

    def test_check_user_pro_license_ok_user_same_different_than_buyer(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer_id = self.created_by_id
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=200)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("notifications", content)

        self.assertIsNone(content["notifications"])

    def test_check_user_pro_license_ok_user_same_as_buyer_license_expired_no_notif(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer_id = user.pk
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() - datetime.timedelta(days=31)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("notifications", content)

        self.assertIsNone(content["notifications"])

    def test_check_user_pro_license_user_different_than_buyer_license_expired_no_notif(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer_id = self.created_by_id
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() - datetime.timedelta(days=31)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("notifications", content)

        self.assertIsNone(content["notifications"])

    def test_check_user_pro_license_user_different_than_buyer_license_expired_notif_ok(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer_id = user.pk
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() - datetime.timedelta(days=29)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("notifications", content)

        self.assertIsNotNone(content["notifications"])
        self.assertEqual(content["notifications"]["type"], 1)

    def test_check_user_pro_license_user_same_as_buyer_license_expired_notif_ok(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer_id = self.created_by_id
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() - datetime.timedelta(days=29)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("notifications", content)

        self.assertIsNotNone(content["notifications"])
        self.assertEqual(content["notifications"]["type"], 1)

    def test_check_user_pro_license_user_same_as_buyer_license_expiring_notif_ok(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer_id = self.created_by_id
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=soc_constants.MAX_DAYS_BEFORE_PRO_EXPIRATION - 1)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("notifications", content)

        self.assertIsNotNone(content["notifications"])
        self.assertEqual(content["notifications"]["type"], 0)

    def test_check_user_pro_license_user_different_than_buyer_license_expiring_notif_ok(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer_id = user.pk
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=soc_constants.MAX_DAYS_BEFORE_PRO_EXPIRATION - 1)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("notifications", content)

        self.assertIsNotNone(content["notifications"])
        self.assertEqual(content["notifications"]["type"], 0)

    def test_check_user_pro_license_2_licenses_1_expired_one_not_no_notif(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer_id = user.pk
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() - datetime.timedelta(days=31)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        l1 = Licenses()
        l1.buyer_id = user.pk
        l1.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l1.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=soc_constants.MAX_DAYS_BEFORE_PRO_EXPIRATION + 1)
        l1.years = 1
        l1.price_per_seat = 10
        l1.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("notifications", content)

        self.assertIsNone(content["notifications"])

    def test_check_user_pro_license_2_licenses_1_expired_one_not_expired_notif(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer_id = user.pk
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() - datetime.timedelta(days=29)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        l1 = Licenses()
        l1.buyer_id = user.pk
        l1.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l1.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=soc_constants.MAX_DAYS_BEFORE_PRO_EXPIRATION + 1)
        l1.years = 1
        l1.price_per_seat = 10
        l1.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("notifications", content)

        self.assertIsNone(content["notifications"])

    def test_check_user_pro_license_2_licenses_1_expired_one_expiring_expired_notif(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer_id = user.pk
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() - datetime.timedelta(days=soc_constants.MAX_DAYS_AFTER_PRO_EXPIRATION - 1)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        l1 = Licenses()
        l1.buyer_id = user.pk
        l1.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l1.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=soc_constants.MAX_DAYS_BEFORE_PRO_EXPIRATION - 1)
        l1.years = 1
        l1.price_per_seat = 10
        l1.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("notifications", content)

        self.assertIsNotNone(content["notifications"])
        self.assertEqual(content["notifications"]["type"], 0)

    def test_check_user_pro_license_2_licenses_buyer_different_than_user_1_expired_one_expiring_expired_notif(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer_id = self.created_by_id
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() - datetime.timedelta(days=soc_constants.MAX_DAYS_AFTER_PRO_EXPIRATION - 1)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        l1 = Licenses()
        l1.buyer_id = user.pk
        l1.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l1.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=soc_constants.MAX_DAYS_BEFORE_PRO_EXPIRATION - 1)
        l1.years = 1
        l1.price_per_seat = 10
        l1.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("notifications", content)

        self.assertIsNotNone(content["notifications"])
        self.assertEqual(content["notifications"]["type"], 0)

    def test_check_user_pro_license_2_licenses_buyer_different_than_user_1_expired_one_expiring_expiring_notif(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer_id = self.created_by_id
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() - datetime.timedelta(days=soc_constants.MAX_DAYS_AFTER_PRO_EXPIRATION - 1)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        l1 = Licenses()
        l1.buyer_id = user.pk
        l1.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l1.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=soc_constants.MAX_DAYS_BEFORE_PRO_EXPIRATION - 1)
        l1.years = 1
        l1.price_per_seat = 10
        l1.save()

        la = License_Activations()
        la.license = l1
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 201)
        content = resp.json()
        self.assertIn("notifications", content)

        self.assertIsNotNone(content["notifications"])
        self.assertEqual(content["notifications"]["type"], 0)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}), content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 200)
        content = resp.json()
        self.assertIn("notifications", content)

        self.assertIsNotNone(content["notifications"])
        self.assertEqual(content["notifications"]["type"], 1)
