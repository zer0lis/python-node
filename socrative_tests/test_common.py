# coding=utf-8
import django
django.setup()

from django.test.client import Client
from unittest import TestCase
import time
import ujson
from socrative_tornado.publish import PubnubWrapper
import logging
import datetime
from socrative_users.models import SocrativeUser, Licenses, License_Activations
from rooms.models import Room, Roster
from quizzes.models import Quiz, Question, Answer
from students.models import Student
from common.models import MediaResource, ActivityInstance
import string
import random
from common.cryptography import Cryptography
alphabet = string.digits + string.letters

logger = logging.getLogger(__name__)


class TestCommon(TestCase):
    MEDIA_RESOURCE_URL = '/common/api/media-resources/'
    REPORTS_URL = '/activities/api/reports/'
    LIVE_RESULTS_URL = '/activities/api/report/'
    HIDE_REPORT_URL = '/activities/api/report/'
    PURGE_REPORTS_URL = '/activities/api/reports/purge/'
    PING_URL = '/api/ping/'

    JOIN_URL = "/rooms/api/join/"
    REGISTER_URL = "/users/register/"
    LOGIN_URL = "/users/login/"
    UPDATE_ROOM_NAME_URL = '/rooms/api/room'
    START_QUIZ_URL = '/lecturers/api/start-quiz/'
    START_SPACE_RACE_URL = '/lecturers/api/start-space-race/'
    START_1Q_URL = '/lecturers/api/start-quick-question/'
    START_EXIT_TICKET_URL = '/lecturers/api/start-exit-ticket/'
    END_ACTIVITY_URL = '/rooms/api/end-current-activity/'
    SUBMIT_RESPONSE = '/students/api/responses/'
    SET_STUDENT_NAME = "/students/api/set-name/"
    CREATE_QUIZ_URL = "/quizzes/api/quiz/"
    START_EXIT_TICKET_QUIZ_URL = "/lecturers/api/start-exit-ticket/"
    STUDENT_LOGIN_URL = "/students/api/login/"


    messages = list()

    @classmethod
    def tearDownClass(cls):
        """

        :param cls:
        :return:
        """
        pass

    @classmethod
    def setUpClass(cls):
        """

        :param cls:
        :return:
        """
        cls.client = Client()

    @classmethod
    def mock_publish(cls, message):
        cls.messages.append(message)
        return True

    def setUp(self):
        """
        
        :return: 
        """
        email = "test%s@socrativetest.com" % str(time.time())
        password = "12345678"

        user = SocrativeUser(email=email, password=password)
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        room_name = "test" + str(time.time()).replace('.','')
        room = Room(name=room_name, name_lower=room_name, created_by=user, status=1)
        room.save()

        quiz = Quiz(created_by=user, name=str(time.time()), is_hidden=False, last_updated=datetime.datetime.utcnow())
        quiz.save()
        question1 = Question(question_text="tf question text", created_by=user, type="TF", order=1, quiz=quiz)
        question1.save()
        question2 = Question(question_text="mc question text", created_by=user, type="MC", order=2, quiz=quiz)
        question2.save()
        question3 = Question(question_text="fr question text", created_by=user, type="FR", order=3, quiz=quiz)
        question3.save()
        question4 = Question(question_text="fr question text with correct answer", created_by=user, type="FR", order=4,
                             quiz=quiz)
        question4.save()
        answer11 = Answer(is_correct=False, text="False", created_by=user, order=1)
        answer11.question = question1
        answer11.save()

        answer12 = Answer(is_correct=True, text="True", created_by=user, order=2)
        answer12.question = question1
        answer12.save()
        answer21 = Answer(is_correct=True, text="343", created_by=user, order=1)
        answer21.question = question2
        answer21.save()
        answer22 = Answer(is_correct=True, text="434234", created_by=user, order=2)
        answer22.question = question2
        answer22.save()
        answer23 = Answer(is_correct=False, text="adasdsa", created_by=user, order=3)
        answer23.question = question2
        answer23.save()
        answer41 = Answer(is_correct=True, text="true", created_by=user, order=1)
        answer41.question = question4
        answer41.save()

        m1 = MediaResource()
        m1.owner = user
        m1.data = "{}"
        m1.name = "test"
        m1.type = MediaResource.IMAGE
        m1.url = "test://some_url"
        m1.save()

        m2 = MediaResource()
        m2.owner = user
        m2.data = "{}"
        m2.name = "test"
        m2.type = MediaResource.IMAGE
        m2.url = "test://some_url"
        m2.save()

        self.res1_id = m1.id
        self.res2_id = m2.id

        question1.resources.add(m1)
        question1.save()
        question3.resources.add(m2)
        question3.save()

        self.auth_token_user = user.auth_token
        self.soc_number = quiz.soc_number
        self.room_name = room.name
        self.question1 = question1
        self.question2 = question2
        self.question3 = question3
        self.question4 = question4
        self.answer11 = answer11
        self.answer12 = answer12
        self.answer21 = answer21
        self.answer22 = answer22
        self.answer23 = answer23
        self.answer41 = answer41
        self.quiz = quiz
        resp = self.client.get(self.PING_URL)
        self.assertEqual(resp.status_code, 200)

        PubnubWrapper.override_publish(self.mock_publish)

        self.quiz.soc_number = str(5000000 + quiz.id)
        self.quiz.save()

        d = 'soc_num=%s&room_name=%s' % (self.quiz.soc_number, self.room_name)
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUIZ_URL, data=d,
                                content_type='application/x-www-form-urlencoded')
        self.assertEqual(resp.status_code, 200)

        logger.info("RAW" + resp.content)

        self.activi_id = ujson.loads(resp.content)['id']

        answerText = [u"Uno dos tres", u"fête commence", u"Празднование начнется", u"ਜਸ਼ਨ ਸ਼ੁਰੂ ਹੋ",
                      u"kutlama başlar", u"αρχίζουν γιορτή", u"慶祝活動開始", u"お祝い始まる", u"يبدأ الاحتفا", u"축제 시작"]

        for i in range(10):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet) - 1)] for _ in range(20)])

            name = "Josh" + str(i)
            data = {"student_name": name, "activity_instance_id": self.activi_id}
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SET_STUDENT_NAME, data=ujson.dumps(data), content_type="application/json")
            self.assertEqual(resp.status_code, 200)

            d = {'question_id': self.question3.question_id, 'answer_text': answerText[i],
                 'activity_instance_id': self.activi_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")
            self.assertEqual(resp.status_code, 201)

        d = "room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 200)

        d = {
            "soc_num": self.quiz.soc_number,
            "room_name": self.room_name
        }

        resp = self.client.post(self.START_SPACE_RACE_URL, data=ujson.dumps(d), content_type='application/json')
        self.assertEqual(resp.status_code, 200)

        self.activi_id = ujson.loads(resp.content)['id']

        answerText = [u"Uno dos tres", u"fête commence", u"Празднование начнется", u"ਜਸ਼ਨ ਸ਼ੁਰੂ ਹੋ",
                      u"kutlama başlar", u"αρχίζουν γιορτή", u"慶祝活動開始", u"お祝い始まる", u"يبدأ الاحتفا", u"축제 시작"]

        for i in range(10):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet) - 1)] for _ in range(20)])

            name = "Josh" + str(i)
            data = {"student_name": name, "activity_instance_id": self.activi_id}
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SET_STUDENT_NAME, data=ujson.dumps(data), content_type="application/json")
            self.assertEqual(resp.status_code, 200)

            d = {'question_id': self.question3.question_id, 'answer_text': answerText[i],
                 'activity_instance_id': self.activi_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")
            self.assertEqual(resp.status_code, 201)

        d = "room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 200)

        d = {
            "room_name": self.room_name,
            "question_type": "FR",
            "question_text": "what_is_the_question",
            "require_names": False
        }

        resp = self.client.post(self.START_1Q_URL, data=ujson.dumps(d), content_type="application/json")
        self.assertEqual(resp.status_code, 200)

        activity_id = ujson.loads(resp.content)["id"]
        quiz_id = ujson.loads(resp.content)["activity_id"]

        resp = self.client.get(self.CREATE_QUIZ_URL + str(quiz_id) + "/")
        self.assertEqual(resp.status_code, 200)

        question_id = Question.objects.filter(quiz_id=quiz_id).only('question_id').first().question_id

        answerText = [u"Uno dos tres", u"fête commence", u"Празднование начнется", u"ਜਸ਼ਨ ਸ਼ੁਰੂ ਹੋ",
                      u"kutlama başlar", u"αρχίζουν γιορτή", u"慶祝活動開始", u"お祝い始まる", u"يبدأ الاحتفا", u"축제 시작"]

        for i in range(10):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet) - 1)] for _ in range(20)])
            d = {'question_id': question_id, 'answer_text': answerText[i],
                 'activity_instance_id': activity_id, 'multi_response': False}
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")
            self.assertEqual(resp.status_code, 201)

        d = "room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 200)

        d = {"room_name": self.room_name}

        resp = self.client.post(self.START_EXIT_TICKET_URL, data=ujson.dumps(d), content_type='application/json')
        self.assertEqual(resp.status_code, 200)

        activi_id = ujson.loads(resp.content)["id"]
        qz = Quiz.objects.get(id=ujson.loads(resp.content)["activity_id"])
        question_id = qz.questions.all()[1].question_id

        answerText = [u"Uno dos tres", u"fête commence", u"Празднование начнется", u"ਜਸ਼ਨ ਸ਼ੁਰੂ ਹੋ",
                      u"kutlama başlar", u"αρχίζουν γιορτή", u"慶祝活動開始", u"お祝い始まる", u"يبدأ الاحتفا", u"축제 시작"]

        for i in range(10):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet) - 1)] for _ in range(20)])

            name = "Joshua" + str(i)
            data = {"student_name": name, "activity_instance_id": activi_id}
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SET_STUDENT_NAME, data=ujson.dumps(data), content_type="application/json")
            self.assertEqual(resp.status_code, 200)

            d = {'question_id': question_id, 'answer_text': answerText[i],
                 'activity_instance_id': activi_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")
            self.assertEqual(resp.status_code, 201)

        d = "room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 200)
        

    def tearDown(self):
        """

        :return:
        """
        self.messages = []

        if "sa" in self.client.cookies:
            self.client.cookies.pop("sa")

        if "ua" in self.client.cookies:
            self.client.cookies.pop("ua")

    def checkErrResp(self, resp, statusCode):
        """

        :param resp:
        :param statusCode:
        :return:
        """

        logger.info("RAW OUTPUT: %s, STATUS CODE: %d" % (resp.content, resp.status_code))
        self.assertEqual(resp.status_code, statusCode)
        self.assertIn("error", resp.content)

    def checkOkResp(self, resp, statusCode, obj=None):
        """
        """

        logger.info("RAW OUTPUT: %s, STATUS CODE: %d" % (resp.content if resp._headers.get("content-type")[1] == "application/json" else "{\"msg\":\"Content OK\"}", resp.status_code))
        self.assertEqual(resp.status_code, statusCode)
        if statusCode != 204:
            self.assertNotIn("error", ujson.loads(resp.content) if resp._headers.get("content-type")[1] == "application/json" else "{\"msg\":\"Content OK\"}")

    def test_get_all_reports_type_active(self):
        """

        :return:
        """

        url = self.REPORTS_URL+"?state=active&offset=0&limit=30"
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)

        self.checkOkResp(resp, 200)

        # not the good way to test , but it will be enough for now
        self.assertEqual(len(ujson.loads(resp.content)["reports"]), 4)
        self.assertIn("metadata", ujson.loads(resp.content))
        self.assertIn("active", ujson.loads(resp.content)["metadata"])
        self.assertIn("trashed", ujson.loads(resp.content)["metadata"])
        self.assertIn("archived", ujson.loads(resp.content)["metadata"])

    def test_get_all_reports_rtype_simple(self):
        """

        :return:
        """

        url = self.REPORTS_URL+"?state=active&offset=0&limit=50"

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)

        self.checkOkResp(resp, 200)

        content = ujson.loads(resp.content)
        # not the good way to test , but it will be enough for now
        self.assertEqual(len(content), 3)
        self.assertEqual(len(content["reports"]), 4)
        self.assertIn("metadata", content)
        self.assertIn("active", content["metadata"])
        self.assertIn("trashed", content["metadata"])
        self.assertIn("archived", content["metadata"])

    def test_get_all_reports_terms_filtering(self):
        """

        :return:
        """
        url = self.REPORTS_URL+"?terms=ma&state=active&offset=0&limit=4"

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)

        self.checkOkResp(resp, 200)

        # not the good way to test , but it will be enough for now
        self.assertEqual(len(ujson.loads(resp.content)["reports"]), 0)

    def test_get_all_reports_sort_0_limit_minus1(self):
        """

        :return:
        """
        url = self.REPORTS_URL+"?state=active&offset=1&limit=-1"

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)

        self.checkErrResp(resp, 400)

    def test_get_all_reports_limit_31(self):
        """

        :return:
        """
        url = self.REPORTS_URL+"?state=active&offset=0&limit=51"
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)

        self.checkErrResp(resp, 400)

    def test_get_archived_reports(self):
        """

        :return:
        """
        url = self.REPORTS_URL+"?state=archived&offset=0&limit=30"
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)

        self.checkOkResp(resp, 200)

        # not the good way to test , but it will be enough for now
        self.assertEqual(len(ujson.loads(resp.content)["reports"]), 0)

        url = self.HIDE_REPORT_URL + "%d/" % self.activi_id
        d = {"state": "archived"}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.put(url, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

        url = self.REPORTS_URL + "?state=archived&offset=0&limit=30"
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)

        self.checkOkResp(resp, 200)

        # not the good way to test , but it will be enough for now
        self.assertEqual(len(ujson.loads(resp.content)["reports"]), 1)

    def test_get_trashed_reports(self):
        """

        :return:
        """
        url = self.REPORTS_URL+"?state=trashed&offset=0&limit=30"
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)

        self.checkOkResp(resp, 200)

        # not the good way to test , but it will be enough for now
        self.assertEqual(len(ujson.loads(resp.content)["reports"]), 0)

        url = self.HIDE_REPORT_URL + "%d/" % self.activi_id
        d = {"state": "trashed"}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.put(url, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

        url = self.REPORTS_URL + "?state=trashed&offset=0&limit=30"
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)

        self.checkOkResp(resp, 200)

        # not the good way to test , but it will be enough for now
        self.assertEqual(len(ujson.loads(resp.content)["reports"]), 1)

    def test_get_active_limit0_offset0(self):
        """

        :return:
        """
        url = self.REPORTS_URL+"?state=active&offset=0&limit=0"

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)

        self.checkErrResp(resp, 400)

    
    def test_get_archived_state_reports(self):
        """
        
        :return:
        """
        url = self.REPORTS_URL+"?state=archived&offset=0&limit=30"

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)

        self.checkOkResp(resp, 200)

        # not the good way to test , but it will be enough for now
        self.assertEqual(len(ujson.loads(resp.content)), 3)
        self.assertEqual(len(ujson.loads(resp.content)["reports"]), 0)
        
        # archive and try again
        url = self.HIDE_REPORT_URL + "%d/" % self.activi_id
        d = {"state": "archived"}
        resp = self.client.put(url, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

        url = self.REPORTS_URL + "?state=archived&offset=0&limit=30"

        resp = self.client.get(url)

        self.checkOkResp(resp, 200)

        # not the good way to test , but it will be enough for now
        self.assertEqual(len(ujson.loads(resp.content)["reports"]), 1)

        # archive and try again
        url = self.HIDE_REPORT_URL + "%d/" % self.activi_id
        d = {"state": "active"}
        resp = self.client.put(url, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

        url = self.REPORTS_URL + "?state=archived&offset=0&limit=30"

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)

        self.checkOkResp(resp, 200)

        # not the good way to test , but it will be enough for now
        self.assertEqual(len(ujson.loads(resp.content)), 3)
        self.assertEqual(len(ujson.loads(resp.content)["reports"]), 0)

    def test_get_live_results(self):
        """

        :return:
        """
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(self.LIVE_RESULTS_URL+"%d/" % self.activi_id)

        self.checkOkResp(resp, 200)

        self.assertEqual(ujson.loads(resp.content)["id"], self.activi_id)

    def test_archive_reports(self):
        """

        :return:
        """

        # archive and try again
        url = self.HIDE_REPORT_URL + "%d/" % self.activi_id
        d = {"state": "archived"}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.put(url, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

        activity = ActivityInstance.objects.get(id=self.activi_id)
        self.assertEqual(activity.state, 16)

        url = self.HIDE_REPORT_URL + "%d/" % self.activi_id
        d = {"state": "active"}
        resp = self.client.put(url, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

        activity = ActivityInstance.objects.filter(id=self.activi_id).first()
        self.assertEqual(activity.hide_report, 0)

    def test_archive_archived_reports(self):
        """

        :return:
        """
        url = self.HIDE_REPORT_URL + "%d/" % self.activi_id
        d = {"state": "archived"}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.put(url, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

        url = self.HIDE_REPORT_URL + "%d/" % self.activi_id
        d = {"state": "archived"}
        resp = self.client.put(url, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

        url = self.HIDE_REPORT_URL + "%d/" % self.activi_id
        d = {"state": "active"}
        resp = self.client.put(url, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

    def test_unarchive_normal_reports(self):
        """

        :return:
        """

        url = self.HIDE_REPORT_URL + "%d/" % self.activi_id
        d = {"state": "active"}
        resp = self.client.put(url, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

    def test_ping(self):
        """

        :return:
        """

        resp = self.client.get(self.PING_URL)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.content, "\"OK\"")

    def test_purge_reports(self):
        """

        :return:
        """
        resp = self.client.post(self.PURGE_REPORTS_URL, data=ujson.dumps({"activity_ids":[self.activi_id]}),
                                content_type="application/json")

        self.checkErrResp(resp, 400)

        url = self.HIDE_REPORT_URL + "%d/" % self.activi_id
        d = {"state": "trashed"}
        resp = self.client.put(url, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

        resp = self.client.post(self.PURGE_REPORTS_URL, data=ujson.dumps({"activity_instance_ids": [self.activi_id]}),
                                content_type="application/json")

        logger.error(resp.content)
        self.checkOkResp(resp, 200)

    def test_activity_presence_ok(self):
        """

        :return:
        """

        email = "test%s@socrativetest.com" % str(time.time())
        password = "12345678"

        user = SocrativeUser(email=email, password=password)
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.',"") + str(random.randint(0,1000))
        l.expiration_date = datetime.datetime.utcnow()+datetime.timedelta(days=2)
        l.years=1
        l.price_per_seat=10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        room_name = "test" + str(time.time()).replace('.', '')
        room = Room(name=room_name, name_lower=room_name, created_by=user, status=1)
        room.save()

        room_name2 = "rostered" + str(time.time()).replace(".", "")
        room = Room(name=room_name2, name_lower=room_name2, created_by=user, status=4)
        room.save()

        roster = Roster(room=room, status=0, student_count=2)
        roster.save()

        student = Student(student_id="A", first_name="ala", last_name="bala")
        student.save()
        student.rosters.add(roster)
        student.save()

        student2 = Student(student_id="B", first_name="ala", last_name="bala")
        student2.save()
        student2.rosters.add(roster)
        student2.save()

        d = {"room_name": room_name2}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.START_EXIT_TICKET_QUIZ_URL, data=ujson.dumps(d),
                                content_type='application/json')
        logger.error(resp.content)
        self.checkOkResp(resp, 200)

        activity_id = resp.json()["id"]
        quiz_id = resp.json()["activity_id"]

        # login student A
        # 1.join room -> get user_uuid
        self.client.cookies.pop("ua")
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({"name": room_name2}), content_type="application/json")
        self.checkOkResp(resp, 200)

        stud_uuid = resp.json()["user_uuid"]

        # 2. login student
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth(stud_uuid)
        resp = self.client.post(self.STUDENT_LOGIN_URL, data=ujson.dumps({"student_id": "A", "room_name": room_name2}),
                                content_type="application/json")
        self.checkOkResp(resp, 200)

        resp = self.client.get(self.CREATE_QUIZ_URL + "%d/?room_name=%s" % (quiz_id, room_name2))

        self.checkOkResp(resp, 200)

        self.client.cookies.pop("sa")
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)

        resp = self.client.post(self.END_ACTIVITY_URL, data=ujson.dumps({"room_name": room_name2}),
                                content_type="application/json")
        self.checkErrResp(resp, 400)

        # ask the final results data
        resp = self.client.get(self.LIVE_RESULTS_URL + "%d/" % activity_id)

        self.checkOkResp(resp, 200)

        data = resp.json()

        self.assertEqual(len(data["student_names"]), 2)
        for studentDict in data["student_names"]:
            if studentDict["student_id"] == "A":
                self.assertEqual(studentDict["present"], True)
            else:
                self.assertEqual(studentDict["present"], False)