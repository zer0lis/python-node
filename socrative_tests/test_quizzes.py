# coding=utf-8
import django
django.setup()

from django.test.client import Client
from unittest import TestCase
from socrative import settings as projectSettings
import time

import os
import ujson
from socrative_tornado.publish import PubnubWrapper
import logging
import datetime
from socrative_users.models import SocrativeUser, License_Activations, Licenses
from rooms.models import Room
from quizzes.models import Quiz, Question, Answer
from common.models import MediaResource
import string
import random
from common.cryptography import Cryptography
alphabet = string.digits + string.letters

logger = logging.getLogger(__name__)


class TestQuizzes(TestCase):
    LIST_QUIZZES_URL = '/quizzes/api/quizzes/'
    GET_QUIZ_BY_SOC_URL = '/quizzes/api/get-quiz-by-soc/'
    GET_S3_IMAGE_DATA_URL = '/media/api/get-s3-upload-data/'
    CREATE_QUIZ_URL = "/quizzes/api/quiz/"
    PURGE_QUIZZES_URL = "/quizzes/api/quizzes/purge"
    QUESTIONS_URL = "/quizzes/api/questions/"
    QUESTIONS_RESOURCES_URL = '/quizzes/api/questions/resources'
    ANSWERS_URL = '/quizzes/api/answers/'
    ACTIVITY_REPORT_URL = '/quizzes/api/activity-report/'
    IMPORT_QUIZ_URL = '/quizzes/api/import/'
    DOWNLOAD_REPORT_URL = '/quizzes/api/reports/download/'
    IMPORT_EXCEL_URL = '/quizzes/api/import-excel/'
    ADD_TAG_URL = "/quizzes/api/tag/"
    LIST_TAGS_URL = "/quizzes/api/tags/"
    ADD_TAG_TO_QUIZ_URL = "/quizzes/api/quiz/%d/tags/"
    QUIZ_MERGE_URL = "/quizzes/api/quiz/merge/"


    JOIN_URL = "/rooms/api/join/"
    REGISTER_URL = "/users/register/"
    LOGIN_URL = "/users/login/"
    UPDATE_ROOM_NAME_URL = '/rooms/api/update-room-name'
    START_QUIZ_URL = '/lecturers/api/start-quiz'
    START_1Q_URL = '/lecturers/api/start-quick-question/'
    END_ACTIVITY_URL = '/rooms/api/end-current-activity/'
    SUBMIT_RESPONSE = '/students/api/responses/'
    SET_STUDENT_NAME = "/students/api/set-name/"
    CREATE_ROOM_URL = '/rooms/api/room/'

    @classmethod
    def tearDownClass(cls):
        """

        :param cls:
        :return:
        """
        pass

    @classmethod
    def setUpClass(cls):
        """

        :param cls:
        :return:
        """
        cls.client = Client()
        cls.messages = list()

    def mock_publish(self, message):
        self.messages.append(message)
        return True

    def setUp(self):
        email = "test%s@socrativetest.com" % str(time.time())
        password = "12345678"

        user = SocrativeUser(email=email, password=password)
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        room_name = "test" + str(time.time()).replace(".","")
        room = Room(name=room_name, name_lower=room_name, created_by=user, status=1)
        room.save()

        quiz = Quiz(created_by=user, name=str(time.time()), is_hidden=False, last_updated=datetime.datetime.utcnow())
        quiz.save()
        question1 = Question(question_text="<1 and < 8 are alternate exterior angles", created_by=user, type="TF", order=1)
        question1.quiz = quiz
        question1.save()
        question2 = Question(question_text="mc question text", created_by=user, type="MC", order=2)
        question2.quiz = quiz
        question2.save()
        question3 = Question(question_text="fr question text", created_by=user, type="FR", order=3)
        question3.quiz = quiz
        question3.save()
        question4 = Question(question_text="fr question text with correct answer", created_by=user, type="FR", order=4)
        question4.quiz = quiz
        question4.save()
        answer11 = Answer(is_correct=False, text="False", created_by=user, order=1)
        answer11.question = question1
        answer11.save()
        answer12 = Answer(is_correct=True, text="True", created_by=user, order=2)
        answer12.question = question1
        answer12.save()
        answer21 = Answer(is_correct=True, text="343", created_by=user, order=1)
        answer21.question = question2
        answer21.save()
        answer22 = Answer(is_correct=True, text="434234", created_by=user, order=2)
        answer22.question = question2
        answer22.save()
        answer23 = Answer(is_correct=False, text="adasdsa", created_by=user, order=3)
        answer23.question = question2
        answer23.save()
        answer41 = Answer(is_correct=True, text="true", created_by=user, order=1)
        answer41.question = question4
        answer41.save()

        quiz.questions.add(question1, question2, question3, question4)
        quiz.save()
        quiz.soc_number = str(quiz.id + 5000000)
        quiz.save()

        self.auth_token_user = user.auth_token
        self.soc_number = quiz.soc_number
        self.room_name = room.name
        self.question1 = question1
        self.question2 = question2
        self.question3 = question3
        self.question4 = question4
        self.answer11 = answer11
        self.answer12 = answer12
        self.answer21 = answer21
        self.answer22 = answer22
        self.answer23 = answer23
        self.answer41 = answer41
        self.quiz = quiz
        self.user = user
        PubnubWrapper.override_publish(self.mock_publish)

    def tearDown(self):
        """

        :return:
        """
        self.messages = []
        if "ua" in self.client.cookies:
            self.client.cookies.pop("ua")
        if "sa" in self.client.cookies:
            self.client.cookies.pop("sa")

        if "socrative_lang" in self.client.cookies:
            self.client.cookies.pop("socrative_lang")


    def checkErrResp(self, resp, statusCode):
        """

        :param resp:
        :param statusCode:
        :return:
        """

        logger.info("RAW OUTPUT: %s, STATUS CODE: %d" % (resp.content, resp.status_code))
        self.assertEqual(resp.status_code, statusCode)
        self.assertIn("error", resp.content)

    def checkOkResp(self, resp, statusCode, obj=None):
        """

        :param resp:
        :param statusCode:
        :param obj:
        :return:
        """

        logger.info("RAW OUTPUT: %s, STATUS CODE: %d" % (resp.content if resp._headers.get("content-type")[1] == "application/json" else "{\"msg\":\"Content OK\"}", resp.status_code))
        self.assertEqual(resp.status_code, statusCode)
        if statusCode != 204:
            self.assertNotIn("error", ujson.loads(resp.content) if resp._headers.get("content-type")[1] == "application/json" else "{\"msg\":\"Content OK\"}")

    def check_quiz(self, respQuiz, origQuiz, keysList=None):
        """

        :param respQuiz:
        :param origQuiz:
        :return:
        """

        if keysList is None or "name" in keysList:
            self.assertEqual(respQuiz["name"], origQuiz.name)
        if keysList is None or "last_updated" in keysList:
            self.assertEqual(type(respQuiz["last_updated"]), int)
        if keysList is None or "soc_number" in keysList:
            self.assertEqual(respQuiz["soc_number"], origQuiz.soc_number)
        if keysList is None or "created_date" in keysList:
            self.assertEqual(type(respQuiz["created_date"]), int)
        if keysList is None or "id" in keysList:
            self.assertEqual(respQuiz["id"], origQuiz.id)
        # if keysList is None or "questions" in keysList:
        #     self.assertEqual(len(respQuiz["questions"]), origQuiz.questions.count())
            
    def check_question(self, respQuestion, origQuestion, keyList=None, student=False):
        """
        
        :param respQuestion: 
        :param origQuestion: 
        :return:
        """

        if keyList is None or "question_text" in keyList:
            self.assertEqual(respQuestion["question_text"], origQuestion.question_text)
        if not student and (keyList is None or "explanation" in keyList):
            self.assertEqual(respQuestion["explanation"], origQuestion.explanation)
        if keyList is None or "grading_weight" in keyList:
            self.assertEqual(respQuestion["grading_weight"], origQuestion.grading_weight)
        if keyList is None or "type" in keyList:
            self.assertEqual(respQuestion["type"], origQuestion.type)
        if keyList is None or "order" in keyList:
            self.assertEqual(respQuestion["order"], origQuestion.order)
        if keyList is None or "resources" in keyList:
            self.assertEqual(len(respQuestion["resources"]), origQuestion.resources.count())
        if keyList is None or "question_id" in keyList:
            self.assertEqual(respQuestion["question_id"], origQuestion.question_id)
        if not student and (keyList is None or "answers" in keyList):
            self.assertEqual(len(respQuestion["answers"]), origQuestion.answers.count())

    def check_answer(self, respAnswer, origAnswer, keyList=None):
        """

        :param respAnswer:
        :param origAnswer:
        :param keyList:
        :return:
        """

        if keyList is None or "text" in keyList:
            self.assertEqual(respAnswer["text"], origAnswer.text)
        if keyList is None or "id" in keyList:
            self.assertEqual(respAnswer["id"], origAnswer.id)
        if keyList is None or "order" in keyList:
            self.assertEqual(respAnswer["order"], origAnswer.order)
        if keyList is None or "text" in keyList:
            self.assertEqual(respAnswer["text"], origAnswer.text)
        if keyList is None or "is_correct" in keyList:
            self.assertEqual(respAnswer["is_correct"], origAnswer.is_correct)

    def check_resources(self, respResource, origResource, keyList=None):
        """
        check resources
        :param respResource:
        :param origResource:
        :param keyList:
        :return:
        """

        if keyList is None or "data" in keyList:
            self.assertEqual(respResource["data"], origResource.data)
        if keyList is None or "url" in keyList:
            self.assertEqual(respResource["url"], origResource.url)
        if keyList is None or "id" in keyList:
            self.assertEqual(respResource["id"], origResource.id)
        if keyList is None or "name" in keyList:
            self.assertEqual(respResource["name"], origResource.name)
        if keyList is None or "type" in keyList:
            self.assertEqual(respResource["type"], origResource.type)

    def check_quiz_resp(self, resp, dbModel, status):
        """

        :param resp:
        :param dbModel:
        :param status:
        :return:
        """

        logger.info("RAW OUTPUT: %s, STATUS CODE: %d" % (resp.content if resp._headers.get("content-type")[1] == "application/json" else "{\"msg\":\"Content OK\"}", resp.status_code))
        self.assertEqual(resp.status_code, status)
        self.assertNotIn("error", ujson.loads(resp.content) if resp._headers.get("content-type")[1] == "application/json" else "{\"msg\":\"Content OK\"}")

        content = ujson.loads(resp.content)

        rQuiz = content
        oQuiz = dbModel

        self.check_quiz(rQuiz, oQuiz)
        for j in range(len(rQuiz["questions"])):
            rQuestion = rQuiz["questions"][j]
            for q in oQuiz.questions.all():
                if q.question_id == rQuestion["question_id"]:
                    oQuestion = q
                    break
            self.check_question(rQuestion, oQuestion)

            self.assertEqual(len(rQuestion["resources"]), oQuestion.resources.count())
            if oQuestion.resources.count() > 0:
                self.check_resources(rQuestion["resources"][0], oQuestion.resources.all()[0])
            for k in range(len(rQuestion["answers"])):
                rAnswer = rQuestion["answers"][k]
                for a in oQuestion.answers.all():
                    if a.id == rAnswer["id"]:
                        oAnswer = a
                        break
                self.check_answer(rAnswer, oAnswer)

    def test_list_quizzes(self):
        """

        :return:
        """

        url1 = self.LIST_QUIZZES_URL + '?state=active'
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url1)
        self.checkOkResp(resp, 200)
        content = ujson.loads(resp.content)
        self.assertIn("quizzes", content)
        self.assertEqual(len(content["quizzes"]), 1)


        data = {"soc_number": self.quiz.soc_number}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.quiz.created_by.auth_token)
        resp = self.client.post(self.IMPORT_QUIZ_URL, data=ujson.dumps(data), content_type="application/json")
        self.checkOkResp(resp, 201)

        quiz1 = self.quiz

        quiz2 = Quiz.objects.get(name=self.quiz.name + " (copy)")

        quiz2.state = 128
        quiz2.save()

        # check that the cloned quiz is returned
        url1 = self.LIST_QUIZZES_URL
        resp = self.client.get(url1)
        self.checkOkResp(resp, 200)
        content = ujson.loads(resp.content)
        self.assertIn("quizzes", content)
        self.assertEqual(len(content["quizzes"]), 2)

        data = {"soc_number": quiz2.soc_number}
        resp = self.client.post(self.IMPORT_QUIZ_URL, data=ujson.dumps(data), content_type="application/json")
        self.checkOkResp(resp, 201)

        quiz3 = Quiz.objects.get(name=quiz2.name + " (copy)")

        quiz3.state = 16
        quiz3.save()

        # check that the cloned quiz is returned
        url1 = self.LIST_QUIZZES_URL + '?state=archived'
        resp = self.client.get(url1)
        self.checkOkResp(resp, 200)
        content = ujson.loads(resp.content)
        self.assertIn("quizzes", content)
        self.assertEqual(len(content["quizzes"]), 3)

    def test_create_quiz(self):
        """

        :return:
        """
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.CREATE_QUIZ_URL, data="{}",
                                content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 201)

        content = ujson.loads(resp.content)

        self.assertIsNone(content["last_updated"])
        self.assertEqual(content["name"], "Untitled quiz")
        self.assertEqual(content["soc_number"], str(content["id"] + 5000000))
        self.assertEqual(content["created_by"], self.quiz.created_by_id)
        self.assertEqual(content["questions"], [])
        self.assertEqual(type(content["created_date"]), int)
        self.assertTrue(content["sharable"])
        self.assertFalse(content["is_hidden"])
        self.assertEqual(type(content["id"]), int)

    def test_quiz_api_view(self):
        url = self.CREATE_QUIZ_URL + str(self.quiz.id) + "/?room_name=%s" % self.room_name
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth("anon"+str(time.time).replace('.', ''))
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        content = ujson.loads(resp.content)
        self.check_quiz(content, self.quiz, ["name", "id"])

        for rQuestion in content["questions"]:
            for oQuestion in self.quiz.questions.all():
                if oQuestion.question_id == rQuestion["question_id"]:
                    break
            self.check_question(rQuestion, oQuestion, student=True)

            if oQuestion.resources.count() > 0:
                self.check_resources(rQuestion["resources"][0], oQuestion.resources.all()[0])

            for rAnswer in rQuestion["answers"]:
                for oAnswer in oQuestion.answers.all():
                    if oAnswer.id == rAnswer["id"]:
                        break
                self.check_answer(rAnswer, oAnswer, ["id", "text", "order"])

    def test_teacher_quiz(self):
        """

        :return:
        """
        url = self.CREATE_QUIZ_URL + "%d/" % self.quiz.id
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)
        self.check_quiz_resp(resp, self.quiz, 200)

        resp = self.client.post(self.PURGE_QUIZZES_URL, data=ujson.dumps({"ids":[self.quiz.id]}), content_type="application/json")
        self.assertEqual(resp.status_code, 200)

        url = self.CREATE_QUIZ_URL + "%d/" % self.quiz.id
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        url = self.CREATE_QUIZ_URL  + "%d/" % self.quiz.id
        resp = self.client.put(url, data=ujson.dumps({"name": "new name", "id": self.quiz.id}),
                               content_type="application/json")
        self.checkErrResp(resp, 404)

        url = self.CREATE_QUIZ_URL + "%d/" % self.quiz.id
        resp = self.client.delete(url)
        self.checkErrResp(resp, 405)

        url = self.CREATE_QUIZ_URL + "%d/" % self.quiz.id
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_get_quiz_by_soc(self):
        """

        :return:
        """
        url = self.GET_QUIZ_BY_SOC_URL + str(self.soc_number) + '/'
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)
        self.check_quiz_resp(resp, self.quiz, 200)

    def test_get_quiz_by_soc_error(self):
        """

        :return:
        """
        self.client.cookies["ua"] = Cryptography.encryptUserAuth("asdsadasdadasd")
        url = self.GET_QUIZ_BY_SOC_URL + str(self.soc_number) + '/'
        resp = self.client.get(url)
        self.checkErrResp(resp, 400)

        url = self.GET_QUIZ_BY_SOC_URL + str("0") + '/'
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(url)
        self.checkErrResp(resp, 400)

    def test_get_s3_image_data(self):
        """

        :return:
        """
        url = self.GET_S3_IMAGE_DATA_URL
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        self.assertTrue('amzDate' in resp.content)
        self.assertTrue('access' in resp.content)
        self.assertTrue('uploadDate' in resp.content)
        self.assertTrue('key' in resp.content)
        self.assertTrue('signature' in resp.content)
        self.assertTrue('policy' in resp.content)
        self.assertEqual(resp.status_code, 200)

    def test_activity_report(self):

        PubnubWrapper.override_publish(self.mock_publish)
        d = {"soc_num": self.soc_number, "room_name": self.room_name}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUIZ_URL, data=ujson.dumps(d), content_type='application/json')

        self.checkOkResp(resp, 200)
        self.activi_id = ujson.loads(resp.content)['id']

        d = "room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (self.activi_id, 'dl', 'whole-class-excel')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (self.activi_id, 'dl', 'whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (self.soc_number, 'dl', 'unanswered')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (self.activi_id, 'go', 'whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkErrResp(resp, 400)

        d = "di=%s&dt=%s&rt=%s" % (self.activi_id, 'em', 'whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (self.activi_id, 'go', 'whole-class-excel')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkErrResp(resp, 400)

        d = "di=%s&dt=%s&rt=%s" % (self.activi_id, 'em', 'whole-class-excel')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_free_response_reports(self):
        """

        :return:
        """
        PubnubWrapper.override_publish(self.mock_publish)
        d = {
            "room_name": self.room_name,
            "question_type": "FR",
            "question_text": "what_is_the_question"
        }

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_1Q_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

        activity_id = ujson.loads(resp.content)["id"]
        quiz_id = ujson.loads(resp.content)["activity_id"]

        resp = self.client.get(self.CREATE_QUIZ_URL + str(quiz_id) + "/")

        self.checkOkResp(resp, 200)
        question_id = Question.objects.filter(quiz_id=quiz_id).only('question_id').first().question_id

        answerText = [u"Uno dos tres", u"fête commence", u"Празднование начнется", u"ਜਸ਼ਨ ਸ਼ੁਰੂ ਹੋ",
                      u"kutlama başlar", u"αρχίζουν γιορτή", u"慶祝活動開始",u"お祝い始まる",u"يبدأ الاحتفا", u"축제 시작"]

        for i in range(10):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0,len(alphabet)-1)] for _ in range(20)])
            name = "Josh" + str(i)
            data = {"student_name": name, "activity_instance_id": activity_id}

            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SET_STUDENT_NAME, data=ujson.dumps(data), content_type="application/json")
            d = {'question_id': question_id, 'answer_text': answerText[i],
                 'activity_instance_id': activity_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")

            self.checkOkResp(resp, 201)

        d="room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkErrResp(resp, 400)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkErrResp(resp, 400)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel+whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel+all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id,'dl', 'whole-class+all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkErrResp(resp, 400)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel+all-students+whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_fr_response_reports_download_all_available_languages(self):
        """
        test to see if the reports work with the available languages
        :return:
        """
        PubnubWrapper.override_publish(self.mock_publish)
        for value in projectSettings.LANGUAGES:
            lang = value[0]
            self.client.cookies["socrative_lang"] = lang
            logger.info("*"*20 + "Testing with language %s" % lang)
            self.test_free_response_reports()

    def test_quiz_reports_download(self):
        """

        :return:
        """

        d = {
            "room_name": self.room_name,
            "soc_num": self.quiz.soc_number
        }

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUIZ_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

        activity_id = ujson.loads(resp.content)["id"]
        quiz_id = self.quiz.id
        question_id = self.question3.question_id

        answerText = [u"Uno dos tres", u"fête commence", u"Празднование начнется", u"ਜਸ਼ਨ ਸ਼ੁਰੂ ਹੋ",
                      u"kutlama başlar", u"αρχίζουν γιορτή", u"慶祝活動開始",u"お祝い始まる",u"يبدأ الاحتفا",
                      u"축제 시작"]

        for i in range(10):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0,len(alphabet)-1)] for _ in range(20)])

            name = "Josh" + str(i)
            data = {"student_name": name, "activity_instance_id": activity_id}
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SET_STUDENT_NAME, data=ujson.dumps(data), content_type="application/json")

            self.checkOkResp(resp, 200)

            d = { 'question_id': question_id, 'answer_text': answerText[i],
                 'activity_instance_id': activity_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")

            self.checkOkResp(resp, 201)

        d="room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel+whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel+all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class+all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel+all-students+whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_quiz_reports_download_all_available_languages(self):
        """
        test to see if the reports work with the available languages
        :return:
        """
        PubnubWrapper.override_publish(self.mock_publish)
        for value in projectSettings.LANGUAGES:
            lang = value[0]
            self.client.cookies["socrative_lang"] = lang
            logger.info("*" * 20 + "Testing with language %s" % lang)
            self.test_quiz_reports_download()

    def test_quiz_reports_email(self):
        """

        :return:
        """

        d = 'room_name=%s&soc_num=%s' % (self.room_name, self.quiz.soc_number)
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUIZ_URL, data=d, content_type="application/x-www-form-urlencoded")

        self.checkOkResp(resp, 200)

        activity_id = ujson.loads(resp.content)["id"]
        quiz_id = self.quiz.id
        question_id = self.question3.question_id

        answerText = [u"Uno dos tres", u"fête commence", u"Празднование начнется", u"ਜਸ਼ਨ ਸ਼ੁਰੂ ਹੋ",
                      u"kutlama başlar", u"αρχίζουν γιορτή", u"慶祝活動開始",u"お祝い始まる",u"يبدأ الاحتفا",
                      u"축제 시작"]

        for i in range(10):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0,len(alphabet)-1)] for _ in range(20)])

            name = "Josh" + str(i)
            data = {"student_name": name, "activity_instance_id": activity_id}
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SET_STUDENT_NAME, data=ujson.dumps(data), content_type="application/json")

            self.checkOkResp(resp, 200)

            d = {'question_id': question_id, 'answer_text': answerText[i],
                 'activity_instance_id': activity_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")

            self.checkOkResp(resp, 201)

        d="room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'em', 'whole-class-excel')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'em', 'whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'em', 'all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'em', 'whole-class-excel+whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'em', 'whole-class-excel+all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'em', 'whole-class+all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'em', 'whole-class-excel+all-students+whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_quiz_reports_empty_responses(self):
        """

        :return:
        """

        d = 'room_name=%s&soc_num=%s' % (self.room_name, self.quiz.soc_number)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUIZ_URL, data=d, content_type="application/x-www-form-urlencoded")

        self.checkOkResp(resp, 200)

        activity_id = ujson.loads(resp.content)["id"]
        quiz_id = self.quiz.id
        question_id = self.question3.question_id

        answerText = [" "*i for i in range(10)]

        for i in range(10):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

            name = "Josh" + str(i)
            data = {"student_name": name, "activity_instance_id": activity_id}
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SET_STUDENT_NAME, data=ujson.dumps(data), content_type="application/json")

            self.checkOkResp(resp, 200)

            d = {'question_id': question_id, 'answer_text': answerText[i],
                 'activity_instance_id': activity_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")

            self.checkOkResp(resp, 201)

        d="room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel+whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel+all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class+all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel+all-students+whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_quiz_reports_empty_quiz_name(self):
        """

        :return:
        """

        self.quiz.name = "      "
        self.quiz.save()
        d = 'room_name=%s&soc_num=%s' % (self.room_name, self.quiz.soc_number)
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUIZ_URL, data=d, content_type="application/x-www-form-urlencoded")

        self.checkOkResp(resp, 200)

        activity_id = ujson.loads(resp.content)["id"]
        quiz_id = self.quiz.id
        question_id = self.question3.question_id

        answerText = [" "*i for i in range(10)]

        for i in range(10):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

            name = "Josh" + str(i)
            data = {"student_name": name, "activity_instance_id": activity_id}
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SET_STUDENT_NAME, data=ujson.dumps(data), content_type="application/json")

            self.checkOkResp(resp, 200)

            d = {'question_id': question_id, 'answer_text': answerText[i],
                 'activity_instance_id': activity_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")

            self.checkOkResp(resp, 201)

        d="room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_quiz_reports_empty_question_texts(self):
        """

        :return:
        """

        self.quiz.name = "      "
        self.quiz.save()
        self.question1.question_text = " "
        self.question2.question_text = " "
        self.question3.question_text = " "
        self.question4.question_text = " "
        self.question1.save()
        self.question2.save()
        self.question3.save()
        self.question4.save()
        d = 'room_name=%s&soc_num=%s' % (self.room_name, self.quiz.soc_number)
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUIZ_URL, data=d, content_type="application/x-www-form-urlencoded")

        self.checkOkResp(resp, 200)

        activity_id = ujson.loads(resp.content)["id"]
        quiz_id = self.quiz.id
        question_id = self.question3.question_id

        answerText = [" "*i for i in range(10)]

        for i in range(10):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

            name = "Josh" + str(i)
            data = {"student_name": name, "activity_instance_id": activity_id}
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SET_STUDENT_NAME, data=ujson.dumps(data), content_type="application/json")

            self.checkOkResp(resp, 200)

            d = {'question_id': question_id, 'answer_text': answerText[i],
                 'activity_instance_id': activity_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")

            self.checkOkResp(resp, 201)

        d="room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_quiz_reports_unicode_chars(self):
        """

        :return:
        """

        self.quiz.name = u"中華民族"
        self.quiz.save()
        self.question1.question_text = u"中華民族"
        self.question2.question_text = u"中華民族"
        self.question3.question_text = u"中華民族"
        self.question4.question_text = u"中華民族"
        self.question1.save()
        self.question2.save()
        self.question3.save()
        self.question4.save()
        d = 'room_name=%s&soc_num=%s' % (self.room_name, self.quiz.soc_number)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUIZ_URL, data=d, content_type="application/x-www-form-urlencoded")

        self.checkOkResp(resp, 200)

        activity_id = ujson.loads(resp.content)["id"]
        quiz_id = self.quiz.id
        question_id = self.question3.question_id

        answerText = ["中華民族"*i for i in range(10)]

        for i in range(10):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

            name = "Josh" + str(i)
            data = {"student_name": name, "activity_instance_id": activity_id}
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SET_STUDENT_NAME, data=ujson.dumps(data), content_type="application/json")

            self.checkOkResp(resp, 200)

            d = {'question_id': question_id, 'answer_text': answerText[i],
                 'activity_instance_id': activity_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")

            self.checkOkResp(resp, 201)

        d = "room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_quiz_reports_htmlresponse(self):
        """

        :return:
        """

        self.quiz.name = u"中華民族"
        self.quiz.save()
        self.question1.question_text = u"中華民族"
        self.question2.question_text = u"中華民族"
        self.question3.question_text = u"中華民族"
        self.question4.question_text = u"中華民族"
        self.question1.save()
        self.question2.save()
        self.question3.save()
        self.question4.save()
        d = 'room_name=%s&soc_num=%s' % (self.room_name, self.quiz.soc_number)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUIZ_URL, data=d, content_type="application/x-www-form-urlencoded")

        self.checkOkResp(resp, 200)

        activity_id = ujson.loads(resp.content)["id"]
        quiz_id = self.quiz.id
        question_id = self.question3.question_id

        answerText = ["<!DOCUMENT html>" for i in range(10)]

        for i in range(10):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

            name = "Josh" + str(i)
            data = {"student_name": name, "activity_instance_id": activity_id}
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SET_STUDENT_NAME, data=ujson.dumps(data), content_type="application/json")

            self.checkOkResp(resp, 200)

            d = {'question_id': question_id, 'answer_text': answerText[i],
                 'activity_instance_id': activity_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")

            self.checkOkResp(resp, 201)

        d="room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_quiz_reports_htmlimage(self):
        """

        :return:
        """

        self.quiz.name = "&lt;img src='https://dl.dropboxusercontent.com/u/11182744/bird.jpg' /&gt;"
        self.quiz.save()
        self.question1.question_text = "&lt;img src='https://dl.dropboxusercontent.com/u/11182744/bird.jpg' /&gt;"
        self.question2.question_text = "<img src='https://dl.dropboxusercontent.com/u/11182744/bird.jpg' />"
        self.question3.question_text = "<img src='https://dl.dropboxusercontent.com/u/11182744/bird.jpg' />"
        self.question4.question_text = "<img src='https://dl.dropboxusercontent.com/u/11182744/bird.jpg' />"
        self.question1.save()
        self.question2.save()
        self.question3.save()
        self.question4.save()
        self.answer21.text = "<img src='https://dl.dropboxusercontent.com/u/11182744/bird.jpg' />"
        self.answer21.save()
        d = 'room_name=%s&soc_num=%s' % (self.room_name, self.quiz.soc_number)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUIZ_URL, data=d, content_type="application/x-www-form-urlencoded")

        self.checkOkResp(resp, 200)

        activity_id = ujson.loads(resp.content)["id"]
        quiz_id = self.quiz.id
        question_id = self.question3.question_id

        answerText = ["<img src='https://dl.dropboxusercontent.com/u/11182744/bird.jpg' />" for i in range(10)]

        for i in range(10):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

            name = "Josh" + str(i)
            data = {"student_name": name, "activity_instance_id": activity_id}
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SET_STUDENT_NAME, data=ujson.dumps(data), content_type="application/json")

            self.checkOkResp(resp, 200)

            d = {'question_id': question_id, 'answer_text': answerText[i],
                 'activity_instance_id': activity_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")

            self.checkOkResp(resp, 201)

        d="room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_quiz_reports_phpcode(self):
        """

        :return:
        """

        self.quiz.name = "&lt;img src='https://dl.dropboxusercontent.com/u/11182744/bird.jpg' /&gt;"
        self.quiz.save()
        self.question1.question_text = "&lt;img src='https://dl.dropboxusercontent.com/u/11182744/bird.jpg' /&gt;"
        self.question2.question_text = "<img src='https://dl.dropboxusercontent.com/u/11182744/bird.jpg' />"
        self.question3.question_text = "<img src='https://dl.dropboxusercontent.com/u/11182744/bird.jpg' />"
        self.question4.question_text = "<img src='https://dl.dropboxusercontent.com/u/11182744/bird.jpg' />"
        self.question1.save()
        self.question2.save()
        self.question3.save()
        self.question4.save()
        self.answer21.text = "<img src='https://dl.dropboxusercontent.com/u/11182744/bird.jpg' />"
        self.answer21.save()
        d = 'room_name=%s&soc_num=%s' % (self.room_name, self.quiz.soc_number)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUIZ_URL, data=d, content_type="application/x-www-form-urlencoded")

        self.checkOkResp(resp, 200)

        activity_id = ujson.loads(resp.content)["id"]
        quiz_id = self.quiz.id
        question_id = self.question3.question_id

        answerText = [u"""'<?php\n$dia_hora_atual = strtotime(date("2017-1-1 00:00:01"));\n$diferenca = $dia_hora_eve"""\
                      """nto - $dia_hora_atual;\n$segundos = $marcador % 60;\necho "$segundos segundo(s)";\n?>"""\
                      for _ in range(40)]

        for i in range(40):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

            name = "Josh" + str(i)
            data = {"student_name": name, "activity_instance_id": activity_id}
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SET_STUDENT_NAME, data=ujson.dumps(data), content_type="application/json")

            self.checkOkResp(resp, 200)

            d = {'question_id': question_id, 'answer_text': answerText[i],
                 'activity_instance_id': activity_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")

            self.checkOkResp(resp, 201)

        d="room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'all-students')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_quiz_report_bad_png(self):
        """

        :return:

        """

        resource = MediaResource()
        resource.name = "bad_png"
        resource.type = MediaResource.IMAGE
        #image uploaded to s3
        resource.url = "https://s3.amazonaws.com/socrative/xOV5wQsMQhKugah3pBtz_bad_pngpng"
        resource.owner = self.quiz.created_by
        resource.save()

        self.question1.resources.add(resource)
        self.question1.save()
        logger.info(resource.url)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)

        d = "di=%s&dt=%s&rt=%s" % (self.soc_number, 'dl', 'unanswered')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_quiz_report_bad_png2(self):
        """

        :return:

        """
        resource = MediaResource()
        resource.name = "bad_png2"
        resource.type = MediaResource.IMAGE
        #image uploaded to s3
        resource.url = "https://s3.amazonaws.com/socrative/kscBrKqESOl9bZS5riW0_transparent_partpng"
        resource.owner = self.quiz.created_by
        resource.save()

        self.question1.resources.add(resource)
        self.question1.save()
        logger.info(resource.url)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        d = "di=%s&dt=%s&rt=%s" % (self.soc_number, 'dl', 'unanswered')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_quiz_report_bad_bmp(self):
        """

        :return:

        """

        resource = MediaResource()
        resource.name = "bad_png"
        resource.type = MediaResource.IMAGE
        #image uploaded to s3
        resource.url = "https://s3.amazonaws.com/socrative/P5tw1MQCmMHmINUBMkAH_unsuported_bmpbmp"
        resource.owner = self.quiz.created_by
        resource.save()

        self.question1.resources.add(resource)
        self.question1.save()
        logger.info(resource.url)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        d = "di=%s&dt=%s&rt=%s" % (self.soc_number, 'dl', 'unanswered')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_import_quiz_by_excel_ok(self):
        """

        :return:
        """
        fpath = os.path.abspath(os.path.dirname(__file__)+"/data/socrativeQuiz.xlsx")
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        with open(fpath, "rb") as fp:
            resp = self.client.post(self.IMPORT_EXCEL_URL, data={'0': [fp]})

        self.checkOkResp(resp, 201)

        quiz = Quiz.objects.filter(name='MyQuiz23').order_by("-created_date").first()
        self.assertEqual(quiz.name, "MyQuiz23")
        self.assertEqual(quiz.questions.count(), 8)

    def test_create_question_ok(self):
        """

        :return:
        """

        d = {'quiz_id': self.quiz.id,
             'question_text': 'what is the meaning of life',
             'order': 5,
             'grading_weight':1.0,
             'type': 'TF',
             'auth_token': self.auth_token_user,
             'answers': [{'text':'True','is_correct':True, 'order':1},
                         {'text':'False','is_correct':False, 'order':2}]}

        resp = self.client.post(self.QUESTIONS_URL, data=ujson.dumps(d), content_type='application/json')

        self.checkOkResp(resp, 201)

    def test_update_quiz_ok(self):
        """

        :return:
        """

        d = {'name': 'new quiz', 'is_sharable': True}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.CREATE_QUIZ_URL, data = ujson.dumps(d), content_type='application/json')

        self.checkOkResp(resp, 201)

        qIz = ujson.loads(resp.content)["id"]

        d = {'name': 'quiz new',
             'create': True,
             'soc_number': str(5000000+qIz),
             'sharable': False,
             'is_hidden': True}

        resp = self.client.put(self.CREATE_QUIZ_URL+"%d/" %  qIz, data = ujson.dumps(d), content_type='application/json')

        self.checkOkResp(resp, 200)

    def test_update_quiz_ok_with_standard(self):
        """

        :return:
        """

        d = {'name': 'new quiz', 'is_sharable': True}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.CREATE_QUIZ_URL, data = ujson.dumps(d), content_type='application/json')

        self.checkOkResp(resp, 201)

        qIz = ujson.loads(resp.content)["id"]

        d = {'name': 'quiz new',
             'create': True,
             'soc_number': str(5000000+qIz),
             'sharable': False,
             'is_hidden': True,
             'standard': {'standard_id': 1, 'core_id': 12, 'grade_id': 100, 'name':'MATA.SR.1', 'subject_id': 1}}

        resp = self.client.put(self.CREATE_QUIZ_URL+"%d/" % qIz, data = ujson.dumps(d),
                               content_type='application/json')

        self.checkOkResp(resp, 200)

    def test_clone_quiz_with_standards(self):
        """

        :return:
        """

        d = {'name': 'new quiz', 'is_sharable': True}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.CREATE_QUIZ_URL, data=ujson.dumps(d), content_type='application/json')

        self.checkOkResp(resp, 201)

        qIz = ujson.loads(resp.content)["id"]

        d = {'name': 'quiz new',
             'create': True,
             'soc_number': str(5000000+qIz),
             'sharable': True,
             'is_hidden': False,
             'standard': {'standard_id': 1, 'core_id': 12, 'grade_id': 100, 'name': 'MATA.SR.1', 'subject_id': 1}}

        resp = self.client.put(self.CREATE_QUIZ_URL+"%d/" % qIz, data = ujson.dumps(d),
                               content_type='application/json')

        self.checkOkResp(resp, 200)
        data = {"soc_number": str(5000000+qIz)}
        resp = self.client.post(self.IMPORT_QUIZ_URL, data=ujson.dumps(data), content_type="application/json" )
        self.checkOkResp(resp, 201)

    def test_clone_quiz_with_standards_quiz_not_sharable(self):
        """

        :return:
        """

        d = {'name': 'new quiz', 'is_sharable': True}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.CREATE_QUIZ_URL, data = ujson.dumps(d), content_type='application/json')

        self.checkOkResp(resp, 201)

        qIz = ujson.loads(resp.content)["id"]

        d = {'name': 'quiz new',
             'create': True,
             'soc_number': str(5000000+qIz),
             'sharable': False,
             'is_hidden': False,
             'standard': {'standard_id': 1, 'core_id': 12, 'grade_id': 100, 'name':'MATA.SR.1', 'subject_id': 1}}

        resp = self.client.put(self.CREATE_QUIZ_URL+"%d/" % qIz, data = ujson.dumps(d),
                               content_type='application/json')

        self.checkOkResp(resp, 200)
        data = {"soc_number": str(5000000 + qIz)}
        resp = self.client.post(self.IMPORT_QUIZ_URL, data=ujson.dumps(data), content_type="application/json")
        self.checkOkResp(resp, 400)

    def test_remove_standard(self):
        """

        :return:
        """

        d = {'name': 'new quiz', 'user_uuid':self.auth_token_user, 'is_sharable': True}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.CREATE_QUIZ_URL, data=ujson.dumps(d), content_type='application/json')

        self.checkOkResp(resp, 201)

        qIz = ujson.loads(resp.content)["id"]

        d = {'name': 'quiz new',
             'create': True,
             'soc_number': str(5000000+qIz),
             'sharable': False,
             'is_hidden': True,
             'standard': {'standard_id': 1, 'core_id': 12, 'grade_id': 100, 'name':'MATA.SR.1', 'subject_id': 1}}

        resp = self.client.put(self.CREATE_QUIZ_URL+"%d/" % qIz, data = ujson.dumps(d),
                               content_type='application/json')

        self.checkOkResp(resp, 200)

        d = {'name': 'quiz new2',
             'create': True,
             'soc_number': str(5000000+qIz),
             'sharable': False,
             'is_hidden': True,
             }

        resp = self.client.put(self.CREATE_QUIZ_URL+"%d/" %  qIz, data=ujson.dumps(d),
                               content_type='application/json')

        self.checkOkResp(resp, 200)

    def test_change_standard(self):
        """

        :return:
        """

        d = {'name': 'new quiz', 'is_sharable': True}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.CREATE_QUIZ_URL, data=ujson.dumps(d), content_type='application/json')

        self.checkOkResp(resp, 201)

        qIz = ujson.loads(resp.content)["id"]

        d = {'name': 'quiz new',
             'create': True,
             'soc_number': str(5000000+qIz),
             'sharable': True,
             'is_hidden': False,
             'standard': {'standard_id': 1, 'core_id': 12, 'grade_id': 100, 'name': 'MATA.SR.1', 'subject_id': 1}}

        resp = self.client.put(self.CREATE_QUIZ_URL+"%d/" % qIz, data=ujson.dumps(d),
                               content_type='application/json')

        self.checkOkResp(resp, 200)

        d = {'name': 'quiz new2',
             'create': True,
             'soc_number': str(5000000+qIz),
             'sharable': False,
             'is_hidden': True,
             'standard': {'standard_id': 2, 'core_id': 123, 'grade_id': 100, 'name':'MATA.SR.1', 'subject_id': 1}
             }

        resp = self.client.put(self.CREATE_QUIZ_URL+"%d/" % qIz, data=ujson.dumps(d),
                               content_type='application/json')

        self.checkOkResp(resp, 200)

    def test_quiz_reports_freeresponse_url_in_response(self):
        """

        :return:
        """

        d = {
            "room_name": self.room_name,
            "question_type": "FR",
            "question_text": "what is the question"
        }
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)

        resp = self.client.post(self.START_1Q_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 200)

        activity_id = ujson.loads(resp.content)["id"]
        quiz_id = ujson.loads(resp.content)["activity_id"]

        resp = self.client.get(self.CREATE_QUIZ_URL + str(quiz_id) + "/")

        self.checkOkResp(resp, 200)
        question_id = Question.objects.filter(quiz_id=quiz_id).only('question_id').first().question_id

        answerText = ["http://www.google.com/#mata#tactu", "https://facebook.com/",
                      "https://www.loogle.com/#string1#string2#string3#"]

        for i in range(10):
            studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])
            name = "Josh" + str(i)
            data = {"student_name": name, "activity_instance_id": activity_id}
            self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
            resp = self.client.post(self.SET_STUDENT_NAME, data=ujson.dumps(data), content_type="application/json")
            d = {'question_id': question_id, 'answer_text': answerText[i%3],
                 'activity_instance_id': activity_id, 'multi_response': False}
            resp = self.client.post(self.SUBMIT_RESPONSE, data=ujson.dumps(d), content_type="application/json")

            self.checkOkResp(resp, 201)

        d="room_name=%s" % self.room_name
        resp = self.client.post(self.END_ACTIVITY_URL, data=d, content_type="application/x-www-form-urlencoded")
        self.checkOkResp(resp, 200)

        d = "di=%s&dt=%s&rt=%s" % (activity_id, 'dl', 'whole-class-excel')
        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)

    def test_formatting_in_pdf_reports_ok(self):
        """
        """

        self.quiz.name = "“&#33; &#64; # $ &#37; ^ &amp; * ( ) _ + - &#61; [ ] \ &#123; &#125; | ; &#039; : &quot; , . / &lt; &gt; ?”"
        self.quiz.save()
        self.question1.question_text = """<p><strong>YES FORMATTING IN THIS QUESTION</strong></p><p><strong><br data-mce-bogus="1"></strong></p><p>﻿</p><p>[[Ä Ë Ï Ö Ü Ÿ ä ë ï ö ü ÿ Ã Ñ Õ ã ñ õ Â Ê Î Ô Û â ê î ô û Á É Í Ó Ú á é í ó ú À È Ì Ò Ù à è ì ò ù å ∫ ç ∂ ´ƒ © ˙ ˆ∆ ˚ ¬ µ ˜ø π œ ® ß † ¨ √ ∑ ≈ ¥ Ω Å ı Ç Î ´ Ï ˝ Ó ˆ Ô &nbsp;Ò Â ˜ Ø ∏ Œ ‰ Í ˇ ¨ ◊ „ ˛ Á ¸ ¡ ™ £ ¢ ∞ § ¶ • ª º – ≠ « ‘ “ æ … ÷ ≥ ≤ `` ⁄ € ‹ › ﬁ ﬂ ‡ ° · ‚ — ± » ’ ” Æ Ú ¿ ˘ ¯ `</p><p>! @ # $ % ^ &amp; * ( ) _ + - = [ ] \ { } | ; ' : " , . / &lt; &gt; ?]]</p><p>ͱ Ͳ ͳ ʹ ͵ Ͷ ͷ ͺ ͻ ͼ ͽ ; Ϳ ΄ ΅ Ά · Έ Ή Ί Ό Ύ Ώ ΐ Α Β Γ Δ Ε Ζ Η Θ Ι Κ Λ Μ Ν Ξ Ο Π Ρ Σ Τ Υ Φ Χ Ψ Ω Ϊ Ϋ ά έ ή ί ΰ α β γ δ ε ζ η θ ι κ λ μ ν ξ ο π ρ ς σ τ υ φ χ ψ ω ϊ ϋ ό ύ ώ Ϗ ϐ ϑ ϒ ϓ ϔ ϕ ϖ ϗ Ϙ ϙ Ϛ ϛ Ϝ ϝ Ϟ ϟ Ϡ ϡ Ϣ ϣ Ϥ ϥ Ϧ ϧ Ϩ ϩ Ϫ ϫ Ϭ ϭ Ϯ ϯ ϰ ϱ ϲ ϳ ϴ ϵ ϶ Ϸ ϸ Ϲ Ϻ ϻ ϼ Ͻ Ͼ Ͽ</p>"""
        self.question2.question_text = "asdasd"
        self.question3.question_text = "sdasd"
        self.question4.question_text = "asdadas"
        self.question1.save()
        self.question2.save()
        self.question3.save()
        self.question4.save()
        self.answer21.text = """<p><sup><sub><span class="underline"><em><strong>ø π œ ® ß † ¨ √ ∑ ≈ ¥ Ω Å ı Ç Î ´ Ï ˝ Ó ˆ Ô &nbsp;Ò Â ˜ Ø ∏ Œ ‰ Í ˇ ¨ ◊ „ ˛ Á ¸ ¡ ™ £ ¢ ∞ § ¶ • ª º – ≠ « ‘ “ æ … ÷ ≥ ≤ `` ⁄ € ‹ › ﬁ ﬂ ‡ ° · ‚ — ± » ’ ” Æ Ú ¿ ˘ ¯ `</strong></em></span></sub></sup></p><p><sup><sub><span class="underline"><em><strong>! @ # $ % ^ &amp; * ( ) _ + - = [ ] \ { } | ; ' : " , . / &lt; &gt; ?]]</strong></em></span></sub></sup></p><p><sup><sub><span class="underline"><em><strong>ͱ Ͳ ͳ ʹ ͵ Ͷ ͷ ͺ ͻ ͼ ͽ ; Ϳ ΄ ΅ Ά · Έ Ή Ί Ό Ύ Ώ ΐ Α Β Γ Δ Ε Ζ Η Θ Ι Κ Λ Μ Ν Ξ Ο Π Ρ Σ Τ Υ Φ Χ Ψ Ω Ϊ Ϋ ά έ ή ί ΰ α β γ δ ε ζ η θ ι κ λ μ ν ξ ο π ρ ς σ τ υ φ χ ψ ω ϊ ϋ ό ύ ώ Ϗ ϐ ϑ ϒ ϓ ϔ ϕ ϖ ϗ Ϙ ϙ Ϛ ϛ Ϝ ϝ Ϟ ϟ Ϡ ϡ Ϣ ϣ Ϥ ϥ Ϧ ϧ Ϩ ϩ Ϫ ϫ Ϭ ϭ Ϯ ϯ ϰ ϱ ϲ ϳ ϴ ϵ ϶ Ϸ ϸ Ϲ Ϻ ϻ ϼ Ͻ Ͼ Ͽ</strong></em></span></sub></sup></p>"""
        self.answer21.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        d = "di=%s&dt=%s&rt=%s" % (self.soc_number, 'dl', 'unanswered')

        url = self.ACTIVITY_REPORT_URL + '?' + d
        resp = self.client.get(url)
        self.checkOkResp(resp, 200)
