# coding=utf-8

import django
django.setup()

from django.test.client import Client
from unittest import TestCase, skip
import time
import ujson
import urllib
from socrative_tornado.publish import PubnubWrapper
import logging
import datetime
import random
from socrative_users.models import SocrativeUser, UserSysMsg, License_Activations, Licenses, SystemMessage
from quizzes.models import Quiz, Question, Answer
from rooms.models import Room
import string
from common.cryptography import Cryptography

logger = logging.getLogger(__name__)
alphabet = string.digits + string.lowercase


class TestRooms(TestCase):
    """
    class to test the socrative_users api's
    """
    JOIN_URL = "/rooms/api/join/"
    CLEAR_URL = '/rooms/api/clear/'
    CURRENT_ACTIVITY_URL = '/rooms/api/current-activity'
    UPDATE_INSTANCE_ACTIVITY = '/rooms/api/update-activity-instance-state'
    END_CURRENT_ACTIVITY = '/rooms/api/end-current-activity'
    CHECK_NAME_URL = '/rooms/api/check-room-name/'
    SYSTEM_MESSAGE_POST_URL = '/rooms/api/post-message/'
    SYSTEM_MESSAGE_GET_URL = '/rooms/api/system-message/'
    CREATE_ROOM_URL = '/rooms/api/room/'
    ROSTER_SETTINGS_URL = "/rooms/api/roster-settings/"
    ROSTER_SETTINGS_UPDATE_IMPORT_URL = "/rooms/api/roster-settings/import/"
    ROSTER_SETTINGS_UPDATE_EXPORT_URL = "/rooms/api/roster-settings/export/"

    REGISTER_URL = "/users/register/v1/"
    LOGIN_URL = "/users/login/"
    CREATE_QUIZ = "/quizzes/api/quiz/"
    START_QUIZ_URL = '/lecturers/api/start-quiz'
    SUBMIT_STUDENT_RESPONSE_URL = '/students/api/responses/'
    SET_NAME_URL = '/students/api/set-name/'
    GET_ROOM_LIST = '/rooms/api/list/'
    AVAILABLE_ROOM_URL = '/rooms/api/available/'
    ROOM_STATUS_URL = '/rooms/api/status/'
    LOGOUT_URL = '/users/api/logout/'



    @classmethod
    def tearDownClass(cls):
        """

        :param cls:
        :return:
        """
        pass

    @classmethod
    def setUpClass(cls):
        """

        :param cls:
        :return:
        """
        cls.client = Client()
        cls.messages = list()

        try:
            user = SocrativeUser.objects.get(email='jenkins@socrative.com')
        except SocrativeUser.DoesNotExist:
            user = SocrativeUser(email='jenkins@socrative.com', password='123456')
            user.display_name = Cryptography.generate_random_password(6)
            user.auth_token = Cryptography.generate_auth_token()
            user.last_login = datetime.datetime.utcnow()
            user.date_joined = datetime.datetime.utcnow()
            user.save()

        cls.admin_auth_token = user.auth_token


    def mock_publish(self, message):
        self.messages.append(message)
        return True

    def checkErrResp(self, resp, statusCode):
        """

        :param resp:
        :param statusCode:
        :return:
        """

        logger.info("RAW OUTPUT: %s, STATUS CODE: %d" % (resp.content, resp.status_code))
        self.assertEqual(resp.status_code, statusCode)
        self.assertIn("error", resp.content)

    def checkOkResp(self, resp, statusCode, obj=None):
        """
        """

        logger.info("RAW OUTPUT: %s, STATUS CODE: %d" % (resp.content if resp._headers.get("content-type")[1] == "application/json" else "{\"msg\":\"Content OK\"}", resp.status_code))
        self.assertEqual(resp.status_code, statusCode)
        if statusCode != 204:
            self.assertNotIn("error", ujson.loads(resp.content) if resp._headers.get("content-type")[1] == "application/json" else "{\"msg\":\"Content OK\"}")

    def setUp(self):
        """

        :return:
        """

        PubnubWrapper.override_publish(self.mock_publish)

        email = "test%s@socrativetest.com" % str(time.time())
        password = "12345678"

        d = {"email": email, "password": password, "role": ["teacher"], "org_type": "OTHR",
             "first_name":"aaa", "last_name": "bbb", "country": "US", "org_name": "P5"}
        resp = self.client.post(self.REGISTER_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.assertEqual(resp.status_code, 201)

        d = "email=%s&password=12345678" % email
        resp = self.client.post(self.LOGIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 200)

        self.auth_token_user = Cryptography.decrypt(resp.cookies["ua"].value)
        self.user_id = ujson.loads(resp.content).get("id")

        resp = self.client.post(self.JOIN_URL, data="{}",
                                content_type="application/json")
        self.assertEqual(resp.status_code, 201)

        resp = ujson.loads(resp.content)
        rName = resp.get("name")
        self.room_id = resp.get("id")

        self.room_name = 'test' + str(time.time()).replace('.', '')

        room = Room.objects.get(name_lower=rName.lower())
        room.name = self.room_name
        room.name_lower = self.room_name.lower()
        room.created_by_id = self.user_id
        room.status = 1
        room.save()

        quiz = Quiz(created_by_id=self.user_id, name=str(time.time()), is_hidden=False, last_updated=datetime.datetime.utcnow())
        quiz.save()
        question1 = Question(question_text="tf question text", created_by_id=self.user_id, type="TF", order=1, quiz=quiz)
        question1.save()
        question2 = Question(question_text="mc question text", created_by_id=self.user_id, type="MC", order=2, quiz=quiz)
        question2.save()
        question3 = Question(question_text="fr question text", created_by_id=self.user_id, type="FR", order=3, quiz=quiz)
        question3.save()
        question4 = Question(question_text="fr question text with correct answer", created_by_id=self.user_id, type="FR",
                             order=4, quiz=quiz)
        question4.save()

        answer11 = Answer(is_correct=False, text="False", created_by_id=self.user_id, order=1,question=question1)
        answer11.save()
        answer12 = Answer(is_correct=True, text="True", created_by_id=self.user_id, order=2,question=question1)
        answer12.save()
        answer21 = Answer(is_correct=True, text="343", created_by_id=self.user_id, order=1, question=question2)
        answer21.save()
        answer22 = Answer(is_correct=True, text="434234", created_by_id=self.user_id, order=2, question=question2)
        answer22.save()
        answer23 = Answer(is_correct=False, text="adasdsa", created_by_id=self.user_id, order=3, question=question2)
        answer23.save()
        answer41 = Answer(is_correct=True, text="true", created_by_id=self.user_id, order=1, question=question4)
        answer41.save()

        quiz.soc_number = str(5000000+quiz.id)
        quiz.save()

        self.soc_number = quiz.soc_number
        self.room_name = room.name
        self.question1 = question1
        self.question2 = question2
        self.question3 = question3
        self.question4 = question4
        self.answer11 = answer11
        self.answer12 = answer12
        self.answer21 = answer21
        self.answer22 = answer22
        self.answer23 = answer23
        self.answer41 = answer41
        self.quiz = quiz

        self.user_uid_random = 'anno%s' % str(time.time())
        PubnubWrapper.override_publish(self.mock_publish)

        if "sa" in self.client.cookies:
            self.client.cookies.pop("sa")

        if "ua" in self.client.cookies:
            self.client.cookies.pop("ua")

    def tearDown(self):
        """

        :return:
        """
        SystemMessage.objects.raw("delete from rooms_systemmessage")
        UserSysMsg.objects.raw("delete from socrative_users_usersysmsg")

        if "sa" in self.client.cookies:
            self.client.cookies.pop("sa")

        if "ua" in self.client.cookies:
            self.client.cookies.pop("ua")

    def test_join(self):
        """

        :return:
        """
        d = "name=%s" % self.room_name
        self.client.cookies["sa"] = Cryptography.encryptUserAuth(self.user_uid_random)
        resp = self.client.post(self.JOIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 200)

        d = "name=%s" % self.room_name
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.JOIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 200)

        d = {"name": " "}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.assertEqual(resp.status_code, 400)

        d = "user_uuid=%s&name=%s" % ("", "")
        resp = self.client.post(self.JOIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 400)

        d = "user_uuid=%s&name=%s" % (self.auth_token_user, ".2000")
        resp = self.client.post(self.JOIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 400)

        d = "user_uuid=%s&name=%s" % (self.auth_token_user,
                                      "fdsfsdkhfkskfhksjhfkjshdfkjhsdkjfhsdkjhfkjsdhfksfdsdfsdfsdfsdfsjsdhfkjshdkjfhsd")
        resp = self.client.post(self.JOIN_URL, data=d,
                                content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 400)

    def test_clear(self):
        PubnubWrapper.override_publish(self.mock_publish)
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        d = {"room_name": self.room_name}

        resp = self.client.post(self.CLEAR_URL, data=ujson.dumps(d),
                                content_type="application/json")
        self.assertEqual(resp.status_code, 200)

    def test_current_activity_ok(self):
        """

        :return:
        """
        PubnubWrapper.override_publish(self.mock_publish)
        url = self.CURRENT_ACTIVITY_URL + '/%s/' % self.room_name

        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        url = self.CURRENT_ACTIVITY_URL + '/%s/' % self.room_name

        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    def test_update_instance_activity(self):

        PubnubWrapper.override_publish(self.mock_publish)
        d = {"soc_num": self.soc_number, "room_name": self.room_name}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUIZ_URL, data=ujson.dumps(d), content_type='application/json')

        self.checkOkResp(resp, 200)
        self.activity_id = ujson.loads(resp.content)['id']

        studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

        name = "Josh Baptiste"
        data = {"student_name": name, "activity_instance_id": self.activity_id}
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
        resp = self.client.post(self.SET_NAME_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkOkResp(resp, 200)

        d = {'question_id': self.question3.question_id, 'answer_text': "whatever",
             'activity_instance_id': self.activity_id, 'multi_response': False}
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 201)

        d = "user_uuid=%s&room_name=%s&state_data=%s" % (self.auth_token_user, self.room_name, 1)

        resp = self.client.post(self.UPDATE_INSTANCE_ACTIVITY, data=d,
                                content_type="application/x-www-form-urlencoded")
        self.assertEqual(resp.status_code, 200)

    def test_end_current_activity(self):

        PubnubWrapper.override_publish(self.mock_publish)
        d = 'soc_num=%s&room_name=%s' % (self.soc_number, self.room_name)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.START_QUIZ_URL, data=d,
                                content_type='application/x-www-form-urlencoded')

        self.checkOkResp(resp, 200)
        self.activity_id = ujson.loads(resp.content)['id']

        studentUuid = "anon" + ''.join([alphabet[random.randint(0, len(alphabet)-1)] for _ in range(20)])

        name = "Josh Baptiste"
        data = {"student_name": name, "activity_instance_id": self.activity_id}
        self.client.cookies["sa"] = Cryptography.encryptStudentAuth(studentUuid)
        resp = self.client.post(self.SET_NAME_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkOkResp(resp, 200)

        d = {'question_id': self.question3.question_id, 'answer_text': "whatever",
             'activity_instance_id': self.activity_id, 'multi_response': False}
        resp = self.client.post(self.SUBMIT_STUDENT_RESPONSE_URL, data=ujson.dumps(d), content_type="application/json")

        self.checkOkResp(resp, 201)
        d = "room_name=%s" % self.room_name

        resp = self.client.post(self.END_CURRENT_ACTIVITY, data=d,
                                content_type="application/x-www-form-urlencoded")
        logger.error(resp.content)
        self.assertEqual(resp.status_code, 200)

    def test_check_room_name(self):
        """

        :return:
        """
        d = "new_room_name=%s" % str("mineroom")

        resp = self.client.post(self.CHECK_NAME_URL, data=d, content_type="application/x-www-form-urlencoded")

        logger.error(resp.content)

        self.checkOkResp(resp, 200)

        self.assertEqual(resp.content, "{}")

        d = "new_room_name=%s" % self.room_name

        resp = self.client.post(self.CHECK_NAME_URL, data=d, content_type="application/x-www-form-urlencoded")

        self.checkErrResp(resp, 400)

        d = "new_room_name=%s" % str("mine_room")

        resp = self.client.post(self.CHECK_NAME_URL, data=d, content_type="application/x-www-form-urlencoded")

        self.checkErrResp(resp, 400)

    def test_check_join_room_user_with_2_prev_rooms(self):
        """

        :return:
        """
        room1 = Room()
        room1.created_by_id = self.user_id
        room1.name_lower = str(time.time()).replace('.', '')
        room1.name = room1.name_lower
        room1.save()

        time.sleep(0.1)
        room2 = Room()
        room2.created_by_id = self.user_id
        room2.name_lower = str(time.time()).replace('.', '')
        room2.name = room2.name_lower
        room2.save()

        PubnubWrapper.override_publish(self.mock_publish)
        url = self.CURRENT_ACTIVITY_URL + '/%s/' % self.room_name

        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        self.assertEqual(Room.objects.filter(created_by_id=self.user_id).count(), 3)

    def test_create_room_ok(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)

        room_name = str(time.time()).replace('.', '')

        data = {"room_name": room_name}

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps(data), content_type='application/json')

        self.checkOkResp(resp, 201, {})

        # try to create the same room name again
        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps(data), content_type='application/json')

        self.checkErrResp(resp, 400)

    def test_create_room_fail(self):
        """

        :return:
        """

        auth_token = None
        room_name = str(time.time()).replace('.', '')

        data = {"room_name":room_name}

        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps(data), content_type='application/json')

        self.checkErrResp(resp, 400)

        auth_token = "asa"
        room_name = str(time.time()).replace('.', '')

        data = {"room_name":room_name}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(auth_token)
        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps(data), content_type='application/json')

        self.checkErrResp(resp, 400)

        auth_token = self.auth_token_user
        room_name = None

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(auth_token)
        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps({}), content_type='application/json')

        self.checkErrResp(resp, 400)

        auth_token = self.auth_token_user
        room_name = " "

        data = {"room_name":room_name}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(auth_token)
        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps(data), content_type='application/json')

        self.checkErrResp(resp, 400)

        auth_token = self.auth_token_user
        room_name = str(time.time()).replace('.', '')


    def test_create_room_ok_premium_user(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com'%str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        for i in range(10):
            room_name = str(time.time()).replace('.', '')

            data = {"room_name": room_name}

            resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps(data), content_type='application/json')
            self.checkOkResp(resp, 201)
            time.sleep(0.01)

        room_name = str(time.time()).replace('.', '')

        data = {"room_name": room_name}

        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps(data), content_type='application/json')
        self.checkErrResp(resp, 400)

    def test_delete_default_room_fail(self):
        """

        :return:
        """

        cookie = "ua=%s" % urllib.quote(Cryptography.encryptUserAuth(self.auth_token_user))
        resp = self.client.delete(self.CREATE_ROOM_URL + self.room_name + "/", headers={"Cookie": cookie})

        self.checkErrResp(resp, 400)

    def test_delete_unexisting_room(self):
        """

        :return:
        """

        cookie = "ua=%s" % urllib.quote(Cryptography.encryptUserAuth(self.auth_token_user))
        resp = self.client.delete(self.CREATE_ROOM_URL + "room123/", headers={"Cookie": cookie})

        self.checkErrResp(resp, 400)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth("aaaaaaaaaaaaaaaaaaaaaaaaa")

        resp = self.client.delete(self.CREATE_ROOM_URL + "room123/")

        self.checkErrResp(resp, 400)

    def test_delete_from_premium_user_empty_room(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com'%str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        room_name = str(time.time()).replace('.', '')

        data = {"room_name": room_name}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)

        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps(data), content_type='application/json')
        self.checkOkResp(resp, 201)

        resp = self.client.delete(self.CREATE_ROOM_URL + room_name + "/")
        self.checkOkResp(resp, 200)

    def test_update_room_fail(self):
        """

        :return:
        """
        # user not found
        self.client.cookies["ua"] = Cryptography.encryptUserAuth("aaaaaaaaaa")
        data = ujson.dumps({"new_room_name": "bbbbbbbbb"})
        resp = self.client.put(self.CREATE_ROOM_URL + "room123/", data=data, content_type="application/json")

        self.checkErrResp(resp, 400)

        # room_not_found
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        data = ujson.dumps({"new_room_name": "bbbbbbbbb"})
        resp = self.client.put(self.CREATE_ROOM_URL + "room123/", data=data, content_type="application/json")

        self.checkErrResp(resp, 400)

        user = SocrativeUser(email='jenkins%s@socrative.com'%str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        data = ujson.dumps({"new_room_name": "bbbbbbbbb"})
        resp = self.client.put(self.CREATE_ROOM_URL + self.room_name + "/", data=data, content_type="application/json")

        self.checkErrResp(resp, 400)

    def test_update_room_ok_premium_user(self):
        """

        :return:
        """

        room_name = str(time.time()).replace('.', '')
        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        data = {"room_name": room_name}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps(data), content_type='application/json')

        self.checkOkResp(resp, 201)

        # room is created
        time.sleep(0.1)
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        data = ujson.dumps({"new_room_name": str(time.time()).replace(".", "")})
        resp = self.client.put(self.CREATE_ROOM_URL + room_name + "/", data=data, content_type="application/json")

        self.checkOkResp(resp, 200)

    def test_get_room_list_bad_auth(self):
        """

        :return:
        """

        self.client.cookies["ua"] = Cryptography.encryptUserAuth("asdasdasdasda")
        resp = self.client.get(self.GET_ROOM_LIST)

        self.checkErrResp(resp, 400)

    def test_get_room_list_ok_only_default_room(self):
        """
        """

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(self.GET_ROOM_LIST)

        self.checkOkResp(resp, 200)

        data = ujson.loads(resp.content)
        self.assertIn("rooms", data)
        self.assertEqual(len(data["rooms"]), 1)
        self.assertEqual(data["rooms"][0]["name"], self.room_name)
        self.assertTrue(data["rooms"][0]["default"])
        self.assertFalse(data["rooms"][0]["rostered"])

    def test_get_room_list_ok_default_and_one_room_premium_user(self):
        """
        """

        room_name = str(time.time()).replace('.', '')
        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({"room_name": "", "user_uuid": user.auth_token}),
                                content_type="application/json")
        default_room_name = ujson.loads(resp.content)["name"]
        self.checkOkResp(resp, 201)

        data = {"room_name": room_name}
        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps(data), content_type='application/json')

        self.checkOkResp(resp, 201)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.get(self.GET_ROOM_LIST)

        self.checkOkResp(resp, 200)

        data = ujson.loads(resp.content)
        self.assertIn("rooms", data)
        self.assertEqual(len(data["rooms"]), 2)
        self.assertIn(data["rooms"][0]["name"], [default_room_name, room_name])
        if data["rooms"][0]["name"] == default_room_name:
            self.assertTrue(data["rooms"][0]["default"])
            self.assertFalse(data["rooms"][0]["rostered"])
            self.assertFalse(data["rooms"][0]["active"])

            self.assertIn(data["rooms"][1]["name"], [default_room_name, room_name])
            self.assertFalse(data["rooms"][1]["default"])
            self.assertFalse(data["rooms"][1]["rostered"])
            self.assertTrue(data["rooms"][1]["in_menu"])
            self.assertFalse(data["rooms"][1]["active"])
        else:
            self.assertTrue(data["rooms"][1]["default"])
            self.assertFalse(data["rooms"][1]["rostered"])
            self.assertFalse(data["rooms"][1]["active"])

            self.assertIn(data["rooms"][0]["name"], [default_room_name, room_name])
            self.assertFalse(data["rooms"][0]["default"])
            self.assertFalse(data["rooms"][0]["rostered"])
            self.assertTrue(data["rooms"][0]["in_menu"])
            self.assertFalse(data["rooms"][0]["active"])

    @skip("feature will be removed")
    def test_get_available_room_bad_auth_token(self):
        """
        """

        self.client.cookies["ua"] = Cryptography.encryptUserAuth("asdadasda")
        resp = self.client.get(self.AVAILABLE_ROOM_URL)

        self.checkErrResp(resp, 400)

    @skip("feature will be removed")
    def test_get_available_room_ok(self):
        """
        """

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.get(self.AVAILABLE_ROOM_URL)

        room_name = ujson.loads(resp.content)["name"]
        self.checkOkResp(resp, 200)

        data = {"room_name": room_name}
        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkOkResp(resp, 200)

    def test_update_rooms_status_bad_auth_token(self):
        """
        """
        data = {"statuses": [{"room_id": 1000, "in_menu":False}]}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth("balbalbla")
        resp = self.client.post(self.ROOM_STATUS_URL, data=ujson.dumps(data), content_type='application/json')

        self.checkErrResp(resp, 400)


    def test_update_room_status_bad_request(self):
        """
        """
        data = [{"room_name": "blabla", "in_menu": False}]
        self.client.cookies["ua"] = Cryptography.encryptUserAuth("balbalbla")
        resp = self.client.post(self.ROOM_STATUS_URL, data=ujson.dumps(data), content_type='application/json')

        self.checkErrResp(resp, 400)

    def test_update_room_status_bad_room_id(self):
        """
        """
        data = {"statuses": [{"room_id": 1000, "in_menu": False}]}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.ROOM_STATUS_URL, data=ujson.dumps(data), content_type='application/json')

        self.checkOkResp(resp, 200)
        roomIds = ujson.loads(resp.content)
        self.assertIn("room_ids", roomIds)
        self.assertEqual(len(roomIds["room_ids"]), 1)
        self.assertIn(1000, roomIds["room_ids"])

    def test_update_room_status_invalid_id_in_between_good_ids(self):
        """
        """

        room_name = str(time.time()).replace('.', '')
        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)

        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps({"room_name": room_name}), content_type='application/json')
        self.checkOkResp(resp, 201)
        room_id = Room.objects.filter(created_by_id=user.id).last().pk

        data = {"statuses": [{"room_id": 1000, "in_menu": False}, {"room_id": room_id, "in_menu": False}]}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.ROOM_STATUS_URL, data=ujson.dumps(data), content_type='application/json')

        logger.error(resp.content)
        self.checkOkResp(resp, 200)
        roomIds = ujson.loads(resp.content)
        self.assertIn("room_ids", roomIds)
        self.assertEqual(len(roomIds["room_ids"]), 1)
        self.assertIn(1000, roomIds["room_ids"])

    def test_update_room_status_default_room(self):
        """
        """
        data = {"statuses": [{"room_id": self.room_id, "in_menu": False}]}
        self.client.cookies["ua"] = Cryptography.encryptUserAuth(self.auth_token_user)
        resp = self.client.post(self.ROOM_STATUS_URL, data=ujson.dumps(data), content_type='application/json')

        logger.error(resp.content)
        self.checkOkResp(resp, 200)
        roomIds = ujson.loads(resp.content)
        self.assertIn("room_ids", roomIds)
        self.assertEqual(len(roomIds["room_ids"]), 1)
        self.assertIn(self.room_id, roomIds["room_ids"])

    def test_update_room_status_premium_user_ok(self):
        """
        """
        room_name = str(time.time()).replace('.', '')
        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps({"room_name": room_name}), content_type='application/json')
        self.checkOkResp(resp, 201)

        room_id = Room.objects.filter(created_by_id=user.id).last().pk
        data = {"statuses": [{"room_id": room_id, "in_menu": False}]}
        resp = self.client.post(self.ROOM_STATUS_URL, data=ujson.dumps(data), content_type="application/json")

        self.checkOkResp(resp, 200)
        self.assertEqual(len(ujson.loads(resp.content)["room_ids"]), 0)

    def test_join_other_room_premium(self):
        """
        blah
        :return:
        """
        room_name = str(time.time()).replace('.', '')
        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        time.sleep(0.1)
        data = {"room_name": str(time.time()).replace(".","")}
        room1 = data["room_name"]

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps(data), content_type='application/json')
        self.checkOkResp(resp, 201)

        time.sleep(0.1)

        data = {"room_name": str(time.time()).replace(".","")}
        room2 = data["room_name"]

        time.sleep(0.1)
        resp = self.client.post(self.CREATE_ROOM_URL, data=ujson.dumps(data), content_type='application/json')
        self.checkOkResp(resp, 201)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({}),
                                content_type="application/json")

        self.checkOkResp(resp, 201)

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps({"name": room2}),
                                content_type="application/json")
        self.checkOkResp(resp, 200)

        data = ujson.loads(resp.content)
        self.assertEqual(data["name"], room2)

    def test_join_free_room_50_students_limit(self):
        """

        :return:
        """
        data = {"name": self.room_name}
        for _ in range(50):
            if "sa" in self.client.cookies:
                self.client.cookies.pop("sa")
            resp = self.client.post(self.JOIN_URL, data=ujson.dumps(data), content_type="application/json")
            self.checkOkResp(resp, 200)

        # the next join room should return 200
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps(data), content_type="application/json")
        self.checkOkResp(resp, 200)

    def test_join_free_room_50_students_limit_with_student_logout(self):
        """

        :return:
        """
        data = {"name": self.room_name}
        for _ in range(50):
            resp = self.client.post(self.JOIN_URL, data=ujson.dumps(data), content_type="application/json")
            self.checkOkResp(resp, 200)
            resp = self.client.post(self.LOGOUT_URL, data=ujson.dumps({"room_name": self.room_name}),
                                    content_type="application/json")
            self.assertEqual(resp.status_code, 200)
            try:
                content = ujson.loads(resp.content)
            except Exception:
                self.assertTrue(False)

        # the next join room should return 200
        resp = self.client.post(self.JOIN_URL, data=ujson.dumps(data), content_type="application/json")
        self.checkOkResp(resp, 200)


    def test_create_import_roster_settings_ok(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        data = {"import_order": 1, "import_type": 1, "import_header": True, "export_order":2, "export_type":2,
                "export_header":False}

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.ROSTER_SETTINGS_URL, data=ujson.dumps(data), content_type='application/json')
        self.checkOkResp(resp, 201)

    def test_create_import_roster_settings_fail(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        # wrong import order
        data = {"import_order": 3, "import_type": 1, "import_header": True, "export_order":2, "export_type":2,
                "export_header":False}

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.ROSTER_SETTINGS_URL, data=ujson.dumps(data), content_type='application/json')
        self.checkErrResp(resp, 400)

        # wrong import type
        data = {"import_order": 1, "import_type": 3, "import_header": True, "export_order": 2, "export_type": 2,
                "export_header": False}
        resp = self.client.post(self.ROSTER_SETTINGS_URL, data=ujson.dumps(data), content_type='application/json')
        self.checkErrResp(resp, 400)

        # wrong export order
        data = {"import_order": 1, "import_type": 1, "import_header": True, "export_order": 3, "export_type": 2,
                "export_header": False}
        resp = self.client.post(self.ROSTER_SETTINGS_URL, data=ujson.dumps(data), content_type='application/json')
        self.checkErrResp(resp, 400)

        # wrong export type
        data = {"import_order": 1, "import_type": 1, "import_header": True, "export_order": 2, "export_type": 3,
                "export_header": False}
        resp = self.client.post(self.ROSTER_SETTINGS_URL, data=ujson.dumps(data), content_type='application/json')
        self.checkErrResp(resp, 400)

    def test_get_roster_settings_ok(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        data = {"import_order": 1, "import_type": 1, "import_header": True, "export_order":2, "export_type":2,
                "export_header":False}

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.ROSTER_SETTINGS_URL, data=ujson.dumps(data), content_type='application/json')
        self.checkOkResp(resp, 201)

        resp = self.client.get(self.ROSTER_SETTINGS_URL)

        self.checkOkResp(resp, 200)

        content = resp.json()

        self.assertIn("import_header", content)
        self.assertTrue(content["import_header"])
        self.assertIn("export_header", content)
        self.assertFalse(content["export_header"])
        self.assertIn("import_order", content)
        self.assertEqual(content["import_order"], 1)
        self.assertIn("export_order", content)
        self.assertEqual(content["export_order"], 2)
        self.assertIn("import_type", content)
        self.assertEqual(content["import_type"], 1)
        self.assertIn("export_type", content)
        self.assertEqual(content["export_type"], 2)

    def test_update_import_roster_settings_ok(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        data = {"import_order": 1, "import_type": 1, "import_header": True, "export_order": 2, "export_type": 2,
                "export_header": False}

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.ROSTER_SETTINGS_URL, data=ujson.dumps(data), content_type='application/json')
        self.checkOkResp(resp, 201)

        id = resp.json()["id"]

        updateData = {"import_order": 2, "import_type": 2, "import_header": False}
        resp = self.client.put(self.ROSTER_SETTINGS_UPDATE_IMPORT_URL + "%s/" % id, data=ujson.dumps(updateData),
                               content_type='application/json')

        self.checkOkResp(resp, 200)

        resp = self.client.get(self.ROSTER_SETTINGS_URL)

        self.checkOkResp(resp, 200)

        content = resp.json()

        self.assertIn("import_header", content)
        self.assertFalse(content["import_header"])
        self.assertIn("export_header", content)
        self.assertFalse(content["export_header"])
        self.assertIn("import_order", content)
        self.assertEqual(content["import_order"], 2)
        self.assertIn("export_order", content)
        self.assertEqual(content["export_order"], 2)
        self.assertIn("import_type", content)
        self.assertEqual(content["import_type"], 2)
        self.assertIn("export_type", content)
        self.assertEqual(content["export_type"], 2)

    def test_update_export_roster_settings_ok(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        data = {"import_order": 1, "import_type": 1, "import_header": True, "export_order": 2, "export_type": 2,
                "export_header": False}

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)
        resp = self.client.post(self.ROSTER_SETTINGS_URL, data=ujson.dumps(data), content_type='application/json')
        self.checkOkResp(resp, 201)

        id = resp.json()["id"]

        updateData = {"export_order": 1, "export_type": 1, "export_header": True}
        resp = self.client.put(self.ROSTER_SETTINGS_UPDATE_EXPORT_URL + "%s/" % id, data=ujson.dumps(updateData),
                               content_type='application/json')

        self.checkOkResp(resp, 200)

        resp = self.client.get(self.ROSTER_SETTINGS_URL)

        self.checkOkResp(resp, 200)

        content = resp.json()

        self.assertIn("import_header", content)
        self.assertTrue(content["import_header"])
        self.assertIn("export_header", content)
        self.assertTrue(content["export_header"])
        self.assertIn("import_order", content)
        self.assertEqual(content["import_order"], 1)
        self.assertIn("export_order", content)
        self.assertEqual(content["export_order"], 1)
        self.assertIn("import_type", content)
        self.assertEqual(content["import_type"], 1)
        self.assertIn("export_type", content)
        self.assertEqual(content["export_type"], 1)

    def test_get_roster_settings_no_settings_available(self):
        """

        :return:
        """

        user = SocrativeUser(email='jenkins%s@socrative.com' % str(time.time()), password='123456')
        user.display_name = Cryptography.generate_random_password(6)
        user.auth_token = Cryptography.generate_auth_token()
        user.last_login = datetime.datetime.utcnow()
        user.date_joined = datetime.datetime.utcnow()
        user.save()

        l = Licenses()
        l.buyer = user
        l.key = str(time.time()).replace('.', "") + str(random.randint(0, 1000))
        l.expiration_date = datetime.datetime.utcnow() + datetime.timedelta(days=2)
        l.years = 1
        l.price_per_seat = 10
        l.save()

        la = License_Activations()
        la.license = l
        la.user = user
        la.activation_date = datetime.date.today()
        la.save()

        self.client.cookies["ua"] = Cryptography.encryptUserAuth(user.auth_token)

        resp = self.client.get(self.ROSTER_SETTINGS_URL)

        self.checkOkResp(resp, 200)

        content = resp.json()

        self.assertEqual(content, {})


